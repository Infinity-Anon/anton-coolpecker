var FileDownload = {
    DownloadFile: function (content, filename) {
        var link = document.createElement('a');
        link.setAttribute('href',
               'data:text/plain;charset=utf-8,' + encodeURIComponent(Pointer_stringify(content))
        );
        link.setAttribute('download', Pointer_stringify(filename));

        if (document.createEvent) {
            var event = document.createEvent('MouseEvents');
            event.initEvent('click', true, true);
            link.dispatchEvent(event);
        } else {
            link.click();
        }
    }
};
mergeInto(LibraryManager.library, FileDownload);
