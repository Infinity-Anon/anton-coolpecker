var FileUploaderPlugin = {
    FileUploaderCaptureClick: function () {
        if (!document.getElementById('FileUploaderInput')) {
            var fileInput = document.createElement('input');
            fileInput.setAttribute('type', 'file');
            fileInput.setAttribute('id', 'FileUploaderInput');
            fileInput.style.visibility = 'hidden';
            fileInput.onclick = function (event) {
                this.value = null;
            };
            fileInput.onchange = function (event) {
                SendMessage('ImportButton', 'FileSelected', URL.createObjectURL(event.target.files[0]));
            }
            document.body.appendChild(fileInput);
        }
        var OpenFileDialog = function () {
            document.getElementById('FileUploaderInput').click();
            document.getElementById('canvas').removeEventListener('click', OpenFileDialog);
        };
        document.getElementById('canvas').addEventListener('click', OpenFileDialog, false);
    }
};
mergeInto(LibraryManager.library, FileUploaderPlugin);
