// Shader created with Shader Forge Beta 0.34 
// Shader Forge (c) Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:0.34;sub:START;pass:START;ps:flbk:Standard,lico:1,lgpr:1,nrmq:1,limd:0,uamb:True,mssp:True,lmpd:False,lprd:False,enco:False,frtr:True,vitr:True,dbil:False,rmgx:True,rpth:0,hqsc:True,hqlp:False,blpr:0,bsrc:0,bdst:1,culm:0,dpts:2,wrdp:True,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:1,x:32719,y:32712|custl-6-OUT;n:type:ShaderForge.SFN_Tex2d,id:3,x:33355,y:32631,tex:ecfc9c1221d7e3a4dbebfedfcbf8dd0a,ntxv:0,isnm:False|TEX-4-TEX;n:type:ShaderForge.SFN_Tex2dAsset,id:4,x:33642,y:32668,ptlb:Texture,ptin:_Texture,glob:False,tex:ecfc9c1221d7e3a4dbebfedfcbf8dd0a;n:type:ShaderForge.SFN_Blend,id:6,x:33063,y:32665,blmd:1,clmp:True|SRC-3-RGB,DST-31-RGB;n:type:ShaderForge.SFN_VertexColor,id:31,x:33359,y:32896;proporder:4;pass:END;sub:END;*/

Shader "AntonCoolpecker/VertColorUnlitCustomQueue" {
    Properties {
        _Texture ("Texture", 2D) = "white" {}
	_TilingX("Tiling X", Float) = 1.0
	_TilingY("Tiling Y", Float) = 1.0
	//_AlphaMult("Alpha Multiplier", Range(0.0, 5.0)) = 1.0
	_VertexMult("Vertex Colour Multiplier", Range(0.0, 10.0)) = 1.0
	//_CustomRenderVal("Custom Render Queue Value", Float) = 1000.0
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
            //"Queue" = "Geometry+" + _CustomRenderVal + ""
            "Queue" = "Geometry+1000"
        }
        Pass {
            Name "ForwardBase"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
	    #pragma multi_compile_fog
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma exclude_renderers xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0
            uniform sampler2D _Texture; uniform float4 _Texture_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
		UNITY_FOG_COORDS(1)
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };

	    half _TilingX;
            half _TilingY;

            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.texcoord0 * half2(_TilingX, _TilingY);
                o.vertexColor = v.vertexColor;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
		UNITY_TRANSFER_FOG(o,o.pos);
		//TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }

	    //half _AlphaMult;
	    half _VertexMult;
	   
            fixed4 frag(VertexOutput i) : COLOR {
/// Lighting:
		fixed4 col = tex2D(_Texture, i.uv0);
		UNITY_APPLY_FOG(i.fogCoord, col);	
                UNITY_OPAQUE_ALPHA(col.a);
		float3 finalVColor = i.vertexColor.rgb * _VertexMult;	
/// Final Color:
                return fixed4(col.rgb * finalVColor, 1);
            }
            ENDCG
        }
    }
    FallBack "Standard"
    CustomEditor "ShaderForgeMaterialInspector"
}
