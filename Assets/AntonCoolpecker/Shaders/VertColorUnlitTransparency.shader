Shader "AntonCoolpecker/VertColorUnlitTransparency" {
    Properties {
    _Texture("Texture", 2D) = "white" {}
	_TilingX("Tiling X", Float) = 1.0
	_TilingY("Tiling Y", Float) = 1.0
	//_AlphaMult("Alpha Multiplier", Range(0.0, 5.0)) = 1.0
	_VertexMult("Vertex Colour Multiplier", Range(0.0, 10.0)) = 1.0
    }

    SubShader {
    	///LOD 200
        Tags {
        	//"LightMode"="Vertex" "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"
            //"RenderType"="Opaque"
            /// /// ///"LightMode"="Vertex" "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"
            "LightMode"="ForwardBase"
            "Queue"="Transparent" 
            "RenderType"="Opaque"
            //"ForceNoShadowCasting"="True"
        }

        //Stencil {
        //	Ref 2
        //	Comp Always
        //	Pass Replace
        //}

        //ZTest Equal
        //ZWrite On

        //LOD 100
        //Cull back

        Pass {
            Name "ForwardBase"
            ///Tags { "LightMode"="Vertex" "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }

            ///Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
			///Blend Zero SrcColor
			///Cull Off Lighting Off ZWrite Off

			///ZWrite On
       		///ColorMask 0

       		//ZWrite On
       		///Blend SrcAlpha OneMinusSrcAlpha
       		//ColorMask RGB

       		Blend SrcAlpha OneMinusSrcAlpha
       		//ColorMask RGB
       		//Cull Off

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
	   		#pragma multi_compile_fog
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma exclude_renderers xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0

            uniform sampler2D _Texture; uniform float4 _Texture_ST;

            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
				UNITY_FOG_COORDS(1)
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                fixed4 vertexColor : COLOR;
            };

            float4 _MainTex_ST;
		    half _TilingX;
	        half _TilingY;

	        VertexOutput vert (VertexInput v) {
	            VertexOutput o;
	            o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
	            o.vertexColor = v.vertexColor;
	            o.uv0 = v.texcoord0 * half2(_TilingX, _TilingY);
				UNITY_TRANSFER_FOG(o,o.pos);
				//TRANSFER_VERTEX_TO_FRAGMENT(o)
	            return o;
	        }

		    //half _AlphaMult;
		    half _VertexMult;
		   
	        fixed4 frag(VertexOutput i) : SV_Target {
				/// - Lighting:
				///fixed4 col = tex2D(_Texture, i.uv0);
				/// half4 prev = tex2D(_Texture, i.uv0);
				/// fixed4 col = lerp(half4(1,1,1,1), prev, prev.a);
				/// UNITY_APPLY_FOG_COLOR(i.fogCoord, col, fixed4(1,1,1,1));

				//UNITY_APPLY_FOG(i.fogCoord, col);	
	            ///UNITY_OPAQUE_ALPHA(col.a);

				//float3 finalVColor = i.vertexColor.rgba * _VertexMult;	
                //return fixed4(col.rgba * finalVColor, 1);

                ////half4 prev = tex2D(_Texture, i.uv0);
                ////float3 finalVColor = i.vertexColor.rgb * _VertexMult;
                //fixed4 col = tex2D(_Texture, i.uv0);
                ////fixed4 col = lerp(half4(finalVColor, 0), prev, prev.a);
				//UNITY_APPLY_FOG(i.fogCoord, col);	
                //UNITY_OPAQUE_ALPHA(col.a);
				//float3 finalVColor = i.vertexColor.rgb * _VertexMult;
				////return col;

                // //half4 prev = i.vertexColor * tex2D(_Texture, i.uv0);
				// //fixed4 col = lerp(half4(1,1,1,0), prev, prev.a);
				// //UNITY_APPLY_FOG_COLOR(i.fogCoord, col, fixed4(1,1,1,1)); // fog towards white due to our blend mode
				// //return col;

				half4 prev = _VertexMult * i.vertexColor * tex2D(_Texture, i.uv0);
				fixed4 col = prev;
				//UNITY_APPLY_FOG_COLOR(i.fogCoord, col, fixed4(1,1,1,1)); // fog towards white due to our blend mode
				UNITY_APPLY_FOG(i.fogCoord, col);	
                //UNITY_OPAQUE_ALPHA(col.a);
				return col;
            }
            ENDCG
        }
    }
    ///FallBack "Standard"
}
