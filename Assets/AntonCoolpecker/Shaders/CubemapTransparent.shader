    Shader "Skybox/CubemapTransparent" {
    Properties {
        _Tint ("Tint 1 Color", Color) = (.5, .5, .5, .5)
		_TintTwo ("Tint 2 Color", Color) = (.5, .5, .5, .5)
        [Gamma] _Exposure ("Exposure", Range(0, 8)) = 1.0
        _Rotation ("Cubemap Rotation", Range(0, 360)) = 0
		_Alpha ("Cubemap Lerp Value", Range(0, 1.0)) = 0.5
		_AlphaTex ("Cubemap 1 Transparency", Range(0, 1.0)) = 0.5
		_AlphaTexTwo ("Cubemap 2 Transparency", Range(0, 1.0)) = 0.5
        [NoScaleOffset] _Tex ("Cubemap 1  (HDR)", Cube) = "grey" {}
		[NoScaleOffset] _TexTwo ("Cubemap 2  (HDR)", Cube) = "grey" {}
    }
     
    SubShader {
        Tags { "Queue"="Background" "RenderType"="Background" "PreviewType"="Skybox" }
        Cull Off ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha
     
        Pass {
         
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
     
            #include "UnityCG.cginc"
     
            samplerCUBE _Tex;
			samplerCUBE _TexTwo;
            half4 _Tex_HDR;
            half4 _Tint;
			half4 _TintTwo;
            half _Exposure;
            float _Rotation;
     
            float4 RotateAroundYInDegrees (float4 vertex, float degrees)
            {
                float alpha = degrees * UNITY_PI / 180.0;
                float sina, cosa;
                sincos(alpha, sina, cosa);
                float2x2 m = float2x2(cosa, -sina, sina, cosa);
                return float4(mul(m, vertex.xz), vertex.yw).xzyw;
            }
         
            struct appdata_t {
                float4 vertex : POSITION;
            };
     
            struct v2f {
                float4 vertex : SV_POSITION;
                float3 texcoord : TEXCOORD0;
            };
     
            v2f vert (appdata_t v)
            {
                v2f o;
                o.vertex = mul(UNITY_MATRIX_MVP, RotateAroundYInDegrees(v.vertex, _Rotation));
                o.texcoord = v.vertex;
                return o;
            }
			
			float _Alpha;
			float _AlphaTex;
			float _AlphaTexTwo;
     
            fixed4 frag (v2f i) : SV_Target
            {
                half4 tex = texCUBE (_Tex, i.texcoord);
                half3 c = DecodeHDR (tex, _Tex_HDR);
                c = c * _Tint.rgb * unity_ColorSpaceDouble;
                c *= _Exposure;
				
				half4 texmex = texCUBE (_TexTwo, i.texcoord);
                half3 d = DecodeHDR (texmex, _Tex_HDR);
                d = d * _TintTwo.rgb * unity_ColorSpaceDouble;
                d *= _Exposure;
				
				half4 res = lerp(half4(c, _AlphaTex), half4(d, _AlphaTexTwo), _Alpha);
				return res;
            }
            ENDCG
        }
    }  
     
     
    Fallback Off
     
    }
     
