﻿using AntonCoolpecker.Abstract.Enemies.States;
using AntonCoolpecker.Abstract.StateMachine;
using AntonCoolpecker.Concrete.Utils;
using UnityEngine;

namespace AntonCoolpecker.API.Listeners.Enemies
{
	/// <summary>
	/// Enemy Interface listener - Contains all of the enemy-related messages.
	/// </summary>
	public interface IEnemyListener
	{
		/// <summary>
		/// Called when the enemy changes state.
		/// </summary>
		/// <param name="changeInfo">Change info.</param>
		void OnStateChange(StateChangeInfo<AbstractEnemyState> changeInfo);

		/// <summary>
		/// Called when the enemy gets damaged/hit.
		/// </summary>
		/// <param name="collision">Collision.</param>
		void OnDamage(CharacterLocomotorCollisionData collision);

		/// <summary>
		/// Called when an enemy-related animation event occurs.
		/// </summary>
		/// <param name="animationEvent">Animation event.</param>
		void OnAnimationEvent(AnimationEvent animationEvent);
	}
}