﻿namespace AntonCoolpecker.API.Listeners.Collectables
{
	/// <summary>
	/// Collectable Interface listener - Contains all collectable-related messages
	/// whenever an object(Player, enemy, etc.) collects a collectable.
	/// </summary>
	public interface ICollectableListener
	{
		#region Messages

		/// <summary>
		/// Called when the collectable gets collected.
		/// </summary>
		void OnCollected();

		#endregion
	}
}