﻿namespace AntonCoolpecker.API.Listeners.Player.Coolpecker
{
	/// <summary>
	/// Coolpecker Interface listener - Contains all of the Coolpecker-specific player messages.
	/// </summary>
	public interface ICoolpeckerListener : IPlayerListener {}
}