﻿namespace AntonCoolpecker.API.Listeners.Player.Anton
{
	/// <summary>
	/// Anton Interface listener - Contains all of the Anton-specific player messages.
	/// </summary>
	public interface IAntonListener : IPlayerListener {}
}