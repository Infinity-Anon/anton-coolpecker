﻿using AntonCoolpecker.Abstract.Player.States;
using AntonCoolpecker.Abstract.StateMachine;
using AntonCoolpecker.Concrete.Utils;
using UnityEngine;

namespace AntonCoolpecker.API.Listeners.Player
{
	/// <summary>
	/// Player Interface Listener - Contains all of the player-related messages.
	/// </summary>
	public interface IPlayerListener
	{
		#region Messages

		/// <summary>
		/// 	Called when the player jumps.
		/// </summary>
		void OnJump();

		/// <summary>
		/// 	Called when the player gets damaged/hit.
		/// </summary>
		/// <param name="collision">The collision info.</param>
		void OnDamage(CharacterLocomotorCollisionData collision);

		/// <summary>
		/// 	Called when the state is changed.
		/// </summary>
		/// <param name="changeInfo">Change info.</param>
		void OnStateChange(StateChangeInfo<AbstractPlayerState> changeInfo);

		/// <summary>
		/// 	Called when the player changes playable character.
		/// </summary>
		void OnSwapEnter();

		/// <summary>
		/// 	Called when an player-related animation event is fired.
		/// </summary>
		/// <param name="animationEvent">Animation event.</param>
		void OnAnimationEvent(AnimationEvent animationEvent);

		#endregion
	}
}
