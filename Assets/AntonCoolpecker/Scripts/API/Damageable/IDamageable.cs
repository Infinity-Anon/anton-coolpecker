﻿using AntonCoolpecker.Concrete.Utils;

namespace AntonCoolpecker.API.Damageable
{
	/// <summary>
	/// Damageable Interface - Contains all of the damage-related messages 
	/// whenever an object(Player, enemy, etc.) gets damaged/hit in-game.
	/// </summary>
	public interface IDamageable
	{
		/// <summary>
		/// Called when the Damageable gets damaged.
		/// </summary>
		/// <param name="damage">Amount of damage to apply.</param>
		/// <param name="collision">Collision info.</param>
		void Damage(int damage, CharacterLocomotorCollisionData collision);

		/// <summary>
		/// Called when the Damageable gets damaged.
		/// </summary>
		/// <param name="damage">Amount of damage to apply.</param>
		/// <param name="other">Collider that hit us.</param>
		//void Damage(int damage, Collider other);

		/// <summary>
		/// 	Called when the Damageable gets damaged because of a collision with two rigidbodies
		/// </summary>
		/// <param name="damage">Damage.</param>
		/// <param name="collision">Collision.</param>
		///void Damage(int damage, Collision collision);
	}
}