﻿using Hydra.HydraCommon.Editor.Utils;
using System;
using UnityEditor;

namespace AntonCoolpecker.Editor
{
	/// <summary>
	/// Editor build helper
	/// </summary>
	public class Build
	{
		#region Static Functions

		/// <summary>
		/// Structures up the build and runs it.
		/// </summary>
		public static void FromEnvironment()
		{
			string buildOutput = Environment.GetEnvironmentVariable("BUILD_OUTPUT");
			string targetString = Environment.GetEnvironmentVariable("BUILD_TARGET");
			BuildTarget buildTarget = (BuildTarget)Enum.Parse(typeof(BuildTarget), targetString);
			RunBuild(buildTarget, buildOutput);
		}

		/// <summary>
		/// 	Runs the build.
		/// </summary>
		/// <param name="buildTarget">Build target.</param>
		/// <param name="buildOutput">Build output.</param>
		private static void RunBuild(BuildTarget buildTarget, string buildOutput)
		{
			PreBuild();

			EditorBuildSettingsScene[] scenes = EditorSceneUtils.GetEnabledScenes();
			string[] paths = new string[scenes.Length];

			for (int index = 0; index < scenes.Length; index++)
				paths[index] = scenes[index].path;

			// Build player.
			string error = BuildPipeline.BuildPlayer(paths, buildOutput, buildTarget, BuildOptions.None);
			if (!String.IsNullOrEmpty(error))
				throw new Exception(error);
		}

		/// <summary>
		/// 	Called before the automated build.
		/// </summary>
		private static void PreBuild()
		{
			// Refresh the lighting
			Lightmapping.ClearDiskCache();
			Lightmapping.Bake();
		}

		#endregion
	}
}