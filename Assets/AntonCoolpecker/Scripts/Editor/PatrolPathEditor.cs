﻿using System.Collections;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace AntonCoolpecker.Editor
{
    [CustomEditor(typeof(PatrolPath), true)]

	/// <summary>
	/// Patroll path editor.
	/// </summary>
    public class PatrollPathEditor : UnityEditor.Editor
    {
		#region Variables

        PatrolPath m_PatrolPath; //Reference to current patrol path under edit
        protected Tool LastTool = Tool.Move; //Stores which editor tool that was last used in Unity editor
        private ReorderableList _list; //Stores all the patrol points visible in Unity editor

		#endregion

		#region Functions

		/// <summary>
		/// Raises the enable event.
		/// </summary>
        void OnEnable()
        {
            LastTool = Tools.current;
            Tools.current = Tool.None;

            m_PatrolPath = (PatrolPath)target;

            _list = new ReorderableList(serializedObject, serializedObject.FindProperty("EditorPoints"), true, true, true, true);

            GUIContent delayLable = new GUIContent();
            delayLable.text = "delay";
          
            _list.drawElementCallback = (rect, index, active, focused) =>
            {
                var element = _list.serializedProperty.GetArrayElementAtIndex(index);
                rect.y += 2;
                float boxWidth = rect.width * 0.5f;
               
                EditorGUI.PropertyField(new Rect(rect.x, rect.y, boxWidth, EditorGUIUtility.singleLineHeight),
                    element.FindPropertyRelative("position"), GUIContent.none);
				
                EditorGUI.PropertyField(new Rect(rect.x + boxWidth, rect.y, boxWidth, EditorGUIUtility.singleLineHeight),
                    element.FindPropertyRelative("delay"), delayLable);
                
            };

            _list.drawHeaderCallback = rect =>
            {
            	// draw the header
        	};
        }

        void OnDisable()
        {
            Tools.current = LastTool;
        }

		/// <summary>
		/// Handles the position of a certain patrol point
		/// </summary>
		/// <param name="PointIndex">Point index.</param>
        private void PositionHandle(int PointIndex)
        {
            Vector3 point = m_PatrolPath.EditorPoints[PointIndex].position;
            Handles.Label(point + m_PatrolPath.transform.position - Vector3.up * 3, (1 + PointIndex).ToString());
            EditorGUI.BeginChangeCheck();
            
            point = Handles.PositionHandle(point + m_PatrolPath.transform.position, Quaternion.identity);

            if (EditorGUI.EndChangeCheck())
            {
                PatrolPoint ChangedPoint = m_PatrolPath.EditorPoints[PointIndex];

                ChangedPoint.position = (point - m_PatrolPath.transform.position);
                m_PatrolPath.EditorPoints[PointIndex] = ChangedPoint;
            }
        }

		#endregion

		#region Override Functions

		/// <summary>
		/// Raises the inspector GUI event.
		/// </summary>
        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            _list.DoLayoutList();

            DrawDefaultInspector();

            serializedObject.ApplyModifiedProperties();
        }

		#endregion

		#region Virtual Functions

		/// <summary>
		/// Raises the scene GUI event.
		/// </summary>
        protected virtual void OnSceneGUI()
        {
            Tools.current = Tool.None;

            m_PatrolPath = (PatrolPath)target;
            for (int i = 0; i < m_PatrolPath.EditorPoints.Count; i++)
                PositionHandle(i);
        }

		#endregion
    }
}