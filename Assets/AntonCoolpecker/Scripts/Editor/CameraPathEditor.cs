﻿using System.Collections;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace AntonCoolpecker.Editor
{
    [CustomEditor(typeof(CameraPath), true)]

	/// <summary>
	/// Camera path editor.
	/// </summary>
    public class CameraPathEditor : UnityEditor.Editor
    {
		CameraPath m_CamPath; //Reference to current patrol path under edit
		protected Tool LastTool = Tool.Move; //Stores which editor tool that was last used in Unity editor
		private ReorderableList _list; //Stores all the camera path points visible in Unity editor

		/// <summary>
		/// Called when component is enabled (i.e editor opening or compile)
		/// </summary>
        void OnEnable()
        {
            LastTool = Tools.current;
            Tools.current = Tool.None;

            m_CamPath = (CameraPath)target;

            _list = new ReorderableList(serializedObject, serializedObject.FindProperty("EditorPoints"), true, true, true, true);

            GUIContent delayLable = new GUIContent();
            delayLable.text = "Time To This Node";
            
            _list.elementHeight = EditorGUIUtility.singleLineHeight * 7f;

            //Define what the List elements will be
            _list.drawElementCallback = (rect, index, active, focused) =>
            {
                var element = _list.serializedProperty.GetArrayElementAtIndex(index);
                rect.y += 2;
                float boxWidth = rect.width * 0.5f;

                //Position
                EditorGUI.PropertyField(
                    new Rect(rect.x, rect.y, boxWidth, EditorGUIUtility.singleLineHeight),
                    element.FindPropertyRelative("position"), GUIContent.none);
				
               //rotation
               EditorGUI.PropertyField(
                     new Rect(rect.x, rect.y+EditorGUIUtility.singleLineHeight, boxWidth, EditorGUIUtility.singleLineHeight),
                    element.FindPropertyRelative("Rotation"), new GUIContent("Rotation"),true);  
				
                //Time               
                EditorGUI.PropertyField(
                    new Rect(rect.x + boxWidth, rect.y, boxWidth, EditorGUIUtility.singleLineHeight),
                    element.FindPropertyRelative("TimeToThisNode"),delayLable);
				
                //FOV
                EditorGUI.PropertyField(
                   new Rect(rect.x + boxWidth, rect.y+EditorGUIUtility.singleLineHeight, boxWidth, EditorGUIUtility.singleLineHeight),
                   element.FindPropertyRelative("FieldOfView"), new GUIContent("FOV"));
				
                //Fulstrum toggle
                EditorGUI.PropertyField(
                   new Rect(rect.x + boxWidth, rect.y + EditorGUIUtility.singleLineHeight*2, boxWidth, EditorGUIUtility.singleLineHeight),
                   element.FindPropertyRelative("DrawFrustrum"), new GUIContent("Draw Cam frustrum"));
				
                //Input Requirement
                EditorGUI.PropertyField(
                  new Rect(rect.x + boxWidth, rect.y + EditorGUIUtility.singleLineHeight * 3, boxWidth, EditorGUIUtility.singleLineHeight),
                  element.FindPropertyRelative("RequireInputToAdvance"), new GUIContent("RequireInputToAdvance","If input is required to advance/end the path at this node"));
				
                //Change how t is calculated
                EditorGUI.PropertyField(
                  new Rect(rect.x + boxWidth, rect.y + EditorGUIUtility.singleLineHeight * 4, boxWidth, EditorGUIUtility.singleLineHeight),
                  element.FindPropertyRelative("TimeOrSpeed"), new GUIContent("False Time or True speed", "whever time to this node is an overall speed,"+
                  "I.E all nodes with 1 will have the same move speed regardless of distance."+
                  " or if it is how long it will take to get to this node from a previous node. NOT YET IMPLMENTED"));

                //Can we skip ahead to this one
                EditorGUI.PropertyField(
                 new Rect(rect.x + boxWidth, rect.y + EditorGUIUtility.singleLineHeight * 5, boxWidth, EditorGUIUtility.singleLineHeight),
                 element.FindPropertyRelative("InputAutoAdvance"), new GUIContent("Input Auto Advance","Will Input auto advance to this node if it is next"));
            };

            _list.drawHeaderCallback = rect =>
            {
                // draw the header
            };
        }

		/// <summary>
		/// Called when this component is disabled.
		/// </summary>
        void OnDisable()
        {
            Tools.current = LastTool;
        }

        /// <summary>
        /// Handles the position of a camera path point.
        /// </summary>
        /// <param name="PointIndex">Point index.</param>
        private void PositionHandle(int PointIndex)
        {
            Vector3 point = m_CamPath.EditorPoints[PointIndex].position;
            Handles.Label(point + m_CamPath.transform.position - Vector3.up * 3, (1 + PointIndex).ToString());
            EditorGUI.BeginChangeCheck();

            point = Handles.PositionHandle(point + m_CamPath.transform.position, Quaternion.identity);

            if (EditorGUI.EndChangeCheck())
            {
                CameraPoint ChangedPoint = m_CamPath.EditorPoints[PointIndex];

                ChangedPoint.position = (point - m_CamPath.transform.position);
                m_CamPath.EditorPoints[PointIndex] = ChangedPoint;
            }
        }

		/// <summary>
		/// Handles the rotation of a camera path point.
		/// </summary>
		/// <param name="PointIndex">Point index.</param>
        private void RotationHandle(int PointIndex)
        {
            Vector3 point = m_CamPath.EditorPoints[PointIndex].position;
           // Quaternion rotation = m_CamPath.EditorPoints[PointIndex].Rotation;
            EditorGUI.BeginChangeCheck();

            Quaternion rotation = Handles.RotationHandle(m_CamPath.EditorPoints[PointIndex].Rotation,point + m_CamPath.transform.position);

            if (EditorGUI.EndChangeCheck())
            {
                CameraPoint ChangedPoint = m_CamPath.EditorPoints[PointIndex];

                ChangedPoint.Rotation = rotation;
                m_CamPath.EditorPoints[PointIndex] = ChangedPoint;
            }
        }

        /// <summary>
        /// Raises the inspector GUI event.
        /// </summary>
        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            m_CamPath.m_ID= EditorGUILayout.IntField("ID", m_CamPath.m_ID);
         	//m_CamPath.Skipable = EditorGUILayout.Toggle("Skipable", m_CamPath.Skipable);
            m_CamPath.m_Level = (AntonCoolpecker.Concrete.Collectables.Level)EditorGUILayout.EnumPopup("Level",m_CamPath.m_Level);
            _list.DoLayoutList();
          
            serializedObject.ApplyModifiedProperties();
        }

        /// <summary>
        /// Raises the scene GUI event.
        /// </summary>
        protected virtual void OnSceneGUI()
        {
            Tools.current = Tool.None;

            m_CamPath = (CameraPath)target;

            for (int i = 0; i < m_CamPath.EditorPoints.Count; i++)
            {
                PositionHandle(i);
                RotationHandle(i);
            }
        }
    }
}