﻿using UnityEditor;

namespace AntonCoolpecker.Editor.Windows
{
	/// <summary>
	/// 	AcpEditorWindow is the base class for all ACP editor windows.
	/// </summary>
	public abstract class AcpEditorWindow : EditorWindow
	{
		#region Variables

		public const string MENU = "Window/Anton and Coolpecker/";

		#endregion

		#region Virtual Functions

		/// <summary>
		/// 	Called to draw the window contents.
		/// </summary>
		protected virtual void OnGUI() {}

		#endregion
	}
}