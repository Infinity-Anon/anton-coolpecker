﻿using Hydra.HydraCommon.Editor.Utils;
using UnityEditor;

namespace AntonCoolpecker.Editor
{
	/// <summary>
	/// 	Autorun allows us to do some automatic maintenance.
	/// </summary>
	[InitializeOnLoad]
	public static class Autorun
	{
		#region Static Functions/Methods

		/// <summary>
		/// 	Called when the editor starts, and every time the assembly is recompiled.
		/// </summary>
		static Autorun()
		{
			MaintenanceUtils.RemoveEmptyDirectories();
		}

		#endregion
	}
}