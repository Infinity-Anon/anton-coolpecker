﻿using AntonCoolpecker.Concrete.Menus.DebugGUI;
using UnityEditor;
using UnityEngine;

namespace AntonCoolpecker.Editor
{
	/// <summary>
	/// Debug GUI helper.
	/// </summary>
	public class DebugGuiHelper
	{
		#region Static Functions

		/// <summary>
		/// Populates the DebugGUI prefab's 'Other Objects' list with all the scriptable objects in the project
		/// </summary>
		[MenuItem("Custom/Populate DebugGUI object list")]
		public static void PopulatePrefabList()
		{
			string[] debugGuis = AssetDatabase.FindAssets("t:prefab DebugGUI");

			if (debugGuis.Length == 0)
			{
				EditorUtility.DisplayDialog("Error", "DebugGUI prefab not found.", "OK");
				return;
			}

			if (debugGuis.Length > 1)
			{
				EditorUtility.DisplayDialog("Error", "More than one DebugGUI prefab found.", "OK");
				return;
			}

			try
			{
				GameObject debugGuiPrefab = AssetDatabase.LoadAssetAtPath<GameObject>(AssetDatabase.GUIDToAssetPath(debugGuis[0]));

				DebugGuiComponent debugGuiComponent = debugGuiPrefab.GetComponent<DebugGuiComponent>();

				string[] objects = AssetDatabase.FindAssets("t:scriptableobject");
				debugGuiComponent.otherObjects.Clear();
				debugGuiComponent.otherObjects.Capacity = objects.Length;

				foreach (string guid in objects)
				{
					Object obj = AssetDatabase.LoadAssetAtPath<Object>(AssetDatabase.GUIDToAssetPath(guid));
					debugGuiComponent.otherObjects.Add(obj);
				}

				EditorUtility.DisplayDialog("Success", "Loaded " + objects.Length.ToString() + " objects.", "OK");
			}
			catch (System.Exception e)
			{
				EditorUtility.DisplayDialog("Error", e.Message, "OK");
			}
		}

		#endregion
	}
}