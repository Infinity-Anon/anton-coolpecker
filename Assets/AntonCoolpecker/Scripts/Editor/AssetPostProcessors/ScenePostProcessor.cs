﻿using AntonCoolpecker.Concrete.Utils;
using Hydra.HydraCommon.Editor.Utils;
using Hydra.HydraCommon.Utils;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace AntonCoolpecker.Editor.AssetPostProcessors
{
	/// <summary>
	/// Scene post processor.
	/// </summary>
	public class ScenePostProcessor : AssetPostprocessor
	{
		#region Variables

		public const string SCENE_EXT = ".unity";
		public const string EDITOR_BUILD_SETTINGS_FILENAME = "EditorBuildSettings.asset";

		#endregion

		#region Functions/Methods

		/// <summary>
		/// 	Unity doesn't provide scene names at runtime, so we dump them to a text asset.
		/// </summary>
		/// <param name="imported">Imported.</param>
		/// <param name="deleted">Deleted.</param>
		/// <param name="moved">Moved.</param>
		/// <param name="movedFromAssetPaths">Moved from asset paths.</param>
		private static void OnPostprocessAllAssets(string[] imported, string[] deleted, string[] moved,
												   string[] movedFromAssetPaths)
		{
			string[] paths = GetModified(imported, deleted, moved, movedFromAssetPaths);

			for (int index = 0; index < paths.Length; index++)
			{
				if (IsScene(paths[index]) || IsBuildSettings(paths[index]))
				{
					DumpSceneNames();
					return;
				}
			}
		}

		/// <summary>
		/// 	Gets all of the modified paths.
		/// </summary>
		/// <returns>The paths.</returns>
		/// <param name="imported">Imported.</param>
		/// <param name="deleted">Deleted.</param>
		/// <param name="moved">Moved.</param>
		/// <param name="movedFromAssetPaths">Moved from asset paths.</param>
		private static string[] GetModified(string[] imported, string[] deleted, string[] moved, string[] movedFromAssetPaths)
		{
			string[] output = new string[0];

			ArrayUtils.AddRange(ref output, imported);
			ArrayUtils.AddRange(ref output, deleted);
			ArrayUtils.AddRange(ref output, moved);
			ArrayUtils.AddRange(ref output, movedFromAssetPaths);

			return output;
		}

		/// <summary>
		/// 	Determines if the asset at the path is a scene.
		/// </summary>
		/// <returns><c>true</c> if scene; otherwise, <c>false</c>.</returns>
		/// <param name="path">Path.</param>
		private static bool IsScene(string path)
		{
			return Path.GetExtension(path) == SCENE_EXT;
		}

		/// <summary>
		/// 	Determines if the asset at the path is the build settings file.
		/// </summary>
		/// <returns><c>true</c> if is build settings; otherwise, <c>false</c>.</returns>
		/// <param name="path">Path.</param>
		private static bool IsBuildSettings(string path)
		{
			return Path.GetFileName(path) == EDITOR_BUILD_SETTINGS_FILENAME;
		}

		/// <summary>
		/// 	Dumps the scene names to a text asset.
		/// </summary>
		public static void DumpSceneNames()
		{
			//string fileName = Path.GetFileNameWithoutExtension(pathToBuiltProject);
			//string buildDirectory = Path.Combine(Path.GetDirectoryName(pathToBuiltProject) ?? string.Empty, fileName);
			//string path = Path.Combine(buildDirectory, SceneUtils.SCENE_NAMES_FILE);

			string path = Path.Combine(Application.dataPath, SceneUtils.sceneNamesAssetPath);

			Directory.CreateDirectory(Path.GetDirectoryName(path));

			using (FileStream stream = File.Open(path, FileMode.Create, FileAccess.Write))
			{
				using (StreamWriter writer = new StreamWriter(stream))
				{
					EditorBuildSettingsScene[] scenes = EditorSceneUtils.GetEnabledScenes();

					for (int index = 0; index < scenes.Length; index++)
						writer.WriteLine(EditorSceneUtils.GetSceneName(scenes[index]));
				}
			}
		}

		#endregion
	}
}