﻿using AntonCoolpecker.Abstract.Props;
using AntonCoolpecker.Concrete;
using Hydra.HydraCommon.Abstract;
using System.Collections;
using UnityEngine;

/// <summary>
/// Transition system.
/// </summary>
public class TransitionSystem : HydraMonoBehaviour
{
	#region Variables

	[SerializeField] private Texture m_CutOut;
	[SerializeField] private float cogRotationSpeed = 90f;

	// queue of TransitionData structures
	// false - in transition
	// true - out transition
	private static Queue m_transitions;
	private TransitionData m_CurrentTransition;

	private bool m_Active = false;
	private bool m_transOut = false;
	private float m_zoomPoint = (Screen.width * 4f) / (Screen.width / 20);
	private float m_cogRotation = 0;
	private bool m_BlackScreen = true;

	private Rect m_cutOutRect;
	private Rect m_topRect;
	private Rect m_botRect;
	private Rect m_leftRect;
	private Rect m_rightRect;

	private AbstractTeleporter m_Teleporter;
	private CharacterController m_ToTeleport;

	public static TransitionSystem transitionSystem;
	public TransitionSystem ()
	{
		m_transitions = new Queue ();
	}

	#endregion

	#region Protected Methods/Functions

	/// <summary>
	/// Called when the object is instantiated.
	/// </summary>
	protected override void Awake ()
	{
		base.Awake ();

		if (transitionSystem == null) 
		{
			transitionSystem = this;
			DontDestroyOnLoad (gameObject);
		} 
		else if (transitionSystem != this) 
		{
			Debug.Log ("Additional TransitionSystem Detected");
			Destroy (this);
		}

		transitionIn ();
	}

	/// <summary>
	/// Called once every frame.
	/// </summary>
	protected override void Update ()
	{
		base.Update ();

		if ((!m_Active && m_transitions.Count == 0) || Time.unscaledDeltaTime > 0.066f) 
		{
			return; // early-out if we have nothing to do
		}

		if (!m_Active) 
		{
			try 
			{
				m_CurrentTransition = (TransitionData)m_transitions.Dequeue ();
				m_Active = true;
				m_BlackScreen = false;
			} 
			catch (System.Exception e) 
			{
				Debug.LogErrorFormat ("Transition queue is empty, yet we tried to pop it");
				Debug.LogErrorFormat ("Exception: {0}", e);
				return; // early-out condition doesn't catch transition that's done on level load from editor
			}
		}

		m_cogRotation += cogRotationSpeed * Time.unscaledDeltaTime;

		// Update member variable so it shows up on debug view
		m_zoomPoint = m_CurrentTransition.zoomPoint;

		if (m_cogRotation > 360)
		{
			m_cogRotation -= 360;
		}

		if (!m_CurrentTransition.transOut) 
		{
			m_CurrentTransition.zoomPoint += Time.unscaledDeltaTime * Screen.width / 16;

			if (m_CurrentTransition.zoomPoint * Screen.width / 20 > Screen.width * 5f) 
			{
				m_Active = false;
				return;
			}
		} 
		else 
		{
			if (m_CurrentTransition.zoomPoint == 0) 
			{
				//transition();
				m_Active = false;
				Time.timeScale = 1;

				if (m_CurrentTransition.teleporter != null && m_CurrentTransition.toTeleport != null)
					m_CurrentTransition.teleporter.Teleport (m_CurrentTransition.toTeleport);
				
				return;
			}

			m_CurrentTransition.zoomPoint -= Time.unscaledDeltaTime * Screen.width / 16;

			if (m_CurrentTransition.zoomPoint < 0) 
			{
				m_CurrentTransition.zoomPoint = 0f;
			}
		}

		m_cutOutRect = new Rect (Screen.width / 2 - Screen.width / 40 * m_CurrentTransition.zoomPoint, 
			Screen.height / 2 - Screen.width / 40 * m_CurrentTransition.zoomPoint, 
			Screen.width / 20 * m_CurrentTransition.zoomPoint, Screen.width / 20 * m_CurrentTransition.zoomPoint);

		m_topRect = new Rect (m_cutOutRect.x, 0, m_cutOutRect.width, m_cutOutRect.y + m_cutOutRect.height * 0.2f);
		m_botRect = new Rect (m_cutOutRect.x, m_cutOutRect.y + m_cutOutRect.height * 0.8f, m_cutOutRect.width, m_topRect.height);

		m_leftRect = new Rect (0, 0, m_cutOutRect.x + m_cutOutRect.width * 0.2f, Screen.height);
		m_rightRect = new Rect (m_cutOutRect.x + m_cutOutRect.width * 0.8f, 0, m_leftRect.width, Screen.height);
	}

	/// <summary>
	/// Called when GUI is drawn.
	/// </summary>
	protected void OnGUI ()
	{
		if (m_BlackScreen) 
		{
			GUI.color = Color.black;
			GUI.DrawTexture (new Rect (0, 0, Screen.width, Screen.height), Texture2D.whiteTexture);
		}

		Matrix4x4 baseGUIMatrix = GUI.matrix;

		GUIUtility.RotateAroundPivot (m_cogRotation, new Vector2 (Screen.width / 2f, Screen.height / 2f));

		GUI.DrawTexture (m_cutOutRect, m_CutOut);

		GUI.matrix = baseGUIMatrix;

		GUI.color = Color.black;

		GUI.DrawTexture (m_topRect, Texture2D.whiteTexture);
		GUI.DrawTexture (m_botRect, Texture2D.whiteTexture);

		GUI.DrawTexture (m_leftRect, Texture2D.whiteTexture);
		GUI.DrawTexture (m_rightRect, Texture2D.whiteTexture);

		GUI.color = Color.white;
	}

	#endregion

	#region Public Methods/Functions

	/// <summary>
	/// Transitions in. NOTE: Requires Teleporter to activate at end of transition.
	/// </summary>
	/// <param name="Teleporter"></param>
	public void transitionIn (AbstractTeleporter teleporter, CharacterController controller)
	{
		TransitionData nextTransition = new TransitionData ();
		nextTransition.transOut = true;
		nextTransition.toTeleport = controller;
		nextTransition.teleporter = teleporter;
		nextTransition.zoomPoint = (Screen.width * 4f) / (Screen.width / 20);
		m_transitions.Enqueue (nextTransition);
		GameObject.Find ("Main").GetComponent<Main> ().SetPause (false);
	}

	/// <summary>
	/// Transitions in.
	/// </summary>
	/// <param name="Teleporter"></param>
	public void transitionIn ()
	{
		TransitionData nextTransition = new TransitionData ();
		nextTransition.transOut = false;
		nextTransition.zoomPoint = 0;
		m_transitions.Enqueue (nextTransition);
		GameObject.Find ("Main").GetComponent<Main> ().SetPause (true);
	}

	/// <summary>
	/// Transitions out.
	/// </summary>
	public void transitionOut ()
	{
		TransitionData nextTransition = new TransitionData ();
		nextTransition.transOut = true;
		nextTransition.zoomPoint = (Screen.width * 4f) / (Screen.width / 20);
		m_transitions.Enqueue (nextTransition);
		GameObject.Find ("Main").GetComponent<Main> ().SetPause (false);
	}

	#endregion

	#region Internal Classes

	/// <summary>
	/// Transition data.
	/// </summary>
	private class TransitionData
	{
		public bool transOut;
		public AbstractTeleporter teleporter;
		public CharacterController toTeleport;
		public float zoomPoint;
	}

	#endregion
}