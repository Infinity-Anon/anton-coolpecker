﻿using AntonCoolpecker.Abstract.Prefabs;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Prefabs
{
	/// <summary>
	/// 	Transform asset instantiator.
	/// </summary>
	public class TransformAssetInstantiator : AbstractAssetInstantiator<Transform> {}
}