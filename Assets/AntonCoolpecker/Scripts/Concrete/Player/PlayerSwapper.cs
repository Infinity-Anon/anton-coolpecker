﻿using AntonCoolpecker.Concrete.Cameras;
using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using AntonCoolpecker.Concrete.Messaging;
using AntonCoolpecker.Abstract.Player;
using AntonCoolpecker.Concrete.Player.Anton;
using AntonCoolpecker.Concrete.Player.Coolpecker;
using AntonCoolpecker.Abstract.Player.States;
using AntonCoolpecker.Utils.Time;
using Hydra.HydraCommon.Abstract;
using Hydra.HydraCommon.Extensions;
using Hydra.HydraCommon.Utils;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Player
{
	/// <summary>
	/// 	Player swapper.
	/// </summary>
	public class PlayerSwapper : SingletonHydraMonoBehaviour<PlayerSwapper>
	{
		#region Variables

		[SerializeField] private AntonController m_AntonPrefab;
		[SerializeField] private CoolpeckerController m_CoolpeckerPrefab;

		private CameraController m_PlayerCamera;
		private AntonController m_AntonInstance;
		private CoolpeckerController m_CoolpeckerInstance;
        
		private static bool m_AntonActive = true;

		#endregion

		#region Properties

        /// <summary>
        /// Returns true if Anton is active.
        /// </summary>
        public static bool antonActive { get { return m_AntonActive; } }

		/// <summary>
		/// 	Gets or sets the player camera.
		/// </summary>
		/// <value>The player camera.</value>
		public CameraController playerCamera { get { return m_PlayerCamera; } set { m_PlayerCamera = value; } }

		/// <summary>
		/// 	Gets the anton prefab.
		/// </summary>
		/// <value>The anton prefab.</value>
		public AntonController antonPrefab { get { return m_AntonPrefab; } }

		/// <summary>
		/// 	Gets the coolpecker prefab.
		/// </summary>
		/// <value>The coolpecker prefab.</value>
		public CoolpeckerController coolpeckerPrefab { get { return m_CoolpeckerPrefab; } }

		#endregion

		#region Messages

		/// <summary>
		/// 	Called once every frame.
		/// </summary>
		protected override void Update()
		{
			base.Update();

			if (InputMapping.switchInput.GetButtonDown() && GetPlayerSwitch() && !GameTime.paused)
				Toggle();
		}

		/// <summary>
		/// 	Called when the object is destroyed.
		/// </summary>
		protected override void OnDestroy()
		{
			base.OnDestroy();

			m_AntonInstance = ObjectUtils.SafeDestroyGameObject(m_AntonInstance);
			m_CoolpeckerInstance = ObjectUtils.SafeDestroyGameObject(m_CoolpeckerInstance);
		}

		#endregion

		#region Methods

		/// <summary>
		/// 	Sets Anton active.
		/// </summary>
		/// <returns>Anton.</returns>
		public AntonController SetAntonActive()
		{
			return SetAntonActive(true) as AntonController;
		}

		/// <summary>
		/// 	Sets Coolpecker active.
		/// </summary>
		/// <returns>Coolpecker.</returns>
		public CoolpeckerController SetCoolpeckerActive()
		{
			return SetCoolpeckerActive(true) as CoolpeckerController;
		}

		/// <summary>
		/// 	Sets Anton active.
		/// </summary>
		/// <returns>The new active player.</returns>
		/// <param name="active">If set to <c>true</c> active.</param>
		public AbstractPlayerController SetAntonActive(bool active)
		{
			AbstractPlayerController old = GetActivePlayer();

			if (active == m_AntonActive)
				return old;

			m_AntonActive = active;
			AbstractPlayerController current = GetActivePlayer();

			PostToggle(old, current);

			return current;
		}

		/// <summary>
		/// 	Sets Coolpecker active.
		/// </summary>
		/// <returns>The new active player.</returns>
		/// <param name="active">If set to <c>true</c> active.</param>
		public AbstractPlayerController SetCoolpeckerActive(bool active)
		{
			return SetAntonActive(!active);
		}

		/// <summary>
		/// 	Toggles the currently active player.
		/// </summary>
		/// <returns>The new active player.</returns>
		public AbstractPlayerController Toggle()
		{
			return SetAntonActive(!m_AntonActive);
		}

		/// <summary>
		/// 	Gets the active player.
		/// </summary>
		/// <returns>The active player.</returns>
		public AbstractPlayerController GetActivePlayer()
		{
			if (m_AntonActive)
				return GetAntonInstance();

			return GetCoolpeckerInstance();
		}

		/// <summary>
		/// Eagerly instantiate Anton and Coolpecker. Used primarily by the DebugGUI.
		/// We have to leave the ontherwise inactive character active until the DebugGUI is finished with it.
		/// </summary>
		public void PreWarm()
		{
			GetAntonInstance();
			GetCoolpeckerInstance();
		}

		/// <summary>
		/// De-activate the inactive player now that the Debug GUI is done with it.
		/// </summary>
		public void PreCool()
		{
			var anton = GetAntonInstance();
			var coolpecker = GetCoolpeckerInstance();

			if (m_AntonActive)
				coolpecker.gameObject.SetActive(false);
			else
				anton.gameObject.SetActive(false);
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// 	Gets the anton instance.
		/// </summary>
		/// <returns>The anton instance.</returns>
		private AntonController GetAntonInstance()
		{
			if (m_AntonInstance == null)
			{
				m_AntonInstance = Instantiate(m_AntonPrefab);
				PostInstantiate(m_AntonInstance);
			}

			return m_AntonInstance;
		}

		/// <summary>
		/// 	Gets the coolpecker instance.
		/// </summary>
		/// <returns>The coolpecker instance.</returns>
		private CoolpeckerController GetCoolpeckerInstance()
		{
			if (m_CoolpeckerInstance == null)
			{
				m_CoolpeckerInstance = Instantiate(m_CoolpeckerPrefab);
				PostInstantiate(m_CoolpeckerInstance);
			}

			return m_CoolpeckerInstance;
		}

		/// <summary>
		/// 	Performs some operations on the player after being instantiated.
		/// </summary>
		/// <param name="player">Player.</param>
		private void PostInstantiate(AbstractPlayerController player)
		{
			player.transform.parent = transform;
			player.playerCamera = m_PlayerCamera;

			if (m_PlayerCamera.target == null)
				m_PlayerCamera.target = player;
		}

		/// <summary>
		/// 	Performs some operations after toggling the active players.
		/// </summary>
		/// <param name="old">Old.</param>
		/// <param name="current">Current.</param>
		private void PostToggle(AbstractPlayerController old, AbstractPlayerController current)
		{
			current.transform.Copy(old);

			current.isInWater = old.isInWater;
			current.airTracker.Set(old.airTracker);
			current.characterLocomotor.velocity = old.characterLocomotor.velocity;

			m_PlayerCamera.target = current;

			old.gameObject.SetActive(false);
			current.gameObject.SetActive(true);

			PlayerMessages.BroadcastOnSwapEnter(current);
		}

		/// <summary>
		/// 	Called when the player wants to switch character
		/// </summary>
		/// <returns>The character switch availability.</returns>
		private bool GetPlayerSwitch()
		{
			AbstractPlayerController currentPlayer = GetActivePlayer();
			AbstractPlayerState currentState = currentPlayer.stateMachine.activeState;

			if (currentState.canSwitchCharacters)
				return true;

			return false;
		}

		#endregion
	}
}