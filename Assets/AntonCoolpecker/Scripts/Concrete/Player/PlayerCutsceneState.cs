﻿using AntonCoolpecker.Abstract.Player.States;
using System.Collections;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Player
{
    //TODO: Prevent all additional input from having an effect and make the player immune/invisible to enemies during this state.

	/// <summary>
	/// Player cutscene state - The PC will be in this state while a cutscene plays in-game.
	/// </summary>
    public class PlayerCutsceneState : AbstractPlayerState
    {
		#region Variables

        [SerializeField] protected AbstractStandingState NextState; //Next state to transition to

        bool m_Transition; //Whether the player should transition to next state or not

		#endregion

		#region Properties

		/// <summary>
		/// Sets a value indicating whether this <see cref="AntonCoolpecker.Concrete.Player.PlayerCutsceneState"/> will transition to a specific state.
		/// </summary>
		/// <value><c>true</c> if transitioning to a specific state; otherwise, <c>false</c>.</value>
        public bool Transition { set { m_Transition = value; } }

		#endregion

		#region Override Functions

		/// <summary>
		/// Called when the state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
        public override void OnEnter(MonoBehaviour parent)
        {
            base.OnEnter(parent);
            GetPlayerController(parent).characterLocomotor.velocity = Vector3.zero;
            m_Transition = false;
        }

		/// <summary>
		/// Returns a state for transition. Return self if no transition.
		/// </summary>
		/// <returns>The next state.</returns>
		/// <param name="parent">Parent.</param>
        public override AbstractPlayerState GetNextState(MonoBehaviour parent)
        {
            if (m_Transition)
                return NextState;
            else
                return base.GetNextState(parent);
        }

		#endregion
    }
}