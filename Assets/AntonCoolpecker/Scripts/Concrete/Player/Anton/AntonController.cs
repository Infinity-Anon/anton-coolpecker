﻿using AntonCoolpecker.Abstract.Player;
using AntonCoolpecker.Concrete.Listeners.Player.Anton;
using AntonCoolpecker.Utils;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Player.Anton
{
	/// <summary>
	/// 	AntonController is the controller for the playable Anton model.
	/// </summary>
	[RequireComponent(typeof(AntonAudioListener))]
	[RequireComponent(typeof(AntonAnimationListener))]
	public class AntonController : AbstractPlayerController
    {
		#region Variables

        [SerializeField] private GameObject m_BillboardPrefab;
        [SerializeField] [Tweakable("Swing")] private bool m_OnlyGrappleForward;
        [SerializeField] [Tweakable("Swing")] private float m_GrappleForwardMaxAngle;

        private Billboard m_Billboard;

		#endregion

		#region Properties

        public bool m_GrappleForward { get { return m_OnlyGrappleForward; } }
        public float m_GrappleForwardAngleMax { get { return m_GrappleForwardMaxAngle; } }

		#endregion

		#region Functions

		/// <summary>
		/// Called when the object is instantiated.
		/// </summary>
        protected override void Awake()
        {
            base.Awake();

            m_Billboard = Instantiate(m_BillboardPrefab).GetComponent<Billboard>();
        }

		/// <summary>
		/// Called once every frame.
		/// </summary>
        protected override void Update()
        {
            base.Update();

            markApplicableGrapplePoint();
        }

		/// <summary>
		/// Visually marks any applicable grapple point closest to the player.
		/// </summary>
        protected void markApplicableGrapplePoint()
        {
			States.MidairAntonState midairstate = stateMachine.initialState as States.MidairAntonState;

            Scene.GrapplePoint toMark = midairstate.m_SwingingState.FindGrapplePoint(this);

            if (toMark != null)
            {
                m_Billboard.gameObject.SetActive(true);
                m_Billboard.transform.position = toMark.transform.position;
            }
            else
            {
                m_Billboard.gameObject.SetActive(false);
            }
        }

		#endregion
    }
}