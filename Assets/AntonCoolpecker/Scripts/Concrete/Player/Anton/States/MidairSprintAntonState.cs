﻿using UnityEngine;
using System.Collections;
using AntonCoolpecker.Abstract.Player.States;
using AntonCoolpecker.Abstract.Player;
using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
namespace AntonCoolpecker.Concrete.Player.Anton.States
{
    /// <summary>
    /// Anton midair sprinting state
    /// </summary>
    public class MidairSprintAntonState : AbstractMidairSprintState
    {
        [SerializeField]
        AbstractBounceJumpState AntonBounceState;

        public override AbstractPlayerState GetNextState(MonoBehaviour parent)
        {
            AbstractPlayerController player = GetPlayerController(parent);
            if (InputMapping.groundPoundInput.GetButtonDown()&&player.airTracker.canBounce)
            {
                return AntonBounceState;
            }

            return base.GetNextState(parent);
        }
        public override void HandleAirJump(MonoBehaviour parent)
        {
            //Airjump disabled for Anton
            AbstractPlayerController player = GetPlayerController(parent);

            if (player.airTracker.canAirJump)
                player.airTracker.canAirJump = false;
        }
    }
}
