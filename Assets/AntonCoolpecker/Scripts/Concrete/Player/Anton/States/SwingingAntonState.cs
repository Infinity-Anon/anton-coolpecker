﻿using AntonCoolpecker.Abstract.Player;
using AntonCoolpecker.Abstract.Player.States;
using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using AntonCoolpecker.Concrete.Scene;
using AntonCoolpecker.Utils;
using AntonCoolpecker.Utils.Time;
using Hydra.HydraCommon.Extensions;
using Hydra.HydraCommon.Utils;
using System.Collections.Generic;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Player.Anton.States
{
	/// <summary>
	/// 	Swinging state for Anton.
	/// </summary>
	public class SwingingAntonState : AbstractPlayerState
	{
		[SerializeField] private MidairAntonState m_MidairAntonState;
		[SerializeField] private AbstractSwimmingState m_SwimmingState;
		[SerializeField] private StandingAntonState m_StandingState;
        
		[SerializeField] [Tweakable("Swing")] private float m_GrappleDistance; //Distance at which grapple points can be targeted for a grapple.
		[SerializeField] [Tweakable("Swing")] private float m_TongueLength; //Max distance of the tongue.
		[SerializeField] [Tweakable("Swing")] private float m_SwingInputForce; //Multiplier of swing force based on movement input.
		[SerializeField] [Tweakable("Swing")] private float m_SwingLaunchMultiplier; //Multiplier for force upon exiting the swing mode.
		[SerializeField] [Tweakable("Swing")] private float m_RotateSpeedMultiplier; //Multiplier applied to the rotation speed of the character.
		[SerializeField] [Tweakable("Swing")] private float m_LeftRightResistance; //Speed reduction for left/right movement if AutoFollowRotation is false.
		[SerializeField] [Tweakable("Swing")] private float m_LeftRightInputDeadZone; //Deadzone for the left/right input stick.
		[SerializeField] [Tweakable("Swing")] private float m_SwingGravity; //False gravity given to the character as well as speed at which the character swings.
        
		[SerializeField] [Tweakable("Swing")] private bool m_LerpToTongueLength; //If true, tongue will lerp from current distance to the tongue length distance.
		[SerializeField] [Tweakable("Swing")] private float m_TongueLerpSpeed; //The speed at which the tongue lerps each second.
		[SerializeField] [Tweakable("Swing")] private float m_LerpJumpLength; //Distance at which the system stops lerping and just sets the bone distance equal to the tongue length.

		[SerializeField] [Tweakable("Swing")] private bool m_UseAngleLimit; //If true, will put resisting force if the player goes past the angle limit.
		[SerializeField] [Tweakable("Swing")] private float m_AngleLimitMax; //Max angle (Where 0/360 is up)
		[SerializeField] [Tweakable("Swing")] private float m_AngleLimitMin; //Minimum angle (Where 0/360 is up)
        [SerializeField] [Tweakable("Swing")] private float m_AngleLimitForceMultiplier; //Amount of force applied downwards if player exceeds the angle limit.

		[SerializeField] [Tweakable("Swing")] private bool m_UseSpeedLimt; //While true, will limit the fun of the player.
		[SerializeField] [Tweakable("Swing")] private float m_SpeedLimit; //The speed at which the player's fun is limited.
        
		[SerializeField] [Tweakable("Swing")] private bool m_UseAutoFollowRotation; //Sets swing to only exist on a forwards to backwards plane.
		[SerializeField] [Tweakable("Swing")] private float m_AutoFollowRotationSpeed; //The speed at which the velocity vector rotates towards the player's facing.
		[SerializeField] [Tweakable("Swing")] private float m_AutoFollowRotationVectorMax; //The max angle the velocity vector is allowed to lag behind the player's facing.
		[SerializeField] [Tweakable("Swing")] private bool m_UseAutoFollowRotationMirror; //Sets swing to rotate ahead of the camera instead of trying to catch up.
        
		[SerializeField] [Tweakable("Swing")] private bool m_FocusOnNextTarget; //While true, will only put target recticle on next target.
		[SerializeField] [Tweakable("Swing")] private float m_PreviousSwingCooldown; //Time a grapple point will not be selectable after being used.
        
		[SerializeField] [Tweakable("Swing")] private bool m_RotateIfOffset; //While true, will have Anton roll while swinging.
		[SerializeField] [Tweakable("Swing")] private bool m_RotateIfOffsetMirror; //While true, will have Anton roll while swinging, but in the opposite expected direction.
		//[SerializeField] [Tweakable("Swing")] private float m_RotateMaxPitch; //The max X rotation Anton will rotate.
		[SerializeField] [Tweakable("Swing")] private float m_RotateMaxRoll; //The max Z rotation Anton will rotate.

		[SerializeField] private string m_TongueJointName;

		private GrapplePoint m_GrapplePoint;
		private Vector3 m_TongueLocalPosition;
		private Transform m_CachedTongueJoint;
		private List<GrapplePoint> m_TempGrapplePoints;
		private GameObject m_Tongue;
		private ConfigurableJoint m_TongueJoint;
		private SoftJointLimit m_TongueLimit = new SoftJointLimit();
        private Quaternion m_AutoFollowFacing;
        private bool m_AutoFollowMoved;
        private AnimatorGameTimeSubscriber m_FakeBody;

        #region Properties

        /// <summary>
        /// 	Gets the length of the tongue.
        /// </summary>
        /// <value>The length of the tongue.</value>
        public float tongueLength { get { return m_TongueLength; } }

		/// <summary>
		/// 	Gets or sets the grapple point.
		/// </summary>
		/// <value>The grapple point.</value>
		public GrapplePoint grapplePoint { get { return m_GrapplePoint; } set { m_GrapplePoint = value; } }

        #endregion

        #region Override Methods

        /// <summary>
        /// 	Called when the state becomes active.
        /// </summary>
        /// <param name="parent">Parent.</param>
        public override void OnEnter(MonoBehaviour parent)
		{
			base.OnEnter(parent);

			AbstractPlayerController player = GetPlayerController(parent);

			// Keep a copy of the original tongue position.
			m_Tongue = new GameObject();
			m_TongueLimit = new SoftJointLimit();

            m_Tongue.name = m_TongueJointName;
            m_Tongue.transform.position = m_GrapplePoint.transform.position;

            m_Tongue.AddComponent<Rigidbody>();
			m_Tongue.AddComponent<ConfigurableJoint>();
			Rigidbody Weight = m_Tongue.GetComponent<Rigidbody>();
			m_TongueJoint = m_Tongue.GetComponent<ConfigurableJoint>();

            m_TongueLocalPosition = GetTongueJoint(parent).localPosition;
			m_TongueJoint.anchor = m_TongueJoint.transform.InverseTransformPoint(m_GrapplePoint.transform.position);

			//Tongue limit
            if (m_LerpToTongueLength)
                m_TongueLimit.limit = Vector3.Distance(m_GrapplePoint.transform.position, player.transform.position);
            else
                m_TongueLimit.limit = m_TongueLength;

            m_TongueJoint.linearLimit = m_TongueLimit;
			m_TongueJoint.xMotion = ConfigurableJointMotion.Limited;
			m_TongueJoint.yMotion = ConfigurableJointMotion.Limited;
			m_TongueJoint.zMotion = ConfigurableJointMotion.Limited;

            Weight.velocity = player.characterLocomotor.velocity;
			player.characterLocomotor.useGravity = false;
            Weight.useGravity = false;

            player.transform.LookAt(new Vector3(m_GrapplePoint.transform.position.x, player.transform.position.y, m_GrapplePoint.transform.position.z));
            m_Tongue.transform.position = player.transform.position;

            m_AutoFollowFacing = player.transform.rotation;
            grapplePoint.GrappleTimer.maxTime = m_PreviousSwingCooldown;

            m_FakeBody = player.GetComponentInChildren<AnimatorGameTimeSubscriber>();

            if (cameraMode != Cameras.CameraController.Mode.Swing)
                throw new System.Exception("Non-Swing Camera implemented into SwingingAntonState!");
        }

		/// <summary>
		/// 	Called before another state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnExit(MonoBehaviour parent)
		{
			base.OnExit(parent);

			AbstractPlayerController player = GetPlayerController(parent);

			// Reset the tongue joint.
			GetTongueJoint(parent).localPosition = m_TongueLocalPosition;
			player.characterLocomotor.useGravity = true;
            Rigidbody Weight = m_Tongue.GetComponent<Rigidbody>();
            player.characterLocomotor.velocity = Weight.velocity*m_SwingLaunchMultiplier;
            Destroy(m_Tongue);
            grapplePoint.GrappleTimer.Reset();

            m_FakeBody.transform.rotation = Quaternion.Euler(0f, player.transform.rotation.eulerAngles.y, 0f);
        }

		/// <summary>
        /// Called every FixedUpdate.
        /// </summary>
        /// <param name="player"></param>
		protected override void StateUpdate(AbstractPlayerController player)
		{
			//Prevent the system from targeting the current grapplepoint.
            if (m_FocusOnNextTarget)
                grapplePoint.GrappleTimer.Reset();

            //Lerp length to Tongue Length
            if (m_LerpToTongueLength)
            {
                if (m_TongueLimit.limit != m_TongueLength)
                {
                    if (HydraMathUtils.Abs(m_TongueLimit.limit - m_TongueLength) > m_LerpJumpLength)
                    {
                        m_TongueLimit.limit = Mathf.Lerp(m_TongueLimit.limit, m_TongueLength, GameTime.fixedDeltaTime * m_TongueLerpSpeed);
                        m_TongueJoint.linearLimit = m_TongueLimit;
                    }
                    else
                    {
                        m_TongueLimit.limit = m_TongueLength;
                        m_TongueJoint.linearLimit = m_TongueLimit;
                    }
                }
            }

            // Move anton towards the tongue length from the grapple point
            player.transform.position = m_Tongue.transform.position;
			Rigidbody Weight = m_Tongue.GetComponent<Rigidbody>();
            float inputForce = InputMapping.verticalInput.GetAxisRaw();
            Weight.velocity += player.transform.forward * m_SwingInputForce * GameTime.fixedDeltaTime * inputForce;

            if (!m_UseAutoFollowRotation)
            {
                //Reduce Left/Right velocity
                Vector3 perp = Vector3.Cross(player.transform.forward, Weight.velocity);
                float dir = Vector3.Dot(perp, Vector3.up);

                if (dir > 0f)
                {
                    Weight.velocity -= player.transform.right * m_LeftRightResistance * GameTime.fixedDeltaTime;
                }
                else if (dir < 0f)
                {
                    Weight.velocity += player.transform.right * m_LeftRightResistance * GameTime.fixedDeltaTime;
                }

                //Increase velocity towards left/right center of grapple point.
                perp = Vector3.Cross(player.transform.forward, m_GrapplePoint.transform.position - player.transform.position);
                dir = Vector3.Dot(perp, Vector3.up);

                if (dir > 0f)
                {
                    Weight.velocity += player.transform.right * m_LeftRightResistance * GameTime.fixedDeltaTime;
                }
                else if (dir < 0f)
                {
                    Weight.velocity -= player.transform.right * m_LeftRightResistance * GameTime.fixedDeltaTime;
                }
            }

            //Apply Swing Gravity/Speed System
            Weight.velocity += new Vector3(0, -m_SwingGravity, 0) * GameTime.fixedDeltaTime;

            if (m_UseAngleLimit)
            {
                Vector3 tongueFacing = Weight.position - m_GrapplePoint.transform.position;
                float angle = Vector3.Angle(tongueFacing, Vector3.up);
                
                float forwardAngle = Vector3.Angle(tongueFacing, player.transform.forward);

                if (forwardAngle > 90f)
                    angle = 360 - angle;

                if (angle > m_AngleLimitMax && Weight.velocity.y > 0)
                    Weight.velocity += new Vector3(0, -m_SwingGravity, 0) * GameTime.fixedDeltaTime * m_AngleLimitForceMultiplier * (angle - m_AngleLimitMax);
                else if (angle < m_AngleLimitMin && Weight.velocity.y > 0)
                    Weight.velocity += new Vector3(0, -m_SwingGravity, 0) * GameTime.fixedDeltaTime * m_AngleLimitForceMultiplier * (m_AngleLimitMin - angle);
            }

            //Cap Speed
            if (m_UseSpeedLimt && Weight.velocity.sqrMagnitude > m_SpeedLimit * m_SpeedLimit)
                Weight.velocity = Weight.velocity.normalized * m_SpeedLimit;

            //Rotate Model
            if (m_RotateIfOffset)
            {
                //Calculate roll
                Quaternion fakeRotation;
                if (m_RotateIfOffsetMirror)
                {
                    if (Weight.transform.position.y < grapplePoint.transform.position.y)
                        fakeRotation = Quaternion.LookRotation(player.transform.forward, //forward
                            new Vector3(Weight.transform.position.x - grapplePoint.transform.position.x, //up x
                            grapplePoint.transform.position.y - Weight.transform.position.y, //up y
                            Weight.transform.position.z - grapplePoint.transform.position.z)); //up z
                    else
                        fakeRotation = Quaternion.LookRotation(player.transform.forward,
                            new Vector3(Weight.transform.position.x - grapplePoint.transform.position.x, 0, Weight.transform.position.z - grapplePoint.transform.position.z));
                }
                else
                {
                    if (Weight.transform.position.y < grapplePoint.transform.position.y)
                        fakeRotation = Quaternion.LookRotation(player.transform.forward, grapplePoint.transform.position - Weight.transform.position);
                    else
                        fakeRotation = Quaternion.LookRotation(player.transform.forward,
                            new Vector3(grapplePoint.transform.position.x - Weight.transform.position.x, 0, grapplePoint.transform.position.z - Weight.transform.position.z));
                }

                //Calculate pitch
                /*Vector3 tongueFacing = Weight.position - m_GrapplePoint.transform.position;
                float angle = Vector3.Angle(tongueFacing, Vector3.up);
                float forwardAngle = Vector3.Angle(tongueFacing, player.transform.forward);
                if (forwardAngle > 90f)
                    angle = 360 - angle;*/

                //Add pitch
                //fakeRotation *= Quaternion.Euler(angle - 180f, 0f, 0f);

                //Max Roll Limit
                if (fakeRotation.eulerAngles.z < 180f && fakeRotation.eulerAngles.z > m_RotateMaxRoll)
                    fakeRotation.eulerAngles = new Vector3(fakeRotation.eulerAngles.x, fakeRotation.eulerAngles.y, m_RotateMaxRoll);
                else if (fakeRotation.eulerAngles.z >= 180f && fakeRotation.eulerAngles.z < 360f - m_RotateMaxRoll)
                    fakeRotation.eulerAngles = new Vector3(fakeRotation.eulerAngles.x, fakeRotation.eulerAngles.y, -m_RotateMaxRoll);

                //Apply calculations
                m_FakeBody.transform.rotation = fakeRotation;
            }
        }

	    /// <summary>
	    /// 	Called when the parent late updates.
	    /// </summary>
	    /// <param name="parent">Parent.</param>
	    public override void OnLateUpdate(MonoBehaviour parent)
		{
			base.OnLateUpdate(parent);

			// Move the tongue joint to the anchor.
			Transform tongueModelJoint = GetTongueJoint(parent);

			// Anton's model is rigged to include an extendable tongue
			// This bit of code visually attaches Anton's tongue to the anchor being grabbed
			tongueModelJoint.position = m_GrapplePoint.transform.position;
			tongueModelJoint.LookAt(tongueModelJoint.parent);
		}

		/// <summary>
		/// 	Returns a state for transition. Return self if no transition.
		/// </summary>
		/// <returns>The next state.</returns>
		/// <param name="parent">Parent.</param>
		public override AbstractPlayerState GetNextState(MonoBehaviour parent)
		{
			AbstractPlayerController player = GetPlayerController(parent);

            /*if (InputMapping.jumpInput.GetButtonDown())
                m_UseAutoFollowRotationMirror = !m_UseAutoFollowRotationMirror;*/

            if (!InputMapping.swingingInput.GetButton())
				return m_MidairAntonState;

			if (player.characterLocomotor.isGrounded)
				return m_StandingState;

			if (player.isInWater)
				return m_SwimmingState;

			return base.GetNextState(parent);
		}

        /// <summary>
        /// Rotates towards movement vector.
        /// </summary>
        /// <param name="player"></param>
        /// <param name="movement"></param>
        protected override void RotateTowardsMovementVector(AbstractPlayerController player, Vector3 movement)
        {
            Rigidbody Weight = m_Tongue.GetComponent<Rigidbody>();
            float speed = 0;

			//If true, it will change all x and z velocity into forward velocity after turning.
            if (m_UseAutoFollowRotation)
            {
                float angle = Vector3.Angle(Weight.velocity, player.transform.forward);

                Vector3 flatVelocty = Weight.velocity;
                flatVelocty.y = 0;
                float speedMod = 0;

                if (angle > 90f)
                {
                    speedMod = -1;
                }
                else
                {
                    speedMod = 1;
                }

                speed = flatVelocty.magnitude * speedMod;
            }

            Quaternion lookRotation = Quaternion.LookRotation(player.transform.forward);
            Quaternion targetRotation = new Quaternion();
            float delta = player.rotateSpeed * GameTime.fixedDeltaTime * m_RotateSpeedMultiplier * HydraMathUtils.Abs(InputMapping.horizontalInput.GetAxisRaw());

            if (movement != Vector3.zero)
            {
                targetRotation = Quaternion.LookRotation(movement);

                if (m_UseAutoFollowRotationMirror)
                    m_AutoFollowMoved = true;
				
                player.transform.rotation = Quaternion.RotateTowards(lookRotation, targetRotation, delta);
            }

			//If true, the character will orbit around the grapple point when attempting to turn
            if (m_UseAutoFollowRotation)
            {
                Quaternion prevFacing = m_AutoFollowFacing;

                if (m_AutoFollowMoved)
                {
                    m_AutoFollowFacing = Quaternion.RotateTowards(m_AutoFollowFacing, targetRotation, delta + m_AutoFollowRotationSpeed * GameTime.fixedDeltaTime);
                    m_AutoFollowMoved = false;
                }
                else
                    m_AutoFollowFacing = Quaternion.RotateTowards(m_AutoFollowFacing, player.transform.rotation, m_AutoFollowRotationSpeed * GameTime.fixedDeltaTime);

                float angleDiff = Quaternion.Angle(m_AutoFollowFacing, player.transform.rotation);

                if (angleDiff > m_AutoFollowRotationVectorMax)
                {
                    m_AutoFollowFacing = Quaternion.RotateTowards(m_AutoFollowFacing, player.transform.rotation, angleDiff - m_AutoFollowRotationVectorMax);
                }

                //Get a "forward vector" for each rotation
                Vector3 forwardA = prevFacing * Vector3.forward;
                Vector3 forwardB = m_AutoFollowFacing * Vector3.forward;

                //Get a numeric angle for each vector, on the X-Z plane (relative to world forward)
                float angleA = Mathf.Atan2(forwardA.x, forwardA.z) * Mathf.Rad2Deg;
                float angleB = Mathf.Atan2(forwardB.x, forwardB.z) * Mathf.Rad2Deg;

                //Get the signed difference in these angles
                angleDiff = Mathf.DeltaAngle(angleA, angleB);

                Vector3 FollowFacing = m_AutoFollowFacing * Vector3.forward;
                Weight.transform.RotateAround(m_GrapplePoint.transform.position, m_GrapplePoint.transform.up, -angleDiff);
                Weight.velocity = new Vector3(FollowFacing.x * speed, Weight.velocity.y, FollowFacing.z * speed);
            }
        }

        /// <summary>
        /// Calls RotateTowardsMovementVector and prevents the player from rotating if the backwards direction is held.
        /// </summary>
        /// <param name="parent"></param>
        protected override void HandleMovement(MonoBehaviour parent)
        {
            AbstractPlayerController player = GetPlayerController(parent);

            Vector3 desiredMovement = GetMovementVector(parent);
            RotateTowardsMovementVector(player, desiredMovement);

            Vector3 deltaV = ResolveVelocityDelta(player, desiredMovement, player.characterLocomotor.velocity);
            player.characterLocomotor.velocity = Vector3.zero;
        }

        /// <summary>
		/// 	Transforms the input vector from camera space into world space.
		/// </summary>
		/// <returns>The camera oriented input.</returns>
		protected override Vector3 GetCameraOrientedInput(MonoBehaviour parent)
        {
            AbstractPlayerController player = GetPlayerController(parent);

            Vector3 input = GetInputVector(player);
            input = Vector3.ClampMagnitude(input, 1.0f);

            input.z = 0;

            Vector3 cameraForward = player.playerCamera.transform.forward;
            cameraForward.y = 0.0f;

            return Quaternion.LookRotation(cameraForward) * input;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// 	Finds the most ideal grapple point Anton can swing from.
        /// </summary>
        /// <returns>The grapple point.</returns>
        /// <param name="parent">Parent.</param>
        public GrapplePoint FindGrapplePoint(MonoBehaviour parent)
		{
			AntonController anton = parent as AntonController;
			Vector3 origin = GetHeadPosition(parent);

			return GrapplePoint.FindIdeal(anton, origin, m_GrappleDistance);
		}

		/// <summary>
		/// 	Gets the world position of Anton's head.
		/// </summary>
		/// <returns>The head position.</returns>
		/// <param name="parent">Parent.</param>
		public Vector3 GetHeadPosition(MonoBehaviour parent)
		{
			return GetTongueJoint(parent).parent.position;
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// 	Gets the tongue joint.
		/// </summary>
		/// <returns>The tongue joint.</returns>
		/// <param name="parent">Parent.</param>
		private Transform GetTongueJoint(MonoBehaviour parent)
		{
			if (m_CachedTongueJoint == null)
				m_CachedTongueJoint = parent.transform.FindRecursive(m_TongueJointName);

			return m_CachedTongueJoint;
		}

		#endregion
	}
}