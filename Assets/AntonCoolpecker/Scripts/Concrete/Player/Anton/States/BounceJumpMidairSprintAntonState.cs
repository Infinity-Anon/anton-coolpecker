﻿using AntonCoolpecker.Abstract.Player.States;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Player.Anton.States
{
	/// <summary>
	/// Bounce jump midair sprint Anton state - Initally reached when Anton is midair after 
	/// first sprinting, then jumping and then doing a bounce jump up into the air
	/// </summary>
    public class BounceJumpMidairSprintAntonState : AbstractBounceMidair {}
}