﻿using AntonCoolpecker.Abstract.Player;
using AntonCoolpecker.Abstract.Player.States;
using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using AntonCoolpecker.Concrete.Player.Anton.States.Digging;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Player.Anton.States
{
    /// <summary>
    /// 	MidairAntonState is used when Anton is off the ground.
    /// </summary>
    public class MidairAntonState : AbstractMidairState
    {
		#region Variables

        [SerializeField] public SwingingAntonState m_SwingingState;
        [SerializeField] private BounceJumpAntonState m_BounceJump;
		[SerializeField] private AirDigAntonState m_AirDigState;
		[SerializeField] protected AirAttackAntonState m_AirAttackState;

		#endregion

		#region Override Functions/Methods

		/// <summary>
		/// 	Returns a state for transition. Return self if no transition.
		/// </summary>
		/// <returns>The next state.</returns>
		/// <param name="parent">Parent.</param>
		public override AbstractPlayerState GetNextState(MonoBehaviour parent)
		{
			AbstractPlayerController controller = GetPlayerController(parent);

            if (InputMapping.swipeAttackInput.GetButtonDown() && controller.airTracker.canSpinHover)
            {
                controller.airTracker.canSpinHover = false;
                m_AirAttackState.ResetCombo();
                return m_AirAttackState;
            }
				
            if (InputMapping.groundPoundInput.GetButtonDown()&&controller.airTracker.canBounce)
            {
                return m_BounceJump;
            }

            if (controller.progress.unlockedDig && InputMapping.digInput.GetButtonDown() && m_AirDigState.CanAirDig(parent) && !PreDigAntonState.willNotDigAgain)
                return m_AirDigState;
			
            if (controller.progress.unlockedTongue && InputMapping.swingingInput.GetButtonDown())
			{
				m_SwingingState.grapplePoint = m_SwingingState.FindGrapplePoint(parent);

				if (m_SwingingState.grapplePoint != null)
					return m_SwingingState;
			}

			return base.GetNextState(parent);
		}

		/// <summary>
		/// Called before another state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnExit(MonoBehaviour parent)
		{
			if (PreDigAntonState.willNotDigAgain)
				PreDigAntonState.willNotDigAgain = false;

			base.OnExit(parent);
		}

		/// <summary>
		/// Handles the air jump.
		/// </summary>
		/// <param name="parent">Parent.</param>
        public override void HandleAirJump(MonoBehaviour parent)
        {
            //Airjump disabled for Anton
            AbstractPlayerController player = GetPlayerController(parent);

            if (player.airTracker.canAirJump)
                player.airTracker.canAirJump = false;
        }

		#endregion
    }
}