﻿using AntonCoolpecker.Abstract.Player;
using AntonCoolpecker.Abstract.Player.States;
using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using System.Collections;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Player.Anton.States
{
	/// <summary>
	/// Bounce jump land Anton state - Reached when Anton either initiates a bounce jump 
	/// by landing/air digging(previously doing a sprint before going mid-air)
	/// or lands after doing a sprint bounce jump.
	/// </summary>
    public class BounceJumpLandAntonSprintState : AbstractBounceLandState
    {
		#region Override Functions

		/// <summary>
		/// Called when the parent updates.
		/// </summary>
		/// <param name="parent">Parent.</param>
        public override void OnUpdate(MonoBehaviour parent)
        {
            Vector3 input = GetCameraOrientedInput(GetPlayerController(parent));
            
            if (input.magnitude != 0)
                storedInput = input *storedInput.magnitude;
			
            base.OnUpdate(parent);
        }

		#endregion
    }
}