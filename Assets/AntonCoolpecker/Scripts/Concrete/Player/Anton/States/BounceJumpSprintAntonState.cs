﻿using AntonCoolpecker.Abstract.Player;
using AntonCoolpecker.Abstract.Player.States;
using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using AntonCoolpecker.Concrete.Player.Anton.States.Digging;
using AntonCoolpecker.Concrete.Utils;
using AntonCoolpecker.Utils;
using System.Collections;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Player.Anton.States
{
	//TODO: ABSTRACT BOUNCEJUMPSPRINTANTONSTATE AND BOUNCEJUMPANTONSTATE INTO ONE STATE SCRIPT

	/// <summary>
	/// Bounce jump sprint anton state - When Anton does a bounce jump after sprinting and going midair.
	/// </summary>
    public class BounceJumpSprintAntonState : AbstractBounceJumpState
    {
		#region Variables

		[SerializeField] private AirDigAntonState m_AirDigState;
		[SerializeField] private PreDigAntonState m_PreDigState;

		[Tweakable("BounceJump")] [SerializeField] private float m_RequiredHoldTime;
		[Tweakable("BounceJump")] [SerializeField] private bool m_TrueCanGoToDigFalseCannotGoToDig;
		[Tweakable("BounceJump")] [SerializeField] private bool m_DirectlyBounceOnNoneDiggable;

		private Timer m_Timer;

		#endregion

		#region Override Functions/Methods

		/// <summary>
		/// Called when the state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnEnter(MonoBehaviour parent)
		{
			base.OnEnter(parent);

			m_Timer = new Timer();
			m_Timer.maxTime = m_RequiredHoldTime;

			if (InputMapping.digInput.GetButton())
			{
				m_Timer.Reset();
			}
		}

		/// <summary>
		/// Called when the parent updates.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnUpdate(MonoBehaviour parent)
		{
			base.OnUpdate(parent);

			if (InputMapping.digInput.GetButtonDown())
			{
				m_Timer.Reset();
			}

			if (InputMapping.digInput.GetButtonUp())
			{
				m_Timer.Pause();
			}
		}

		/// <summary>
		/// Returns a state for transition. Return self if no transition.
		/// </summary>
		/// <returns>The next state.</returns>
		/// <param name="parent">Parent.</param>
		public override AbstractPlayerState GetNextState(MonoBehaviour parent)
		{
			AbstractPlayerController controller = GetPlayerController(parent);

			if (m_Timer.complete)
			{
				if (m_TrueCanGoToDigFalseCannotGoToDig)
				{
					if (controller.progress.unlockedDig)
					{
						if (!m_DirectlyBounceOnNoneDiggable || PreDigAntonState.ColliderRaysMeshCheck(parent, Vector3.down, Mathf.Infinity))
						{
							if (m_PreDigState.WillNotDigAgain)
								m_PreDigState.WillNotDigAgain = false;
							
							return m_AirDigState;
						}
					}
				}
			}

			return base.GetNextState(parent);
		}

		#endregion
    }
}