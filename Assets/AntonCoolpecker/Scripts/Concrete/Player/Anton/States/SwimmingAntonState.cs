﻿using AntonCoolpecker.Abstract.Player.States;

namespace AntonCoolpecker.Concrete.Player.Anton.States
{
	/// <summary>
	/// Swimming Anton state - When Anton is swimming.
	/// </summary>
	public class SwimmingAntonState : AbstractSwimmingState {}
}