﻿using AntonCoolpecker.Abstract.Player;
using AntonCoolpecker.Abstract.Player.States;
using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using AntonCoolpecker.Concrete.Player.Anton.States.Digging;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Player.Anton.States
{
	/// <summary>
	/// Bounce jump midair Anton state - Initally reached when Anton is midair after 
	/// first running/walking/standing still, then jumping and then doing a bounce jump up into the air.
	/// </summary>
    public class BounceJumpMidairAntonState : AbstractBounceMidair {}
}