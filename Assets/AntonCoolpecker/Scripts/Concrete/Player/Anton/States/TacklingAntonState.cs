﻿using AntonCoolpecker.Abstract.Player;
using AntonCoolpecker.Abstract.Player.States;
using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using AntonCoolpecker.Concrete.Listeners.Player.Anton;
using AntonCoolpecker.Concrete.HitBoxes;
using AntonCoolpecker.Concrete.Utils;
using AntonCoolpecker.Utils;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Player.Anton.States
{
    /// <summary>
    /// 	Anton's running tackle attack
    /// </summary>
    public class TacklingAntonState : AbstractAttackState
    {
		#region Variables

		[SerializeField] private StandingAntonState m_StandingState;

		[Tweakable("Tackle")] [SerializeField] private float m_HoldTime;
        [Tweakable("Tackle")] [SerializeField] private float m_ControlLockTime; //Time until the controls are unlocked after performing the tackle
        [Tweakable("Tackle")] [SerializeField] private float m_SlowTime; //Time until the player's velocity slows down
        [Tweakable("Tackle")] [SerializeField] private float m_TotalTime; //Time length of the tackle state
        [Tweakable("Tackle")] [SerializeField] protected float m_BaseSpeedMultiplier = 3.0f; //Initial speed multiplier when beginning the tackle
        [Tweakable("Tackle")] [SerializeField] protected float m_EndSpeedMultiplier = 0.75f; //Resulting speed multiplier at the end of the tackle
        [Tweakable("Tackle")] [SerializeField] private float m_AnimationSpeedMultiplier; //Controls speed of the tackle animation

        protected float m_MaxSpeedMultiplier;
        private Animator m_Animator;
        private Timer m_Timer;
        private HitBox m_HitBox;

		#endregion

		#region Properites

		/// <summary>
		/// Gets the max speed multiplier.
		/// </summary>
		/// <value>The max speed multiplier.</value>
		protected override float maxSpeedMultiplier { get { return m_MaxSpeedMultiplier; } }

		/// <summary>
		/// Gets the hold time.
		/// </summary>
		/// <value>The hold time.</value>
        public float HoldTime { get { return m_HoldTime; } }

		#endregion

		#region Override Functions/Methods

        /// <summary>
        /// Called when state is entered.
        /// </summary>
        /// <param name="parent"></param>
        public override void OnEnter(MonoBehaviour parent)
        {
            if (m_Timer == null)
            {
                m_Timer = new Timer();
                m_Timer.Pause();
            }

            base.OnEnter(parent);

            m_Animator = parent.GetComponentInChildren<Animator>();
            m_Animator.speed = m_AnimationSpeedMultiplier;
            m_Timer.maxTime = m_TotalTime;
            m_Timer.Reset();
            m_MaxSpeedMultiplier = m_BaseSpeedMultiplier;
            m_HitBox = GetHitBox(parent);
            m_HitBox.gameObject.SetActive(true);
        }

        /// <summary>
        /// Called when state is exited.
        /// </summary>
        /// <param name="parent"></param>
        public override void OnExit(MonoBehaviour parent)
        {
            base.OnExit(parent);

            m_Animator.speed = 1;
            m_HitBox.gameObject.SetActive(false);
        }

        /// <summary>
        /// Called every frame.
        /// </summary>
        /// <param name="parent"></param>
        public override void OnUpdate(MonoBehaviour parent)
        {
            base.OnUpdate(parent);

            if (m_Timer.elapsed >= m_SlowTime)
            {
                float transTime = m_TotalTime - m_SlowTime;
                float speedDiffPerSec = (m_BaseSpeedMultiplier - m_EndSpeedMultiplier) / transTime;

                m_MaxSpeedMultiplier = m_BaseSpeedMultiplier - speedDiffPerSec * (m_Timer.elapsed - m_SlowTime);
            }
        }

        /// <summary>
        /// Called every frame. Returning a state will cause character to change to said state.
        /// </summary>
        /// <param name="parent"></param>
        /// <returns></returns>
        public override AbstractPlayerState GetNextState(MonoBehaviour parent)
        {
            if (m_Timer.complete)
                return m_StandingState;
			
            return base.GetNextState(parent);
        }

        /// <summary>
        /// Returns world space movement vector for the character.
        /// </summary>
        /// <param name="parent"></param>
        /// <returns></returns>
        protected override Vector3 GetCameraOrientedInput(MonoBehaviour parent)
        {
            if (m_Timer.elapsed >= m_ControlLockTime)
            {
                return GetPlayerController(parent).characterLocomotor.transform.forward;
            }

            Vector3 ToReturn = base.GetCameraOrientedInput(parent).normalized;

            if (ToReturn.magnitude == 0)
                ToReturn = GetPlayerController(parent).characterLocomotor.transform.forward;
			
            return ToReturn;
        }

		#endregion
    }
}