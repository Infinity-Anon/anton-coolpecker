﻿using AntonCoolpecker.Abstract.Player.States;

namespace AntonCoolpecker.Concrete.Player.Anton.States
{
	/// <summary>
	/// Dead Anton state - When Anton dies.
	/// </summary>
	public class DeadAntonState : AbstractDeadState {}
}