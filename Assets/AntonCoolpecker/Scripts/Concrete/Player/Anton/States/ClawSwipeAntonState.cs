﻿using AntonCoolpecker.Abstract.Player.States;
using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using AntonCoolpecker.Concrete.Listeners.Player.Anton;
using AntonCoolpecker.Concrete.Utils;
using AntonCoolpecker.Utils;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Player.Anton.States
{
	/// <summary>
	/// 	Anton's claw swipe attack
	/// </summary>
	public class ClawSwipeAntonState : AbstractAttackState
	{
		#region Variables

        //Forward velocities to be seperately applied to the player at each stage of the combo
        [Tweakable("ClawSwipe")] [SerializeField] private float m_FirstAttackForwardV;
        [Tweakable("ClawSwipe")] [SerializeField] private float m_SecondAttackForwardV;
        [Tweakable("ClawSwipe")] [SerializeField] private float m_ThirdAttackForwardV;

        //Animation transitions
        [Tweakable("ClawSwipe")] [SerializeField] private float m_RecoveryTime = 0.5f; 

        //How many steps to the combo, will not do anything past 3
        [Tweakable("ClawSwipe")] [SerializeField] private float m_MaxCombo;

		//Timer
		[SerializeField] private Timer m_StateTimer;
		[SerializeField] private AbstractPlayerState m_TimerState;

        //First attack
        [SerializeField] private AnimationClip m_FirstSwipe;
        [Tweakable("ClawSwipeFirstAttack")] [SerializeField] private float m_FirstAttackLength = 0.433f;
		[Tweakable("ClawSwipeFirstAttack")] [SerializeField] private float m_FirstAttackVelocityLerpLength = 0.3f;
		[Tweakable("ClawSwipeFirstAttack")] [SerializeField] private float m_FirstComboFromTime = 0.2f;
		[Tweakable("ClawSwipeFirstAttack")] [SerializeField] private float m_FirstComboEndTime = 0.4f;
		[Tweakable("ClawSwipeFirstAttack")] [SerializeField] private float m_FirstHitboxScaleUpStartTime = 0.1f;
		[Tweakable("ClawSwipeFirstAttack")] [SerializeField] private float m_FirstHitboxScaleUpEndTime = 0.15f;
		[Tweakable("ClawSwipeFirstAttack")] [SerializeField] private float m_FirstHitboxScaleDownStartTime = 0.3f;
		[Tweakable("ClawSwipeFirstAttack")] [SerializeField] private float m_FirstHitboxScaleDownEndTime = 0.4f;

        //Second attack
        [SerializeField] private AnimationClip m_SecondSwipe;
		[Tweakable("ClawSwipeSecondAttack")] [SerializeField] private float m_SecondAttackLength = 0.417f;
		[Tweakable("ClawSwipeSecondAttack")] [SerializeField] private float m_SecondAttackVelocityLerpLength = 0.3f;
		[Tweakable("ClawSwipeSecondAttack")] [SerializeField] private float m_SecondComboFromTime = 0.2f;
		[Tweakable("ClawSwipeSecondAttack")] [SerializeField] private float m_SecondComboEndTime = 0.4f;
		[Tweakable("ClawSwipeSecondAttack")] [SerializeField] private float m_SecondHitboxScaleUpStartTime = 0.2f;
		[Tweakable("ClawSwipeSecondAttack")] [SerializeField] private float m_SecondHitboxScaleUpEndTime = 0.25f;
		[Tweakable("ClawSwipeSecondAttack")] [SerializeField] private float m_SecondHitboxScaleDownStartTime = 0.3f;
		[Tweakable("ClawSwipeSecondAttack")] [SerializeField] private float m_SecondHitboxScaleDownEndTime = 0.4f;

        //Third attack
        [SerializeField] private AnimationClip m_Thirdswipe;
		[Tweakable("ClawSwipeThirdAttack")] [SerializeField] private float m_ThirdAttackLength = 1.250f;
		[Tweakable("ClawSwipeThirdAttack")] [SerializeField] private float m_ThirdAttackVelocityLerpLength = 0.3f;
		[Tweakable("ClawSwipeThirdAttack")] [SerializeField] private float m_ThirdHitboxScaleUpStartTime = 0.5f;
		[Tweakable("ClawSwipeThirdAttack")] [SerializeField] private float m_ThirdHitboxScaleUpEndTime = 0.7f;
		[Tweakable("ClawSwipeThirdAttack")] [SerializeField] private float m_ThirdHitboxScaleDownStartTime = 1f;
		[Tweakable("ClawSwipeThirdAttack")] [SerializeField] private float m_ThirdHitboxScaleDownEndTime = 1.1f;

		//Bools
        [Tweakable("ClawSwipe")] [SerializeField] private bool m_LerpVelocity;
        [Tweakable("ClawSwipe")] [SerializeField] private bool m_AirCanceling;

		//Ground interpolation
		[Tweakable("ClawSwipe")] [SerializeField] internal bool m_InterpolateGravityScaleY = true;
		[Tweakable("ClawSwipe")] [SerializeField] internal bool m_UseExponentialAngleCalc = true;
		[Tweakable("ClawSwipe")] [SerializeField] internal bool m_TrigVelocity = true;
		[Tweakable("ClawSwipe")] [SerializeField] internal float m_LinearCalcInterpolationMultiplier = 0.01f;
		[Tweakable("ClawSwipe")] [SerializeField] internal float m_LinearCalcAngleLimitX = 1f;
		[Tweakable("ClawSwipe")] [SerializeField] internal float m_LinearCalcAngleLimitY = 40f;

		private Vector2 m_LinearCalcAngleLimit = new Vector2(1, 40);
		private bool m_UseLinearCalc = true;

        private int m_AttackStep;
        private Vector3 m_DefaultHitBoxSize;
        private Animator m_Animator;
        private HitBoxes.HitBox m_HitBox;
        private float m_InitialVelocity;
		private bool m_LerpFinished;

		#endregion

		#region Properties

		/// <summary>
		/// 	Gets the state to transition to after the timer elapses.
		/// </summary>
		/// <value>The state of the timer.</value>
		protected override AbstractPlayerState timerState { get { return m_TimerState; } }

		/// <summary>
		/// Gets the recovery time.
		/// </summary>
		/// <value>The recovery time.</value>
        public float RecoveryTime { get { return m_RecoveryTime; } }

		#endregion

        #region Override Methods/Functions

        /// <summary>
        /// 	Called when the state becomes active.
        /// </summary>
        /// <param name="parent">Parent.</param>
        public override void OnEnter(MonoBehaviour parent)
		{
			base.OnEnter(parent);

            disabledControls = true;

            m_Animator= parent.GetComponentInChildren<Animator>();
			m_HitBox= GetHitBox(parent);
            m_HitBox.gameObject.SetActive(true);
            m_DefaultHitBoxSize = m_HitBox.transform.localScale;
            m_HitBox.gameObject.transform.localScale = Vector3.zero;
            m_Animator.speed = (m_FirstSwipe.length/ m_FirstAttackLength);
            m_Animator.SetInteger("ComboCounter", 0);
            m_StateTimer.maxTime = (m_FirstAttackLength);
			m_AttackStep = 0;

            if (!m_LerpVelocity)
            {
                 GetPlayerController(parent).characterLocomotor.velocity = 
                 (GetPlayerController(parent).transform.forward.normalized) * m_FirstAttackForwardV;       
            }
            else
            {
                m_InitialVelocity = GetPlayerController(parent).characterLocomotor.velocity.magnitude;
                m_LerpFinished = false;
            }

			m_LinearCalcAngleLimit.x = m_LinearCalcAngleLimitX;
			m_LinearCalcAngleLimit.y = m_LinearCalcAngleLimitY;

			if (m_InterpolateGravityScaleY) 
				m_UseLinearCalc = true;
        }

        /// <summary>
        /// 	Called when the parent updates.
        /// </summary>
        public override void OnUpdate(MonoBehaviour parent)
		{
            float stateTime = m_StateTimer.elapsed;
            base.OnUpdate(parent);

            if (m_AttackStep == 0)
            {
                if (m_LerpVelocity && !m_LerpFinished)
                    LerpVelocity(m_FirstAttackForwardV, GetPlayerController(parent), m_FirstAttackVelocityLerpLength);
				
                HandleScaling(m_FirstHitboxScaleUpStartTime, m_FirstHitboxScaleUpEndTime,
                			  m_FirstHitboxScaleDownStartTime, m_FirstHitboxScaleDownEndTime, stateTime);
				
                if (stateTime >= m_FirstComboFromTime&&stateTime<=m_FirstComboEndTime && m_AttackStep < m_MaxCombo - 1 && InputMapping.swipeAttackInput.GetButtonDown())
                	ComboAttack(parent);                
            }
            else if (m_AttackStep == 1)
            {
                if (m_LerpVelocity && !m_LerpFinished)
                    LerpVelocity(m_SecondAttackForwardV, GetPlayerController(parent), m_SecondAttackVelocityLerpLength);
				
                HandleScaling(m_SecondHitboxScaleUpStartTime, m_SecondHitboxScaleUpEndTime,
                    		  m_SecondHitboxScaleDownStartTime, m_SecondHitboxScaleDownEndTime, stateTime);
				
                if (stateTime >= m_SecondComboFromTime&&stateTime<m_SecondComboEndTime && m_AttackStep < m_MaxCombo - 1 && InputMapping.swipeAttackInput.GetButtonDown())
                        ComboAttack(parent);
            }
            else if (m_AttackStep == 2)
            {
                if (m_LerpVelocity && !m_LerpFinished)
                    LerpVelocity(m_ThirdAttackForwardV, GetPlayerController(parent), m_ThirdAttackVelocityLerpLength);
				
                HandleScaling(m_ThirdHitboxScaleUpStartTime, m_ThirdHitboxScaleUpEndTime,
                			  m_ThirdHitboxScaleDownStartTime, m_ThirdHitboxScaleDownEndTime, stateTime);
            }

			if (m_AirCanceling && !GetPlayerController(parent).characterLocomotor.isSoftGrounded)
            {
                m_StateTimer.End();
            }
        }

		/// <summary>
		/// 	Called before another state becomes active.
		/// </summary>
		public override void OnExit(MonoBehaviour parent)
		{
            base.OnExit(parent);

            //reset anything that was changed      
            m_Animator.speed = 1;
            GetHitBox(parent).gameObject.transform.localScale = m_DefaultHitBoxSize;
            GetHitBox(parent).gameObject.SetActive(false);

			if (m_InterpolateGravityScaleY) 
			{
				if (GetPlayerController(parent).characterLocomotor.gravityScaleY != GetPlayerController(parent).characterLocomotor.originalGravityScale.y)
					setGravityScaleBack (GetPlayerController(parent).characterLocomotor.originalGravityScale.y, parent);

				m_UseLinearCalc = false;
			}
        }

        /// <summary>
		/// 	Called when the parent updates.
		/// </summary>
		/// <param name="parent">Parent.</param>
        public override void OnFixedUpdate(MonoBehaviour parent)
        {
            //TODO: Make the claw swipe work properly when swiping up a hill.
            base.OnFixedUpdate(parent);
            Abstract.Player.AbstractPlayerController player = GetPlayerController(parent);

            if (m_InterpolateGravityScaleY) //GROUND INTERPOLATION
            {
                if (m_UseLinearCalc)
                {
                    RaycastHit hitInfo;
                    Vector3 rayPosition = new Vector3(player.transform.position.x, (player.transform.position.y + player.collider.bounds.size.y - 0.1f), player.transform.position.z);
                    Vector3 direction = Vector3.down;
                    float distance = Mathf.Infinity;
                    LayerMask layerToIgnore = 1 << 4; //IGNORE WATER LAYER
                    layerToIgnore |= 1 << 12; //IGNORE PLAYER LAYER
                    bool canDo = Physics.Raycast(rayPosition, direction, out hitInfo, distance, ~layerToIgnore);
                    float surfaceAngle = player.characterLocomotor.originalGravityScale.y;

                    if (canDo)
                    {
                        if (hitInfo.collider.gameObject.tag == "Mushroom")
                            m_UseLinearCalc = false;

                        surfaceAngle = Vector3.Angle(hitInfo.normal, Vector3.up);
                    }

                    float angleCalc;

                    if (m_UseExponentialAngleCalc)
                        angleCalc = (surfaceAngle * surfaceAngle);
                    else
                        angleCalc = surfaceAngle;

                    if (surfaceAngle >= m_LinearCalcAngleLimit.x && surfaceAngle <= m_LinearCalcAngleLimit.y)
                    {
                            player.characterLocomotor.gravityScaleY = (player.characterLocomotor.originalGravityScale.y * angleCalc * m_LinearCalcInterpolationMultiplier);
                    }
                    else
                        player.characterLocomotor.gravityScaleY = player.characterLocomotor.originalGravityScale.y;
                }

                if (!m_UseLinearCalc)
                {
                    player.characterLocomotor.gravityScaleY = player.characterLocomotor.originalGravityScale.y;
                }
            }

            if (m_TrigVelocity)
            {
                float playerAngle = Vector3.Angle(player.characterLocomotor.GroundedCast().normal, player.transform.forward);
                playerAngle = playerAngle - 90;
                Vector3 temp = (player.transform.forward.normalized * player.characterLocomotor.flatVelocity.magnitude);
                float yComp = (temp.magnitude * Mathf.Tan(Mathf.Deg2Rad * playerAngle));
                temp.y = yComp;
                GetPlayerController(parent).characterLocomotor.velocity = temp;
            }

        }

        /// <summary>
        ///     Gets the timer to transition to another state.
        /// </summary>
        /// <value>The state timer.</value>
		public override Timer GetStateTimer(MonoBehaviour parent)
		{
            return m_StateTimer;
		}

        #endregion

        #region Private Functions

		/// <summary>
		/// Lerps the velocity.
		/// </summary>
		/// <param name="velocity">Velocity.</param>
		/// <param name="player">Player.</param>
		/// <param name="time">Time.</param>
        private void LerpVelocity(float velocity, Abstract.Player.AbstractPlayerController player, float time)
        {
            float currentV = player.characterLocomotor.velocity.magnitude;

            if (currentV != velocity)
            {
                Quaternion temp = Quaternion.FromToRotation(player.transform.up, player.characterLocomotor.GroundedCast().normal);
                player.characterLocomotor.velocity =
                    (temp * player.transform.forward.normalized) * Mathf.Lerp(m_InitialVelocity, velocity, m_StateTimer.elapsed / time);
            }

            if (m_StateTimer.elapsed >= time)
                m_LerpFinished = true;
        }

        /// <summary>
        /// Handle the scaling of the hitbox
        /// </summary>
        /// <param name="scaleUpStart">Start of the scale-up</param>
		/// <param name="scaleUpEnd">End of the scale-up</param>
		/// <param name="scaleDownStart">Start of the scale-down</param>
		/// <param name="scaleDownEnd">End of the scale-dow</param>
        /// <param name="stateTime">The timer of the state</param>
        /// <param name="velocity">The currently desired velocity</param>
        private void HandleScaling(float scaleUpStart, float scaleUpEnd, float scaleDownStart, float scaleDownEnd,float stateTime)
        {
            if (stateTime > scaleUpStart && stateTime < scaleUpEnd)
            {
                m_HitBox.transform.localScale = Vector3.Slerp(Vector3.zero, m_DefaultHitBoxSize,
                    (stateTime - scaleUpStart) / (scaleUpEnd - scaleUpStart));
            }
            else if ((stateTime < scaleDownStart) && (stateTime > scaleUpEnd))
                m_HitBox.transform.localScale = m_DefaultHitBoxSize;
            else if (stateTime > scaleDownStart && stateTime < scaleDownEnd)
            {
                m_HitBox.transform.localScale = Vector3.Slerp(m_DefaultHitBoxSize, Vector3.zero,
                    (stateTime - scaleDownStart) / (scaleDownEnd - scaleDownStart));
            }
            else if (stateTime > scaleDownEnd)
                m_HitBox.gameObject.SetActive(false);
        }
        
		/// <summary>
		/// Handles performed combo attacks.
		/// </summary>
		/// <param name="parent">Parent.</param>
        private void ComboAttack(MonoBehaviour parent)
		{
			//In order to ensure the collision detection is not conituous, disable and re-enable the collider
			m_HitBox.collider.enabled = false;
			m_HitBox.collider.enabled = true;
			m_HitBox.gameObject.SetActive(true);
            m_HitBox.transform.localScale = Vector3.zero;

            if (AbstractSlidingState.ShouldStartSliding(parent)||!GetPlayerController(parent).characterLocomotor.isGrounded)
				return;
			
            m_StateTimer.Reset();
            SnapToMovementVector(GetPlayerController(parent), GetMovementVector(parent));

            if (m_AttackStep == 0)
            {
                m_HitBox.transform.localScale = Vector3.zero;

                if (m_LerpVelocity)
                {
                    m_InitialVelocity = GetPlayerController(parent).characterLocomotor.velocity.magnitude;
                    m_LerpFinished = false;
                }
                else
                    GetPlayerController(parent).characterLocomotor.flatVelocity = GetPlayerController(parent).transform.forward.normalized * m_SecondAttackForwardV;
				
                m_Animator.SetInteger("ComboCounter", 1);
                m_StateTimer.maxTime = (m_SecondAttackLength);
                m_Animator.speed = (m_SecondSwipe.length / m_SecondAttackLength);
            }
            else
            {
                if (m_LerpVelocity)
                {
                    m_InitialVelocity = GetPlayerController(parent).characterLocomotor.velocity.magnitude;
                    m_LerpFinished = false;
                }
                else
                    GetPlayerController(parent).characterLocomotor.flatVelocity = GetPlayerController(parent).transform.forward.normalized * m_ThirdAttackForwardV;

				m_Animator.SetInteger("ComboCounter", 2);
                m_StateTimer.maxTime = (m_ThirdAttackLength);
                m_Animator.speed = (m_Thirdswipe.length / m_ThirdAttackLength);
            }

            m_AttackStep++;
		}

		/// <summary>
		/// Sets the gravity scale back to it's original value.
		/// </summary>
		/// <param name="orggravscale">Original gravity scale.</param>
		/// <param name="parent">Parent.</param>
		private void setGravityScaleBack(float orggravscale, MonoBehaviour parent)
		{
			if (!m_InterpolateGravityScaleY)
				return;

			GetPlayerController(parent).characterLocomotor.gravityScaleY = orggravscale;
		}

		#endregion
    }
}