﻿using AntonCoolpecker.Abstract.Player;
using AntonCoolpecker.Abstract.Player.States;
using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using AntonCoolpecker.Concrete.Player.Anton.States.Digging;
using AntonCoolpecker.Concrete.Player.Anton.States.Hitstun;
using AntonCoolpecker.Concrete.Utils;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Player.Anton.States
{
	/// <summary>
	/// 	StandingAntonState is Anton's default state.
	/// </summary>
	public class StandingAntonState : AbstractStandingState
	{
		#region Variables

		[SerializeField] private ClawSwipeAntonState m_CSwipeState;
		[SerializeField] private HitstunAntonState m_HitstunAntonState;
		[SerializeField] private PreDigAntonState m_DigPreAntonState;
        [SerializeField] private SprintAntonState m_SprintAntonState;
		[SerializeField] private TacklingAntonState m_TacklingState;

		#endregion

		#region Override Methods

        /// <summary>
        /// 	Returns a state for transition. Return self if no transition.
        /// </summary>
        /// <returns>The next state.</returns>
        /// <param name="parent">Parent.</param>
        public override AbstractPlayerState GetNextState(MonoBehaviour parent)
		{
			AbstractPlayerController controller = GetPlayerController(parent);

            if (InputMapping.sprintInput.GetAxisRaw()!=0 
				&& parent.GetComponentInChildren<Animator>().GetFloat("InputMagnitude") >= m_SprintAntonState.AllowSprintMagnitude)
                return m_SprintAntonState;

            if (InputMapping.swipeAttackInput.GetButtonDown())
                return m_CSwipeState;

            if (controller.progress.unlockedDig && InputMapping.digInput.GetButtonDown() && PreDigAntonState.CanDig(parent))
				return m_DigPreAntonState;

			return base.GetNextState(parent);
		}

		#endregion
	}
}