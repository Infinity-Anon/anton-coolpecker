﻿using AntonCoolpecker.Abstract.Player;
using AntonCoolpecker.Abstract.Player.States;
using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using AntonCoolpecker.Concrete.HitBoxes;
using AntonCoolpecker.Concrete.Listeners.Player.Anton;
using AntonCoolpecker.Concrete.Utils;
using AntonCoolpecker.Utils;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Player.Anton.States
{
    /// <summary>
	/// 	AirAttackAntonState is the class for the attack state Anton enters into when attacking in the air.
	/// </summary>
    public class AirAttackAntonState : AbstractAttackState
    {
        //TODO: FIX FLOAT ARRAY SUPPORT FOR DEBUGGUI SO THAT THE ARRAYS CAN BE MODIFIED THROUGH DEBUGGUI

		#region Variables

		[SerializeField] private AbstractSwimmingState m_SwimmingState;
        [SerializeField] private StandingAntonState m_StandingState;
        [SerializeField] private MidairAntonState m_MidairState;
        [SerializeField] private AirAttackFallingAntonState m_AirAttackFallingState;

		//The initial speed multiplier that lasts until gravity is restored.
        [Tweakable("AirAttack")] [SerializeField] protected float[] m_PreGravitySpeedMultiplier = new float[3] { 2, 3, 4 };
		//The speed multiplier that is achieved after gravity is restored and the transition time completes.
		[Tweakable("AirAttack")] [SerializeField] protected float[] m_PostGravitySpeedMultiplier = new float[3] { 0.5f, 0.75f, 1 };

        [Tweakable("AirAttack")] [SerializeField] private float m_AnimationSpeedMultiplier;
        [Tweakable("AirAttack")] [SerializeField] private int m_MaxCombo;
		[Tweakable("AirAttack")] [SerializeField] private float m_GravityTime = 1.0f; //Time until gravity is restored and begins transitioning to full gravity.
		[Tweakable("AirAttack")] [SerializeField] private float m_FullGravityTime = 1.0f; //Time until gravity is fully restored and transition is complete.
		[Tweakable("AirAttack")] [SerializeField] private float m_FullGravity = 1.0f; //Gravity multiplier that is fully applied after gravity transition is complete.
		[Tweakable("AirAttack")] [SerializeField] private float m_PostGravitySpeedTransitionStartTime = 1.0f; //Time the post-gravity speed transition begins.
		[Tweakable("AirAttack")] [SerializeField] private float m_PostGravitySpeedTransitionEndTime = 1.0f; //The Time the prost-gravity speed transition completes.
		[Tweakable("AirAttack")] [SerializeField] private float m_BonusJumpBeginTime = 1.0f; //Beginning of the window where jumping grants extra jump height.
		[Tweakable("AirAttack")] [SerializeField] private float m_BonusJumpEndTime = 1.0f; //End of the window where jumping grants extra jump height.
		[Tweakable("AirAttack")] [SerializeField] private float m_JumpBeginTime = 1.0f; //Beginning of the window where jumping is possible.
		[Tweakable("AirAttack")] [SerializeField] private float m_JumpEndTime = 1.0f; //End of the window where jumping is possible.

        [Tweakable("AirAttack")] [SerializeField] private float m_DashLineX = 1.0f; //Forward-Coordinate used to determine the angle of the attack.
		[Tweakable("AirAttack")] [SerializeField] private float m_DashLineY = 1.0f; //Upward-Coordinate used to determine the angle of the attack.

		[Tweakable("AirAttack")] [SerializeField] private bool m_UseButtonHold = true; //Use holding down the attack button instead of pressing the jump button.

        protected float m_MaxSpeedMultiplier;

		private Animator m_Animator;
		private HitBox m_HitBox;
		private int m_ComboCounter;
		private Vector2 m_Dashline;
		private Timer m_GravityClock = new Timer();

		#endregion

		#region Properties

		/// <summary>
		/// Gets the max speed multiplier.
		/// </summary>
		/// <value>The max speed multiplier.</value>
        protected override float maxSpeedMultiplier { get { return m_MaxSpeedMultiplier; } }

		#endregion

		#region Override Functions/Methods

        /// <summary>
        /// Called when state is entered.
        /// </summary>
        /// <param name="parent"></param>
        public override void OnEnter(MonoBehaviour parent)
        {
            base.OnEnter(parent);

            m_ComboCounter++;

            AbstractPlayerController controller = GetPlayerController(parent);

            m_Animator = parent.GetComponentInChildren<Animator>();
            m_Animator.speed = m_AnimationSpeedMultiplier;

            m_MaxSpeedMultiplier = m_PreGravitySpeedMultiplier[m_ComboCounter];

            m_HitBox = GetHitBox(parent);
            m_HitBox.gameObject.SetActive(true);

            m_Dashline = new Vector2(m_DashLineX, m_DashLineY).normalized;

            controller.characterLocomotor.velocity = (new Vector3(0, m_Dashline.y, 0) + controller.transform.forward * m_Dashline.x) * controller.maxSpeed * m_MaxSpeedMultiplier;

            m_Animator.SetInteger("ComboCounter", m_ComboCounter);

            controller.characterLocomotor.gravityScaleY = 0;

            m_GravityClock.maxTime = m_GravityTime;
            m_GravityClock.Reset();

            controller.useJumpAccelDecel = false;
        }

        /// <summary>
        /// Called when state is exited.
        /// </summary>
        /// <param name="parent"></param>
        public override void OnExit(MonoBehaviour parent)
        {
            base.OnExit(parent);

            AbstractPlayerController controller = GetPlayerController(parent);

            m_Animator.speed = 1;

            m_HitBox.gameObject.SetActive(false);

            controller.characterLocomotor.gravityScaleY = controller.characterLocomotor.originalGravityScale.y;
            controller.useJumpAccelDecel = true;
        }

        /// <summary>
		/// 	Called when the parent's physics update.
		/// </summary>
		/// <param name="parent">Parent.</param>
        public override void OnFixedUpdate(MonoBehaviour parent)
        {
            AbstractPlayerController controller = GetPlayerController(parent);

            if (m_GravityClock.complete)
            {
                if (m_GravityClock.elapsed > m_PostGravitySpeedTransitionEndTime)
                    m_MaxSpeedMultiplier = m_PostGravitySpeedMultiplier[m_ComboCounter];
                else if (m_GravityClock.elapsed > m_PostGravitySpeedTransitionStartTime)
                {
                    float transitionPercent = (m_GravityClock.elapsed - m_PostGravitySpeedTransitionStartTime) / (m_PostGravitySpeedTransitionEndTime - m_PostGravitySpeedTransitionStartTime);
                    m_MaxSpeedMultiplier = m_PostGravitySpeedMultiplier[m_ComboCounter] * transitionPercent + m_PreGravitySpeedMultiplier[m_ComboCounter] * (1 - transitionPercent);
                }

                controller.characterLocomotor.velocity = new Vector3(0, controller.characterLocomotor.velocity.y, 0) + controller.transform.forward * m_Dashline.x * controller.maxSpeed * m_MaxSpeedMultiplier;

                if (m_GravityClock.elapsed > m_FullGravityTime)
                    controller.characterLocomotor.gravityScaleY = controller.characterLocomotor.originalGravityScale.y * m_FullGravity;
                else
                    controller.characterLocomotor.gravityScaleY = controller.characterLocomotor.originalGravityScale.y * (m_GravityClock.elapsed - m_GravityTime) / (m_FullGravityTime - m_GravityTime) * m_FullGravity;
            }

            base.OnFixedUpdate(parent);
        }

        /// <summary>
        /// Called every frame. Returning a state will cause character to change to said state.
        /// </summary>
        /// <param name="parent"></param>
        /// <returns></returns>
        public override AbstractPlayerState GetNextState(MonoBehaviour parent)
        {
            AbstractPlayerController controller = GetPlayerController(parent);

            if (controller.isInWater)
                return m_SwimmingState;

            if (controller.characterLocomotor.isGrounded)
                return m_StandingState;
            
            if (m_GravityClock.elapsed >= m_JumpBeginTime && m_GravityClock.elapsed <= m_JumpEndTime &&
                ((m_UseButtonHold && !InputMapping.swipeAttackInput.GetButton()) || (!m_UseButtonHold && InputMapping.jumpInput.GetButtonDown())))
            {
                if (m_ComboCounter < m_MaxCombo - 1)
                {
                    if (m_GravityClock.elapsed >= m_BonusJumpBeginTime && m_GravityClock.elapsed <= m_BonusJumpEndTime)
                        m_AirAttackFallingState.GiveBonusJump();
					
                    if (!m_UseButtonHold)
                        controller.airTracker.canJumpCancel = true;

                    return m_AirAttackFallingState;
                }
                else
                    return m_MidairState;
            }

            return base.GetNextState(parent);
        }

        /// <summary>
        /// Returns world space movement vector for the character.
        /// </summary>
        /// <param name="parent"></param>
        /// <returns></returns>
        protected override Vector3 GetCameraOrientedInput(MonoBehaviour parent)
        {
            if (m_GravityClock.complete)
                return GetPlayerController(parent).characterLocomotor.transform.forward;
            else
                return Vector3.zero;
        }

		#endregion

		#region Public Functions

        /// <summary>
        /// Resets the combo counter.
        /// </summary>
        public void ResetCombo()
        {
            m_ComboCounter = -1;
        }

		#endregion
    }
}