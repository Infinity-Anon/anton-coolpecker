﻿using AntonCoolpecker.Abstract.Player.States.Underwater;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Player.Anton.States.Underwater
{
	/// <summary>
	/// Underwater Anton state - When Anton is underwater.
	/// </summary>
	public class UnderwaterAntonState : AbstractUnderwaterState
	{
		#region Variables

		//[SerializeField] private bool m_CanFloatDownwards = false;
		[SerializeField] private float m_DownwardsFloat = 0.8f;

		#endregion

		#region Properties

		/// <summary>
		/// 	Gets or sets the upwards float for Coolpecker(Floating slowly upwards).
		/// </summary>
		/// <value>The upwards float.</value>
		public float downwardsFloat { get { return m_DownwardsFloat; } set { m_DownwardsFloat = value; } }

		#endregion
	}
}