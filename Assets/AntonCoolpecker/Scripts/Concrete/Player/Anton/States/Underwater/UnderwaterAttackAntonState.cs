﻿using AntonCoolpecker.Abstract.Player.States.Underwater;

namespace AntonCoolpecker.Concrete.Player.Anton.States.Underwater
{
	/// <summary>
	/// Underwater attack Anton state - When Anton is attacking underwater.
	/// </summary>
	public class UnderwaterAttackAntonState : AbstractUnderwaterAttackState {}
}