﻿using AntonCoolpecker.Abstract.Player.States.Underwater;

namespace AntonCoolpecker.Concrete.Player.Anton.States.Underwater
{
	/// <summary>
	/// Underwater swimming Anton state - When Anton is swimming underwater.
	/// </summary>
	public class UnderwaterSwimmingAntonState : AbstractUnderwaterSwimmingState {}
}