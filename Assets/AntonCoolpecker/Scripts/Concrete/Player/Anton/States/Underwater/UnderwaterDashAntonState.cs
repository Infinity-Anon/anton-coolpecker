﻿using AntonCoolpecker.Abstract.Player.States.Underwater;

namespace AntonCoolpecker.Concrete.Player.Anton.States.Underwater
{
	/// <summary>
	/// Underwater dash Anton state - When Anton is dashing underwater.
	/// </summary>
	public class UnderwaterDashAntonState : AbstractUnderwaterDashState {}
}