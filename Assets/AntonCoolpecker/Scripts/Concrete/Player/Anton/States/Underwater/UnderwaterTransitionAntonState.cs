﻿using AntonCoolpecker.Abstract.Player.States.Underwater;

namespace AntonCoolpecker.Concrete.Player.Anton.States.Underwater
{
	/// <summary>
	/// Underwater transition Anton state - When Anton transitions from/to underwater.
	/// </summary>
	public class UnderwaterTransitionAntonState : AbstractUnderwaterTransitionState {}
}