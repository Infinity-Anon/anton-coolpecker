﻿using AntonCoolpecker.Abstract.Player;
using AntonCoolpecker.Abstract.Player.States;
using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using AntonCoolpecker.Concrete.HitBoxes;
using AntonCoolpecker.Concrete.Listeners.Player.Anton;
using AntonCoolpecker.Concrete.Player.Anton.States.Digging;
using AntonCoolpecker.Concrete.Utils;
using AntonCoolpecker.Utils;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Player.Anton.States
{
    /// <summary>
	/// 	AirAttackFallingAntonState is the class for midair state Anton enters into after completing an air attack.
	/// </summary>
    public class AirAttackFallingAntonState : MidairAntonState
    {
		#region Variables

		//If bonus jump is active, multiply the jump height by this number
		[Tweakable("AirAttackFall")] [SerializeField] private float m_BonusJumpMultiplier = 5.0f;
		//If bonus jump is active, multiply the jump height by this number
		[Tweakable("AirAttackFall")] [SerializeField] private float m_MaxSpeedMultiplier = 0.1f;

		private bool m_BonusJumpHeight;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the max speed multiplier.
		/// </summary>
		/// <value>The max speed multiplier.</value>
        protected override float maxSpeedMultiplier { get { return m_MaxSpeedMultiplier; } }

		#endregion

		#region Override Functions/Methods

        /// <summary>
        /// Called when state is entered.
        /// </summary>
        /// <param name="parent"></param>
        public override void OnEnter(MonoBehaviour parent)
        {
            base.OnEnter(parent);

            AbstractPlayerController controller = GetPlayerController(parent);

            controller.useJumpAccelDecel = false;

            float appliedJumpMulti = 1;
            if (m_BonusJumpHeight)
                appliedJumpMulti = m_BonusJumpMultiplier;
			
            controller.characterLocomotor.velocity = new Vector3(0, controller.JumpForce(controller.jumpHeight * jumpHeightMultiplier * appliedJumpMulti), 0) + controller.transform.forward;
        }

        /// <summary>
        /// Called when state is exited.
        /// </summary>
        /// <param name="parent"></param>
        public override void OnExit(MonoBehaviour parent)
        {
            base.OnExit(parent);

            AbstractPlayerController player = GetPlayerController(parent);

            player.airTracker.canSpinHover = false;
            player.useJumpAccelDecel = true;
            m_BonusJumpHeight = false;
        }

        /// <summary>
        /// Called every frame. Returning a state will cause character to change to said state.
        /// </summary>
        /// <param name="parent"></param>
        /// <returns></returns>
        public override AbstractPlayerState GetNextState(MonoBehaviour parent)
        {
            AbstractPlayerController controller = GetPlayerController(parent);

            if (InputMapping.swipeAttackInput.GetButtonDown())
                return m_AirAttackState;

            return base.GetNextState(parent);
        }

        /// <summary>
        /// Rotates the character towards the movement vector.
        /// </summary>
        /// <param name="player"></param>
        /// <param name="movement"></param>
        protected override void RotateTowardsMovementVector(AbstractPlayerController player, Vector3 movement)
        {
            return;
        }

		#endregion

		#region Public Functions

		/// <summary>
		/// Allow bonus height to jump.
		/// </summary>
        public void GiveBonusJump()
        {
            m_BonusJumpHeight = true;
        }

		#endregion
    }
}