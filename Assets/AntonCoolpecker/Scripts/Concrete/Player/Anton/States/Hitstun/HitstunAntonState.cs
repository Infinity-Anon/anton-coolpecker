using AntonCoolpecker.Abstract.Player.States.Hitstun;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Player.Anton.States.Hitstun
{
	/// <summary>
	/// Hitstun Anton state - When Anton is stunned after a hit.
	/// </summary>
	public class HitstunAntonState : AbstractHitstunState 
	{
		#region Variables

		#endregion

		#region Properties

		#endregion

		#region Override Functions

		/// <summary>
		/// Called when the parent updates.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnUpdate(MonoBehaviour parent)
		{
			base.OnUpdate(parent);
		}

		#endregion
	}
}