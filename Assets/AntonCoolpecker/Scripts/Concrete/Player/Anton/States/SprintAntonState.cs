﻿using AntonCoolpecker.Abstract.Player;
using AntonCoolpecker.Abstract.Player.States;
using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using System.Collections;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Player.Anton.States
{
    /// <summary>
    /// Anton sprinting state
    /// </summary>
    public class SprintAntonState : AbstractSprintState
    {
		#region Variables

        [SerializeField] private TacklingAntonState m_TacklingState;

		#endregion

		#region Override Methods

        /// <summary>
        /// 	Returns a state for transition. Return self if no transition.
        /// </summary>
        /// <returns>The next state.</returns>
        /// <param name="parent">Parent.</param>
        public override AbstractPlayerState GetNextState(MonoBehaviour parent)
        {
            AbstractPlayerController controller = GetPlayerController(parent);

            if (InputMapping.swipeAttackInput.GetButtonDown())
                return m_TacklingState;

            return base.GetNextState(parent);
        }

		#endregion
    }
}