﻿using AntonCoolpecker.Abstract.Player;
using AntonCoolpecker.Abstract.Player.States;
using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using System.Collections;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Player.Anton.States
{
	/// <summary>
	/// Bounce jump land Anton state - Reached when Anton either initiates a bounce jump 
	/// by landing/air digging or lands after doing a bounce jump.
	/// </summary>
    public class BounceJumpLandAntonState : AbstractBounceLandState {}
}