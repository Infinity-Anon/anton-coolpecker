using AntonCoolpecker.Abstract.Player.States;

namespace AntonCoolpecker.Concrete.Player.Anton.States
{
	/// <summary>
	/// Sliding Anton state is used when Anton is sliding on a slope.
	/// </summary>
	public class SlidingAntonState : AbstractSlidingState {}
}