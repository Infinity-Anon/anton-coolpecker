﻿using AntonCoolpecker.Abstract.Player;
using AntonCoolpecker.Abstract.Player.States;
using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using AntonCoolpecker.Concrete.Player.Anton.States.Digging;
using AntonCoolpecker.Concrete.Utils;
using AntonCoolpecker.Utils;
using System.Collections;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Player.Anton.States
{
	/// <summary>
	/// Bounce jump Anton state - When Anton does a bounce jump(No sprinting beforehand) while midair.
	/// </summary>
    public class BounceJumpAntonState : AbstractBounceJumpState
    {
		#region Variables

        [SerializeField] private AirDigAntonState m_AirDigState;
        [SerializeField] private PreDigAntonState m_PreDigState;

		//Required button hold/unhold time to register a bounce jump.
        [Tweakable("BounceJump")] [SerializeField] private float m_RequiredHoldTime;
		//Allows the player to dig into ground if true, otherwise false.
        [Tweakable("BounceJump")] [SerializeField] private bool m_TrueCanGoToDigFalseCannotGoToDig;
		//Allows the player to directly bounce on non-diggable ground /regardless of hold time).
		[Tweakable("BounceJump")] [SerializeField] private bool m_DirectlyBounceOnNoneDiggable;

        private Timer m_Timer;

		#endregion

		#region Override Functions/Methods

		/// <summary>
		/// Called when the state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
        public override void OnEnter(MonoBehaviour parent)
        {
            base.OnEnter(parent);

            m_Timer = new Timer();
            m_Timer.maxTime = m_RequiredHoldTime;

            if (InputMapping.digInput.GetButton())
            {
                m_Timer.Reset();
            }
        }

		/// <summary>
		/// Called when the parent updates.
		/// </summary>
		/// <param name="parent">Parent.</param>
        public override void OnUpdate(MonoBehaviour parent)
        {
            base.OnUpdate(parent);

            if (InputMapping.digInput.GetButtonDown())
            {
                m_Timer.Reset();
            }

            if (InputMapping.digInput.GetButtonUp())
            {
                m_Timer.Pause();
            }
        }

		/// <summary>
		/// Returns a state for transition. Return self if no transition.
		/// </summary>
		/// <returns>The next state.</returns>
		/// <param name="parent">Parent.</param>
        public override AbstractPlayerState GetNextState(MonoBehaviour parent)
        {
            AbstractPlayerController controller = GetPlayerController(parent);

            if (m_Timer.complete)
            {
                if (m_TrueCanGoToDigFalseCannotGoToDig)
                {
                    if (controller.progress.unlockedDig)
                    {
                        if (!m_DirectlyBounceOnNoneDiggable || PreDigAntonState.ColliderRaysMeshCheck(parent, Vector3.down, Mathf.Infinity))
                        {
                            if (m_PreDigState.WillNotDigAgain)
                                m_PreDigState.WillNotDigAgain = false;
							
                            return m_AirDigState;
                        }
                    }
                }
            }

            return base.GetNextState(parent);
        }

		#endregion
    }   
}