﻿using AntonCoolpecker.Abstract.Cameras.States;
using AntonCoolpecker.Abstract.Player;
using AntonCoolpecker.Abstract.Player.States;
using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using AntonCoolpecker.Concrete.Utils;
using AntonCoolpecker.Utils;
using AntonCoolpecker.Utils.Time;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Player.Anton.States.Digging
{
	/// <summary>
	/// 	DuringDigAntonState is used when Anton is going to dig through the air.
	/// </summary>
	public class DuringDigAntonState : AbstractPlayerState
	{
		#region Variables

		// References
		[SerializeField] private ParticleSystem m_DigParticles;
		private ParticleSystem m_InstantiatedDP;

		// Variables
		// Smoothness of linear interpolation
		[SerializeField] [Tweakable("DuringDig")] private float m_LerpSmooth = 5f;
		// The maximum distance of the ground check
		[SerializeField] [Tweakable("DuringDig")] public static float maxGroundCheckDistance = 4f;
		// Max time value of timer
		[SerializeField] [Tweakable("DuringDig")] private float m_TimerLength = 5f;

		[SerializeField] private Timer m_StateTimer;
		[SerializeField] private AbstractPlayerState m_TimerState;

		#endregion

		#region Properties

		/// <summary>
		/// 	Gets the state to transition to after the timer elapses.
		/// </summary>
		/// <value>The state of the timer.</value>
		protected override AbstractPlayerState timerState { get { return m_TimerState; } }

		#endregion

		#region Override Functions/Methods

		/// <summary>
		/// 	Called when the object is about to be destroyed.
		/// </summary>
		protected override void OnDestroy()
		{
			base.OnDestroy();

			DestroyDigParticles(0.0f);
		}

		public override void OnEnter(MonoBehaviour parent)
		{
			base.OnEnter(parent);

			m_StateTimer.maxTime = m_TimerLength;

			AbstractPlayerController anton = GetPlayerController(parent);
			RaycastHit skyInfo = anton.characterLocomotor.SkywardsCast();

			// Instantiate the dig particles
			m_InstantiatedDP =
				Instantiate(m_DigParticles, (new Vector3(skyInfo.point.x, skyInfo.point.y, skyInfo.point.z)),
							anton.transform.rotation) as ParticleSystem;

			// Insert the height of collision mesh in the Y position (Currently 1f)
			anton.transform.position =
				(new Vector3(skyInfo.point.x, (skyInfo.point.y + (1f + (skyInfo.collider.contactOffset * 5f))), skyInfo.point.z));

			// Hide the player
			anton.Hide();

			anton.characterLocomotor.velocity = Vector3.zero;

			if (Physics.GetIgnoreLayerCollision(12, 13))
				Physics.IgnoreLayerCollision(12, 13, false);

			//Make sure player doesn't collide with enemy nor projectile while digging
			Physics.IgnoreLayerCollision(12, 14, true); //PLAYER - ENEMY
			Physics.IgnoreLayerCollision(12, 15, true); //PLAYER - PROJECTILE
		}

		public override void OnExit(MonoBehaviour parent)
		{
			base.OnExit(parent);

			DestroyDigParticles(0.5f);
		}

		public override AbstractPlayerState GetNextState(MonoBehaviour parent)
		{
			if (!CollideWithSurface(parent))
				return timerState;

			return base.GetNextState(parent);
		}

		public override void OnUpdate(MonoBehaviour parent)
		{
			base.OnUpdate(parent);

			AbstractPlayerController anton = GetPlayerController(parent);
			RaycastHit groundInfo = anton.characterLocomotor.ColliderCast(Vector3.down * PreDigAntonState.rayDistance, true);

			m_InstantiatedDP.transform.position = new Vector3(anton.transform.position.x,
															  Mathf.Lerp(anton.transform.position.y, groundInfo.point.y, (m_LerpSmooth * 5f) * GameTime.deltaTime),
															  anton.transform.position.z);

			if (anton.characterLocomotor.velocity.magnitude > 2f)
			{
				m_InstantiatedDP.transform.rotation =
					Quaternion.LookRotation(Vector3.ProjectOnPlane(m_InstantiatedDP.transform.forward, groundInfo.normal),
											groundInfo.normal);
				m_InstantiatedDP.transform.rotation = Quaternion.Euler(m_InstantiatedDP.transform.rotation.eulerAngles.x,
																	   anton.transform.rotation.eulerAngles.y + 180, m_InstantiatedDP.transform.rotation.eulerAngles.z);
				m_InstantiatedDP.gravityModifier = Mathf.Lerp(m_InstantiatedDP.gravityModifier, 0.5f,
															  m_LerpSmooth * GameTime.deltaTime);
			}

			else
			{
				m_InstantiatedDP.transform.rotation = Quaternion.Lerp(m_InstantiatedDP.transform.rotation,
																	  Quaternion.Euler(-90, 0, 0), m_LerpSmooth * GameTime.deltaTime);
				m_InstantiatedDP.gravityModifier = Mathf.Lerp(m_InstantiatedDP.gravityModifier, 2f,
															  m_LerpSmooth * GameTime.deltaTime);
			}

			m_InstantiatedDP.emissionRate = 3 + Vector3.SqrMagnitude(anton.characterLocomotor.velocity) * 0.5f;
			m_InstantiatedDP.playbackSpeed = 2 + Vector3.SqrMagnitude(anton.characterLocomotor.velocity) * 0.001f;
		}

        /// <summary>
        ///     Gets the timer to transition to another state.
        /// </summary>
        /// <value>The state timer.</value>
		public override Timer GetStateTimer(MonoBehaviour parent)
		{
            return m_StateTimer;
		}

		#endregion

		#region Public Methods

		public bool CollideWithSurface(MonoBehaviour parent)
		{
			AbstractPlayerController anton = GetPlayerController(parent);

			if (InputMapping.digInput.GetButtonDown())
				return false;

			if (PreDigAntonState.ColliderRaysMeshCheck(anton, Vector3.down, maxGroundCheckDistance))
				return true;

			return false;
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// 	Destroys the dig particles.
		/// </summary>
		/// <returns>The dig particles.</returns>
		private ParticleSystem DestroyDigParticles(float delay)
		{
			m_InstantiatedDP.emissionRate = 0;
			Destroy(m_InstantiatedDP.gameObject, delay);
			m_InstantiatedDP = null;
			return null;
		}

		#endregion
	}
}