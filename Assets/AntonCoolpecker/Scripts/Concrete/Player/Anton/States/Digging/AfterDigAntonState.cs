﻿using AntonCoolpecker.Abstract.Player;
using AntonCoolpecker.Abstract.Player.States;
using AntonCoolpecker.Concrete.HitBoxes;
using AntonCoolpecker.Concrete.Utils;
using AntonCoolpecker.Utils;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Player.Anton.States.Digging
{
	/// <summary>
	/// 	AfterDigAntonState is used when Anton has finished digging and is returning to the surface.
	/// </summary>
	public class AfterDigAntonState : AbstractPlayerState
	{
		#region Variables

		[SerializeField] [Tweakable("AfterDig")] private float m_AfterDigJumpForce;
		[SerializeField] [Tweakable("AfterDig")] private float m_EnemyKnockbackTime;
		[SerializeField] [Tweakable("AfterDig")] private float m_EnemyKnockbackSphereRadius;
		[SerializeField] [Tweakable("AfterDig")] private float m_TimerLength = 0.01f;

		[SerializeField] private Timer m_StateTimer;
		[SerializeField] private AbstractPlayerState m_TimerState;
		[SerializeField] private GameObject m_EnemyKnockbackCollision;

		private GameObject m_InstantiatedEKC;

		#endregion

		#region Properties

		/// <summary>
		/// 	Gets the state to transition to after the timer elapses.
		/// </summary>
		/// <value>The state of the timer.</value>
		protected override AbstractPlayerState timerState { get { return m_TimerState; } }

		#endregion

		#region Override Function/Methods

		/// <summary>
		/// 	Called when the state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnEnter(MonoBehaviour parent)
		{
			base.OnEnter(parent);

			AbstractPlayerController anton = GetPlayerController(parent);

			m_StateTimer.maxTime = m_TimerLength;
			m_StateTimer.Pause();

			//Instantiate collision prefab
			m_InstantiatedEKC =
				Instantiate(m_EnemyKnockbackCollision, anton.transform.position, anton.transform.rotation) as GameObject;

			anton.characterLocomotor.velocity = Vector3.zero;
			Jump(Vector3.up, anton, m_AfterDigJumpForce);

			//Get the hitbox script inside the collision prefab and make sure it ignores the player
			DamageDealingHitBox m_HitBoxInfo = m_InstantiatedEKC.GetComponent<DamageDealingHitBox>();
			m_HitBoxInfo.ignorePlayer = true;

			//Get the collision prefab's sphere collider and make it a trigger
			SphereCollider spherie = m_InstantiatedEKC.GetComponent<SphereCollider>();
			spherie.isTrigger = true;

			//Make sure the collision radius stays consistent with the inserted values in the after dig state
			if (spherie.radius != m_EnemyKnockbackSphereRadius)
				spherie.radius = m_EnemyKnockbackSphereRadius;

			//Destroy the collider after a specific amount of time
			Destroy(m_InstantiatedEKC, m_EnemyKnockbackTime);

			// Show the player
			anton.Show();
		}

		/// <summary>
		/// 	Called when the parent's physics update.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnFixedUpdate(MonoBehaviour parent)
		{
			base.OnFixedUpdate(parent);

			AbstractPlayerController anton = GetPlayerController(parent);

			if (anton.characterLocomotor.isGrounded && anton.characterLocomotor.velocity.y <= 0)
				m_StateTimer.Resume();
		}

		/// <summary>
		/// Called before another state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnExit(MonoBehaviour parent)
		{
			Physics.IgnoreLayerCollision(12, 14, false); //PLAYER - ENEMY
			Physics.IgnoreLayerCollision(12, 15, false); //PLAYER - PROJECTILE;

			base.OnExit(parent);
		}

        /// <summary>
        ///     Gets the timer to transition to another state.
        /// </summary>
        /// <value>The state timer.</value>
		public override Timer GetStateTimer(MonoBehaviour parent)
		{
            return m_StateTimer;
		}

		#endregion
	}
}