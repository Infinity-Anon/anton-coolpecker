using AntonCoolpecker.Abstract.Player;
using AntonCoolpecker.Abstract.Player.States;
using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using AntonCoolpecker.Concrete.Player.Anton.States.Underwater;
using AntonCoolpecker.Concrete.Utils;
using AntonCoolpecker.Utils;
using System;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Player.Anton.States.Digging
{
	/// <summary>
	/// 	AirDigAntonState is used when Anton is going to dig through the air.
	/// </summary>
	public class AirDigAntonState : AbstractPlayerState
	{
		#region Variables

		// States
		[SerializeField] private StandingAntonState m_StandingState;
		[SerializeField] private SwimmingAntonState m_SwimmingState;
		[SerializeField] private UnderwaterTransitionAntonState m_UnderwaterTransitionState;
		[SerializeField] private DuringDigAntonState m_DuringDigState;
		[SerializeField] private MidairAntonState m_MidairState;
        [SerializeField]private BounceJumpAntonState m_BounceJumpState;
		[SerializeField] private PreDigAntonState m_PreDigState;

		// Variables
		// Decides at which vertical velocity the player may air dig
		[SerializeField] [Tweakable("AirDig")] private float m_AirDigActivateValue = 5f;
		// The downforce of the player jump
		[SerializeField] [Tweakable("AirDig")] private float m_DownForce;

		[SerializeField] private Timer m_Timer;
		[SerializeField] [Tweakable("AirDig")] private float m_TimerLength = 0;

		// Bools
		// Hold is false and toggle is true by default.
		[SerializeField] [Tweakable("AirDig")] private bool m_HoldOffToggleOn;

		#endregion

		#region Override Methods

		/// <summary>
		/// 	Called when the state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnEnter(MonoBehaviour parent)
		{
			base.OnEnter(parent);

			m_Timer.Reset();

			m_Timer.maxTime = m_TimerLength;

			AbstractPlayerController anton = GetPlayerController(parent);

			m_UnderwaterTransitionState.CurrentDownforcePosition (anton.transform.position.y);

			anton.characterLocomotor.velocity = Vector3.zero;
			anton.characterLocomotor.useGravity = false;

			// If the player can dig, ignore the collision between the player and the ground
			if (PreDigAntonState.ColliderRaysMeshCheck(parent, Vector3.down, Mathf.Infinity))
			{
				m_PreDigState.SetTheSurfaceHeights(parent);

				if (IsRayDistanceOverGroundLimit(anton))
					Physics.IgnoreLayerCollision(12, 13, true);

				if (!IsRayDistanceOverGroundLimit(anton))
					Physics.IgnoreLayerCollision(12, 13, false);
			}

			// Else, the player won't pass through the ground
			else
				Physics.IgnoreLayerCollision(12, 13, false);
		}

		/// <summary>
		/// 	Called before another state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnExit(MonoBehaviour parent)
		{
			base.OnExit(parent);

			AbstractPlayerController anton = GetPlayerController(parent);
			anton.characterLocomotor.useGravity = true;
			Physics.IgnoreLayerCollision(12, 13, false);
		}

		/// <summary>
		/// 	Returns a state for transition. Return self if no transition.
		/// </summary>
		/// <returns>The next state.</returns>
		/// <param name="parent">Parent.</param>
		public override AbstractPlayerState GetNextState(MonoBehaviour parent)
		{
			AbstractPlayerController anton = GetPlayerController(parent);

			if (PreDigAntonState.WillDig(parent))
				return m_DuringDigState;

			if (!Physics.GetIgnoreLayerCollision(12, 13))
			{
				if (anton.characterLocomotor.isGrounded || anton.characterLocomotor.isSoftGrounded)
					return m_StandingState;

				//if (anton.isInWater)
				//	return m_SwimmingState;
			}

			if (m_HoldOffToggleOn)
			{
				if (InputMapping.jumpInput.GetButtonDown() && PreDigAntonState.HoldDigRayCheck(parent, Vector3.down, Mathf.Infinity))
				{
					Jump(Vector3.up, anton, m_DownForce * 1.2f);
					m_PreDigState.PrematureDigExit();
					return m_MidairState;
				}
			}

			else
			{
				if (!InputMapping.digInput.GetButton() && PreDigAntonState.HoldDigRayCheck(parent, Vector3.down, Mathf.Infinity))
				{
					m_PreDigState.PrematureDigExit();
					if (anton.characterLocomotor.isGrounded || anton.characterLocomotor.isSoftGrounded)
						return m_StandingState;

                    //if (anton.isInWater)
                    //	return m_SwimmingState;

                    anton.GetComponentInChildren<Animator>().SetTrigger("AirDigCancel");
                    if (InputMapping.groundPoundInput.GetButtonUp())
                        return m_BounceJumpState;
					return m_MidairState;
				}
			}

			if (anton.isInWater)
			{
				if (anton.waterTrigger.allowUnderwater && m_UnderwaterTransitionState.DownUnder(anton))
				{
					m_UnderwaterTransitionState.GroundPoundAirDig();
					m_UnderwaterTransitionState.SwimIn();
					//m_UnderwaterTransitionState.GetVelocity(anton);
					return m_UnderwaterTransitionState;
				}

				return m_SwimmingState;
			}

			return base.GetNextState(parent);
		}

		public override void OnUpdate(MonoBehaviour parent)
		{
			base.OnUpdate(parent);

			AbstractPlayerController anton = GetPlayerController(parent);

			if (m_Timer.complete)
				UseDownforce(anton);
		}

		#endregion

		#region Public Methods

		public bool CanAirDig(MonoBehaviour parent)
		{
			AbstractPlayerController anton = GetPlayerController(parent);

			if (!IsAirDigAllowed(anton))
				return false;

			return true;
		}

		#endregion

		#region Private Methods

		private bool IsAirDigAllowed(AbstractPlayerController anton)
		{
			if (anton.characterLocomotor.velocity.y < m_AirDigActivateValue &&
				anton.characterLocomotor.velocity.y > -m_AirDigActivateValue)
				return true;

			if (PreDigAntonState.willNotDigAgain)
				return false;

			return false;
		}

		private void UseDownforce(AbstractPlayerController anton)
		{
			anton.characterLocomotor.useGravity = true;
			anton.characterLocomotor.velocity = Vector3.down * m_DownForce; //Push Anton down.
		}

		/// <summary>
		/// 	Checks if one ray height point differates too much from the other rays
		/// 	height points(Distance higher than ground height limit)
		/// </summary>
		/// <returns><c>true</c> if one ray distance is different; otherwise, <c>false</c>.</returns>
		/// <param name="anton">Anton.</param>
		private bool IsRayDistanceOverGroundLimit(AbstractPlayerController anton)
		{
			float[] heightvalues = new float[4];

			for (int i = 0; i < PreDigAntonState.rayHitPoses.Length; i++)
				heightvalues[i] = PreDigAntonState.rayHitPoses[i].y;

			Array.Sort(heightvalues);

			float[] distances = new float[4];

			for (int i = 0; i < distances.Length - 1; i++)
			{
				distances[i] = (heightvalues[i] - heightvalues[i + 1]);

				if (-distances[i] >= DuringDigAntonState.maxGroundCheckDistance)
					return false;
			}

			return true;
		}

		#endregion
	}
}