using AntonCoolpecker.Abstract.Player;
using AntonCoolpecker.Abstract.Player.States;
using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using AntonCoolpecker.Utils;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Player.Anton.States.Digging
{
	/// <summary>
	/// 	PreDigAntonState is used when Anton is going to do a normal dig
	/// </summary>
	public class PreDigAntonState : AbstractPlayerState
	{
		#region Variables

		// States
		[SerializeField] private DuringDigAntonState m_DiggingState;
		[SerializeField] private MidairAntonState m_MidairState;
		[SerializeField] private StandingAntonState m_StandingState;

		// Variables
		// How far player jump before digging
		[SerializeField] [Tweakable("BeforeDig")] private static float m_PreDigJumpForce = 2f;
		// The vertical distance of the ray check
		[SerializeField] [Tweakable("BeforeDig")] private static float m_RayPosModY = 1f;
		// The horizontal distance of the ray check
		[SerializeField] [Tweakable("BeforeDig")] private static float m_RayPosModZX = 1f;
		// Modifier for distance after player is through ground, where digging will be activated
		[SerializeField] [Tweakable("BeforeDig")] private static float m_DigHitPointMod = 3.5f;
		// Distance of ray and collider cast check.
		[SerializeField] [Tweakable("BeforeDig")] public static float rayDistance = 15f;

		// Bools
		// Hold is false and toggle is true by default.
		[SerializeField] [Tweakable("BeforeDig")] private bool m_HoldOffToggleOn;

		public static bool willNotDigAgain = false;

		// Rays
		public static Vector3[] rayHitPoses = new Vector3[4];

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets a value indicating whether Anton will not dig again.
		/// </summary>
		/// <value><c>true</c> if will not dig again; otherwise, <c>false</c>.</value>
        public bool WillNotDigAgain { get { return willNotDigAgain; } set { willNotDigAgain = value; } }

		#endregion

		#region Override Methods

		/// <summary>
		/// 	Called when the state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnEnter(MonoBehaviour parent)
		{
			base.OnEnter(parent);

			AbstractPlayerController anton = GetPlayerController(parent);

			anton.characterLocomotor.velocity = Vector3.zero;
			Jump(Vector3.up, anton, m_PreDigJumpForce);

			Physics.IgnoreLayerCollision(12, 13, true);

			SetTheSurfaceHeights(parent);
		}

		/// <summary>
		/// Returns a state for transition. Return self if no transition.
		/// </summary>
		/// <returns>The next state.</returns>
		/// <param name="parent">Parent.</param>
		public override AbstractPlayerState GetNextState(MonoBehaviour parent)
		{
			AbstractPlayerController anton = GetPlayerController(parent);

			if (WillDig(parent))
				return m_DiggingState;

			if (m_HoldOffToggleOn)
			{
				if (InputMapping.jumpInput.GetButtonDown() && HoldDigRayCheck(parent, Vector3.down, rayDistance))
				{
					Jump(anton);
					PrematureDigExit();
					return m_MidairState;
				}
			}

			if (!m_HoldOffToggleOn)
			{
				if (!InputMapping.digInput.GetButton() && HoldDigRayCheck(parent, Vector3.down, rayDistance))
				{
					PrematureDigExit();

					if (anton.characterLocomotor.isGrounded)
						return m_StandingState;

					return m_MidairState;
				}
			}

			return base.GetNextState(parent);
		}

		public override void OnExit(MonoBehaviour parent)
		{
			Physics.IgnoreLayerCollision(12, 13, false); // Layer 13 - Diggable
		}

		#endregion

		#region Public Methods

		public static bool CanDig(MonoBehaviour parent)
		{
			AbstractPlayerController anton = GetPlayerController(parent);

			if (ColliderRaysMeshCheck(anton, Vector3.down, DuringDigAntonState.maxGroundCheckDistance))
				return true;

			return false;
		}

		public static bool WillDig(MonoBehaviour parent)
		{
			AbstractPlayerController anton = GetPlayerController(parent);
			RaycastHit hitInfo = anton.characterLocomotor.SkywardsCast();

			if (hitInfo.collider == null)
				return false;

			if (hitInfo.collider.GetComponent<DiggableMesh>() == null)
				return false;

			else
			{
				if (hitInfo.collider.GetComponent<DiggableMesh>().enabled != true)
					return false;
			}

			if (!DiggablePointsMeshCheck(parent))
				return false;

			if (willNotDigAgain)
				return false;

			return true;
		}

		/// <summary>
		/// 	Casts rays along the given direction. Returns true if all of them collide with a DiggableMesh.
		/// </summary>
		/// <returns><c>true</c>, if parent is above a DiggableMesh, <c>false</c> otherwise.</returns>
		/// <param name="parent">Parent.</param>
		/// <param name="direction">Direction.</param>
		/// <param name="distance">Distance.</param>
		public static bool ColliderRaysMeshCheck(MonoBehaviour parent, Vector3 direction, float distance)
		{
			AbstractPlayerController anton = GetPlayerController(parent);

			Vector3[] rayPositions = new Vector3[4];

			rayPositions[0] = new Vector3(anton.transform.position.x, anton.transform.position.y + m_RayPosModY,
										  (anton.transform.position.z + m_RayPosModZX));
			rayPositions[1] = new Vector3(anton.transform.position.x, anton.transform.position.y + m_RayPosModY,
										  (anton.transform.position.z - m_RayPosModZX));
			rayPositions[2] = new Vector3((anton.transform.position.x + m_RayPosModZX), anton.transform.position.y + m_RayPosModY,
										  anton.transform.position.z);
			rayPositions[3] = new Vector3((anton.transform.position.x - m_RayPosModZX), anton.transform.position.y + m_RayPosModY,
										  anton.transform.position.z);

			for (int index = 0; index < rayPositions.Length; index++)
			{
				RaycastHit hitInfo;
				if (!Physics.Raycast(rayPositions[index], direction, out hitInfo, distance))
					return false;

				float slidingThreshold = anton.characterLocomotor.characterController.slopeLimit;
				float surfaceAngle = Vector3.Angle(hitInfo.normal, Vector3.up);

				if (surfaceAngle > slidingThreshold)
					return false;

				DiggableMesh diggableMesh = hitInfo.collider.GetComponent<DiggableMesh>();

				if (diggableMesh != null && diggableMesh.enabled)
					continue;

				return false;
			}

			return true;
		}

		public static bool HoldDigRayCheck(MonoBehaviour parent, Vector3 direction, float distance)
		{
			AbstractPlayerController anton = GetPlayerController(parent);

			if (!Physics.Raycast(anton.transform.position, direction, distance))
				return false;

			return true;
		}

		public static bool DiggablePointsMeshCheck(MonoBehaviour parent)
		{
			AbstractPlayerController anton = GetPlayerController(parent);

			for (int i = 0; i < rayHitPoses.Length; i++)
			{
				if (anton.transform.position.y > (rayHitPoses[i].y - m_DigHitPointMod))
					return false;
			}

			return true;
		}

		public void PrematureDigExit()
		{
			willNotDigAgain = true;
		}

		public void SetTheSurfaceHeights(MonoBehaviour parent)
		{
			AbstractPlayerController anton = GetPlayerController(parent);

			Vector3[] rayosas = new Vector3[4];

			rayosas[0] = new Vector3(anton.transform.position.x, anton.transform.position.y + m_RayPosModY,
									 (anton.transform.position.z + m_RayPosModZX));
			rayosas[1] = new Vector3(anton.transform.position.x, anton.transform.position.y + m_RayPosModY,
									 (anton.transform.position.z - m_RayPosModZX));
			rayosas[2] = new Vector3((anton.transform.position.x + m_RayPosModZX), anton.transform.position.y + m_RayPosModY,
									 anton.transform.position.z);
			rayosas[3] = new Vector3((anton.transform.position.x - m_RayPosModZX), anton.transform.position.y + m_RayPosModY,
									 anton.transform.position.z);

			for (int index = 0; index < rayosas.Length; index++)
			{
				RaycastHit hitInfo;

				if (Physics.Raycast(rayosas[index], Vector3.down, out hitInfo, Mathf.Infinity))
					rayHitPoses[index] = hitInfo.point;
			}
		}

		#endregion
	}
}