﻿using AntonCoolpecker.Abstract.Player;
using AntonCoolpecker.Abstract.Player.States;
using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Player.Coolpecker.States
{
	/// <summary>
	/// 	StandingCoolpeckerState is Coolpecker's default state.
	/// </summary>
	public class StandingCoolpeckerState : AbstractStandingState
	{
		#region Variables

		[SerializeField] private SpinningCoolpeckerState m_SpinningState;
		[SerializeField] private DashChargingCoolpeckerState m_DashChargingState;
        [SerializeField] private SprintCoolpeckerState m_SprintCoolpeckerState;

		#endregion

		#region Override Methods

		/// <summary>
		/// Returns a state for transition. Return self if no transition.
		/// </summary>
		/// <returns>The next state.</returns>
		/// <param name="parent">Parent.</param>
        public override AbstractPlayerState GetNextState(MonoBehaviour parent)
		{
			AbstractPlayerController controller = GetPlayerController(parent);

            if (InputMapping.sprintInput.GetAxisRaw() != 0 &&
				parent.GetComponentInChildren<Animator>().GetFloat("InputMagnitude") >= m_SprintCoolpeckerState.AllowSprintMagnitude)
                return m_SprintCoolpeckerState;

            if (InputMapping.spinningInput.GetButtonDown())
				return m_SpinningState;

			if (controller.progress.unlockedDash && InputMapping.dashInput.GetButtonDown())
				return m_DashChargingState;

			return base.GetNextState(parent);
		}

		#endregion
	}
}