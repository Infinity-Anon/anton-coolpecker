﻿using AntonCoolpecker.Abstract.Player;
using AntonCoolpecker.Abstract.Player.States;
using AntonCoolpecker.Concrete.Player.Coolpecker.States.Hitstun;
using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using AntonCoolpecker.Concrete.HitBoxes;
using AntonCoolpecker.Concrete.Utils;
using AntonCoolpecker.Utils;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Player.Coolpecker.States
{
	/// <summary>
    /// The state Coolpecker will be in when he cancels his dash while in mid-air.
    /// </summary>
    public class MidairDashCancelCoolpeckerState : MidairCoolpeckerState
    {
		#region Variables

		[SerializeField] private MidairCoolpeckerState m_MidAirState;

		[SerializeField] private Timer m_StateTimer;

		[Tweakable("Midair Dash Cancel")] [SerializeField] internal float m_defaultTimeRemaining = 1f; //How long the cancel state lasts

		#endregion

		#region Override Functions

		/// <summary>
        /// Called when entering this state.
        /// </summary>
        /// <param name="parent"></param>
        public override void OnEnter(MonoBehaviour parent)
        {
            base.OnEnter(parent);
			m_StateTimer.maxTime = m_defaultTimeRemaining;
        }

		/// <summary>
		/// Gets a normalised vector that represents the player's directional input (in controller/camera space).
		/// </summary>
		/// <returns>The input vector.</returns>
		/// <param name="player">Player.</param>
        protected override Vector3 GetInputVector(AbstractPlayerController player)
        {
            Vector3 flatCamera = new Vector3(player.playerCamera.camera.transform.forward.x, 0,
                                                    player.playerCamera.camera.transform.forward.z);
            Vector3 fullBlast = Quaternion.Inverse(Quaternion.LookRotation(flatCamera)) * player.transform.forward;

            return fullBlast;
        }

        /// <summary>
        /// 	Returns a state for transition. Return self if no transition.
        /// </summary>
        /// <returns>The next state.</returns>
        /// <param name="parent">Parent.</param>
        public override AbstractPlayerState GetNextState(MonoBehaviour parent)
        {
			if (m_StateTimer.complete)
                return m_MidAirState;

            return base.GetNextState(parent);
        }

        /// <summary>
        /// Called when attempting to jump while in air.
        /// </summary>
        /// <param name="parent"></param>
        public override void HandleAirJump(MonoBehaviour parent)
        {
        }

        /// <summary>
        ///     Gets the timer to transition to another state.
        /// </summary>
        /// <value>The state timer.</value>
		public override Timer GetStateTimer(MonoBehaviour parent)
		{
            return m_StateTimer;
		}

		#endregion
    }
}