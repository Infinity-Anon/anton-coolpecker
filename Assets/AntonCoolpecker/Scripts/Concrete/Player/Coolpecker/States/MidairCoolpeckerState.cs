using AntonCoolpecker.Abstract.Player;
using AntonCoolpecker.Abstract.Player.States;
using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using AntonCoolpecker.Concrete.Player.Coolpecker.States.GroundPound;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Player.Coolpecker.States
{
	/// <summary>
	/// 	Midair coolpecker state.
	/// </summary>
	public class MidairCoolpeckerState : AbstractMidairState
	{
		#region Variables

		[SerializeField] private GlidingCoolpeckerState m_GlidingState;
		[SerializeField] private SpinningCoolpeckerState m_SpinningState;
		[SerializeField] private GroundPoundDownforceCoolpeckerState m_GroundpoundState;

		#endregion

		#region Override Methods

		/// <summary>
		/// 	Returns a state for transition. Return self if no transition.
		/// </summary>
		/// <returns>The next state.</returns>
		/// <param name="parent">Parent.</param>
		public override AbstractPlayerState GetNextState(MonoBehaviour parent)
		{
			AbstractPlayerController controller = GetPlayerController(parent);

			if (controller.progress.unlockedGlide && InputMapping.glidingInput.GetButtonDown())
				return m_GlidingState;

			if (InputMapping.spinningInput.GetButtonDown())
				return m_SpinningState;

			if (controller.progress.unlockedPound && InputMapping.groundPoundInput.GetButtonDown())
				return m_GroundpoundState;

			return base.GetNextState(parent);
		}

		#endregion
	}
}