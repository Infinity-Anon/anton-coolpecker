﻿using AntonCoolpecker.Abstract.Player;
using AntonCoolpecker.Abstract.Player.States;
using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using AntonCoolpecker.Concrete.HitBoxes;
using AntonCoolpecker.Concrete.Player.Coolpecker.States.GroundPound;
using AntonCoolpecker.Concrete.Player.Coolpecker.States.Hitstun;
using AntonCoolpecker.Utils;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Player.Coolpecker.States
{
	/// <summary>
	/// Midair dashing Coolpecker state - Handles the player's dash while midair.
	/// </summary>
	public class MidairDashingCoolpeckerState : AbstractDashingState
	{
		#region Variables

		[SerializeField] private MidairCoolpeckerState m_MidairState;
		[SerializeField] private AbstractSwimmingState m_SwimmingState;
		[SerializeField] private GlidingCoolpeckerState m_GlidingState;
		[SerializeField] private GroundPoundDownforceCoolpeckerState m_GroundpoundState;
		[SerializeField] private DashStunCoolpeckerState m_DashStunState;
		[SerializeField] private MidairDashCancelCoolpeckerState m_MidairCancelState;
		[SerializeField] private DashingCoolpeckerState m_DashingState;

		//TODO: add strong dash and other states
		///<not yet implimented>[SerializeField] private StrongDashingCoolpeckerState m_StrongDashingState;

		[Tweakable("AirDash")] [SerializeField] private float m_MaxSpeedMultiplier = 3.0f;
		[Tweakable("AirDash")] [SerializeField] private float m_AccelerationMultiplier = 2.0f;
		[Tweakable("AirDash")] [SerializeField] private float m_RotateSpeedMultiplier = 0.5f;
		[Tweakable("AirDash")] [SerializeField] private float m_MinAngleDif = 175f; //The minimal angle difference required to cause a hitstun
		[Tweakable("AirDash")] [SerializeField] private float m_RaycastCheckDist = 0.8f; //The distance the raycast will check to see if it runs into anything.

        [SerializeField] private float m_CurrentDashTime = 0.0f; //The amount of time Coolpecker will continue to dash. It is informed by the DashCharging state.

		#endregion

		#region Properties

		protected override float maxSpeedMultiplier { get { return m_MaxSpeedMultiplier; } }
		protected override float accelerationMultiplier { get { return m_AccelerationMultiplier; } }
		protected override float rotateSpeedMultiplier { get { return m_RotateSpeedMultiplier; } }

        #endregion

        #region Override Methods

		/// <summary>
		/// Called when the parent updates.
		/// </summary>
		/// <param name="parent">Parent.</param>
        public override void OnUpdate(MonoBehaviour parent)
        {
            m_CurrentDashTime -= Time.deltaTime;

            base.OnUpdate(parent);
        }

        /// <summary>
        /// 	Called when the state becomes active.
        /// </summary>
        /// <param name="parent">Parent.</param>
        public override void OnEnter(MonoBehaviour parent)
		{
			base.OnEnter(parent);

			HitBox hb = GetHitBox(parent);
			AbstractPlayerController player = GetPlayerController(parent);
			hb.gameObject.SetActive(true);

			// Ignore our own collider so CP does not hit himself. Has to be done every time hb is reactivated
			Physics.IgnoreCollision(player.collider, hb.collider, true);
		}

		/// <summary>
		/// 	Called before another state becomes active.
		/// </summary>
		public override void OnExit(MonoBehaviour parent)
		{
			base.OnExit(parent);

            m_CurrentDashTime = 0;

			if (animatorState != "")
				SetAnimatorState ("");

			GetHitBox(parent).gameObject.SetActive(false);
		}

        /// <summary>
        /// 	Returns a state for transition. Return self if no transition.
        /// </summary>
        /// <returns>The next state.</returns>
        /// <param name="parent">Parent.</param>
        public override AbstractPlayerState GetNextState(MonoBehaviour parent)
        {
            AbstractPlayerController player = GetPlayerController(parent);

            if (shouldDashCancel(parent, player))
            {
                return m_MidairCancelState;
            }

            Vector3 higherPos = parent.transform.position + new Vector3(0, 1, 0);
            Ray ray = new Ray(higherPos, parent.transform.forward);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, m_RaycastCheckDist) && !hit.collider.GetComponent<Collectables.Collectable>())
            {
                Vector3 hitDir = hit.point - higherPos;
                float pointAngle = Vector3.Angle(hitDir, hit.normal);

                if (pointAngle >= m_MinAngleDif)
                {
                    player.characterLocomotor.velocity = Vector3.zero;
                    return m_DashStunState;
                }
            }

            //TODO: Add nicer slowdown
            if (m_CurrentDashTime <= 0)
            {
                m_DashingState.setDashTime(m_CurrentDashTime);
                return m_MidairState;
            }

            if (player.isInWater)
            {
                m_DashingState.setDashTime(0);
                return m_SwimmingState;
            }

            if (player.characterLocomotor.isGrounded)
            {
                m_DashingState.setDashTime(m_CurrentDashTime);
                return m_DashingState;
            }

            if (InputMapping.groundPoundInput.GetButtonDown())
            {
                m_DashingState.setDashTime(0);
                return m_GroundpoundState;
            }

			return base.GetNextState(parent);
		}

        #endregion

        #region Public Methods

		/// <summary>
		/// Sets the dash time.
		/// </summary>
		/// <param name="time">The new dash time value.</param>
        public void setDashTime(float time)
        {
            m_CurrentDashTime = time;
        }

        #endregion
    }
}