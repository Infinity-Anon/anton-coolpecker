﻿using AntonCoolpecker.Abstract.Player;
using AntonCoolpecker.Abstract.Player.States;
using AntonCoolpecker.Concrete.Player.Coolpecker.States.Hitstun;
using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using AntonCoolpecker.Concrete.HitBoxes;
using AntonCoolpecker.Utils;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Player.Coolpecker.States
{
	/// <summary>
	/// 	DashingCoolpeckerState is used while Coolpecker is dashing. 
	/// </summary>
	public class DashingCoolpeckerState : AbstractDashingState
	{
		#region Variables

		[SerializeField] private MidairDashingCoolpeckerState m_MidairDashingState;
		[SerializeField] private AbstractSwimmingState m_SwimmingState;
		[SerializeField] private StandingCoolpeckerState m_StandingState;
		[SerializeField] private SlidingCoolpeckerState m_SlidingState;
		[SerializeField] private DashStunCoolpeckerState m_DashStunState;
		[SerializeField] private DashCancelCoolpeckerState m_DashCancelState;

		[Tweakable("Dash")] [SerializeField] protected float m_MaxSpeedMultiplier = 3.0f;
		[Tweakable("Dash")] [SerializeField] private float m_AccelerationMultiplier = 2.0f;
		[Tweakable("Dash")] [SerializeField] private float m_RotateSpeedMultiplier = 0.5f;
		[Tweakable("Dash")] [SerializeField] private float m_MinAngleDif = 175f; //The minimial angle difference required to cause a hitstun
		[Tweakable("Dash")] [SerializeField] private float m_RaycastCheckDist = 0.8f; //The distance the raycast will check to see if it runs into anything.
		[Tweakable("Dash")] [SerializeField] protected float m_DashDesireTime = 5f; //The max time the dash can last

        [SerializeField] private float m_CurrentDashTime = 0.0f; //The amount of time Coolpecker will continue to dash. It is informed by the DashCharging state.

        private bool dashCancel;

		#endregion

		#region Properties

		protected override float maxSpeedMultiplier { get { return m_MaxSpeedMultiplier; } }
		protected override float accelerationMultiplier { get { return m_AccelerationMultiplier; } }
		protected override float rotateSpeedMultiplier { get { return m_RotateSpeedMultiplier; } }

        #endregion

        #region Public Methods

		/// <summary>
		/// Sets the dash time(Used from the charge state).
		/// </summary>
		/// <param name="time">Time.</param>
        public void setDashTimeFromCharge(float time)
        {
            m_CurrentDashTime = time * m_DashDesireTime;
        }

		/// <summary>
		/// Sets the dash time.
		/// </summary>
		/// <param name="time">Time.</param>
        public void setDashTime(float time)
        {
            m_CurrentDashTime = time;
        }

        #endregion

        #region Override Methods

        /// <summary>
        /// 	Called when the state becomes active.
        /// </summary>
        /// <param name="parent">Parent.</param>
        public override void OnEnter(MonoBehaviour parent)
		{
			base.OnEnter(parent);

			HitBox hb = GetHitBox(parent);
			AbstractPlayerController player = GetPlayerController(parent);
			hb.gameObject.SetActive(true);

			// Ignore our own collider so CP does not hit himself. Has to be done every time hb is reactivated
			Physics.IgnoreCollision(player.collider, hb.collider, true);
		}

		/// <summary>
		/// 	Called before another state becomes active.
		/// </summary>
		public override void OnExit(MonoBehaviour parent)
		{
			base.OnExit(parent);

            m_CurrentDashTime = 0;
			GetHitBox(parent).gameObject.SetActive(false);
		}

		/// <summary>
		/// 	Returns a state for transition. Return self if no transition.
		/// </summary>
		/// <returns>The next state.</returns>
		/// <param name="parent">Parent.</param>
		public override AbstractPlayerState GetNextState(MonoBehaviour parent)
		{
            AbstractPlayerController player = GetPlayerController(parent);
            
            if (shouldDashCancel(parent, player))
            {
                return m_DashCancelState;
            }

            Vector3 higherPos = parent.transform.position + new Vector3(0, 1, 0);
            Ray ray = new Ray(higherPos, parent.transform.forward);
            RaycastHit hit;

            LayerMask layerToIgnore = 1 << 4; //IGNORE WATER LAYER
            layerToIgnore |= 1 << 12; //IGNORE PLAYER LAYER
            layerToIgnore |= 1 << 18; //IGNORE ATTACK HITBOX LAYER

            if (Physics.Raycast(ray, out hit, m_RaycastCheckDist, ~layerToIgnore) && !hit.collider.GetComponent<Collectables.Collectable>())
            {
                Vector3 hitDir = hit.point - higherPos;
                float pointAngle = Vector3.Angle(hitDir, hit.normal);
                if (pointAngle >= m_MinAngleDif)
                {
                    player.characterLocomotor.velocity = Vector3.zero;
                    return m_DashStunState;
                }
            }

            if (!player.characterLocomotor.isSoftGrounded)
			{
                if (!player.isInWater)
                {
                    m_MidairDashingState.setDashTime(m_CurrentDashTime);
                    return m_MidairDashingState;
                }

				return m_SwimmingState;
			}

            //TODO: Add nicer slowdown
            if (m_CurrentDashTime <= 0)
            {
                if (m_LedgeCancel)
                    return m_DashCancelState;
                return m_StandingState;
            }

			if (AbstractSlidingState.ShouldStartSliding (parent)) 
			{
				//TODO: The player's velocity gets very high due to the interpolation system,
				//so even if the scale is set back to normal, the velocity from when the scale
				//differed is still left. Set the player's velocity back to normal in a smoother manner
				player.characterLocomotor.velocity = Vector3.zero;
				return m_SlidingState;
			}

			return base.GetNextState(parent);
		}

		/// <summary>
		/// 	Called when the parent updates.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnUpdate(MonoBehaviour parent)
		{
			base.OnUpdate(parent);
            
            if(m_CurrentDashTime > 0)
                m_CurrentDashTime -= Time.deltaTime;

			AbstractPlayerController player = GetPlayerController(parent);

			if (InputMapping.jumpInput.GetButtonDown ()) 
			{
				if (m_InterpolateGravityScaleY) 
				{
					setGravityScaleBack (player, player.characterLocomotor.originalGravityScale.y);
					m_UseLinearCalc = false;
				}

				Jump (player);
				m_MidairDashingState.SetAnimatorState ("Jump");
			}
		}

		/// <summary>
        /// Sets dashCancel to true if a ledge is detected.
        /// </summary>
        /// <param name="parent"></param>
        public override void LedgeDetected(MonoBehaviour parent)
        {
        	AbstractPlayerController player = GetPlayerController(parent);
        	m_CurrentDashTime = 0;
        	player.characterLocomotor.velocity = Vector3.zero;

        	dashCancel = true;
        }

        #endregion
    }
}