﻿using AntonCoolpecker.Abstract.Player.States;

namespace AntonCoolpecker.Concrete.Player.Coolpecker.States
{
	/// <summary>
	/// Dead Coolpecker state - When Coolpecker dies ingame.
	/// </summary>
	public class DeadCoolpeckerState : AbstractDeadState {}
}