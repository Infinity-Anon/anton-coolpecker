﻿using AntonCoolpecker.Abstract.Player;
using AntonCoolpecker.Abstract.Player.States;
using System.Collections;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Player.Coolpecker.States
{
    /// <summary>
    /// Coolpecker midair sprinting state
    /// </summary>
    public class MidairSprintCoolpeckerState : AbstractMidairSprintState {}
}