using AntonCoolpecker.Abstract.Player.States;

namespace AntonCoolpecker.Concrete.Player.Coolpecker.States
{
	/// <summary>
	/// Sliding Coolpecker state.
	/// </summary>
	public class SlidingCoolpeckerState : AbstractSlidingState {}
}