﻿using AntonCoolpecker.Abstract.Player;
using AntonCoolpecker.Abstract.Player.States;
using AntonCoolpecker.Concrete.Player.Coolpecker.States.Hitstun;
using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using AntonCoolpecker.Concrete.HitBoxes;
using AntonCoolpecker.Concrete.Utils;
using AntonCoolpecker.Utils;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Player.Coolpecker.States
{
	/// <summary>
	/// Dash cancel Coolpecker state - When the player has chosen to cancel
	/// the dash by turning 180 degrees while dashing.
	/// </summary>
    public class DashCancelCoolpeckerState : StandingCoolpeckerState
    {
		#region Variables

		[Tweakable("Dash Cancel")] [SerializeField] internal float m_LedgeDetectionDistance; //Distance until a ledge will be detected
		[Tweakable("Dash Cancel")] [SerializeField] internal bool m_UseLedgeDetection = true; //Whether to detect ledges or not
		[Tweakable("Dash Cancel")] [SerializeField] internal bool m_UseSlowDown = true; //Whether to slow down the dash cancel velocity or not
        [Tweakable("Dash Cancel")] [SerializeField] internal bool m_GoOutEdgeWhenNoInput = true; //Whether to allow the player to go out any edges as the dash is cancelled or not

		[Tweakable("Dash Cancel")] [SerializeField] internal float m_defaultTimeRemaining; //Set remaining time of dash cancel state
        [Tweakable("Dash Cancel")] [SerializeField] internal float m_percentVelocityFallBegin; //The percent of the total cancel time spent before velocity begins to decrease.
		[Tweakable("Dash Cancel")] [SerializeField] protected float m_BaseSpeedMultiplier; //Max speed multiplier

		[SerializeField] private StandingCoolpeckerState m_StandingState;
		[SerializeField] private Timer m_StateTimer;

        private float m_CurrentSpeedMultiplier;
        private float m_MaxSpeedMultiplier;
        private float m_SpeedChangeDistance;

		#endregion

        #region Properties

		/// <summary>
		/// Gets the max speed multiplier.
		/// </summary>
		/// <value>The max speed multiplier.</value>
        protected override float maxSpeedMultiplier { get { return m_MaxSpeedMultiplier; } }

        #endregion

		#region Override Methods

		/// <summary>
		/// Called when the state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
        public override void OnEnter(MonoBehaviour parent)
        {
            base.OnEnter(parent);
            m_SpeedChangeDistance = m_defaultTimeRemaining * (1 - m_percentVelocityFallBegin);
            m_StateTimer.maxTime = m_defaultTimeRemaining;
        }

		/// <summary>
		/// Returns a state for transition. Return self if no transition.
		/// </summary>
		/// <returns>The next state.</returns>
		/// <param name="parent">Parent.</param>
        public override AbstractPlayerState GetNextState(MonoBehaviour parent)
        {
			if (m_StateTimer.complete)
                return m_StandingState;

            return base.GetNextState(parent);
        }

		/// <summary>
		/// Called when the parent's physics update.
		/// </summary>
		/// <param name="parent">Parent.</param>
        public override void OnFixedUpdate(MonoBehaviour parent)
        {
            base.OnFixedUpdate(parent);

            AbstractPlayerController player = GetPlayerController(parent);
            
            if (m_UseLedgeDetection)
            {
                bool playerIsHoldingBack = Input.GetAxis("Vertical") < 0;
                RaycastHit hitInfo;
                Vector3 rayPosition = new Vector3(player.transform.position.x, (player.transform.position.y + player.collider.bounds.size.y - 0.1f), player.transform.position.z);
                Vector3 direction = Vector3.down;
                float distance = m_GravityRayMaxDistance;
                LayerMask layerToIgnore = 1 << 4; //IGNORE WATER LAYER
                layerToIgnore |= 1 << 12; //IGNORE PLAYER LAYER
                layerToIgnore |= 1 << 18; //IGNORE ATTACK HITBOX LAYER
                bool canDo = Physics.Raycast(rayPosition, direction, out hitInfo, distance, ~layerToIgnore);

                RaycastHit hitInfo2;
                Vector3 secondaryRayPos = new Vector3(player.transform.position.x, (player.transform.position.y + player.collider.bounds.size.y - 0.1f), player.transform.position.z)
                                                    + player.characterLocomotor.velocity * Time.fixedDeltaTime;
                bool ledgeUndetected = Physics.Raycast(secondaryRayPos, direction, out hitInfo2, m_LedgeDetectionDistance, ~layerToIgnore);
                if (!ledgeUndetected)
                {
                    if (!m_GoOutEdgeWhenNoInput)
                        player.characterLocomotor.velocity = Vector3.zero;
                    else if (playerIsHoldingBack)
                        player.characterLocomotor.velocity = Vector3.zero;
                }
			}
        }

		/// <summary>
		/// Gets a normalised vector that represents the player's directional input (in controller/camera space).
		/// </summary>
		/// <returns>The input vector.</returns>
		/// <param name="player">Player.</param>
        protected override Vector3 GetInputVector(AbstractPlayerController player)
        {
            Vector3 flatCamera = new Vector3(player.playerCamera.camera.transform.forward.x, 0,
                                                    player.playerCamera.camera.transform.forward.z);
            Vector3 fullBlast = Quaternion.Inverse(Quaternion.LookRotation(flatCamera)) * player.transform.forward;

            return fullBlast;
        }

		/// <summary>
		/// Called when the parent updates.
		/// </summary>
		/// <param name="parent">Parent.</param>
        public override void OnUpdate(MonoBehaviour parent)
        {
            if (m_UseSlowDown && m_StateTimer.elapsedAsPercentage >= m_percentVelocityFallBegin)
            {
                float maxSpeedPercent = 1 - (m_StateTimer.elapsedAsPercentage - m_percentVelocityFallBegin) / m_SpeedChangeDistance;
                m_MaxSpeedMultiplier = m_BaseSpeedMultiplier * maxSpeedPercent;
            }
            else
            {
                m_MaxSpeedMultiplier = m_BaseSpeedMultiplier;
            }
        }

        /// <summary>
        ///     Gets the timer to transition to another state.
        /// </summary>
        /// <value>The state timer.</value>
		public override Timer GetStateTimer(MonoBehaviour parent)
		{
            return m_StateTimer;
		}

		#endregion
    }
}