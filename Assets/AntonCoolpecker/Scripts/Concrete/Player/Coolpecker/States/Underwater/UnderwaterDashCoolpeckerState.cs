﻿using AntonCoolpecker.Abstract.Player.States.Underwater;

namespace AntonCoolpecker.Concrete.Player.Coolpecker.States.Underwater
{
	/// <summary>
	/// Underwater dash Coolpecker state - When Coolpecker dashes underwater.
	/// </summary>
	public class UnderwaterDashCoolpeckerState : AbstractUnderwaterDashState {}
}