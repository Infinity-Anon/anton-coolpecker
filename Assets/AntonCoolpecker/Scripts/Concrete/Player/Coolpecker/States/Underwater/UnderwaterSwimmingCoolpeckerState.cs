﻿using AntonCoolpecker.Abstract.Player.States.Underwater;

namespace AntonCoolpecker.Concrete.Player.Coolpecker.States.Underwater
{
	/// <summary>
	/// Underwater swimming Coolpecker state - When Coolpecker is swimming underwater.
	/// </summary>
	public class UnderwaterSwimmingCoolpeckerState : AbstractUnderwaterSwimmingState {}
}