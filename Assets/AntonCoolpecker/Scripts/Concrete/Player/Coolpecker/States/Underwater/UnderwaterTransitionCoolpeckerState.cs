﻿using AntonCoolpecker.Abstract.Player.States.Underwater;

namespace AntonCoolpecker.Concrete.Player.Coolpecker.States.Underwater
{
	/// <summary>
	/// Underwater transition Coolpecker state - When Coolpecker transitions to/from the underwater state.
	/// </summary>
	public class UnderwaterTransitionCoolpeckerState : AbstractUnderwaterTransitionState {}
}
