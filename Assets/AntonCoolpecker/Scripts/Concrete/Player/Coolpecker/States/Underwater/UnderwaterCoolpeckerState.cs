﻿using AntonCoolpecker.Abstract.Player.States.Underwater;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Player.Coolpecker.States.Underwater
{
	/// <summary>
	/// Underwater Coolpecker state - When Coolpecker is underwater.
	/// </summary>
	public class UnderwaterCoolpeckerState : AbstractUnderwaterState
	{
		#region Variables

		//[SerializeField] private bool m_CanFloatUpwards = false;
		[SerializeField] private float m_UpwardsFloat = 0.8f;

		#endregion

		#region Properties

		/// <summary>
		/// 	Gets or sets the upwards float for Coolpecker(Floating slowly upwards).
		/// </summary>
		/// <value>The upwards float.</value>
		public float upwardsFloat { get { return m_UpwardsFloat; } set { m_UpwardsFloat = value; } }

		#endregion

		#region Override Functions

		public override void OnUpdate(MonoBehaviour parent)
		{
			base.OnUpdate(parent);

			//AbstractPlayerController player = GetPlayerController(parent);

			//player.transform.position.y += m_UpwardsFloat;

			//if (m_CanFloatUpwards)
			//player.transform.position.y = 1f * Time.deltaTime;
		}

		#endregion
	}
}