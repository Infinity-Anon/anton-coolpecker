﻿using AntonCoolpecker.Abstract.Player.States.Underwater;

namespace AntonCoolpecker.Concrete.Player.Coolpecker.States.Underwater
{
	/// <summary>
	/// Underwater attack Coolpecker state - When Coolpecker attacks underwater.
	/// </summary>
	public class UnderwaterAttackCoolpeckerState : AbstractUnderwaterAttackState {}
}