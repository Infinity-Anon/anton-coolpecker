﻿using AntonCoolpecker.Abstract.Player;
using AntonCoolpecker.Abstract.Player.States;
using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using AntonCoolpecker.Concrete.HitBoxes;
using AntonCoolpecker.Utils;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Player.Coolpecker.States
{
    /// <summary>
    /// 	DashChargingCoolpeckerState is used while Coolpecker is charging his dash. 
    /// </summary>
    public class DashChargingCoolpeckerState : AbstractDashChargingState
    {
		#region Variables

        [SerializeField] private MidairDashingCoolpeckerState m_MidairDashingState;
		[SerializeField] private AbstractSwimmingState m_SwimmingState;
        [SerializeField] private DashingCoolpeckerState m_DashingState;
        [SerializeField] private StandingCoolpeckerState m_StandingState;
        [SerializeField] private SlidingCoolpeckerState m_SlidingState;
        [SerializeField] private DashChargingCoolpeckerState m_FullChargeState;

		#endregion

        #region Override Methods

		/// <summary>
		/// Rotates towards movement vector.
		/// </summary>
		/// <param name="player">Player.</param>
		/// <param name="movement">Movement.</param>
        protected override void RotateTowardsMovementVector(AbstractPlayerController player, Vector3 movement)
        {
            Quaternion lookRotation = Quaternion.LookRotation(player.transform.forward);

            if (movement == Vector3.zero)
                return;

            Quaternion targetRotation = Quaternion.LookRotation(movement);
            float delta = player.rotateSpeed * m_RotateSpeedMultiplier * Time.deltaTime;

            player.transform.rotation = Quaternion.RotateTowards(lookRotation, targetRotation, delta);
        }

		/// <summary>
		/// Resolves the character's rotation due to the movement that just occurred in the current frame.
		/// </summary>
		/// <param name="player">The player character that just moved and needs to rotate.</param>
		/// <param name="movement">The normalised movement vector input for this frame, in world space.</param>
        protected override void ResolveRotation(AbstractPlayerController player, Vector3 movement)
        {
            RotateTowardsMovementVector(player, movement);
        }

		/// <summary>
		/// Handles the movement.
		/// </summary>
		/// <param name="parent">Parent.</param>
        protected override void HandleMovement(MonoBehaviour parent)
        {
            AbstractPlayerController player = GetPlayerController(parent);

			if (InputMapping.verticalInput.GetAxisRaw () < 0f && InputMapping.horizontalInput.GetAxisRaw() == 0f)
				return;

            Vector3 desiredMovement = GetMovementVector(parent);
            ResolveRotation(player, desiredMovement);

            Vector3 deltaV = ResolveVelocityDelta(player, desiredMovement, player.characterLocomotor.velocity);
            player.characterLocomotor.velocity = Vector3.zero;
        }

		/// <summary>
		/// Called before another state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
        public override void OnExit(MonoBehaviour parent)
        {
            m_CurrentChargeTime = 0;
			m_ChargeFull = false;
            base.OnExit(parent);
        }

		/// <summary>
		/// Returns a state for transition. Return self if no transition.
		/// </summary>
		/// <returns>The next state.</returns>
		/// <param name="parent">Parent.</param>
        public override AbstractPlayerState GetNextState(MonoBehaviour parent)
        {
            AbstractPlayerController player = GetPlayerController(parent);

			if (m_ChargeFull && m_FullChargeState != null)
            {
                m_FullChargeState.m_CurrentChargeTime = m_CurrentChargeTime;
                return m_FullChargeState;
            }

            if (!player.characterLocomotor.isSoftGrounded)
            {
                if (!player.isInWater)
                    return m_MidairDashingState;
                    
                return m_SwimmingState;
            }

            if (!InputMapping.dashInput.GetButton())
            {
                if (InputMapping.verticalInput.GetAxis() > 0)
                {
                    m_DashingState.setDashTimeFromCharge(m_CurrentChargeTime / m_MaxChargeTime);
                    return m_DashingState;
                }
                else
                    return m_StandingState;
            }

            if (AbstractSlidingState.ShouldStartSliding(parent))
            {
                return m_SlidingState;
            }
            
            return base.GetNextState(parent);
        }

        #endregion
    }
}
