﻿using AntonCoolpecker.Abstract.Player.States;

namespace AntonCoolpecker.Concrete.Player.Coolpecker.States
{
	/// <summary>
	/// Swimming Coolpecker state.
	/// </summary>
	public class SwimmingCoolpeckerState : AbstractSwimmingState {}
}