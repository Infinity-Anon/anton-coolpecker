﻿using AntonCoolpecker.Abstract.Player;
using AntonCoolpecker.Abstract.Player.States;
using AntonCoolpecker.Concrete.Listeners.Player.Coolpecker;
using AntonCoolpecker.Concrete.Utils;
using AntonCoolpecker.Utils;
using AntonCoolpecker.Utils.Time;
using Hydra.HydraCommon.Utils;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Player.Coolpecker.States
{
	/// <summary>
	/// Spinning coolpecker state.
	/// </summary>
	public class SpinningCoolpeckerState : AbstractAttackState
	{
		#region Variables

		[SerializeField] private Timer m_StateTimer;
		[SerializeField] private AbstractPlayerState m_TimerState;

		[SerializeField] [Tweakable("Spin")] private float m_HoverForce = 30f; //Should be at least as much as gravity, which is ~20
		[SerializeField] [Tweakable("Spin")] private bool m_DoNotHoverOnGround = true;
		[SerializeField] [Tweakable("Spin")] private float m_MaxTime = 0.5f;

		private HitBoxes.HitBox m_HitBox;

		#endregion

		#region Properties

		/// <summary>
		/// 	Gets the state to transition to after the timer elapses.
		/// </summary>
		/// <value>The state of the timer.</value>
		protected override AbstractPlayerState timerState { get { return m_TimerState; } }

		#endregion

		#region Override Functions/Methods

		/// <summary>
		/// Called when the state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnEnter(MonoBehaviour parent)
		{
			base.OnEnter(parent);

			m_StateTimer.maxTime = m_MaxTime;
			m_HitBox = GetHitBox(parent);
            m_HitBox.gameObject.SetActive(true);
		}

		/// <summary>
		/// Called before another state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnExit(MonoBehaviour parent)
		{
			base.OnExit(parent);

			AbstractPlayerController player = GetPlayerController(parent);
			player.airTracker.canSpinHover = false;
			m_HitBox.gameObject.SetActive(false);
		}

		/// <summary>
		/// Gets the change in velocity, given the current velocity, desired final velocity, and max acceleration.
		/// </summary>
		/// <param name="player">The player.</param>
		/// <param name="desiredV">The desired velocity, according to the character's max speed and input direction.</param>
		/// <param name="currentV">The character's current velocity.</param>
		/// <returns>The change in velocity for this step.</returns>
		protected override Vector3 ResolveVelocityDelta(AbstractPlayerController player, Vector3 desiredV, Vector3 currentV)
		{
			// Apply a vertical force to make Coolpecker hover slightly when spinning for the first time in the air
			float deltaY = 0f;

			if (player.airTracker.canSpinHover) 
			{
				float maxY = currentV.y + player.characterLocomotor.gravity.y * GameTime.deltaTime;
				deltaY = m_HoverForce * GameTime.deltaTime;
				deltaY = HydraMathUtils.Clamp(deltaY, 0, HydraMathUtils.Abs(maxY)) * -Mathf.Sign(maxY);
			}

			Vector3 delta = base.ResolveVelocityDelta(player, desiredV, currentV);
			delta.y += deltaY;

			if (player.characterLocomotor.isGrounded && m_DoNotHoverOnGround)
				delta.y = 0;

			return delta;
		}

        /// <summary>
        ///     Gets the timer to transition to another state.
        /// </summary>
        /// <value>The state timer.</value>
		public override Timer GetStateTimer(MonoBehaviour parent)
		{
            return m_StateTimer;
		}

		#endregion
	}
}