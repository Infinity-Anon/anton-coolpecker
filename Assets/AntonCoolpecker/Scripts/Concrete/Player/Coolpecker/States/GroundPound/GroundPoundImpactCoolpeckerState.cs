﻿using AntonCoolpecker.Abstract.Player.States;
using AntonCoolpecker.Concrete.Utils;
using AntonCoolpecker.Utils;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Player.Coolpecker.States.GroundPound
{
	/// <summary>
	/// 	Second stage of the groundpound - Dealing damage when landing and recovering from the groundpound.
	/// </summary>
	public class GroundPoundImpactCoolpeckerState : AbstractAttackState
	{
		//TODO: MAKE BONUS DAMAGE MECHANIC MORE DYNAMIC(SCALE BONUS DAMAGE BASED ON HEIGHT)
		//TODO: FIX PROPER ANIMATION FOR GROUNDPOUND IMPACT
		//TODO: FIX SO COLLIDER PLACES ITSELF ACCORDING TO SURFACE ROTATION OR IMPLEMENT ALTERNATIVE COLLISION
		//      THAT COLLIDES WITH ALL ENEMIES ON THE GROUND WITHIN A SPECIFIC RADIUS

		#region Variables

		[Tweakable("Groundpound")] [SerializeField] private float m_BaseDamage = 3;
		[Tweakable("Groundpound")] [SerializeField] private float m_BonusDamage = 3;
		[Tweakable("Groundpound")] [SerializeField] private float m_BonusDamageHeight = 10;
		[Tweakable("Groundpound")] [SerializeField] private float m_MaxTime = 0.5f;

		[SerializeField] private Timer m_StateTimer;
		[SerializeField] private AbstractPlayerState m_TimerState;

		private Component m_HitBox;

		#endregion

		#region Properties

		/// <summary>
		/// 	Gets the state to transition to after the timer elapses.
		/// </summary>
		/// <value>The state of the timer.</value>
		protected override AbstractPlayerState timerState { get { return m_TimerState; } }

		/// <summary>
		/// Gets or sets the starting height.
		/// </summary>
		/// <value>The starting height.</value>
		public float startingHeight { get; set; }

		#endregion

		#region Override Functions/Methods

		/// <summary>
		/// Called when the state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnEnter(MonoBehaviour parent)
		{
			base.OnEnter(parent);

			m_StateTimer.maxTime = m_MaxTime;

			if (startingHeight >= m_BonusDamageHeight)
				damage = (int)(m_BaseDamage + m_BonusDamage); //If we ground pound from a specified height or greater, deal bonus damage.
			else
				damage = (int)m_BaseDamage;

			m_HitBox = GetHitBox(parent);
			m_HitBox.gameObject.SetActive(true);
		}

		/// <summary>
		/// Called before another state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnExit(MonoBehaviour parent)
		{
			base.OnExit(parent);
			m_HitBox.gameObject.SetActive(false);
		}

        /// <summary>
        ///     Gets the timer to transition to another state.
        /// </summary>
        /// <value>The state timer.</value>
		public override Timer GetStateTimer(MonoBehaviour parent)
		{
            return m_StateTimer;
		}

		#endregion
	}
}