﻿using AntonCoolpecker.Abstract.Player;
using AntonCoolpecker.Abstract.Player.States;
using AntonCoolpecker.Concrete.Player.Coolpecker.States.Underwater;
using AntonCoolpecker.Utils;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Player.Coolpecker.States.GroundPound
{
	/// <summary>
	/// 	First stage of the ground pound, pushing the player to the ground.
	/// </summary>
	public class GroundPoundDownforceCoolpeckerState : AbstractPlayerState
	{
		#region Variables

		[Tweakable("Groundpound")] [SerializeField] private float m_DownForce = 35;

		[SerializeField] private GroundPoundImpactCoolpeckerState m_ImpactState;
		[SerializeField] private UnderwaterCoolpeckerState m_UnderwaterState;
		[SerializeField] private UnderwaterTransitionCoolpeckerState m_UnderwaterTransitionState;
		[SerializeField] private SwimmingCoolpeckerState m_SwimmingState;
		[SerializeField] private StandingCoolpeckerState m_StandingState;

		private RaycastHit m_Hit;

		#endregion

		#region Override Methods

		public override void OnEnter(MonoBehaviour parent)
		{
			base.OnEnter(parent);

			AbstractPlayerController player = GetPlayerController(parent);
			player.characterLocomotor.velocity = Vector3.down * m_DownForce;

			m_UnderwaterTransitionState.CurrentDownforcePosition (player.transform.position.y);

			if (Physics.Raycast(player.transform.position,
				player.transform.TransformDirection(Vector3.down),
				out m_Hit,
				Mathf.Infinity))
				m_ImpactState.startingHeight = m_Hit.distance; //Get the distance from coolpecker to the ground.
		}

		/// <summary>
		/// 	 a state for transition. Return self if no transition.
		/// </summary>
		/// <returns>The next state.</returns>
		/// <param name="parent">Parent.</param>
		public override AbstractPlayerState GetNextState(MonoBehaviour parent)
		{
			AbstractPlayerController player = GetPlayerController(parent);

			if (player.characterLocomotor.isGrounded) 
			{
				if (player.characterLocomotor.GroundedCast().collider.gameObject.tag == "Mushroom")
					return m_StandingState;
				
				return m_ImpactState;
			}

			if (player.isInWater)
			{
				if (player.waterTrigger.allowUnderwater && m_UnderwaterTransitionState.DownUnder(player))
				{
					m_UnderwaterTransitionState.GroundPoundAirDig();
					m_UnderwaterTransitionState.SwimIn();
					return m_UnderwaterTransitionState;
				}

				return m_SwimmingState;
			}

			return base.GetNextState(parent);
		}

		#endregion
	}
}