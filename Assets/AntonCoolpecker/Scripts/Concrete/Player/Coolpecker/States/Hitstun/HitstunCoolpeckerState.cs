using AntonCoolpecker.Abstract.Player.States.Hitstun;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Player.Coolpecker.States.Hitstun
{
	/// <summary>
	/// Hitstun coolpecker state - When Coolpecker gets stunned from a hit.
	/// </summary>
	public class HitstunCoolpeckerState : AbstractHitstunState 
	{
		#region Variables

		#endregion

		#region Properties

		#endregion

		#region Override Functions

		/// <summary>
		/// Called when the parent updates.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnUpdate(MonoBehaviour parent)
		{
			base.OnUpdate(parent);
		}

		#endregion
	}
}