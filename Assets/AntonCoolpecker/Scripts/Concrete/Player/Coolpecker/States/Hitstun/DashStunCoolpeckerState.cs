﻿using AntonCoolpecker.Utils;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Player.Coolpecker.States.Hitstun
{
	/// <summary>
	/// Dash stun Coolpecker state - When Coolpecker is stunned after dashing(Due to e.g. hitting a vertical wall directly on the nose)
	/// </summary>
    public class DashStunCoolpeckerState : HitstunCoolpeckerState
    {
		#region Variables

		[Tweakable("DashStun")] [SerializeField] protected float m_MaxStunTime = 1f; //Dash stun duration

		#endregion

		#region Functions

		/// <summary>
		/// 	Called when the state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnEnter(MonoBehaviour parent)
		{
			base.OnEnter (parent);

		    GetStateTimer(parent).maxTime = m_MaxStunTime;
		}

		#endregion
    }
}