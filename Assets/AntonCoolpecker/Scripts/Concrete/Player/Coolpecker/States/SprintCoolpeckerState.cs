﻿using AntonCoolpecker.Abstract.Player;
using AntonCoolpecker.Abstract.Player.States;
using System.Collections;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Player.Coolpecker.States
{
    /// <summary>
    /// Coolpecker Sprinting State
    /// </summary>
    public class SprintCoolpeckerState : AbstractSprintState
    {
		#region Override Methods

        /// <summary>
        /// 	Returns a state for transition. Return self if no transition.
        /// </summary>
        /// <returns>The next state.</returns>
        /// <param name="parent">Parent.</param>
        public override AbstractPlayerState GetNextState(MonoBehaviour parent)
        {
            AbstractPlayerController controller = GetPlayerController(parent);

            return base.GetNextState(parent);
        }

		#endregion
    }
}