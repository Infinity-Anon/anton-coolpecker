﻿using AntonCoolpecker.Abstract.Player;
using AntonCoolpecker.Abstract.Player.States;
using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using AntonCoolpecker.Concrete.Utils;
using AntonCoolpecker.Utils;
using AntonCoolpecker.Utils.Time;
using Hydra.HydraCommon.Utils;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Player.Coolpecker.States
{
	/// <summary>
	/// Gliding Coolpecker state - When Coolpecker is gliding.
	/// </summary>
	public class GlidingCoolpeckerState : AbstractPlayerState
	{
        /*public enum GlideMode
        {
            Default,            //Do not ajust the velocity
            LerpToStartForce,   //lerp to a starting force
            SnapToStartForce    //snap to the starting force
        }*/
		//TODO: ADD ENUM SUPPORT TO DEBUG GUI

        [Tweakable("Glide")] [SerializeField] private float m_MaxSpeedMultiplier = 1.5f;
		[Tweakable("Glide")] [SerializeField] private float m_DesiredGlideForce = 5.0f;
		[Tweakable("Glide")] [SerializeField] private float m_AccelerationMultiplier = 0.2f;
		[Tweakable("Glide")] [SerializeField] private float m_RotateSpeedMultiplier = 0.1f;
        [Tweakable("Glide")] [SerializeField] private float m_LerpToStartTotalTime = 3f;

		[Tweakable("Glide")] [SerializeField] private float m_Drag = 0.15f;
		[Tweakable("Glide")] [SerializeField] private float m_StallSpeed = 4.0f;
		[Tweakable("Glide")] [SerializeField] private float m_BankSpeed = 30.0f;
		[Tweakable("Glide")] [SerializeField] private float m_BankLimit = 30.0f;
		[Tweakable("Glide")] [SerializeField] private float m_ReleasePenalty = 0.5f;
		[Tweakable("Glide")] [SerializeField] private float m_MaxGlideTime = 30f;

		[Tweakable("Glide")] [SerializeField] private bool m_UseDrag = true;

		[Tweakable("Glide")] [SerializeField] private bool m_Default = true; //Do not ajust the velocity
		[Tweakable("Glide")] [SerializeField] private bool m_LerpToStartForce = false; //lerp to a starting force
		[Tweakable("Glide")] [SerializeField] private bool m_SnapToStartForce = false; //Snap to the starting force

		[SerializeField] private Timer m_StateTimer;
		[SerializeField] private AbstractPlayerState m_TimerState;
        //[SerializeField] private GlideMode m_GlideMode = GlideMode.Default;
       
        private bool m_ShouldEndGlide = false;
		private float m_StartVelocity;
        private float m_LerpToStartMultiplier;
        private float m_bankAmount;
        private float m_maxAngle;
        private float m_lastInput;

		#region Properties

		/// <summary>
		///     Gets the max speed multiplier.
		/// </summary>
		/// <value>The max speed multiplier.</value>
		protected override float maxSpeedMultiplier { get { return m_MaxSpeedMultiplier; } }

		/// <summary>
		///     Gets the acceleration multiplier.
		/// </summary>
		/// <value>The acceleration multiplier.</value>
		protected override float accelerationMultiplier { get { return m_AccelerationMultiplier; } }

		/// <summary>
		///     Gets the rotate speed multiplier.
		/// </summary>
		/// <value>The rotate speed multiplier.</value>
		protected override float rotateSpeedMultiplier { get { return m_RotateSpeedMultiplier; } }

		/// <summary>
		///     Gets the state to transition to after the timer elapses.
		/// </summary>
		/// <value>The state of the timer.</value>
		protected override AbstractPlayerState timerState { get { return m_TimerState; } }

        ///<summary>
        ///     Gets or sets the value for how long the glide will take to slowdown.
        /// </summary>
        /// <value>The time for the glide to slow down to the inital start value</value>
        public float lerpToStartTotalTime { get { return m_LerpToStartTotalTime; }set { m_LerpToStartTotalTime = value; } }
       
		/// <summary>
		/// 	Gets or sets the desired force for the glide.
		/// </summary>
		/// <value>The desired force for the glide.</value>
		public float desiredGlideForce { get { return m_DesiredGlideForce; } set { m_DesiredGlideForce = value; } }

		/// <summary>
		/// 	Gets or sets the drag.
		/// </summary>
		/// <value>The drag.</value>
		public float drag { get { return m_Drag; } set { m_Drag = value; } }

		/// <summary>
		/// 	Gets or sets the stall speed, the glide will stop if below this speed.
		/// </summary>
		/// <value>The stall speed.</value>
		public float stallSpeed { get { return m_StallSpeed; } set { m_StallSpeed = value; } }
        /// <summary>
        ///     Gets or sets the limit for how much coolPecker can bank
        /// </summary>
        /// <value>The bank Limit</value>
        public float bankLimit { get { return m_BankLimit; }set { m_BankLimit = value; } }

		#endregion

		#region Static Methods

		/// <summary>
		/// 	Determines whether Coolpecker should glide up slopes.
		/// </summary>
		public static bool ShouldGlideRamp(MonoBehaviour parent)
		{
			AbstractPlayerController player = GetPlayerController(parent);
            
			RaycastHit hitInfo = player.characterLocomotor.GroundedCast();
			float glidingThreshold = player.characterLocomotor.characterController.slopeLimit * 0.75f;
			float surfaceAngle = Vector3.Angle(hitInfo.normal, Vector3.up);

			if ((surfaceAngle < glidingThreshold) && player.airTracker.canGlideRamp == false)
				return false;
			else
				return true;
		}

		#endregion

		#region Public/Protected Functions/Methods

        /// <summary>
        ///     Gets the timer to transition to another state.
        /// </summary>
        /// <value>The state timer.</value>
		public override Timer GetStateTimer(MonoBehaviour parent)
		{
            return m_StateTimer;
		}

        /// <summary>
        ///     Resolve the change in velocity for the current frame
        /// </summary>
        /// <param name="player">Player</param>
        /// <param name="desiredV">Desired Velocity</param>
        /// <param name="currentV">Current Velocity</param>
        /// <returns></returns>
		protected override Vector3 ResolveVelocityDelta(AbstractPlayerController player, Vector3 desiredV, Vector3 currentV)
		{
            Vector3 dragVector = currentV * drag * GameTime.deltaTime;
            Vector3 right = new Vector3(currentV.z, 0, -currentV.x);
            float DeltaVMultiplier = 0;

			if (InputMapping.horizontalInput.GetAxisRaw() == -1 || InputMapping.horizontalInput.GetAxisRaw() == 1 || InputMapping.verticalInput.GetAxisRaw() == 1 || InputMapping.verticalInput.GetAxisRaw() == -1)
            {
				if (InputMapping.verticalInput.GetAxisRaw() == -1 && InputMapping.horizontalInput.GetAxisRaw() == 0)
                {
					DeltaVMultiplier = Mathf.Sign(m_lastInput) * 1; 
				}
                else if ((InputMapping.horizontalInput.GetAxisRaw() == 1) || (InputMapping.horizontalInput.GetAxisRaw() == -1))
                {
                    DeltaVMultiplier = 1 * Mathf.Sign(InputMapping.horizontalInput.GetAxisRaw());
                    m_lastInput = DeltaVMultiplier;
                }
                else if (InputMapping.verticalInput.GetAxisRaw() < 0 && InputMapping.horizontalInput.GetAxisRaw() != 0)
                {
                    DeltaVMultiplier = Mathf.Sign(InputMapping.horizontalInput.GetAxisRaw()) * HydraMathUtils.Clamp(HydraMathUtils.Abs(InputMapping.verticalInput.GetAxisRaw()) + HydraMathUtils.Abs(InputMapping.horizontalInput.GetAxisRaw()), 0, 1);
                    m_lastInput = DeltaVMultiplier;
                }
                else if (InputMapping.horizontalInput.GetAxisRaw() != 0)
                {
                    DeltaVMultiplier = InputMapping.horizontalInput.GetAxisRaw();
                    m_lastInput = DeltaVMultiplier;
                }
            }
            else
            {
				if (InputMapping.verticalInput.GetAxisRaw() < 0 && InputMapping.horizontalInput.GetAxisRaw() == 0)
                {
                    DeltaVMultiplier = Mathf.Sign(m_lastInput) * HydraMathUtils.Abs(InputMapping.verticalInput.GetAxis());
                }
				else if (InputMapping.verticalInput.GetAxisRaw() < 0 && InputMapping.horizontalInput.GetAxisRaw() != 0)
                {
					DeltaVMultiplier = Mathf.Sign(InputMapping.horizontalInput.GetAxisRaw()) * HydraMathUtils.Clamp(HydraMathUtils.Abs(InputMapping.verticalInput.GetAxisRaw())+ HydraMathUtils.Abs(InputMapping.horizontalInput.GetAxisRaw()),0,1);
                    m_lastInput = DeltaVMultiplier;
                }
				else if (InputMapping.horizontalInput.GetAxisRaw() != 0)
                {
					DeltaVMultiplier = InputMapping.horizontalInput.GetAxisRaw();
                    m_lastInput = DeltaVMultiplier;
                }
            }    

            Vector3 newHeading = Vector3.RotateTowards(currentV,right ,
                (player.rotateSpeed *Mathf.Deg2Rad * rotateSpeedMultiplier) *DeltaVMultiplier* GameTime.deltaTime, 0f);
            Vector3 deltaV = newHeading - currentV;

            if (m_UseDrag)
                deltaV = deltaV - dragVector;
			
            return deltaV;
		}

		/// <summary>
		/// Handles the movement.
		/// </summary>
		/// <param name="parent">Parent.</param>
        protected override void HandleMovement(MonoBehaviour parent)
        {
            AbstractPlayerController player = GetPlayerController(parent);

            Vector3 desiredMovement = GetMovementVector(parent);
            ResolveRotation(player, desiredMovement);

            Vector3 deltaV = ResolveVelocityDelta(player, desiredMovement, player.characterLocomotor.velocity);
            player.characterLocomotor.velocity += deltaV;
        }

        /// <summary>
        ///     Face towards the current velocity heading, instead of that frame's input.
        /// </summary>
        protected override void ResolveRotation(AbstractPlayerController player, Vector3 movement)
		{
            RotateTowardsVelocityVector(player);

			if ((InputMapping.horizontalInput.GetAxisRaw() == 0 && InputMapping.verticalInput.GetAxisRaw() >= 0))
            {
                m_bankAmount = Mathf.MoveTowards(m_bankAmount, 0, m_BankSpeed * GameTime.deltaTime);
            }
            else
            {
				if (InputMapping.verticalInput.GetAxisRaw() <= 0 && InputMapping.horizontalInput.GetAxisRaw() != 0)
                {
                    m_bankAmount = Mathf.MoveTowards(m_bankAmount, 
						m_BankLimit * m_lastInput, 
						m_BankSpeed * GameTime.deltaTime);
                }
				else if (InputMapping.verticalInput.GetAxisRaw() <= 0 && InputMapping.horizontalInput.GetAxisRaw() == 0)
                {
					m_bankAmount = Mathf.MoveTowards(m_bankAmount,
						m_BankLimit * (HydraMathUtils.Abs(InputMapping.verticalInput.GetAxisRaw()) * Mathf.Sign(m_lastInput)),
						m_BankSpeed * GameTime.deltaTime);

                }
                else
                {
                    m_bankAmount = Mathf.MoveTowards(m_bankAmount,
						m_BankLimit * InputMapping.horizontalInput.GetAxisRaw(),
                        m_BankSpeed * GameTime.deltaTime);
                }
            }

            m_bankAmount = HydraMathUtils.Clamp(m_bankAmount, -m_BankLimit, m_BankLimit);
            Vector3 tiltedUp = Vector3.RotateTowards(Vector3.up, player.transform.right, m_bankAmount * Mathf.Deg2Rad, 0f);
            Quaternion rolled = Quaternion.LookRotation(player.transform.forward, tiltedUp);
            player.transform.rotation = rolled;
        }
			
        /// <summary>
        /// 	Sick aerials.(gliding up ramps)
        /// </summary>
        public void GlideRamp(MonoBehaviour parent)
		{
			AbstractPlayerController player = GetPlayerController(parent);

			RaycastHit hitInfo = player.characterLocomotor.GroundedCast();
			float surfaceAngle = Vector3.Angle(hitInfo.normal, Vector3.up);

			if (!HydraMathUtils.Approximately(surfaceAngle, 90.0f))
			{
				StartFreeFloating(parent);
				player.airTracker.canGlideRamp = true;
			}
			else
				StartFreeFloating(parent);
		}

        /// <summary>
        ///     Called when the state becomes active.
        /// </summary>
        /// <param name="parent">Parent.</param>
        public override void OnEnter(MonoBehaviour parent)
        {
            base.OnEnter(parent);

            AbstractPlayerController player = GetPlayerController(parent);

            Cameras.States.GlideCameraState GlideCam = player.playerCamera.stateMachine.GetStateByMode(Cameras.CameraController.Mode.Glide) as Cameras.States.GlideCameraState;

            if (GlideCam.GetMaxAngle() != 0)
                m_maxAngle =  GlideCam.GetMaxAngle();
            else
                m_maxAngle = 0;
			
            m_StateTimer.maxTime = m_MaxGlideTime;
            player.airTracker.canGlideRamp = false;
            m_ShouldEndGlide = false;
            m_StateTimer.AddSeconds(-player.airTracker.glideTimeElapsed);

            //In order to avoid unassigned variables, set the value of the start force to the desired force
            m_StartVelocity = m_DesiredGlideForce;

            //Private asignment
            m_bankAmount = 0;
            m_lastInput = -1;

            //Change the total time to a multiplier
            m_LerpToStartMultiplier = 1 / m_LerpToStartTotalTime;

            if (!m_StateTimer.complete)
            {
                //Set the locomotor velocity to the current velocity of the player
                player.characterLocomotor.UpdateVelocity();
                StartFloating(parent);

                if (player.characterLocomotor.flatVelocity.magnitude < m_DesiredGlideForce)
                    SetToStartForce(player);
                else
                {
                    //Since we will need to slow down change the value of m_StartForce to the real starting force
                    m_StartVelocity = player.characterLocomotor.flatVelocity.magnitude;

                    // Adjust depending on glide type
					if (m_LerpToStartForce)                   
                        LerpToStartForce(player);
					else if (m_SnapToStartForce)
                        SetToStartForce(player);
                }
            }
        }

        /// <summary>
        ///     Lerp the velocity towards m_GlideStartForce 
        /// </summary>
        /// <param name="player"></param>
        public void LerpToStartForce(AbstractPlayerController player)
        {
            Vector3 entryForce = player.characterLocomotor.flatVelocity.normalized * m_StartVelocity;
            Vector3 glideForce = player.characterLocomotor.flatVelocity.normalized * m_DesiredGlideForce;

            player.characterLocomotor.flatVelocity = Vector3.Lerp(
                entryForce,
				glideForce,
				m_StateTimer.elapsed * m_LerpToStartMultiplier);
        }

        /// <summary>
        ///     Set the velocity to m_GlideStartForce
        /// </summary>
        /// <param name="player">Player.</param>
        public void SetToStartForce(AbstractPlayerController player)
        {
            Vector3 glideDirection = player.transform.forward;
            glideDirection.y = 0.0f;

            Vector3 glideForce = glideDirection.normalized * m_DesiredGlideForce;
            player.characterLocomotor.flatVelocity = glideForce;
        }

        /// <summary>
        /// 	Called when the parent's CharacterController collides while moving.
        /// </summary>
        /// <param name="hit">Hit.</param>
        /// <param name="parent">Parent.</param>
        public override void ParentControllerColliderHit(ControllerColliderHit hit, MonoBehaviour parent)
		{
			base.ParentControllerColliderHit(hit, parent);

			if (!ShouldGlideRamp(parent))
				m_ShouldEndGlide = true;
		}

        /// <summary>
        ///     Called each frame.
        /// </summary>
        /// <param name="parent"></param>
        public override void OnUpdate(MonoBehaviour parent)
        {
            base.OnUpdate(parent);
            
            AbstractPlayerController player = GetPlayerController(parent);

			if (m_LerpToStartForce && player.characterLocomotor.flatVelocity.magnitude > m_DesiredGlideForce)
                LerpToStartForce(player);
			
            if (InputMapping.glidingInput.GetButtonUp())
                m_ShouldEndGlide = true;
			
            if (player.characterLocomotor.isGrounded && !ShouldGlideRamp(parent))
            {
                Debug.Log("help");
                m_ShouldEndGlide = true;
            }

            if (ShouldGlideRamp(parent))
                GlideRamp(parent);
        }

        /// <summary>
        /// 	Called before another state becomes active.
        /// </summary>
        /// <param name="parent">Parent.</param>
        public override void OnExit(MonoBehaviour parent)
		{
			base.OnExit(parent);

			StopFloating(parent);
			AbstractPlayerController player = GetPlayerController(parent);
			player.airTracker.glideTimeElapsed = m_StateTimer.elapsed + m_ReleasePenalty;

			//TODO: FIX SMOOTH TRANSITION TO END GLIDE ROTATION
			player.transform.rotation = Quaternion.Euler(0, player.transform.rotation.eulerAngles.y, 0);
		}

		/// <summary>
		/// 	Returns a state for transition. Return self if no transition.
		/// </summary>
		/// <returns>The next state.</returns>
		/// <param name="parent">Parent.</param>
		public override AbstractPlayerState GetNextState(MonoBehaviour parent)
		{
			AbstractPlayerController player = GetPlayerController(parent);

			if (player.characterLocomotor.flatVelocity.magnitude < stallSpeed)
				m_ShouldEndGlide = true;
			
			if (m_ShouldEndGlide)
				return timerState;
			
			return base.GetNextState(parent);
		}

        #endregion

        #region Private Methods

        /// <summary>
        ///     Returns the value with the lowest absolute value.
        /// </summary>
        private float AbsMin(float a, float b)
		{
			return HydraMathUtils.Abs(a) < HydraMathUtils.Abs(b) ? a : b;
		}

		/// <summary>
		///     Calculate how much Coolpecker should bank while gliding, based on how much Coolpecker needs to turn in order to face the desired direction.
		/// </summary>
		private float CalculateRollPredictive(AbstractPlayerController player)
		{
			Vector3 futureFacing = this.GetMovementVector(player);

            if (futureFacing == Vector3.zero)
               futureFacing = player.characterLocomotor.velocity;
			
            if (futureFacing.magnitude < 0.001f)
				futureFacing = player.transform.forward;
			
			float futureYaw = Vector3.Angle(player.transform.forward, futureFacing) *
							  Mathf.Sign(Vector3.Dot(futureFacing, player.transform.right));

			float rollTargetFuture = (futureYaw * m_BankSpeed) / (player.rotateSpeed * rotateSpeedMultiplier*GameTime.deltaTime);

            return futureYaw;
		}

        #endregion
    }
}