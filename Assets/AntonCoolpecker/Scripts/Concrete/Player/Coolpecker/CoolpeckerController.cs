﻿using AntonCoolpecker.Abstract.Player;
using AntonCoolpecker.Concrete.Listeners.Player.Coolpecker;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Player.Coolpecker
{
	/// <summary>
	/// 	CoolpeckerController is the controller for the Coolpecker player.
	/// </summary>
	[RequireComponent(typeof(CoolpeckerAudioListener))]
	[RequireComponent(typeof(CoolpeckerAnimationListener))]
	public class CoolpeckerController : AbstractPlayerController {}
}