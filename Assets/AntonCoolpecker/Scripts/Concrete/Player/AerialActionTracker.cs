﻿namespace AntonCoolpecker.Concrete.Player
{
	/// <summary>
	/// Aerial action tracker - Keeps track of air-related player action statistics.
	/// </summary>
	public class AerialActionTracker
	{
		#region Variables

		private bool m_CanAirJump = false; //Keeps track of whether the player can jump again while in the air or not
		private bool m_CanJumpCancel = false; //Keeps track of whether the player can cancel a jump while midair or not
		private bool m_CanGlideRamp = false; //Keeps track of whether the player can glide up ramps/slopes while gliding or not
		private bool m_CanSpinHover = false; //Keeps track of whether the player can hover while spin-attacking midair as Coolpecker or not
        private bool m_CanBounce = false; //Keeps track of whether the player can bounce jump or not
        private float m_GlideTimeElapsed = 0.0f; //Keeps track of elapsed glide time
        private float m_BounceNumber = 0.0f; //Keeps track of executed bounce jumps

		#endregion

		#region Properties

		public bool canAirJump { get { return m_CanAirJump; } set { m_CanAirJump = value; } }
		public bool canJumpCancel { get { return m_CanJumpCancel; } set { m_CanJumpCancel = value; } }
		public bool canGlideRamp { get { return m_CanGlideRamp; } set { m_CanGlideRamp = value; } }
		public bool canSpinHover { get { return m_CanSpinHover; } set { m_CanSpinHover = value; } }
        public bool canBounce { get { return m_CanBounce; } set { m_CanBounce = value; } }
		public float glideTimeElapsed { get { return m_GlideTimeElapsed; } set { m_GlideTimeElapsed = value; } }
        public float bounceNumber { get { return m_BounceNumber; } set { m_BounceNumber = value; } }

		#endregion

		#region Functions

		/// <summary>
		/// Reset this instance.
		/// </summary>
		public void Reset()
		{
			m_CanAirJump = true;
			m_CanJumpCancel = false;
			m_CanGlideRamp = true;
			m_CanSpinHover = true;
            m_CanBounce = true;
            m_BounceNumber = 0;
			m_GlideTimeElapsed = 0.0f;
		}

		/// <summary>
		/// Set the specified other.
		/// </summary>
		/// <param name="other">Other.</param>
		public void Set(AerialActionTracker other)
		{
			m_CanAirJump = other.canAirJump;
			m_CanJumpCancel = other.canJumpCancel;
			m_GlideTimeElapsed = other.glideTimeElapsed;
			m_CanGlideRamp = other.canGlideRamp;
			m_CanSpinHover = other.canSpinHover;
            m_CanBounce = other.canBounce;
            m_BounceNumber = other.m_BounceNumber;
		}

		#endregion
	}
}