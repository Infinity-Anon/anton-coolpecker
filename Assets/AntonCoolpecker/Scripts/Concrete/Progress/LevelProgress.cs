﻿using System;
using System.Collections;
using System.Collections.Generic; //Required for Lists.
using System.Collections.ObjectModel;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Progress
{
	/// <summary>
	/// Status of a collectable.
	/// 
	/// Uncollected - This collectable has yet to be collected
	/// Collected - This collectable has been collected
	/// Preserved - This collectable has yet to be spent(Used in-game)
	/// Spent - This collectable has been spent(Used in-game)
	/// </summary>
	public enum CollectedStatus : byte
	{
		Uncollected,
		Collected,
		Preserved,
		Spent
	}

	/// <summary>
	/// Level progress script.
	/// </summary>
	[Serializable]
	public class LevelProgress
	{
		#region Variables

		[SerializeField] private CollectedStatus[] m_Doodads; //Keeps track of DOODADs in a level
		[SerializeField] private CollectedStatus[] m_Fruit; //Keeps track of fruits in a level
		[SerializeField] private CollectedStatus[] m_Scraps; //Keeps track of scraps in a level
		[SerializeField] private CollectedStatus[] m_Flags; //Keeps track of enviromental triggers in a level

		[NonSerialized] private int m_TotalDoodads; //Total number of DOODADs in a level
		[NonSerialized] private int m_UnspentFruit; //Number of fruits that has yet to be spent in a level
		[NonSerialized] private int m_UnspentScrap; //Number of scraps that has yet to be spent in a level

        private int m_UnspentDoodads; //Number of DOODADs that has yet to be used for the DOODAD door in a level

		#endregion

		#region Properties

		/// <summary>
		/// The status of the doodads.
		/// Do not check this when determining whether to reset the challenge for the doodad.
		/// That should be done in flags. It might be that the player has completed a challenge
		/// to unlock a doodad, but left the level before they actually walked over and picked it up.
		/// </summary>
		public ReadOnlyCollection<CollectedStatus> doodads
		{
			get
			{
				return Array.AsReadOnly<CollectedStatus>(m_Doodads);
			}
		}

		/// <summary>
		/// Collected status for fruits.
		/// </summary>
		public ReadOnlyCollection<CollectedStatus> fruit
		{
			get
			{
				return Array.AsReadOnly<CollectedStatus>(m_Fruit);
			}
		}

		/// <summary>
		/// Collected status for scraps.  The same note as doods re unlocking vs collecting applies.
		/// (Whoever wrote the comment above, could you please elaborate on the last part? 
		/// It's written in a very incomprehensible manner. -BrokenProgrammer)
		/// </summary>
		public ReadOnlyCollection<CollectedStatus> scraps
		{
			get
			{
				return Array.AsReadOnly<CollectedStatus>(m_Scraps);
			}
		}

		/// <summary>
		/// Not the kind of flags you stick on a pole, but environmental triggers. Have they beaten that boss? Have they burned that bridge?
		/// </summary>
		public ReadOnlyCollection<CollectedStatus> flags
		{
			get
			{
				return Array.AsReadOnly<CollectedStatus>(m_Flags);
			}
		}

		/// <summary>
		/// Gets the total doodads.
		/// </summary>
		/// <value>The total doodads.</value>
		public int totalDoodads
		{
			get
			{
				return m_TotalDoodads;
			}
		}

		/// <summary>
		/// Gets the unspent doodads.
		/// </summary>
		/// <value>The unspent doodads.</value>
        public int unspentDoodads
        {
            get
            {
                return m_UnspentDoodads;
            }
        }

		/// <summary>
		/// Gets the unspent fruit.
		/// </summary>
		/// <value>The unspent fruit.</value>
		public int unspentFruit
		{
			get
			{
				return m_UnspentFruit;
			}
		}

		/// <summary>
		/// Gets the unspent scrap.
		/// </summary>
		/// <value>The unspent scrap.</value>
		public int unspentScrap
		{
			get
			{
				return m_UnspentScrap;
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="AntonCoolpecker.Concrete.Progress.LevelProgress"/> class.
		/// </summary>
		/// <param name="numDoodads">Number of DOODADs.</param>
		/// <param name="numFruit">Number of fruits.</param>
		/// <param name="numScraps">Number of scraps.</param>
		/// <param name="numFlags">Number of flags.</param>
		public LevelProgress(int numDoodads, int numFruit, int numScraps, int numFlags)
		{
			m_Doodads = new CollectedStatus[numDoodads];
			m_Fruit = new CollectedStatus[numFruit];
			m_Scraps = new CollectedStatus[numScraps];
			m_Flags = new CollectedStatus[numFlags];
		}

		#endregion

		#region Functions/Methods

		/// <summary>
		/// Fragile flags reset when the player dies, if they haven't collected a doodad or returned to the hub yet.
		/// </summary>
		public void SetFragileFlag(int flag) 
		{
			m_Flags[flag] = CollectedStatus.Collected;
		}

		/// <summary>
		/// Permanent flags do not reset when the player dies, even if they haven't collected a doodad or returned to the hub yet.
		/// </summary>
		public void SetPermanentFlag(int flag)
		{
			m_Flags[flag] = CollectedStatus.Preserved;
		}

		/// <summary>
		/// Called when collecting a fruit.
		/// </summary>
		/// <param name="id">Fruit identifier number.</param>
		public void CollectFruit(int id)
		{
            if (id >= m_Fruit.Length)
				return;
			
            m_Fruit[id] = CollectedStatus.Collected;
			m_UnspentFruit++;
		}

		/// <summary>
		/// Called when collecting a DOODAD.
		/// </summary>
		/// <param name="id">DOODAD identifier number.</param>
        public void CollectDoodad(int id)
        {
            if (id >= m_Doodads.Length)
            {
                Debug.LogWarning("Doodad " + id.ToString() + " does not exist in revelent level progress. Update main");
                return;
            }

            m_Doodads[id] = CollectedStatus.Collected;
            m_UnspentDoodads++;
            m_TotalDoodads++;
        }

		/// <summary>
		/// Get the total amount of DOODADs
		/// </summary>
		/// <returns>Total amount of doodads.</returns>
        public int AllDoodads()
        {
            return m_Doodads.Length;
        }

		/// <summary>
		/// Called when the player spends DOODADs
		/// </summary>
		/// <param name="amount">Amount of DOODADs to spend.</param>
		/// <param name="Used">List to keep track of spent DOODADs</param>
        public void SpendDoodads(int amount, List<int> Used)
        {
            int loopTracker = 0;
            int spent = 0;

            while (spent != amount && loopTracker < m_Doodads.Length)
            { 
                if (m_Doodads[loopTracker] == CollectedStatus.Collected || m_Doodads[loopTracker] == CollectedStatus.Preserved)
                {
                    Used.Add(loopTracker);
                    Debug.Log("Spending doodad "+loopTracker);
                    m_Doodads[loopTracker] = CollectedStatus.Spent;
                    m_UnspentDoodads--;
                    spent++;
                }

                loopTracker++;
            }

            if (spent != amount)
                Debug.LogWarning("Function SpendDoodads call did not find enough free doodads in LevelProgress to spend");
        }

		/// <summary>
		/// Refunds the doodads.
		/// </summary>
		/// <param name="id">Identifier.</param>
        public void refundDoodads(int id)
        {
            if (id > m_Doodads.Length)
                return;
			
            if (m_Doodads[id] != CollectedStatus.Spent)
            {
                Debug.Log("Invalid refund of doodad " + id);
            }
            else
            {
                m_UnspentDoodads++;
                m_Doodads[id] = CollectedStatus.Collected;
            }
        }

		/// <summary>
		/// Preserve all collected but unpreserved fruit, scraps and flags.  Call this when leaving a level or collecting a doodad.
		/// </summary>
		public void PreserveAll()
		{
			for (int i = 0; i < m_Fruit.Length; i++) 
			{
				if (m_Fruit[i] == CollectedStatus.Collected) 
				{
					m_Fruit[i] = CollectedStatus.Preserved;
				}
			}

			for (int i = 0; i < m_Scraps.Length; i++) 
			{
				if (m_Scraps[i] == CollectedStatus.Collected) 
				{
					m_Scraps[i] = CollectedStatus.Preserved;
				}
			}

			for (int i = 0; i < m_Flags.Length; i++)
			{
				if (m_Flags[i] == CollectedStatus.Collected) 
				{
					m_Flags[i] = CollectedStatus.Preserved;
				}
			}
		}

		/// <summary>
		/// Reset all collected but unpreserved fruit, scraps and flags. Call this when the player character dies.
		/// </summary>
		public void ResetAll()
		{
			for (int i = 0; i < m_Fruit.Length; i++) 
			{
				if (m_Fruit[i] == CollectedStatus.Collected) 
				{
					m_Fruit[i] = CollectedStatus.Uncollected;
					m_UnspentFruit--;
				}
			}
			for (int i = 0; i < m_Scraps.Length; i++) 
			{
				if (m_Scraps[i] == CollectedStatus.Collected) 
				{
					m_Scraps[i] = CollectedStatus.Uncollected;
					m_UnspentScrap--;
				}
			}
			for (int i = 0; i < m_Flags.Length; i++) 
			{
				if (m_Flags[i] == CollectedStatus.Collected) 
				{
					m_Flags[i] = CollectedStatus.Uncollected;
				}
			}
		}

		/// <summary>
		/// Recount cached doodad, scrap and fruit counts.
		/// </summary>
		public void Recount()
		{
			m_TotalDoodads = m_UnspentFruit = m_UnspentScrap = m_UnspentDoodads = 0;

			foreach (CollectedStatus doodad in m_Doodads) 
			{
				if (doodad == CollectedStatus.Collected || doodad == CollectedStatus.Preserved || doodad == CollectedStatus.Spent) 
				{
                    m_TotalDoodads++;

                    if (doodad == CollectedStatus.Collected || doodad == CollectedStatus.Preserved)
                    {
                        m_UnspentDoodads++;
                    }
                }
			}

			foreach (CollectedStatus fruit in m_Fruit) 
			{
				if (fruit == CollectedStatus.Collected || fruit == CollectedStatus.Preserved) 
				{
					m_UnspentFruit++;
				}
			}

			foreach (CollectedStatus scrap in m_Scraps) 
			{
				if (scrap == CollectedStatus.Collected || scrap == CollectedStatus.Preserved) 
				{
					m_UnspentScrap++;
				}
			}
		}

		#endregion
	}
}