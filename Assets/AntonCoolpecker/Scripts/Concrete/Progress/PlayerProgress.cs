﻿using System;
using System.Collections;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Progress
{
	/// <summary>
	/// Player progress script.
	/// </summary>
	[Serializable]
	public class PlayerProgress
	{
		#region Variables

		[SerializeField] private LevelProgress m_Tutorial;
		[SerializeField] private LevelProgress m_PoynettePort;
		[SerializeField] private LevelProgress m_TranquilTreeline;
		[SerializeField] private LevelProgress m_AncientAntics;
		[SerializeField] private LevelProgress m_ColossusCoast;
		[SerializeField] private LevelProgress m_VileValley;
		[SerializeField] private LevelProgress m_GlimmerGlacier;
		[SerializeField] private LevelProgress m_HorrendousHill;

		[SerializeField] private int m_ScrapDoodads;
		[SerializeField] private int m_HealthUpgrades;

		//Anton's abilities
		[SerializeField] private bool m_UnlockedTongue;
		[SerializeField] private bool m_UnlockedDig;
		[SerializeField] private bool m_UnlockedShoot;
		[SerializeField] private int m_AirAttackCombo;

		//Coolpecker's abilities
		[SerializeField] private bool m_UnlockedDash;
		[SerializeField] private bool m_UnlockedPound;
		[SerializeField] private bool m_UnlockedGlide;

		#endregion

		#region Properties

		public LevelProgress Tutorial { get { return m_Tutorial; } }
		public LevelProgress PoynettePort { get { return m_PoynettePort; } }
		public LevelProgress TranquilTreeline { get { return m_TranquilTreeline; } }
		public LevelProgress AncientAntics { get { return m_AncientAntics; } }
		public LevelProgress ColossusCoast { get { return m_ColossusCoast; } }
		public LevelProgress VileValley { get { return m_VileValley; } }
		public LevelProgress GlimmerGlacier { get { return m_GlimmerGlacier; } }
		public LevelProgress HorrendousHill { get { return m_HorrendousHill; } }

		/// <summary>
		/// Gets the number of collected and preserved (but not spent) fruit.
		/// </summary>
		public int unspentFruit
		{
			get
			{
				return m_PoynettePort.unspentFruit
					+ m_TranquilTreeline.unspentFruit
					+ m_AncientAntics.unspentFruit
					+ m_ColossusCoast.unspentFruit
					+ m_VileValley.unspentFruit
					+ m_GlimmerGlacier.unspentFruit
					+ m_HorrendousHill.unspentFruit;
			}
		}

		/// <summary>
		/// Gets the number of collected and preserved (but not spent) scrap.
		/// </summary>
		public int unspentScrap
		{
			get
			{
				return m_PoynettePort.unspentScrap
					+ m_TranquilTreeline.unspentScrap
					+ m_AncientAntics.unspentScrap
					+ m_ColossusCoast.unspentScrap
					+ m_VileValley.unspentScrap
					+ m_GlimmerGlacier.unspentScrap
					+ m_HorrendousHill.unspentScrap;
			}
		}

		/// <summary>
		/// Get the total number of doodads collected from all levels and scrap so far.
		/// </summary>
		public int totalDoodads
		{
			get
			{
				return m_PoynettePort.totalDoodads
					+ m_TranquilTreeline.totalDoodads
					+ m_AncientAntics.totalDoodads
					+ m_ColossusCoast.totalDoodads
					+ m_VileValley.totalDoodads
					+ m_GlimmerGlacier.totalDoodads
					+ m_HorrendousHill.totalDoodads
					+ m_ScrapDoodads;
			}
		}

		/// <summary>
		/// Get the number of doodads converted from scrap so far.
		/// </summary>
		public int scrapDoodads { get { return m_ScrapDoodads; } }

		/// <summary>
		/// Gets a value indicating whether this <see cref="AntonCoolpecker.Concrete.Progress.PlayerProgress"/> unlocked tongue swing ability.
		/// </summary>
		/// <value><c>true</c> if unlocked tongue swing/attack; otherwise, <c>false</c>.</value>
		public bool unlockedTongue { get { return m_UnlockedTongue; } }

		/// <summary>
		/// Gets a value indicating whether this <see cref="AntonCoolpecker.Concrete.Progress.PlayerProgress"/> unlocked digging ability.
		/// </summary>
		/// <value><c>true</c> if unlocked digging ability; otherwise, <c>false</c>.</value>
		public bool unlockedDig { get { return m_UnlockedDig; } }

		/// <summary>
		/// Gets a value indicating whether this <see cref="AntonCoolpecker.Concrete.Progress.PlayerProgress"/> unlocked tongue shoot ability.
		/// </summary>
		/// <value><c>true</c> if unlocked tongue shoot ability; otherwise, <c>false</c>.</value>
		public bool unlockedShoot { get { return m_UnlockedShoot; } }

		/// <summary>
		/// Gets a value indicating whether this <see cref="AntonCoolpecker.Concrete.Progress.PlayerProgress"/> unlocked dashing ability.
		/// </summary>
		/// <value><c>true</c> if unlocked dashing ability; otherwise, <c>false</c>.</value>
		public bool unlockedDash { get { return m_UnlockedDash; } }

		/// <summary>
		/// Gets a value indicating whether this <see cref="AntonCoolpecker.Concrete.Progress.PlayerProgress"/> unlocked groundpound ability.
		/// </summary>
		/// <value><c>true</c> if unlocked groundpound abilitiy; otherwise, <c>false</c>.</value>
		public bool unlockedPound { get { return m_UnlockedPound; } }

		/// <summary>
		/// Gets a value indicating whether this <see cref="AntonCoolpecker.Concrete.Progress.PlayerProgress"/> unlocked gliding ability.
		/// </summary>
		/// <value><c>true</c> if unlocked glide; otherwise, <c>false</c>.</value>
		public bool unlockedGlide { get { return m_UnlockedGlide; } }

		#endregion

		#region Constructors

		/// <summary>
		/// Private constructor for player progress.
		/// Get the current progress from the Main object.
		/// Get a new instance from StartNew or LoadGame.
		/// </summary>
		private PlayerProgress()
		{
			m_Tutorial = new LevelProgress(1, 0, 0, 1);
			m_PoynettePort = new LevelProgress(12, 0, 20, 6);
			m_TranquilTreeline = new LevelProgress(6, 100, 5, 1);
			m_ColossusCoast = new LevelProgress(6, 100, 5, 1);
			m_AncientAntics = new LevelProgress(6, 100, 5, 1);
			m_VileValley = new LevelProgress(6, 100, 5, 1);
			m_GlimmerGlacier = new LevelProgress(6, 100, 5, 1);
			m_HorrendousHill = new LevelProgress(6, 100, 5, 1);

			m_ScrapDoodads = 0;
			m_HealthUpgrades = 0;

			m_UnlockedDash = false;
			m_UnlockedDig = false;
			m_UnlockedGlide = false;
			m_UnlockedPound = false;
			m_UnlockedShoot = false;
            m_AirAttackCombo = 1;
			m_UnlockedTongue = false;
		}

		#endregion

		#region Instance Methods

		/// <summary>
		/// Gets an instance of the current level progress in a specified level.
		/// </summary>
		/// <returns>The current level progress.</returns>
		/// <param name="sceneName">Name of level(Unity scene).</param>
		public LevelProgress GetCurrentLevelProgress (string sceneName)
		{
			switch (sceneName) 
			{
			case "Tutorial":
				return m_Tutorial;
			case "PoynettePort":
				return m_PoynettePort;
			case "AncientAntics":
				return m_AncientAntics;
			case "ColossusCoast":
				return m_ColossusCoast;
			case "VileValley":
				return m_VileValley;
			case "GlimmerGlacier":
				return m_GlimmerGlacier;
			case "HorrendousHill":
				return m_HorrendousHill;
			default:
				Debug.LogWarning ("No LevelProgress for current scene. Returning dummy LevelProgress");
				return new LevelProgress(1, 0, 0, 0);
			}
		}

		#endregion

		#region Static Methods

		/// <summary>
		/// Creates a new instance of player progress when a new game has been started.
		/// </summary>
		/// <returns>A new player progress instance.</returns>
		public static PlayerProgress NewGame()
		{
			return new PlayerProgress();
		}

		/// <summary>
		/// Supposed to load an existing instance of player progress when a saved game has been loaded.
		/// </summary>
		/// <returns>An existing instance of player progress</returns>
		/// <param name="slot">Save slot to load.</param>
		//TODO: IMPLEMENT ABILITY TO LOAD A SAVED GAME(PLAYERPROGRESS)
		public static PlayerProgress LoadGame(int slot)
		{
			throw new NotImplementedException();
		}

		#endregion
	}
}