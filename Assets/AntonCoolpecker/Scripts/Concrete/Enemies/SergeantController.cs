﻿using AntonCoolpecker.Abstract.Enemies.States;
using AntonCoolpecker.Concrete.Enemies;
using AntonCoolpecker.Concrete.Enemies.States;
using AntonCoolpecker.Concrete.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Enemies
{
	/// <summary>
	/// Sergeant Ant controller.
	/// </summary>
    public class SergeantController : EnemyController
    {
		#region Variables

        private List<EnemyController> m_Soldiers;

		#endregion

		#region Properties

		/// <summary>
		/// Sets the list of ant soldiers.
		/// </summary>
		/// <value>The soldiers list.</value>
        public List<EnemyController> soldiersList { set { m_Soldiers = value; } }

		#endregion

		#region Override Functions

		/// <summary>
		/// Called once every frame.
		/// </summary>
        protected override void Update()
        {
            base.Update();

            if (!canSeePlayer)
                m_Soldiers = null;

			if (m_Soldiers != null) 
			{
				for (int i = 0; i < m_Soldiers.Count; i++) 
				{
					if (m_Soldiers [i].stateMachine.activeState.GetType () == typeof(PanicEnemyState))
						m_Soldiers [i].stateMachine.SetActiveState (m_Soldiers [i].panicState.calmState, m_Soldiers [i]);
					else if (m_Soldiers [i].stateMachine.activeState.GetType () == typeof(DeadState)) 
					{
						m_Soldiers.RemoveAt (i);
						i--;
						continue;
					}

					m_Soldiers [i].pursuitTimer.Reset ();
				}
			}
        }

		/// <summary>
		/// Called when the Sergeant Ant dies.
		/// </summary>
		/// <param name="collision">Collision.</param>
        protected override void Die(CharacterLocomotorCollisionData collision)
        {
            base.Die(collision);

            if (m_Soldiers != null)
            {
                //Debug.Log("Panicking troops due to death Of Sergeant Ant");

                for (int i = 0; i < m_Soldiers.Count; i++)
                {
                    m_Soldiers[i].panicState.Panic(PanicEnemyState.PanicType.Forced, m_Soldiers[i], 0);
                }

                m_Soldiers = null;
            }
        }

		#endregion
    }
}