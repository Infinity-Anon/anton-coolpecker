﻿using AntonCoolpecker.API.Damageable;
using AntonCoolpecker.Concrete.Utils;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Enemies.GruntSquad
{
	/// <summary>
	/// Big guy behavior.
	/// </summary>
	[RequireComponent(typeof(CharacterController))]
	public class BigGuyBehavior : MonoBehaviour, IDamageable
	{
		#region Variables

		[SerializeField] private bool m_IsAlive = true; //Is the big guy alive?

		#endregion

		#region Properties

		/// <summary>
		/// 	Gets or sets a value indicating whether this BigGuyBehavior is alive.
		/// </summary>
		/// <value><c>true</c> if is alive; otherwise, <c>false</c>.</value>
		public bool isAlive { get { return m_IsAlive; } set { m_IsAlive = value; } }

		#endregion

		#region Virtual Functions

		/// <summary>
		/// Called when the big guy gets damaged/hit.
		/// </summary>
		/// <param name="damage">Amount of damage to apply.</param>
		/// <param name="collision">Collision info.</param>
		public virtual void Damage(int damage, CharacterLocomotorCollisionData collision)
		{
			isAlive = false;
		}

		#endregion
	}
}