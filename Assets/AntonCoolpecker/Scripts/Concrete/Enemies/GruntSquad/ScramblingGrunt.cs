﻿using AntonCoolpecker.Utils.Time;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Enemies.GruntSquad
{
	/// <summary>
	/// Scrambling grunt.
	/// </summary>
	public class ScramblingGrunt : StateMachineBehaviour
	{
		#region Variables

		private const float REORIENT_RATE = 2.0f;

		private float m_NextReorient;
		private NavMeshAgent m_NavMeshAgentCached;
		private Vector3 m_Center;

		#endregion

		#region Override Functions

		/// <summary>
		/// Raises the state enter event.
		/// </summary>
		/// <param name="animator">Animator.</param>
		/// <param name="stateInfo">State info.</param>
		/// <param name="layerIndex">Layer index.</param>
		public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			m_NavMeshAgentCached = animator.gameObject.GetComponent<NavMeshAgent>();
			m_Center = animator.gameObject.transform.position;
		}

		/// <summary>
		/// Raises the state update event.
		/// </summary>
		/// <param name="animator">Animator.</param>
		/// <param name="stateInfo">State info.</param>
		/// <param name="layerIndex">Layer index.</param>
		public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			if (GameTime.time < m_NextReorient)
				return;

			// Run to random offsets around the point where Grunt entered the scrambling state
			Vector3 dest = m_Center + new Vector3(Random.value * 5, 0, Random.value * 5);
			m_NextReorient = GameTime.time + REORIENT_RATE;
			m_NavMeshAgentCached.destination = dest;
		}

		#endregion
	}
}