﻿using AntonCoolpecker.Concrete.Utils;
using Hydra.HydraCommon.Abstract;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Enemies.GruntSquad
{
	/// <summary>
	/// Grunt spawner controller.
	/// </summary>
	public class GruntSpawnerController : HydraMonoBehaviour
	{
		#region Variables

		[SerializeField] private BigGuyBehavior m_SquadLeader; //Reference to grunt squad leader
		[SerializeField] private GruntBehavior m_GruntPrefab; //Reference to grunt

		[SerializeField] private int m_MaxGruntsSpawned; //Maximum number of allowed grunt spawns
		[SerializeField] private float m_MaxXSpawnOffset = 5.0f; //Maximum spawn offset in X axis
		[SerializeField] private float m_MaxZSpawnOffset = 5.0f; //Maximum spawn offset in Z axis
		[SerializeField] private float m_SpawnRateInSeconds = 1.0f; //Time between each grunt spawn

		private int m_GruntCounter = 0; //Number of grunts spawned
		private Timer m_SpawnTimer; //Keeps track of when grunt(s) can be spawned

		#endregion

		#region Override Functions

		/// <summary>
		/// Called before the first Update.
		/// </summary>
		protected override void Start()
		{
			base.Start();

			m_SpawnTimer = new Timer();

			m_MaxXSpawnOffset = System.Math.Abs(m_MaxXSpawnOffset);
			m_MaxZSpawnOffset = System.Math.Abs(m_MaxZSpawnOffset);
		}

		/// <summary>
		/// Called once every frame.
		/// </summary>
		protected override void Update()
		{
			base.Update();

			if (m_SpawnTimer.elapsed > m_SpawnRateInSeconds && m_GruntCounter < m_MaxGruntsSpawned)
			{
				SpawnGrunt();
				m_SpawnTimer.Reset();
			}
		}

		/// <summary>
		/// Called to draw gizmos in the scene view.
		/// </summary>
		protected override void OnDrawGizmos()
		{
			base.OnDrawGizmos();

			Gizmos.color = Color.red;
			Gizmos.DrawCube(transform.position, new Vector3(1, 1, 1));
		}

		#endregion

		#region Private Functions

		/// <summary>
		/// Updates the grunt counter.
		/// </summary>
		private void UpdateGruntCounter()
		{
			--m_GruntCounter;

			if (m_GruntCounter < 0)
				m_GruntCounter = 0;
		}

		/// <summary>
		/// Spawns a grunt.
		/// </summary>
		private void SpawnGrunt()
		{
			float xoffset = Random.Range(-m_MaxXSpawnOffset, m_MaxXSpawnOffset);
			float zoffset = Random.Range(-m_MaxZSpawnOffset, m_MaxZSpawnOffset);

			Vector3 offset = new Vector3(xoffset, 0.0f, zoffset);

			GruntBehavior g = Instantiate(m_GruntPrefab, transform.position + offset, transform.rotation) as GruntBehavior;

			if (m_SquadLeader)
				g.squadLeader = m_SquadLeader;
			
			g.onDestroyedCallback += UpdateGruntCounter;

			// Probably room for race conditions, I'll need a better way to do this
			++m_GruntCounter;
		}

		#endregion
	}
}