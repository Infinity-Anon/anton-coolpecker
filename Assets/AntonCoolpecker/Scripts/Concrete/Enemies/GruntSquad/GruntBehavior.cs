﻿using AntonCoolpecker.API.Damageable;
using AntonCoolpecker.Concrete.Utils;
using Hydra.HydraCommon.Abstract;
using UnityEngine;
using UnityEngine.Events;

namespace AntonCoolpecker.Concrete.Enemies.GruntSquad
{
	/// <summary>
	/// Grunt behavior.
	/// </summary>
	[DisallowMultipleComponent]
	[RequireComponent(typeof(SphereCollider))]
	[RequireComponent(typeof(CharacterController))]
	[RequireComponent(typeof(NavMeshAgent))]
	[RequireComponent(typeof(Animator))]
	public class GruntBehavior : HydraMonoBehaviour, IDamageable
	{
		#region Variables

		// Assignable in editor rather than through GetComponent<>() so that the editor view can draw the gizmos as well
		[SerializeField] private BigGuyBehavior m_SquadLeader;

		// For keeping track of player's position when fleeing
		private GameObject m_CachedPlayer;
		private NavMeshAgent m_CachedNavMeshAgent;
		private Animator m_CachedAnimator;
		private SphereCollider m_CachedSphereCollider;

		public event UnityAction onDestroyedCallback;

		#endregion

		#region Properties

		/// <summary>
		/// 	Gets the nav mesh agent.
		/// </summary>
		/// <value>The nav mesh agent.</value>
		public NavMeshAgent navMeshAgent
		{
			get { return m_CachedNavMeshAgent ?? (m_CachedNavMeshAgent = GetComponent<NavMeshAgent>()); }
		}

		/// <summary>
		/// 	Gets the animator.
		/// </summary>
		/// <value>The animator.</value>
		public Animator animator { get { return m_CachedAnimator ?? (m_CachedAnimator = GetComponent<Animator>()); } }

		/// <summary>
		/// Gets or sets the squad leader.
		/// </summary>
		/// <value>The squad leader.</value>
		public BigGuyBehavior squadLeader { get { return m_SquadLeader; } set { m_SquadLeader = value; } }

		/// <summary>
		/// Gets the sphere collider.
		/// </summary>
		/// <value>The sphere collider.</value>
		public SphereCollider sphereCollider
		{
			get { return m_CachedSphereCollider ?? (m_CachedSphereCollider = GetComponent<SphereCollider>()); }
		}

		#endregion

		#region Messages

		/// <summary>
		/// 	Called before the first Update.
		/// </summary>
		protected override void Start()
		{
			base.Start();

			if (m_SquadLeader)
				animator.SetBool("CommanderPresent", true);
		}

		/// <summary>
		/// 	Called once every frame.
		/// </summary>
		protected override void Update()
		{
			base.Update();

			// TODO: instead of polling, make squad leader broadcast message on death
			if (m_SquadLeader && !m_SquadLeader.isAlive)
				animator.SetBool("CommanderPresent", false);
		}

		/// <summary>
		/// 	OnTriggerStay is called every FixedUpdate for every Collider that is
		/// 	touching the trigger.
		/// </summary>
		/// <param name="other">Other.</param>
		protected override void OnTriggerStay(Collider other)
		{
			base.OnTriggerStay(other);

			if (other.gameObject.tag == "Player")
				animator.SetTrigger("PlayerDetected");
		}

		/// <summary>
		/// 	Called to draw gizmos in the scene view.
		/// </summary>
		protected override void OnDrawGizmos()
		{
			base.OnDrawGizmos();

			Gizmos.color = Color.yellow;
			Gizmos.DrawWireSphere(transform.position, sphereCollider.radius);

			if (m_SquadLeader)
				Gizmos.DrawLine(transform.position, m_SquadLeader.transform.position);
		}

		#endregion

		#region Virtual Functions

		/// <summary>
		/// Called when the grunt gets damaged.
		/// </summary>
		/// <param name="damage">Amount of damage to apply.</param>
		/// <param name="collision">Collision info.</param>
		public virtual void Damage(int damage, CharacterLocomotorCollisionData collision)
		{
			//this.gameObject.SetActive(false);
			try
			{
				onDestroyedCallback.Invoke();
			}
			catch (System.NullReferenceException e)
			{
				Debug.LogFormat("No destroy callback set for {0}: {1}", this, e);
			}

			Destroy(this.gameObject);
		}

		#endregion
	}
}