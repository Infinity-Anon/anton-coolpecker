﻿using UnityEngine;

namespace AntonCoolpecker.Concrete.Enemies.GruntSquad
{
	/// <summary>
	/// Attacking player grunt - When grunt attacks player.
	/// </summary>
	public class AttackingPlayerGrunt : StateMachineBehaviour
	{
		#region Variables

		private GameObject m_PlayerCached;
		private NavMeshAgent m_NavMeshAgentCached;

		#endregion

		#region Override Functions

		/// <summary>
		/// Raises the state enter event.
		/// </summary>
		/// <param name="animator">Animator.</param>
		/// <param name="stateInfo">State info.</param>
		/// <param name="layerIndex">Layer index.</param>
		public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			m_NavMeshAgentCached = animator.gameObject.GetComponent<NavMeshAgent>();
			m_PlayerCached = GameObject.FindGameObjectWithTag("Player");
		}

		/// <summary>
		/// Raises the state update event.
		/// </summary>
		/// <param name="animator">Animator.</param>
		/// <param name="stateInfo">State info.</param>
		/// <param name="layerIndex">Layer index.</param>
		public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			m_NavMeshAgentCached.destination = m_PlayerCached.transform.position;
		}

		#endregion
	}
}