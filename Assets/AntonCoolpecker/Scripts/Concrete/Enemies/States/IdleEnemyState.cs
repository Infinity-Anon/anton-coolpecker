﻿using AntonCoolpecker.Abstract.Enemies.States;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Enemies.States
{
	/// <summary>
	/// Idle enemy state - When the enemy is idling around.
	/// </summary>
	public class IdleEnemyState : AbstractEnemyState
	{
		#region Variables

		[SerializeField] private AbstractEnemyState m_StateUponSeeingPlayer;

		#endregion

		#region Override Methods

		/// <summary>
		/// Get the current destination.
		/// Since we have nothing better to do, just return to the starting location.
		/// </summary>
		/// <param name="parent">Parent.</param>
		/// <returns>The current destination</returns>
		protected override Vector3 GetMotionTarget(MonoBehaviour parent)
		{
			EnemyController enemy = GetEnemyController(parent);
			return enemy.wanderStartPosition;
		}

		/// <summary>
		/// Decide what to do.
		/// In this case, if we can see the player, do whatever it is we do then.
		/// </summary>
		/// <param name="parent">Parent.</param>
		/// <returns>The next EnemyState</returns>
		public override AbstractEnemyState GetNextState(MonoBehaviour parent)
		{
			EnemyController enemy = GetEnemyController(parent);

			if (enemy.canSeePlayer && m_StateUponSeeingPlayer != null)
				return m_StateUponSeeingPlayer;

			return base.GetNextState(parent);
		}

		#endregion
	}
}