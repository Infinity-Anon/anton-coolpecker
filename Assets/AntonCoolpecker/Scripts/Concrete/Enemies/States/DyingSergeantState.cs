﻿using AntonCoolpecker.Abstract.Enemies.States;
using System.Collections;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Enemies.States
{
    /// <summary>
    /// Dying state for SergeAnt.
    /// TODO: Play death animations.
    /// </summary>
    public class DyingSergeantState : AbstractDyingState {}
}