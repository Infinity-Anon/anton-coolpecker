﻿using AntonCoolpecker.Abstract.Enemies.States;
using AntonCoolpecker.Concrete.Utils;
using AntonCoolpecker.Concrete.Enemies;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Enemies.States
{
    /// <summary>
    /// SergeAnt command state.  When the Sergeant sees the player, he issues a command to his troops before charging in himself.
    /// TODO: Point and shout/blow whistle animation
    /// TODO: Squad AI to make his troops actually charge
    /// </summary>
    public class CommandSergeantState : AbstractEnemyState
    {
		#region Variables

        [SerializeField] private Timer m_StateTimer;
        [SerializeField] private AbstractEnemyState m_TimerState;

        [SerializeField] private float m_CommandRadius; //Maximum ant commanding radius

		#endregion

		#region Properties

        /// <summary>
        /// 	Gets the state to transition to after the timer elapses.
        /// </summary>
        /// <value>The state of the timer.</value>
        protected override AbstractEnemyState timerState { get { return m_TimerState; } }

		/// <summary>
		/// Tells the controller whether to use CharacterLocomotor or NavMeshAgent for movement in the given state
		/// </summary>
		/// <value>The locomotion mode.</value>
        public override LocomotionMode locomotionMode
        {
            get { return LocomotionMode.Locomotor; }
        }

		#endregion

		#region Override Methods/Functions

        /// <summary>
        /// 	Gets the timer to transition to another state.
        /// </summary>
        /// <param name="parent">Parent.</param>
        public override Timer GetStateTimer(MonoBehaviour parent)
        {
            EnemyController enemy = GetEnemyController(parent);
            return enemy.stateTimer;
        }

        /// <summary>
        /// 	Called when the state becomes active.
        /// </summary>
        /// <param name="parent">Parent.</param>
        public override void OnEnter(MonoBehaviour parent)
        {
            SergeantController enemy = GetEnemyController(parent) as SergeantController;
            enemy.stateTimer = new Timer(m_StateTimer);

            base.OnEnter(parent);
        }

		/// <summary>
        ///     Would handle movement, but Sergeants do not move, so method is left blank.
        /// </summary>
        /// <param name="parent"></param>
        public override void HandleMovement(MonoBehaviour parent)
        {
            //Don't move
        }

		/// <summary>
		/// 	Returns a state for transition. Return self if no transition.
		/// </summary>
		/// <returns>The next state.</returns>
		/// <param name="parent">Parent.</param>
		public override AbstractEnemyState GetNextState(MonoBehaviour parent)
		{
			SergeantController enemy = GetEnemyController(parent) as SergeantController;

			if (GetStateTimer(parent) != null && GetStateTimer(parent).complete)
				enemy.soldiersList = PopulateSoldierList(enemy);

			return base.GetNextState(parent);
		}

		#endregion

		#region Private Methods

        /// <summary>
        /// Returns a list of enemies within the area of the Sergeant.
        /// </summary>
        /// <param name="enemy">Sergeant's enemy controller</param>
        /// <returns>List of enemies within the area.</returns>
        private List<EnemyController> PopulateSoldierList(EnemyController enemy)
        {
            List<EnemyController> soldierList = new List<EnemyController>();

            Collider[] collidersInPanicArea = FindFellowEnemies(enemy, m_CommandRadius);

            for (int i = 0; i < collidersInPanicArea.Length; i++)
            {
                EnemyController toCommand = collidersInPanicArea[i].GetComponent<EnemyController>();

                if (toCommand == null || toCommand == enemy)
                    continue;
				
                System.Type toCheck = toCommand.stateMachine.activeState.GetType();

                if (toCheck == typeof(AbstractDyingState))
                    continue;

                soldierList.Add(toCommand);
            }

            Debug.Log("Sergeant is commanding " + soldierList.Count + " enemies.");

            return soldierList;
        }

        /// <summary>
        /// Finds all colliders in the radius on the 14th Layermask and returns them in an array.
        /// </summary>
        /// <param name="enemy">Enemy this state belongs to.</param>
        /// <returns>Array of colliders on the 14th LayerMask</returns>
        private Collider[] FindFellowEnemies(EnemyController enemy, float radius)
        {
            LayerMask layerToGet = 1 << 14; //GET ENEMY LAYER

            return Physics.OverlapSphere(enemy.transform.position, radius, layerToGet);
        }

		#endregion
    }
}