﻿using AntonCoolpecker.Abstract.Enemies.States;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Enemies.States
{
	/// <summary>
	/// Moving to player enemy state.
	/// </summary>
	public class MovingToPlayerEnemyState : AbstractEnemyState
	{
		#region Variables

		[SerializeField] private AbstractEnemyState m_StateUponLosingPlayer;
		[SerializeField] private AbstractEnemyState m_StateUponReachingPlayer;

		[SerializeField] private Vector3 m_TargetOffset; //Target offset
		[SerializeField] private float m_PlayerReachingDistance; //Minimum distance required to consider the player as reached
		[SerializeField] private float m_AimingLeewayDegrees = 10.0f; //Leeway for when the ant aims towards the player

		#endregion

		#region Override Methods

		/// <summary>
		/// 	Gets the target towards which this enemy is trying to move.
		/// </summary>
		/// <returns>The motion target.</returns>
		/// <param name="parent">Parent.</param>
		protected override Vector3 GetMotionTarget(MonoBehaviour parent)
		{
			return player.transform.position + m_TargetOffset;
		}

		/// <summary>
		/// 	Returns a state for transition. Return self if no transition.
		/// </summary>
		/// <returns>The next state.</returns>
		/// <param name="parent">Parent.</param>
		public override AbstractEnemyState GetNextState(MonoBehaviour parent)
		{
			EnemyController enemy = GetEnemyController(parent);

			if (!enemy.canSeePlayer && m_StateUponLosingPlayer != null)
				return m_StateUponLosingPlayer;

			Vector3 playerpos = player.transform.position + m_TargetOffset;

			if (!enemy.movesIn3D)
				playerpos.y = enemy.transform.position.y;

			if (m_StateUponReachingPlayer != null &&
				Vector3.Distance(playerpos, enemy.transform.position) < m_PlayerReachingDistance &&
				enemy.IsAimedHorizontallyAt(playerpos, m_AimingLeewayDegrees))
				return m_StateUponReachingPlayer;

			return base.GetNextState(parent);
		}

		#endregion
	}
}