﻿using AntonCoolpecker.Abstract.Enemies.States;
using System.Collections;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Enemies.States
{
	/// <summary>
	/// The state to enter when the enemy has finished playing death animations
	/// and is about to be removed from the scene. Should only be used to clean
	/// up things that acknowledge the existence of this enemy. Any actual
	/// functionality related to death should go in the appropriate DyingState.
	/// </summary>
	public class DeadState : AbstractEnemyState
	{
		#region Override Functions

		/// <summary>
		/// Called when the state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnEnter(MonoBehaviour parent)
		{
			GetEnemyController(parent).FinishDying();
			GameObject.Destroy(parent.gameObject);
		}

		#endregion
	}
}