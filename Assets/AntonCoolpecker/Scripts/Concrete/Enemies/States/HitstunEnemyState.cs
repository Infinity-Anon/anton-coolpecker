﻿using AntonCoolpecker.Abstract.Enemies.States;
using AntonCoolpecker.Concrete.Utils;
using AntonCoolpecker.Utils.Time;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Enemies.States
{
	/// <summary>
	/// Hitstun enemy state - Base class for all enemy hitstun states.
	/// </summary>
	public class HitstunEnemyState : AbstractEnemyState
	{
		#region Variables

		[SerializeField] private Timer m_StateTimer;
		[SerializeField] private AbstractEnemyState m_TimerState;

		[SerializeField] private float m_HitstunForceVertical = 8.0f; //Vertical hitstun force when damaged/hit
		[SerializeField] private float m_HitstunForceHorizontal = 8.0f; //Horizontal hitstun force when damaged/hit
		[SerializeField] private float m_Acceleration = 0.5f; //Enemy acceleration during hitstun
        [SerializeField] private bool m_FacePlayer; //Should the enemy face the player during hitstun?

		private Vector3 m_Source; //Origin position of hitstun

		#endregion

		#region Properties

		/// <summary>
		/// 	Gets the state to transition to after the timer elapses.
		/// </summary>
		/// <value>The state of the timer.</value>
		protected override AbstractEnemyState timerState { get { return m_TimerState; } }

		/// <summary>
		/// Tells the controller whether to use CharacterLocomotor or NavMeshAgent for movement in the given state
		/// </summary>
		/// <value>The locomotion mode.</value>
		public override AbstractEnemyState.LocomotionMode locomotionMode
		{
			get { return AbstractEnemyState.LocomotionMode.Locomotor; }
		}

		/// <summary>
		/// 	The origin point of the damage, in world coordinates.
		/// </summary>
		/// <value>The damage source coordinates.</value>
		public Vector3 source { get { return m_Source; } set { m_Source = value; } }

		/// <summary>
		/// 	Gets the vertical force of the hitstun.
		/// </summary>
		/// <value>The force of the hitstun.</value>
		public float hitstunForceVertical { get { return m_HitstunForceVertical; } }

		/// <summary>
		/// 	Gets the horizontal force of the hitstun.
		/// </summary>
		/// <value>The force of the hitstun.</value>
		public float hitstunForceHorizontal { get { return m_HitstunForceHorizontal; } }

		#endregion

		#region Override Functions/Methods

		/// <summary>
		/// 	Gets the timer to transition to another state.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override Timer GetStateTimer(MonoBehaviour parent)
		{
			EnemyController enemy = GetEnemyController(parent);

			return enemy.stateTimer;
		}

		/// <summary>
		/// 	Called when the state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnEnter(MonoBehaviour parent)
		{
			EnemyController enemy = GetEnemyController(parent);

			enemy.stateTimer = new Timer(m_StateTimer);

            if (m_FacePlayer)
            {
                Vector3 playerPos = player.transform.position;
                playerPos.y = enemy.transform.position.y;
                enemy.transform.LookAt(playerPos);
            }

			base.OnEnter(parent);
		}

		/// <summary>
		/// Choose and execute the movement strategy. Called every FixedUpdate
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void HandleMovement(MonoBehaviour parent)
		{
			/*EnemyController enemy = GetEnemyController(parent);
			Vector3 currentV = enemy.characterLocomotor.velocity;
			float currentSpeed = currentV.magnitude;
			float newSpeed = Mathf.Max(0, currentSpeed - (m_Acceleration * GameTime.fixedDeltaTime));
			enemy.characterLocomotor.velocity = currentV.normalized * newSpeed;*/
		}

		#endregion
	}
}