﻿using AntonCoolpecker.Abstract.Enemies.States;
using System.Collections;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Enemies.States
{
	/// <summary>
	/// Dying state for PunchingBag enemy.
	/// </summary>
	public class DyingPunchingBagState : AbstractDyingState {}
}