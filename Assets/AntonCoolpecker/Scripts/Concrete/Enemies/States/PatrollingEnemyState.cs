﻿using AntonCoolpecker.Abstract.Enemies.States;
using AntonCoolpecker.Concrete.Utils;
using Hydra.HydraCommon.Utils;
using System;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Enemies.States
{
	/// <summary>
	/// Patrolling enemy state - When the enemy is patrolling in a certain path/area.
	/// </summary>
    public class PatrollingEnemyState : IdleEnemyState
    {
		#region Variables

        [SerializeField] float RequiredPointProximity = 0.2f; //Minimal distance required to consider a specific wander point reached

		#endregion

		#region Functions

		/// <summary>
		/// Increases or decreases the patrol path index(Which point in the patrol path that the enemy is at).
		/// </summary>
		/// <param name="Enemy">Enemy.</param>
        void IncreaseIndex(EnemyController Enemy)
        {
            if (Enemy.patrolPath.TrueLoopPathFalseReversePath)
            {
				if (Enemy.patrolIndex == Enemy.patrolPath.Points.Length - 1)
					Enemy.patrolIndex = 0;
                else
					Enemy.patrolIndex++;
            }
            else
            {
                if (!Enemy.walkingBack)
                {
					if (Enemy.patrolIndex == Enemy.patrolPath.Points.Length - 1)
                    {
						Enemy.patrolIndex--;
                        Enemy.walkingBack= true;
                    }
                    else
						Enemy.patrolIndex++;
                }
                else
                {
					if (Enemy.patrolIndex == 0)
                    {
						Enemy.patrolIndex++;
                        Enemy.walkingBack = false;
                    }
                    else
						Enemy.patrolIndex--;
                }
            }
        }

		#endregion

		#region Override Functions/Methods

		/// <summary>
		/// Called when the state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
        public override void OnEnter(MonoBehaviour parent)
        {
            base.OnEnter(parent);

            EnemyController enemy = GetEnemyController(parent);
            enemy.patrolTimer.maxTime = 0;
            
            enemy.patrolIndex = enemy.patrolPath.GetClosestPointIndex(enemy.transform.position);
            enemy.enteredPatrol = true;

            enemy.currentWanderTarget = enemy.patrolPath.GetClosestPoint(enemy.patrolIndex, enemy.transform.position,enemy);
        }

        /// <summary>
        /// Get the current destination.
        /// This is either a semi-static location or defined by a TrackFollower.
        /// </summary>
        /// <param name="parent">Parent</param>
        /// <returns>The current destination, in world space.</returns>
        protected override Vector3 GetMotionTarget(MonoBehaviour parent)
        {
            EnemyController enemy = GetEnemyController(parent);

            return enemy.currentWanderTarget; //enemy.currentWanderTarget then calls the track follower if necessary.
        }

		/// <summary>
		/// Choose and execute the movement strategy. Called every FixedUpdate
		/// </summary>
		/// <param name="parent">Parent.</param>
        public override void HandleMovement(MonoBehaviour parent)
        {
            EnemyController enemy = GetEnemyController(parent);

            if (enemy.patrolTimer.complete)
                base.HandleMovement(parent);
        }

        /// <summary>
        /// Do whatever we want to do this tick.
        /// This includes choosing a new wander target.
        /// </summary>
        /// <param name="parent">Parent.</param>
        public override void OnFixedUpdate(MonoBehaviour parent)
        {
            base.OnFixedUpdate(parent);

            EnemyController enemy = GetEnemyController(parent);

            if (Vector3.Distance(enemy.transform.position, enemy.currentWanderTarget) <= RequiredPointProximity)
            {
                //If this is our first check since entering, we need to check if this is an actual patrol point.

                if ((enemy.enteredPatrol && (Vector3.Distance(enemy.transform.position, enemy.patrolPath.Points[enemy.patrolIndex].position) <= RequiredPointProximity))
					|| !enemy.enteredPatrol)
                {
                    enemy.patrolTimer.maxTime = enemy.patrolPath.Points[enemy.patrolIndex].delay;
                    enemy.enteredPatrol = false;
					enemy.patrolTimer.Reset();
                }
            
                IncreaseIndex(enemy);
				enemy.currentWanderTarget = enemy.patrolPath.Points[enemy.patrolIndex].position;
            }
        }

		#endregion
    }
}