﻿using AntonCoolpecker.Abstract.Enemies.States;
using AntonCoolpecker.Concrete.Utils;
using AntonCoolpecker.Utils.Time;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Enemies.States
{
    /// <summary>
    /// Cooldown enemy state - By default, makes the enemy stand still for a few seconds.
	/// Similar to HitstunEnemyState, except the ant is not invincible.
    /// </summary>
    public class CooldownEnemyState : AbstractEnemyState
    {
		#region Variables

        [SerializeField] private Timer m_StateTimer;
        [SerializeField] private AbstractEnemyState m_TimerState;
        [SerializeField] private float m_Acceleration = 0.5f;

        private Vector3 m_Source; //TODO: WHAT IS THE INTENTION OF THIS VARIABLE?

		#endregion

		#region Properties

        /// <summary>
        /// 	Gets the state to transition to after the timer elapses.
        /// </summary>
        /// <value>The state of the timer.</value>
        protected override AbstractEnemyState timerState { get { return m_TimerState; } }

		/// <summary>
		/// Tells the controller whether to use CharacterLocomotor or NavMeshAgent for movement in the given state
		/// </summary>
		/// <value>The locomotion mode.</value>
        public override AbstractEnemyState.LocomotionMode locomotionMode
        {
            get { return AbstractEnemyState.LocomotionMode.Locomotor; }
        }

		#endregion

		#region Override Functions/Methods

        /// <summary>
        /// 	Gets the timer to transition to another state.
        /// </summary>
        /// <param name="parent">Parent.</param>
        public override Timer GetStateTimer(MonoBehaviour parent)
        {
            EnemyController enemy = GetEnemyController(parent);

            return enemy.stateTimer;
        }

        /// <summary>
        /// 	Called when the state becomes active.
        /// </summary>
        /// <param name="parent">Parent.</param>
        public override void OnEnter(MonoBehaviour parent)
        {
            EnemyController enemy = GetEnemyController(parent);

            enemy.stateTimer = new Timer(m_StateTimer);

            base.OnEnter(parent);
        }

		/// <summary>
		/// Choose and execute the movement strategy. Called every FixedUpdate
		/// </summary>
		/// <param name="parent">Parent.</param>
        public override void HandleMovement(MonoBehaviour parent)
        {
            EnemyController enemy = GetEnemyController(parent);

            Vector3 currentV = enemy.characterLocomotor.velocity;
            float currentSpeed = currentV.magnitude;
            float newSpeed = Mathf.Max(0, currentSpeed - (m_Acceleration * GameTime.deltaTime));

            enemy.characterLocomotor.velocity = currentV.normalized * newSpeed;
        }

		#endregion
    }
}