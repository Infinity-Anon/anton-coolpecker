﻿using AntonCoolpecker.Abstract.Enemies.States;
using AntonCoolpecker.Concrete.Utils;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Enemies.States
{
	/// <summary>
	/// Tackling soldier state - The enemy tried to attack the player by doing a tackle jump towards the player
	/// </summary>
    public class TacklingSoldierState : AbstractEnemyState
    {
		#region Variables

        [SerializeField] private float m_JumpDistance = 6.0f; //How far the enemy should jump
        [SerializeField] private float m_GravityModifier = 1.0f; //Enemy gravity modifier
        [SerializeField] private float m_WarmUpTime = 0.5f; //Time before the enemy executes the jump

        [SerializeField] private Timer m_StateTimer; 
        [SerializeField] private AbstractEnemyState m_CooldownState; //Cool down the enemy after the tackling jump

		#endregion

		#region Properties

        /// <summary>
        /// 	Gets the state to transition to after the timer elapses.
        /// </summary>
        /// <value>The state of the timer.</value>
        protected override AbstractEnemyState timerState { get { return HitstunState; } }

        /// <summary>
        /// 	Tells the controller whether to use CharacterLocomotor or NavMeshAgent for movement in the state.
        /// </summary>
        /// <value>The locomotion mode.</value>
        public override AbstractEnemyState.LocomotionMode locomotionMode
        {
            get { return AbstractEnemyState.LocomotionMode.Locomotor; }
        }

		#endregion

		#region Override Functions/Methods

        /// <summary>
        /// 	Gets the timer to transition to another state.
        /// </summary>
        /// <param name="parent">Parent.</param>
        public override Timer GetStateTimer(MonoBehaviour parent)
        {
            EnemyController enemy = GetEnemyController(parent);

            return enemy.stateTimer;
        }

		/// <summary>
		/// Called when the state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
        public override void OnEnter(MonoBehaviour parent)
        {
            EnemyController enemy = GetEnemyController(parent);

            enemy.stateTimer = new Timer(m_StateTimer);

            base.OnEnter(parent);
        }

        /// <summary>
        /// 	Called when the parent's physics update.
        /// </summary>
        /// <param name="parent">Parent.</param>
        public override void OnFixedUpdate(MonoBehaviour parent)
        {
            base.OnFixedUpdate(parent);

            EnemyController enemy = GetEnemyController(parent);

            if (GetStateTimer(parent).elapsed > m_WarmUpTime && enemy.characterLocomotor.isGrounded)
                Jump(parent);
        }

		/// <summary>
		/// Called before another state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
        public override void OnExit(MonoBehaviour parent)
        {
            base.OnExit(parent);

            EnemyController enemy = GetEnemyController(parent);

            enemy.characterLocomotor.ResetGravityScale();
        }

        /// <summary>
        /// 	Gets the target towards which this enemy is trying to move.
        /// </summary>
        /// <returns>The motion target.</returns>
        /// <param name="parent">Parent.</param>
        protected override Vector3 GetMotionTarget(MonoBehaviour parent)
        {
            return parent.transform.position + parent.transform.forward;
        }

        /// <summary>
        /// 	Returns a state for transition. Return self if no transition.
        /// </summary>
        /// <returns>The next state.</returns>
        /// <param name="parent">Parent.</param>
        public override AbstractEnemyState GetNextState(MonoBehaviour parent)
        {
            EnemyController enemy = GetEnemyController(parent);

            if (enemy.characterLocomotor.isGrounded && enemy.stateTimer.elapsed > m_WarmUpTime + GetJumpTime(enemy) / 2.0f)
                return m_CooldownState;

            return base.GetNextState(parent);
        }

		#endregion

		#region Functions/Methods

		/// <summary>
		/// Called when the enemy jumps towards the player.
		/// </summary>
		/// <param name="parent">Parent.</param>
		private void Jump(MonoBehaviour parent)
		{
			EnemyController enemy = GetEnemyController(parent);

			enemy.characterLocomotor.gravityScale *= m_GravityModifier;

			enemy.characterLocomotor.velocity = enemy.characterLocomotor.flatVelocity +
				Vector3.up * GetJumpForce(enemy);
		}

		/// <summary>
		/// 	Calculates the jump force necessary for an enemy to jump a certain distance.
		/// </summary>
		/// <returns>The necessary force.</returns>
		/// <param name="jumpDistance">Jump distance.</param>
		/// <param name="enemy">Enemy.</param>
		public float GetJumpForce(EnemyController enemy)
		{
			return -enemy.characterLocomotor.gravity.y * GetJumpTime(enemy) / 2.0f;
		}

		/// <summary>
		/// 	Gets the jump time necessary for an enemy to jump a certain distance.
		/// </summary>
		/// <returns>The necessary time.</returns>
		/// <param name="enemy">Enemy.</param>
		public float GetJumpTime(EnemyController enemy)
		{
			return m_JumpDistance / GetSpeed(enemy);
		}

		#endregion
    }
}