﻿using AntonCoolpecker.Abstract.Enemies.States;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Enemies.States
{
	/// <summary>
	/// Facing player enemy state - When the enemy tries to face the player
	/// </summary>
	public class FacingPlayerEnemyState : AbstractEnemyState
	{
		#region Variables

		[SerializeField] private AbstractEnemyState m_StateUponLosingPlayer;
		[SerializeField] private AbstractEnemyState m_StateWhenAimedAtPlayer;
		[SerializeField] private float m_AimingLeewayDegrees; //Leeway for aiming towards the player in degrees

		public override LocomotionMode locomotionMode { get { return LocomotionMode.Locomotor; } }

		#endregion

		#region Override Functions/Methods

		/// <summary>
		/// Called when the state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnEnter(MonoBehaviour parent)
		{
			base.OnEnter(parent);
		}

		/// <summary>
		/// Called before another state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnExit(MonoBehaviour parent)
		{
			base.OnExit(parent);
		}

		/// <summary>
		/// Rotate to an appropriate direction, given the desired velocity
		/// </summary>
		/// <param name="enemy">The EnemyController that is rotating.</param>
		/// <param name="movement">The desired velocity vector.</param>
		protected override void ResolveRotation(EnemyController enemy, Vector3 movement)
		{
			Vector3 position = player.transform.position - enemy.transform.position;
			position.y = 0;
			RotateTowardsVector(enemy, position);
		}

		/// <summary>
		/// 	Returns a state for transition. Return self if no transition.
		/// </summary>
		/// <returns>The next state.</returns>
		/// <param name="parent">Parent.</param>
		public override AbstractEnemyState GetNextState(MonoBehaviour parent)
		{
			EnemyController enemy = GetEnemyController(parent);

			if (!enemy.canSeePlayer && m_StateUponLosingPlayer != null)
				return m_StateUponLosingPlayer;

			if (enemy.IsAimedHorizontallyAt(player.transform.position, m_AimingLeewayDegrees) && m_StateWhenAimedAtPlayer != null)
				return m_StateWhenAimedAtPlayer;

			return base.GetNextState(parent);
		}

		#endregion
	}
}