﻿using AntonCoolpecker.Abstract.Enemies.States;
using AntonCoolpecker.Concrete.HitBoxes;
using AntonCoolpecker.Concrete.Utils;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Enemies.States
{
	/// <summary>
	/// Projectile shooting enemy state - When the enemy is shooting projectiles towards the player.
	/// </summary>
    public class ProjectileShootingEnemyState : AbstractEnemyState
    {
		#region Variables

        [SerializeField] private bool m_UseDefaultProjectile = true; //Use default if true, using homing if false
        [SerializeField] private Projectile m_ProjectilePrefab; //Reference to regular projectile
		[SerializeField] private Projectile m_HomingProjectilePrefab; //Reference to homing projectile
        [SerializeField] private float m_ProjectileSpeed; //Velocity of projectile
        [SerializeField] private Vector3 m_RelativeProjectileOffset; //Projectile spawn position offset
        [SerializeField] private float m_WarmUpTime; //Time interval between every projectile shootout

        [SerializeField] private Timer m_StateTimer;
        [SerializeField] private AbstractEnemyState m_TimerState; 

		#endregion

		#region Properties

		/// <summary>
		/// Tells the controller whether to use CharacterLocomotor or NavMeshAgent for movement in the given state
		/// </summary>
		/// <value>The locomotion mode.</value>
        public override LocomotionMode locomotionMode { get { return LocomotionMode.Locomotor; } }

        /// <summary>
        /// 	Gets the state to transition to after the timer elapses.
        /// </summary>
        /// <value>The state of the timer.</value>
        protected override AbstractEnemyState timerState { get { return m_TimerState; } }

		#endregion

		#region Override Functions/Methods

        /// <summary>
        /// 	Gets the timer to transition to another state.
        /// </summary>
        /// <param name="parent">Parent.</param>
        public override Timer GetStateTimer(MonoBehaviour parent)
        {
            EnemyController enemy = GetEnemyController(parent);
            return enemy.stateTimer;
        }

        /// <summary>
        /// 	Called when the state becomes active.
        /// </summary>
        /// <param name="parent">Parent.</param>
        public override void OnEnter(MonoBehaviour parent)
        {
            EnemyController enemy = GetEnemyController(parent);

            enemy.hasFiredProjectile = false;
            enemy.stateTimer = new Timer(m_StateTimer);

            base.OnEnter(parent);
        }

        /// <summary>
        /// 	Called when the parent updates.
        /// </summary>
        /// <param name="parent">Parent.</param>
        public override void OnUpdate(MonoBehaviour parent)
        {
            base.OnUpdate(parent);

            EnemyController enemy = GetEnemyController(parent);

            if (!enemy.hasFiredProjectile && GetStateTimer(parent).remaining < GetStateTimer(parent).maxTime - m_WarmUpTime)
                ShootProjectile(parent);
        }

		#endregion

		#region Virtual Functions

        /// <summary>
        /// 	Shoots a projectile in the default direction.
        /// </summary>
        /// <param name="parent">Parent.</param>
        public virtual void ShootProjectile(MonoBehaviour parent)
        {
            ShootProjectile(parent.transform.forward, parent);
            // TODO: Account for projectiles that arc.
        }

		#endregion

		#region Public Functions

        /// <summary>
        /// 	Shoots a projectile in a specified direction.
        /// </summary>
        /// <param name="parent">Parent.</param>
        /// <param name="direction">Direction.</param>
        public void ShootProjectile(Vector3 direction, MonoBehaviour parent)
        {
			Projectile proj;

            EnemyController enemy = GetEnemyController(parent);

            enemy.hasFiredProjectile = true;

			if (m_UseDefaultProjectile == true)
			{
				proj = Instantiate(m_ProjectilePrefab, parent.transform.TransformPoint(m_RelativeProjectileOffset), parent.transform.rotation) as Projectile;
			}
			else
			{
				proj = Instantiate(m_HomingProjectilePrefab, parent.transform.TransformPoint(m_RelativeProjectileOffset), parent.transform.rotation) as Projectile;
			}

            ///BULLET TRACKING LOGIC//
            Vector3 playerpos = player.transform.position + new Vector3(0, 1, 0);
            proj.transform.LookAt(playerpos);
        }

		#endregion
    }
}