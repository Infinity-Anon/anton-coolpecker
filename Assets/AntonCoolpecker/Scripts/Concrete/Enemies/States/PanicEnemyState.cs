﻿using AntonCoolpecker.Abstract.Enemies.States;
using AntonCoolpecker.Concrete.Enemies;
using System.Collections;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Enemies.States
{
	/// <summary>
	/// Panic enemy state.
	/// </summary>
	public class PanicEnemyState : AbstractEnemyState
	{
		#region Variables

		/// <summary>
		/// 
		/// Panic types:
		/// 
		/// NonSpreadable - This self-contained panic type does not spread panic to other enemies/entities unless the enemy dies(Death penality).
		/// 
		/// Speadable - This panic type allows the enemy to spread non-spreadable panic to other enemies once, but no further.
		/// 
		/// Viral - This type allows panic to spread through all enemies in the manner of a tree sprouting roots in all levels.
		///         One enemy spreads panics to all ants that enemy can reach, whereach each other of the ants recieving that panic will spread
		///         panic to all ants that each of these can reach, etc.
		/// 
		/// Forced - This type works the same as NonSpreadable, except there's no panic spread when the enemy dies when using this panic type
		///          and the probability for panicking is 100 percents.
		/// 		 Used for e.g. forcing panic to all soldier ants under the command of the SergeAnt, once the SergeAnt dies.
		/// 
		/// </summary>
		public enum PanicType
		{
			NonSpredable,
			Spreadable,
			Viral,
			Forced
		}

	    [SerializeField] private AbstractEnemyState m_CalmState; //State enemy will go to upon leaving the panic state.
	    [SerializeField] private float m_RequiredCalmDistance; //Distance the enemy must be from the player before they are allowed to leave the panic state.
	    [SerializeField] private float m_FleeDistance; //How far the enemy flees given one update.
		[SerializeField] private float m_PanicChance; //Chance to fail a panic roll.
		[SerializeField] private float m_PanicSpreadChance; //Chance to spread panic on a failed panic roll.
	    [SerializeField] private float m_CompanionPanicMod; //Mod to panic roll chance while in a group.
		[SerializeField] private bool m_ForcePanicOnDeath; //When dies, causes all around them to panic instead of taking a panic roll.
		[SerializeField] private float m_SpreadRadius; //Radius that panic will spread.
	    [SerializeField] private float m_CompanionRadius; //Radius that fellow enemies inside of will grant their companion bonus against panic rolls.
	    [SerializeField] private bool m_Suicidal; //If true, enemy will run off the edge while panicking.
	    [SerializeField] private float m_DeathPanicPenalty; //The increased chance of an enemy panicking if this enemy dies.

		#endregion

		#region Properties

		/// <summary>
		/// Gets the panic companion bonus.
		/// </summary>
		/// <value>The panic companion bonus.</value>
	    public float companionBonus { get { return m_CompanionPanicMod; } }

		/// <summary>
		/// Gets the enemy's calm state
		/// </summary>
		/// <value>The enemy's calm state.</value>
	    public AbstractEnemyState calmState { get { return m_CalmState; } }

	    /// <summary>
	    /// Tells the controller whether to use CharacterLocomotor or NavMeshAgent for movement in the given state
	    /// </summary>
	    /// <value>The locomotion mode.</value>
	    public override LocomotionMode locomotionMode
	    {
	        get
	        {
	            if (m_Suicidal)
	                return LocomotionMode.Locomotor;
	            return LocomotionMode.NMAgent;
	        }
	    }

		#endregion

		#region Override Methods

	    /// <summary>
	    /// Get the current destination.
	    /// Since we have nothing better to do, just return to the starting location.
	    /// </summary>
	    /// <param name="parent">Parent.</param>
	    /// <returns>The current destination</returns>
	    protected override Vector3 GetMotionTarget(MonoBehaviour parent)
	    {
	        EnemyController enemy = GetEnemyController(parent);
	        Vector3 fleeDir = (enemy.transform.position - player.transform.position).normalized;
	        return enemy.transform.position + fleeDir * m_FleeDistance;
	    }

	    /// <summary>
	    /// Decide what to do.
	    /// In this case, if we can't see the player, stop panicking.
	    /// </summary>
	    /// <param name="parent">Parent.</param>
	    /// <returns>The next EnemyState</returns>
	    public override AbstractEnemyState GetNextState(MonoBehaviour parent)
	    {
	        EnemyController enemy = GetEnemyController(parent);

	        if(enemy.canSeePlayer || Vector3.Distance(enemy.transform.position, player.transform.position) < m_RequiredCalmDistance)
	            return base.GetNextState(parent);

	        return m_CalmState;
	    }

		#endregion

		#region Methods/Functions

	    /// <summary>
	    /// Compiles companion mod for panic rolls.
	    /// </summary>
	    /// <param name="enemy">Enemy this state belongs to.</param>
	    /// <returns></returns>
	    private float GetCompanionMod(EnemyController enemy)
	    {
	        float companionMod = 0f;
	        Collider[] collidersInPanicArea = FindFellowEnemies(enemy, m_CompanionRadius);
	        //Debug.Log("Panic assisted by " + collidersInPanicArea.Length + " enemies.");
	        for (int i = 0; i < collidersInPanicArea.Length; i++)
	        {
	            EnemyController toPanic = collidersInPanicArea[i].GetComponent<EnemyController>();
	            if (toPanic == null || toPanic == enemy)
	                continue;
	            System.Type toCheck = toPanic.stateMachine.activeState.GetType();
	            if (toCheck == typeof(PanicEnemyState) || toCheck == typeof(AbstractDyingState))
	                continue;

	            companionMod += toPanic.panicState.companionBonus;
	        }
	        return companionMod;
	    }

	    /// <summary>
	    /// Make Panic roll.
	    /// </summary>
	    /// <param name="enemy">Enemy this state belongs to.</param>
	    /// <returns></returns>
	    public bool PanicRoll(EnemyController enemy, float deathPenalty)
	    {
	        float companionMod = GetCompanionMod(enemy);

	        return Random.Range(0f, 1f) + companionMod <= m_PanicChance + deathPenalty;
	    }

	    /// <summary>
	    /// Set panic system in motion.
	    /// </summary>
	    /// <param name="type">Spread-type</param>
	    public void Panic(PanicType type, EnemyController enemy, float deathPenalty)
	    {
	        if (type == PanicType.Forced)
	        {
	            BeginPanic(enemy);
	            return;
	        }

	        if (PanicRoll(enemy, deathPenalty))
	        {
	            BeginPanic(enemy);

	            if (type == PanicType.NonSpredable || !TestPanicSpread())
	                return;
				
	            if (type == PanicType.Spreadable)
	                SpreadPanic(enemy, deathPenalty);
				
	            if (type == PanicType.Viral)
	                ViralPanic(enemy, deathPenalty);
	        }
	    }

	    /// <summary>
	    /// Returns true if roll is equal to or less than PanicSpreadChance
	    /// </summary>
	    /// <returns></returns>
	    public bool TestPanicSpread()
	    {
	        if (Random.Range(0f, 1f) <= m_PanicSpreadChance)
	            return true;
	        return false;
	    }

	    /// <summary>
	    /// Change enemy state to panic.
	    /// </summary>
	    public void BeginPanic(EnemyController enemy)
	    {
	        enemy.stateMachine.SetActiveState(enemy.panicState, enemy);
	    }

	    /// <summary>
	    /// Type of panic that spreads to nearby enemies.
	    /// </summary>
	    public void SpreadPanic(EnemyController enemy, float deathPenalty)
	    {
	        Collider[] collidersInPanicArea = FindFellowEnemies(enemy, m_SpreadRadius);

	        for (int i = 0; i < collidersInPanicArea.Length; i++)
	        {
	            EnemyController toPanic = collidersInPanicArea[i].GetComponent<EnemyController>();

	            if (toPanic == null || toPanic.stateMachine.activeState.GetType() == typeof(PanicEnemyState))
	                continue;

	            toPanic.panicState.Panic(PanicType.NonSpredable, toPanic, deathPenalty);
	        }
	    }

	    /// <summary>
	    /// Type of panic that spreads to nearby enemies upon death.
	    /// </summary>
	    public void SpreadDeathPanic(EnemyController enemy)
	    {
	        Collider[] collidersInPanicArea = FindFellowEnemies(enemy, m_SpreadRadius);
	        for (int i = 0; i < collidersInPanicArea.Length; i++)
	        {
	            EnemyController toPanic = collidersInPanicArea[i].GetComponent<EnemyController>();

	            if (toPanic == null || toPanic == enemy || toPanic.stateMachine.activeState.GetType() == typeof(PanicEnemyState))
	                continue;
				
	            if (m_ForcePanicOnDeath)
	                toPanic.panicState.Panic(PanicType.Forced, toPanic, 0);
	            else
	                toPanic.panicState.Panic(PanicType.NonSpredable, toPanic, m_DeathPanicPenalty);
	        }
	    }

	    /// <summary>
	    /// Finds all colliders in the radius on the 14th Layermask and returns them in an array.
	    /// </summary>
	    /// <param name="enemy">Enemy this state belongs to.</param>
	    /// <returns>Array of colliders on the 14th LayerMask</returns>
	    private Collider[] FindFellowEnemies(EnemyController enemy, float radius)
	    {
	        LayerMask layerToGet = 1 << 14; //GET ENEMY LAYER

	        return Physics.OverlapSphere(enemy.transform.position, radius, layerToGet);
	    }

	    /// <summary>
	    /// A form of panic that spreads forever. Very laggy.
	    /// </summary>
	    private void ViralPanic(EnemyController enemy, float deathPenalty)
	    {
	        Collider[] collidersInPanicArea = FindFellowEnemies(enemy, m_SpreadRadius);

	        for (int i = 0; i < collidersInPanicArea.Length; i++)
	        {
	            EnemyController toPanic = collidersInPanicArea[i].GetComponent<EnemyController>();

	            if (toPanic == null || toPanic.stateMachine.activeState.GetType() == typeof(PanicEnemyState))
	                continue;

	            toPanic.panicState.Panic(PanicType.Viral, toPanic, deathPenalty);
	        }
	    }

		#endregion
	}
}