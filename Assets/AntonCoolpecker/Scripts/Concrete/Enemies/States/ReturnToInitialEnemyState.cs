﻿using AntonCoolpecker.Abstract.Enemies.States;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Enemies.States
{
	/// <summary>
	/// Utility state that immediately transitions to the starting state of the state machine.
	/// Useful for when the same enemy may have different starting states.
	/// </summary>
	public class ReturnToInitialEnemyState : AbstractEnemyState
	{
		#region Override Methods

		/// <summary>
		/// Returns a state for transition. Return self if no transition.
		/// </summary>
		/// <returns>The next state.</returns>
		/// <param name="parent">Parent.</param>
		public override AbstractEnemyState GetNextState(MonoBehaviour parent)
		{
			EnemyController enemy = GetEnemyController(parent);

			return enemy.stateMachine.initialState;
		}

		#endregion
	}
}