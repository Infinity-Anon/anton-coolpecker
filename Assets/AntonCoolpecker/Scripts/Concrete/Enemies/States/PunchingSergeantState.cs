﻿using AntonCoolpecker.Abstract.Enemies.States;
using AntonCoolpecker.Concrete.Utils;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Enemies.States
{
	/// <summary>
	/// Punching sergeant state - When the Serge Ant tries to punch the player.
	/// </summary>
    public class PunchingSergeantState : AbstractEnemyState
    {
		#region Variables

        [SerializeField] private float m_WarmUpTime = 0.5f; //Time before the sergeant tries to punch the player
        [SerializeField] private float m_PunchTime = 1f; //Max time length of this state

        [SerializeField] private Timer m_StateTimer;
        [SerializeField] private AbstractEnemyState m_CooldownState;

		#endregion

		#region Properties

        /// <summary>
        /// 	Gets the state to transition to after the timer elapses.
        /// </summary>
        /// <value>The state of the timer.</value>
        protected override AbstractEnemyState timerState { get { return HitstunState; } }

        /// <summary>
        /// 	Tells the controller whether to use CharacterLocomotor or NavMeshAgent for movement in the state.
        /// </summary>
        /// <value>The locomotion mode.</value>
        public override AbstractEnemyState.LocomotionMode locomotionMode
        {
            get { return AbstractEnemyState.LocomotionMode.Locomotor; }
        }

		#endregion

		#region Override Functions/Methods

        /// <summary>
        /// 	Gets the timer to transition to another state.
        /// </summary>
        /// <param name="parent">Parent.</param>
        public override Timer GetStateTimer(MonoBehaviour parent)
        {
            EnemyController enemy = GetEnemyController(parent);

            return enemy.stateTimer;
        }

		/// <summary>
		/// Called when the state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
        public override void OnEnter(MonoBehaviour parent)
        {
            EnemyController enemy = GetEnemyController(parent);

            enemy.stateTimer = new Timer(m_StateTimer);

            base.OnEnter(parent);
        }

        /// <summary>
        /// 	Called when the parent's physics update.
        /// </summary>
        /// <param name="parent">Parent.</param>
        public override void OnFixedUpdate(MonoBehaviour parent)
        {
            base.OnFixedUpdate(parent);

            EnemyController enemy = GetEnemyController(parent);

            if (GetStateTimer(parent).elapsed > m_WarmUpTime)
                Punch(parent);
        }

		/// <summary>
		/// Called before another state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
        public override void OnExit(MonoBehaviour parent)
        {
            base.OnExit(parent);

            EnemyController enemy = GetEnemyController(parent);

            enemy.hitBox.SetActive(false);
        }

        /// <summary>
        /// 	Returns a state for transition. Return self if no transition.
        /// </summary>
        /// <returns>The next state.</returns>
        /// <param name="parent">Parent.</param>
        public override AbstractEnemyState GetNextState(MonoBehaviour parent)
        {
            EnemyController enemy = GetEnemyController(parent);

            if (enemy.characterLocomotor.isGrounded && enemy.stateTimer.elapsed > m_WarmUpTime + m_PunchTime)
            {
                return m_CooldownState;
            }

            return base.GetNextState(parent);
        }

		#endregion

		#region Private Functions

		/// <summary>
		/// Called when the Serge Ant tries to punch.
		/// </summary>
		/// <param name="parent">Parent.</param>
		private void Punch(MonoBehaviour parent)
		{
			EnemyController enemy = GetEnemyController(parent);
			enemy.hitBox.SetActive(true);
		}

		#endregion
    }
}