﻿using AntonCoolpecker.Abstract.Enemies.States;
using System.Collections;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Enemies.States
{
	/// <summary>
	/// Dying state for Flying Ant.
	/// TODO: play dying animations. Crash to ground with trail of smoke?
	/// </summary>
	public class DyingFlierState : AbstractDyingState {}
}