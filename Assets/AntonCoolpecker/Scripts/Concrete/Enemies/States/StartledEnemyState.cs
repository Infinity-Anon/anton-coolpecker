﻿using AntonCoolpecker.Abstract.Enemies.States;
using AntonCoolpecker.Concrete.Utils;
using AntonCoolpecker.Utils;
using AntonCoolpecker.Utils.Time;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Enemies.States
{
	/// <summary>
	/// Startled enemy state.
	/// </summary>
    public class StartledEnemyState : AbstractEnemyState
    {
		#region Variables

        [Tweakable("Startle")] [SerializeField] private float m_StartleProbability = 0.5f; //How probable it is that the enemy is startled
        [Tweakable("Startle")] [SerializeField] private float m_JumpHeight = 0.5f; //Startle jump height
		[Tweakable("Startle")] [SerializeField] private bool m_OnlyStartleFirstTime = true; //Should the enemy be only started the first time it sees the player?
        [Tweakable("Startle")] [SerializeField] private bool m_TurnTowardsPlayer = true; //Should the enemy turn towards the player?

		[SerializeField] private Timer m_StateTimer;
		//[SerializeField] private Timer m_ImmunityTimer;
        [SerializeField] private AbstractEnemyState m_TimerState;

		#endregion

		#region Properties

		/// <summary>
		/// 	Gets the state to transition to after the timer elapses.
		/// </summary>
		/// <value>The state of the timer.</value>
		protected override AbstractEnemyState timerState { get { return m_TimerState; } }

		/// <summary>
		/// Tells the controller whether to use CharacterLocomotor or NavMeshAgent for movement in the given state
		/// </summary>
		/// <value>The locomotion mode.</value>
        public override LocomotionMode locomotionMode
        {
            get { return LocomotionMode.Locomotor; }
        }

		#endregion

		#region Override Methods/Functions

        /// <summary>
        /// 	Called when the state becomes active.
        /// </summary>
        /// <param name="parent">Parent.</param>
        public override void OnEnter(MonoBehaviour parent)
        {
            EnemyController enemy = GetEnemyController(parent);

            base.OnEnter(parent);

            // If we have already encountered the player and are set to not restartle, skip startling
            if ((m_OnlyStartleFirstTime && enemy.hasEncounteredPlayer))// || !m_ImmunityTimer.complete)
            {
                enemy.stateMachine.SetActiveState(m_TimerState, parent);
            }
            // If we happen to have nerves of steel, complete startling
            else if (Random.value > m_StartleProbability)
            {
                enemy.stateTimer = new Timer(m_StateTimer);
                enemy.stateTimer.AddSeconds(enemy.stateTimer.maxTime);
            }
            // Else, startle
            else
            {
                Jump(parent);
                enemy.stateTimer = new Timer(m_StateTimer);
                enemy.stateTimer.Reset();
            }

            //m_ImmunityTimer = new Timer(m_ImmunityTimer);
            //m_ImmunityTimer.Reset();

            enemy.hasEncounteredPlayer = true;
        }

        /// <summary>
        /// Set the motion target to be the player, so that we turn and face him.
        /// </summary>
        protected override Vector3 GetMotionTarget(MonoBehaviour parent)
        {
            if (m_TurnTowardsPlayer)
            {
                return player.transform.position;
            }
            else
            {
                return parent.transform.position;
            }
        }

		/// <summary>
		/// Accelerate to the desired movement.
		/// </summary>
		/// <param name="enemy">The EnemyController that is moving</param>
		/// <param name="movement">The desired velocity vector</param>
        protected override void ResolveMovement(EnemyController enemy, Vector3 movement)
        {
            //Don't move
        }

		/// <summary>
		/// 	Gets the timer to transition to another state.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override Timer GetStateTimer(MonoBehaviour parent)
		{
			EnemyController enemy = GetEnemyController(parent);

			return enemy.stateTimer;
		}

		#endregion

		#region Functions/Methods

		/// <summary>
		/// Jump the specified parent.
		/// </summary>
		/// <param name="parent">Parent.</param>
		private void Jump(MonoBehaviour parent)
		{
			EnemyController enemy = GetEnemyController(parent);

			//enemy.characterLocomotor.gravityScale *= m_GravityModifier;

			enemy.characterLocomotor.velocity = enemy.characterLocomotor.flatVelocity +
				Vector3.up * GetJumpForce(enemy);
		}

		/// <summary>
		/// 	Calculates the jump force necessary for an enemy to jump a certain height.
		/// </summary>
		/// <returns>The necessary force.</returns>
		/// <param name="enemy">Enemy.</param>
		public float GetJumpForce(EnemyController enemy)
		{
			float f = Mathf.Sqrt(Mathf.Abs(2 * enemy.characterLocomotor.gravity.y * m_JumpHeight));
            return f;
		}

		/// <summary>
		/// Returns a state for transition. Return self if no transition.
		/// </summary>
		/// <returns>The next state.</returns>
		/// <param name="parent">Parent.</param>
        public override AbstractEnemyState GetNextState(MonoBehaviour parent)
        {
            EnemyController enemy = GetEnemyController(parent);

            if (GetStateTimer(parent) != null && GetStateTimer(parent).complete)
            {
                if (enemy.panicState.PanicRoll(enemy, 0))
                {
                    return enemy.panicState;
                }
                else
                {
                    return timerState;
                }
            }

            return base.GetNextState(parent);
        }

		#endregion
    }
}