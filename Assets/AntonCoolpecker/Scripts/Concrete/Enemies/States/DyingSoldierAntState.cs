﻿using AntonCoolpecker.Abstract.Enemies.States;
using System.Collections;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Enemies.States
{
	/// <summary>
	/// Dying state for Soldier Ant.
	/// TODO: Play death animations.
	/// </summary>
	public class DyingSoldierAntState : AbstractDyingState {}
}