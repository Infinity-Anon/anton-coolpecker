﻿using AntonCoolpecker.Abstract.Enemies.States;
using AntonCoolpecker.Concrete.Utils;
using Hydra.HydraCommon.Utils;
using System;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Enemies.States
{
	/// <summary>
	/// Wandering enemy state.
	/// </summary>
	public class WanderingEnemyState : IdleEnemyState
	{
		#region Variables

		/// <summary>
		/// How close we need to be to the current waypoint before starting to wait for the next waypoint.
		/// </summary>
		[SerializeField] private float m_TrackTimerPausingDistance = 0.1f;
		[SerializeField] private PatrollingEnemyState patrolState;

		#endregion

		#region Override Functions/Methods

		/// <summary>
		/// 	Called when the state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnEnter(MonoBehaviour parent)
		{
			base.OnEnter(parent);

			EnemyController enemy = GetEnemyController(parent);

			enemy.wanderTimer.Reset();
			enemy.currentWanderTarget = NewTarget(parent);
			
			if (enemy.trackFollower.isActive)
				enemy.trackFollower.instructionTimer.Resume();
		}

		/// <summary>
		/// Do whatever we want to do this tick.
		/// This includes choosing a new wander target.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnFixedUpdate(MonoBehaviour parent)
		{
			base.OnFixedUpdate(parent);

			EnemyController enemy = GetEnemyController(parent);

			if (enemy.wanderTimer.complete)
			{
				enemy.wanderTimer.Reset();
				enemy.currentWanderTarget = NewTarget(parent);
			}
			
			if (enemy.trackFollower.isActive)
			{
				Vector3 target = enemy.currentWanderTarget;
				target.y = enemy.transform.position.y;
				enemy.trackFollower.instructionTimer.paused = (target - enemy.transform.position).magnitude > m_TrackTimerPausingDistance;
			}
		}

		/// <summary>
		/// Get the current destination.
		/// This is either a semi-static location or defined by a TrackFollower.
		/// </summary>
		/// <param name="parent">Parent</param>
		/// <returns>The current destination, in world space.</returns>
		protected override Vector3 GetMotionTarget(MonoBehaviour parent)
		{
			EnemyController enemy = GetEnemyController(parent);
			return enemy.currentWanderTarget; // enemy.currentWanderTarget then calls the track follower if necessary.
		}

		/// <summary>
		/// 	Returns a state for transition. Return self if no transition.
		/// </summary>
		/// <returns>The next state.</returns>
		/// <param name="parent">Parent.</param>
		public override AbstractEnemyState GetNextState(MonoBehaviour parent)
		{
			// Here, account for transitions mandated by the parent's track follower.
			// Be sure to pause the track follower's timer if necessary.

			//CheckIf this exists and has stuff in it
			if (GetEnemyController(parent).patrolPath != null && GetEnemyController(parent).patrolPath.Points!=null)
				return patrolState;

			return base.GetNextState(parent);
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// 	Returns a new target towards which this ant should move (to be used in GetMotionTarget.)
		/// </summary>
		/// <returns>The target.</returns>
		/// <param name="parent">Parent.</param>
		private Vector3 NewTarget(MonoBehaviour parent)
		{
			EnemyController enemy = GetEnemyController(parent);

			if (enemy.wanderWithinSphere)
				return enemy.wanderStartPosition + MapToRadius(UnityEngine.Random.insideUnitSphere, enemy);

			Vector2 v2 = UnityEngine.Random.insideUnitCircle;
			Vector3 pointInCircle = new Vector3(v2.x, 0.0f, v2.y);

			return enemy.wanderStartPosition + MapToRadius(pointInCircle, enemy);
		}

		/// <summary>
		/// Maps a point inside a unit sphere or circle to a point in a sphere shell or donut/annulus, defined by
		/// <c>enemy</c>'s min and max wander radii.
		/// </summary>
		/// <param name="point">Point within a unit sphere or circle (magnitude no more than 1.0)</param>
		/// <param name="enemy">EnemyController with defined <c>minWanderRadius</c> and <c>maxWanderRadius</c></param>
		/// <returns>A point at least <c>minWanderRadius</c> away from the origin, but no more than <c>maxWanderRadius</c> away.</returns>
		private Vector3 MapToRadius(Vector3 point, EnemyController enemy)
		{
			float magnitude = point.magnitude;
			magnitude = HydraMathUtils.MapRange(0.0f, 1.0f, enemy.minWanderRadius, enemy.maxWanderRadius, point.magnitude);
			return point.normalized * magnitude;
		}

		#endregion
	}

	#region Structures

	/// <summary>
	/// Enemy wander data.
	/// </summary>
	[Serializable]
	public struct WanderData
	{
		public float maxRadius; //Maximum wandering radius.
		public float minRadius; //Minimum wandering radius
		public Timer timer; //Wandering timer
		public bool wanderWithinSphere; //Should the enemy be wandering within sphere radius?

		[HideInInspector] public Vector3 startPoint; //Wandering start position
		[HideInInspector] public Vector3 currentTarget; //Current wandering target position
	}

	#endregion
}
