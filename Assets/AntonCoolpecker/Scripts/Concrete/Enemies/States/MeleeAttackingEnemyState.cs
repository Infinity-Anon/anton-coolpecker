﻿using AntonCoolpecker.Abstract.Enemies.States;
using AntonCoolpecker.Concrete.HitBoxes;
using AntonCoolpecker.Concrete.Utils;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Enemies.States
{
	/// <summary>
	/// Melee attacking enemy state - When the enemy is melee attacking the player
	/// </summary>
	public class MeleeAttackingEnemyState : AbstractEnemyState
	{
		#region Variables

		public const string CLONE_SUFFIX = "(Clone)";

		[SerializeField] private HitBox m_HitBoxPrefab; //Reference to hitbox
		[SerializeField] private int m_Damage = 1; //Max damage deliverance
		[SerializeField] private float m_WarmUpTime; //Time before performing the melee attack
		[SerializeField] private float m_AttackDuration; //Length of melee attack

		[SerializeField] private Timer m_StateTimer;
		[SerializeField] private AbstractEnemyState m_TimerState;

		#endregion

		#region Properties

		/// <summary>
		/// 	Gets the hit box prefab.
		/// </summary>
		/// <value>The hit box prefab.</value>
		protected virtual HitBox hitBoxPrefab { get { return m_HitBoxPrefab; } }

		/// <summary>
		/// 	Gets the damage.
		/// </summary>
		/// <value>The damage.</value>
		public int damage { get { return m_Damage; } }

		/// <summary>
		/// 	Gets the state to transition to after the timer elapses.
		/// </summary>
		/// <value>The state of the timer.</value>
		protected override AbstractEnemyState timerState { get { return m_TimerState; } }

		#endregion

		#region Override Functions/Methods

		/// <summary>
		/// 	Gets the timer to transition to another state.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override Timer GetStateTimer(MonoBehaviour parent)
		{
			EnemyController enemy = GetEnemyController(parent);

			return enemy.stateTimer;
		}

		/// <summary>
		/// 	Called when the state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnEnter(MonoBehaviour parent)
		{
			EnemyController enemy = GetEnemyController(parent);

			enemy.stateTimer = new Timer(m_StateTimer);
			
			base.OnEnter(parent);
		}

		/// <summary>
		/// 	Called when the parent updates.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnUpdate(MonoBehaviour parent)
		{
			base.OnUpdate(parent);

            // If past the warmup time
			if (GetStateTimer(parent).remaining < GetStateTimer(parent).maxTime - m_WarmUpTime)
			{
                // If past the attack duration
                if (GetStateTimer(parent).remaining < GetStateTimer(parent).maxTime - (m_WarmUpTime + m_AttackDuration))
                    GetHitBox(parent).ignorePlayer = true;
                // If not yet past the attack duration
                else
                    GetHitBox(parent).ignorePlayer = false;
			}
		}

		/// <summary>
		/// Called before another state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnExit(MonoBehaviour parent)
		{
			base.OnExit(parent);

            GetHitBox(parent).ignorePlayer = true;
		}

		#endregion

		#region Public Methods

		/// <summary>
		/// 	Gets the attack hit box. Instantiates it if it does not exist.
		/// </summary>
		/// <returns>The hit box instance.</returns>
		/// <param name="parent">Parent.</param>
		public DamageDealingHitBox GetHitBox(MonoBehaviour parent)
		{
			Transform child = parent.transform.Find(m_HitBoxPrefab.name + CLONE_SUFFIX);

			if (child != null)
				return child.GetComponent<DamageDealingHitBox>();

			Vector3 position = parent.transform.position + parent.transform.rotation * m_HitBoxPrefab.transform.position;
			Quaternion rotation = parent.transform.rotation * m_HitBoxPrefab.transform.rotation;

			DamageDealingHitBox output = Instantiate(m_HitBoxPrefab, position, rotation) as DamageDealingHitBox;
			output.transform.parent = parent.transform;

			return output;
		}

		#endregion
	}
}