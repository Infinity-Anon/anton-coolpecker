﻿using AntonCoolpecker.Abstract.Enemies.States;
using AntonCoolpecker.API.Damageable;
using AntonCoolpecker.Concrete.Audio;
using AntonCoolpecker.Concrete.Enemies.States;
using AntonCoolpecker.Concrete.HitBoxes;
using AntonCoolpecker.Concrete.Platforms;
using AntonCoolpecker.Concrete.StateMachine;
using AntonCoolpecker.Concrete.Utils;
using Hydra.HydraCommon.Abstract;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Enemies
{
	[DisallowMultipleComponent]
	[RequireComponent(typeof(CharacterLocomotor))]
	[RequireComponent(typeof(NavMeshAgent))]
	/// <summary>
	/// Enemy Controller - The base class for all enemy-related controllers.
	/// </summary>
	public class EnemyController : HydraMonoBehaviour, IDamageable
	{
		#region Variables

		[SerializeField] protected EnemyStateMachine m_StateMachine; //Handles the states of the enemy
		[SerializeField] private float m_PlayerDetectionRadius; //Maximum area radius for detecting the player 
		[SerializeField] private Timer m_PlayerDetectionTimer; //Keeps track of how long the enemy is considered detected
		[SerializeField] private float m_WalkingSpeed; //Enemy walking speed
		[SerializeField] private float m_RunningSpeed; //Enemy running speed
		[SerializeField] private float m_Acceleration; //Enemy acceleration
		[SerializeField] private float m_RotateSpeed; //Enemy rotation speed
		[SerializeField] private WanderData m_WanderData; //Keeps track of the wandering data for the enemy
		[SerializeField] private float m_MinimumNMADestinationChange = 0.25f; //Minimum distance magnitude for nav mesh agent destination change
		[SerializeField] private bool m_MovesIn3D = false; //Does the enemy move in 3D?
		[SerializeField] private TrackFollower m_TrackFollower; //Enemy track follower
		[SerializeField] private int m_Health; //Current enemy health
		[SerializeField] private PatrolPath m_Path; //The patrolling path of the enemy(If it's assigned to one)
		[SerializeField] private PanicEnemyState m_PanicState; //Current state will be swapped to this when panic rolls fail.
		[SerializeField] private bool m_CanSeeThroughWall; //While true, enemy ignores the need to raycast in order to see the player.
		[SerializeField] private float m_PursuitTime; //Amount of time the enemy will pursuit the player.

        private int m_PatrolIndex = 0; //Current patrol path point index
        private bool m_EnteredPatrol = false; //Has the enemy entered its' patrol?
        private bool m_WalkingBack = false; //Is the enemy walking back the patrol path?
        private Timer m_PatrolTimer = new Timer(); //Keeps track of how long the enemy stays at each patrol point.
        private Timer m_PursuitTimer = new Timer(); //Keeps track of how long the enemy will pursuit the player.

        private bool m_CanSeePlayer = false; //Can the enemy see the player?
        private bool m_HasEncounteredPlayer = false; //Has the enemy encountered the player?
		private Timer m_StateTimer = new Timer(); //Timer for the enemy's current state

		//TODO: MOVE BELOW TO THE PROJECTILE SCRIPTS
		private bool m_HasFiredProjectile = false; //Has the enemy fired projectile(s)?

		private CharacterLocomotor m_CachedCharacterLocomotor; //Cache of the enemy's character locomotor
		private NavMeshAgent m_CachedNavMeshAgent; //Cache of the enemy's navigation mesh agent
		private MusicManager m_CachedMusicManager; //Cache of the enemy's music manager
		private GameObject m_CachedHitbox; //Cache of the enemy's hitbox

		private static GameObject m_CachedPlayer; //Cache of the player instance

		#endregion

		#region Properties
	
		/// <summary>
		/// Gets the enemy state machine.
		/// </summary>
		/// <value>The state machine.</value>
		public EnemyStateMachine stateMachine { get { return m_StateMachine; } }

		/// <summary>
		/// Gets the enemy's track follower.
		/// </summary>
		/// <value>The track follower.</value>
		public TrackFollower trackFollower { get { return m_TrackFollower; } }

		/// <summary>
		/// Gets or sets the state timer.
		/// </summary>
		/// <value>The state timer.</value>
		public Timer stateTimer { get { return m_StateTimer; } set { m_StateTimer = value; } }

		/// <summary>
		/// Gets the player detection radius.
		/// </summary>
		/// <value>The player detection radius.</value>
		public float playerDetectionRadius { get { return m_PlayerDetectionRadius; } }

		/// <summary>
		/// Gets the player detection timer(How long the player will stay detected by this enemy).
		/// </summary>
		/// <value>The player detection timer.</value>
		public Timer playerDetectionTimer { get { return m_PlayerDetectionTimer; } }

		/// <summary>
		/// Gets the enemy's walking speed.
		/// </summary>
		/// <value>The enemy's walking speed.</value>
		public float walkingSpeed { get { return m_WalkingSpeed; } }

		/// <summary>
		/// Gets the enemy's running speed.
		/// </summary>
		/// <value>The enemy's running speed.</value>
		public float runningSpeed { get { return m_RunningSpeed; } }

		/// <summary>
		/// Gets the acceleration.
		/// </summary>
		/// <value>The acceleration.</value>
		public float acceleration { get { return m_Acceleration; } }

		/// <summary>
		/// Gets the rotate speed.
		/// </summary>
		/// <value>The rotate speed.</value>
		public float rotateSpeed { get { return m_RotateSpeed; } }

		/// <summary>
		/// Gets the max wander radius.
		/// </summary>
		/// <value>The max wander radius.</value>
		public float maxWanderRadius { get { return m_WanderData.maxRadius; } }

		/// <summary>
		/// Gets the minimum wander radius.
		/// </summary>
		/// <value>The minimum wander radius.</value>
		public float minWanderRadius { get { return m_WanderData.minRadius; } }

		/// <summary>
		/// Gets the wander timer.
		/// </summary>
		/// <value>The wander timer.</value>
		public Timer wanderTimer { get { return m_WanderData.timer; } }

		/// <summary>
		/// Gets a value indicating whether this <see cref="AntonCoolpecker.Concrete.Enemies.EnemyController"/> should
		/// wander within a specific sphere area.
		/// </summary>
		/// <value><c>true</c> if wandering within sphere; otherwise, <c>false</c>.</value>
		public bool wanderWithinSphere { get { return m_WanderData.wanderWithinSphere; } }

		/// <summary>
		/// Gets the enemy's wander start position.
		/// </summary>
		/// <value>The enemy's wander start position.</value>
		public Vector3 wanderStartPosition { get { return m_WanderData.startPoint; } }

		/// <summary>
		/// Gets the enemy's patrol path.
		/// </summary>
		/// <value>The patrol path.</value>
        public PatrolPath patrolPath { get { return m_Path; } }

		/// <summary>
		/// Gets or sets the index of the patrol(Current patrol path point).
		/// </summary>
		/// <value>The index of the patrol path points.</value>
        public int patrolIndex { get { return m_PatrolIndex; } set { m_PatrolIndex = value; } }

		/// <summary>
		/// Gets or sets a value indicating whether this <see cref="AntonCoolpecker.Concrete.Enemies.EnemyController"/>
		/// has entered patrol.
		/// </summary>
		/// <value><c>true</c> if entered patrol; otherwise, <c>false</c>.</value>
        public bool enteredPatrol { get { return m_EnteredPatrol; } set { m_EnteredPatrol = value; } }

		/// <summary>
		/// Gets or sets a value indicating whether this <see cref="AntonCoolpecker.Concrete.Enemies.EnemyController"/>
		/// is walking back in it's patrol path.
		/// </summary>
		/// <value><c>true</c> if walking back; otherwise, <c>false</c>.</value>
        public bool walkingBack { get { return m_WalkingBack; } set { m_WalkingBack = value; } }

		/// <summary>
		/// Gets the patrolling timer.
		/// </summary>
		/// <value>The patrolling timer.</value>
		public Timer patrolTimer { get { return m_PatrolTimer; } }

		/// <summary>
		/// Gets the pursuit timer.
		/// </summary>
		/// <value>The pursuit timer.</value>
        public Timer pursuitTimer { get { return m_PursuitTimer; } }

		/// <summary>
		/// Gets the mimimum NMA desintation change(Minimum destination magnitude before the enemy's wandering path is changed).
		/// </summary>
		/// <value>The mimimum NMA desintation change.</value>
        public float mimimumNMADesintationChange { get { return m_MinimumNMADestinationChange; } }

		/// <summary>
		/// Gets a value indicating whether this <see cref="AntonCoolpecker.Concrete.Enemies.EnemyController"/> moves in 3D or 2D space.
		/// </summary>
		/// <value><c>true</c> if the enemy moves in 3D space; otherwise, the enemy moves in 2D space.</value>
		public bool movesIn3D { get { return m_MovesIn3D; } }

		/// <summary>
		/// Gets or sets a value indicating whether this <see cref="AntonCoolpecker.Concrete.Enemies.EnemyController"/> has
		/// fired projectile.
		/// </summary>
		/// <value><c>true</c> if has fired projectile; otherwise, <c>false</c>.</value>
		//TODO: MOVE THIS TO THE PROJECTILE SCRIPTS
		public bool hasFiredProjectile { get { return m_HasFiredProjectile; } set { m_HasFiredProjectile = value; } }

		/// <summary>
		/// Gets the health of the enemy.
		/// </summary>
		/// <value>The health.</value>
        public int health { get { return m_Health; } }

        /// <summary>
        /// Whether this particular enemy has ever detected the player character before. (Used for startling).
        /// </summary>
        public bool hasEncounteredPlayer
        {
            get { return this.m_HasEncounteredPlayer;  }
            set { this.m_HasEncounteredPlayer = value; }
        }

		/// <summary>
		/// Gets the enemy's panic state.
		/// </summary>
		/// <value>The state of the panic.</value>
        public PanicEnemyState panicState { get { return m_PanicState; } }

        /// <summary>
		/// 	Gets the player.
		/// </summary>
		/// <value>The player.</value>
		public GameObject player
        {
            get
            {
                return (m_CachedPlayer && m_CachedPlayer.activeInHierarchy)
                           ? m_CachedPlayer
                           : m_CachedPlayer = GameObject.FindWithTag("Player");
            }
        }

		/// <summary>
		/// Gets or sets the current wander target.
		/// </summary>
		/// <value>The current wander target.</value>
        public Vector3 currentWanderTarget
		{
			get
			{
				return m_TrackFollower.isActive
					? m_TrackFollower.currentInstruction.GetNextPosition(m_TrackFollower)
						: m_WanderData.currentTarget;
			}
			set { m_WanderData.currentTarget = value; }
		}

		/// <summary>
		/// Property for determining whether the navmesh agent should perform movement.
		/// Setting this to false allows e.g. enemy states to use their own movement code,
		/// but keeping it in sync with the simulated position falls on the state. 
		/// </summary>
		/// <value><c>true</c> if using navmesh agent; otherwise, <c>false</c>.</value>
		public bool usingNavMeshAgent { get { return navMeshAgent.enabled; } set { navMeshAgent.enabled = value; } }

		/// <summary>
		/// 	Gets the character locomotor.
		/// </summary>
		/// <value>The character locomotor.</value>
		public CharacterLocomotor characterLocomotor
		{
			get { return m_CachedCharacterLocomotor ?? (m_CachedCharacterLocomotor = GetComponent<CharacterLocomotor>()); }
		}

		/// <summary>
		/// Gets the navmesh agent.
		/// </summary>
		/// <value>The navmesh agent.</value>
		public NavMeshAgent navMeshAgent
		{
			get { return m_CachedNavMeshAgent ?? (m_CachedNavMeshAgent = GetComponent<NavMeshAgent>()); }
		}

		/// <summary>
		/// Gets the child GameObject that has a DamageDealingHitBox component.
		/// </summary>
		public GameObject hitBox
		{
			get
			{
				if (m_CachedHitbox == null)
				{
					DamageDealingHitBox hitboxComponent = GetComponentInChildren<DamageDealingHitBox>(true);

					if (hitboxComponent != null)
					{
						m_CachedHitbox = hitboxComponent.gameObject;
					}
				}

				return m_CachedHitbox;
			}
		}

		/// <summary>
		/// Gets the music manager.
		/// </summary>
		/// <value>The music manager.</value>
		private MusicManager musicManager { get { return m_CachedMusicManager ?? (m_CachedMusicManager = MusicManager.instance); } }

		/// <summary>
		/// 	Gets a value indicating whether this <see cref="AntonCoolpecker.Concrete.Enemies.EnemyController"/> can see the player.
		/// </summary>
		/// <value><c>true</c> if the enemy can see player; otherwise, <c>false</c>.</value>
		public bool canSeePlayer { get { return m_CanSeePlayer; } }

		#endregion

		#region Virtual Functions

		/// <summary>
		/// 	Called when the Damageable gets damaged.
		/// </summary>
		/// <param name="damage">Amount of damage to apply.</param>
		/// <param name="collision">Collision info.</param>
		public virtual void Damage(int damage, CharacterLocomotorCollisionData collision)
		{
			if (m_StateMachine.activeState is HitstunEnemyState)
				return;
			
			if (m_StateMachine.activeState is AbstractDyingState)
				return;

			GetStunned(collision);

			m_Health -= damage;

			if (m_Health <= 0)
                Die(collision);
		}

		/// <summary>
		/// Called when the dying animation is finished, just before the GameObject is destroyed by DeadState.
		/// </summary>
		public virtual void FinishDying()
		{
			if (m_CanSeePlayer) 
			{
				PlayCombatMusic(false);
			}
		}

		/// <summary>
		/// Called when the enemy dies.
		/// </summary>
		/// <param name="collision">Collision.</param>
		protected virtual void Die(CharacterLocomotorCollisionData collision)
		{
			AbstractEnemyState dyingState = m_StateMachine.activeState.DyingState;

			m_StateMachine.SetActiveState(dyingState, this);

            panicState.SpreadDeathPanic(this);
        }

		/// <summary>
		/// Called when the enemy gets hit/damaged.
		/// </summary>
		/// <param name="collision">Collision.</param>
		protected virtual void GetStunned(CharacterLocomotorCollisionData collision)
		{
			//Note: collision.point is the player location + an offset depending on the exact attack

			HitstunEnemyState hitstunState = m_StateMachine.activeState.HitstunState;
			hitstunState.source = collision.point;
			m_StateMachine.SetActiveState(hitstunState, this);
			
			Vector3 knockbackDirection = (transform.position - collision.point);
			knockbackDirection.y = 0;
			
			//On flat surfaces, continue going in same direction
			if (knockbackDirection.magnitude < 0.1f)
				knockbackDirection = characterLocomotor.flatVelocity;
			
			//Keep the horizontal direction, but angle 30° up
			knockbackDirection.y = Mathf.Sqrt(knockbackDirection.sqrMagnitude/3);
			
			knockbackDirection.Normalize();

			//Ignore pre-existing velocity when you get stunned
			characterLocomotor.velocity = new Vector3 (knockbackDirection.x * hitstunState.hitstunForceHorizontal,
				knockbackDirection.y * hitstunState.hitstunForceVertical,
				knockbackDirection.z * hitstunState.hitstunForceHorizontal);
		}

		/// <summary>
		/// Message to set the navmesh agent to move towards a specific target/destination.
		/// </summary>
		/// <param name="target">Target.</param>
		protected virtual void SetNavMeshAgentTarget(Vector3 target)
		{
			navMeshAgent.destination = target;
		}

		#endregion

		#region Overrides

		/// <summary>
		/// 	Called when the component is enabled.
		/// </summary>
		protected override void OnEnable()
		{
			base.OnEnable();
			
			m_WanderData.startPoint = transform.position;
			m_StateMachine.OnEnable(this);
			playerDetectionTimer.Reset();
			m_TrackFollower.OnParentEnable();
            
            m_PursuitTimer.Reset();
            m_PursuitTimer.maxTime = m_PursuitTime;
            m_PursuitTimer.AddSeconds(-m_PursuitTime);

            navMeshAgent.acceleration = acceleration;
			navMeshAgent.angularSpeed = rotateSpeed;
		}

		/// <summary>
		/// 	Called once every frame.
		/// </summary>
		protected override void Update()
		{
			base.Update();

			m_StateMachine.Update(this);

			if (playerDetectionTimer.complete)
			{
				playerDetectionTimer.Reset();
				bool playerDetected = DetectPlayer();

                if (playerDetected)
                    pursuitTimer.Reset();
                else if (!pursuitTimer.complete)
                    playerDetected = true;

				if (playerDetected != m_CanSeePlayer) 
					PlayCombatMusic(playerDetected);

				m_CanSeePlayer = playerDetected;
			}
		}

		/// <summary>
		/// 	Called every physics timestep.
		/// </summary>
		protected override void FixedUpdate()
		{
			base.FixedUpdate();

			m_StateMachine.FixedUpdate(this);

			m_TrackFollower.OnFixedUpdate();
		}

		/// <summary>
		/// 	Called after all Updates have finished.
		/// </summary>
		protected override void LateUpdate()
		{
			base.LateUpdate();

			m_StateMachine.LateUpdate(this);
		}

		/// <summary>
		/// Called when object is Instantiated.
		/// </summary>
		protected override void Awake()
		{
			base.Awake();

			m_CanSeePlayer = false;
		}

		#endregion

		#region Method/Functions

		/// <summary>
		/// Call this method when the enemy enters or leaves 'combat mode' with the player.
		/// </summary>
		/// <param name="playMusic">True to start (or continue) playing combat music, false to stop playing</param>
		private void PlayCombatMusic(bool playMusic)
		{
			if (playMusic) 
			{
				musicManager.FadeToAnt();
			} 
			else 
			{
				musicManager.FadeFromAnt();
			}
		}

		/// <summary>
		/// 	Check to see if the player is within visibility range.
		/// </summary>
		/// <returns><c>true</c>, if player was detected, <c>false</c> otherwise.</returns>
		private bool DetectPlayer()
		{
			if (m_TrackFollower.isActive && m_TrackFollower.preventReactionToPlayer) //If is active and preventing reaction to player.
				return false;

			float distance = Vector3.Distance(player.transform.position, transform.position);

			if (!(distance < playerDetectionRadius)) //If within playerDetectionRadius
				return false;

			if (!m_CanSeeThroughWall)
			{
				RaycastHit hit;

				LayerMask layerToIgnore = 1 << 4; //IGNORE WATER LAYER
				layerToIgnore |= 1 << 12; //IGNORE PLAYER LAYER
				layerToIgnore |= 1 << 14; //IGNORE ENEMY LAYER
				layerToIgnore |= 1 << 18; //IGNORE ATTACK HITBOX LAYER

				if (Physics.Raycast(transform.position, (player.transform.position + new Vector3(0, 1, 0) - transform.position), out hit, distance, ~layerToIgnore))
					return false;
			}

			return true;
		}

		/// <summary>
		/// Determines whether the enemy is roughly facing the specified point, ignoring height/vertical angle.
		/// </summary>
		/// <param name="position">The point, in scene space, we might be aiming at</param>
		/// <param name="leeway">The fudge factor, in degrees. How close is 'close enough' in terms of facing direction</param>
		/// <returns>True if the enemy is facing within <c>leeway</c> degrees of <c>position</c></returns>
		public bool IsAimedHorizontallyAt(Vector3 position, float leeway)
		{
			position.y = transform.position.y;
			return Vector3.Angle(transform.forward, position - transform.position) < leeway;
		}

		#endregion
    }
}