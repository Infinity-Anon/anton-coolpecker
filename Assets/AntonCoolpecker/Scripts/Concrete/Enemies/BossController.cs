﻿using AntonCoolpecker.Abstract.Enemies.States;
using AntonCoolpecker.Concrete.Enemies;
using AntonCoolpecker.Concrete.Utils;
using System.Collections;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Enemies
{
	/// <summary>
	/// Boss controller.
	/// </summary>
    public class BossController : EnemyController
    {
		#region Variables

        [SerializeField] private Texture2D m_BossPortrait;
        [SerializeField] private Rect[] m_BossPortraitPullPoints;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the boss portrait.
		/// </summary>
		/// <value>The boss portrait.</value>
        public Texture2D bossPortrait { get { return m_BossPortrait; } }

		/// <summary>
		/// Gets the boss portrait pull points.
		/// </summary>
		/// <value>The boss portrait pull points.</value>
        public Rect[] bossPortraitPullPoints { get { return m_BossPortraitPullPoints; } }

		#endregion

		#region Override Functions

		/// <summary>
		/// Called when the boss dies.
		/// </summary>
		/// <param name="collision">Collision.</param>
        protected override void Die(CharacterLocomotorCollisionData collision)
        {
            AbstractEnemyState dyingState = m_StateMachine.activeState.DyingState;
            m_StateMachine.SetActiveState(dyingState, this);
        }

		/// <summary>
		/// Called before the first Update.
		/// </summary>
        protected override void Start()
        {
            base.Start();

            PlayerStatistics.currentBoss = this;
        }

		/// <summary>
		/// Called when the boss gets damaged/hit.
		/// </summary>
		/// <param name="damage">Amount of damage to apply.</param>
		/// <param name="collision">Collision info.</param>
        public override void Damage(int damage, CharacterLocomotorCollisionData collision)
        {
            base.Damage(damage, collision);
            
            PlayerStatistics.bossHUD.TakeDamage();
        }

		#endregion
    }
}