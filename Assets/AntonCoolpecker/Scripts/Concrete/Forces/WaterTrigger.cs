﻿using AntonCoolpecker.Utils;
using AntonCoolpecker.Utils.Time;
using Hydra.HydraCommon.Abstract;
using Hydra.HydraCommon.Utils;
using System.Collections.Generic;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Forces
{
	/// <summary>
	/// 	WaterTrigger script for water physics
	/// </summary>
	/// 
	/// IMPORTANT: PREFAB "WATERDESHEIGHT" MUST BE ATTACHED TO OBJECT WITH WATERTRIGGER SCRIPT IN ORDER FOR WATER PHYSICS TO PROPERLY WORK
	/// CHILD OF "WATERDESHEIGHT" OBJECT - "SURFACE COLLIDER" - MUST HAVE THE SCALE(NOT SIZE) ADJUSTED TO PROPER VALUES DEPENDING ON WATER OBJECT VOLUME
	/// "SURFACE COLLIDER" MUST HAVE "ISTRIGGER" PROPERTY ENABLED
	/// "WATERDESHEIGHT" MUST HAVE MESH COLLIDER AND MESH RENDERER DISABLED(NOT REMOVED, FOR THE SAKE OF PROPERLY ADJUSTING THE "WATERDESHEIGHT" WITH THE AID OF SEEING THE OBJECT)

	//TODO OPTIMIZATION: COMBINE ALL WATER MESHES INTO ONE SOLITARY MESH AND ADD CONVEX MESH COLLIDER TO NEW INSTANTIATED MESH(SINCE HAVING MULTIPLE MESH COLLIDERS IS PERFORMANCE-DRAINING)

	[DisallowMultipleComponent]
	[RequireComponent(typeof(Collider))]
	[RequireComponent(typeof(Rigidbody))]
	public class WaterTrigger : HydraMonoBehaviour
	{
		#region Variables

		[SerializeField] private float m_Friction = 0.05f;
		[SerializeField] private float m_BobStrength = 0.05f;
		[SerializeField] private float m_BobSpeed = 1.5f;
		[SerializeField] private float m_InterpolateVelocityValue = -2;

		[SerializeField] private bool m_AllowUnderwater = true;
		[SerializeField] private bool m_EnableBob = true;
		[SerializeField] private bool m_EnableFriction = true;

		[SerializeField] private bool m_InterpolateSwimVelocity = false;
		[SerializeField] private bool m_InterpolateWaterVelocity = true;
		[SerializeField] private bool m_InterpolateAnimatedVelocity = false;

		[SerializeField] private bool m_InterpolatePlayerUpwards = true;
		[SerializeField] private bool m_InterpolateRigidbodiesUpwards = true;
		[SerializeField] private bool m_InterpolateEnemiesUpwards = true;

		[SerializeField] private bool m_RaycastCollision_True_FlatCollision_False = false;
		[SerializeField] private float m_SurfaceColliderYModifier = 2f;
		[SerializeField] private float m_SwimLerpYModifier = 1f;
		[SerializeField] private float m_SwimLerpMagnitudeModifier = 0.5f;

		//[SerializeField] private bool m_IgnorePlayer = true;
		private bool m_IgnorePlayer = true;

		private bool m_IsPlayer = false;
		private Dictionary<GameObject, float> m_EntryVelocityDict;
		private Dictionary<GameObject, bool> m_EntryVelocityBool;
		private GameObject m_SurfaceCollider;
		private float m_origAnimatedSpeed;

		private bool m_CheckForOtherWaters = false;

		#endregion

		#region Properties

		/// <summary>
		/// 	Gets or sets the friction.
		/// </summary>
		/// <value>The friction.</value>
		[Tweakable("WaterPhysics")]
		public float friction { get { return m_Friction; } set { m_Friction = value; } }

		/// <summary>
		///		Gets or sets the amount that the object will continue bob up and down in the water, despite friction.
		/// </summary>
		[Tweakable("WaterPhysics")]
		public float bobStrength { get { return m_BobStrength; } set { m_BobStrength = value; } }

		/// <summary>
		///		Gets or sets the amount that the object will continue bob up and down in the water, despite friction.
		/// </summary>
		[Tweakable("WaterPhysics")]
		public float bobSpeed { get { return m_BobSpeed; } set { m_BobSpeed = value; } }

		/// <summary>
		///		Gets or sets at what velocity value the interpolation of the player velocity stops
		/// </summary>
		[Tweakable("WaterPhysics")]
		public float interpolateVelocityValue { get { return m_InterpolateVelocityValue; } set { m_InterpolateVelocityValue = value; } }

		/// <summary>
		///		Gets or sets the enabling of bobbing whilst in water
		/// </summary>
		[Tweakable("WaterPhysics")]
		public bool enableBob { get { return m_EnableBob; } set { m_EnableBob = value; } }

		/// <summary>
		///		Gets or sets the enabling of friction whilst in water
		/// </summary>
		[Tweakable("WaterPhysics")]
		public bool enableFriction { get { return m_EnableFriction; } set { m_EnableFriction = value; } }

		/// <summary>
		///		Gets or sets whether the vertical swim velocity should be interpolated or not
		/// </summary>
		[Tweakable("WaterPhysics")]
		public bool interpolateSwimVelocity
		{
			get { return m_InterpolateSwimVelocity; }
			set { m_InterpolateSwimVelocity = value; }
		}

		/// <summary>
		///		Gets or sets whether the water velocity should be interpolated(and activated) or not
		/// </summary>
		[Tweakable("WaterPhysics")]
		public bool interpolateWaterVelocity
		{
			get { return m_InterpolateWaterVelocity; }
			set { m_InterpolateWaterVelocity = value; }
		}

		/// <summary>
		///		Gets or sets whether the water velocity should be interpolated(and activated) or not
		/// </summary>
		[Tweakable("WaterPhysics")]
		public bool interpolateAnimatedVelocity
		{
			get { return m_InterpolateAnimatedVelocity; }
			set { m_InterpolateAnimatedVelocity = value; }
		}

		/// <summary>
		///		Gets or sets whether the player should be float upwards towards the surface or not
		/// </summary>
		[Tweakable("WaterPhysics")]
		public bool interpolatePlayerUpwards
		{
			get { return m_InterpolatePlayerUpwards; }
			set { m_InterpolatePlayerUpwards = value; }
		}

		/// <summary>
		///		Gets or sets whether rigidbodies should be float upwards towards the surface or not
		/// </summary>
		[Tweakable("WaterPhysics")]
		public bool interpolateRigidbodiesUpwards
		{
			get { return m_InterpolateRigidbodiesUpwards; }
			set { m_InterpolateRigidbodiesUpwards = value; }
		}

		/// <summary>
		///		Gets or sets whether enemies should be float upwards towards the surface or not
		/// </summary>
		[Tweakable("WaterPhysics")]
		public bool interpolateEnemiesUpwards
		{
			get { return m_InterpolateEnemiesUpwards; }
			set { m_InterpolateEnemiesUpwards = value; }
		}

		/// <summary>
		///		Gets or sets whether the player can swim underwater or not
		/// </summary>
		[Tweakable("WaterPhysics")]
		public bool allowUnderwater { get { return m_AllowUnderwater; } set { m_AllowUnderwater = value; } }

		/// <summary>
		///		Gets or sets whether the current water object should check for collision with other water objects or not
		/// </summary>
		[Tweakable("WaterPhysics")]
		public bool checkForOtherWaters { get { return m_CheckForOtherWaters; } set { m_CheckForOtherWaters = value; } }

		/// <summary>
		///		Gets or sets whether the player should be ignored by the water script or not
		/// </summary>
		[Tweakable("WaterPhysics")]
		public bool ignorePlayer { get { return m_IgnorePlayer; } set { m_IgnorePlayer = value; } }

		#endregion

		#region Messages

		/// <summary>
		/// 	Called when the component is enabled.
		/// </summary>
		protected override void OnEnable()
		{
			base.OnEnable();

			if (m_EntryVelocityDict == null)
				m_EntryVelocityDict = new Dictionary<GameObject, float>();

			if (m_EntryVelocityBool == null)
				m_EntryVelocityBool = new Dictionary<GameObject, bool>();

			if (this.rigidbody.useGravity)
				this.rigidbody.useGravity = false;
		}

		/// <summary>
		/// 	Called every physics timestep.
		/// </summary>
		protected override void FixedUpdate()
		{
			base.FixedUpdate();

			ConfigureCollider();

			if (m_InterpolateWaterVelocity)
				UpdateWaterVelocity();

			if (m_InterpolateAnimatedVelocity)
				UpdateAnimWater();
		}

		/// <summary>
		/// 	OnTriggerEnter is called when the Collider other enters the trigger.
		/// </summary>
		/// <param name="other">Other.</param>
		protected override void OnTriggerEnter(Collider other)
		{
			base.OnTriggerEnter(other);

			//if (other.gameObject.layer == 12 && m_IgnorePlayer)
				//return;

			if ((other.gameObject.layer == 12 && !m_IgnorePlayer) || other.gameObject.layer != 12)
			AddKey(other);
		}

		/// <summary>
		/// 	OnTriggerExit is called when the Collider other has stopped touching the trigger.
		/// </summary>
		/// <param name="other">Other.</param>
		protected override void OnTriggerExit(Collider other)
		{
			base.OnTriggerExit(other);

			//if (other.gameObject.layer == 12 && m_IgnorePlayer)
				//return;

			RemoveKey(other);
		}

		/// <summary>
		/// 	OnTriggerStay is called every FixedUpdate for every Collider that is
		/// 	touching the trigger.
		/// </summary>
		/// <param name="other">Other.</param>
		protected override void OnTriggerStay(Collider other)
		{
			base.OnTriggerStay(other);

			//if (other.gameObject.layer == 12 && m_IgnorePlayer)
				//return;

			if (m_CheckForOtherWaters)
				CheckForOtherWaters (other);

			UpdateVelocity(other);
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// 	Updates the velocity.
		/// </summary>
		/// <param name="other">Other.</param>
		private void UpdateVelocity(Collider other)
		{
			CharacterLocomotor charLocMot = other.GetComponent<CharacterLocomotor>();
			Rigidbody rigBod = other.GetComponent<Rigidbody>();
			/*WaterTrigger watTrig = other.GetComponent<WaterTrigger>();

			if (watTrig != null) 
			{
				watTrig.interpolatePlayerUpwards = m_InterpolatePlayerUpwards;
			}*/

			if ((rigBod == null && charLocMot == null) || other.gameObject.layer == 4 || other.gameObject.layer == 16)
				return; //RETURN IF NO RIGIDBODY AND CHARACTERLOCOMOTOR IS FOUND OR IF THE COLLIDER BELONGS TO THE WATER OBJECT ITSELF/A MOVING PLATFORM

			// --- //

			if (rigBod != null) 
			{
				if ((other.gameObject.layer == 12 && !m_IgnorePlayer) || other.gameObject.layer != 12) 
				{

					if (GameTime.paused)
					{
						rigBod.velocity = Vector3.zero; //ONLY NECCESSARY FOR RIGIDBODIES - CHARACTER LOCOMOTORS HANDLE THIS AUTOMATICALLY
						return;
					}

					if (other.gameObject.layer != 12) 
					{ 
						//IS RIGIDBODY ISN'T PART OF PLAYER, MAKE SURE IT KEEPS IT'S OWN NATURAL ROTATION
						rigBod.rotation = other.gameObject.transform.rotation;
						rigBod.freezeRotation = true;
					}

					// Apply friction
					if (m_EnableFriction)
						rigBod.velocity -= m_Friction * rigBod.velocity;
				
					// Severely limit the downward velocity
					if (rigBod.velocity.y < 0.0f) 
					{
						float entryVelocity = 0;

						if (m_EntryVelocityDict.TryGetValue (rigBod.gameObject, out entryVelocity))
							entryVelocity = m_EntryVelocityDict [rigBod.gameObject];

						Vector3 velocity = rigBod.velocity;
					
//						if (m_InterpolateSwimVelocity)
//						{
//							float sinkVelocity = SimulatedWaterVelocity(other, entryVelocity);				
//							rigBod.velocity = new Vector3(velocity.x, sinkVelocity, velocity.z);
//						}

						bool boolus;
					
						if (m_EntryVelocityBool.TryGetValue (rigBod.gameObject, out boolus)) 
						{
							if (velocity.y >= m_InterpolateVelocityValue && boolus)
								StopVelInterpolation (rigBod.gameObject);
						
							if (m_InterpolateSwimVelocity && boolus) 
							{
								float sinkVelocity = SimulatedWaterVelocity (other, entryVelocity);	
								rigBod.velocity = new Vector3 (velocity.x, sinkVelocity, velocity.z);
							}
						}
					
						if (!m_InterpolateSwimVelocity)
							rigBod.velocity = new Vector3 (velocity.x, velocity.y, velocity.z);
					}
				
					// Bob
					if (m_EnableBob) 
					{
						if (other.gameObject.layer == 17 && m_InterpolateRigidbodiesUpwards) 
						{
							if (rigBod.useGravity)
								rigBod.useGravity = false;

							Transform surfaceCollider = this.gameObject.transform.GetChild (0).GetChild (0); //GET SURFACE COLLIDER

							float rigBodSizeY = other.bounds.size.y / 7;

							Vector3 yPos = new Vector3 (rigBod.transform.position.x, surfaceCollider.position.y - (rigBodSizeY) - (bobStrength * 0.4f), rigBod.transform.position.z);

							if (rigBod.transform.position.y < (surfaceCollider.position.y - (rigBodSizeY)) && !IgnoreUpwardsInterpolation (other)) 
							{
								if (m_InterpolateWaterVelocity && !m_InterpolateAnimatedVelocity)
									rigBod.transform.position = Vector3.Lerp (rigBod.transform.position, yPos, (0.1f + (0.5f * bobStrength / (0.5f * bobSpeed)) * GameTime.deltaTime));

								if (!m_InterpolateWaterVelocity && !m_InterpolateAnimatedVelocity)
									rigBod.transform.position = Vector3.Lerp (rigBod.transform.position, yPos, (0.75f + (0.5f * bobStrength)) * GameTime.deltaTime);

								if (m_InterpolateAnimatedVelocity && !m_InterpolateWaterVelocity) 
								{
									yPos = new Vector3 (rigBod.transform.position.x, surfaceCollider.position.y - (rigBodSizeY) - 0.2f, rigBod.transform.position.z);
									rigBod.transform.position = Vector3.Lerp (rigBod.transform.position, yPos, 3f * GameTime.deltaTime);

									//charLocMot.transform.position = Vector3.Lerp (charLocMot.transform.position, yPos, (0.1f + (0.5f * bobStrength / (0.5f * bobSpeed)) * GameTime.deltaTime));
								}
							}
						}

						if ((other.gameObject.layer == 12 && m_InterpolatePlayerUpwards) || other.gameObject.layer != 12) 
						{
							float bob = (HydraMathUtils.Abs ((GameTime.fixedTime * bobSpeed) % 4 - 2) - 1) * bobStrength;
							rigBod.velocity += Vector3.up * bob;
						}
					}
				}
			}

			// --- //

			if (charLocMot != null && charLocMot.enabled) 
			{
				if ((other.gameObject.layer == 12 && !m_IgnorePlayer) || other.gameObject.layer != 12) 
				{
					// Apply friction
					if (m_EnableFriction)
						charLocMot.velocity -= m_Friction * charLocMot.velocity;
				
					// Severely limit the downward velocity
					if (charLocMot.velocity.y < 0.0f) 
					{
						float entryVelocity = 0;

						if (m_EntryVelocityDict.TryGetValue (charLocMot.gameObject, out entryVelocity))
							entryVelocity = m_EntryVelocityDict [charLocMot.gameObject];

						Vector3 velocity = charLocMot.velocity;

						//if (velocity.y >= -0.1 && m_EntryVelocityBool[charLocMot.gameObject])
						//StopVelInterpolation(charLocMot.gameObject);

						//velocity.y >= -0.1
						bool boolus;

						if (m_EntryVelocityBool.TryGetValue (charLocMot.gameObject, out boolus)) 
						{
							if (velocity.y >= m_InterpolateVelocityValue && boolus)
								StopVelInterpolation (charLocMot.gameObject);

							if (m_InterpolateSwimVelocity && boolus) 
							{
								float sinkVelocity = SimulatedWaterVelocity (other, entryVelocity);	
								charLocMot.velocity = new Vector3 (velocity.x, sinkVelocity, velocity.z);
							}
						}
					
						if (!m_InterpolateSwimVelocity)
							charLocMot.velocity = new Vector3 (velocity.x, velocity.y, velocity.z);
					}
				
					// Bob
					if (m_EnableBob) 
					{
						//if ((other.gameObject.layer == 12 && m_InterpolatePlayerUpwards) || other.gameObject.layer != 12)
						if ((other.gameObject.layer == 12 && m_InterpolatePlayerUpwards) || (other.gameObject.layer == 14 && m_InterpolateEnemiesUpwards)) 
						{
							Vector3 surColPos = this.gameObject.transform.GetChild (0).GetChild (0).position; //GET SURFACE COLLIDER;

							//OBSERVE - THE SURFACE COLLIDER MUST BE ABOVE THE USUAL SURFACE COLLIDER IN THIS CASE
							if (m_RaycastCollision_True_FlatCollision_False)
							{
								Vector3 rayPosition = new Vector3(other.transform.position.x, other.transform.position.y + 10f,
									(other.transform.position.z));
								RaycastHit hitInfo;

								LayerMask layerToIgnore = 1 << 4; //IGNORE EVERYTHING BUT OBJECTS WITH LAYER "WATER"

								if (Physics.Raycast (rayPosition, Vector3.down, out hitInfo, Mathf.Infinity, layerToIgnore))
								{
									if (hitInfo.collider.gameObject.name == "Surface Collider") 
										surColPos = new Vector3 (hitInfo.point.x, hitInfo.point.y + m_SurfaceColliderYModifier, hitInfo.point.z);
									else
										Debug.LogError ("CAN'T FIND SURFCOL BY RAYCAST");
								}

								// --- //

								//Vector3 project = Vector3.ProjectOnPlane (charLocMot.velocity, hitInfo.normal);
								charLocMot.transform.position = Vector3.Lerp (charLocMot.transform.position,
									new Vector3(charLocMot.transform.position.x, surColPos.y - m_SwimLerpYModifier, charLocMot.transform.position.z),
									(charLocMot.flatVelocity.magnitude * m_SwimLerpMagnitudeModifier) * GameTime.deltaTime);
								//charLocMot.velocity += Vector3.up * project.y * 0.01f * charLocMot.flatVelocity.magnitude;
							}

							float charLocSizeY = (other.bounds.size.y / 3) * 2;

							if (other.gameObject.layer == 14) 
							{
								if (charLocMot.useGravity)
									charLocMot.useGravity = false;

								if (charLocMot.useFriction)
									charLocMot.useFriction = false;

								charLocSizeY = -other.bounds.size.y / 14;
							}

							Vector3 yPos = new Vector3 (charLocMot.transform.position.x, surColPos.y - (charLocSizeY) - (bobStrength * 0.4f), charLocMot.transform.position.z);

							if (charLocMot.transform.position.y < (surColPos.y - (charLocSizeY)) && !IgnoreUpwardsInterpolation (other)) 
							{
								if (m_InterpolateWaterVelocity && !m_InterpolateAnimatedVelocity)
									charLocMot.transform.position = Vector3.Lerp (charLocMot.transform.position, yPos, (0.1f + (0.5f * bobStrength / (0.5f * bobSpeed)) * GameTime.deltaTime));

								if (!m_InterpolateWaterVelocity && !m_InterpolateAnimatedVelocity)
									charLocMot.transform.position = Vector3.Lerp (charLocMot.transform.position, yPos, (0.75f + (0.5f * bobStrength)) * GameTime.deltaTime);

								if (m_InterpolateAnimatedVelocity && !m_InterpolateWaterVelocity) 
								{
									yPos = new Vector3 (charLocMot.transform.position.x, surColPos.y - (charLocSizeY) - 0.2f, charLocMot.transform.position.z);
									charLocMot.transform.position = Vector3.Lerp (charLocMot.transform.position, yPos, 3f * GameTime.deltaTime);

									//charLocMot.transform.position = Vector3.Lerp (charLocMot.transform.position, yPos, (0.1f + (0.5f * bobStrength / (0.5f * bobSpeed)) * GameTime.deltaTime));
								}
							}
					
							float bob = (HydraMathUtils.Abs ((GameTime.fixedTime * bobSpeed) % 4 - 2) - 1) * bobStrength;
							charLocMot.velocity += Vector3.up * bob;
						}
					}
				}
			}
		}

		/// <summary>
		/// 	Updates water object velocity.
		/// </summary>
		/// <param name="other">Other.</param>
		private void UpdateWaterVelocity()
		{
			if (GameTime.paused) 
			{
				this.rigidbody.velocity = Vector3.zero;
				return;
			}

			//Apply friction
			if (m_EnableFriction) 
				this.rigidbody.velocity -= m_Friction * this.rigidbody.velocity;
			
			// Bob
			if (m_EnableBob)
			{
				float bob = ((HydraMathUtils.Abs((GameTime.fixedTime * bobSpeed) % 4 - 2) - 1) * bobStrength);
				this.rigidbody.velocity += Vector3.up * bob;
			}
		}
		
		/// <summary>
		/// 	Keep a record of the entry velocity of the object.
		/// </summary>
		/// <param name="other">Other.</param>
		private void AddKey(Collider other)
		{
			CharacterLocomotor charLocMot = other.GetComponent<CharacterLocomotor>();
			Rigidbody rigbod = other.GetComponent<Rigidbody>();
			GameObject key = null;

			if ((rigbod == null && charLocMot == null) || other.gameObject.layer == 4 || other.gameObject.layer == 16)
				return;

			if (charLocMot != null && charLocMot.enabled) 
			{
				key = charLocMot.gameObject;
				m_EntryVelocityDict[key] = charLocMot.velocity.y;
				m_EntryVelocityBool[key] = true;
			}
			
			if (rigbod != null) //&& key == null <-- IF PLAYER GETS MESSED UP IN UNDERWATER MODE
			{
				rigbod.useGravity = false; //Else custom gravity won't work
				//rigidbody.freezeRotation = true;
				key = rigbod.gameObject;
				m_EntryVelocityDict[key] = rigbod.velocity.y;
				m_EntryVelocityBool[key] = true;
			}
		}

		/// <summary>
		/// 	Remove the entry for the object.
		/// </summary>
		/// <param name="other">Other.</param>
		private void RemoveKey(Collider other)
		{
			CharacterLocomotor charLocMot = other.GetComponent<CharacterLocomotor>();
			Rigidbody rigbod = other.GetComponent<Rigidbody>();
			GameObject key = null;

			//Ignore water and moving platforms
			if ((rigbod == null && charLocMot == null) || other.gameObject.layer == 4 || other.gameObject.layer == 16)
				return;

			if (charLocMot != null && charLocMot.enabled)
				key = charLocMot.gameObject;

			if (rigbod != null) 
			{
				key = rigbod.gameObject;
				rigbod.useGravity = true;
				//rigidbody.freezeRotation = false;
			}

			if (m_EntryVelocityDict.ContainsKey(key))
				m_EntryVelocityDict.Remove(key);

			if (m_EntryVelocityBool.ContainsKey(key))
				m_EntryVelocityBool.Remove(key);
		}

		/// <summary>
		/// 	Gets the immersed volume, as a proportion of the total volume of the object.
		/// 
		/// 	This is a very rough calculation that just calculates overlap in the y axis.
		/// </summary>
		/// <returns>The immersed volume.</returns>
		/// <param name="other">Other.</param>
		private float GetImmersedVolume(Collider other)
		{
			float otherTop;
			float otherBottom;
			float waterTop;
			float waterBottom;

			GetColliderTopBottom(other, other.transform.position, out otherTop, out otherBottom);
			GetColliderTopBottom(collider, other.transform.position, out waterTop, out waterBottom);

			float objectHeight = otherTop - otherBottom;
			float overlap = objectHeight;

			if (otherTop > waterTop)
				overlap -= (otherTop - waterTop);

			if (otherBottom < waterBottom)
				overlap -= (waterBottom - otherBottom);

			return HydraMathUtils.Max(overlap, 0.0f) / objectHeight;
		}

		/// <summary>
		/// 	Gets the collider top and bottom.
		/// </summary>
		/// <param name="other">Other.</param>
		/// <param name="position">Position.</param>
		/// <param name="top">Top.</param>
		/// <param name="bottom">Bottom.</param>
		private void GetColliderTopBottom(Collider other, Vector3 position, out float top, out float bottom)
		{
			top = other.bounds.max.y;
			bottom = other.bounds.min.y;
		}

		/// <summary>
		/// 	Configures the collider.
		/// </summary>
		private void ConfigureCollider()
		{
			collider.isTrigger = true;
		}

		/// <summary>
		/// 	Stops interpolation of the water velocity
		/// </summary>
		private void StopVelInterpolation(GameObject gamus)
		{
			m_EntryVelocityBool[gamus] = false;
		}

		/// <summary>
		/// 	Simulate water velocity
		/// </summary>
		private float SimulatedWaterVelocity(Collider otherCollider, float originVelocity)
		{
			float floatHeight = otherCollider.bounds.size.y * (2.0f / 3.0f);
			float immersedHeight = GetImmersedVolume(otherCollider) * otherCollider.bounds.size.y;
			
			float delta = immersedHeight / floatHeight;
			float clampedDelta = HydraMathUtils.Clamp(delta, 0.0f, 1.0f);
			
			float sinkVelocity = Interpolater.Interpolate(clampedDelta, originVelocity, 0.0f,
			                                              Interpolater.Interpolant.Sinusoidal);

			return sinkVelocity;
		}

		private bool IgnoreUpwardsInterpolation(Collider collidus)
		{
			Vector3 rayPosition = new Vector3(collidus.transform.position.x, collidus.transform.position.y,
			                                  (collidus.transform.position.z));

			RaycastHit hitInfo;

			//Ignore water, player and enemy
			LayerMask layerToIgnore = 1 << 4;
			layerToIgnore |= 1 << 12;
			layerToIgnore |= 1 << 14;

			if (Physics.Raycast (rayPosition, Vector3.up, out hitInfo, Mathf.Infinity, ~layerToIgnore))
			{
				//TODO: REPLACE VALUE WITH CONSTANT
				if (hitInfo.distance < 2.3f)
				return true;
			}
			
			return false;
		}

		private void UpdateAnimWater()
		{
			Animator waterAnimation = this.gameObject.GetComponent<Animator>();

			if (waterAnimation == null) 
			{
				Debug.LogError ("Animator missing from water object - an Animator component must be attached to the water object!");
				return;
			}

			//There is a pause function in the Animator class, but it's not consistent(The root position of the water will change after multiple pauses) so the speed is set to zero instead
			if (GameTime.paused)
			{
				if (waterAnimation.speed == 0)
					return;

				m_origAnimatedSpeed = waterAnimation.speed; 
				waterAnimation.speed = 0;
				//this.rigidbody.velocity = Vector3.zero;
				return;
			} 

			if (!GameTime.paused && waterAnimation.speed == 0) 
			{
				waterAnimation.speed = m_origAnimatedSpeed;
			}
				
			/*//Apply friction
			if (m_EnableFriction) 
				this.rigidbody.velocity -= m_Friction * this.rigidbody.velocity;

			// Bob
			if (m_EnableBob)
			{
				float bob = ((Mathf.Abs((GameTime.fixedTime * bobSpeed) % 4 - 2) - 1) * bobStrength);
				this.rigidbody.velocity += Vector3.up * bob;
			}*/
		}

		private void CheckForOtherWaters(Collider other)
		{
			WaterTrigger watTrig = other.GetComponent<WaterTrigger>();

			if (watTrig == null || (other.gameObject == this.gameObject))
				return;

			if (watTrig.interpolatePlayerUpwards == m_InterpolatePlayerUpwards) 
			{
				m_CheckForOtherWaters = false;
				return;
			}

			watTrig.interpolatePlayerUpwards = m_InterpolatePlayerUpwards;
			watTrig.checkForOtherWaters = true;
		}
		
		#endregion

		#region Public Methods

		/// <summary>
		/// 	Detects if a collidable object should float or not
		/// </summary>
		public bool AllowFloatState(Collider collidus, Vector3 direction, float distance, float waterHeightOffset)
		{
			Vector3 rayPosition = new Vector3(collidus.transform.position.x, collidus.transform.position.y + waterHeightOffset,
			                                  (collidus.transform.position.z));
			RaycastHit hitInfo;
			
			LayerMask layerToIgnore = 1 << 4; //IGNORE EVERYTHING BUT OBJECTS WITH LAYER "WATER"

			if (Physics.Raycast (rayPosition, direction, out hitInfo, distance, layerToIgnore) && 
			    (hitInfo.collider.GetComponent<WaterTrigger>() != null || hitInfo.collider.GetComponentInParent<WaterTrigger>() != null)) 
				return true;
			
			return false;
		}

		#endregion
	}
}