﻿using AntonCoolpecker.Concrete.Collectables;
using AntonCoolpecker.Concrete.Enemies;
using AntonCoolpecker.Concrete.Menus.HUD;
using Hydra.HydraCommon.Abstract;
using System.Collections;
using UnityEngine;

/// <summary>
/// Player statistics - This script keeps track of the player
/// statistics displayed in the GUI in-game.
/// </summary>
public class PlayerStatistics : HydraMonoBehaviour
{
	#region Variables

	[SerializeField] private AnimatingIcon[] m_AnimatingIcons;
	[SerializeField] private Texture2D[] m_Toasts;
	[SerializeField] private Texture2D[] m_BossToasts;
	[SerializeField] private Texture2D[] m_Portraits;
	[SerializeField] private HUDDigit m_Digits;
	[SerializeField] private int m_GUIDepth;
	[SerializeField] private int m_MaxHealth;
	[SerializeField] private static int m_Lives = 3; // changed to static so that lives are kept between levels
	[SerializeField] private Vector2 m_Offset;
	[SerializeField] private float m_HealthShowTime;
	[SerializeField] private float m_HealthHideTime;
	[SerializeField] private float m_PulseHeartBigTime;
	[SerializeField] private float m_PulseHeartRegularTime;
	[SerializeField] private float m_MaxHitShowTime;

    public static PlayerStatistics PlayerCollectableStatistics; //Static reference to current PlayerStatistics instance.
    private static BossController m_CurrentBoss; //Reference to current boss that the player is fighting.
    private static BossHealthHUDElement m_BossHUD; //HUD for the current boss that the player is fighting.

	public static HUDElement[] elements = new HUDElement[5]; //HUD elements for displaying player statistics.
	private static int[] m_Amounts = new int[5];

	#endregion

	#region Properties

	/// <summary>
	/// Gets or sets the current boss.
	/// </summary>
	/// <value>The current boss.</value>
    public static BossController currentBoss { get { return m_CurrentBoss; }
        set
        {
            m_CurrentBoss = value;
            m_BossHUD.SetMaxHealth();
        }
    }

	/// <summary>
	/// Gets the boss HUD.
	/// </summary>
	/// <value>The boss HUD.</value>
    public static BossHealthHUDElement bossHUD { get { return m_BossHUD; } }

	//TODO: Integrate this with PlayerProgress branch
	/// <summary>
	/// Gets or sets the max health.
	/// </summary>
	/// <value>The max health.</value>
    public int maxHealth
    {
        get
        {
            return m_MaxHealth;
        }
        set
        {
            m_MaxHealth = value;
            HealthHUDElement elem = (HealthHUDElement)elements[0];
            elem.SetMaxHealth();
        }
    }

	/// <summary>
	/// Gets or sets the lives.
	/// </summary>
	/// <value>The lives.</value>
    public int lives
    {
        get { return m_Lives; }
        set
        {
            if (value > 9)
                return;
            if (value < 0)
                return;
            m_Lives = value;
        }
    }

	/// <summary>
	/// Gets or sets the <see cref="PlayerStatistics"/> with the specified type.
	/// </summary>
	/// <param name="type">Type.</param>
    public int this[CollectableType type]
    {
        get { return m_Amounts[(int)type]; }
        set
        {
            if (type == CollectableType.Health)
            {
                if(value < m_Amounts[(int)type])
                {
                    elements[0].TakeDamage();
                }

                if(value > maxHealth)
                {
                    m_Amounts[(int)type] = maxHealth;
                }
                else if(value < 0)
                {
                    m_Amounts[(int)type] = 0;
                }
                else
                {
                    m_Amounts[(int)type] = value;
                }

                elements[0].CheckHealthLevels();
            }
            else
            {
                m_Amounts[(int)type] = value;
            }

            elements[(int)type].SetNumbers(value);
        }
    }

	#endregion

	#region Override Functions

	/// <summary>
	/// Called when the object is instantiated.
	/// </summary>
    protected override void Awake()
    {
        base.Awake();

        if (PlayerCollectableStatistics == null)
        {
            PlayerCollectableStatistics = this;

            //health.Set(m_Toasts, m_Portraits);
            for (int i = 0; i < elements.Length; i++)
            {
				CollectableType type = (CollectableType)i;
                if (type == CollectableType.Health)
                {
                    HealthHUDElement toSet = new HealthHUDElement();
                    toSet.Set(m_Toasts, m_Digits, m_Portraits, m_HealthHideTime, m_HealthShowTime, m_Offset, m_PulseHeartRegularTime, m_PulseHeartBigTime, m_MaxHitShowTime);
                    elements[i] = toSet;
                    this[type] = maxHealth;
                }
                else
                {
                    elements[i] = new HUDElement();
                    elements[i].Set((i) * 2, 1.5f, m_Digits, m_AnimatingIcons[i - 1], 0, 0.75f, 0.25f, m_Offset);
                }
            }

            m_BossHUD = new BossHealthHUDElement();
            m_BossHUD.Set(m_BossToasts, m_Digits, m_Portraits, m_HealthHideTime, m_HealthShowTime, m_Offset, m_PulseHeartRegularTime, m_PulseHeartBigTime, m_MaxHitShowTime);

            maxHealth = maxHealth; //Used to set the HUD Health Element.
            //this[0] = maxHealth;

            DontDestroyOnLoad(gameObject);
        }
        else if (PlayerCollectableStatistics != this)
        {
            Destroy(gameObject);
        }
    }

    /// <summary>
    /// Called before the first Update.
    /// </summary>
    protected override void Start ()
    {
        base.Start();
    }

	/// <summary>
	/// Called when the component is enabled.
	/// </summary>
    protected override void OnEnable()
    {
        base.OnEnable();
    }

    /// <summary>
    /// Called once every frame.
    /// </summary>
    protected override void Update ()
    {
        base.Update();
        //health.Update();

        for(int i = 0; i < elements.Length; i++)
        {
            elements[i].Update();
        }

        if (currentBoss != null)
            m_BossHUD.Update();
    }

	#endregion

	#region Protected/Public Functions

	/// <summary>
	/// Called when the GUI is drawn.
	/// </summary>
    protected void OnGUI()
    {
        GUI.depth = m_GUIDepth;
        //base.OnGUI();
        //health.OnGUI();

        for (int i = 0; i < elements.Length; i++)
        {
            GUI.color = Color.white;
            elements[i].OnGUI();
        }

        if (currentBoss != null)
            m_BossHUD.OnGUI();
    }

	/// <summary>
	/// Displays all of the player statistics related elements in the GUI.
	/// </summary>
    public void DisplayAll()
    {
        for (int i = 0; i < elements.Length; i++)
        {
            elements[i].ForceShow();
        }
    }

	/// <summary>
	/// Hides all of the player statistics related elements in the GUI.
	/// </summary>
    public void HideAll()
    {
        for (int i = 0; i < elements.Length; i++)
        {
            elements[i].ForceHide();
        }
    }

	#endregion
}