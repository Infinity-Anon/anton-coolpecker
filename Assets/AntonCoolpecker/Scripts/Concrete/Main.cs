﻿using AntonCoolpecker.Abstract.Menus.States;
using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using AntonCoolpecker.Concrete.Menus;
using AntonCoolpecker.Concrete.Menus.DebugGUI;
using AntonCoolpecker.Concrete.Progress;
using AntonCoolpecker.Concrete.Scene;
using AntonCoolpecker.Utils.Time;
using Hydra.HydraCommon.Abstract;
using Hydra.HydraCommon.Utils;
using System;
using UnityEngine;

namespace AntonCoolpecker.Concrete
{
	/// <summary>
	/// 	Main prefabs.
	/// </summary>
	[Serializable]
	public class MainPrefabs
	{
		#region Variables

		[SerializeField] private SceneManager m_SceneManagerPrefab; //Reference to Scene Manager prefab
		[SerializeField] private DebugGuiComponent m_DebugGuiPrefab; //Reference to Debug GUI prefab

		#endregion

		#region Properties

		/// <summary>
		/// Gets the scene manager prefab.
		/// </summary>
		/// <value>The scene manager prefab.</value>
		public SceneManager sceneManagerPrefab { get { return m_SceneManagerPrefab; } }

		/// <summary>
		/// Gets the debug GUI prefab.
		/// </summary>
		/// <value>The debug GUI prefab.</value>
		public DebugGuiComponent debugGuiPrefab { get { return m_DebugGuiPrefab; } }

		#endregion
	}

	/// <summary>
	/// 	Main is the top-level class for the AntonCoolpecker application.
	/// </summary>
	[ExecuteInEditMode]
	public class Main : SingletonHydraMonoBehaviour<Main>
	{
		#region Variables

		[SerializeField] private MainPrefabs m_Prefabs; //Reference to the main prefabs for Main.
		[SerializeField] private SceneManager m_SceneManager; //Reference to the Scene Manager.
		[SerializeField] private DebugGuiComponent m_DebugGui; //Reference to the Debug GUI.
		[SerializeField] private MenuController m_MenuController; //Reference to the Menu Controller.
		[SerializeField] private AbstractMenuState m_PauseMenu; //Reference to the pause menu.
		[SerializeField] private PlayerStatistics m_PlayerStatistics; //Reference to the player statistics.
		[SerializeField] private PlayerProgress m_PlayerProgress; //Reference to the player progress.

        private bool m_CanPause = true; //Keeps track of whether the game can be paused or not.

		#endregion

		#region Properties

		/// <summary>
		/// 	Gets the prefab factory.
		/// </summary>
		/// <value>The prefab factory.</value>
		public MainPrefabs prefabs { get { return m_Prefabs ?? (m_Prefabs = new MainPrefabs()); } }

		/// <summary>
		/// 	Gets the scene manager.
		/// </summary>
		/// <value>The scene manager.</value>
		public SceneManager sceneManager { get { return m_SceneManager; } }

		/// <summary>
		/// 	Gets the debug GUI.
		/// </summary>
		/// <value>The debug GUI.</value>
		public DebugGuiComponent debugGui { get { return m_DebugGui; } }

		/// <summary>
		/// Gets the menu controller.
		/// </summary>
		/// <value>The menu controller.</value>
		public MenuController menuController { get { return m_MenuController; } }

		/// <summary>
		/// Gets the player statistics.
		/// </summary>
		/// <value>The player statistics.</value>
        public PlayerStatistics playerStatistics { get { return m_PlayerStatistics; } }

		/// <summary>
		/// Gets the player progress.
		/// </summary>
		/// <value>The player progress.</value>
        public PlayerProgress playerProgress { get { return m_PlayerProgress; } }

		#endregion

		#region Messages

		/// <summary>
		/// 	Called when the component is enabled.
		/// </summary>
		protected override void OnEnable()
		{
			base.OnEnable();

			InstantiateDependencies();
		}

		/// <summary>
		/// 	Called before the first Update.
		/// </summary>
		protected override void Start()
		{
			base.Start();
		}

		/// <summary>
		/// 	Called once every frame.
		/// </summary>
		protected override void Update()
		{
			base.Update();

			if (Application.isPlaying)
				UpdateDebugGuiVisibility();

			if (InputMapping.pause.GetButtonDown() && m_CanPause && !GameTime.paused) 
			{
				menuController.NextMenu(m_PauseMenu);
				LockCursor(!GameTime.paused);
            }

            if (!m_MenuController.currentMenuState.IsNonState())
            {
                if (m_MenuController.currentMenuState == m_PauseMenu)
                {
                    playerStatistics.DisplayAll();
                }
                else
                {
                    playerStatistics.HideAll();
                }
            }
		}

		#endregion

		#region Methods

		/// <summary>
		/// 	Instantiates the dependencies.
		/// </summary>
		public void InstantiateDependencies()
		{
			ObjectUtils.LazyInstantiateOrFind(prefabs.sceneManagerPrefab, ref m_SceneManager);
			m_SceneManager.InstantiateDependencies();

			if (Application.isPlaying)
				ObjectUtils.LazyInstantiateOrFind(prefabs.debugGuiPrefab, ref m_DebugGui);
		}

		/// <summary>
		/// 	Sets whether the game is pausable or not
		/// </summary>
		public void SetPause(bool enablePause)
		{
			m_CanPause = enablePause;
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// 	Updates the debug GUI visibility.
		/// </summary>
		private void UpdateDebugGuiVisibility()
		{
			if (InputMapping.toggleDebugUI.GetButtonDown())
			{
				ShowDebugGui(!m_DebugGui.IsVisible());
				LockCursor(false);
			}

			if (Input.GetMouseButtonDown(0))
			{
				if (m_DebugGui.IsVisible())
				{
					Rect debugGUIRect = debugGui.GetGUIRect();
					Vector2 mousePosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

					if (!debugGUIRect.Contains(mousePosition))
						ShowDebugGui(false);
				} 
				else if(!GameTime.paused)
				{
					LockCursor(true);
				}
			}
		}

		/// <summary>
		/// 	Locks the cursor.
		/// </summary>
		/// <param name="lockState">Locks if true.</param>
		private void LockCursor(bool lockState)
		{
			Cursor.lockState = lockState ? CursorLockMode.Locked : CursorLockMode.None;
			Cursor.visible = !lockState;
			m_SceneManager.playerCamera.orbitEnabled = lockState;
		}

		/// <summary>
		/// 	Show or hide the debug GUI.
		/// </summary>
		/// <param name="shown">If set to <c>true</c>, show the debug GUI.</param>
		private void ShowDebugGui(bool shown)
		{
			LockCursor(!shown);

			if (shown) 
			{
				GameTime.paused = true;
				m_DebugGui.Show();
			} 
			else 
			{
				m_DebugGui.Hide();
				GameTime.paused = false;
			}
		}

		#endregion
	}
}