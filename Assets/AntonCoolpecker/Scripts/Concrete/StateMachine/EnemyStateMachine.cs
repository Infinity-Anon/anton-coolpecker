﻿using AntonCoolpecker.Abstract.Enemies.States;
using AntonCoolpecker.Abstract.StateMachine;
using AntonCoolpecker.Concrete.Enemies;
using AntonCoolpecker.Concrete.Messaging;
using AntonCoolpecker.Utils.Time;
using System;
using UnityEngine;

namespace AntonCoolpecker.Concrete.StateMachine
{
	[Serializable]
	public class EnemyStateMachine : AbstractStateMachine<AbstractEnemyState>
	{
		#region Variables

		[SerializeField] private AbstractEnemyState m_InitialState;

		#endregion

		#region Properties

		public override AbstractEnemyState initialState { get { return m_InitialState; } }

		#endregion

		#region Functions

		/// <summary>
		/// 	Called when the state changes.
		/// </summary>
		/// <param name="previous">Previous.</param>
		/// <param name="current">Current.</param>
		/// <param name="parent">Parent.</param>
		protected override void OnStateChanged(AbstractEnemyState previous, AbstractEnemyState current, MonoBehaviour parent)
		{
			base.OnStateChanged(previous, current, parent);

			EnemyController enemy = parent as EnemyController;

			EnemyMessages.BroadcastOnStateChange(enemy, new StateChangeInfo<AbstractEnemyState>(previous, current));
		}

		/// <summary>
		/// Only update enemy states when the game is not paused
		/// </summary>
		/// <param name="parent"></param>
		public new void Update(MonoBehaviour parent)
		{
			if (!GameTime.paused)
			{
				base.Update(parent);
			}
		}

		/// <summary>
		/// Only update enemy states when the game is not paused
		/// </summary>
		/// <param name="parent"></param>
		public new void FixedUpdate(MonoBehaviour parent)
		{
			if (!GameTime.paused)
			{
				base.FixedUpdate(parent);
			}
		}

		/// <summary>
		/// Only update enemy states when the game is not paused
		/// </summary>
		/// <param name="parent"></param>
		public new void LateUpdate(MonoBehaviour parent)
		{
			if (!GameTime.paused)
			{
				base.LateUpdate(parent);
			}
		}

		#endregion
	}
}