﻿using AntonCoolpecker.Abstract.Player;
using AntonCoolpecker.Abstract.Player.States;
using AntonCoolpecker.Abstract.Player.States.Hitstun;
using AntonCoolpecker.Abstract.StateMachine;
using AntonCoolpecker.Concrete.Messaging;
using AntonCoolpecker.Utils.Time;
using System;
using UnityEngine;

namespace AntonCoolpecker.Concrete.StateMachine
{
	/// <summary>
	/// 	Player state machine.
	/// </summary>
	[Serializable]
	public class PlayerStateMachine : AbstractStateMachine<AbstractPlayerState>
	{
		#region Variables

		[SerializeField] private AbstractPlayerState m_InitialState;

		#endregion

		#region Properties

		/// <summary>
		/// 	Gets the initial state.
		/// </summary>
		/// <value>The initial state.</value>
		public override AbstractPlayerState initialState { get { return m_InitialState; } }

		#endregion

		#region Methods

		/// <summary>
		/// 	Returns the state that handles hitstun.
		/// </summary>
		/// <returns>The hitstun state.</returns>
		public AbstractHitstunState GetHitstunState()
		{
			return activeState.GetHitstunState();
		}

		#endregion

		#region Functions

		/// <summary>
		/// 	Passes the controller collider hit message to the active state.
		/// </summary>
		/// <param name="hit">Hit.</param>
		/// <param name="parent">Parent.</param>
		public void OnControllerColliderHit(ControllerColliderHit hit, MonoBehaviour parent)
		{
			ProceedCollisionState(hit, parent);
			activeState.ParentControllerColliderHit(hit, parent);
		}

		/// <summary>
		/// Only update player states when the game is not paused
		/// </summary>
		/// <param name="parent"></param>
		public new void Update(MonoBehaviour parent)
		{
			if (!GameTime.paused)
			{
				base.Update(parent);
			}
		}

		/// <summary>
		/// Only update player states when the game is not paused
		/// </summary>
		/// <param name="parent"></param>
		public new void FixedUpdate(MonoBehaviour parent)
		{
			if (!GameTime.paused)
			{
				base.FixedUpdate(parent);
			}
		}

		/// <summary>
		/// Only update player states when the game is not paused
		/// </summary>
		/// <param name="parent"></param>
		public new void LateUpdate(MonoBehaviour parent)
		{
			if (!GameTime.paused)
			{
				base.LateUpdate(parent);
			}
		}

		#endregion

		#region Override Functions

		/// <summary>
		/// 	Called when the state changes.
		/// </summary>
		/// <param name="previous">Previous.</param>
		/// <param name="current">Current.</param>
		/// <param name="parent">Parent.</param>
		protected override void OnStateChanged(AbstractPlayerState previous, AbstractPlayerState current, MonoBehaviour parent)
		{
			base.OnStateChanged(previous, current, parent);

			AbstractPlayerController player = parent as AbstractPlayerController;
			PlayerMessages.BroadcastOnStateChange(player, new StateChangeInfo<AbstractPlayerState>(previous, current));
		}

		/// <summary>
		/// 	Proceeds to the next state in a collision event.
		/// </summary>
		/// <param name="hit">Hit.</param>
		/// <param name="parent">Parent.</param>
		private void ProceedCollisionState(ControllerColliderHit hit, MonoBehaviour parent)
		{
			AbstractPlayerState next = activeState.GetCollisionNextState(hit, parent);
			SetActiveState(next, parent);
		}

		#endregion
	}
}