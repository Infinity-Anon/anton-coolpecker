﻿using UnityEngine;

namespace AntonCoolpecker.Concrete.Utils
{
	/// <summary>
	/// Structure for storing Character Locomotor collision data.
	/// </summary>
	public struct CharacterLocomotorCollisionData
	{
		#region Variables

		private readonly CharacterController m_Controller;
		private readonly Vector3 m_Point;

		#endregion

		#region Properties

		/// <summary>
		/// The controller that hit the collider.
		/// </summary>
		/// <value>The originator controller.</value>
		public CharacterController Controller { get { return m_Controller; } }

		/// <summary>
		/// The point of the collision in world space.
		/// </summary>
		/// <value>The point.</value>
		public Vector3 point { get { return m_Point; } }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="AntonCoolpecker.Concrete.Utils.CharacterLocomotorCollisionData"/> struct.
		/// </summary>
		/// <param name="controller">Controller.</param>
		/// <param name="point">Point.</param>
		public CharacterLocomotorCollisionData(CharacterController controller, Vector3 point)
		{
			m_Controller = controller;
			m_Point = point;
		}

		#endregion
	}
}