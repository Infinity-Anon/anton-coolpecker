﻿using System;
using Hydra.HydraCommon.Abstract;
using UnityEngine;
using UnityEngine.UI;

namespace AntonCoolpecker.Concrete.Utils.UI
{
	/// <summary>
	/// 	ButtonWrapper is a component that provides some extended functionality to a UI Button.
	/// </summary>
	[RequireComponent(typeof(Button))]
	public class ButtonWrapper : HydraMonoBehaviour
	{
		public event EventHandler onClickedCallback;

		[SerializeField] private Text m_Text;

		private Button m_Cachedbutton;
		private Text m_CachedText;

		#region Properties

		/// <summary>
		/// 	Gets the button.
		/// </summary>
		/// <value>The button.</value>
		public Button button { get { return m_Cachedbutton ?? (m_Cachedbutton = GetComponent<Button>()); } }

		/// <summary>
		/// 	Gets the text.
		/// </summary>
		/// <value>The text.</value>
		public Text text { get { return m_Text; } }

		#endregion

		#region Messages

		/// <summary>
		/// 	Called when the component is enabled.
		/// </summary>
		protected override void OnEnable()
		{
			base.OnEnable();

			Subscribe(button);
		}

		/// <summary>
		/// 	Called when the component is disabled.
		/// </summary>
		protected override void OnDisable()
		{
			base.OnDisable();

			Unsubscribe(button);
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// 	Subscribe the specified button.
		/// </summary>
		/// <param name="button">Button.</param>
		private void Subscribe(Button button)
		{
			button.onClick.AddListener(OnClick);
		}

		/// <summary>
		/// 	Unsubscribe the specified button.
		/// </summary>
		/// <param name="button">Button.</param>
		private void Unsubscribe(Button button)
		{
			button.onClick.RemoveListener(OnClick);
		}

		/// <summary>
		/// 	Raises the click event.
		/// </summary>
		private void OnClick()
		{
			EventHandler handler = onClickedCallback;
			if (handler != null)
				handler(this, EventArgs.Empty);
		}

		#endregion
	}
}
