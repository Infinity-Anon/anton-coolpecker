﻿using UnityEngine.Events;

namespace AntonCoolpecker.Concrete.Utils.UI
{

	[System.Serializable]
	public class FileSelectedEvent : UnityEvent<string>
	{
	}
}
