﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Runtime.InteropServices;
using UnityEngine.EventSystems;

namespace AntonCoolpecker.Concrete.Utils.UI
{

	/// <summary>
	/// Opens a file select dialog when clicked in WEBGL builds. Once the file is selected, fires onFileSelected with the (file:///) URL of the selected file.
	/// Does nothing in non-WEBGL builds.
	/// </summary>
	public class FileUploadListener : MonoBehaviour, IPointerDownHandler
	{
#if UNITY_WEBGL
		[DllImport("__Internal")]
		private static extern void FileUploaderCaptureClick();
#endif

		void Start()
		{
			if (onFileSelected == null) {
				onFileSelected = new FileSelectedEvent();
			}
		}

		public void OnPointerDown(PointerEventData eventData)
		{
			Debug.LogWarning("Pointer Down");
#if UNITY_WEBGL
			FileUploaderCaptureClick();
#endif
		}

		void FileSelected(string url)
		{
			Debug.LogWarningFormat("File selected: {0}", url);
			onFileSelected.Invoke(url);
		}

		public FileSelectedEvent onFileSelected;

	}
}