﻿using AntonCoolpecker.Utils.Time;
using System;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Utils
{
	/// <summary>
	/// 	Timer is a simple class that allows for checking if an amount of
	/// 	time has elapsed.
	/// </summary>
	[Serializable]
	public class Timer
	{
		#region Variables

		[SerializeField] private float m_MaxTime;

		private float m_StartTime;
		private bool m_Paused;
		private float m_PausedTime;

		#endregion

		#region Properties

		/// <summary>
		/// 	Gets or sets the max time.
		/// </summary>
		/// <value>The max time.</value>
		public float maxTime { get { return m_MaxTime; } set { m_MaxTime = value; } }

		/// <summary>
		/// 	Gets the number of seconds that have passed since the timer started.
		/// </summary>
		/// <value>The time elapsed.</value>
		public float elapsed
		{
			get
			{
				float output = GameTime.time - m_StartTime;

				if (m_Paused)
					output -= elapsedPaused;

				return output;
			}
		}

		/// <summary>
		/// 	Gets the elapsed time as a percentage of the timer's max time.
		/// </summary>
		/// <value>The time elapsed as percentage.</value>
		public float elapsedAsPercentage { get { return elapsed / m_MaxTime; } }

		/// <summary>
		/// 	Returns the amount of time that has passed since the timer was last paused.
		/// </summary>
		/// <value>The seconds since the timer was paused.</value>
		public float elapsedPaused { get { return GameTime.time - m_PausedTime; } }

		/// <summary>
		/// 	Gets the number of seconds remaining.
		/// </summary>
		/// <value>The remaining time.</value>
		public float remaining { get { return m_MaxTime - elapsed; } }

		/// <summary>
		/// 	Gets a value indicating whether this <see cref="AntonCoolpecker.Concrete.Utils.Timer"/> is complete.
		/// </summary>
		/// <value><c>true</c> if complete; otherwise, <c>false</c>.</value>
		public bool complete { get { return elapsed >= m_MaxTime; } }

		/// <summary>
		/// 	Gets the number of seconds that have elapsed past the completion of the timer.
		/// </summary>
		/// <value>The overtime.</value>
		public float overtime { get { return elapsed - m_MaxTime; } }

		/// <summary>
		/// 	Gets or sets a value indicating whether this Timer is paused.
		/// </summary>
		/// <value><c>true</c> if paused; otherwise, <c>false</c>.</value>
		public bool paused
		{
			get { return m_Paused; }
			set
			{
				if (value == m_Paused)
					return;

				m_Paused = value;

				if (m_Paused)
					m_PausedTime = GameTime.time;
				else
					AddSeconds(GameTime.time - m_PausedTime);
			}
		}

		#endregion

		#region Constructors

		public Timer()
		{
			m_MaxTime = 0;
			// Don't initialize the start time. When the timer is instantiated, it doesn't have access to Time.time.
			m_Paused = false;
			m_PausedTime = 0;
		}

		public Timer(Timer other)
		{
			m_MaxTime = other.m_MaxTime;
			m_StartTime = other.m_StartTime;
			m_Paused = other.m_Paused;
			m_PausedTime = other.m_PausedTime;
		}

		#endregion

		#region Functions

		/// <summary>
		/// 	Adds the seconds.
		/// </summary>
		/// <param name="seconds">Seconds.</param>
		public void AddSeconds(float seconds)
		{
			m_StartTime += seconds;
		}

		/// <summary>
		/// 	Resets the timer.
		/// </summary>
		public void Reset()
		{
			Resume();
			m_StartTime = GameTime.time;
		}

		/// <summary>
		/// 	Ends the timer early.
		/// </summary>
		public void End()
		{
			Resume();
			m_StartTime -= m_MaxTime;
		}

		/// <summary>
		/// 	Pause the timer.
		/// </summary>
		public void Pause()
		{
			paused = true;
		}

		/// <summary>
		/// 	Resume the timer.
		/// </summary>
		public void Resume()
		{
			paused = false;
		}

		#endregion
	}
}