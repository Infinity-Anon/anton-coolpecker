﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Utils
{
	/// <summary>
	/// 	Scene utilities.
	/// </summary>
	public static class SceneUtils
	{
		#region Variables

		private static List<string> s_CachedSceneNames;

		#endregion

		#region Properties

		/// <summary>
		/// 	Gets the scene name resources local path.
		/// </summary>
		/// <value>The scene name resources local path.</value>
		public static string sceneNameResourcesLocalPath { get { return "Text/Scenes.txt"; } }

		/// <summary>
		/// 	Gets the scene names resource path.
		/// </summary>
		/// <value>The scene names resource path.</value>
		public static string sceneNamesResourcePath
		{
			get { return Path.ChangeExtension(sceneNameResourcesLocalPath, null); }
		}

		/// <summary>
		/// 	Gets the scene names asset path.
		/// </summary>
		/// <value>The scene names asset path.</value>
		public static string sceneNamesAssetPath
		{
			get { return string.Format("AntonCoolpecker/Resources/{0}", sceneNameResourcesLocalPath); }
		}

		#endregion

		#region Methods

		/// <summary>
		/// 	Gets the name of the scene.
		/// </summary>
		/// <returns>The scene name.</returns>
		/// <param name="index">Index.</param>
		public static string GetSceneName(int index)
		{
			if (s_CachedSceneNames == null)
				CacheSceneNames();

			return s_CachedSceneNames[index];
		}

		/// <summary>
		/// 	Caches the scene names.
		/// </summary>
		public static void CacheSceneNames()
		{
			s_CachedSceneNames = new List<string>();

			TextAsset text = Resources.Load<TextAsset>(sceneNamesResourcePath);

			using (StringReader reader = new StringReader(text.text))
			{
				string line;
				while ((line = reader.ReadLine()) != null)
					s_CachedSceneNames.Add(line);
			}
		}

		#endregion
	}
}