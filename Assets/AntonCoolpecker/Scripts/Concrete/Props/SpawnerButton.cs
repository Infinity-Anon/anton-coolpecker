﻿using AntonCoolpecker.API.Damageable;
using AntonCoolpecker.Concrete.Utils;
using Hydra.HydraCommon.Abstract;
using System.Collections;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Props
{
	/// <summary>
	/// Spawner button - An object that spawns a prefab at a certain location when it's attacked/punched by player.
	/// </summary>
	public class SpawnerButton : HydraMonoBehaviour, IDamageable
	{
		#region Variables 

		[SerializeField] GameObject m_SpawnedPrefab; //Prefab to spawn
		[SerializeField] Vector3 m_SpawnLocation; //Where to spawn prefab

        int counter = 0;

		#endregion

		#region Messages

		public void Damage(int damage, CharacterLocomotorCollisionData collision)
		{
			if (m_SpawnedPrefab != null) 
			{
				GameObject newObject = (GameObject)GameObject.Instantiate(m_SpawnedPrefab, m_SpawnLocation, Quaternion.identity);
                newObject.name = newObject.name + " " + counter;
				newObject.SetActive (true);
                counter++;
			}
			else
				Debug.LogError ("Spawned Prefab is currently set to no prefab/reference! Please set proper prefab/reference in SpawnerButton before attacking this object!");
		}

		#endregion
	}
}