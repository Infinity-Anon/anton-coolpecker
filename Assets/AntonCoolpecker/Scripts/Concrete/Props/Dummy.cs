﻿using AntonCoolpecker.Abstract.Props;
using AntonCoolpecker.API.Damageable;
using AntonCoolpecker.Concrete.HitBoxes;
using AntonCoolpecker.Concrete.Props;
using AntonCoolpecker.Concrete.Utils;
using AntonCoolpecker.Utils.Time;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.AntonCoolpecker.Scripts.Concrete.Props
{
	/// <summary>
	/// Script for training dummy.
	/// </summary>
    class Dummy : HitBox, IDamageable
    {
		#region Variables

        Animator dummyAnimation; //Animator component for the dummy

		#endregion

		#region Functions

		/// <summary>
		/// Called when the component is enabled.
		/// </summary>
		protected override void OnEnable()
		{
			base.OnEnable();

            dummyAnimation = GetComponent<Animator>();
			dummyAnimation.gameObject.AddComponent<AnimatorGameTimeSubscriber>();
        }

		/// <summary>
		/// Called when the dummy gets damaged/hit.
		/// </summary>
		/// <param name="damage">Amount of damage to apply.</param>
		/// <param name="collision">Collision info.</param>
        public void Damage(int damage, CharacterLocomotorCollisionData collision)
        {
            if (dummyAnimation.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
                dummyAnimation.SetTrigger("IsHit");
            else
            {
                dummyAnimation.Play("Idle");
                dummyAnimation.SetTrigger("IsHit");
            }
        }

		#endregion
    }
}