using AntonCoolpecker.Abstract.Props.States;
using AntonCoolpecker.Abstract.StateMachine;
using System;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Props
{
	/// <summary>
	/// Prop state machine.
	/// </summary>
	[Serializable]
	public class PropStateMachine : AbstractStateMachine<AbstractPropState>
	{
		#region Variables

		[SerializeField] private AbstractPropState m_InitialState;

		#endregion

		#region Properties

		public override AbstractPropState initialState { get { return m_InitialState; } }

		#endregion
	}
}