﻿using AntonCoolpecker.Abstract.Player;
using AntonCoolpecker.Abstract.Props;
using AntonCoolpecker.Concrete.Scene;
using Hydra.HydraCommon.PropertyAttributes;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Props
{
	/// <summary>
	/// 	Portal transports the player to a different scene.
	/// </summary>
	[ExecuteInEditMode]
	public class Portal : AbstractTeleporter
	{
		#region Variables

		public const string SUFFIX = "Portal";

		[SerializeField] private SceneAttribute m_Scene;
		[SerializeField] private uint m_Index;
		[SerializeField] private uint m_TargetIndex;

		private static string s_Destination;

		#endregion

		#region Properties

		/// <summary>
		/// 	Gets the name of the target portal.
		/// </summary>
		/// <value>The name of the target portal.</value>
		public string targetName { get { return GetPortalName(Application.loadedLevelName, m_TargetIndex); } }

		#endregion

		#region Override Functions

		/// <summary>
		/// 	Called when the component is enabled.
		/// </summary>
		protected override void OnEnable()
		{
			base.OnEnable();

			if (m_Scene == null)
				m_Scene = new SceneAttribute();
		}

		/// <summary>
		/// 	Called once every frame.
		/// </summary>
		protected override void Update()
		{
			base.Update();

			if (!Application.isPlaying)
				gameObject.name = GetPortalName(m_Scene.name, m_Index);
		}

		/// <summary>
		/// 	Called before the first Update.
		/// </summary>
		protected override void Start()
		{
			base.Start();

			if (s_Destination != this.name)
				return;

			active = false;

			AbstractPlayerController player = SceneManager.instance.playerSwapper.GetActivePlayer();
			player.transform.position = transform.position;

			s_Destination = string.Empty;
		}

		/// <summary>
		/// 	Teleports the character to the destination.
		/// </summary>
		/// <param name="character">Character.</param>
		public override void Teleport(CharacterController character)
		{
			s_Destination = targetName;
			Application.LoadLevel(m_Scene.index);
		}

		#endregion

		#region Static Methods

		/// <summary>
		/// 	Gets the suffix.
		/// </summary>
		/// <returns>The suffix.</returns>
		/// <param name="id">Identifier.</param>
		public static string GetSuffix(uint id)
		{
			return " (" + SUFFIX + " " + id.ToString() + ")";
		}

		/// <summary>
		/// 	Gets the name of the portal.
		/// </summary>
		/// <returns>The portal name.</returns>
		/// <param name="scene">Scene.</param>
		/// <param name="id">Identifier.</param>
		public static string GetPortalName(string scene, uint id)
		{
			return scene + GetSuffix(id);
		}

		#endregion
	}
}