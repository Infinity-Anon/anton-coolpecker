using AntonCoolpecker.Abstract.Props.States;
using AntonCoolpecker.Concrete.Utils;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Props.BreakableWall.States
{
	/// <summary>
	/// Broken state - The state of a prop/wall has been broken/destroyed by the player.
	/// </summary>
	public class BrokenState : AbstractPropState
	{
		#region Variables

		[SerializeField] private UnbrokenState m_UnbrokenState;
		[SerializeField] private Timer m_Timer;
		[SerializeField] private Material m_BrokenMaterial;

		private BoxCollider m_CachedCollider;
		private Renderer m_CachedRenderer;

		#endregion

		#region Properties

		public Material material { get { return m_BrokenMaterial; } }

		#endregion

		#region Override Functions/Methods

		/// <summary>
		/// Returns a state for transition. Return self if no transition.
		/// </summary>
		/// <returns>The next state.</returns>
		/// <param name="parent">Parent.</param>
		public override AbstractPropState GetNextState(MonoBehaviour parent)
		{
			if (m_Timer.complete)
				return m_UnbrokenState;

			return base.GetNextState(parent);
		}

		/// <summary>
		/// Called when the state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnEnter(MonoBehaviour parent)
		{
			base.OnEnter(parent);

			m_Timer.Reset();

			m_CachedCollider = parent.gameObject.GetComponent<BoxCollider>();
			m_CachedRenderer = parent.gameObject.GetComponent<Renderer>();

			m_CachedCollider.enabled = false;
			m_CachedRenderer.material = material;
		}

		/// <summary>
		/// Called before another state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnExit(MonoBehaviour parent)
		{
			base.OnExit(parent);

			m_CachedCollider.enabled = true;
		}

		#endregion
	}
}