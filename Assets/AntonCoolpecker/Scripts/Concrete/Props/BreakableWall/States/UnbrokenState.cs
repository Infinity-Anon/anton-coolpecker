using AntonCoolpecker.Abstract.Props.States;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Props.BreakableWall.States
{
	/// <summary>
	/// Unbroken state.
	/// </summary>
	public class UnbrokenState : AbstractPropState
	{
		#region Variables

		[SerializeField] private Material m_UnbrokenMaterial;

		private Renderer m_CachedRenderer;

		#endregion

		#region Properties

		public Material material { get { return m_UnbrokenMaterial; } }

		#endregion

		#region Override Functions

		/// <summary>
		/// Called when the state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnEnter(MonoBehaviour parent)
		{
			base.OnEnter(parent);

			m_CachedRenderer = parent.gameObject.GetComponent<Renderer>();
			m_CachedRenderer.material = material;
		}

		#endregion
	}
}