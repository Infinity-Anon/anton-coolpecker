using AntonCoolpecker.Abstract.Props;
using AntonCoolpecker.Concrete.Props.BreakableWall.States;
using AntonCoolpecker.Concrete.Utils;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Props.BreakableWall
{
	/// <summary>
	/// Breakable wall controller.
	/// </summary>
	[DisallowMultipleComponent]
	public class BreakableWallController : AbstractPropController
	{
		#region Variables

		[SerializeField] private BrokenState m_BrokenState;

		#endregion

		#region Override Functions

		/// <summary>
		/// Called when the wall/prop gets damaged.
		/// </summary>
		/// <param name="damage">Amount of damage to apply.</param>
		/// <param name="collision">Collision info.</param>
		public override void Damage(int damage, CharacterLocomotorCollisionData collision)
		{
			base.Damage(damage, collision);

			//TODO: Set variable in UnbrokenState instead of SetActiveState()
			stateMachine.SetActiveState(m_BrokenState, this);
		}

		#endregion
	}
}