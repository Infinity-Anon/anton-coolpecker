﻿using AntonCoolpecker.Abstract.Props;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Props
{
	/// <summary>
	/// 	Teleporter transports the player within the scene.
	/// </summary>
	public class Teleporter : AbstractTeleporter
	{
		#region Variables

		[SerializeField] private Teleporter m_Target;

		#endregion

		#region Override Functions

		/// <summary>
		/// 	Teleports the character to the destination.
		/// </summary>
		/// <param name="character">Character.</param>
		public override void Teleport(CharacterController character)
		{
			m_Target.active = false;
			character.transform.position = m_Target.transform.position;
			TransitionSystem.transitionSystem.transitionIn ();
		}

		#endregion
	}
}