﻿using System.Collections;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Platforms
{
	/// <summary>
	/// Script for surfaces with ice behaviour.
	/// </summary>
	public class Ice : MonoBehaviour
	{
		#region Variables

		[SerializeField] private bool m_IsPartial;
		[SerializeField] private Vector3 m_Center;
		[SerializeField] private float m_Radius;

		#endregion

		#region Methods

		/// <summary>
		/// Detects whether the whole surface or a certain radius of the surface is icy.
		/// </summary>
		/// <returns><c>true</c>, if it's icy, <c>false</c> otherwise.</returns>
		/// <param name="point">Point.</param>
		public bool pointIsIcy(Vector3 point)
		{
			return m_IsPartial ? Vector3.Distance(point, m_Center) <= m_Radius : true;
		}

		#endregion
	}
}