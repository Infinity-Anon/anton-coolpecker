﻿using AntonCoolpecker.Abstract.Platforms;

namespace AntonCoolpecker.Concrete.Platforms
{
	/// <summary>
	/// "Wait" track instruction.
	/// </summary>
	public class WaitTrackInstruction : AbstractTrackInstruction {}
}