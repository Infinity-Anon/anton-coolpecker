﻿using AntonCoolpecker.Abstract.Platforms;
using AntonCoolpecker.Utils;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Platforms
{
	/// <summary>
	/// "Move" track instruction.
	/// </summary>
	public class MoveTrackInstruction : AbstractTrackInstruction
	{
		#region Variables

		[SerializeField] private Interpolater m_Interpolater;
		[SerializeField] private bool m_UseLargerRotationArc;
		[SerializeField] private AbstractTrackInstruction m_NextInstructionObject;

		#endregion

		#region Properties

		/// <summary>
		/// 	Gets the instruction set that comes after this object's set.
		/// </summary>
		/// <value>The next instruction set.</value>
		public override AbstractTrackInstruction[] nextInstructionSet
		{
			get { return m_NextInstructionObject.instructionSet; }
		}

		/// <summary>
		/// 	Gets the interpolater used to interpolate a follower's position along this instruction's path.
		/// </summary>
		/// <value>The interpolater.</value>
		protected Interpolater interpolater { get { return m_Interpolater; } }

		/// <summary>
		/// 	Gets the next instruction object.
		/// </summary>
		/// <value>The next instruction object.</value>
		protected AbstractTrackInstruction nextInstructionObject { get { return m_NextInstructionObject; } }

		#endregion

		#region Public Override Methods

		/// <summary>
		/// Gets the position that a track follower should be moving towards.
		/// </summary>
		/// <returns>The next position.</returns>
		/// <param name="follower">Follower.</param>
		public override Vector3 GetNextPosition(TrackFollower follower)
		{
			return interpolater.Interpolate(follower.instructionTimer.elapsedAsPercentage,
				transform.position,
				nextInstructionObject.transform.position);
		}
		
		/// <summary>
		/// 	Gets the rotation that a track follower should be turning towards.
		/// </summary>
		/// <returns>The next rotation.</returns>
		/// <param name="follower">Follower.</param>
		public override Quaternion GetNextRotation(TrackFollower follower)
		{
			return interpolater.Interpolate(follower.instructionTimer.elapsedAsPercentage,
				transform.rotation,
				nextInstructionObject.transform.rotation, m_UseLargerRotationArc);
		}

		#endregion
	}
}