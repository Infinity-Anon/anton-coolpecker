﻿using AntonCoolpecker.Abstract.Platforms;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Platforms
{
	/// <summary>
	/// "Wait For Player" track instruction.
	/// </summary>
	public class WaitForPlayerTrackInstruction : WaitTrackInstruction
	{
		#region Variables

		[SerializeField] private bool m_Invert; //Invert track instruction.

		#endregion

		#region Override Functions

		/// <summary>
		/// 	Adjusts a follower without changing its position, rotation, or scale.
		/// </summary>
		/// <param name="follower">Follower.</param>
		public override void UpdateFollower(TrackFollower follower)
		{
			if (follower.playerIsInPosition ^ m_Invert)
				follower.instructionTimer.End();
			else
				follower.instructionTimer.Reset();
		}

		#endregion
	}
}