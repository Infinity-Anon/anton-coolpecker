﻿using AntonCoolpecker.Abstract.Platforms;
using AntonCoolpecker.Concrete.Utils;
using AntonCoolpecker.Utils.Time;
using System.Collections.Generic;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Platforms
{
	/// <summary>
	/// Single pivot rotational platform - Platforms that rotate in all directions around a single pivot.
	/// </summary>
    public class SinglePivotRotationalPlatform : MovingPlatform
    {
		//!IMPORTANT! - MAKE SURE THE INITIAL Y ROTATION OF THE PLATFORM IS BETWEEN 0 AND 360 IF USING Y ROTATION - !IMPORTANT!

		#region Variables

		[SerializeField] private bool m_AllowXRotation; //Allows the platform to rotate in X axis.
		[SerializeField] private bool m_AllowYRotation; //Allows the platform to rotate in Y axis.
		[SerializeField] private bool m_AllowZRotation; //Allows the platform to rotate in Z axis.

		[SerializeField] private bool m_AllowXRotationDecel; //Allows the platform to decelerate its' rotation in X axis.
		[SerializeField] private bool m_AllowYRotationDecel; //Allows the platform to decelerate its' rotation in Y axis.
		[SerializeField] private bool m_AllowZRotationDecel; //Allows the platform to decelerate its' rotation in Z axis.

		[SerializeField] private bool m_InverseXRotation; //Inverse the rotation of the platform in X axis.
		[SerializeField] private bool m_InverseYRotation; //Inverse the rotation of the platform in Y axis.
		[SerializeField] private bool m_InverseZRotation; //Inverse the rotation of the platform in Z axis.
        
		[SerializeField] private float m_CharacterWeightX; //Amount of weight(Involving current weighted object(s) on platform) to apply to the platform acceleration in X axis.
		[SerializeField] private float m_CharacterWeightY; //Amount of weight(Involving current weighted object(s) on platform) to apply to the platform acceleration in Y axis.
		[SerializeField] private float m_CharacterWeightZ; //Amount of weight(Involving current weighted object(s) on platform) to apply to the platform acceleration in Z axis.

		[SerializeField] private float m_MaxAccelerationX; //The highest speed the platform can rotate in X axis.
		[SerializeField] private float m_MaxAccelerationY; //The highest speed the platform can rotate in Y axis.
		[SerializeField] private float m_MaxAccelerationZ; //The highest speed the platform can rotate in Z axis.

        [SerializeField] private float m_RotationFriction; //Multiplies current speed before applying to it.

		[SerializeField] private float m_EquilibriumForceX; //Force the platform uses to return to equilibrium in X axis.
		[SerializeField] private float m_EquilibriumForceY; //Force the platform uses to return to equilibrium in Y axis.
		[SerializeField] private float m_EquilibriumForceYLimit; //Limit of the force the platform uses to return to equilibrium. No limit if set to 0.
		[SerializeField] private float m_EquilibriumForceZ; //Force the platform uses to return to equilibrium in Z axis.

		[SerializeField] private float m_MaxAngleX; //The farthest the platform can rotate in X axis.
		[SerializeField] private float m_MaxAngleY; //The farthest the platform can rotate in Y axis
		[SerializeField] private float m_MinAngleY; //The farthest the platform can rotate in Y axis in the other direction.
		[SerializeField] private float m_MaxAngleZ; //The farthest the platform can rotate in Z axis.

        [SerializeField] private float m_DecelerationAngleX; //The point at which the platform begins to decelerate in X axis.
		[SerializeField] private float m_DecelerationMaxAngleY; //The point at which the platform begins to decelerate when reaching max Y angle.
		[SerializeField] private float m_DecelerationMinAngleY; //The point at which the platform begins to decelerate when reaching min Y angle.
		[SerializeField] private float m_DecelerationAngleZ; //The point at which the platform begins to decelerate in Z axis.

		[SerializeField] private bool m_UnlimitedYRotation; //Disables max/min angle limits for Y rotation axis.
		[SerializeField] private bool m_UseParentAsRotationPivot; //For rotating the platform around a parent gameobject(RECOMMENDED: Empty parent, only containing transform)

		private Quaternion baseRotation; //The rotation the system will always find as its resting position. This is set when the platform/object is first enabled.
        private Vector3 acceleration = new Vector3(); //Used for storing current platform acceleration
		private float yRotation = 0; //Used for calculating current Y axis eulerangle rotation
		private float yRotationPointMultiplier = 0; //Used for determining the amount of positive/negative loops(360 degrees) in Y axis eulerangle rotation

		#endregion

		#region Override Functions

        /// <summary>
        /// Called when script is first enabled.
        /// </summary>
        protected override void OnEnable()
        {
            base.OnEnable();

            baseRotation = transform.rotation;
			yRotation = baseRotation.eulerAngles.y;

			if (m_DecelerationAngleX >= m_MaxAngleX) 
			{
				Debug.LogError ("The deceleration X value must be lower than the maximum angle X value - Resetting decel value to 0");
				m_DecelerationAngleX = 0;
			}
			else if (m_DecelerationMaxAngleY >= m_MaxAngleY) 
			{
				Debug.LogError ("The deceleration maximum Y value must be lower than the maximum angle Y value - Resetting decel value to 0");
				m_DecelerationMaxAngleY = 0;
			}
			else if (m_DecelerationMaxAngleY <= m_MinAngleY) 
			{
				Debug.LogError ("The deceleration minimum Y value must be higher than the minimum angle Y value - Resetting decel value to 0");
				m_DecelerationMinAngleY = 0;
			}
			else if (m_DecelerationAngleZ >= m_MaxAngleZ) 
			{
				Debug.LogError ("The deceleration Z value must be lower than the maximum angle Z value - Resetting decel value to 0");
				m_DecelerationAngleZ = 0;
			}
        }

        /// <summary>
        /// Called once every physics update.
        /// </summary>
        protected override void FixedUpdate()
        {
            base.FixedUpdate();

            if (!GameTime.paused)
                AccelerateTowardsWeight(carriedCharacters);
        }

		#endregion

		#region Private Functions

        /// <summary>
        /// Rotates towards the combined "weight" positions on the platform.
        /// </summary>
        /// <param name="weightList">A list containing all character locomotors currently touching the platform</param>
        private void AccelerateTowardsWeight(List<CharacterLocomotor> weightList)
        {
            List<Vector3> relativePositions = new List<Vector3>();
            foreach (CharacterLocomotor weight in weightList) //Find each beginning relative posiiton
            {
				Vector3 relativeWeightPosition = transform.InverseTransformPoint(weight.transform.position);
                relativePositions.Add(relativeWeightPosition);

				//Inverse the platform rotation conditionally
				relativeWeightPosition = m_InverseXRotation ? new Vector3(relativeWeightPosition.x, relativeWeightPosition.y, -relativeWeightPosition.z) : relativeWeightPosition;
				relativeWeightPosition = m_InverseZRotation ? new Vector3(-relativeWeightPosition.x, relativeWeightPosition.y, relativeWeightPosition.z) : relativeWeightPosition;
				relativeWeightPosition = m_InverseYRotation ? new Vector3(relativeWeightPosition.x, -relativeWeightPosition.y, relativeWeightPosition.z) : relativeWeightPosition;

				acceleration += new Vector3(relativeWeightPosition.z * m_CharacterWeightX,
					relativeWeightPosition.y * m_CharacterWeightY,
					-relativeWeightPosition.x * m_CharacterWeightZ) * GameTime.fixedDeltaTime;
            }

            AccelerateTowardsBaseRotation();

            acceleration *= m_RotationFriction; //Apply friction to current force

			if (m_AllowXRotation) 
			{
				if (m_AllowXRotationDecel) 
				{
					float realXAngle = transform.rotation.eulerAngles.x;

					if (realXAngle > 90)
						realXAngle -= 360;
                    
					float decelerationLimit = m_MaxAccelerationX * (m_MaxAngleX - (Mathf.Abs (realXAngle))) / (m_MaxAngleX - m_DecelerationAngleX);

					float maxAccel = (acceleration.x < 0 && realXAngle < 0) || (acceleration.x > 0 && realXAngle > 0) ? (m_MaxAccelerationX < decelerationLimit ? m_MaxAccelerationX : decelerationLimit) : m_MaxAccelerationX;

					acceleration.x = Mathf.Clamp (acceleration.x, -maxAccel, maxAccel);
				}
				else
					acceleration.z = Mathf.Clamp (acceleration.x, -m_MaxAccelerationX, m_MaxAccelerationX);
			} 
			else 
			{
				acceleration.x = 0;
				transform.rotation = Quaternion.Euler (baseRotation.eulerAngles.x, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);
			}

			if (m_AllowZRotation) 
			{
				if (m_AllowZRotationDecel) 
				{
					float realZAngle = transform.rotation.eulerAngles.z;

					if (realZAngle > 90)
						realZAngle -= 360;

					float decelerationLimit = m_MaxAccelerationZ * (m_MaxAngleZ - (Mathf.Abs (realZAngle))) / (m_MaxAngleZ - m_DecelerationAngleZ);

					float maxAccel = (acceleration.x < 0 && realZAngle < 0) || (acceleration.x > 0 && realZAngle > 0) ? (m_MaxAccelerationZ < decelerationLimit ? m_MaxAccelerationZ : decelerationLimit) : m_MaxAccelerationZ;

					acceleration.z = Mathf.Clamp (acceleration.z, -maxAccel, maxAccel);
				}
				else
					acceleration.z = Mathf.Clamp (acceleration.z, -m_MaxAccelerationZ, m_MaxAccelerationZ);
			} 
			else 
			{
				acceleration.z = 0;
				transform.rotation = Quaternion.Euler (transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, baseRotation.eulerAngles.z);
			}

			if (m_AllowYRotation) 
			{
				if (m_AllowYRotationDecel) 
				{
					float minDecel = 0, maxDecel = 0;

					if (yRotation >= 0)
						minDecel = m_MaxAccelerationY * (m_MinAngleY - (Mathf.Abs (yRotation))) / (m_MinAngleY - m_DecelerationMinAngleY);
					else
						minDecel = m_MaxAccelerationY * (m_MinAngleY + (Mathf.Abs (yRotation))) / (m_MinAngleY - m_DecelerationMinAngleY);

					maxDecel = m_MaxAccelerationY * (m_MaxAngleY - (Mathf.Abs (yRotation))) / (m_MaxAngleY - m_DecelerationMaxAngleY);

					float minAccel = 0, maxAccel = 0;

					maxAccel = (acceleration.y < 0 && yRotation < 0) || (acceleration.y > 0 && yRotation > 0) ? (m_MaxAccelerationY < maxDecel ? m_MaxAccelerationY : maxDecel) : m_MaxAccelerationY;

					if ((yRotation > m_DecelerationMinAngleY && yRotation >= 0) 
						|| (yRotation < m_DecelerationMinAngleY && yRotation < 0))
						minAccel = (acceleration.y < 0 && yRotation < 0) || (acceleration.y > 0 && yRotation > 0) ? (m_MaxAccelerationY < minDecel ? m_MaxAccelerationY : minDecel) : m_MaxAccelerationY;
					else if ((yRotation > m_DecelerationMinAngleY && yRotation < 0) 
						|| (yRotation < m_DecelerationMinAngleY && yRotation >= 0))
						minAccel = m_MaxAccelerationY < minDecel ? m_MaxAccelerationY : minDecel;
					
					acceleration.y = Mathf.Clamp (acceleration.y, -minAccel, maxAccel);
				}
				else
					acceleration.y = Mathf.Clamp (acceleration.y, -m_MaxAccelerationY, m_MaxAccelerationY);
			} 
			else 
			{
				transform.rotation = Quaternion.Euler (transform.rotation.eulerAngles.x, baseRotation.eulerAngles.y, transform.rotation.eulerAngles.z);
				acceleration.y = 0;
			}

			//Apply acceleration to platform rotation
			if (m_UseParentAsRotationPivot)
				transform.parent.rotation *= Quaternion.Euler(acceleration * GameTime.fixedDeltaTime);
			else
				transform.rotation *= Quaternion.Euler(acceleration * GameTime.fixedDeltaTime);

			//Get platform rotation eulerangles
            Vector3 euler = transform.rotation.eulerAngles;

            //Max Angle Check
			if (m_AllowXRotation)
            {
                float realXAngle = euler.x;

                if (realXAngle > 90)
                    realXAngle -= 360;

				if(Mathf.Abs(realXAngle) > m_MaxAngleX)
                {
                    if (realXAngle < 0)
						euler.x = -m_MaxAngleX;
                    else
						euler.x = m_MaxAngleX;

                    transform.rotation = Quaternion.Euler(euler);
                    acceleration.x = 0;
                }
            }
			if (m_AllowZRotation)
            {
                float realZAngle = euler.z;

                if (realZAngle > 90)
                    realZAngle -= 360;

				if (Mathf.Abs(realZAngle) > m_MaxAngleZ)
                {
                    if (realZAngle < 0)
						euler.z = -m_MaxAngleZ;
                    else
						euler.z = m_MaxAngleZ;

                    transform.rotation = Quaternion.Euler(euler);
                    acceleration.z = 0;
                }
            }
			if (m_AllowYRotation)
			{
				if (!m_UnlimitedYRotation) 
				{
					float realYAngle = euler.y;

					if (realYAngle < 5 && yRotation > (355 + (360 * yRotationPointMultiplier))) 
					{
						yRotationPointMultiplier += 1;
					} 
					else if (realYAngle > 355 && yRotation < (5 + (360 * yRotationPointMultiplier)))
					{
						yRotationPointMultiplier -= 1;
					} 

					yRotation = euler.y + (360 * yRotationPointMultiplier);

					if ((yRotation > m_MaxAngleY) || (yRotation < m_MinAngleY)) 
					{
						if (yRotation < m_MinAngleY)
							euler.y = m_MinAngleY;
						else if (yRotation > m_MaxAngleY)
							euler.y = m_MaxAngleY;

						transform.rotation = Quaternion.Euler(euler);
						acceleration.y = 0;
					}
				}
			}

			//Set positions to new relative positions.
            for (int i = 0; i < weightList.Count; i++)
            {
				//Magic number to prevent characters from sinking into the floor for some reason.
                weightList[i].transform.position = transform.TransformPoint(relativePositions[i]) + new Vector3(0, 0.005f);
            }
        }

        /// <summary>
        /// Rotates towards its' base position and sets self to base position values if able to.
        /// </summary>
        private void AccelerateTowardsBaseRotation()
        {
            float realXAngle = transform.rotation.eulerAngles.x;
            float realZAngle = transform.rotation.eulerAngles.z;

            if (realXAngle > 90)
                realXAngle -= 360;

            if (realZAngle > 90)
                realZAngle -= 360;

			float relativeXWeightForce = m_EquilibriumForceX * (baseRotation.eulerAngles.x - realXAngle) * GameTime.fixedDeltaTime;
			float relativeZWeightForce = m_EquilibriumForceZ * (baseRotation.eulerAngles.z - realZAngle) * GameTime.fixedDeltaTime;
			float relativeYWeightForce = m_EquilibriumForceY * (baseRotation.eulerAngles.y - yRotation) * GameTime.fixedDeltaTime;

			relativeYWeightForce = (m_EquilibriumForceYLimit != 0) ? Mathf.Clamp(relativeYWeightForce, -m_EquilibriumForceYLimit, m_EquilibriumForceYLimit) : relativeYWeightForce;

            acceleration.x += relativeXWeightForce;
            acceleration.z += relativeZWeightForce;
			acceleration.y += relativeYWeightForce;
        }

		#endregion
    }
}