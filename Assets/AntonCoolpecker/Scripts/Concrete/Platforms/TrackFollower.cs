﻿using AntonCoolpecker.Abstract.Platforms;
using AntonCoolpecker.Concrete.Utils;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Platforms
{
	/// <summary>
	/// Track follower.
	/// </summary>
	[Serializable]
	public class TrackFollower
	{
		#region Variables

		[SerializeField] private AbstractTrackInstruction m_InitialInstructionObject;
		[SerializeField] private bool m_PreventReactionToPlayer = true; //Specifically intended for enemies.
		
		private Timer m_InstructionTimer = new Timer();
		private bool m_PlayerIsInPosition;
		private Queue<AbstractTrackInstruction> m_Instructions = new Queue<AbstractTrackInstruction>();

		#endregion

		#region Properties

		/// <summary>
		/// Gets the instruction timer.
		/// </summary>
		/// <value>The instruction timer.</value>
		public Timer instructionTimer { get { return m_InstructionTimer; } }

		/// <summary>
		/// Gets or sets a value indicating whether the player is in proper position.
		/// </summary>
		/// <value><c>true</c> if player is in position; otherwise, <c>false</c>.</value>
		public bool playerIsInPosition { get { return m_PlayerIsInPosition; } set { m_PlayerIsInPosition = value; } }

		/// <summary>
		/// Gets a value indicating whether this <see cref="AntonCoolpecker.Concrete.Platforms.TrackFollower"/> prevents
		/// any reactions to the player.
		/// </summary>
		/// <value><c>true</c> if prevent reaction to player; otherwise, <c>false</c>.</value>
		public bool preventReactionToPlayer { get { return m_PreventReactionToPlayer; } }
		
		/// <summary>
		/// 	Gets the instruction this follower is currently obeying.
		/// </summary>
		/// <value>The current instruction.</value>
		public AbstractTrackInstruction currentInstruction { get { return m_Instructions.Peek(); } }

		/// <summary>
		/// Gets a value indicating whether this <see cref="AntonCoolpecker.Concrete.Platforms.TrackFollower"/> is active.
		/// </summary>
		/// <value><c>true</c> if is active; otherwise, <c>false</c>.</value>
		public bool isActive { get { return !(m_Instructions.Count == 0); } }

		#endregion

		#region Public Functions
		
		/// <summary>
		/// 	Called when the parent is enabled.
		/// </summary>
		public void OnParentEnable()
		{
			if (m_InitialInstructionObject == null)
				return;
			
			Add(m_InitialInstructionObject.instructionSet);

			m_InstructionTimer = currentInstruction.timer;
			m_InstructionTimer.Reset();
		}
		
		/// <summary>
		/// 	Called every physics timestep.
		/// </summary>
		public void OnFixedUpdate()
		{
			if (!isActive)
				return;
			
			if (m_InstructionTimer.complete)
				NextInstruction();
			
			currentInstruction.UpdateFollower(this);
		}
		
		/// <summary>
		/// 	Adds the track instruction to this follower's instruction queue.
		/// </summary>
		/// <param name="instruction">Instruction.</param>
		public void Add(AbstractTrackInstruction instruction)
		{
			if (instruction != null)
				m_Instructions.Enqueue(instruction);
		}
		
		/// <summary>
		/// 	Adds multiple track instructions to this follower's instruction queue.
		/// </summary>
		/// <param name="instructions">Instructions.</param>
		public void Add(AbstractTrackInstruction[] instructions)
		{
			if (instructions != null)
				for (int index = 0; index < instructions.Length; index++)
					Add(instructions[index]);
		}

		#endregion

		#region Private Functions
		
		/// <summary>
		/// 	Causes this follower to obey the next instruction in its queue.
		/// </summary>
		private void NextInstruction()
		{
			if (!isActive)
				return;
			
			Add(m_Instructions.Dequeue().nextInstructionSet);

			m_InstructionTimer = currentInstruction.timer;
			m_InstructionTimer.Reset();
		}

		#endregion
	}
}