﻿using AntonCoolpecker.Abstract.Platforms;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Platforms
{
	/// <summary>
	/// "Arc Motion" track instruction.
	/// </summary>
	public class ArcMotionTrackInstruction : MoveTrackInstruction
	{
		#region Variables

		[SerializeField] private Vector3 m_ArcCenter;
		[SerializeField] private bool m_UseLargerMotionArc;

		#endregion

		#region Override Methods

		/// <summary>
		/// 	Gets the position that a track follower should be moving towards.
		/// </summary>
		/// <returns>The next position.</returns>
		/// <param name="follower">Follower.</param>
		public override Vector3 GetNextPosition(TrackFollower follower)
		{
			return interpolater.InterpolateArc(follower.instructionTimer.elapsedAsPercentage, 
				transform.position, m_ArcCenter,
				nextInstructionObject.transform.position, m_UseLargerMotionArc);
		}

		#endregion
	}
}