﻿using AntonCoolpecker.Abstract.Platforms;
using AntonCoolpecker.Utils.Time;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Platforms
{
	public class SinkRisePlatform : MovingPlatform
	{
		#region Variables

		[SerializeField] private Vector3 m_SinkVelocity;
		[SerializeField] private float m_RiseSpeed;

		private Vector3 m_StartPosition;

		#endregion

		#region Override Functions

		/// <summary>
		/// 	Called when the component is enabled.
		/// </summary>
		protected override void OnEnable()
		{
			base.OnEnable();

			m_StartPosition = transform.position;
		}

		/// <summary>
		/// 	Called every physics timestep.
		/// </summary>
		protected override void FixedUpdate()
		{
			base.FixedUpdate();

			Vector3 movement = isSupportingPlayer
								   ? m_SinkVelocity * GameTime.deltaTime
								   : Vector3.ClampMagnitude((m_StartPosition - transform.position).normalized * m_RiseSpeed * GameTime.deltaTime,
															Vector3.Distance(m_StartPosition, transform.position));
			
			HandleMovement(movement, Quaternion.identity);
		}

		#endregion
	}
}