﻿using AntonCoolpecker.Abstract.Platforms;
using AntonCoolpecker.Concrete.Utils;
using System.Collections.Generic;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Platforms
{
	/// <summary>
	/// Track follower platform.
	/// </summary>
	public class TrackFollowerPlatform : MovingPlatform
	{
		#region Variables

		[SerializeField] private TrackFollower m_TrackFollower;

		#endregion

		#region Override Functions

		/// <summary>
		/// Called when the component is enabled.
		/// </summary>
		protected override void OnEnable()
		{
			base.OnEnable();

			m_TrackFollower.OnParentEnable();
		}

		/// <summary>
		/// Called every physics timestep.
		/// </summary>
		protected override void FixedUpdate()
		{
			base.FixedUpdate();
			
			if (!m_TrackFollower.isActive)
				return;
			
			m_TrackFollower.OnFixedUpdate();
			m_TrackFollower.playerIsInPosition = isSupportingPlayer;
			
			Vector3 movement = m_TrackFollower.currentInstruction.GetNextPosition(m_TrackFollower) - transform.position;
			Quaternion nextRotation = m_TrackFollower.currentInstruction.GetNextRotation(m_TrackFollower);
			Quaternion rotation = Quaternion.FromToRotation(transform.rotation * Vector3.forward, nextRotation * Vector3.forward);
			
			HandleMovement(movement, rotation);
		}

		#endregion
	}
}