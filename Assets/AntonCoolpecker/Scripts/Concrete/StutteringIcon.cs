﻿using AntonCoolpecker.Concrete.Menus.HUD;
using System.Collections;
using UnityEngine;

/// <summary>
/// Animated stuttering icon - An animated icon with different stutter times(Frame timings)
/// for each frame(2 seconds for first frame, 0.3 seconds for the second frame, etc).
/// The stutter times will be looped when the end of the stutter time list has been reached.
/// </summary>
public class StutteringIcon : AnimatingIcon
{
	#region Variables

	[SerializeField] protected float[] stutterTimes; //The time moments when the animation should go to the next frame(s)
	protected int currentStutterTimeFrame; //Keeps track of the current stutter time frame(Per the stutter time list, not the sprite sheet).

	#endregion

	#region Override Functions

	/// <summary>
	/// Called when the icon frame is changed to the next frame in the spritesheet.
	/// </summary>
    protected override void ChangeFrame()
    {
        if (timeKeeper >= stutterTimes[currentStutterTimeFrame])
        {
            timeKeeper -= stutterTimes[currentStutterTimeFrame];
            currentColumn++;

            if (currentColumn >= spriteColumn)
            {
                currentColumn = 0;
                currentRow++;

                if (currentRow >= spriteRow)
                    currentRow = 0;
            }

            currentStutterTimeFrame++;

            if (currentStutterTimeFrame >= stutterTimes.Length)
            {
                currentStutterTimeFrame = 0;
            }
        }
    }

	#endregion
}