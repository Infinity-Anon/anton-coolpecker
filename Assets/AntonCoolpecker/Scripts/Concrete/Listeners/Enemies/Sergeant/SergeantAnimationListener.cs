﻿using AntonCoolpecker.Abstract.Listeners.Enemies;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Listeners.Enemies.Sergeant
{
	/// <summary>
	/// Sergeant Ant animation listener.
	/// </summary>
	[DisallowMultipleComponent]
    public class SergeantAnimationListener : AbstractEnemyAnimationListener {}
}