﻿using AntonCoolpecker.Abstract.Audio.Enemies;
using AntonCoolpecker.Abstract.Listeners.Enemies;
using AntonCoolpecker.Concrete.Audio.Enemies;
using System.Collections;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Listeners.Enemies.Sergeant
{
	/// <summary>
	/// Sergeant Ant audio listener.
	/// </summary>
    public class SergeantAudioListener : AbstractEnemyAudioListener
    {
		#region Variables

        [SerializeField] private SergeantSfx m_Sfx; //Basic serge ant SFX.

		#endregion

		#region Properties

        public override AbstractEnemySfx sfx { get { return m_Sfx; } }

		#endregion
    }
}