﻿using AntonCoolpecker.Abstract.Audio.Enemies;
using AntonCoolpecker.Abstract.Listeners.Enemies;
using AntonCoolpecker.Concrete.Audio.Enemies;
using System.Collections;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Listeners.Enemies.SoldierAnt
{
	/// <summary>
	/// Soldier Ant audio listener.
	/// </summary>
	public class SoldierAntAudioListener : AbstractEnemyAudioListener
	{
		#region Variables

		[SerializeField] private SoldierAntSfx m_Sfx; //Basic Soldier Ant SFX

		#endregion

		#region Properties 

		public override AbstractEnemySfx sfx { get { return m_Sfx; } }

		#endregion
	}
}