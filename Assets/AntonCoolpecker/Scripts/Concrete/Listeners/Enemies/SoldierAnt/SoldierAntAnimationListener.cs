﻿using AntonCoolpecker.Abstract.Listeners.Enemies;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Listeners.Enemies.SoldierAnt
{
	/// <summary>
	/// Soldier ant animation listener.
	/// </summary>
	[DisallowMultipleComponent]
	public class SoldierAntAnimationListener : AbstractEnemyAnimationListener {}
}