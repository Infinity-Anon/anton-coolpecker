﻿using AntonCoolpecker.Abstract.Player;
using AntonCoolpecker.API.Listeners.Collectables;
using AntonCoolpecker.Concrete.Collectables;
using AntonCoolpecker.Concrete.Player;
using AntonCoolpecker.Utils.Time;
using Hydra.HydraCommon.Utils.Audio;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Listeners.Collectables
{
	/// <summary>
	/// Collectable audio listener.
	/// </summary>
	public class CollectableAudioListener : MonoBehaviour, ICollectableListener
	{
		#region Variables

		[SerializeField] private CollectableSFX m_SFX;

		private static int s_SoundCounter = 0;
		private static float s_lastPlayTime = 0.0f;

		#endregion

		#region Properties

		/// <summary>
		/// 	Gets the sound effects.
		/// </summary>
		/// <value>The sound effects.</value>
		public CollectableSFX sfx { get { return m_SFX; } }

		#endregion

		#region Messages

		/// <summary>
		/// 	Called when the collectable is collected.
		/// </summary>
		public virtual void OnCollected()
		{
			if (GameTime.time - s_lastPlayTime > 1)
				s_SoundCounter = 0;

			int index = s_SoundCounter % m_SFX.collect.Length;
			int semitone = s_SoundCounter / m_SFX.collect.Length;
			float pitch = AudioSourceUtils.IncreaseSemitones(1.0f, semitone);

			AbstractPlayerController player = PlayerSwapper.instance.GetActivePlayer();

			AudioSource source = AudioSourcePool.PlayOneShot(player.gameObject, sfx.collect[index].audioClip,
															 sfx.collect[index].volumeScale, pitch);
			source.spatialBlend = 1.0f;

			s_lastPlayTime = GameTime.time;
			s_SoundCounter++;
		}

		#endregion
	}
}