﻿using AntonCoolpecker.Abstract.Audio.Player;
using AntonCoolpecker.Abstract.Listeners.Player;
using AntonCoolpecker.API.Listeners.Player.Anton;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Listeners.Player.Anton
{
	/// <summary>
	/// Anton audio listener.
	/// </summary>
	public class AntonAudioListener : AbstractPlayerAudioListener, IAntonListener
	{
		#region Variables

		[SerializeField] private AntonSFX m_SFX; //Basic Anton SFX

		#endregion

		#region Properties

		/// <summary>
		/// 	Gets the sound effects.
		/// </summary>
		/// <value>The sound effects.</value>
		public override AbstractPlayerSFX sfx { get { return m_SFX; } }

		#endregion
	}
}