﻿using AntonCoolpecker.Abstract.Listeners.Player;
using AntonCoolpecker.Abstract.Player.States;
using AntonCoolpecker.Abstract.StateMachine;
using AntonCoolpecker.API.Listeners.Player.Anton;
using AntonCoolpecker.Concrete.Player.Anton.States;
using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using AntonCoolpecker.Utils;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Listeners.Player.Anton
{
	/// <summary>
	/// Anton animation listener.
	/// </summary>
    public class AntonAnimationListener : AbstractPlayerAnimationListener, IAntonListener
    {
		#region Override Functions

		/// <summary>
		/// 	Called when the base updates.
		/// </summary>
        protected override void Update()
        {
            base.Update();

            if (player.stateMachine.activeState is SprintAntonState) 
			{
				SprintAntonState sprintState = player.stateMachine.activeState as SprintAntonState;
				animator.speed = sprintState.AnimationSpeedDefault + (sprintState.AnimationSpeedAddition * InputMapping.sprintInput.GetAxisRaw());
			}
        }

		/// <summary>
		/// Called when Anton changes state.
		/// </summary>
		/// <param name="changeInfo">Change info.</param>
		public override void OnStateChange(StateChangeInfo<AbstractPlayerState> changeInfo)
        {
            base.OnStateChange(changeInfo);

            if (changeInfo.previous is StandingAntonState && changeInfo.current is ClawSwipeAntonState)
            {
                animator.CrossFade("ClawSwipe", 0, 0, 0);
            }
            else if (changeInfo.previous is ClawSwipeAntonState && changeInfo.current is StandingAntonState)
            {
                ClawSwipeAntonState clawSwipe = changeInfo.previous as ClawSwipeAntonState;

                if (InputMapping.horizontalInput.GetAxisRaw() != 0 || InputMapping.verticalInput.GetAxisRaw() != 0)
                {
                    if (animator.GetFloat("InputMagnitude") <= 0.5)
                        animator.CrossFade("Walk", clawSwipe.RecoveryTime, 0);
                    else
                        animator.CrossFade("Run", clawSwipe.RecoveryTime, 0);
                }
                else
                    animator.CrossFade("Idle", clawSwipe.RecoveryTime, 0);
            }
            else if (changeInfo.previous is BounceJumpLandAntonSprintState && changeInfo.current is SprintAntonState)
                animator.Play("LandStill");
        }

		#endregion
    }
}