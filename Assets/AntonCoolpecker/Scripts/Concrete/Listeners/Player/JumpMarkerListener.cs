﻿using AntonCoolpecker.Abstract.Player.States;
using AntonCoolpecker.Abstract.StateMachine;
using AntonCoolpecker.API.Listeners.Player;
using AntonCoolpecker.Concrete.Utils;
using AntonCoolpecker.Utils;
using Hydra.HydraCommon.Abstract;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Listeners.Player
{
	/// <summary>
	/// Jump marker listener.
	/// </summary>
	public class JumpMarkerListener : HydraMonoBehaviour, IPlayerListener
	{
		#region Variables

		[SerializeField] private GameObject m_MarkerPrefab; //Reference to jump marker model.

		[SerializeField] private Color m_ColorA; //Color of jump marker 1
		[SerializeField] private Color m_ColorB; //Color of jump marker 2

		private GameObject m_MarkerInstanceA; //First jump marker instance - First spawned.
		private GameObject m_MarkerInstanceB; //Second jump marker instance

		private bool useA = true; //Whether the initial jump marker instance should use color A or B

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets a value indicating whether this
		/// <see cref="AntonCoolpecker.Concrete.Listeners.Player.JumpMarkerListener"/> enables the jump marker(s).
		/// </summary>
		/// <value><c>true</c> if jump marker(s) should be enabled; otherwise, <c>false</c>.</value>
		[Tweakable("Jump")]
		public bool enableJumpMarker
		{
			get { return this.enabled; }
			set
			{
				this.enabled = value;

				if (m_MarkerInstanceA != null)
					m_MarkerInstanceA.SetActive(value);
				
				if (m_MarkerInstanceB != null)
					m_MarkerInstanceB.SetActive(value);
			}
		}

		/// <summary>
		/// Gets or sets the marker prefab.
		/// </summary>
		/// <value>The marker prefab.</value>
		public GameObject markerPrefab { get { return m_MarkerPrefab; } set { m_MarkerPrefab = value; } }

		/// <summary>
		/// Gets or sets the color a for the first jump marker.
		/// </summary>
		/// <value>The color a.</value>
		public Color colorA { get { return m_ColorA; } set { m_ColorB = value; } }

		/// <summary>
		/// Gets or sets the color b for the second jump marker.
		/// </summary>
		/// <value>The color b.</value>
		public Color colorB { get { return m_ColorB; } set { m_ColorB = value; } }

		#endregion

		#region Messages

		/// <summary>
		/// Called when the player jumps.
		/// </summary>
		public void OnJump()
		{
			GameObject marker = GetNextMarker();
			marker.transform.position = this.transform.position;
		}

		/// <summary>
		/// Called when the state changes.
		/// </summary>
		/// <param name="changeInfo">Change info.</param>
		public void OnStateChange(StateChangeInfo<AbstractPlayerState> changeInfo) {}

		/// <summary>
		/// Called when the player is damaged.
		/// </summary>
		/// <param name="collision">The collision info.</param>
		public void OnDamage(CharacterLocomotorCollisionData collision) {}

		/// <summary>
		/// Called when we swap to the player.
		/// </summary>
		public void OnSwapEnter() {}

		/// <summary>
		/// 	Called when an animation event is fired.
		/// </summary>
		/// <param name="animationEvent">Animation event.</param>
		public virtual void OnAnimationEvent(AnimationEvent animationEvent) {}

		#endregion

		#region Private Functions/Methods

		/// <summary>
		/// Sets the color of a jump marker.
		/// </summary>
		/// <param name="marker">Marker.</param>
		/// <param name="color">Color.</param>
		private void SetColor(GameObject marker, Color color)
		{
			MeshRenderer[] meshes = marker.GetComponentsInChildren<MeshRenderer>();
			foreach (MeshRenderer mesh in meshes)
				mesh.material.color = color;
		}

		/// <summary>
		/// Get the next/previous jump marker.
		/// </summary>
		/// <returns>The next jump marker.</returns>
		private GameObject GetNextMarker()
		{
			GameObject marker;

			if (useA)
			{
				if (m_MarkerInstanceA == null)
				{
					m_MarkerInstanceA = Instantiate<GameObject>(m_MarkerPrefab);
					SetColor(m_MarkerInstanceA, m_ColorA);
				}
				marker = m_MarkerInstanceA;
			}
			else
			{
				if (m_MarkerInstanceB == null)
				{
					m_MarkerInstanceB = Instantiate<GameObject>(m_MarkerPrefab);
					SetColor(m_MarkerInstanceB, m_ColorB);
				}
				marker = m_MarkerInstanceB;
			}

			useA = !useA;
			return marker;
		}

		#endregion
	}
}