﻿using AntonCoolpecker.Abstract.Listeners.Player;
using AntonCoolpecker.Abstract.Player.States;
using AntonCoolpecker.Abstract.StateMachine;
using AntonCoolpecker.API.Listeners.Player.Coolpecker;
using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using AntonCoolpecker.Concrete.Player.Coolpecker.States;
using AntonCoolpecker.Utils;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Listeners.Player.Coolpecker
{
	/// <summary>
	/// Coolpecker animation listener.
	/// </summary>
	public class CoolpeckerAnimationListener : AbstractPlayerAnimationListener, ICoolpeckerListener
	{
        #region Messages

		/// <summary>
        /// Called every frame.
        /// </summary>
        protected override void Update()
        {
			if (player.stateMachine.activeState is SprintCoolpeckerState) 
			{
				SprintCoolpeckerState sprintState = player.stateMachine.activeState as SprintCoolpeckerState;
				animator.speed = sprintState.AnimationSpeedDefault + (sprintState.AnimationSpeedAddition * InputMapping.sprintInput.GetAxisRaw());
			}

            base.Update();
        }

        /// <summary>
        /// 	Called when Coolpecker changes state.
        /// </summary>
        /// <param name="changeInfo">Change info.</param>
        public override void OnStateChange(StateChangeInfo<AbstractPlayerState> changeInfo)
		{
			base.OnStateChange (changeInfo);

            if (changeInfo.current is DashingCoolpeckerState)
			{
				animator.Rebind ();
				animator.Play ("Dash", -1, 0f);
			}

			if (changeInfo.previous is DashingCoolpeckerState) 
			{
				animator.SetTrigger ("DashStop");
			}

			if (changeInfo.previous is GlidingCoolpeckerState && changeInfo.current is MidairCoolpeckerState) 
			{
				animator.SetBool ("GlideNoMore", true);
				animator.Play ("GlideForward", -1, 0f);
			}

			if ((changeInfo.previous is MidairCoolpeckerState|changeInfo.previous is MidairSprintCoolpeckerState)
				&& changeInfo.current is StandingCoolpeckerState) 
			{
				animator.SetBool ("GlideNoMore", false);
				animator.Play ("LandStill", -1, 0f);
			}
		}

		#endregion
	}
}