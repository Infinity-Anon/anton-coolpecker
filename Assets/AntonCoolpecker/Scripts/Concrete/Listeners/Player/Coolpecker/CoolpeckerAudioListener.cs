﻿using AntonCoolpecker.Abstract.Audio.Player;
using AntonCoolpecker.Abstract.Listeners.Player;
using AntonCoolpecker.API.Listeners.Player.Coolpecker;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Listeners.Player.Coolpecker
{
	/// <summary>
	/// Coolpecker audio listener.
	/// </summary>
	public class CoolpeckerAudioListener : AbstractPlayerAudioListener, ICoolpeckerListener
	{
		#region Variables

		[SerializeField] private CoolpeckerSFX m_SFX; //Basic Coolpecker SFX

		#endregion

		#region Properties

		/// <summary>
		/// 	Gets the sound effects.
		/// </summary>
		/// <value>The sound effects.</value>
		public override AbstractPlayerSFX sfx { get { return m_SFX; } }

		#endregion
	}
}