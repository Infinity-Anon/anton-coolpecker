﻿using AntonCoolpecker.Concrete.Utils;
using AntonCoolpecker.Utils.Time;
using Hydra.HydraCommon.Abstract;
using UnityEngine;

namespace AntonCoolpecker.Concrete.HitBoxes
{
	/// <summary>
	/// Projectile class - Primarily intended for enemies shooting straight bullets/projectiles.
	/// </summary>
	public class Projectile : HydraMonoBehaviour
	{
		#region Variables

        [SerializeField] private float m_Speed;
		[SerializeField] private Timer m_DeletionTimer;
		[SerializeField] private bool m_UseGravity;
		[SerializeField] private Timer m_GravityTimer;

		#endregion

		#region Override Functions

		/// <summary>
		/// Called when the component is enabled.
		/// </summary>
		protected override void OnEnable()
		{
			base.OnEnable();

			m_DeletionTimer.Reset();
			m_GravityTimer.Reset();
		}

		/// <summary>
		/// Called once every frame.
		/// </summary>
        protected override void Update()
        {
            base.Update();

            if (m_DeletionTimer.complete)
                Destroy(gameObject);
			
            if (m_UseGravity && m_GravityTimer.complete)
                GetComponent<Rigidbody>().useGravity = true;
        }

		/// <summary>
		/// OnTriggerEnter is called when the Collider other enters the trigger.
		/// </summary>
		/// <param name="other">Other.</param>
		protected override void OnTriggerEnter(Collider other)
		{
			base.OnTriggerEnter(other);

			//TODO: MAKE SURE THE BULLETS CAN HIT OTHER ENEMIES TOO, MAYBE? PERHAPS THE BULLETS SHOULD BE CHILDREN TO THE BULLET'S ORIGIN ENEMY
			// who the shit writes their comments in all caps
			// oh apparently it's brokenideascompany
			// fuk u broken use your indoor voice
			// FUCK YOU I WON'T DO WHAT YOU TELL ME

			if (other.tag != "Enemy")
				Destroy(gameObject);
		}

		/// <summary>
		/// Called every physics timestep.
		/// </summary>
        protected override void FixedUpdate()
        {
            base.FixedUpdate();
            transform.position += transform.forward * m_Speed * GameTime.fixedDeltaTime;
        }

		#endregion
	}
}