using AntonCoolpecker.Concrete.Utils;
using Hydra.HydraCommon.Abstract;
using UnityEngine;

namespace AntonCoolpecker.Concrete.HitBoxes
{
	/// <summary>
	/// 	Hit box.
	/// </summary>
	[RequireComponent(typeof(Collider))]
	[DisallowMultipleComponent]
	public class HitBox : HydraMonoBehaviour
	{
		#region Messages

		/// <summary>
		/// 	OnTriggerEnter is called when the Collider other enters the trigger.
		/// </summary>
		/// <param name="other">Other.</param>
		protected override void OnTriggerEnter(Collider other)
		{
			base.OnTriggerEnter(other);
		}

		/// <summary>
		/// Raises the character locomotor collision event.
		/// </summary>
		/// <param name="collision">Collision.</param>
		protected virtual void OnCharacterLocomotorCollision(CharacterLocomotorCollisionData collision) {}

		#endregion
	}
}