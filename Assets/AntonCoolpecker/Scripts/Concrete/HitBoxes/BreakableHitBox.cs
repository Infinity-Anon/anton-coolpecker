﻿using AntonCoolpecker.API.Damageable;
using AntonCoolpecker.Concrete.Utils;
using UnityEngine;

namespace AntonCoolpecker.Concrete.HitBoxes
{
	/// <summary>
	/// Breakable hit box.
	/// </summary>
	[RequireComponent(typeof(Collider))]
	[DisallowMultipleComponent]
	public class BreakableHitBox : HitBox, IDamageable
	{
		#region Virtual Functions

		/// <summary>
		/// Called when the Damageable gets damaged.
		/// </summary>
		/// <param name="damage">Amount of damage to apply.</param>
		/// <param name="collision">Collision info.</param>
		public virtual void Damage(int damage, CharacterLocomotorCollisionData collision)
		{
			this.gameObject.SetActive(false);
		}

		#endregion
	}
}