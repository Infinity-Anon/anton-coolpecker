﻿using AntonCoolpecker.Abstract.Enemies.States;
using AntonCoolpecker.Concrete.HitBoxes;
using AntonCoolpecker.Concrete.Utils;
using AntonCoolpecker.Utils.Time;
using Hydra.HydraCommon.Abstract;
using UnityEngine;

namespace AntonCoolpecker.Concrete.HitBoxes
{
	/// <summary>
	/// Homing projectile - Primarily intended for enemies shooting homing bullets/projectiles.
	/// </summary>
    public class HomingProjectile : Projectile
    {
		#region Variables

        [SerializeField] private float m_BulletRotationSpeed = 100f;
		[SerializeField] private Vector3 m_TargetOffset = new Vector3(0, 1.1f, 0);

        private static GameObject m_CachedPlayer;

		#endregion

		#region Functions/Methods

        /// <summary>
		/// 	Gets the player.
		/// </summary>
		/// <value>The player.</value>
		public GameObject player
        {
            get
            {
                return (m_CachedPlayer && m_CachedPlayer.activeInHierarchy)
                           ? m_CachedPlayer
                           : m_CachedPlayer = GameObject.FindWithTag("Player");
            }
        }

		/// <summary>
		/// Called every physics timestep.
		/// </summary>
        protected override void FixedUpdate()
        {
			Quaternion newRotation = Quaternion.LookRotation(((this.player.transform.position + m_TargetOffset) - transform.position));
			transform.rotation = Quaternion.RotateTowards(transform.rotation, newRotation, m_BulletRotationSpeed * GameTime.fixedDeltaTime);

            base.FixedUpdate();
        }

		#endregion
    }
}