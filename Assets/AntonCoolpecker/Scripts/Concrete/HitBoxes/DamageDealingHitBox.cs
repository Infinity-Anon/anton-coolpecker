﻿using AntonCoolpecker.API.Damageable;
using AntonCoolpecker.Concrete.Utils;
using System.Collections.Generic;
using UnityEngine;

namespace AntonCoolpecker.Concrete.HitBoxes
{
	/// <summary>
	/// Hitbox for HazardTest object that damages the player/enemy upon touching it
	/// </summary>
	[RequireComponent(typeof(Collider))]
	[DisallowMultipleComponent]
	public class DamageDealingHitBox : HitBox
	{
		#region Variables

		[SerializeField] private bool m_IgnorePlayer;
		[SerializeField] private bool m_IgnoreEnemy;
		[SerializeField] private Vector3 m_Origin;
		[SerializeField] private int m_Damage = 1;

		#endregion

		#region Properties

		/// <summary>
		/// 		True if player collision should be ignored, false if not
		/// </summary>
		public bool ignorePlayer { get { return m_IgnorePlayer; } set { m_IgnorePlayer = value; } }

		/// <summary>
		/// 		True if enemy collision should be ignored, false if not
		/// </summary>
		public bool ignoreEnemy { get { return m_IgnorePlayer; } set { m_IgnorePlayer = value; } }

		#endregion

		#region Messages

		/// <summary>
		/// Raises the character locomotor collision event.
		/// </summary>
		/// <param name="collision">Collision.</param>
		protected override void OnCharacterLocomotorCollision(CharacterLocomotorCollisionData collision)
		{
			if (m_IgnorePlayer)
			{
				if (collision.Controller.gameObject.tag == "Player")
					return;
			}

			if (m_IgnoreEnemy)
			{
				if (collision.Controller.gameObject.tag == "Enemy")
					return;
			}

			base.OnCharacterLocomotorCollision(collision);

			List<IDamageable> damageables = new List<IDamageable>(collision.Controller.gameObject.GetComponents<IDamageable>());
			foreach (IDamageable damageable in damageables)
				damageable.Damage(m_Damage, collision);
		}

		/// <summary>
		/// OnTriggerEnter is called when the Collider other enters the trigger.
		/// </summary>
		/// <param name="other">Other.</param>
		protected override void OnTriggerEnter(Collider other)
		{
			if (m_IgnorePlayer)
			{
				if (other.gameObject.tag == "Player")
					return;
			}

			if (m_IgnoreEnemy)
			{
				if (other.gameObject.tag == "Enemy")
					return;
			}

			CharacterLocomotorCollisionData collision = new CharacterLocomotorCollisionData(
				null,
				this.gameObject.transform.position + m_Origin
			);
			base.OnTriggerEnter(other);

			List<IDamageable> damageables = new List<IDamageable>(other.gameObject.GetComponents<IDamageable>());
			foreach (IDamageable damageable in damageables)
				damageable.Damage(m_Damage, collision);
		}

		#endregion
	}
}