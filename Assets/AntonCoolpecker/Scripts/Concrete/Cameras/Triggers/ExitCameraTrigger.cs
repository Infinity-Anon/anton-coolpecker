﻿using AntonCoolpecker.Abstract.Cameras.Triggers;

namespace AntonCoolpecker.Concrete.Cameras.Triggers
{
	/// <summary>
	/// 	Exit camera trigger is used in conjunction with an EnterCameraTrigger
	/// 	to end the current camera state.
	/// </summary>
	public class ExitCameraTrigger : AbstractCameraTrigger {}
}
