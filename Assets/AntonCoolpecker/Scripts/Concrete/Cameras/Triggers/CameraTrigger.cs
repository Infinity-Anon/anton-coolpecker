﻿using AntonCoolpecker.Abstract.Cameras.Triggers;

namespace AntonCoolpecker.Concrete.Cameras.Triggers
{
	/// <summary>
	/// 	Camera trigger lets us set up sections where the camera mode changes
	/// 	when the player enters an area.
	/// </summary>
	public class CameraTrigger : AbstractTemplateCameraTrigger {}
}
