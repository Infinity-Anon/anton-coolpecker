﻿using UnityEngine;
using System.Collections;
using AntonCoolpecker.Abstract.Cameras.States;
namespace AntonCoolpecker.Concrete.Cameras.Triggers
{
    public class CutsceenCameraTrigger : MonoBehaviour
    {
        [SerializeField]
        CameraPath m_Path;
       protected void SetCamera()
       {
            Camera.main.GetComponent<CameraController>().SetPath = m_Path;
       }

        protected void SetPlayer(GameObject player)
        {
            player.GetComponent<Abstract.Player.AbstractPlayerController>().EnterCutscene(m_Path.ID,m_Path.Level);
        }
        private void OnTriggerEnter(Collider other)
        {
            if (other.GetComponent<Abstract.Player.AbstractPlayerController>() != null)
            {
                SetCamera();
                SetPlayer(other.gameObject);
            }
        }
    }
}
