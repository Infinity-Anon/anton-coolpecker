﻿using AntonCoolpecker.Abstract.Cameras.Triggers;
using AntonCoolpecker.Abstract.Player;
using Hydra.HydraCommon.EventArguments;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Cameras.Triggers
{
	/// <summary>
	/// 	Enter camera trigger sets the current camera state until the player
	/// 	enters an ExitCameraTrigger.
	/// </summary>
	public class EnterCameraTrigger : AbstractTemplateCameraTrigger
	{
		[SerializeField] private ExitCameraTrigger[] m_Exits;

		#region Messages

		/// <summary>
		/// 	Called when the component is enabled.
		/// </summary>
		protected override void OnEnable()
		{
			base.OnEnable();

			Subscribe(m_Exits);
		}

		/// <summary>
		/// 	Called when the component is disabled.
		/// </summary>
		protected override void OnDisable()
		{
			base.OnDisable();

			Unsubscribe(m_Exits);
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// 	Called when the player enters the trigger.
		/// </summary>
		/// <param name="player">Player.</param>
		protected override void OnEnter(AbstractPlayerController player)
		{
			if (m_Exits == null || m_Exits.Length == 0)
			{
				Debug.LogWarningFormat("EnterCameraTrigger {0} has no exits.", name);
				return;
			}

			base.OnEnter(player);
		}

		/// <summary>
		/// 	Called when the player exits the trigger.
		/// </summary>
		/// <param name="player">Player.</param>
		protected override void OnExit(AbstractPlayerController player)
		{
			// We handle exit ourselves.
		}

		/// <summary>
		/// 	Called when the player reaches an exit.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
		private void OnExitReached(object sender, EventArg<AbstractPlayerController> e)
		{
			// Call the default exit mechanism.
			base.OnExit(e.data);
		}

		/// <summary>
		/// 	Subscribe to the exit events.
		/// </summary>
		/// <param name="exits">Exits.</param>
		private void Subscribe(ExitCameraTrigger[] exits)
		{
			if (exits == null)
				return;

			for (int index = 0; index < exits.Length; index++)
				Subscribe(exits[index]);
		}

		/// <summary>
		/// 	Unsubscribe from the exit events.
		/// </summary>
		/// <param name="exits">Exits.</param>
		private void Unsubscribe(ExitCameraTrigger[] exits)
		{
			if (exits == null)
				return;

			for (int index = 0; index < exits.Length; index++)
				Unsubscribe(exits[index]);
		}

		/// <summary>
		/// 	Subscribe to the exit events.
		/// </summary>
		/// <param name="exit">Exit.</param>
		private void Subscribe(ExitCameraTrigger exit)
		{
			if (exit == null)
				return;

			exit.onEnter += OnExitReached;
		}

		/// <summary>
		/// 	Unsubscribe from the exit events.
		/// </summary>
		/// <param name="exit">Exit.</param>
		private void Unsubscribe(ExitCameraTrigger exit)
		{
			if (exit == null)
				return;

			exit.onEnter -= OnExitReached;
		}

		#endregion
	}
}
