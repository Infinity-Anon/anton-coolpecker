using AntonCoolpecker.Abstract.Cameras.States;
using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using AntonCoolpecker.Utils;
using AntonCoolpecker.Utils.Time;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Cameras.States
{
	/// <summary>
	/// 	Underwater camera follows the rotation of the target or vice-versa
	/// </summary>
	public class UnderwaterCameraState : AbstractCameraState
	{
		#region Variables

		// Speed of the input movement(If underwater movement is input-based instead of camera-based)
		[SerializeField] private Vector2 m_MovInputSpeed = new Vector2(12.0f, 12.0f);

		// Use camera-based or input-based underwater movement
		[Tweakable("UnderwaterCamera")] [SerializeField] private bool m_UNDWATER_INPUT_FALSE_CAMERA_TRUE;

		// Initial rotation to underwater state is based either on camera or player rotation(UNUSED: PLAYER ROTATION IS CURRENTLY USED)
		//[Tweakable("UnderwaterCamera")] [SerializeField] private bool m_ROTTOPLAYER_FALSE_ROTTOCAMERA_TRUE;

		//Use initial underwater rotation or not when transition to underwater is finished
		[Tweakable("UnderwaterCamera")] private bool m_InitialRotation = true;

		private float input_X; //Stores the player's input for the camera's X rotation orbit
		private float input_Y; //Stores the player's input for the camera's Y rotation orbit

		private CameraController cameraController; //Reference to the camera controller instance
		private Transform transform; //Reference to the camera controller's transform

		private float m_Distance; //Stores the camera controller's distance to the player
		private float m_X; //Stores the camera controller's X orbit value
		private float m_Y; //Stores the camera controller's Y orbit value
		private Vector2 m_Speed = Vector2.zero; //Stores the camera controller's speed

		#endregion

		#region Properties

		/// <summary>
		/// 	Gets or sets the control method for the underwater camera.
		/// </summary>
		/// <value><c>true</c> if orbit enabled; otherwise, <c>false</c>.</value>
		[Tweakable("UnderwaterCamera")] public bool undWatInputFalseCameraTrue
		{
			get { return m_UNDWATER_INPUT_FALSE_CAMERA_TRUE; }
			set { m_UNDWATER_INPUT_FALSE_CAMERA_TRUE = value; }
		}

		/// <summary>
		/// Gets or sets the horizontal input speed of the input-based underwater camera.
		/// </summary>
		/// <value>Horizontal input speed.</value>
		[Tweakable("UnderwaterCamera")] public float movInputSpeedX { get { return m_MovInputSpeed.x; } set { m_MovInputSpeed.x = value; } }

		/// <summary>
		/// Gets or sets the vertical input speed of the input-based underwater camera.
		/// </summary>
		/// <value>Vertical input speed.</value>
		[Tweakable("UnderwaterCamera")] public float movInputSpeedY { get { return m_MovInputSpeed.y; } set { m_MovInputSpeed.y = value; } }

		#endregion

		#region Override Functions

		/// <summary>
		/// Orient the camera.
		/// </summary>
		/// <param name="parent">Parent.</param>
		protected override void Orient(MonoBehaviour parent)
		{
			cameraController = GetCameraController(parent);
			transform = cameraController.transform;

			if (m_Speed == Vector2.zero)
				MakeStuff();

			CapsuleCollider collidus = cameraController.target.GetComponent<CapsuleCollider>();

			if (collidus != null && collidus.enabled == true)
			{
				if (m_InitialRotation) 
				{
					if (CameraController.HasUserInput() || InputMapping.horizontalInput.GetAxisRaw() != 0 || InputMapping.verticalInput.GetAxisRaw() != 0) 
					{
						SetOrbit();
						StopInitialRotation();

						return;
					}
						
					Transform target = cameraController.target.transform;

					//collidus.transform.rotation = Quaternion.Slerp(collidus.transform.rotation, transform.rotation, 3f * Time.deltaTime);
					collidus.transform.rotation = Quaternion.Slerp (collidus.transform.rotation,
						Quaternion.Euler (0, target.rotation.eulerAngles.y,
							0.0f), 5f * Time.deltaTime);

					Quaternion targetRot = Quaternion.Euler (target.rotation.eulerAngles.x, target.rotation.eulerAngles.y, 0.0f);
					transform.rotation = Quaternion.Slerp (transform.rotation, targetRot, 5f * GameTime.deltaTime);
					transform.position = GetPositionFromRotation (parent);

					return;
				}

				if (m_UNDWATER_INPUT_FALSE_CAMERA_TRUE) //Target follows camera rotation(through camera input)
				{
					collidus.transform.rotation = Quaternion.Slerp(collidus.transform.rotation, transform.rotation, 3f * Time.deltaTime);
				}

				if (!m_UNDWATER_INPUT_FALSE_CAMERA_TRUE) //Camera follows target rotation(through movement input)
				{
					transform.rotation = Quaternion.Slerp(transform.rotation, collidus.transform.rotation, 4.3f * Time.deltaTime);
				}
			}

			if (collidus == null)
				Debug.LogError("NO PLAYER COLLIDER FOUND - MAKE SURE UNDERWATER CAMERA MODE IS ENABLED CORRECTLY");

			if (m_UNDWATER_INPUT_FALSE_CAMERA_TRUE) //If using camera-based underwater camera system
			{
				if (cameraController.cameraSmoothing) 
				{
					float orbitX = m_X;
					float orbitY = m_Y;

					if (transform.rotation.eulerAngles.z < 181f && transform.rotation.eulerAngles.z > 179f)
						orbitX -= InputMapping.horizontalCameraInput.GetAxis() * m_Speed.x * (cameraController.invertedX ? -1 : 1) * m_Distance * Time.fixedDeltaTime;
					else
						orbitX += InputMapping.horizontalCameraInput.GetAxis() * m_Speed.x * (cameraController.invertedX ? -1 : 1) * m_Distance * Time.fixedDeltaTime;

					orbitY -= InputMapping.verticalCameraInput.GetAxis() * m_Speed.y * (cameraController.invertedY ? -1 : 1) * Time.fixedDeltaTime;

					m_X = Mathf.Lerp (m_X, orbitX, cameraController.smoothXLerp * Time.deltaTime);
					m_Y = Mathf.Lerp (m_Y, orbitY, cameraController.smoothYLerp * Time.deltaTime);
				} 
				else 
				{
					if (transform.rotation.eulerAngles.z < 181f && transform.rotation.eulerAngles.z > 179f)
						m_X -= InputMapping.horizontalCameraInput.GetAxis() * m_Speed.x * (cameraController.invertedX ? -1 : 1) * m_Distance * Time.fixedDeltaTime;
					else
						m_X += InputMapping.horizontalCameraInput.GetAxis() * m_Speed.x * (cameraController.invertedX ? -1 : 1) * m_Distance * Time.fixedDeltaTime;

					m_Y -= InputMapping.verticalCameraInput.GetAxis() * m_Speed.y * (cameraController.invertedY ? -1 : 1) * Time.fixedDeltaTime;
				}

				transform.rotation = Quaternion.Euler(m_Y, m_X, transform.rotation.z);
			}

			if (!m_UNDWATER_INPUT_FALSE_CAMERA_TRUE) //If using input-based underwater camera system
			{ 
				if (transform.rotation.eulerAngles.z < 190f && transform.rotation.eulerAngles.z > 170f) 
				{
					input_X -= InputMapping.horizontalInput.GetAxis () * m_MovInputSpeed.x * m_Distance * Time.deltaTime;
					//m_X -= InputMapping.horizontalCameraInput.GetAxis () * m_Speed.x * m_Distance * Time.fixedDeltaTime;

					m_X -= InputMapping.horizontalCameraInput.GetAxis () * m_Speed.x * m_Distance * Time.fixedDeltaTime;
				} 
				else 
				{
					m_X += InputMapping.horizontalCameraInput.GetAxis () * m_Speed.x * m_Distance * Time.fixedDeltaTime;

					//m_X += InputMapping.horizontalInput.GetAxis () * MovInputSpeed.x * m_Distance * Time.deltaTime;
					input_X += InputMapping.horizontalInput.GetAxis () * m_MovInputSpeed.x * m_Distance * Time.deltaTime;
				}

				input_Y -= InputMapping.verticalInput.GetAxis() * m_MovInputSpeed.y * cameraController.distance * Time.deltaTime;

				cameraController.target.transform.rotation = Quaternion.Euler(input_Y, input_X,
																			  cameraController.target.transform.rotation.z);
			}

			transform.position = GetPositionFromRotation(parent);
		}

		#endregion

		#region Private Functions

		/// <summary>
		/// Initializes CameraController-related variables for this camera state.
		/// </summary>
		private void MakeStuff()
		{
			m_Distance = cameraController.distance;
			SetOrbit ();
			m_Speed = cameraController.speed;
		}

		/// <summary>
		/// Sets the camera orbit settings.
		/// </summary>
		private void SetOrbit()
		{
			m_X = cameraController.orbitX;
			m_Y = cameraController.orbitY;
			input_X = cameraController.orbitX;
			input_Y = cameraController.orbitY;
		}

		/// <summary>
		/// Called when the initial rotation is done.
		/// </summary>
		private void StopInitialRotation()
		{
			m_InitialRotation = false;
		}

		/// <summary>
		/// Called when the initial rotation needs to be started.
		/// </summary>
		public void StartInitialRotation()
		{
			m_InitialRotation = true;
		}

		#endregion
	}
}