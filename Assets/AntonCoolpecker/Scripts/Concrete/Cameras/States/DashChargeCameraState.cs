﻿using AntonCoolpecker.Abstract.Cameras.States;
using AntonCoolpecker.Utils;
using AntonCoolpecker.Utils.Time;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Cameras.States
{
    /// <summary>
    /// 	Camera follows the rotation of the target.
    /// </summary>
    public class DashChargeCameraState : AbstractCameraState
    {
		#region Variables

		//Determines how smooth/sharp the camera should track the rotation of the dash charge
        [Tweakable("DashChargeCamera")] [SerializeField] private float trackingMultiplier = 2f;
		//Determines the maximum angle difference before the center of the camera's vertical rotation,
		//A.K.A how much you can rotate the camera upwards/downwards the center of the camera's vertical rotation.
        [Tweakable("DashChargeCamera")] [SerializeField] private float maxAngleDifference = 5f;

		#endregion

		#region Override Functions

        /// <summary>
        /// 	Orient the camera.
        /// </summary>
        /// <param name="parent">Parent.</param>
        protected override void Orient(MonoBehaviour parent)
        {
            CameraController cameraController = GetCameraController(parent);

            float yaw = cameraController.target.transform.eulerAngles.y;
            
            if (cameraController.orbitY > 180 && cameraController.orbitY < 360 - maxAngleDifference)
                cameraController.orbitY = 360 - maxAngleDifference;
            else if (cameraController.orbitY < 180 && cameraController.orbitY > maxAngleDifference)
                cameraController.orbitY = maxAngleDifference;

            Quaternion current = parent.transform.rotation;
            Quaternion target = Quaternion.Euler(cameraController.orbitY, yaw, 0.0f);

            parent.transform.rotation = Quaternion.Slerp(current, target, GameTime.fixedDeltaTime*trackingMultiplier);
            parent.transform.position = GetPositionFromRotation(parent);
        }

		#endregion
    }
}