﻿using AntonCoolpecker.Abstract.Cameras.States;
using AntonCoolpecker.Abstract.StateMachine;
using Hydra.HydraCommon.Extensions;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Cameras.States
{
	/// <summary>
	/// 	CameraStateMachine is the state machine for the CameraController.
	/// </summary>
	[Serializable]
	public class CameraStateMachine : AbstractStateMachine<AbstractCameraState>
	{
		#region Variables

		[SerializeField] private AbstractCameraState[] m_States;
		[SerializeField] private AbstractCameraState m_InitialState;
        [SerializeField] private AbstractCameraState m_CutsceneState;

        private static readonly Dictionary<CameraController.Mode, Type> s_ModeMap = new Dictionary
			<CameraController.Mode, Type>
		{
			{CameraController.Mode.Active, typeof(ActiveCameraState)},
			{CameraController.Mode.Passive, typeof(PassiveCameraState)},
			{CameraController.Mode.Fixed, typeof(FixedCameraState)},
			{CameraController.Mode.Underwater, typeof(UnderwaterCameraState)},
			{CameraController.Mode.Static, typeof(StaticCameraState)},
			{CameraController.Mode.Free, typeof(FreeCameraState)},
			{CameraController.Mode.Auto, typeof(AutoCameraState)},
            {CameraController.Mode.Glide, typeof(GlideCameraState)},
            {CameraController.Mode.DashCharge, typeof(DashChargeCameraState)},
            {CameraController.Mode.Swing, typeof(SwingCameraState)},
            {CameraController.Mode.Dash, typeof(DashCameraState)},
            {CameraController.Mode.Cutscene,typeof(CutsceneCameraState) }
        };

		#endregion

		#region Properties

		/// <summary>
		/// 	Gets the initial state.
		/// </summary>
		/// <value>The initial state.</value>
		public override AbstractCameraState initialState { get { return m_InitialState; } }

		#endregion

		#region Methods

		/// <summary>
		/// 	Sets the active state.
		/// </summary>
		/// <param name="parent">Parent.</param>
		/// <param name="mode">Mode.</param>
		public AbstractCameraState SetActiveState(CameraController.Mode mode, MonoBehaviour parent)
		{
			AbstractCameraState state = GetStateByMode(mode);
			return SetActiveState(state, parent);
		}

		/// <summary>
		/// 	Gets the mode.
		/// </summary>
		/// <returns>The mode.</returns>
		public CameraController.Mode GetMode()
		{
			Type type = activeState.GetType();
			return s_ModeMap.GetKeyForValue(type);
		}

		/// <summary>
		/// 	Gets the state by mode.
		/// </summary>
		/// <returns>The state.</returns>
		/// <param name="mode">Mode.</param>
		public AbstractCameraState GetStateByMode(CameraController.Mode mode)
		{
			Type stateType = GetStateTypeByMode(mode);

			for (int index = 0; index < m_States.Length; index++)
			{
				AbstractCameraState state = m_States[index];
				if (state.GetType() == stateType)
					return state;
			}

			throw new ArgumentOutOfRangeException();
		}

		/// <summary>
		/// 	Gets the state type by mode.
		/// </summary>
		/// <returns>The state type by mode.</returns>
		/// <param name="mode">Mode.</param>
		public static Type GetStateTypeByMode(CameraController.Mode mode)
		{
			return s_ModeMap[mode];
		}

		#endregion
	}
}