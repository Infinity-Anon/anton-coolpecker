﻿using AntonCoolpecker.Abstract.Cameras.States;
using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using AntonCoolpecker.Utils.Time;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Cameras.States
{
	/// <summary>
	/// 	Camera is rotated based on mouse input.
	/// </summary>
	public class PassiveCameraState : AbstractCameraState
	{
		#region Variables

		//Snap camera back immediately if true, snap back camera smoothly when button/key is pressed down if false
		[SerializeField] private bool m_CameraSnapHoldTrueToggleFalse;
		//Should the camera snap back button be continiously held for the snap back?
		[SerializeField] private bool m_CameraRotationHold;
		//Is the camera smoothly transitioning back to it's original position?
		[SerializeField] private bool m_ToggleBack;

		#endregion

		#region Override Functions

		/// <summary>
		/// 	Orient the camera.
		/// </summary>
		/// <param name="parent">Parent.</param>
		protected override void Orient(MonoBehaviour parent)
		{
			CameraController cameraController = GetCameraController(parent);

			Transform transform = cameraController.transform;
			Transform target = cameraController.target.transform;

			Quaternion quarter = Quaternion.Euler(target.rotation.eulerAngles.x, target.rotation.eulerAngles.y, 0.0f);

			if (!CameraController.HasUserInput())
			{
				if (m_CameraSnapHoldTrueToggleFalse)
				{
					if (m_CameraRotationHold && InputMapping.snapCamera.GetButton())
						transform.rotation = Quaternion.Slerp(transform.rotation, quarter, 5f * GameTime.deltaTime);

					if (!m_CameraRotationHold && InputMapping.snapCamera.GetButtonDown())
					{
						if (m_ToggleBack != true)
							m_ToggleBack = true;
					}
				}

				if (!m_CameraSnapHoldTrueToggleFalse)
				{
					if (!m_CameraRotationHold && InputMapping.snapCamera.GetButtonDown())
						transform.rotation = quarter;

					if (m_CameraRotationHold && InputMapping.snapCamera.GetButton())
						transform.rotation = Quaternion.Slerp(transform.rotation, quarter, 5f * GameTime.deltaTime);
				}

				if (m_ToggleBack == true)
					transform.rotation = Quaternion.Slerp(transform.rotation, quarter, 5f * GameTime.deltaTime);
			}

			else
			{
				transform.rotation = Quaternion.Euler(cameraController.orbitY, cameraController.orbitX, 0);

				if (m_ToggleBack == true)
					m_ToggleBack = false;
			}

			transform.position = GetPositionFromRotation(parent);
		}

		#endregion
	}
}