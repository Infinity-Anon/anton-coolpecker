﻿using AntonCoolpecker.Abstract.Cameras.States;

namespace AntonCoolpecker.Concrete.Cameras.States
{
	/// <summary>
	/// 	Camera does not move.
	/// </summary>
	public class StaticCameraState : AbstractTemplateCameraState {}
}
