﻿using AntonCoolpecker.Abstract.Cameras.States;
using AntonCoolpecker.Abstract.Player;
using AntonCoolpecker.Concrete.Player.Anton;
using AntonCoolpecker.Concrete.Player.Anton.States;
using AntonCoolpecker.Utils;
using AntonCoolpecker.Utils.Time;
using System;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Cameras.States
{
	/// <summary>
	/// Swing camera state - Camera state when Anton is swinging his tongue while midair
	/// </summary>
    public class SwingCameraState : AbstractCameraState
    {
		#region Variables

		//A multiplier for the distance between Anton and the Graaple point that sets the distance between the grapple point and the camera.
        [Tweakable("SwingCamera")] [SerializeField] private float m_cameraDistanceMultiplier;

        [Tweakable("SwingCamera")] [SerializeField] private float m_cameraPositionOffsetX; //Offset of the camera's base position on the target's right direction.
        [Tweakable("SwingCamera")] [SerializeField] private float m_cameraPositionOffsetY; //Offset of the camera's base position on target's up direction.

        [Tweakable("SwingCamera")] [SerializeField] private float m_cameraTargetOffsetX; //Offset of where the camera looks based on the target's right direction.
        [Tweakable("SwingCamera")] [SerializeField] private float m_cameraTargetOffsetY; //Offset of where the camera looks based on the target's up direction.

        [Tweakable("SwingCamera")] [SerializeField] private bool m_cameraPositionLerp; //While true, will Lerp towards its expected position.
        [Tweakable("SwingCamera")] [SerializeField] private float m_cameraPositionLerpSpeed; //How quickly the Camera lerps.

        [Tweakable("SwingCamera")] [SerializeField] private bool m_cameraRotationLerp; //While true, will lerp towards its expected rotation.
        [Tweakable("SwingCamera")] [SerializeField] private float m_cameraRotationLerpSpeed; //How quickly the camera lerps.

		#endregion

		#region Override Functions

        /// <summary>
        /// How the camera orients itself each update.
        /// </summary>
        /// <param name="parent"></param>
        protected override void Orient(MonoBehaviour parent)
        {
            CameraController cameraController = GetCameraController(parent);
            SwingingAntonState targetState = (SwingingAntonState)cameraController.target.stateMachine.activeState;

            Vector3 cameraDir = -cameraController.target.transform.forward * Vector3.Distance(cameraController.target.transform.position, targetState.grapplePoint.transform.position);

            Vector3 cameraBasePos = new Vector3(cameraController.target.transform.position.x,
				targetState.grapplePoint.transform.position.y,
				cameraController.target.transform.position.z) + cameraController.target.transform.right * m_cameraPositionOffsetX + cameraController.target.transform.up * m_cameraPositionOffsetY;
			
            Vector3 cameraPos = cameraDir * m_cameraDistanceMultiplier + cameraBasePos;

            if (m_cameraPositionLerp)
                cameraController.transform.position = Vector3.Lerp(cameraController.transform.position, cameraPos, m_cameraPositionLerpSpeed);
            else
                cameraController.transform.position = cameraPos;

            Quaternion currentRot = cameraController.transform.rotation;

            cameraController.transform.LookAt(targetState.grapplePoint.transform.position +
				cameraController.target.transform.right * m_cameraTargetOffsetX +
				cameraController.target.transform.up * m_cameraTargetOffsetY);
            
			if (m_cameraRotationLerp)
                cameraController.transform.rotation = Quaternion.Lerp(currentRot, cameraController.transform.rotation, m_cameraRotationLerpSpeed);
        }

		#endregion
    }
}