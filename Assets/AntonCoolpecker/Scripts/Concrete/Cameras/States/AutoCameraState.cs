﻿using AntonCoolpecker.Abstract.Cameras.States;
using AntonCoolpecker.Abstract.Player;
using AntonCoolpecker.Concrete.Configuration;
using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using AntonCoolpecker.Utils;
using AntonCoolpecker.Utils.Time;
using Hydra.HydraCommon.Utils;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Cameras.States
{
	/// <summary>
	/// 	The Auto camera will offset from the pivot slightly in the direction the
	/// 	player is facing. It will gradually pan behind the player until facing
	/// 	along the player's forward.
	/// 	
	/// 	The Auto camera will transition to the Free camera when the player starts
	/// 	to pan the camera manually.
    /// 	
    ///     When the player is moving sideways relative to the direction of the camera, the Auto camera has two modes of operation:
    ///         - Circular mode (default): Camera stays roughly in place, and pans/rotates to face player.
    ///           If the player keeps moving sideways, they will go in a circle.
    ///         - Strafe mode: Camera moves left and right to follow the player.
    ///           If the player keeps moving sideways, they will go in a line.
    ///     The mode can be toggle in the pause menu options, or by clicking the right analog stick.
	/// </summary>
	public class AutoCameraState : AbstractPredictiveCameraState
	{
		#region Variables

		[SerializeField] private FreeCameraState m_FreeCameraState;

		[Tweakable("Camera")] [SerializeField] private float m_PredictionDistanceSideways; //How much the camera will predict sideways
		[Tweakable("Camera")] [SerializeField] private float m_PredictionDistanceForward; //How much the camera will predict forwards
		[Tweakable("Camera")] [SerializeField] private float m_PredictionDistanceBackwards; //How much the camera will predict backwards

		#endregion

		#region Override Functions/Methods

		/// <summary>
		/// 	Orient the camera.
		/// </summary>
		/// <param name="parent">Parent.</param>
		protected override void Orient(MonoBehaviour parent)
		{
			base.Orient(parent);

			CameraController cameraController = GetCameraController(parent);

            // orbitY → pitch → rotate about X axis
            // orbitX →  yaw  → rotate about Y axis
			Quaternion currentRotation = Quaternion.Euler(cameraController.orbitY, cameraController.orbitX, 0);

            // Part 1 - yaw so that we face the character (+ offset) if not in strafe mode
            if (!Options.cameraStrafeMode)
            {
                //Find the 2d angle (on the XZ plane) between the current and desired direction
                Vector3 offsetPosition = GetOffsetPosition(parent);
                Vector3 desiredDirection = offsetPosition - cameraController.transform.position;
                Vector3 currentDirection = cameraController.transform.forward;
                //Note that Atan2 is anti-clockwise, while we want the clockwise direction, so we do the subtraction backwards.
                float angle = (Mathf.Atan2(currentDirection.z, currentDirection.x) - Mathf.Atan2(desiredDirection.z, desiredDirection.x)) * Mathf.Rad2Deg;

                // Apply the transformation
                currentRotation *= Quaternion.AngleAxis(angle, Vector3.up);
            }

            // Part 2 - slowly orbit to be behind character

			float targetYaw = cameraController.target.transform.rotation.eulerAngles.y;
			Quaternion targetRotation = Quaternion.Euler(currentRotation.x, targetYaw, 0.0f);

			cameraController.transform.rotation = Quaternion.RotateTowards(currentRotation, targetRotation,
																		   GameTime.deltaTime * 5.0f); // Max 5°/s
			cameraController.transform.position = GetPositionFromRotation(parent);
		}

		/// <summary>
		/// 	Returns a state for transition. Return self if no transition.
		/// </summary>
		/// <returns>The next state.</returns>
		/// <param name="parent">Parent.</param>
		public override AbstractCameraState GetNextState(MonoBehaviour parent)
		{
			if (CameraController.HasUserInput())
				return m_FreeCameraState;

			return base.GetNextState(parent);
		}

		/// <summary>
		/// 	Updates the prediction offset.
		/// </summary>
		/// <param name="parent">Parent.</param>
		protected override void UpdatePredictionOffset(MonoBehaviour parent)
		{
			CameraController controller = GetCameraController(parent);
			AbstractPlayerController target = controller.target;

			Vector3 cameraForward = controller.transform.forward;
			Vector3 playerForward = target.transform.forward;

			cameraForward.y = 0.0f;
			playerForward.y = 0.0f;

			float dot = Vector3.Dot(cameraForward, playerForward);
			float distance;

			if (dot >= 0.0f)
				distance = HydraMathUtils.MapRange(0.0f, 1.0f, m_PredictionDistanceSideways, m_PredictionDistanceForward, dot);
			else
				distance = HydraMathUtils.MapRange(-1.0f, 0.0f, m_PredictionDistanceBackwards, m_PredictionDistanceSideways, dot);

			Quaternion rotation = target.transform.rotation;

			Vector3 offset = rotation * (Vector3.forward * distance);
			Vector3 delta = offset - controller.predictionOffset;

			controller.predictionOffset += delta * predictionSpeed * GameTime.fixedDeltaTime;
		}

        /// <summary>
        /// Handle input for toggling before doing regular stuff.
        /// We would do this in the regular update phase, but camera states don't get OnUpdate calls.
        /// </summary>
        /// <param name="parent"></param>
        public override void OnLateUpdate(MonoBehaviour parent)
        {
            if (InputMapping.cameraMode.GetButtonDown())
            {
                Options.cameraStrafeMode = !Options.cameraStrafeMode;
            }

            base.OnLateUpdate(parent);
        }

		#endregion
	}
}