﻿using AntonCoolpecker.Abstract.Cameras.States;
using AntonCoolpecker.Abstract.Player;
using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using AntonCoolpecker.Concrete.Utils;
using AntonCoolpecker.Utils;
using AntonCoolpecker.Utils.Time;
using Hydra.HydraCommon.Utils;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Cameras.States
{
    /// <summary>
    /// Camera will rotate to get behind the player,
    /// once behind the camera will try to remain behind the player.
	/// Will go to the free camera state if the player moves the camera.
    /// </summary>
    public class GlideCameraState : AbstractCameraState
    {
		#region Variables

		[SerializeField] private FreeCameraState m_FreeCameraState;

		[Tweakable("GlideCamera")] [SerializeField] private float m_MaxAngle = 30; //Max angle before the camera starts to get behind the player
		[Tweakable("GlideCamera")] [SerializeField] private float m_InitialCamRotationSpeed = 2f; //Inital camera rotation speed
		[Tweakable("GlideCamera")] [SerializeField] private float m_AutoCenterTimeDelay = 2f; //Time delay before the camera auto-centers(No movement/camera input used)
		[Tweakable("GlideCamera")] [SerializeField] private float m_AutoCenterRotationSpeed = 2f; //Auto center rotation speed
		[Tweakable("GlideCamera")] [SerializeField] private float m_CamSmoothTurnSpeed = 10f; //Camera rotation speed if using turn angle smoothing
		[Tweakable("GlideCamera")] [SerializeField] private float m_MoveTime = 0.5f; //Amount of time before system goes back to auto-orientation

		private Vector3 m_Offset; //Camera offset

		[Tweakable("GlideCamera")] [SerializeField] private bool m_SmoothTurnAngle = true; //Should the camera's turnaround be smoothed?

        private bool m_GettingBehind; //Is the camera getting behind the player?
        private float m_AutoCenterTimeTracker; //Decremential value keeping track of whenever it's time for auto-centralizing the camera
        private float m_GetBehindTimer; //Time-dependent multiplier for when the camera is getting behind the player
        private Timer m_MoveTimer; //TODO: CHECK WHAT THIS TIMER IS USED FOR

        //TODO: IMPLEMENT CAMERA SMOOTHING DEPENDENT ON MAXANGLE VALUE(IF PLAYER'S ROTATION ISN'T PAST THE MAX ANGLE)

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the camera X offset.
		/// </summary>
		/// <value>The camera X offset.</value>
		[Tweakable("GlideCamera")] [SerializeField] private float m_OffsetX { get { return m_Offset.x; } set { m_Offset.x = value; } }

		/// <summary>
		/// Gets or sets the camera Y offset.
		/// </summary>
		/// <value>The camera Y offset.</value>
		[Tweakable("GlideCamera")] [SerializeField] private float m_OffsetY { get { return m_Offset.y; } set { m_Offset.y = value; } }

		/// <summary>
		/// Gets or sets the camera Z offset.
		/// </summary>
		/// <value>The camera Z offset.</value>
		[Tweakable("GlideCamera")] [SerializeField] private float m_OffsetZ { get { return m_Offset.z; } set { m_Offset.z = value; } }

		#endregion

        #region Override Methods

        /// <summary>
        /// 	Called when the state becomes active.
        /// </summary>
        /// <param name="parent">Parent.</param>
        public override void OnEnter(MonoBehaviour parent)
        {
            base.OnEnter(parent);

            if (m_MoveTimer == null)
                m_MoveTimer = new Timer();

            m_MoveTimer.maxTime = m_MoveTime;
            m_MoveTimer.AddSeconds(m_MoveTime);

            //Initialise private variables 
            m_AutoCenterTimeTracker = m_AutoCenterTimeDelay;
            m_GetBehindTimer = 0;

            //Check if the camera is already behind the player - if so, the camera will get behind the player.
            CameraController cameraController = GetCameraController(parent);
            Quaternion orbitRotation = Quaternion.Euler(cameraController.orbitY, cameraController.orbitX, 0);
            float yaw = cameraController.target.transform.eulerAngles.y;

            float angleDifference = HydraMathUtils.Abs(Mathf.DeltaAngle(cameraController.transform.rotation.eulerAngles.y, yaw));

            if (angleDifference > m_MaxAngle)
				m_GettingBehind = true;            
            else
                m_GettingBehind = false;
        }

        /// <summary>
        ///     Orient the camera.
        /// </summary>
        /// <param name="parent">Parent.</param>
        protected override void Orient(MonoBehaviour parent)
        {
            CameraController cameraController = GetCameraController(parent);
            Transform transform = cameraController.transform;

            float yaw = cameraController.target.transform.eulerAngles.y;          
            Quaternion targetQuaternion = Quaternion.Euler(transform.rotation.eulerAngles.x, yaw, 0.0f);
            float angleDifference = HydraMathUtils.Abs(Mathf.DeltaAngle(transform.rotation.eulerAngles.y, yaw));
            float heading = transform.eulerAngles.y - yaw;
           
            if (heading < 0)
                heading = heading + 360;

            if (m_GettingBehind)
            {
                //The value for t needs to increase every frame to ensure that the camera will eventualy get behind the player.
                m_GetBehindTimer = m_GetBehindTimer + GameTime.deltaTime;
                transform.rotation = Quaternion.Slerp(transform.rotation, targetQuaternion,
                    m_InitialCamRotationSpeed * m_GetBehindTimer);

                //If we have gotten behind the player in the current frame, snap to a MaxAngle value to prevent jittering.
                if (HydraMathUtils.Abs(Mathf.DeltaAngle(transform.rotation.eulerAngles.y, yaw)) <= m_MaxAngle)
                {
                    m_GettingBehind = false;

                    if (heading > 180)
                        transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, yaw - (m_MaxAngle), 0.0f);
                    else
                        transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, yaw + (m_MaxAngle), 0.0f);
                }
            }
			else if (InputMapping.horizontalInput.GetAxis() != 0 || InputMapping.verticalInput.GetAxis() != 0) //Y axis rotation
            {
                if (angleDifference > m_MaxAngle)
                {
                    if (heading > 180)
                        transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, yaw - (m_MaxAngle), 0.0f);
                    else
                        transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, yaw + (m_MaxAngle), 0.0f);

                }
                else if (m_SmoothTurnAngle)
                {
                    if (angleDifference > 0)
                    {
                        float delta = (GameTime.deltaTime * Mathf.Pow((angleDifference / m_MaxAngle), m_CamSmoothTurnSpeed));
                        transform.rotation = Quaternion.Slerp(transform.rotation, targetQuaternion, delta);
                    }
                }
            }

            //Auto centering
			if (InputMapping.horizontalInput.GetAxisRaw() == 0 && InputMapping.verticalInput.GetAxisRaw() >= 0)
            {
                targetQuaternion.x = 0;
                targetQuaternion.z = 0;

                if (m_AutoCenterTimeTracker <= 0)
                {
                    transform.rotation = Quaternion.Slerp(transform.rotation, targetQuaternion, GameTime.deltaTime * m_AutoCenterRotationSpeed);
                }
                else
                    m_AutoCenterTimeTracker = m_AutoCenterTimeTracker - GameTime.deltaTime;
            }
            else
                m_AutoCenterTimeTracker = m_AutoCenterTimeDelay;
			
            transform.position = GetPositionFromRotation(parent);
        }

        /// <summary>
        /// Gets the offset position
        /// </summary>
        /// <param name="parent"></param>
        protected override Vector3 GetOffsetPosition(MonoBehaviour parent)
        {
            CameraController controller = GetCameraController(parent);

            controller.predictionOffset = Vector3.Lerp(controller.predictionOffset,
                controller.target.transform.TransformDirection(m_Offset), GameTime.deltaTime);
			
            Vector3 offset = base.GetOffsetPosition(parent) + controller.predictionOffset;

            return offset;
        }
			
		/// <summary>
		/// 	Returns a state for transition. Return self if no transition.
		/// </summary>
		/// <returns>The next state.</returns>
		/// <param name="parent">Parent.</param>
		public override AbstractCameraState GetNextState(MonoBehaviour parent)
		{
			if (CameraController.HasUserInput())
				return m_FreeCameraState;

			return base.GetNextState(parent);
		}

		#endregion

		#region Protected/Public Methods

        /// <summary>
		/// 	Gets the position from rotation.
		/// </summary>
		/// <returns>The position from rotation.</returns>
		/// <param name="parent">Parent.</param>
		/// <param name="rotation">Rotation.</param>
		protected new Vector3 GetPositionFromRotation(MonoBehaviour parent, Quaternion rotation)
        {
            CameraController cameraController = GetCameraController(parent);

            float distance = cameraController.distance;
            Vector3 offsetPosition = GetOffsetPosition(parent);
            Vector3 back = rotation * Vector3.back;

            return back * distance + offsetPosition;
        }

		/// <summary>
		/// Gets the max angle.
		/// </summary>
		/// <returns>The max angle.</returns>
        public float GetMaxAngle()
        {
            return m_MaxAngle;
        }

        #endregion
    }
}