﻿using AntonCoolpecker.Abstract.Cameras.States;
using AntonCoolpecker.Concrete.Cameras;
using AntonCoolpecker.Concrete.Utils;
using AntonCoolpecker.Utils.Time;
using System;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Cameras.States
{
	/// <summary>
	/// Cutscene camera state.
	/// </summary>
    public class CutsceneCameraState : AbstractCameraState
    {
		#region Variables

        private CameraPoint[] path; //Path of Camera points that will be folowed - is set from CameraController
        private int arrayPoint; //Where we are in the array of camera points
        float CurrentTime; //The current time since last node point
        float DesiredTime; //At what time do we need to be at the next node
        float currentFOV; //The FOV of the current node
        float DesiredFOV; //The FOV of the next node
        float StoredFOV; //Our original FOV, for restoration when this state is over
        private Transform StoredTransform; //Our original transform position, may not be needed

        private Vector3 CurrentPosition, TargetPosition;       
        private Quaternion CurrentRotation,TargetRotation;
        private CameraController m_Controller;

		#endregion

		#region Private Functions

		/// <summary>
		/// Works the out next point - Sets variables for the next point in the array.
		/// </summary>
        private void WorkOutNextPoint()
        {
            CameraPoint CurrentPoint = path[arrayPoint];
            arrayPoint++;
            CameraPoint NextPoint = path[arrayPoint];
            CurrentTime -= CurrentPoint.TimeToThisNode;
            DesiredTime = NextPoint.TimeToThisNode;
            CurrentPosition = CurrentPoint.position;
            TargetPosition = NextPoint.position;
            CurrentRotation = CurrentPoint.Rotation;
            TargetRotation = NextPoint.Rotation;
            currentFOV = CurrentPoint.FieldOfView;
            DesiredFOV = NextPoint.FieldOfView;
        }
			
		/// <summary>
		/// End the specified parent - Called to change the player state via CameraController.
		/// </summary>
		/// <param name="parent">Parent.</param>
        private void End(MonoBehaviour parent)
        {
            GetCameraController(parent).target.ExitCutscene();
        }

		#endregion

		#region Override Functions

		/// <summary>
		/// Called before another state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
        public override void OnExit(MonoBehaviour parent)
        {
            base.OnExit(parent);

            parent.transform.position = StoredTransform.position;
            GetCameraController(parent).camera.fieldOfView = StoredFOV;
        }
			
		/// <summary>
		/// Called when the state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
        public override void OnEnter(MonoBehaviour parent)
        {
            base.OnEnter(parent);

            m_Controller = GetCameraController(parent);
            path = m_Controller.GetPath;
            StoredFOV = m_Controller.camera.fieldOfView;
            StoredTransform = parent.transform;
            arrayPoint = 0;
             
            WorkOutNextPoint();
            CurrentTime = 0;
            parent.transform.position = path[0].position;
            parent.transform.rotation = path[0].Rotation;
        }

        /// <summary>
        /// 	Called when the parent late updates.
        /// </summary>
        /// <param name="parent">Parent.</param>
        public override void OnLateUpdate(MonoBehaviour parent)
        {
            if (GameTime.paused)
                return;

            if (path[arrayPoint].InputAutoAdvance && Configuration.Controls.Mapping.InputMapping.interactionInput.GetButtonDown())
            {
                if (arrayPoint < path.Length - 1)
                    WorkOutNextPoint();
                else
                    End(parent);
				
                CurrentTime = 0;
                Orient(parent);

                return;
            }

            CurrentTime += GameTime.deltaTime;

            if (CurrentTime >= DesiredTime)
            {
                if (!path[arrayPoint].RequireInputToAdvance || Configuration.Controls.Mapping.InputMapping.interactionInput.GetButtonDown())
                {
                    if (arrayPoint < path.Length - 1)
                        WorkOutNextPoint();
                    else
                        End(parent);
                }
                else
                    CurrentTime -= GameTime.deltaTime;
            }

            Orient(parent);
        }

		/// <summary>
		/// Orient the camera.
		/// </summary>
		/// <param name="parent">Parent.</param>
        protected override void Orient(MonoBehaviour parent)
        {
            float TValue;

            if (DesiredTime == 0)
                TValue = 1;
            else
                TValue = CurrentTime / DesiredTime;  
            
            m_Controller.transform.position = Vector3.Lerp(CurrentPosition, TargetPosition, TValue);
            m_Controller.transform.rotation = Quaternion.Slerp(CurrentRotation, TargetRotation, TValue);
            m_Controller.camera.fieldOfView = Mathf.Lerp(currentFOV, DesiredFOV, TValue);
        }

		#endregion
    }
}