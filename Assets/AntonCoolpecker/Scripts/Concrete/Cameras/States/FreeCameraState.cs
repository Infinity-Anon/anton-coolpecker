﻿using AntonCoolpecker.Abstract.Cameras.States;
using AntonCoolpecker.Abstract.Player;
using AntonCoolpecker.Utils.Time;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Cameras.States
{
	/// <summary>
	/// 	The Free camera gradually returns to the target pivot. It rotates around
	/// 	the pivot based on user input.
	/// 	
	/// 	The Free camera transitions to the Auto camera when there is no user input.
	/// </summary>
	public class FreeCameraState : AbstractPredictiveCameraState
	{
		#region Variables

		[SerializeField] private AbstractCameraState m_CameraChangeState; //Next camera state
		[SerializeField] private float m_TransitionDelay; //Time delay(after last time input was used) before camera transition occurs

		private float m_LastInputTime; //Stores the time that the player last used input

		#endregion

		#region Override Functions/Methods

		/// <summary>
		/// 	Orient the camera.
		/// </summary>
		/// <param name="parent">Parent.</param>
		protected override void Orient(MonoBehaviour parent)
		{
			base.Orient(parent);

			CameraController cameraController = GetCameraController(parent);

			cameraController.transform.rotation = Quaternion.Euler(cameraController.orbitY, cameraController.orbitX, 0);
			cameraController.transform.position = GetPositionFromRotation(parent);

			if (CameraController.HasUserInput())
				m_LastInputTime = GameTime.time;
		}

		/// <summary>
		/// 	Returns a state for transition. Return self if no transition.
		/// </summary>
		/// <returns>The next state.</returns>
		/// <param name="parent">Parent.</param>
		public override AbstractCameraState GetNextState(MonoBehaviour parent)
		{
			if (GameTime.time - m_LastInputTime >= m_TransitionDelay && AbstractPlayerController.HasUserInput())
				return m_CameraChangeState;

			return base.GetNextState(parent);
		}

		/// <summary>
		/// 	Gradually moves the prediction offset back to 0.
		/// </summary>
		protected override void UpdatePredictionOffset(MonoBehaviour parent)
		{
			CameraController controller = GetCameraController(parent);

			Vector3 delta = Vector3.ClampMagnitude(controller.predictionOffset, predictionSpeed * GameTime.deltaTime);

			controller.predictionOffset -= delta;
		}

		#endregion
	}
}