﻿using AntonCoolpecker.Abstract.Cameras.States;
using AntonCoolpecker.Utils;
using AntonCoolpecker.Utils.Time;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Cameras.States
{
	/// <summary>
	/// Dash camera state - Extension of the dash camera state.
	/// Switches to free camera state when the player moves the camera around manually.
	/// </summary>
    public class DashCameraState : DashChargeCameraState
    {
		#region Variables

		[SerializeField] private FreeCameraState m_FreeCameraState;

		#endregion

		#region Override Functions/Methods

        /// <summary>
        /// 	Returns a state for transition. Return self if no transition.
        /// </summary>
        /// <returns>The next state.</returns>
        /// <param name="parent">Parent.</param>
        public override AbstractCameraState GetNextState(MonoBehaviour parent)
        {
            if (CameraController.HasUserInput())
                return m_FreeCameraState;

            return base.GetNextState(parent);
        }

		#endregion
    }
}