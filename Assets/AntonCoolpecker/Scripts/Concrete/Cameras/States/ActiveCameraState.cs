﻿using AntonCoolpecker.Abstract.Cameras.States;
using AntonCoolpecker.Utils.Time;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Cameras.States
{
    /// <summary>
    ///     Camera follows the rotation of the target.
    /// </summary>
    public class ActiveCameraState : AbstractCameraState
    {
		#region Override Functions/Methods

        /// <summary>
        ///     Orient the camera.
        /// </summary>
        /// <param name="parent">Parent.</param>
        protected override void Orient(MonoBehaviour parent)
        {
            CameraController cameraController = GetCameraController(parent);

            Transform transform = cameraController.transform;
            float yaw = cameraController.target.transform.eulerAngles.y;

            Quaternion current = parent.transform.rotation;
            Quaternion target = Quaternion.Euler(cameraController.orbitY, yaw, 0.0f);

            parent.transform.rotation = Quaternion.Slerp(current, target, GameTime.fixedDeltaTime);
            parent.transform.position = GetPositionFromRotation(parent);
        }

		/// <summary>
		/// Gets the offset position for the target, in world space.
		/// </summary>
		/// <returns>The offset position.</returns>
		/// <param name="parent">Parent.</param>
        protected override Vector3 GetOffsetPosition(MonoBehaviour parent)
        {
            CameraController controller = GetCameraController(parent);

            return base.GetOffsetPosition(parent) + controller.predictionOffset;
        }

		#endregion
    }
}