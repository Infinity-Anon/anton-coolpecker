﻿using AntonCoolpecker.Abstract.Cameras.States;
using AntonCoolpecker.Abstract.Cameras.Triggers;
using AntonCoolpecker.Abstract.Player;
using AntonCoolpecker.Abstract.Player.States;
using AntonCoolpecker.Concrete.Cameras.States;
using AntonCoolpecker.Concrete.Configuration;
using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using AntonCoolpecker.Concrete.Forces;
using AntonCoolpecker.Utils;
using AntonCoolpecker.Utils.Time;
using Hydra.HydraCommon.Abstract;
using Hydra.HydraCommon.Utils;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Cameras
{
	/// <summary>
	/// Camera controller.
	/// </summary>
    [RequireComponent(typeof(Camera))]
    public class CameraController : HydraMonoBehaviour
    {
		#region Variables

        //TODO: Fix so the player's input is independent of camera angle when entering a camera trigger until the player releases the input
        public enum Mode
        {
            Passive,
            Active,
            Fixed,
            Underwater,
            Static,
            Auto,
            Glide,
            DashCharge,
            Free,
            Swing,
            Dash,
            Cutscene
        }

		// Current skybox material(SHOULD ONLY BE CHANGED IN SCENEMANAGER PREFAB - FIELD SHOULD BE EMPTY, DO NOT MODIFY)
		// Requires "CubemapTransparent" shader applied to desired skybox material in order for underwater fog/skybox lerping to properly work
		//IMPORTANT: DEFAULT SKYBOX MUST BE SET TO "DEFAULTSKYBOX" IN UNITY-->WINDOW->LIGHTNING FOR SKYBOX TO BE PROPERLY RESET(MUST BE DONE FOR EACH LEVEL SEPERATELY)
		[SerializeField] private Material m_Skybox;

        [SerializeField] private CameraStateMachine m_StateMachine; //Camera state machine reference
		[SerializeField] private AbstractPlayerController m_Target; //Reference to the camera target(A.K.A the player)

		[SerializeField] private Vector3 m_PivotOffset; //Camera pivot offset
		[SerializeField] private bool m_ClampCamera; //Should the eulerangles

        [Tweakable("Camera")] [SerializeField] private float m_Distance = 5.0f; //The camera's distance to the player
        [Tweakable("Camera")] [SerializeField] private float m_YMinLimit = -20.0f; //Minimum clamp limit for the camera's Y orbit
        [Tweakable("Camera")] [SerializeField] private float m_YMaxLimit = 80.0f; //Maximum clamp limit for the camera's Y orbit

		[Tweakable("Camera")] [SerializeField] private bool m_CameraSmoothing = true; //Should the movement of the camera be smoothed?
		[Tweakable("Camera")] [SerializeField] private float m_SmoothXLerp = 5f; //Camera smoothing lerp in X orbit
		[Tweakable("Camera")] [SerializeField] private float m_SmoothYLerp = 25f; //Camera smoothing lerp in Y orbit

        [Tweakable("Camera")] [SerializeField] private float m_CameraRadius = 0.1f; //Camera sphere collider radius
        [Tweakable("Camera")] [SerializeField] private bool m_IsCameraColliderTrigger = true; //Is the camera's collider a trigger?
        [Tweakable("Camera")] [SerializeField] private float m_UnderwaterFogLerpSpeed = 3f; //Lerp speed for transition to underwater fog
        [Tweakable("Camera")] [SerializeField] private float m_UnderwaterFogDensity = 0.04f; //Density of underwater fog
        [Tweakable("Camera")] [SerializeField] private float m_SkyboxAlphaAdd = 0.05f; //Transparency to add/subtract when transitioning from/to underwater fog

		private float m_X = 0.0f; //Camera X orbit
		private float m_Y = 0.0f; //Camera Y orbit

		private SphereCollider m_Spherie; //Sphere collider for camera(Not serialized)

		private float a_sky = 0; //Initial alpha value of skybox material
		private bool isOutOfWater = true; //Detects whether the camera is under or over the water
        private Color m_UnderwaterFogColor = new Color(0f, 0.4f, 0.7f, 0.6f); //TODO: ADD COLOR SUPPORT TO DEBUG GUI - VALUE CHANGED ONLY CODE-WISE IN THE CURRENT MOMENT

        private Camera m_Template; //Camera template
        private CameraPath m_Path; //Camera cutscene path
        private List<AbstractTemplateCameraTrigger> m_Triggers; //Keeps track of triggers currently colliding with the camera

		private Vector3 m_PredictionOffset; //Camera prediction offset

		private bool m_OrbitEnabled = true; //Should the camera be able to orbit?

        #endregion

        #region Properties

		/// <summary>
		/// Gets the cutscene camera point path.
		/// </summary>
		/// <value>The get path.</value>
        public CameraPoint[] GetPath { get { return m_Path.Points; } }

		/// <summary>
		/// Sets the cutscene camera point path.
		/// </summary>
		/// <value>The set path.</value>
        public CameraPath SetPath { set { m_Path = value; } }

        /// <summary>
		/// 	Gets or sets the camera collider
		/// </summary>
		/// <value>The collider.</value>
		public SphereCollider collider { get { return m_Spherie; } set { m_Spherie = value; } }

        /// <summary>
        /// 	Gets or sets the skybox.
        /// </summary>
        /// <value>The skybox.</value>
        public Material skybox { get { return m_Skybox; } set { m_Skybox = value; } }

        /// <summary>
        /// 	Gets or sets the target.
        /// </summary>
        /// <value>The target.</value>
        public AbstractPlayerController target { get { return m_Target; } set { m_Target = value; } }

        /// <summary>
        /// 	Gets the distance.
        /// </summary>
        /// <value>The distance.</value>
        public float distance { get { return m_Distance; } }

        /// <summary>
        /// 	Gets the pivot offset.
        /// </summary>
        /// <value>The pivot offset.</value>
        public Vector3 pivotOffset { get { return m_PivotOffset; } }

        /// <summary>
        /// 	Gets or sets the prediction offset.
        /// </summary>
        /// <value>The prediction offset.</value>
        public Vector3 predictionOffset { get { return m_PredictionOffset; } set { m_PredictionOffset = value; } }

        /// <summary>
        /// 	Gets or sets a value indicating whether orbit is enabled.
        /// </summary>
        /// <value><c>true</c> if orbit enabled; otherwise, <c>false</c>.</value>
        public bool orbitEnabled { get { return m_OrbitEnabled; } set { m_OrbitEnabled = value; } }

        /// <summary>
        /// 	Gets or sets the orientation x.
        /// </summary>
        /// <value>The orientation x.</value>
        public float orbitX { get { return m_X; } set { m_X = value; } }

        /// <summary>
        /// 	Gets or sets the orientation y.
        /// </summary>
        /// <value>The orientation y.</value>
        public float orbitY { get { return m_Y; } set { m_Y = value; } }

        /// <summary>
        /// 	Gets or sets whether the orientation x is inverted or not.
        /// </summary>
        /// <value>Inversion of the orientation x.</value>
        //public bool invertedX { get { return m_InvertedX; } set { m_InvertedX = value; } }
        [Tweakable("Camera")]
        public bool invertedX { get { return Options.cameraHorizontalInverted; } set { Options.cameraHorizontalInverted = value; } }

        /// <summary>
        /// 	Gets or sets whether the orientation y is inverted or not.
        /// </summary>
        /// <value>Inversion of the orientation y.</value>
        //public bool invertedY { get { return m_InvertedY; } set { m_InvertedY = value; } }
        [Tweakable("Camera")]
        public bool invertedY { get { return Options.cameraVerticalInverted; } set { Options.cameraVerticalInverted = value; } }

        /// <summary>
        /// 	Gets the minimum Y limit.
        /// </summary>
        /// <value>The minimum Y limit.</value>
        public float minYLimit { get { return m_YMinLimit; } }

        /// <summary>
        /// 	Gets the max Y limit.
        /// </summary>
        /// <value>The max Y limit.</value>
        public float maxYLimit { get { return m_YMaxLimit; } }

        /// <summary>
        /// 	Gets the template.
        /// </summary>
        /// <value>The template.</value>
        public Camera template { get { return m_Template; } }

        /// <summary>
        /// 	Gets the camera velocity.
        /// </summary>
        /// <value>The template.</value>
        public Vector2 speed
        {
            get { return new Vector2(Options.cameraHorizontalSensitivity, Options.cameraVerticalSensitivity); }
        }

        /// <summary>
        /// 	Gets or sets a value indicating whether the camera is clamped or not
        /// </summary>
        /// <value><c>true</c> if clamp enabled; otherwise, <c>false</c>.</value>
        public bool clampEnabled { get { return m_ClampCamera; } set { m_ClampCamera = value; } }

        /// <summary>
        /// 	Gets or sets a value indicating whether the camera is smoothed or not
        /// </summary>
        /// <value><c>true</c> if smooth enabled; otherwise, <c>false</c>.</value>
        public bool cameraSmoothing { get { return m_CameraSmoothing; } set { m_CameraSmoothing = value; } }

        /// <summary>
        /// 	Gets or sets the smooth multiplier for the x-coordinate of the camera's rotation
        /// </summary>
        /// <value>The x-coordinate smooth multiplier for camera smoothing.</value>
        public float smoothXLerp { get { return m_SmoothXLerp; } }

        /// <summary>
        /// 	Gets or sets the smooth multiplier for the y-coordinate of the camera's rotation
        /// </summary>
        /// <value>The y-coordinate smooth multiplier for camera smoothing.</value>
        public float smoothYLerp { get { return m_SmoothYLerp; } }

        /// <summary>
        /// 	Gets or sets the camera state machine
        /// </summary>
        /// <value>The camera state machine.</value>
        public CameraStateMachine stateMachine { get { return m_StateMachine; } set { m_StateMachine = value; } }

        #endregion

        #region Messages

        /// <summary>
        /// 	Called when the component is enabled.
        /// </summary>
        protected override void OnEnable()
        {
            base.OnEnable();

            if (m_Triggers == null)
                m_Triggers = new List<AbstractTemplateCameraTrigger>();

            //Define camera sphere collider
            m_Spherie = gameObject.AddComponent<SphereCollider>();
            m_Spherie.radius = m_CameraRadius;
            m_Spherie.isTrigger = m_IsCameraColliderTrigger;

            //RenderSettings.skybox = m_Skybox;

            //if (m_Skybox != null && m_Skybox.name != "Default-Skybox")
            //RenderSettings.skybox.SetFloat("_Alpha", 0);
        }

        /// <summary>
        /// 	Called after all Updates have finished.
        /// </summary>
        protected override void LateUpdate()
        {
            Camera.main.fieldOfView = Options.cameraFieldOfView;
            base.LateUpdate();

            m_StateMachine.LateUpdate(this);

            //Debug.Log (orbitX);
            //Debug.Log (orbitY);

            //Return if no skybox is applied
            if (m_Skybox == null || m_Skybox.name == "Default-Skybox")
                return;

            if (isOutOfWater)
                RenderNormalFog();
            else
                RenderUnderwaterFog();

            //If the skybox in the render settings isn't the desired skybox
            if (RenderSettings.skybox != m_Skybox)
            {
                setSkybox(); //Set the skybox in the render settings dynamically
            }
        }

        #endregion

        #region Public Functions/Methods

        public void SetCutsceneFlag()
        {
            Main.instance.playerProgress.GetCurrentLevelProgress(Scene.SceneManager.instance.name).SetPermanentFlag(m_Path.ID);
        }

        /// <summary>
        /// 	Sets the mode.
        /// </summary>
        /// <param name="mode">Mode.</param>
        public void SetMode(Mode mode)
        {
            SetMode(mode, null);
        }

        /// <summary>
        /// 	Sets the mode.
        /// </summary>
        /// <param name="mode">Mode.</param>
        /// <param name="template">Template.</param>
        public void SetMode(Mode mode, Camera template)
        {
            m_StateMachine.SetActiveState(mode, this);
            m_Template = template;
        }

        /// <summary>
        /// 	Gets the mode.
        /// </summary>
        public Mode GetMode()
        {
            return m_StateMachine.GetMode();
        }

        /// <summary>
        /// 	Adds the trigger.
        /// </summary>
        /// <param name="trigger">Trigger.</param>
        public void AddTrigger(AbstractTemplateCameraTrigger trigger)
        {
            RemoveTrigger(trigger);
            m_Triggers.Add(trigger);

            SetMode(trigger.mode, trigger.template);
        }

        /// <summary>
        /// 	Removes the trigger.
        /// </summary>
        /// <param name="trigger">Trigger.</param>
        public void RemoveTrigger(AbstractTemplateCameraTrigger trigger)
        {
            m_Triggers.Remove(trigger);

            AbstractTemplateCameraTrigger active = GetActiveTrigger();

            if (active != null)
            {
                SetMode(active.mode, active.template);
                return;
            }

            AbstractPlayerController player = m_Target.GetComponent<AbstractPlayerController>();

            if (player == null)
                return;

            AbstractPlayerState state = player.stateMachine.activeState;
            SetMode(state.cameraMode);
        }

        /// <summary>
        /// 	Gets the active trigger.
        /// </summary>
        /// <returns>The active trigger.</returns>
        public AbstractTemplateCameraTrigger GetActiveTrigger()
        {
            if (m_Triggers.Count > 0)
                return m_Triggers[m_Triggers.Count - 1];
			
            return null;
        }

        /// <summary>
        /// Removes all triggers, e.g. for resetting the camera after losing a life.
        /// </summary>
        public void RemoveAllTriggers()
        {
            m_Triggers.Clear();

            SetMode(m_Target.GetComponent<AbstractPlayerController>().stateMachine.activeState.cameraMode);
        }

		/// <summary>
		/// 	Checks if the camera is within limits.
		/// </summary>
		/// <value><c>true</c> if the movement keys/joystick isn't used; otherwise, <c>false</c>.</value>
		public bool IsWithinCameraClamp()
		{
			if (m_Y < m_YMaxLimit || m_Y > m_YMinLimit)
				return true;

			return false;
		}

        #endregion

        #region Static Methods

        /// <summary>
        /// 	Returns true if template should be used for the given mode.
        /// </summary>
        /// <returns><c>true</c>, if template is used, <c>false</c> otherwise.</returns>
        /// <param name="mode">Mode.</param>
        public static bool UseTemplate(Mode mode)
        {
            Type stateType = CameraStateMachine.GetStateTypeByMode(mode);

            return typeof(AbstractTemplateCameraState).IsAssignableFrom(stateType);
        }

        /// <summary>
        /// 	Returns true when the user is adjusting the camera manually.
        /// </summary>
        /// <returns><c>true</c> if this instance has user input; otherwise, <c>false</c>.</returns>
        public static bool HasUserInput()
        {
            float horizontal = InputMapping.horizontalCameraInput.GetAxis();
            float vertical = InputMapping.verticalCameraInput.GetAxis();

            if (!HydraMathUtils.Approximately(horizontal, 0.0f))
                return true;

            if (!HydraMathUtils.Approximately(vertical, 0.0f))
                return true;

            return false;
        }

        /// <summary>
        /// 	Returns true when the user is adjusting the camera manually(Raw input data, no smoothing).
        /// </summary>
        /// <returns><c>true</c> if this instance has user input; otherwise, <c>false</c>.</returns>
        public static bool HasUserInputRaw()
        {
            float horizontal = InputMapping.horizontalCameraInput.GetAxisRaw();
            float vertical = InputMapping.verticalCameraInput.GetAxisRaw();

            if (horizontal == 0f && vertical == 0f)
                return false;

            return true;
        }

        #endregion

		#region Trigger Functions

        /// <summary>
        /// 	OnTriggerEnter is called when the Collider other enters the camera's collider trigger.
        /// </summary>
        /// <param name="other">Other.</param>
        protected override void OnTriggerEnter(Collider other)
        {
            base.OnTriggerEnter(other);

            WaterTrigger waterTrigger = other.GetComponent<WaterTrigger>();
            if (waterTrigger == null)
                return;

            if (other.name == "Surface Collider")
                return;

            if (isOutOfWater)
                isOutOfWater = false;
        }

        /// <summary>
        /// 	OnTriggerEnter is called when the Collider other enters the camera's collider trigger.
        /// </summary>
        /// <param name="other">Other.</param>
        protected override void OnTriggerStay(Collider other)
        {
            base.OnTriggerStay(other);

            WaterTrigger waterTrigger = other.GetComponent<WaterTrigger>();

            if (waterTrigger == null)
                return;

            if (other.name == "Surface Collider")
                return;

            if (isOutOfWater)
                isOutOfWater = false;
        }

        /// <summary>
        /// 	OnTriggerExit is called when the Collider other has stopped touching the trigger.
        /// </summary>
        /// <param name="other">Other.</param>
        protected override void OnTriggerExit(Collider other)
        {
            base.OnTriggerExit(other);

            WaterTrigger waterTrigger = other.GetComponent<WaterTrigger>();

            if (waterTrigger == null)
                return;

            if (other.name == "Surface Collider")
                return;

            if (!isOutOfWater)
                isOutOfWater = true;
        }

		#endregion

		#region Private Functions

		/// <summary>
		/// Renders the underwater fog.
		/// </summary>
        private void RenderUnderwaterFog()
        {
            float r = Mathf.Lerp(RenderSettings.fogColor.r, m_UnderwaterFogColor.r, m_UnderwaterFogLerpSpeed * GameTime.deltaTime);
            float g = Mathf.Lerp(RenderSettings.fogColor.g, m_UnderwaterFogColor.g, m_UnderwaterFogLerpSpeed * GameTime.deltaTime);
            float b = Mathf.Lerp(RenderSettings.fogColor.b, m_UnderwaterFogColor.b, m_UnderwaterFogLerpSpeed * GameTime.deltaTime);
            float a = Mathf.Lerp(RenderSettings.fogColor.a, m_UnderwaterFogColor.a, m_UnderwaterFogLerpSpeed * GameTime.deltaTime);

            RenderSettings.fogColor = new Color(r, g, b, a);

            RenderSettings.fogDensity = Mathf.Lerp(RenderSettings.fogDensity, m_UnderwaterFogDensity, m_UnderwaterFogLerpSpeed * GameTime.deltaTime);

            if (a_sky <= 1)
            {
                a_sky += m_SkyboxAlphaAdd;
                //Debug.Log(a_sky);
                RenderSettings.skybox.SetFloat("_Alpha", a_sky);
                //RenderSettings.ambientSkyColor = Color.red;
            }

            RenderSettings.fog = true;
        }

		/// <summary>
		/// Reset the fog settings to default settings
		/// </summary>
        private void RenderNormalFog()
        {
            if (a_sky <= 0)
                return;

            RenderSettings.fogColor = new Color(0.5f, 0.5f, 0.5f, 1f);
            RenderSettings.fogDensity = 0.01f;
            RenderSettings.fog = false;

            if (a_sky > 0)
            {
                a_sky -= (m_SkyboxAlphaAdd);
                RenderSettings.skybox.SetFloat("_Alpha", a_sky);
                //RenderSettings.ambientSkyColor = Color.red;
            }
        }

		/// <summary>
		/// Sets the skybox(Utilizing RenderSettings).
		/// </summary>
        private void setSkybox()
        {
            RenderSettings.skybox = m_Skybox; //Set skybox
            RenderSettings.skybox.SetFloat("_Alpha", 0); //Set the alpha of the internal shader to zero
        }

		#endregion
    }
}