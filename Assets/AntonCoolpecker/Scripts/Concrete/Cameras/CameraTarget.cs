﻿using Hydra.HydraCommon.Abstract;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Cameras
{
	/// <summary>
	/// Camera target prototype script.
	/// </summary>
	//TODO: REMOVE THIS SCRIPT?
	public class CameraTarget : HydraMonoBehaviour
	{
		#region Variables

		[SerializeField] private float xPos = 0f; //Camera target X offset
		[SerializeField] private float yPos = 1.3f; //Camera target Y offset
		[SerializeField] private float zPos = 0f; //Camera target Z offset
		[SerializeField] private Transform tTfT; //Reference to transform

		#endregion

		#region Override Functions

		/// <summary>
		/// Called every physics timestep.
		/// </summary>
		protected override void FixedUpdate()
		{
			gameObject.transform.position = new Vector3(tTfT.position.x + xPos, tTfT.position.y + yPos, tTfT.position.z + zPos);
			gameObject.transform.rotation = tTfT.rotation;

			base.FixedUpdate();
		}

		#endregion
	}
}