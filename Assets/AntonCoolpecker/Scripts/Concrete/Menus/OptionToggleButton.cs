﻿using AntonCoolpecker.Abstract.Menus.States;
using AntonCoolpecker.Concrete.Menus.HUD;
using Hydra.HydraCommon.Abstract;
using Hydra.HydraCommon.EventArguments;
using System;
using System.Collections;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Menus
{
	/// <summary>
	/// Option toggle button.
	/// </summary>
    public class OptionToggleButton : AbstractButton
    {
		#region Variables

        public event EventHandler<EventArg<bool>> onToggleValueChanged;

        public bool toggleValue;

		#endregion

		#region Override Functions

		/// <summary>
		/// Draw the specified rectangle, boxColor, textColor and digits.
		/// </summary>
		/// <param name="drawRect">Draw rect.</param>
		/// <param name="boxColor">Box color.</param>
		/// <param name="textColor">Text color.</param>
		/// <param name="digits">Digits.</param>
        public override void Draw(Rect drawRect, Color boxColor, HUDDigit.TextColor textColor, HUDDigit digits)
        {
			//DRAW OUT CAPITALIZED TITLE OF BUTTON

            GUI.color = Color.white;
            string capsTitle = title.ToUpper();
            for (int i = 0; i < capsTitle.Length; i++)
            {
                Rect doublePos = new Rect(drawRect.x + i * drawRect.width * 0.5f / (capsTitle.Length + 1),
                        drawRect.y + drawRect.height / 2 - drawRect.width / (capsTitle.Length + 1) * 0.25f,
                        drawRect.width / (capsTitle.Length + 1) * 0.5f, drawRect.width / (capsTitle.Length + 1) * 0.5f);
                Rect toGet = digits.GetDigit(capsTitle[i], textColor);
                GUI.DrawTextureWithTexCoords(doublePos, digits.m_DigitSpriteTexture, toGet);
            }

			// DRAW OUT TOGGLE BUTTON
            Color valueColor;
            Rect toggleButton = new Rect(drawRect.x+drawRect.width/2, drawRect.y+drawRect.height/4, drawRect.height/2, drawRect.height/2);

            if (toggleValue)
                valueColor = Color.green;
            else
                valueColor = Color.red;

            GUI.color = valueColor;
            GUI.DrawTexture(toggleButton, Texture2D.whiteTexture);
        }

		/// <summary>
		/// Called when the button has been clicked.
		/// </summary>
        public override void OnButtonClicked()
        {
            toggleValue = !toggleValue;

            EventHandler<EventArg<bool>> handler = onToggleValueChanged;
            if (handler != null)
                handler(this, new EventArg<bool>(toggleValue));
        }

		#endregion
    }
}