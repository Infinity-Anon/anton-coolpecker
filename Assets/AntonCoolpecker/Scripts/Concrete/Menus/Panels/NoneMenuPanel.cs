﻿using AntonCoolpecker.Abstract.Menus.Panels;
using UnityEngine;
using AntonCoolpecker.Concrete.Cameras;
using AntonCoolpecker.Concrete.Scene;

namespace AntonCoolpecker.Concrete.Menus.Panels
{
	/// <summary>
	/// 	None menu panel.
	/// </summary>
	public class NoneMenuPanel : AbstractMenuPanel 
	{
		protected override void Update()
		{
			base.Update();

			if (Cursor.lockState == CursorLockMode.None) 
			{
				if (!GameObject.Find ("Main").GetComponent<Main> ().debugGui.isActiveAndEnabled) 
				{
					Cursor.lockState = CursorLockMode.Locked;
					Cursor.visible = false;
					GameObject.FindGameObjectWithTag ("MainCamera").GetComponent<CameraController> ().orbitEnabled = true;
				}
			}
		}
	}
}
