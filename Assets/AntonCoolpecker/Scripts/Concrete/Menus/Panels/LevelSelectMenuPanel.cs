﻿using System;
using System.Collections.Generic;
using AntonCoolpecker.Abstract.Menus.Panels;
using AntonCoolpecker.Concrete.Utils;
using AntonCoolpecker.Concrete.Utils.UI;
using Hydra.HydraCommon.Utils;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Menus.Panels
{
	/// <summary>
	/// 	Level select menu panel.
	/// </summary>
	public class LevelSelectMenuPanel : AbstractMenuPanel
	{
		/*[SerializeField] private RectTransform m_SceneButtonLayout;
		[SerializeField] private ButtonWrapper m_SceneButtonPrefab;
		[SerializeField] private ButtonWrapper m_BackButton;

		private List<ButtonWrapper> m_SceneButtonInstances;

		#region Messages

		/// <summary>
		/// 	Called when the component is enabled.
		/// </summary>
		protected override void OnEnable()
		{
			base.OnEnable();

			if (m_SceneButtonInstances == null)
				PopulateSceneButtons();

			SubscribeToBackButton();
		}

		/// <summary>
		/// 	Called when the component is disabled.
		/// </summary>
		protected override void OnDisable()
		{
			base.OnDisable();

			UnsubscribeFromBackButton();
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// 	Populates the layout with buttons.
		/// </summary>
		private void PopulateSceneButtons()
		{
			m_SceneButtonInstances = new List<ButtonWrapper>();

			for (int index = 0; index < Application.levelCount; index++)
				CreateSceneButton(index);
		}

		/// <summary>
		/// 	Creates a button for the level with the given index and
		/// 	adds it to the layout.
		/// </summary>
		/// <param name="index">Index.</param>
		private void CreateSceneButton(int index)
		{
			ButtonWrapper instance = ObjectUtils.Instantiate(m_SceneButtonPrefab);

			instance.text.text = string.Format("{0} - {1}", index, SceneUtils.GetSceneName(index));

			if (index == Application.loadedLevel)
				instance.button.interactable = false;
			else
				SubscribeToSceneButton(instance);

			RectTransform instanceTransform = instance.transform as RectTransform;
			instanceTransform.SetParent(m_SceneButtonLayout, false);

			m_SceneButtonInstances.Add(instance);
		}

		/// <summary>
		/// 	Subscribe to the specified scene button.
		/// </summary>
		/// <param name="sceneButtonInstance">Scene Button instance.</param>
		private void SubscribeToSceneButton(ButtonWrapper sceneButtonInstance)
		{
			sceneButtonInstance.onClickedCallback += OnSceneButtonClicked;
		}

		/// <summary>
		/// 	Subscribe to the back button.
		/// </summary>
		private void SubscribeToBackButton()
		{
			m_BackButton.onClickedCallback += OnBackButtonClicked;
		}

		/// <summary>
		/// 	Unsubscribe from the back button.
		/// </summary>
		private void UnsubscribeFromBackButton()
		{
			m_BackButton.onClickedCallback -= OnBackButtonClicked;
		}

		/// <summary>
		/// 	Called when a scene button is clicked.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
		private void OnSceneButtonClicked(object sender, EventArgs e)
		{
			ButtonWrapper button = sender as ButtonWrapper;
			int index = m_SceneButtonInstances.IndexOf(button);

			Application.LoadLevel(index);
		}

		/// <summary>
		/// 	Called when the back button is clicked.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
		private void OnBackButtonClicked(object sender, EventArgs e)
		{
			PreviousMenu();
		}

		#endregion
        */
	}
}
