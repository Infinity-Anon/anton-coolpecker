﻿using System.Collections.Generic;
using System.IO;
using AntonCoolpecker.Abstract.Menus.Panels;
using Hydra.HydraCommon.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace AntonCoolpecker.Concrete.Menus.Panels
{
	/// <summary>
	/// 	Credits menu panel.
	/// </summary>
	public class CreditsMenuPanel : AbstractMenuPanel
	{
		private const string HEADER_PREFIX = ">";

		[SerializeField] private TextAsset m_Credits;
		[SerializeField] private Text m_CreditTextPrefab;
		[SerializeField] private Text m_CredtHeaderTextPrefab;
		[SerializeField] private RectTransform m_Layout;
		[SerializeField] private float m_ScrollSpeed;

		private List<Text> m_CreditTextInstances;

		#region Messages

		/// <summary>
		/// 	Called before the first Update.
		/// </summary>
		protected override void Start()
		{
			base.Start();

			if (m_CreditTextInstances == null)
				GenerateText();
		}

		/// <summary>
		/// 	Called once every frame.
		/// </summary>
		protected override void Update()
		{
			base.Update();

			m_Layout.anchoredPosition += Vector2.up * m_ScrollSpeed * Time.deltaTime;

			Rect rect = GetRelativeRect(m_Layout);
			if (rect.yMin > (transform as RectTransform).rect.yMax)
				ResetPosition();

			if (Input.anyKeyDown)
				PreviousMenu();
		}

		private Rect GetRelativeRect(RectTransform transform)
		{
			Rect rect = transform.rect;
			rect.center += transform.anchoredPosition;
			return rect;
		}

		#endregion

		#region Methods

		/// <summary>
		/// 	Resets the position.
		/// </summary>
		public void ResetPosition()
		{
			Vector2 anchoredPosition = m_Layout.anchoredPosition;
			anchoredPosition.y = (transform as RectTransform).rect.height * -1;

			m_Layout.anchoredPosition = anchoredPosition;
		}

		#endregion

		#region Private methods

		/// <summary>
		/// 	Generates the text.
		/// </summary>
		private void GenerateText()
		{
			m_CreditTextInstances = new List<Text>();

			using (StringReader reader = new StringReader(m_Credits.text))
			{
				string line;
				while ((line = reader.ReadLine()) != null)
					GenerateCredit(line);
			}

			ResetPosition();
		}

		/// <summary>
		/// 	Generates the credit.
		/// </summary>
		/// <param name="line">Line.</param>
		private void GenerateCredit(string line)
		{
			Text prefab;

			if (line.StartsWith(HEADER_PREFIX))
			{
				prefab = m_CredtHeaderTextPrefab;
				line = line.Substring(1);
			}
			else
				prefab = m_CreditTextPrefab;

			Text instance = ObjectUtils.Instantiate(prefab);
			instance.text = string.Format(line);

			instance.transform.SetParent(m_Layout, false);

			m_CreditTextInstances.Add(instance);
		}

		#endregion
	}
}
