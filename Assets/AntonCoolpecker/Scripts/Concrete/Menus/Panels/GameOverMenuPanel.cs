﻿using AntonCoolpecker.Abstract.Menus.Panels;
using AntonCoolpecker.Abstract.Menus.States;
using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using UnityEngine;

namespace AntonCoolpecker.Abstract.Menus.Panels
{
	public class GameOverMenuPanel : AbstractMenuPanel
	{
		protected override void OnEnable ()
		{
			base.OnEnable ();

			if (!(Cursor.lockState == CursorLockMode.None)) {
				Cursor.lockState = CursorLockMode.None;
				Cursor.visible = true;
			}
		}

		// Update is called once per frame
		void Update ()
		{
			
		}
	}
}
