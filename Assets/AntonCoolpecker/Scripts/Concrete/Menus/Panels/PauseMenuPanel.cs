﻿using AntonCoolpecker.Abstract.Menus.Panels;
using AntonCoolpecker.Abstract.Menus.States;
using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Menus.Panels
{
	/// <summary>
	/// 	Pause menu panel.
	/// </summary>
	public class PauseMenuPanel : AbstractMenuPanel {

		[SerializeField] private AbstractMenuState m_OptionsMenu;

		protected override void Update()
		{
			base.Update();

			if (InputMapping.pause.GetButtonDown())
			{
                OnResumeClick();
            }
		}

		public void OnResumeClick()
        {
            PreviousMenu();
		}

		public void OnOptionsClick()
		{
			NextMenu(m_OptionsMenu);
		}

	}
}
