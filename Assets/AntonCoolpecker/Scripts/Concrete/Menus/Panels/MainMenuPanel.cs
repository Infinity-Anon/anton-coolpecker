using AntonCoolpecker.Abstract.Menus.Panels;
using AntonCoolpecker.Concrete.Menus.States;
using UnityEngine;
using UnityEngine.UI;

namespace AntonCoolpecker.Concrete.Menus.Panels
{
	/// <summary>
	/// 	Main menu panel.
	/// </summary>
	public class MainMenuPanel : AbstractMenuPanel
	{


        /*[SerializeField] private Button m_PlayButton;
		[SerializeField] private Button m_OptionsButton;
		[SerializeField] private Button m_CreditsButton;

		#region Messages

		/// <summary>
		/// 	Called when the component is enabled.
		/// </summary>
		protected override void OnEnable()
		{
			base.OnEnable();

			m_PlayButton.onClick.AddListener(OnPlayButtonClicked);
			m_OptionsButton.onClick.AddListener(OnOptionsButtonClicked);
			m_CreditsButton.onClick.AddListener(OnCreditsButtonClicked);
            
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }

		/// <summary>
		/// 	Called when the component is disabled.
		/// </summary>
		protected override void OnDisable()
		{
			base.OnDisable();

			m_PlayButton.onClick.RemoveListener(OnPlayButtonClicked);
			m_OptionsButton.onClick.RemoveListener(OnOptionsButtonClicked);
			m_CreditsButton.onClick.RemoveListener(OnCreditsButtonClicked);
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// 	Called when the play button is clicked.
		/// </summary>
		private void OnPlayButtonClicked()
		{
			NextMenu(m_LevelSelectMenuState);
		}

		/// <summary>
		/// 	Called when the options button is clicked.
		/// </summary>
		private void OnOptionsButtonClicked()
		{
			NextMenu(m_OptionsMenuState);
		}

		/// <summary>
		/// 	Called when the play button is clicked.
		/// </summary>
		private void OnCreditsButtonClicked()
		{
			NextMenu(m_CreditsMenuState);
		}

		#endregion*/
    }
}
