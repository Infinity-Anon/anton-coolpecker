using AntonCoolpecker.Abstract.Menus.Panels;
using AntonCoolpecker.Concrete.Menus.States;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Menus.Panels
{
	/// <summary>
	/// 	Start menu panel.
	/// </summary>
	public class StartMenuPanel : AbstractMenuPanel
	{
		#region Variables

		[SerializeField] private MainMenuState m_MainMenuState;

		#endregion

		#region Messages

		/// <summary>
		/// 	Called once every frame.
		/// </summary>
		protected override void Update()
		{
			base.Update();

			if (Input.anyKeyDown)
				NextMenu(m_MainMenuState);
		}

		#endregion
	}
}