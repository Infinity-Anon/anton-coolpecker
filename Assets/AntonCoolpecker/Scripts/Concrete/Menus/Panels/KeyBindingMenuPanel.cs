﻿using AntonCoolpecker.Abstract.Menus.Panels;
using AntonCoolpecker.Abstract.Menus.States;
using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Menus.Panels
{
    /// <summary>
    /// 	Keybinding menu panel.
    /// </summary>
    public class KeyBindingMenuPanel : AbstractMenuPanel
    {

    }
}
