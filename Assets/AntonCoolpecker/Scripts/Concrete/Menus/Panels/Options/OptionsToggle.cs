﻿using Hydra.HydraCommon.Abstract;
using Hydra.HydraCommon.EventArguments;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace AntonCoolpecker.Concrete.Menus.Panels.Options
{
	/// <summary>
	/// 	Options toggle represents a UI toggle button with title and value labels.
	/// </summary>
	public class OptionsToggle
	{
		#region Variables

		public event EventHandler<EventArg<bool>> onToggleValueChanged;

		[SerializeField] private string m_TitleLabel;
		[SerializeField] private Toggle m_Toggle;
		//[SerializeField] private Text m_ValueLabel;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the title.
		/// </summary>
		/// <value>The title.</value>
		public string title { get { return m_TitleLabel; } set { m_TitleLabel = value; } }

		/// <summary>
		/// Gets or sets the toggle value of this button.
		/// </summary>
		/// <value><c>true</c> if toggle.isOn is true; otherwise, <c>false</c>.</value>
		public bool toggleValue { get { return m_Toggle.isOn; } set { m_Toggle.isOn = value; } }

		#endregion

		#region Private Functions

		/// <summary>
		/// 	Subscribe to slider events.
		/// </summary>
		private void Subscribe()
		{
			m_Toggle.onValueChanged.AddListener(OnToggleValueChanged);
		}

		/// <summary>
		/// 	Unsubscribe from slider events.
		/// </summary>
		private void Unsubscribe()
		{
			m_Toggle.onValueChanged.RemoveListener(OnToggleValueChanged);
		}

		/// <summary>
		/// 	Called when the slider value changes.
		/// </summary>
		/// <param name="newValue">New value.</param>
		private void OnToggleValueChanged(bool newValue)
		{
			EventHandler<EventArg<bool>> handler = onToggleValueChanged;
			if (handler != null)
				handler(this, new EventArg<bool>(newValue));
		}

		#endregion
	}
}