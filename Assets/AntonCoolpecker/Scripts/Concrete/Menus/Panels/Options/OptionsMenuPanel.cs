﻿using AntonCoolpecker.Abstract.Menus.Panels;
using AntonCoolpecker.Concrete.Configuration;
using AntonCoolpecker.Concrete.Utils.UI;
using Hydra.HydraCommon.EventArguments;
using Hydra.HydraCommon.Utils;
using System;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Menus.Panels.Options
{
	//TODO: REMOVE SCRIPT?

	/// <summary>
	/// 	Options menu panel.
	/// </summary>
	public class OptionsMenuPanel : AbstractMenuPanel
	{
		/*[SerializeField] private OptionsSlider m_OptionsSliderPrefab;
		[SerializeField] private OptionsToggle m_OptionsTogglePrefab;
		[SerializeField] private RectTransform m_Layout;
		[SerializeField] private ButtonWrapper m_BackButton;

		#region Messages

		/// <summary>
		/// 	Called when the component is enabled.
		/// </summary>
		protected override void OnEnable()
		{
			base.OnEnable();
            
			if (m_Layout.childCount == 0)
				PopulateOptions();

			m_BackButton.onClickedCallback += OnBackButtonClicked;
		}

		/// <summary>
		/// 	Called when the component is disabled.
		/// </summary>
		protected override void OnDisable()
		{
			base.OnDisable();

			m_BackButton.onClickedCallback -= OnBackButtonClicked;
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// 	Populates the options.
		/// </summary>
		private void PopulateOptions()
		{
			//*CAMERA VALUES
			float min = OptionsDefaults.instance.cameraDefaults.sensitivityMin;
			float max = OptionsDefaults.instance.cameraDefaults.sensitivityMax;

			float horizontal = Configuration.Options.cameraHorizontalSensitivity;
			float vertical = Configuration.Options.cameraVerticalSensitivity;

			bool horizontalInvert = Configuration.Options.cameraHorizontalInverted;
			bool verticalInvert = Configuration.Options.cameraVerticalInverted;

			BuildSlider("Camera Horizontal Sensitivity", min, max, horizontal, OnCameraHorizontalSliderChanged);
			BuildSlider("Camera Vertical Sensitivity", min, max, vertical, OnCameraVerticalSliderChanged);
			BuildToggle("Camera Inverted Horizontal", horizontalInvert, OnCameraHorizontalInvertedToggleChanged);
			BuildToggle("Camera Inverted Vertical", verticalInvert, OnCameraVerticalInvertedToggleChanged);

			//*AUDIO VALUES
			float musicMin = OptionsDefaults.instance.audioDefaults.musicVolMin;
			float musicMax = OptionsDefaults.instance.audioDefaults.musicVolMax;
			float musicVol = Configuration.Options.musicVolume;

			float soundFXMin = OptionsDefaults.instance.audioDefaults.soundFXVolMin;
			float soundFXMax = OptionsDefaults.instance.audioDefaults.soundFXVolMax;
			float soundFXVol = Configuration.Options.soundFXVolume;

			float dialogueMin = OptionsDefaults.instance.audioDefaults.dialogueVolMin;
			float dialogueMax = OptionsDefaults.instance.audioDefaults.dialogueVolMax;
			float dialogueVol = Configuration.Options.dialogueVolume;

			BuildSlider("Music Volume", musicMin, musicMax, musicVol, OnMusicVolChanged);
			BuildSlider("Sound FX Volume", soundFXMin, soundFXMax, soundFXVol, OnSoundFXVolChanged);
			BuildSlider("Dialogue Volume", dialogueMin, dialogueMax, dialogueVol, OnDialogueVolChanged);
		}

		/// <summary>
		/// 	Builds a slider.
		/// </summary>
		/// <returns>The slider.</returns>
		/// <param name="name">Name.</param>
		/// <param name="min">Minimum.</param>
		/// <param name="max">Max.</param>
		/// <param name="sliderValue">Slider value.</param>
		/// <param name="callback">Callback.</param>
		private OptionsSlider BuildSlider(string name, float min, float max, float sliderValue,
										  EventHandler<EventArg<float>> callback)
		{
			OptionsSlider instance = new OptionsSlider();

			instance.title = name;
			instance.min = min;
			instance.max = max;
			instance.sliderValue = sliderValue;

			instance.onSliderValueChanged += callback;

			return instance;
		}

		/// <summary>
		/// 	Builds a toggle.
		/// </summary>
		/// <returns>The toggle.</returns>
		/// <param name="name">Name.</param>
		/// <param name="toggle">Toggle value.</param>
		/// <param name="callback">Callback.</param>
		private OptionsToggle BuildToggle(string name, bool toggleValue, EventHandler<EventArg<bool>> callback)
		{
			OptionsToggle instance = new OptionsToggle();

            instance.title = name;
			instance.toggleValue = toggleValue;

			instance.onToggleValueChanged += callback;

			return instance;
		}

		//*CAMERA FUNCTIONS
		private void OnCameraHorizontalSliderChanged(object sender, EventArg<float> eventArgs)
		{
			Configuration.Options.cameraHorizontalSensitivity = eventArgs.data;
		}

		private void OnCameraVerticalSliderChanged(object sender, EventArg<float> eventArgs)
		{
			Configuration.Options.cameraVerticalSensitivity = eventArgs.data;
		}

		private void OnCameraHorizontalInvertedToggleChanged(object sender, EventArg<bool> eventArgs)
		{
			Configuration.Options.cameraHorizontalInverted = eventArgs.data;
		}

		private void OnCameraVerticalInvertedToggleChanged(object sender, EventArg<bool> eventArgs)
		{
			Configuration.Options.cameraVerticalInverted = eventArgs.data;
		}

		//*AUDIO FUNCTIONS
		private void OnMusicVolChanged(object sender, EventArg<float> eventArgs)
		{
			Configuration.Options.musicVolume = eventArgs.data;
		}

		private void OnSoundFXVolChanged(object sender, EventArg<float> eventArgs)
		{
			Configuration.Options.soundFXVolume = eventArgs.data;
		}

		private void OnDialogueVolChanged(object sender, EventArg<float> eventArgs)
		{
			Configuration.Options.dialogueVolume = eventArgs.data;
		}

		/// <summary>
		/// 	Called when the back button is clicked.
		/// </summary>
		private void OnBackButtonClicked(object sender, EventArgs eventArgs)
		{
			Configuration.Options.Save();

			PreviousMenu();
		}

		#endregion*/
	}
}