﻿using Hydra.HydraCommon.Abstract;
using Hydra.HydraCommon.EventArguments;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace AntonCoolpecker.Concrete.Menus.Panels.Options
{
	/// <summary>
	/// 	Options slider represents a UI slider with title and value labels.
	/// </summary>
	public class OptionsSlider
	{
		#region Variables

		public event EventHandler<EventArg<float>> onSliderValueChanged;

		[SerializeField] private string m_TitleLabel;
		[SerializeField] private Slider m_Slider;
		[SerializeField] private string m_ValueLabel;

		public string title { get { return m_TitleLabel; } set { m_TitleLabel = value; } }
		public float min { get { return m_Slider.minValue; } set { m_Slider.minValue = value; } }
		public float max { get { return m_Slider.maxValue; } set { m_Slider.maxValue = value; } }
		public float sliderValue { get { return m_Slider.value; } set { m_Slider.value = value; } }

		#endregion

		#region Private Methods

		/// <summary>
		/// 	Called when the slider value changes.
		/// </summary>
		/// <param name="newValue">New value.</param>
		private void OnSliderValueChanged(float newValue)
		{
			m_ValueLabel = newValue.ToString("0.00");

			EventHandler<EventArg<float>> handler = onSliderValueChanged;
			if (handler != null)
				handler(this, new EventArg<float>(newValue));
		}

		#endregion
	}
}