﻿using AntonCoolpecker.Abstract.Menus.Panels;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Menus.Panels
{
	/// <summary>
	/// 	Loading menu panel.
	/// </summary>
	public class LoadingMenuPanel : AbstractMenuPanel
	{
		[SerializeField] private RectTransform m_LoadingIcon;
		[SerializeField] private float m_SpinSpeed;

		/// <summary>
		/// 	Called once every frame.
		/// </summary>
		protected override void Update()
		{
			base.Update();

			m_LoadingIcon.Rotate(0.0f, 0.0f, m_SpinSpeed * Time.deltaTime * -1.0f);
		}
	}
}
