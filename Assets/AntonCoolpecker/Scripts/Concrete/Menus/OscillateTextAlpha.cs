﻿using Hydra.HydraCommon.Abstract;
using Hydra.HydraCommon.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace AntonCoolpecker.Concrete.Menus
{
	/// <summary>
	/// 	Oscillate the alpha of a Text component.
	/// </summary>
	[RequireComponent(typeof(Text))]
	public class OscillateTextAlpha : HydraMonoBehaviour
	{
		#region Variables

		[SerializeField] private float m_Speed = 1.0f; //Oscillation speed
		[SerializeField] private float m_Min = 0.0f; //Minimum text alpha
		[SerializeField] private float m_Max = 1.0f; //Maximum text alpha

		private Text m_CachedText;

		#endregion

		#region Properties

		/// <summary>
		/// 	Gets the text.
		/// </summary>
		/// <value>The text.</value>
		public Text text { get { return m_CachedText ?? (m_CachedText = GetComponent<Text>()); } }

		#endregion

		#region Override Functions

		/// <summary>
		/// 	Called once every frame.
		/// </summary>
		protected override void Update()
		{
			base.Update();

			float oscillate = Mathf.Cos(Time.time * m_Speed);
			oscillate = HydraMathUtils.MapRange(-1.0f, 1.0f, m_Min, m_Max, oscillate);

			Color color = text.color;
			color.a = oscillate;

			text.color = color;
		}

		#endregion
	}
}