using UnityEngine;

namespace AntonCoolpecker.Concrete.Menus.HUD
{
	/// <summary>
	/// Two sprite switcher - Switch between two sprites.
	/// </summary>
	public class TwoSpriteSwitcher : SpriteSwitcher
	{
		#region Variables

		[SerializeField] private Sprite m_Sprite0;
		[SerializeField] private Sprite m_Sprite1;

		#endregion

		#region Methods

		/// <summary>
		/// 	Swaps the sprite.
		/// </summary>
		public void SwapSprite()
		{
			image.sprite = (image.sprite == m_Sprite0) ? m_Sprite1 : m_Sprite0;
		}

		#endregion
	}
}