﻿using AntonCoolpecker.Concrete.Enemies;
using AntonCoolpecker.Concrete.Menus.HUD;
using AntonCoolpecker.Concrete.Utils;
using System.Collections;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Menus.HUD
{
	/// <summary>
	/// Boss health HUD element.
	/// </summary>
    public class BossHealthHUDElement : HealthHUDElement
    {
		#region Variables

        private int m_MaxHealth; //Max health for the boss
        private int m_PortraitState = 0; //Potrait state of the boss

		#endregion

		#region Override Functions

		/// <summary>
		/// Called when setting up BossHealthHUDElements
		/// </summary>
		/// <param name="toasts">Toasts.</param>
		/// <param name="digits">Digits.</param>
		/// <param name="portraits">Portraits.</param>
		/// <param name="hideTransTime">Hide trans time.</param>
		/// <param name="appearTransTime">Appear trans time.</param>
		/// <param name="offset">Offset.</param>
		/// <param name="PulseHeartRegularTime">Pulse heart regular time.</param>
		/// <param name="PulseHeartBigTime">Pulse heart big time.</param>
		/// <param name="MaxHitTime">Max hit time.</param>
        public override void Set(Texture2D[] toasts, HUDDigit digits, Texture2D[] portraits, float hideTransTime, float appearTransTime, Vector2 offset, float PulseHeartRegularTime, float PulseHeartBigTime, float MaxHitTime)
        {
            ySlot = 8;
            displayTime = Mathf.Infinity;
            m_Toasts = toasts;
            m_Portraits = portraits;
            transitionHideTime = hideTransTime;
            transitionAppearTime = appearTransTime;
            m_Ones = digits;
            m_offset = offset;

            m_HitTimer = new Timer();
            m_HitTimer.maxTime = MaxHitTime;
            m_HitTimer.End();

            m_HeartTimer = 0;
            m_HeartTimerMax = 0.2f;

            m_PulseHeartRegularTime = PulseHeartRegularTime;
            m_PulseHeartBigTime = PulseHeartBigTime;
        }

		/// <summary>
		/// Called whenever the boss's heath is checked.
		/// </summary>
        public override void CheckHealthLevels()
        {
            if (getCurrentBoss() == null)
                return;

            if (getCurrentBoss().health <= 1)
                m_PortraitState = 4;
            else if (getCurrentBoss().health <= m_MaxHealth * 0.25f)
                m_PortraitState = 3;
            else if (getCurrentBoss().health <= m_MaxHealth * 0.5f)
                m_PortraitState = 2;
            else if (getCurrentBoss().health <= m_MaxHealth * 0.75f)
                m_PortraitState = 1;
            else
                m_PortraitState = 0;

            if (getCurrentBoss().health < m_MaxHealth/2f)
            {
                m_HeartTimer = 0;
                m_ThumpingHeart = true;
            }
            else
            {
                m_ThumpingHeart = false;
                m_PulseHeart = false;
            }
        }

        /// <summary>
        /// Sets max helath of the boss.
        /// </summary>
        public override void SetMaxHealth()
        {
            m_MaxHealth = getCurrentBoss().health;
            m_DisplayRects = new Rect[getCurrentBoss().health + 2];
            CheckHealthLevels();
            setNumberRects();
        }
			
        /// <summary>
        /// Called when damage is taken.
        /// </summary>
        public override void TakeDamage()
        {
            m_HitTimer.Reset();
            CheckHealthLevels();
        }
			
        /// <summary>
        /// Called multiple times an update. Used to draw 2D images on the screen.
        /// </summary>
        public override void OnGUI()
        {
            if (getCurrentBoss() == null)
                return;

            Texture2D toDisplay = Texture2D.whiteTexture;
            GUI.color = Color.white;

            for (int i = m_MaxHealth+1; i >= getCurrentBoss().health + 2; i--)
            {
                GUI.DrawTexture(m_DisplayRects[i], m_Toasts[2]);
            }

            if (m_HitTimer.complete)
                GUI.color = new Color(0.25f, 0.25f, 0.25f, 1f);
            else
                GUI.color = new Color(1, 0, 0, 0.5f);

            GUI.DrawTexture(m_DisplayRects[0], m_Portraits[2]);

            GUI.color = Color.white;

            if (m_HitTimer.complete)
                GUI.DrawTextureWithTexCoords(m_DisplayRects[1], getCurrentBoss().bossPortrait, getCurrentBoss().bossPortraitPullPoints[m_PortraitState]);
            else
                GUI.DrawTextureWithTexCoords(m_DisplayRects[1], getCurrentBoss().bossPortrait, getCurrentBoss().bossPortraitPullPoints[getCurrentBoss().bossPortraitPullPoints.Length-1]);

            GUI.color = Color.white;

            for (int i = 2; i < getCurrentBoss().health + 2; i++)
            {
                toDisplay = m_Toasts[m_PulseHeart ? 1 : 0];
                GUI.DrawTexture(m_DisplayRects[i], toDisplay);
            }
        }

        /// <summary>
        /// Sets the current position of the Display Rects.
        /// </summary>
        protected override void setNumberRects()
        {
            m_DisplayRects[0] = new Rect(m_offset.x * Screen.height * 0.01f,
                Screen.height * 0.1f * ySlot + m_offset.y * Screen.height * 0.01f,
                Screen.height * 1.744f * 0.16f,
                Screen.height * 0.16f);

            m_DisplayRects[1] = new Rect(m_offset.x * Screen.height * 0.01f + Screen.height * 0.05952f,
               Screen.height * 0.1f * ySlot + m_offset.y * Screen.height * 0.01f,
               Screen.height * 0.16f,
               Screen.height * 0.16f);

            for (int i = 2; i < m_MaxHealth + 2; i++)
            {
                if (i % 2 == 1) //Top
                {
                    m_DisplayRects[i] = (new Rect(Screen.height * 0.33f + (Screen.height * 0.04f * i) + m_offset.x * Screen.height * 0.01f,
                        Screen.height * 0.002f + Screen.height * 0.04f + Screen.height * 0.1f * ySlot - Screen.height * 0.02f + m_offset.y * Screen.height * 0.01f,
                        Screen.height * 0.1f,
                        Screen.height * 0.1f));
                }
                else //Bottom
                {
                    m_DisplayRects[i] = (new Rect(Screen.height * 0.33f + (Screen.height * 0.04f * i) + m_offset.x * Screen.height * 0.01f,
                        Screen.height * 0.002f + Screen.height * 0.04f + Screen.height * 0.1f * ySlot + Screen.height * 0.02f + m_offset.y * Screen.height * 0.01f,
                        Screen.height * 0.1f,
                        Screen.height * 0.1f));
                }
            }
            
            if (m_ThumpingHeart)
            {
                if (m_HeartTimer >= m_HeartTimerMax)
                {
                    m_PulseHeart = !m_PulseHeart;

                    if (m_PulseHeart)
                        m_HeartTimerMax = m_PulseHeartBigTime;
                    else
                        m_HeartTimerMax = m_PulseHeartRegularTime;

                    m_HeartTimer = 0;
                }

                m_HeartTimer += Time.deltaTime;
            }
        }

		#endregion

		#region Virtual Methods

		/// <summary>
		/// Gets the current boss.
		/// </summary>
		/// <returns>The current boss.</returns>
		protected virtual BossController getCurrentBoss()
		{
			return PlayerStatistics.currentBoss;
		}

		#endregion
    }
}