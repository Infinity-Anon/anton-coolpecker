﻿using AntonCoolpecker.Concrete.Utils;
using AntonCoolpecker.Utils;
using Hydra.HydraCommon.Abstract;
using System;
using System.Collections.Generic; //Required for Lists.
using UnityEngine;

namespace AntonCoolpecker.Concrete.Menus.HUD
{
	/// <summary>
	/// 	HUD element.
	/// </summary>
	public class HUDElement
	{
		#region Variables

        [SerializeField] private int[] m_DisplayNumber; //Holds each digit of the number of held items in each slot

		[SerializeField] protected HUDDigit m_Ones; //With the changes, only one of these is required
		[SerializeField] public AnimatingIcon itemIcon; //Image of the item type

		[SerializeField] private Interpolater m_Interpolater;

        public int ySlot; //HUD "slot" which determines how low it is on the Y-axis of the screen
        public float xHide; //How far off the X-axis the element needs to slide to be completely hidden
        public float xSlide; //The current slide position on the X-axis

        public bool toHide; //Should the HUD element be hiding?
        public float displayTime; //How long in seconds the HUDElement is visible before it begins to retract

        public float transitionAppearTime = 0.25f; //How long in seconds it takes the HUDElement to reach its display position
        public float transitionHideTime = 0.75f; //How long in seconds it takes the HUDElement to hide

        private float delayTimer; //Counter to count how long the HUD element has been displayed

        protected Vector2 m_offset;

		private List<Rect> numberRects = new List<Rect>();

		#endregion

		#region Virtual Functions

		/// <summary>
		/// 	Called when the component is enabled.
		/// </summary>
		public virtual void OnEnable()
		{
			if (m_Interpolater == null)
				m_Interpolater = new Interpolater(Interpolater.Interpolant.ReverseQuadratic);
		}

        /// <summary>
        /// 	Called once every frame.
        /// </summary>
        public virtual void Update()
		{
            if (itemIcon != null)
                itemIcon.UpdateFrame();

            if (toHide)
            {
                if (xSlide > xHide)
                {
                    xSlide = xHide;
                }
                else if (xSlide != xHide)
                {
                    xSlide += Time.deltaTime * xHide / transitionHideTime;
                }
            }
            else
            {
                if (xSlide < 0)
                {
                    xSlide = 0;
                }
                else if (xSlide != 0)
                {
                    xSlide -= Time.deltaTime * xHide / transitionAppearTime;
                }
                else
                {
                    delayTimer += Time.deltaTime;

                    if (delayTimer >= displayTime)
                    {
                        toHide = true;
                        delayTimer = 0;
                    }
                }
            }

            setNumberRects();
		}

        /// <summary>
        /// Allows for drawing things directly to the screen.
        /// </summary>
        public virtual void OnGUI()
        {
            for (int i = 0; i < numberRects.Count; i++)
            {
                if (i == 0)
                {
                    if (itemIcon != null)
                    {
                        itemIcon.Draw(numberRects[i]);
                    }
                    else
                    {
                        GUI.DrawTexture(numberRects[i], Texture2D.whiteTexture);
                    }
                }
                else
                {
                    GUI.color = Color.white;
                    Rect toGet = m_Ones.GetDigit(m_DisplayNumber[i-1], HUDDigit.TextColor.Yellow);
                    GUI.DrawTextureWithTexCoords(numberRects[i], m_Ones.m_DigitSpriteTexture, toGet);
                }
            }
        }
			
        /// <summary>
        /// Updates the rects in the numberRects lists
        /// </summary>
        protected virtual void setNumberRects()
        {
            numberRects.Clear();

            numberRects.Add(new Rect(Screen.width + xSlide + (-m_offset.x * Screen.height * 0.01f) - Screen.height * 0.1f,
                Screen.height * 0.025f + Screen.height * 0.1f * ySlot + Screen.height * 0.01f * m_offset.y,
                Screen.height * 0.1f, 
				Screen.height * 0.1f));
			
            for (int i = m_DisplayNumber.Length - 1 ; i >= 0; i--)
            {
                numberRects.Add(new Rect(Screen.width + xSlide - (Screen.height * 2 * 0.1f) - (Screen.height / 18.75f * i) + (-m_offset.x * Screen.height * 0.01f),
					Screen.height* 0.025f + Screen.height * 0.1f * ySlot+ Screen.height * 0.0125f + Screen.height * 0.01f * m_offset.y,
                    Screen.height * 0.075f, 
					Screen.height * 0.075f));
            }

            xHide = Screen.height * 0.1f * (m_DisplayNumber.Length + 1) + m_offset.x * Screen.height * 0.01f;
        }

		/// <summary>
		/// Called when potraits are swapped.
		/// </summary>
        public virtual void SwapPortraits() {}

		/// <summary>
		/// Called whenever the player's heath is checked.
		/// </summary>
        public virtual void CheckHealthLevels() {}

		/// <summary>
		/// Called whenever the player takes damage.
		/// </summary>
        public virtual void TakeDamage() {}

        #endregion

        #region Public Functions

        /// <summary>
        /// 	Update the HUDDigits to display the appropriate values.
        /// </summary>
        /// <param name="n">The value to be displayed.</param>
        public void SetNumbers(int n)
		{
            toHide = false;
            delayTimer = 0;
            int numberOfDigits = (int)Math.Floor(Math.Log10(n) + 1);
            if (numberOfDigits < 1)
                numberOfDigits = 1;
            m_DisplayNumber = new int[numberOfDigits];

            int tempN = n;
            for(int i = numberOfDigits-1; i >= 0; i--)
            {
                m_DisplayNumber[i] = tempN % 10;
                tempN /= 10;
            }
		}

        /// <summary>
        ///     Forces the element to transition towards display position.
        /// </summary>
        public void ForceShow()
        {
            toHide = false;
            delayTimer = 0;
        }

        /// <summary>
        /// Forces element to instantly hide at hide position.
        /// </summary>
        public void ForceHide()
        {
            toHide = true;
            xSlide = xHide;
        }

		/// <summary>
		/// Used to set the HUD element.
		/// </summary>
		/// <param name="slot">The display height later multiplied by the height of the elements</param>
		/// <param name="_displayTime">How long the element will display in seconds</param>
		/// <param name="digits">Pointer for the scene's HUDDigit. Used to display text.</param>
		/// <param name="image">An AnimatingIcon class which is displayed.</param>
		/// <param name="startingAmount">The amount this element begins tracking with.</param>
		/// <param name="hideTransTime">The amount of time it takes for the element to transition into its hiding position</param>
		/// <param name="appearTransTime">The amount of time it takes for the element to transition into its display position</param>
		/// <param name="offset">Spacing offset for where the object will be displayed (in pixels)</param>
		public void Set(int slot, float _displayTime, HUDDigit digits, AnimatingIcon image, int startingAmount, float hideTransTime, float appearTransTime, Vector2 offset)
		{
			ySlot = slot;
			displayTime = _displayTime;
			m_Ones = digits;
			SetNumbers(startingAmount);
			itemIcon = image;
			transitionHideTime = hideTransTime;
			transitionAppearTime = appearTransTime;
			m_offset = offset;
		}

		#endregion
	}
}