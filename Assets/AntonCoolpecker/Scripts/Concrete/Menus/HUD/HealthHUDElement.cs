using AntonCoolpecker.Concrete.Player;
using AntonCoolpecker.Concrete.Utils;
using Hydra.HydraCommon.Utils;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Menus.HUD
{
	/// <summary>
    /// The HUD Element responsible for Health. There should only be one.
    /// </summary>
	public class HealthHUDElement : HUDElement
    {
		#region Variables

        protected Rect[] m_DisplayRects; //The positions of all display areas.

        [SerializeField] protected Texture2D[] m_Toasts = new Texture2D[2];
        [SerializeField] protected Texture2D[] m_Portraits;

        protected bool m_AntonActive; //Whether Anton of Coolpecker is active.
        protected Rect m_PullCoords; //The coordinates that the current portraits are being pulled from.
        protected Timer m_HitTimer; //Display timer for the "Being Hit" displays.
        protected float m_HeartTimer; //Timer for animated heart
        protected float m_HeartTimerMax; //Max time for animated heart timer
        protected bool m_ThumpingHeart; //Is heart thumping?
        protected bool m_PulseHeart; //Is heart pulsing?

        protected float m_PulseHeartBigTime; //While hearts are thumping, amount of time the hearts remain their normal size.
        protected float m_PulseHeartRegularTime; //While hearts are thumping, amount of time the hearts remain enlarged.

        #endregion

		#region Virtual Functions

        /// <summary>
        /// Setting for HealthHUDElements
        /// </summary>
        public virtual void Set(Texture2D[] toasts, HUDDigit digits, Texture2D[] portraits, float hideTransTime, float appearTransTime, Vector2 offset, float PulseHeartRegularTime, float PulseHeartBigTime, float MaxHitTime)
        {
            ySlot = 0;
            displayTime = 3;
            m_Toasts = toasts;
            m_Portraits = portraits;
            transitionHideTime = hideTransTime;
            transitionAppearTime = appearTransTime;
            m_AntonActive = PlayerSwapper.antonActive;
            m_Ones = digits;
            m_offset = offset;

            m_HitTimer = new Timer();
            m_HitTimer.maxTime = MaxHitTime;
            m_HitTimer.End();

            m_HeartTimer = 0;
            m_HeartTimerMax = 0.2f;

            m_PulseHeartRegularTime = PulseHeartRegularTime;
            m_PulseHeartBigTime = PulseHeartBigTime;

            CheckHealthLevels();
        }

		/// <summary>
        /// Sets the HUD's max displayed health.
        /// </summary>
        public virtual void SetMaxHealth()
        {
            m_DisplayRects = new Rect[PlayerStatistics.PlayerCollectableStatistics.maxHealth + 4];
            CheckHealthLevels();
        }

		#endregion

		#region Override Functions

		/// <summary>
        /// Called when damage is taken.
        /// </summary>
        public override void TakeDamage()
        {
            m_HitTimer.Reset();
            CheckHealthLevels();
        }

		/// <summary>
        /// Swaps the active portrait between Anton and Coolpecker.
        /// </summary>
        public override void SwapPortraits()
        {
            m_AntonActive = PlayerSwapper.antonActive;
            setNumberRects();
        }

		/// <summary>
        /// Changes portraits and activates the heart thumping system when the player is at certain levels of health.
        /// </summary>
        public override void CheckHealthLevels()
        {
            if (PlayerStatistics.PlayerCollectableStatistics[Collectables.CollectableType.Health] <= 1)
                m_PullCoords = new Rect(0.33f, 0, 0.33f, 0.5f);
            else if (PlayerStatistics.PlayerCollectableStatistics[Collectables.CollectableType.Health] <= PlayerStatistics.PlayerCollectableStatistics.maxHealth * 0.25f)
				m_PullCoords = new Rect(0, 0, 0.33f, 0.5f);
            else if (PlayerStatistics.PlayerCollectableStatistics[Collectables.CollectableType.Health] <= PlayerStatistics.PlayerCollectableStatistics.maxHealth * 0.5f)
				m_PullCoords = new Rect(0.66f, 0.5f, 0.33f, 0.5f);
            else if (PlayerStatistics.PlayerCollectableStatistics[Collectables.CollectableType.Health] <= PlayerStatistics.PlayerCollectableStatistics.maxHealth * 0.75f)
				m_PullCoords = new Rect(0.33f, 0.5f, 0.33f, 0.5f);
            else
				m_PullCoords = new Rect(0, 0.5f, 0.33f, 0.5f);

            if (PlayerStatistics.PlayerCollectableStatistics[Collectables.CollectableType.Health] < PlayerStatistics.PlayerCollectableStatistics.maxHealth * 0.5f)
            {
                m_HeartTimer = 0;
                m_ThumpingHeart = true;
            }
            else
            {
                m_ThumpingHeart = false;
                m_PulseHeart = false;
            }
        }

        /// <summary>
        /// Called multiple times an update. Used to draw 2D images on the screen.
        /// </summary>
        public override void OnGUI()
        {
            Texture2D toDisplay = Texture2D.whiteTexture;
            GUI.color = Color.white;

            for (int i = PlayerStatistics.PlayerCollectableStatistics.maxHealth + 3; i >= PlayerStatistics.PlayerCollectableStatistics[Collectables.CollectableType.Health] + 4; i--)
            {
                GUI.DrawTexture(m_DisplayRects[i], m_Toasts[2]);
            }

            if (m_HitTimer.complete)
                GUI.color = new Color(0.25f, 0.25f, 0.25f, 1f);
            else
                GUI.color = new Color(1, 0, 0, 0.5f);

            GUI.DrawTexture(m_DisplayRects[0], m_Portraits[2]);

            if (m_AntonActive)
            {
                    DrawPortrait(1, new Color(0.5f, 0.5f, 0.5f, 0.5f), 1);
                    DrawPortrait(0, Color.white, 2);
            }
            else
            {
                    DrawPortrait(0, new Color(0.5f, 0.5f, 0.5f, 0.5f), 1);
                    DrawPortrait(1, Color.white, 2);
            }

            GUI.color = Color.white;
            Rect toGet = m_Ones.GetDigit(PlayerStatistics.PlayerCollectableStatistics.lives, HUDDigit.TextColor.Yellow);
            GUI.DrawTextureWithTexCoords(m_DisplayRects[3], m_Ones.m_DigitSpriteTexture, toGet);

            for (int i = 4; i < PlayerStatistics.PlayerCollectableStatistics[Collectables.CollectableType.Health] + 4; i++)
            {
                toDisplay = m_Toasts[m_PulseHeart ? 1 : 0];
                GUI.color = Color.white;
                GUI.DrawTexture(m_DisplayRects[i], toDisplay);
            }
        }

		/// <summary>
        /// Sets the current position of the Display Rects.
        /// </summary>
        protected override void setNumberRects()
        {
            m_DisplayRects[0] = new Rect(m_offset.x * Screen.height * 0.01f - Mathf.Pow(12, (xSlide / 5)),
				Screen.height * 0.1f * ySlot + m_offset.y * Screen.height * 0.01f,
				Screen.height * 1.744f * 0.16f,
				Screen.height * 0.16f);

            if (m_AntonActive)
            {
                m_DisplayRects[2] = (new Rect(m_offset.x * Screen.height * 0.01f - Mathf.Pow(12, (xSlide / 5)),
                    Screen.height * 0.025f + Screen.height * 0.1f * ySlot + m_offset.y * Screen.height * 0.01f,
					Screen.height * 0.16f,
					Screen.height * 0.16f));
				
				m_DisplayRects[1] = (new Rect((Screen.height * 0.025f + Screen.height * 0.11f) + m_offset.x * Screen.height * 0.01f - Mathf.Pow(12, (xSlide / 5)),
					Screen.height * 0.025f + Screen.height * 0.1f * ySlot + m_offset.y * Screen.height * 0.01f,
					Screen.height * 0.16f,
					Screen.height * 0.16f));
            }
            else
            {
				m_DisplayRects[1] = (new Rect(m_offset.x * Screen.height * 0.01f - Mathf.Pow(12, (xSlide / 5)),
					Screen.height * 0.025f + Screen.height * 0.1f * ySlot + m_offset.y * Screen.height * 0.01f,
					Screen.height * 0.16f,
					Screen.height * 0.16f));
				
				m_DisplayRects[2] = (new Rect((Screen.height * 0.025f + Screen.height * 0.11f) + m_offset.x * Screen.height * 0.01f - Mathf.Pow(12, (xSlide / 5)),
					Screen.height * 0.025f + Screen.height * 0.1f * ySlot + m_offset.y * Screen.height * 0.01f,
					Screen.height * 0.16f,
					Screen.height * 0.16f));
            }

			m_DisplayRects[3] = (new Rect((Screen.height * 0.025f + Screen.height * 0.2777f) + m_offset.x * Screen.height * 0.01f - Mathf.Pow(12, (xSlide / 5)),
				Screen.height * 0.025f + Screen.height * 0.1f * ySlot + m_offset.y * Screen.height * 0.01f,
                Screen.height * 0.125f,
				Screen.height * 0.125f));

            for (int i = 4; i < PlayerStatistics.PlayerCollectableStatistics.maxHealth + 4; i++)
            {
                if (i % 2 == 1) //Top
                {
                    m_DisplayRects[i] = (new Rect(Screen.height * 0.33f + (Screen.height * 0.04f * i) + m_offset.x * Screen.height * 0.01f,
                        -Mathf.Pow(3, (xSlide - (PlayerStatistics.PlayerCollectableStatistics.maxHealth - (i - 4)))) * Screen.height * 0.002f + Screen.height * 0.04f + Screen.height * 0.1f * ySlot - Screen.height * 0.02f + m_offset.y * Screen.height * 0.01f,
                        Screen.height * 0.1f, 
						Screen.height * 0.1f));
                }
                else //Bottom
                {
                    m_DisplayRects[i] = (new Rect(Screen.height * 0.33f + (Screen.height * 0.04f * i) + m_offset.x * Screen.height * 0.01f,
						-Mathf.Pow(3, (xSlide - (PlayerStatistics.PlayerCollectableStatistics.maxHealth - (i - 4)))) * Screen.height * 0.002f + Screen.height * 0.04f + Screen.height * 0.1f * ySlot + Screen.height * 0.02f + m_offset.y * Screen.height * 0.01f,
						Screen.height * 0.1f, 
						Screen.height * 0.1f));
                }
            }

            xHide = PlayerStatistics.PlayerCollectableStatistics.maxHealth + 5f;
            //xHide = (Screen.height / 10 * (5 + PlayerStatistics.PlayerCollectableStatistics.maxHealth / 2))-m_offset.x * Screen.height / 100;

            if (m_ThumpingHeart)
            {
                if (m_HeartTimer >= m_HeartTimerMax)
                {
                    m_PulseHeart = !m_PulseHeart;

                    if (m_PulseHeart)
						m_HeartTimerMax = m_PulseHeartBigTime;
                    else
						m_HeartTimerMax = m_PulseHeartRegularTime;
					
                    m_HeartTimer = 0;
                }

                m_HeartTimer += Time.deltaTime;
            }
        }

		#endregion

		#region Private Methods

		/// <summary>
		/// Draws the appropriate portrait to the specific rect.
		/// </summary>
		/// <param name="PortraitToUse">0 = Anton. 1 = Coolpecker.</param>
		/// <param name="color">Color of the portrait.</param>
		/// <param name="RectNumber">Rect the portrait is drawn to.</param>
		/// <returns></returns>
		private bool DrawPortrait(int PortraitToUse, Color color, int RectNumber)
		{
			if (m_Portraits[PortraitToUse] != null)
			{
				GUI.color = color;

				if (m_HitTimer.complete)
				{
					GUI.DrawTextureWithTexCoords(m_DisplayRects[RectNumber], m_Portraits[PortraitToUse], m_PullCoords);
				}
				else
				{
					GUI.DrawTextureWithTexCoords(m_DisplayRects[RectNumber], m_Portraits[PortraitToUse], new Rect(0.66f, 0, 0.33f, 0.5f));
				}

				return true;
			}

			return false;
		}

		#endregion
    }
}