using UnityEngine;

namespace AntonCoolpecker.Concrete.Menus.HUD
{
	/// <summary>
	/// HUD checker.
	/// </summary>
	public class HUDChecker : MonoBehaviour
	{
		#region Variables

		public int count;
		public HUDElement element;

		#endregion

		#region Functions

		/// <summary>
		/// Raises the trigger enter event.
		/// </summary>
		public void OnTriggerEnter()
		{
			//element.count = count; Does this class do anything anymore? 
			//No idea. - BrokenProgrammer
			Debug.Log("Collided.");
		}

		#endregion
	}
}