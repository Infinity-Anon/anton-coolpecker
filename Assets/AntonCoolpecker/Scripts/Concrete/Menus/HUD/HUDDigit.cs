﻿using UnityEngine;

namespace AntonCoolpecker.Concrete.Menus.HUD
{
	/// <summary>
	/// A class which holds the sprite texture and returns rect coordinates to display numbers, letters, and symbols.
	/// </summary>
	public class HUDDigit : SpriteSwitcher
	{
		#region Variables

		[SerializeField] public Texture2D m_DigitSpriteTexture; //Reference to texture(s) for the digit sprites

		//An enum which is used to determine the color of the text
        public enum TextColor
        {
            Yellow = 0,
            Blue = 1,
            Orange = 2,
            White = 3
        }

		#endregion

		#region Methods

		/// <summary>
		/// 	Sets the digit.
		/// </summary>
		/// <param name="n">The digit to be displayed.</param>
		public Rect GetDigit(int n, TextColor color)
		{
			if (n >= 0)
            {
                if (n <= 9)
                {
                    if (n < 5)
                    {
                        return new Rect(n * 0.09090909f, 0.95f - ((int)color * 0.25f), 1f * 0.09090909f, 0.05f);
                    }
                    else
                    {
                        return new Rect((n-5) * 0.09090909f, 0.95f - ((int)color * 0.25f + 0.05f), 1f * 0.09090909f, 0.05f);
                    }
                }
                if (n > 31 && n < 48)
                {
                    switch (n)
                    {
                        case 32:
                            return new Rect();//" "
                        case 33:
                            return new Rect(5 * 0.09090909f, 0.95f - ((int)color * 0.25f), 1f * 0.09090909f, 0.05f);//"!"
                        case 34:
                            return new Rect(9 * 0.09090909f, 0.95f - ((int)color * 0.25f + 0.2f), 1f * 0.09090909f, 0.05f);//"""
                        case 35:
                            return new Rect(4 * 0.09090909f, 0.95f - ((int)color * 0.25f + 0.2f), 1f * 0.09090909f, 0.05f);//"#"
                        case 37:
                            return new Rect(7 * 0.09090909f, 0.95f - ((int)color * 0.25f + 0.2f), 1f * 0.09090909f, 0.05f);//"%"
                        case 38:
                            return new Rect(7f * 0.09090909f, 0.95f - ((int)color * 0.25f + 0.05f), 1f * 0.09090909f, 0.05f);//"&"
                        case 39:
                            return new Rect(6 * 0.09090909f, 0.95f - ((int)color * 0.25f + 0.2f), 1f * 0.09090909f, 0.05f);//"'"
                        case 40:
                            return new Rect(8f * 0.09090909f, 0.95f - ((int)color * 0.25f + 0.05f), 1f * 0.09090909f, 0.05f);//"("
                        case 41:
                            return new Rect(9f  * 0.09090909f, 0.95f - ((int)color * 0.25f + 0.05f), 1f  * 0.09090909f, 0.05f);//")"
                        case 42:
                            return new Rect(8  * 0.09090909f, 0.95f - ((int)color * 0.25f + 0.2f), 1f  * 0.09090909f, 0.05f);//"*"
                        case 43:
                            return new Rect(5  * 0.09090909f, 0.95f - ((int)color * 0.25f + 0.2f), 1f  * 0.09090909f, 0.05f);//"+"
                        case 44:
                            return new Rect(8  * 0.09090909f, 0.95f - ((int)color * 0.25f), 1f  * 0.09090909f, 0.05f);//","
                        case 45:
                            return new Rect(5f  * 0.09090909f, 0.95f - ((int)color * 0.25f + 0.05f), 1f  * 0.09090909f, 0.05f);//"-"
                        case 46:
                            return new Rect(7  * 0.09090909f, 0.95f - ((int)color * 0.25f), 1f  * 0.09090909f, 0.05f);//"."
                        case 47:
                            return new Rect(6f  * 0.09090909f, 0.95f - ((int)color * 0.25f + 0.05f), 1f  * 0.09090909f, 0.05f);//"/"

                    }
                }
                if (n == 58)//":"
                {
                    return new Rect(9  * 0.09090909f, 0.95f - ((int)color * 0.25f), 1f  * 0.09090909f, 0.05f);
                }
                if (n == 59)//";"
                {
                    return new Rect(10  * 0.09090909f, 0.95f - ((int)color * 0.25f), 1f  * 0.09090909f, 0.05f);
                }
                if (n == 61)//"="
                {
                    return new Rect(10f  * 0.09090909f, 0.95f - ((int)color * 0.25f + 0.05f), 1f  * 0.09090909f, 0.05f);
                }
                if (n == 95)//"_"
                {
                    return new Rect(5f  * 0.09090909f, 0.95f - ((int)color * 0.25f + 0.05f), 1f  * 0.09090909f, 0.05f);
                }
                if (n < 58)
                {
                    return GetDigit(n - 48, color);
                }
                if (n == 63)//"?"
                {
                    return new Rect(6  * 0.09090909f, 0.95f - ((int)color * 0.25f), 1f  * 0.09090909f, 0.05f);
                }
                if(n > 64)
                {
                    if (n < 76)
                        return new Rect((n - 65)  * 0.09090909f, 0.95f - ((int)color * 0.25f + 0.1f), 1f  * 0.09090909f, 0.05f);
                    else if (n < 87)
                        return new Rect((n - 76)  * 0.09090909f, 0.95f - ((int)color * 0.25f + 0.15f), 1f  * 0.09090909f, 0.05f);
                    else
                        return new Rect((n - 87)  * 0.09090909f, 0.95f - ((int)color * 0.25f + 0.2f), 1f  * 0.09090909f, 0.05f);
                }
            }

            return new Rect();
		}

		#endregion
	}
}