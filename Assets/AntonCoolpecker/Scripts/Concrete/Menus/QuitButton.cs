﻿using Hydra.HydraCommon.Abstract;
using UnityEngine;
using UnityEngine.UI;

namespace AntonCoolpecker.Concrete.Menus
{
	/// <summary>
	/// 	Quits the application when pressed.
	/// </summary>
	//[RequireComponent(typeof(Button))]
	public class QuitButton : AbstractButton
	{
		#region Variables

		public static string WEBPLAYER_QUIT_URL = "anton.anticruft.com";

		#endregion

        #region Private Methods

        /// <summary>
        /// 	Called when the button is clicked.
        /// </summary>
        public override void OnButtonClicked()
		{
			#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
			#elif UNITY_WEBPLAYER
			Application.OpenURL(WEBPLAYER_QUIT_URL);
			#else
			Application.Quit();
			#endif
		}

		#endregion
	}
}