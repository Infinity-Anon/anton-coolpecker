﻿using AntonCoolpecker.Abstract.Menus.States;
using AntonCoolpecker.Concrete.Menus;
using AntonCoolpecker.Concrete.Menus.HUD;
using AntonCoolpecker.Concrete.Menus.States;
using AntonCoolpecker.Concrete.Utils;
using System.Collections;
using UnityEngine;

namespace Assets.AntonCoolpecker.Scripts.Concrete.Menus
{
    /// <summary>
    /// Button that allows traveling from one menu to another
    /// </summary>
    class NextMenuButton : AbstractButton
    {
		#region Variables

        private AbstractMenuState m_ParentMenu;
        private AbstractMenuState m_NextMenu;

		#endregion

		#region Functions

		/// <summary>
		/// Set the specified ParentMenu and NextMenu.
		/// </summary>
		/// <param name="ParentMenu">Parent menu.</param>
		/// <param name="NextMenu">Next menu.</param>
        public void Set(AbstractMenuState ParentMenu, AbstractMenuState NextMenu)
        {
            m_ParentMenu = ParentMenu;
            m_NextMenu = NextMenu;
        }

		#endregion

		#region Override Functions

		/// <summary>
		/// Called when the button has been clicked.
		/// </summary>
        public override void OnButtonClicked()
        {
            // tell the parent menu to go to m_NextMenu
            m_ParentMenu.panelInstance.NextMenu(m_NextMenu);
        }

		/// <summary>
		/// Draw the specified rectangle, boxColor, textColor and digits.
		/// </summary>
		/// <param name="drawRect">Draw rect.</param>
		/// <param name="boxColor">Box color.</param>
		/// <param name="textColor">Text color.</param>
		/// <param name="digits">Digits.</param>
        public override void Draw(Rect drawRect, Color boxColor, HUDDigit.TextColor textColor, HUDDigit digits)
        {
			//DRAW OUT CAPITALIZED TITLE

            GUI.color = Color.white;
            string capsTitle = title.ToUpper();
            for (int i = 0; i < capsTitle.Length; i++)
            {
                Rect doublePos = new Rect(drawRect.x + i * drawRect.width * 0.5f / (capsTitle.Length + 1),
                        drawRect.y + drawRect.height / 2 - drawRect.width / (capsTitle.Length + 1) * 0.25f,
                        drawRect.width / (capsTitle.Length + 1) * 0.5f, drawRect.width / (capsTitle.Length + 1) * 0.5f);
                Rect toGet = digits.GetDigit(capsTitle[i], textColor);
                GUI.DrawTextureWithTexCoords(doublePos, digits.m_DigitSpriteTexture, toGet);
            }
        }

		#endregion
    }
}