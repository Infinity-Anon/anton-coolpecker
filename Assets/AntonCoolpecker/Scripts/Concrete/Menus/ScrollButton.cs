﻿using AntonCoolpecker.Abstract.Menus.States;
using System.Collections;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Menus
{
	/// <summary>
	/// Scrollable button.
	/// </summary>
    public class ScrollButton : AbstractButton
    {
        protected bool holdingScrollBar = false; //Detects whether this button is being held or not.
        protected Vector2 baseMousPos; //Keeps track of basic mouse position.

		/// <summary>
		/// Set a specified scroll area for a scrollable button.
		/// </summary>
		/// <param name="scrollArea">Scroll area.</param>
        public override void Set(Rect scrollArea)
        {
            base.Set(scrollArea);
        }

		/// <summary>
		/// Called when the button has been clicked.
		/// </summary>
        public override void OnButtonClicked()
        {
            holdingScrollBar = true;
            baseMousPos = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);
        }

		/// <summary>
		/// Gets the velocity delta of the computer mouse movement.
		/// </summary>
		/// <returns>The delta.</returns>
        public float getDelta()
        {
            if (holdingScrollBar)
            {
                if (!Input.GetMouseButton(0))
                {
                    holdingScrollBar = false;
                    return 0;
                }
                else
                {
                    Vector2 currentMousPos = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);
                    Vector2 MouseDelta = baseMousPos - currentMousPos;
                    //baseMousPos = currentMousPos;

                    return MouseDelta.y;
                }
            }

            return 0;
        }
    }
}