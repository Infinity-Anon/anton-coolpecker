﻿using AntonCoolpecker.Concrete.Menus;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Menus
{
	/// <summary>
	/// Restart game over button.
	/// </summary>
	public class RestartGameOverButton : AbstractButton
	{
		//TODO: Change this to starting level when it's properly finished.

		#region Variables

		private int levelToLoad;

		#endregion

		#region Override Functions

		/// <summary>
		/// Sets the specified level(Unity scene) for a button.
		/// </summary>
		/// <param name="level">Level.</param>
		public override void Set (int level)
		{
			levelToLoad = level;
			title = "Restart";
		}

		/// <summary>
		/// Called when the button has been clicked.
		/// </summary>
		public override void OnButtonClicked ()
		{
			Application.LoadLevel (levelToLoad);
		}

		#endregion
	}
}