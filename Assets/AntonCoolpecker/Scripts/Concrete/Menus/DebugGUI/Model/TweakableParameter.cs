﻿using System;
using System.Reflection;

namespace AntonCoolpecker.Concrete.Menus.DebugGUI.Model
{
	/// <summary>
	/// Tweakable parameter - Abstract base class for Tweakable Field Parameter class.
	/// </summary>
	public abstract class TweakableParameter
	{
		#region Variables

		protected object owner;
		private string m_Context;

		#endregion

		#region Constructor

		protected TweakableParameter(object owner, string context)
		{
			this.owner = owner;
			this.m_Context = context;
		}

		#endregion

		#region Properties

		/// <summary>
		/// Gets the name.
		/// </summary>
		/// <value>The name.</value>
		public abstract string Name { get; }

		/// <summary>
		/// Gets the context.
		/// </summary>
		/// <value>The context.</value>
		public string Context { get { return m_Context; } }

		/// <summary>
		/// Gets the type.
		/// </summary>
		/// <value>The type.</value>
		public abstract Type Type { get; }

		#endregion

		#region Functions/Methods

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <returns>The value.</returns>
		public abstract object GetValue();

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <returns>The value.</returns>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public abstract T GetValue<T>();

		/// <summary>
		/// Sets the value.
		/// </summary>
		/// <param name="Value">Value.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public abstract void SetValue<T>(T Value);

		#endregion
	}

	/// <summary>
	/// This class pairs a PropertyInfo with a an instance of an object that has that property.
	/// It has somewhat typesafe methods for getting and setting the value.
	/// </summary>
	public class TweakablePropertyParameter : TweakableParameter
	{
		#region Variables

		private readonly PropertyInfo m_Property;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new TweakableParameter.  If property does not belong to the type of owner, an ArgumentException is thrown.
		/// </summary>
		/// <param name="property">The property of owner that is to be tweaked.</param>
		/// <param name="owner">The instance of an object whose property is to be tweaked.</param>
		public TweakablePropertyParameter(PropertyInfo property, object owner, string context) : base(owner, context)
		{
			if (!(owner.GetType().IsSubclassOf(property.DeclaringType) || owner.GetType() == property.DeclaringType))
			{
				throw new ArgumentException(String.Format("The type {0} does not have the property {1}", owner.GetType(),
														  property.Name));
			}

			m_Property = property;
		}

		#endregion

		#region Properties

		/// <summary>
		/// The Name of the parameter.
		/// </summary>
		public override string Name { get { return m_Property.Name; } }

		/// <summary>
		/// Gets the type.
		/// </summary>
		/// <value>The type.</value>
		public override Type Type { get { return m_Property.PropertyType; } }

		#endregion

		#region Public Override Functions/Methods

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <returns>The value.</returns>
		public override object GetValue()
		{
			return m_Property.GetValue(owner, null);
		}

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <returns>The value.</returns>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public override T GetValue<T>()
		{
			if (typeof(T) != m_Property.PropertyType)
			{
				throw new InvalidCastException(String.Format("Cannot get a value of type {0} when the property is of type {1}",
															 typeof(T).Name, m_Property.PropertyType.Name));
			}

			return (T)m_Property.GetValue(owner, null);
		}

		/// <summary>
		/// Sets the value.
		/// </summary>
		/// <param name="Value">Value.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		/// <param name="value">Value.</param>
		public override void SetValue<T>(T value)
		{
			if (typeof(T) != m_Property.PropertyType)
			{
				throw new InvalidCastException(String.Format("Cannot get a value of type {0} when the property is of type {1}",
															 typeof(T).Name, m_Property.PropertyType.Name));
			}

			m_Property.SetValue(owner, value, null);
		}

		#endregion
	}

	/// <summary>
	/// Tweakable field parameter.
	/// </summary>
	public class TweakableFieldParameter : TweakableParameter
	{
		#region Variables

		private readonly FieldInfo m_Field;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new TweakableFieldParameter.  If field does not belong to the type of owner, an ArgumentException is thrown.
		/// </summary>
		/// <param name="field">The field of owner that is to be tweaked.</param>
		/// <param name="owner">The instance of an object whose field is to be tweaked.</param>
		public TweakableFieldParameter(FieldInfo field, object owner, string context) : base(owner, context)
		{
			if (!(owner.GetType().IsSubclassOf(field.DeclaringType) || owner.GetType() == field.DeclaringType))
				throw new ArgumentException(String.Format("The type {0} does not have the field {1}", owner.GetType(), field.Name));

			m_Field = field;
		}

		#endregion

		#region Properties

		/// <summary>
		/// The Name of the parameter.
		/// </summary>
		public override string Name { get { return m_Field.Name; } }

		/// <summary>
		/// The type of the parameter.
		/// </summary>
		/// <value>The type.</value>
		public override Type Type { get { return m_Field.FieldType; } }

		#endregion

		#region Public Override Functions/Methods

		/// <summary>
		/// Gets the value of the parameter field.
		/// </summary>
		/// <returns>The value.</returns>
		public override object GetValue()
		{
			return m_Field.GetValue(owner);
		}

		/// <summary>
		/// Gets the value of the parameter field.
		/// </summary>
		/// <returns>The value.</returns>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public override T GetValue<T>()
		{
			if (typeof(T) != m_Field.FieldType)
			{
				throw new InvalidCastException(String.Format("Cannot get a value of type {0} when the property is of type {1}",
															 typeof(T).Name, m_Field.FieldType.Name));
			}

			return (T)m_Field.GetValue(owner);
		}

		/// <summary>
		/// Sets the value of the parameter field.
		/// </summary>
		/// <param name="Value">Value.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		/// <param name="value">Value.</param>
		public override void SetValue<T>(T value)
		{
			if (typeof(T) != m_Field.FieldType)
			{
				throw new InvalidCastException(String.Format("Cannot get a value of type {0} when the property is of type {1}",
															 typeof(T).Name, m_Field.FieldType.Name));
			}

			m_Field.SetValue(owner, value);
		}

		#endregion
	}
}