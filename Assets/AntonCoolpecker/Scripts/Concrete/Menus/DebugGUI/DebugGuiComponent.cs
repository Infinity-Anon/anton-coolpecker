﻿using AntonCoolpecker.Concrete.Menus.DebugGUI.Model;
using AntonCoolpecker.Concrete.Menus.DebugGUI.View;
using AntonCoolpecker.Concrete.Player;
using AntonCoolpecker.Concrete.Utils.UI;
using AntonCoolpecker.Utils;
using Hydra.HydraCommon.Abstract;
using Hydra.HydraCommon.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

#if UNITY_STANDALONE
using System.IO;
#endif

namespace AntonCoolpecker.Concrete.Menus.DebugGUI
{
	/// <summary>
	/// 	Debug GUI Class.
	/// </summary>
	public class DebugGuiComponent : HydraMonoBehaviour
	{
		#region Variables

		[SerializeField] private string debugString;

		/// <summary>
		/// These objects will be searched for [Tweakable] fields and properties.
		/// </summary>
		[SerializeField] public List<UnityEngine.Object> otherObjects;

		/// <summary>
		/// This is the main container for the debug gui.
		/// </summary>
		public GameObject hScrollBox;

		/// <summary>
		/// Prefab for category column
		/// </summary>
		public GameObject vScrollBoxPrefab;

		/// <summary>
		/// Prefab for float field editor
		/// </summary>
		public GameObject floatFieldPrefab;

        /// <summary>
        /// Prefab for float array field editor
        /// </summary>
        public GameObject floatArrayFieldPrefab;

        /// <summary>
        /// Prefab for bool field editor
        /// </summary>
        public GameObject boolFieldPrefab;

        /// <summary>
        /// Prefab for int field editor
        /// </summary>
        public GameObject intFieldPrefab;

        /// <summary>
        /// Prefab for int array field editor
        /// </summary>
        public GameObject intArrayFieldPrefab;

        /// <summary>
        /// The text area that contains a filename to save and load settings from
        /// </summary>
        public InputField filenameField;

		/// <summary>
		/// When the user clicks this button, the settings are saved to file.
		/// </summary>
		public Button exportButton;

		/// <summary>
		/// When the user clicks this button, the settings are loaded from file.
		/// </summary>
		#if UNITY_WEBGL

		public FileUploadListener webglImportButton;

		#else

		public Button importButton;

		#endif

		// Cache
		private Dictionary<string, List<TweakableParameter>> tweakableParameters;

		/// <summary>
		/// Serialised setting.
		/// </summary>
		private struct SerialisedSetting
		{
			public string category;
			public string context;
			public string name;
			public string value;
		}

		/// <summary>
		/// Token state.
		/// </summary>
		private enum TokenState
		{
			Category,
			Context,
			Name,
			Value
		}

		#endregion

		#region Public Functions/Methods

		/// <summary>
		/// Determines whether this instance is visible.
		/// </summary>
		/// <returns><c>true</c> if this instance is visible; otherwise, <c>false</c>.</returns>
		public bool IsVisible()
		{
			return hScrollBox.activeInHierarchy;
		}

		/// <summary>
		/// Hide this instance.
		/// </summary>
		public void Hide()
		{
			hScrollBox.SetActive(false);
		}

		/// <summary>
		/// Show this instance.
		/// </summary>
		public void Show()
		{
			hScrollBox.SetActive(true);
		}

		/// <summary>
		/// Loads the settings specified by the given string, and applies the values to the parameters.
		/// </summary>
		/// <param name="settings"></param>
		public void LoadSettings(string settings)
		{
			foreach (SerialisedSetting setting in TokeniseSettings(settings))
				LoadSetting(setting);
		}

		/// <summary>
		/// Serialises the current parameters to a string.
		/// </summary>
		/// <returns>The current parameters.</returns>
		public string SaveSettings()
		{
			StringBuilder sb = new StringBuilder();

			foreach (string category in tweakableParameters.Keys)
			{
				foreach (TweakableParameter p in tweakableParameters[category])
				{
					sb.Append(category);
					sb.Append(':');
					sb.Append(p.Context);
					sb.Append('.');
					sb.Append(p.Name);
					sb.Append('=');
					sb.Append(p.GetValue());
					sb.Append(';');
				}
			}

			return sb.ToString();
		}

		/// <summary>
		/// 	Gets the GUI rect including the text field, in screen space
		/// </summary>
		/// <returns>The extended GUI rect, including text field</returns>
		public Rect GetGUIRect()
		{
			RectTransform rectTransform = hScrollBox.GetComponent<RectTransform>();

			return new Rect(rectTransform.rect.xMin + rectTransform.position.x,
							rectTransform.rect.yMin + rectTransform.position.y, rectTransform.rect.width, rectTransform.rect.height);
		}

		#endregion

		#region Messages

		/// <summary>
		/// Called before the first Update.
		/// </summary>
		protected override void Start()
		{
			tweakableParameters = new Dictionary<string, List<TweakableParameter>>();

			PrewarmPlayers();

			foreach (GameObject obj in GameObject.FindObjectsOfType<GameObject>())
			{
				foreach (Component c in obj.GetComponents<Component>())
					AddParameters(c, obj.name);
			}

			PrecoolPlayers();

			foreach (UnityEngine.Object obj in otherObjects)
			{
				if (obj != null)
					AddParameters(obj, obj.name);
			}

			Transform guiContentTransform = hScrollBox.transform.Find("ScrollView/Content");

			foreach (string category in tweakableParameters.Keys)
			{
				GameObject categoryObject = ObjectUtils.Instantiate(vScrollBoxPrefab);

				//Set the category name
				Transform categoryTextTransform = categoryObject.transform.FindChild("Text");
				categoryTextTransform.GetComponent<Text>().text = category;

				//Add the parameters
				Transform categoryContentTransform = categoryObject.transform.Find("ScrollView/Content");
				foreach (TweakableParameter param in tweakableParameters[category])
				{
					GameObject field = MakeField(param);
					field.transform.SetParent(categoryContentTransform);
				}

				categoryObject.transform.SetParent(guiContentTransform);
			}

			exportButton.onClick.AddListener(OnExport);

			#if UNITY_STANDALONE || UNITY_EDITOR
			//importButton.onClick.AddListener(OnImport);
			#elif UNITY_WEBGL
			webglImportButton.onFileSelected.AddListener(OnWebglImport);
			#endif

			filenameField.text = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "/acpsettings.txt";

			hScrollBox.SetActive(false);
		}

		/// <summary>
		/// Called when the component is enabled.
		/// </summary>
		protected override void OnEnable()
		{
			base.OnEnable();

			if (otherObjects == null)
				otherObjects = new List<UnityEngine.Object>();
		}

		#endregion

		#region Private Functions/Methods

		/// <summary>
		/// Ensure that both Anton and Coolpecker already exists. (Normally the inactive character would be lazily created on first switch)
		/// </summary>
		private void PrewarmPlayers()
		{
			PlayerSwapper swapper = GameObject.FindObjectOfType(typeof(PlayerSwapper)) as PlayerSwapper;
			if (swapper != null)
				swapper.PreWarm();
		}

		/// <summary>
		/// Ensure that both Anton and Coolpecker already exists. (Normally the inactive character would be lazily created on first switch)
		/// </summary>
		private void PrecoolPlayers()
		{
			PlayerSwapper swapper = GameObject.FindObjectOfType(typeof(PlayerSwapper)) as PlayerSwapper;
			if (swapper != null)
				swapper.PreCool();
		}

		/// <summary>
		/// Creates an appropriate debug gui editor field, based on the type of the parameter,
		/// and sets the text and value appropriately.
		/// </summary>
		/// <param name="param"></param>
		/// <returns></returns>
		private GameObject MakeField(TweakableParameter param)
		{
			Type paramType = param.Type;

			if (paramType == typeof(float))
			{
				GameObject field = ObjectUtils.Instantiate(floatFieldPrefab);
				Transform textTransform = field.transform.FindChild("Text");
				Text label = textTransform.GetComponent<Text>();
				label.text = param.Context + "." + param.Name.Replace("m_", "");

				Transform editTransform = field.transform.FindChild("FloatEdit");
				label = editTransform.GetComponent<Text>();
				label.text = param.GetValue<float>().ToString();

				FloatParameterView paramComponent = field.GetComponent<FloatParameterView>();
				paramComponent.parameter = param;

				return field;
			}

            if (paramType == typeof(int))
            {
                GameObject field = ObjectUtils.Instantiate(intFieldPrefab);
                Transform textTransform = field.transform.FindChild("Text");
                Text label = textTransform.GetComponent<Text>();
                label.text = param.Context + "." + param.Name.Replace("m_", "");

                Transform editTransform = field.transform.FindChild("IntEdit");
                label = editTransform.GetComponent<Text>();
                label.text = param.GetValue<int>().ToString();

                IntParameterView paramComponent = field.GetComponent<IntParameterView>();
                paramComponent.parameter = param;

                return field;
            }

            if (paramType == typeof(float[]))
            {
                GameObject field = ObjectUtils.Instantiate(floatArrayFieldPrefab);
                Transform textTransform = field.transform.FindChild("Text");
                Text label = textTransform.GetComponent<Text>();
                label.text = param.Context + "." + param.Name.Replace("m_", "");

                Transform editTransform = field.transform.FindChild("FloatEdit");
                label = editTransform.GetComponent<Text>();
                label.text = param.GetValue<float[]>()[0].ToString();

                FloatArrayParameterView paramComponent = field.GetComponent<FloatArrayParameterView>();
                paramComponent.parameter = param;

                return field;
            }
            
            if (paramType == typeof(int[]))
            {
                GameObject field = ObjectUtils.Instantiate(intArrayFieldPrefab);
                Transform textTransform = field.transform.FindChild("Text");
                Text label = textTransform.GetComponent<Text>();
                label.text = param.Context + "." + param.Name.Replace("m_", "");

                Transform editTransform = field.transform.FindChild("IntEdit");
                label = editTransform.GetComponent<Text>();
                label.text = param.GetValue<int[]>()[0].ToString();

                IntArrayParameterView paramComponent = field.GetComponent<IntArrayParameterView>();
                paramComponent.parameter = param;

                return field;
            }

            if (paramType == typeof(bool))
			{
				GameObject field = ObjectUtils.Instantiate(boolFieldPrefab);
				Transform textTransform = field.transform.FindChild("Text");
				Text label = textTransform.GetComponent<Text>();
				label.text = param.Context + "." + param.Name.Replace("m_", "");

				Transform editTransform = field.transform.FindChild("Toggle");
				Toggle toggle = editTransform.GetComponent<Toggle>();
				toggle.isOn = param.GetValue<bool>();

				BoolParameterView paramComponent = field.GetComponent<BoolParameterView>();
				paramComponent.parameter = param;

				return field;
			}
				
			throw new NotImplementedException(string.Format("Type {0} not supported - Variable/Property {1}.", param.Type, param.Name));
		}

		/// <summary>
		/// Adds all the tweakable parameters from the given object to tweakableParameters.
		/// </summary>
		/// <param name="owner">Any object that has tweakable parameters.</param>
		private void AddParameters(object owner, string context)
		{
			PropertyInfo[] properties =
				owner.GetType().GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static |
											  BindingFlags.Instance);
			
			foreach (PropertyInfo prop in properties)
			{
				if (prop.IsDefined(typeof(TweakableAttribute), false))
				{
					TweakableAttribute attribute = prop.GetCustomAttributes(typeof(TweakableAttribute), false)[0] as TweakableAttribute;
					TweakableParameter param = new TweakablePropertyParameter(prop, owner, context);
					AddParameter(param, attribute.category);
				}
			}

			FieldInfo[] fields =
				owner.GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance);
			
			foreach (FieldInfo field in fields)
			{
				if (field.IsDefined(typeof(TweakableAttribute), false))
				{
					TweakableAttribute attribute =
						field.GetCustomAttributes(typeof(TweakableAttribute), false)[0] as TweakableAttribute;
					TweakableParameter param = new TweakableFieldParameter(field, owner, context);
					AddParameter(param, attribute.category);
				}
			}
		}

		/// <summary>
		/// Adds a single tweakable parameter to tweableParameters, handling the creation of a category list properly.
		/// </summary>
		/// <param name="param">The paramter that may be tweaked.</param>
		/// <param name="category">The category of the parameter.</param>
		private void AddParameter(TweakableParameter param, string category)
		{
			if (tweakableParameters.ContainsKey(category))
				tweakableParameters[category].Add(param);
			else
			{
				List<TweakableParameter> l = new List<TweakableParameter>();
				l.Add(param);
				tweakableParameters.Add(category, l);
			}
		}

		/// <summary>
		/// Loads a serialized setting.
		/// </summary>
		/// <param name="setting">Setting.</param>
		private void LoadSetting(SerialisedSetting setting)
		{
			if (tweakableParameters.ContainsKey(setting.category))
			{
				foreach (TweakableParameter param in tweakableParameters[setting.category])
				{
					if (param.Name.Equals(setting.name) && param.Context.Equals(setting.context))
						TrySetParameterValue(param, setting.value);
				}
			}
		}

		/// <summary>
		/// Attempts to parse and set the value of the parameter, according to its type.
		/// If parsing fails or the parameter is an unknown tpye, return false.
		/// </summary>
		/// <param name="param"></param>
		/// <param name="value"></param>
		private static bool TrySetParameterValue(TweakableParameter param, string value)
		{
			float floatValue;
			bool boolValue;

			if (param.Type == typeof(float) && float.TryParse(value, out floatValue))
			{
				param.SetValue(floatValue);
				return true;
			}

			if (param.Type == typeof(bool) && bool.TryParse(value, out boolValue))
			{
				param.SetValue(boolValue);
				return true;
			}

			return false;
		}
			
		/// <summary>
		/// Tokenises the settings.
		/// </summary>
		/// <returns>The settings.</returns>
		/// <param name="settings">Settings.</param>
		private static IEnumerable<SerialisedSetting> TokeniseSettings(string settings)
		{
			int catStart = 0, catEnd = 0, conStart = 0, conEnd = 0, nameStart = 0, nameEnd = 0, valueStart = 0, valueEnd = 0;
			TokenState state = TokenState.Category;
			for (int i = 0; i < settings.Length; i++)
			{
				switch (state)
				{
					case TokenState.Category:
						if (settings[i] == ':')
						{
							catEnd = i;
							state = TokenState.Context;
							conStart = i + 1;
						}
						break;
					case TokenState.Context:
						if (settings[i] == '.')
						{
							conEnd = i;
							state = TokenState.Name;
							nameStart = i + 1;
						}
						break;
					case TokenState.Name:
						if (settings[i] == '=')
						{
							nameEnd = i;
							state = TokenState.Value;
							valueStart = i + 1;
						}
						break;
					case TokenState.Value:
						if (settings[i] == ';')
						{
							valueEnd = i;
							yield return new SerialisedSetting
							{
								category = settings.Substring(catStart, catEnd - catStart),
								context = settings.Substring(conStart, conEnd - conStart),
								name = settings.Substring(nameStart, nameEnd - nameStart),
								value = settings.Substring(valueStart, valueEnd - valueStart)
							};
							state = TokenState.Category;
							catStart = i + 1;
						}
						break;
				}
			}
		}

		/// <summary>
		/// Called when importing settings in DebugGUI
		/// </summary>
		private void OnImport()
		{
			#if UNITY_STANDALONE

			using (TextReader reader = new StreamReader(filenameField.text)) 
			{
				LoadSettings(reader.ReadToEnd());
			}

			#endif
		}

		/// <summary>
		/// Called when importing settings in DebugGUI through WebGL build.
		/// </summary>
		/// <param name="url">URL.</param>
		private void OnWebglImport(string url)
		{
			#if UNITY_WEBGL

			Debug.Log("About to start coroutine");

			StartCoroutine(UploadSettings(url).GetEnumerator());

			Debug.Log("Started coroutine");

			#endif
		}

		/// <summary>
		/// Uploads the imported settings (when using WebGL build).
		/// </summary>
		/// <returns>The settings.</returns>
		/// <param name="url">URL.</param>
		private IEnumerable UploadSettings(string url)
		{
			Debug.Log("About to load settings");

			//WWW loader = new WWW("file:///" + filenameField.text);
			WWW loader = new WWW(url);

			Debug.LogFormat("Downloading {0}", filenameField.text);

			yield return loader;

			Debug.Log("Downloaded settings, about to apply");
			Debug.LogFormat("Loaded {0} characters", loader.text.Length);
			Debug.Log(loader.text);

			if (!String.IsNullOrEmpty(loader.error)) 
			{
				Debug.LogError(loader.error);
			}

			LoadSettings(loader.text);

			Debug.Log("Applied settings");
		}

		/// <summary>
		/// Called when exporting settings in DebugGUI.
		/// </summary>
		private void OnExport()
		{
			#if (UNITY_STANDALONE)

			using (TextWriter writer = new StreamWriter(filenameField.text)) 
			{
				writer.Write(SaveSettings());
				writer.Flush();
			}

			#elif (UNITY_WEBGL)

			WebUtils.DownloadFile(SaveSettings(), "acpsettings.txt");

			#else

			Debug.LogError("DebugGUI export not supported on this platform");

			#endif
		}

		#endregion
	}
}