﻿using AntonCoolpecker.Concrete.Menus.DebugGUI.Model;
using Hydra.HydraCommon.Abstract;
using System;

namespace AntonCoolpecker.Concrete.Menus.DebugGUI.View
{
	/// <summary>
	/// Abstract parameter view.
	/// </summary>
	public class AbstractParameterView<T> : HydraMonoBehaviour
	{
		#region Variables

		private TweakableParameter m_Parameter;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the tweakable parameter.
		/// </summary>
		/// <value>The parameter.</value>
		public TweakableParameter parameter
		{
			get { return m_Parameter; }
			set
			{
				if (value.Type != typeof(T))
					throw new ArgumentException(string.Format("Parameter must be a parameter of type {0}", typeof(T).Name));
				m_Parameter = value;
			}
		}

		#endregion

		#region Functions

		/// <summary>
		/// Called when the value of the parameter has been changed.
		/// </summary>
		/// <param name="value">Value.</param>
		public void OnValueChanged(T value)
		{
			parameter.SetValue(value);
		}

		#endregion
	}
}