﻿using UnityEngine.Events;
using UnityEngine.UI;

namespace AntonCoolpecker.Concrete.Menus.DebugGUI.View
{
	/// <summary>
	/// Component that binds a <c ref="TweakableParameter">TweakableParameter</c> to a numeric InputField.
	/// When the Input field's value is changed, it will update the Parameter, and the value in the InputField will be updated when the OnGUI message is received.
	/// </summary>
	public class FloatParameterView : AbstractParameterView<float>
	{
		#region Variables

		public InputField input;
		public UnityEvent onValueChange;

		#endregion

		#region Override Functions

		/// <summary>
		/// 	Called before the first Update.
		/// </summary>
		protected override void Start()
		{
			base.Start();

			input.onValueChange.AddListener(OnValueChanged);
		}

		/// <summary>
		/// Called once every frame.
		/// </summary>
		protected override void Update()
		{
			base.Update();

			if (parameter != null)
				input.text = parameter.GetValue<float>().ToString();
		}

		#endregion

		#region Public Functions

		/// <summary>
		/// Called when the value of the parameter has been changed.
		/// </summary>
		/// <param name="value">Value.</param>
		public void OnValueChanged(string value)
		{
			float parsedValue;

			if (float.TryParse(value, out parsedValue))
				parameter.SetValue(parsedValue);
			
			onValueChange.Invoke();
		}

		/// <summary>
		/// Raises the plus clicked event.
		/// </summary>
		public void OnPlusClicked()
		{
			float newValue = parameter.GetValue<float>() + 0.1f;
			input.text = newValue.ToString();
		}

		/// <summary>
		/// Raises the minus clicked event.
		/// </summary>
		public void OnMinusClicked()
		{
			float newValue = parameter.GetValue<float>() - 0.1f;
			input.text = newValue.ToString();
		}

		#endregion
	}
}