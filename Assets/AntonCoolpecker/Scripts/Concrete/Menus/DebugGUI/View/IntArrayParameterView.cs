﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace AntonCoolpecker.Concrete.Menus.DebugGUI.View
{
	/// <summary>
	/// Component that binds a <c ref="TweakableParameter">TweakableParameter</c> to numeric InputFields.
	/// When the value of an InputField is changed, it will update the Parameter, and the value in the InputField will be updated when the OnGUI message is received.
	/// </summary>
    public class IntArrayParameterView : AbstractParameterView<int[]>
    {
		#region Variables
		
        public InputField input;
        public UnityEvent onValueChange;
        public Text slotInput;

        private int slotValue;

		#endregion

		#region Override Functions

        /// <summary>
        /// 	Called before the first Update.
        /// </summary>
        protected override void Start()
        {
            base.Start();

            input.onValueChange.AddListener(OnValueChanged);
        }

		/// <summary>
		/// Called once every frame.
		/// </summary>
        protected override void Update()
        {
            base.Update();

            if (parameter != null)
                input.text = parameter.GetValue<int[]>()[slotValue].ToString();
        }

		#endregion

		#region Public Functions

		/// <summary>
		/// Called when the value of the parameter has been changed.
		/// </summary>
		/// <param name="value">Value.</param>
        public void OnValueChanged(string value)
        {
            int parsedValue;

            if (int.TryParse(value, out parsedValue))
            {
                int[] FullValue = parameter.GetValue<int[]>();
                FullValue[slotValue] = parsedValue;
                parameter.SetValue(FullValue);
            }
            //onValueChange.Invoke();
        }

		/// <summary>
		/// Raises the slot value changed event.
		/// </summary>
        public void OnSlotValueChanged()
        {
            if (slotValue >= parameter.GetValue<int[]>().Length)
                slotValue = parameter.GetValue<int[]>().Length - 1;
			
            if (slotValue < 0)
                slotValue = 0;
			
            slotInput.text ="Element: " + (slotValue+1).ToString();
            input.text = parameter.GetValue<int[]>()[slotValue].ToString();
        }

		/// <summary>
		/// Raises the plus clicked event.
		/// </summary>
        public void OnPlusClicked()
        {
            int newValue = parameter.GetValue<int[]>()[slotValue] + 1;
            input.text = newValue.ToString();
        }

		/// <summary>
		/// Raises the minus clicked event.
		/// </summary>
        public void OnMinusClicked()
        {
            int newValue = parameter.GetValue<int[]>()[slotValue] - 1;
            input.text = newValue.ToString();
        }

		/// <summary>
		/// Raises the slot plus clicked event.
		/// </summary>
        public void OnSlotPlusClicked()
        {
            slotValue++;
            OnSlotValueChanged();
        }

		/// <summary>
		/// Raises the slot minus clicked event.
		/// </summary>
        public void OnSlotMinusClicked()
        {
            slotValue--;
            OnSlotValueChanged();
        }

		#endregion
    }
}