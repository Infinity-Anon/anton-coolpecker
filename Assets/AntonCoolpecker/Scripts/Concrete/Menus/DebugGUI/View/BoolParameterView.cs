﻿using UnityEngine.UI;

namespace AntonCoolpecker.Concrete.Menus.DebugGUI.View
{
	/// <summary>
	/// Component that binds a <c ref="TweakableParameter">TweakableParameter</c> to a numeric InputField.
	/// When the Input field's value is changed, it will update the Parameter, and the value in the InputField will be updated when the OnGUI message is received.
	/// </summary>
	public class BoolParameterView : AbstractParameterView<bool>
	{
		#region Variables

		public Toggle toggle;

		#endregion

		#region Override Functions

		/// <summary>
		/// 	Called before the first Update.
		/// </summary>
		protected override void Start()
		{
			base.Start();

			toggle.onValueChanged.AddListener(OnValueChanged);
		}

		/// <summary>
		/// Called once every frame.
		/// </summary>
		protected override void Update()
		{
			base.Update();

			if (parameter != null)
				toggle.isOn = parameter.GetValue<bool>();
		}

		#endregion
	}
}