﻿using AntonCoolpecker.Abstract.Menus.States;
using AntonCoolpecker.Concrete.Menus.HUD;
using Hydra.HydraCommon.Abstract;
using Hydra.HydraCommon.EventArguments;
using System;
using System.Collections;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Menus
{
	/// <summary>
	/// Option slider button.
	/// </summary>
    public class OptionSliderButton : AbstractButton
    {
		#region Variables

        public event EventHandler<EventArg<float>> onSliderValueChanged;

        public float min; //The minimum slider value of this button
        public float max; //The maximum slider value of this button
        public float sliderValue; //The current slider value of this button

		private Rect m_Bar; //Slider bar rectangle

        private string m_DisplayValue; //Text displaying value of this slider button
		private bool m_HoldingScrollBar; //Detects whether the scroll bar is held or not
		private Vector2 m_BaseMousePos; //Keeps track of basic mouse position

		#endregion

		#region Functions/Methods

		/// <summary>
		/// Adds the slider value.
		/// </summary>
		/// <param name="value">Value.</param>
        public void addSliderValue(float value) {}

		/// <summary>
		/// Coverts the position of the mouse relative to the slider button to slider button value.
		/// </summary>
		/// <returns>The slider button value.</returns>
        private float MousePosToSliderValue()
        {
            Vector2 currentMousPos = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);
            return min + (currentMousPos.x - m_Bar.x - Screen.width / 10) * (max - min) / m_Bar.width;
        }

		/// <summary>
		/// 	Called when the slider value changes.
		/// </summary>
		/// <param name="newValue">New value.</param>
		private void OnSliderValueChanged(float newValue)
		{
			m_DisplayValue = newValue.ToString("0.00");

			EventHandler<EventArg<float>> handler = onSliderValueChanged;
			if (handler != null)
				handler(this, new EventArg<float>(newValue));
		}

		#endregion

		#region Override Functions/Methods

        /// <summary>
        /// Called when the scroll of a button updates.
        /// </summary>
        public override void UpdateScroll()
        {
            if (m_HoldingScrollBar)
            {
                Vector2 currentMousPos = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);

                if (!Input.GetMouseButton(0))
                {
                    m_HoldingScrollBar = false;
                }
                else
                {
                    sliderValue = MousePosToSliderValue();

                    if (sliderValue < min)
                        sliderValue = min;
                    else if (sliderValue > max)
                        sliderValue = max;
					
                    OnSliderValueChanged(sliderValue);
                }
            }
    	}

		/// <summary>
		/// Draw the specified rectangle, boxColor, textColor and digits.
		/// </summary>
		/// <param name="drawRect">Draw rect.</param>
		/// <param name="boxColor">Box color.</param>
		/// <param name="textColor">Text color.</param>
		/// <param name="digits">Digits.</param>
        public override void Draw(Rect drawRect, Color boxColor, HUDDigit.TextColor textColor, HUDDigit digits)
        {
			// DRAW OUT CAPITALIZED TITLE OF BUTTON
            GUI.color = Color.white;
            string capsTitle = title.ToUpper();

            for (int i = 0; i < capsTitle.Length; i++)
            {
                Rect doublePos = new Rect(drawRect.x + i * drawRect.width * 0.5f / (capsTitle.Length+1),
                        drawRect.y + drawRect.height / 2 - drawRect.width / (capsTitle.Length + 1) * 0.25f,
                        drawRect.width / (capsTitle.Length + 1) * 0.5f, drawRect.width / (capsTitle.Length + 1) * 0.5f);
                Rect toGet = digits.GetDigit(capsTitle[i], textColor);
                GUI.DrawTextureWithTexCoords(doublePos, digits.m_DigitSpriteTexture, toGet);
            }

			// DRAW OUT SLIDER BAR
            m_Bar = new Rect(drawRect.x+drawRect.width/2, drawRect.y+drawRect.height*3/8, drawRect.width*9/20, drawRect.height/4);
            GUI.color = Color.gray;
            GUI.DrawTexture(m_Bar, Texture2D.whiteTexture);

			// DRAW OUT SLIDER BUTTON
            float markerCenter = m_Bar.x + m_Bar.width * (sliderValue - min) / (max - min);
            Rect marker = new Rect(markerCenter - drawRect.height / 20,
                drawRect.y + drawRect.height / 2 - drawRect.height / 4,
                drawRect.height / 10, drawRect.height / 2);
            GUI.color = new Color(0.75f, 0.75f, 0.75f, 1f);
            GUI.DrawTexture(marker, Texture2D.whiteTexture);
        }

		/// <summary>
		/// Called when the button as a whole is updated.
		/// </summary>
		/// <returns>true</returns>
		/// <c>false</c>
		/// <param name="m_CurrentSelectTime">M current select time.</param>
		/// <param name="autoScrolling">If set to <c>true</c> auto scrolling.</param>
		/// <param name="sameDirection">If set to <c>true</c> same direction.</param>
		/// <param name="currentState">Current state.</param>
        public override bool UpdateButton(float m_CurrentSelectTime, bool autoScrolling, bool sameDirection, MenuController.ScrollHoldState currentState)
        {
			//TODO: FIX SMOOTH MOVEMENT FOR SLIDER BUTTONS WHEN USING CONTROLLER/KEYBOARD

            bool resetTime = false;
            bool valueChange = false;

            if (autoScrolling)
            {
                if (m_CurrentSelectTime <= 0)
                {
                    if (currentState == MenuController.ScrollHoldState.Left)
                    {
                        sliderValue -= max / 50f;
                        valueChange = true;
                        resetTime = true;
                    }
                    else if (currentState == MenuController.ScrollHoldState.Right)
                    {
                        sliderValue += max / 50f;
                        valueChange = true;
                        resetTime = true;
                    }
                }
            }
            else if (!sameDirection)
            {
                if (currentState == MenuController.ScrollHoldState.Left)
                {
                    sliderValue -= max / 100f;
                    valueChange = true;
                }
                else if (currentState == MenuController.ScrollHoldState.Right)
                {
                    sliderValue += max / 100f;
                    valueChange = true;
                }
            }

            if (valueChange)
            {
                if (sliderValue > max)
                    sliderValue = max;
                else if (sliderValue < min)
                    sliderValue = min;
				
                OnSliderValueChanged(sliderValue);
            }

            return resetTime;
        }

		/// <summary>
		/// Called when the button has been clicked.
		/// </summary>
        public override void OnButtonClicked()
        {
            if (Input.GetMouseButtonDown(0))
            {
                m_BaseMousePos = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);

                if (m_BaseMousePos.x <= m_Bar.x + m_Bar.width + Screen.width / 10 && m_BaseMousePos.x >= m_Bar.x + Screen.width / 10)
                {
                    m_HoldingScrollBar = true;
                    sliderValue = MousePosToSliderValue();
                    OnSliderValueChanged(sliderValue);
                }
            }
        }

		#endregion
    }
}