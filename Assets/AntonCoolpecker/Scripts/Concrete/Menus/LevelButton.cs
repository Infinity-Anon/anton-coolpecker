﻿using AntonCoolpecker.Concrete.Menus;
using AntonCoolpecker.Concrete.Utils;
using System.Collections;
using UnityEngine;

/// <summary>
/// Level button - Loads a new level(scene) when clicked/pushed.
/// </summary>
public class LevelButton : AbstractButton
{
	#region Variables

    private int levelToLoad;

	#endregion

	#region Override Functions

	/// <summary>
	/// Sets the specified level(Unity scene) for a button.
	/// </summary>
	/// <param name="level">Level.</param>
    public override void Set(int level)
    {
        levelToLoad = level;
        title = string.Format("{0} - {1}", level, SceneUtils.GetSceneName(level));
    }

	/// <summary>
	/// Called when the button has been clicked.
	/// </summary>
    public override void OnButtonClicked()
    {
        Application.LoadLevel(levelToLoad);
    }

	#endregion
}