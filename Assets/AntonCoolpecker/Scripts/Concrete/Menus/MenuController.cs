﻿using AntonCoolpecker.Abstract.Menus.States;
using AntonCoolpecker.Concrete.Menus.HUD;
using AntonCoolpecker.Concrete.Menus.States;
using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using Hydra.HydraCommon.Abstract;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace AntonCoolpecker.Concrete.Menus
{
	/// <summary>
	/// 	MenuController is responsible for setting the current active menu.
	/// </summary>
	[RequireComponent(typeof(Canvas))]
	[RequireComponent(typeof(CanvasScaler))]
	[RequireComponent(typeof(GraphicRaycaster))]
	public class MenuController : SingletonHydraMonoBehaviour<MenuController>
	{
		#region Variables

		[SerializeField] private MenuStateMachine m_StateMachine;
        [SerializeField] private float m_SelectButtonTime = 0.25f; //Amount of time needed to pass between before selected button can be changed by the controller
        [SerializeField] public HUDDigit m_Digits;
        [SerializeField] private float m_SelectAutoScrollTime = 0.25f; //Amount of time needed to pass before auto scroll is active

        private float m_CurrentSelectTime = 0f; //Current amount of time needed to wait.
        private float m_CurrentAutoScrollTime; //Current amount of time needed till auto scroll happens.

        public enum ScrollHoldState
		{
			Center,
			Top,
			Bottom,
			Left,
			Right
		}

        private ScrollHoldState previousHoldState = ScrollHoldState.Center;
        private Stack<AbstractMenuState> m_MenuStack;

        private int previousScreenHeight;
        private int previousScreenWidth;

		#endregion

		#region Properties

        public AbstractMenuState currentMenuState 
		{
			get
            {
                if (m_MenuStack == null)
                    m_MenuStack = new Stack<AbstractMenuState>();
				
                if (m_MenuStack.Count == 0)
                    m_MenuStack.Push(m_StateMachine.initialState);
				
                return m_MenuStack.Peek();
            } 
		}

		#endregion

        #region Protected Functions

        protected override void Awake()
        {
            base.Awake();
            m_CurrentAutoScrollTime = m_SelectAutoScrollTime;
        }

        /// <summary>
        /// 	Called once every frame.
        /// </summary>
        protected override void Update()
		{
			base.Update();

            if (previousScreenWidth != Screen.width || previousScreenHeight != Screen.height)
            {
                currentMenuState.OnScreenResolutionChange(this);
            }

            m_StateMachine.Update(this);

            ScrollHoldState currentHoldState;

            if (InputMapping.verticalInput.GetAxisRaw() >= 0.9f)
                currentHoldState = ScrollHoldState.Top;
            else if (InputMapping.verticalInput.GetAxisRaw() <= -0.9f)
                currentHoldState = ScrollHoldState.Bottom;
            else if (InputMapping.horizontalInput.GetAxisRaw() >= 0.9f)
                currentHoldState = ScrollHoldState.Right;
            else if (InputMapping.horizontalInput.GetAxisRaw() <= -0.9f)
                currentHoldState = ScrollHoldState.Left;
            else
                currentHoldState = ScrollHoldState.Center;

            if (currentHoldState == previousHoldState)
                m_CurrentAutoScrollTime -= Time.deltaTime;
            else
                m_CurrentAutoScrollTime = m_SelectAutoScrollTime;

            if (currentMenuState.UpdateMenu(m_CurrentSelectTime, m_CurrentAutoScrollTime <= 0, currentHoldState == previousHoldState, currentHoldState))
                m_CurrentSelectTime = m_SelectButtonTime;
            else if(m_CurrentSelectTime > 0)
                m_CurrentSelectTime -= Time.deltaTime;

            previousScreenWidth = Screen.width;
            previousScreenHeight = Screen.height;
            previousHoldState = currentHoldState;
		}

		/// <summary>
		/// 	Called when the component is enabled.
		/// </summary>
		protected override void OnEnable()
		{
			base.OnEnable();

			if (m_StateMachine == null)
				m_StateMachine = new MenuStateMachine();

			if (m_MenuStack == null)
				m_MenuStack = new Stack<AbstractMenuState>();

            if (m_MenuStack.Count == 0)
                NextMenu(m_StateMachine.initialState);

            m_StateMachine.OnEnable(this);
		}

		/// <summary>
		/// Raises the GU event.
		/// </summary>
        protected void OnGUI()
        {
            if (currentMenuState != null)
            {
                GUI.depth = currentMenuState.GUIDepth;
                currentMenuState.Draw();
            }
        }

        #endregion

        #region Public Functions

        /// <summary>
        /// 	Returns to the previous menu.
        /// </summary>
        public void PreviousMenu()
		{
			// Remove the current menu.
			m_MenuStack.Pop().OnPop(this);

			// Reapply the previous menu.
			AbstractMenuState next = (m_MenuStack.Count > 0) ? m_MenuStack.Pop() : m_StateMachine.initialState;
			NextMenu(next);
		}

		/// <summary>
		/// 	Proceeds to the next menu.
		/// </summary>
		/// <param name="state">State.</param>
		public void NextMenu(AbstractMenuState state)
		{
			m_MenuStack.Push(state);
			m_StateMachine.SetActiveState(state, this);
			state.OnPush(this);
		}

		#endregion
	}
}