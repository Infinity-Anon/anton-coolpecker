﻿using AntonCoolpecker.Abstract.Menus.States;
using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using AntonCoolpecker.Utils.Time;
using System.Collections.Generic;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Menus.States
{
	/// <summary>
	/// 	Pause menu state.
	/// </summary>
	public class PauseMenuState : AbstractMenuState 
	{
		#region Variables

        [SerializeField] private AbstractMenuState m_OptionsMenu;  
        [SerializeField] private float m_FontSizeHeightPercent; //Percent of the Screen.Width that will be the X of the button cluster
        [SerializeField] private float m_menuButtonYHeightPercent; //Percent of the Screen.Height that will be the X of the button cluster
        //[SerializeField] private float m_menuButtonHeightHeightPercent; //Percent of the Screen.Height that will be the Hieght of each button in the cluster
        //[SerializeField] private float m_menuButtonWidthWidthPercent; //Percent of the Screen.Width that will be the Width of each button in the cluster
        [SerializeField] private float m_menuButtonSpacingHeightPercent; //Percent of the Screen.Height that will be the space between each of the button in the cluster

        private enum ExitMenuState 
		{ 
			DoNothing,
			CloseMenu,
			GoToMainMenu
		}

        ExitMenuState toExitMenu;

		#endregion

		#region Override Functions/Methods

		/// <summary>
		/// Called when the state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
        public override void OnEnter(MonoBehaviour parent)
        {
            base.OnEnter(parent);

            toExitMenu = ExitMenuState.DoNothing;

            if (buttonPos != null)
                buttonPos.Clear();
            else
                buttonPos = new List<Rect>();

            if (m_buttonScripts != null)
                m_buttonScripts.Clear();
            else
                m_buttonScripts = new List<AbstractButton>();

            PopulateScriptList();

            for (int i = 0; i < m_buttonScripts.Count; i++)
            {
                Rect toAdd = new Rect(Screen.width * 0.5f - Screen.height * m_FontSizeHeightPercent * m_buttonText[i].Length / 2, //X
                    Screen.height * m_menuButtonYHeightPercent + (Screen.height * (m_FontSizeHeightPercent + m_menuButtonSpacingHeightPercent)) * i, //Y
                    Screen.height * m_FontSizeHeightPercent * m_buttonText[i].Length, Screen.height * m_FontSizeHeightPercent); //Width, Height
                buttonPos.Add(toAdd);
            }
        }

		/// <summary>
		/// Updates the current menu. Returns true if SelectButtonTime needs to be reset.
		/// </summary>
		/// <param name="m_CurrentSelectTime"></param>
		/// <returns></returns>
		/// <param name="autoScrolling">If set to <c>true</c> auto scrolling.</param>
		/// <param name="sameDirection">If set to <c>true</c> same direction.</param>
		/// <param name="currentState">Current state.</param>
        public override bool UpdateMenu(float m_CurrentSelectTime, bool autoScrolling, bool sameDirection, MenuController.ScrollHoldState currentState)
        {
            if (toExitMenu == ExitMenuState.CloseMenu)
            {
                panelInstance.PreviousMenu();
                toExitMenu = ExitMenuState.DoNothing;
                return false;
            }

            if (toExitMenu == ExitMenuState.GoToMainMenu)
            {
                panelInstance.PreviousMenu();
                m_buttonScripts[3].OnButtonClicked();
                toExitMenu = ExitMenuState.DoNothing;
                return false;
            }

            return base.UpdateMenu(m_CurrentSelectTime, autoScrolling, sameDirection, currentState);
        }

		/// <summary>
		/// Called when the menu is entered from a higher level menu. Not called when the menu becomes active after exiting a
		/// deeper menu.
		/// This is called after OnEnter.
		/// </summary>
		/// <param name="parent">Parent.</param>
        public override void OnPush(UnityEngine.MonoBehaviour parent)
		{
			base.OnEnter(parent);
			GameTime.paused = true;
		}

		/// <summary>
		/// Called when the menu is exiting to return to a higher level menu. Not called when entering a deeper menu from this menu.
		/// This is called before OnExit.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnPop(UnityEngine.MonoBehaviour parent)
		{
			base.OnExit(parent);
			GameTime.paused = false;
		}

		/// <summary>
		/// Draw out the graphics for this menu state.
		/// </summary>
        public override void Draw()
        {
            Rect screenRect = new Rect(0, 0, Screen.width, Screen.height);
            GUI.color = new Color(0, 0, 0, 0.5f);
            GUI.DrawTexture(screenRect, Texture2D.whiteTexture);

            base.Draw();
        }

		/// <summary>
		/// Populates the menu based on the current menu script with buttons, sliders, text, etc.
		/// </summary>
        protected override void PopulateScriptList()
        {
            base.PopulateScriptList();

            m_buttonScripts.Add(null);
            m_buttonScripts.Add(new DebugButton());
            m_buttonScripts.Add(null);
            m_buttonScripts.Add(new MainMenuButton());
            m_buttonScripts.Add(new QuitButton());
        }

		/// <summary>
		/// Called when button N has been hit/clicked in the menu.
		/// </summary>
		/// <param name="n">Button number N.</param>
        protected override void HitButton(int n)
        {
            switch (n)
            {
                case 0:
                    toExitMenu = ExitMenuState.CloseMenu;
                    break;
                case 1:
                    m_buttonScripts[n].OnButtonClicked();
                    toExitMenu = ExitMenuState.CloseMenu;
                    break;
                case 2:
                    panelInstance.NextMenu(m_OptionsMenu);
                    break;
                case 3:
                    toExitMenu = ExitMenuState.GoToMainMenu;
                    break;
                case 4:
                    m_buttonScripts[n].OnButtonClicked();
                    break;
            }
        }

		#endregion
    }
}