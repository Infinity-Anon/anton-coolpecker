﻿using AntonCoolpecker.Abstract.Menus.States;

namespace AntonCoolpecker.Concrete.Menus.States
{
	/// <summary>
	/// 	Loading menu state.
	/// </summary>
	public class LoadingMenuState : AbstractMenuState {}
}