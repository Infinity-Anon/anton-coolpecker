﻿using AntonCoolpecker.Abstract.Menus.States;
using AntonCoolpecker.Concrete.Menus.Panels;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Menus.States
{
	/// <summary>
	/// 	Credits menu state.
	/// </summary>
	public class CreditsMenuState : AbstractMenuState
	{
		#region Override Functions

		/// <summary>
		/// 	Called when the state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnEnter(MonoBehaviour parent)
		{
			base.OnEnter(parent);

			(panelInstance as CreditsMenuPanel).ResetPosition();
		}

		#endregion
	}
}