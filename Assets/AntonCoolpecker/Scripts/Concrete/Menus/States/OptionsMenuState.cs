﻿using AntonCoolpecker.Abstract.Menus.Panels;
using AntonCoolpecker.Abstract.Menus.States;
using AntonCoolpecker.Concrete.Configuration;
using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using AntonCoolpecker.Concrete.Menus.Panels.Options;
using AntonCoolpecker.Concrete.Utils.UI;
using Assets.AntonCoolpecker.Scripts.Concrete.Menus;
using Hydra.HydraCommon.EventArguments;
using Hydra.HydraCommon.Utils;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Menus.States
{
	/// <summary>
	/// 	Options menu state.
	/// </summary>
	public class OptionsMenuState : AbstractScrollableMenuState
    {
		#region Variables

        [SerializeField] private AbstractMenuState m_KeyBindingMenu;
        [SerializeField] private float m_backButtonXWidthPercent; //Percentage of Screen.Width which determines the X of the back button. 
        [SerializeField] private float m_backButtonYHeightPercent; //Percentage of Screen.Height which determines the Y of the back button. 
        [SerializeField] private float m_backButtonWidthWidthPercent; //Percentage of Screen.Width which determines the Width of the back button.
        [SerializeField] private float m_backButtonHeightHeightPercent; //Percentage of Screen.Height which determines the Height of the back button.

		#endregion

        #region Override Functions/Methods

        /// <summary>
        /// 	Called when the component is enabled.
        /// </summary>
        public override void OnEnter(MonoBehaviour parent)
        {
            base.OnEnter(parent);

            if (buttonPos != null)
                buttonPos.Clear();
            else
                buttonPos = new List<Rect>();

            if (m_buttonScripts != null)
                m_buttonScripts.Clear();
            else
                m_buttonScripts = new List<AbstractButton>();

            PopulateScriptList();

            buttonPos.Add(new Rect(Screen.width * m_backButtonXWidthPercent, //X
                Screen.height * m_backButtonYHeightPercent, //Y
                Screen.width * m_backButtonWidthWidthPercent, 
                Screen.height * m_backButtonHeightHeightPercent));

			AbstractButton toAdd = new AbstractButton();

            toAdd.title = m_buttonText[m_buttonScripts.Count];
            m_buttonScripts.Add(toAdd);

        }

        /// <summary>
        /// 	Called when the component is disabled.
        /// </summary>
        public override void OnExit(MonoBehaviour parent)
        {
            base.OnExit(parent);
		}

		/// <summary>
		/// Called when button N has been hit/clicked in the menu.
		/// </summary>
		/// <param name="n">Button number N.</param>
        protected override void HitButton(int n)
        {
            if (n < buttonPos.Count - 1)
                m_buttonScripts[n].OnButtonClicked();
            else
                OnBackButtonClicked();
        }

        /// <summary>
        /// 	Populates the options.
        /// </summary>
        protected override void PopulateScriptList()
        {
			//*CAMERA VALUES
			float min = OptionsDefaults.instance.cameraDefaults.sensitivityMin;
			float max = OptionsDefaults.instance.cameraDefaults.sensitivityMax;

			float horizontal = Configuration.Options.cameraHorizontalSensitivity;
			float vertical = Configuration.Options.cameraVerticalSensitivity;

			bool horizontalInvert = Configuration.Options.cameraHorizontalInverted;
			bool verticalInvert = Configuration.Options.cameraVerticalInverted;

            bool strafeMode = Configuration.Options.cameraStrafeMode;

            float fov_min = OptionsDefaults.instance.cameraDefaults.fieldOfViewMin;
            float fov_max = OptionsDefaults.instance.cameraDefaults.fieldOfViewMax;
            float fov = Configuration.Options.cameraFieldOfView;

            int i = 0;
            m_buttonScripts.Add(BuildSlider(m_buttonText[i], min, max, horizontal, OnCameraHorizontalSliderChanged));
            i++;
            m_buttonScripts.Add(BuildSlider(m_buttonText[i], min, max, vertical, OnCameraVerticalSliderChanged));
            i++;
            m_buttonScripts.Add(BuildToggle(m_buttonText[i], horizontalInvert, OnCameraHorizontalInvertedToggleChanged));
            i++;
            m_buttonScripts.Add(BuildToggle(m_buttonText[i], verticalInvert, OnCameraVerticalInvertedToggleChanged));
            i++;
            m_buttonScripts.Add(BuildToggle(m_buttonText[i], strafeMode, OnCameraStrafeModeToggleChanged));
            i++;
            m_buttonScripts.Add(BuildSlider(m_buttonText[i], fov_min, fov_max, fov, OnCameraFieldOfViewSliderChanged));
            i++;
            
            //*AUDIO VALUES
            float musicMin = OptionsDefaults.instance.audioDefaults.musicVolMin;
			float musicMax = OptionsDefaults.instance.audioDefaults.musicVolMax;
			float musicVol = Configuration.Options.musicVolume;

			float soundFXMin = OptionsDefaults.instance.audioDefaults.soundFXVolMin;
			float soundFXMax = OptionsDefaults.instance.audioDefaults.soundFXVolMax;
			float soundFXVol = Configuration.Options.soundFXVolume;

			float dialogueMin = OptionsDefaults.instance.audioDefaults.dialogueVolMin;
			float dialogueMax = OptionsDefaults.instance.audioDefaults.dialogueVolMax;
			float dialogueVol = Configuration.Options.dialogueVolume;

            m_buttonScripts.Add(BuildSlider(m_buttonText[i], musicMin, musicMax, musicVol, OnMusicVolChanged));
            i++;
            m_buttonScripts.Add(BuildSlider(m_buttonText[i], soundFXMin, soundFXMax, soundFXVol, OnSoundFXVolChanged));
            i++;
            m_buttonScripts.Add(BuildSlider(m_buttonText[i], dialogueMin, dialogueMax, dialogueVol, OnDialogueVolChanged));
            i++;

			// Key Binding Menu, only enable if using keyboard controls (TODO)
			NextMenuButton keyBindingButton = new NextMenuButton ();

			if (InputMapping.GetControlScheme() is KeyboardControlScheme)
				keyBindingButton.Set (this, m_KeyBindingMenu);
			
			Rect toAdd = new Rect (0, //X
				                      (m_MenuRect.height * m_menuSButtonHeightMenuSpaceHeightPercent + m_MenuRect.height * m_menuSButtonSpacingMenuSpaceHeightPercent) * buttonPos.Count + m_ScrollOffset, //Y
				                      m_MenuRect.width * m_menuSButtonWidthMenuSpaceWidthPercent,//Width
				                      m_MenuRect.height * m_menuSButtonHeightMenuSpaceHeightPercent); //Height
			
			buttonPos.Add (toAdd);
			keyBindingButton.title = m_buttonText [m_buttonScripts.Count];
			m_buttonScripts.Add (keyBindingButton);
        }

		#endregion

		#region Private Functions/Methods

		/// <summary>
		/// 	Builds a slider.
		/// </summary>
		/// <returns>The slider.</returns>
		/// <param name="name">Name.</param>
		/// <param name="min">Minimum.</param>
		/// <param name="max">Max.</param>
		/// <param name="sliderValue">Slider value.</param>
		/// <param name="callback">Callback.</param>
		private OptionSliderButton BuildSlider(string name, float min, float max, float sliderValue,
										  EventHandler<EventArg<float>> callback)
		{
			OptionSliderButton instance = new OptionSliderButton();

            Rect toAdd = new Rect(0, //X
                    (m_MenuRect.height * m_menuSButtonHeightMenuSpaceHeightPercent + m_MenuRect.height * m_menuSButtonSpacingMenuSpaceHeightPercent) * buttonPos.Count + m_ScrollOffset, //Y
                    m_MenuRect.width * m_menuSButtonWidthMenuSpaceWidthPercent,//Width
                    m_MenuRect.height * m_menuSButtonHeightMenuSpaceHeightPercent); //Height
            buttonPos.Add(toAdd);

            instance.title = name;
			instance.min = min;
			instance.max = max;
			instance.sliderValue = sliderValue;

			instance.onSliderValueChanged += callback;
			return instance;
		}

		/// <summary>
		/// 	Builds a toggle.
		/// </summary>
		/// <returns>The toggle.</returns>
		/// <param name="name">Name.</param>
		/// <param name="toggle">Toggle value.</param>
		/// <param name="callback">Callback.</param>
		private OptionToggleButton BuildToggle(string name, bool toggleValue, EventHandler<EventArg<bool>> callback)
		{
			OptionToggleButton instance = new OptionToggleButton();

            Rect toAdd = new Rect(0, //X
                    (m_MenuRect.height * m_menuSButtonHeightMenuSpaceHeightPercent + m_MenuRect.height * m_menuSButtonSpacingMenuSpaceHeightPercent) * buttonPos.Count + m_ScrollOffset, //Y
                    m_MenuRect.width * m_menuSButtonWidthMenuSpaceWidthPercent,//Width
                    m_MenuRect.height * m_menuSButtonHeightMenuSpaceHeightPercent); //Height
            buttonPos.Add(toAdd);
            instance.title = name;
			instance.toggleValue = toggleValue;

			instance.onToggleValueChanged += callback;
            return instance;
		}

		//*CAMERA FUNCTIONS

		/// <summary>
		/// Raises the camera horizontal slider changed event.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="eventArgs">Event arguments.</param>
		private void OnCameraHorizontalSliderChanged(object sender, EventArg<float> eventArgs)
		{
			Configuration.Options.cameraHorizontalSensitivity = eventArgs.data;
		}

		/// <summary>
		/// Raises the camera vertical slider changed event.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="eventArgs">Event arguments.</param>
		private void OnCameraVerticalSliderChanged(object sender, EventArg<float> eventArgs)
		{
			Configuration.Options.cameraVerticalSensitivity = eventArgs.data;
		}

		/// <summary>
		/// Raises the camera horizontal inverted toggle changed event.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="eventArgs">Event arguments.</param>
		private void OnCameraHorizontalInvertedToggleChanged(object sender, EventArg<bool> eventArgs)
		{
			Configuration.Options.cameraHorizontalInverted = eventArgs.data;
		}

		/// <summary>
		/// Raises the camera vertical inverted toggle changed event.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="eventArgs">Event arguments.</param>
		private void OnCameraVerticalInvertedToggleChanged(object sender, EventArg<bool> eventArgs)
		{
			Configuration.Options.cameraVerticalInverted = eventArgs.data;
		}

		/// <summary>
		/// Raises the camera strafe mode toggle changed event.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="eventArgs">Event arguments.</param>
		private void OnCameraStrafeModeToggleChanged(object sender, EventArg<bool> eventArgs)
		{
			Configuration.Options.cameraStrafeMode = eventArgs.data;
		}

		/// <summary>
		/// Raises the camera field of view slider changed event.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="eventArgs">Event arguments.</param>
        private void OnCameraFieldOfViewSliderChanged(object sender, EventArg<float> eventArgs)
        {
            Configuration.Options.cameraFieldOfView = eventArgs.data;
        }

        //*AUDIO FUNCTIONS

		/// <summary>
		/// Raises the music vol changed event.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="eventArgs">Event arguments.</param>
        private void OnMusicVolChanged(object sender, EventArg<float> eventArgs)
		{
			Configuration.Options.musicVolume = eventArgs.data;
		}

		/// <summary>
		/// Raises the sound FX vol changed event.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="eventArgs">Event arguments.</param>
		private void OnSoundFXVolChanged(object sender, EventArg<float> eventArgs)
		{
			Configuration.Options.soundFXVolume = eventArgs.data;
		}

		/// <summary>
		/// Raises the dialogue vol changed event.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="eventArgs">Event arguments.</param>
		private void OnDialogueVolChanged(object sender, EventArg<float> eventArgs)
		{
			Configuration.Options.dialogueVolume = eventArgs.data;
        }
        
        /// <summary>
        /// 	Called when the back button is clicked.
        /// </summary>
        private void OnBackButtonClicked()
		{
			Options.Save();

            panelInstance.PreviousMenu();
		}

        #endregion
    }
}