﻿using AntonCoolpecker.Abstract.Menus.Panels;
using AntonCoolpecker.Abstract.Menus.States;
using AntonCoolpecker.Concrete.Configuration;
using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using AntonCoolpecker.Concrete.Menus.Panels.Options;
using AntonCoolpecker.Concrete.Utils.UI;
using Assets.AntonCoolpecker.Scripts.Concrete.Configuration.Controls;
using Assets.AntonCoolpecker.Scripts.Concrete.Menus;
using Hydra.HydraCommon.EventArguments;
using Hydra.HydraCommon.PropertyAttributes.InputAttributes;
using Hydra.HydraCommon.Utils;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Menus.States
{
	/// <summary>
	/// Key binding menu state. Responsible for displaying the current keyboard bindings and allowing them to be rebound.
	/// </summary>
    public class KeyBindingMenuState : AbstractScrollableMenuState
    {
		#region Variables

        [SerializeField] private float m_backButtonXWidthPercent;  //Percentage of Screen.Width which determines the X of the back button. 
        [SerializeField] private float m_backButtonYHeightPercent;  //Percentage of Screen.Height which determines the Y of the back button. 
        [SerializeField] private float m_backButtonWidthWidthPercent;  //Percentage of Screen.Width which determines the Width of the back button.
        [SerializeField] private float m_backButtonHeightHeightPercent;  //Percentage of Screen.Height which determines the Height of the back button.

		private bool m_currentlyBinding = false; //Is the player binding a key?
		private int m_indexKeyBeingBound; //The current key button in the menu being re-binded
		private bool m_jumpJustRebinded = false; //Prevents issue when rebinding the jump input, which is used as "confirm" in menus

		#endregion

        #region Messages

		/// <summary>
		/// Sets up the KeyBindingMenuState
		/// </summary>
		/// <param name="parent">Parent.</param>
        public override void OnEnter(MonoBehaviour parent)
        {
            if (buttonPos != null)
                buttonPos.Clear();
            else
                buttonPos = new List<Rect>();

            if (m_buttonScripts != null)
                m_buttonScripts.Clear();
            else
                m_buttonScripts = new List<AbstractButton>();

            PopulateScriptList();

        	// Back button
        	buttonPos.Add(new Rect(Screen.width * m_backButtonXWidthPercent, //X
                Screen.height * m_backButtonYHeightPercent, //Y
                Screen.width * m_backButtonWidthWidthPercent,
                Screen.height * m_backButtonHeightHeightPercent));

            AbstractButton backButton = new AbstractButton();

            backButton.title = m_buttonText[m_buttonScripts.Count];
            m_buttonScripts.Add(backButton);

			m_currentlyBinding = false;

            base.OnEnter(parent);
        }

		/// <summary>
		/// If we are binding, listens for a valid input and rebinds the previously selected key.
		/// The return value here is handled entirely by KeyBindingMenusState's parent.
		/// </summary>
		/// <param name="m_CurrentSelectTime"></param>
		/// <returns></returns>
		/// <param name="autoScrolling">If set to <c>true</c> auto scrolling.</param>
		/// <param name="sameDirection">If set to <c>true</c> same direction.</param>
		/// <param name="currentState">Current state.</param>
		public override bool UpdateMenu(float m_CurrentSelectTime, bool autoScrolling, bool sameDirection, MenuController.ScrollHoldState currentState)
		{
			if (m_currentlyBinding)
			{
				if (Input.anyKeyDown) 
				{
					// Loop through all keycodes
					foreach (KeyCode kC in Enum.GetValues(typeof(KeyCode))) // shoutouts to /sp/
					{
						// This key is pressed
						if (Input.GetKey(kC))
						{
							// Needed for jump key rebinding
							if (m_indexKeyBeingBound == 4)
								m_jumpJustRebinded = true;
							
							// Rebind the key
							((KeyBindButton)m_buttonScripts [m_indexKeyBeingBound]).SetKeycode (kC);

							// Re-enable the rest of the KeyBindButtons
							for (int i = 0; i < buttonPos.Count - 1; i++) 
							{
								((KeyBindButton)m_buttonScripts [i]).enableThis ();
							}

							m_currentlyBinding = false;

							break;
						}
					}
				}
			}

			return base.UpdateMenu (m_CurrentSelectTime, autoScrolling, sameDirection, currentState);
		}

        /// <summary>
        /// 	Called when the component is disabled.
        /// </summary>
        public override void OnExit(MonoBehaviour parent)
        {
            base.OnExit(parent);
        }

		/// <summary>
		/// Hits the button. If we hit a rebindable button, set things up for key rebinding.
		/// </summary>
		/// <param name="n">N.</param>
		protected override void HitButton (int n)
		{
			if (n < buttonPos.Count - 1)
			{
				if (m_jumpJustRebinded) 
				{ 
					// We don't want to rebind jump again
					m_jumpJustRebinded = false;
				} 
				else 
				{
					if (((KeyBindButton)m_buttonScripts [n]).OnBindButtonClicked ()) 
					{
						m_currentlyBinding = true; // Listen for a key press
						m_indexKeyBeingBound = n; // The KeyBindButton to be rebound is at this index

						// Disable the rest of the KeyBindButtons while we are rebinding a key
						for (int i = 0; i < buttonPos.Count - 1; i++) 
						{
							if (i != n)
								((KeyBindButton)m_buttonScripts [i]).disableThis ();
						}
					}
				}
			} 
			else
				OnBackButtonClicked ();
		}

        #endregion

        #region Methods/Functions

        /// <summary>
        /// 	Populates the options.
        /// </summary>
        protected override void PopulateScriptList()
		{
			bool switchBack = false;

			if (InputMapping.GetControlScheme() is JoystickControlScheme)
			{
				InputMapping.SwitchInputJoystick ();
				switchBack = true;
			}

			int i = 0;

			m_buttonScripts.Add(BuildKeyBindButton(InputMapping.verticalInput, 1, m_buttonText[i++]));
			m_buttonScripts.Add(BuildKeyBindButton(InputMapping.verticalInput, -1, m_buttonText[i++]));
			m_buttonScripts.Add(BuildKeyBindButton(InputMapping.horizontalInput, -1, m_buttonText[i++]));
			m_buttonScripts.Add(BuildKeyBindButton(InputMapping.horizontalInput, 1, m_buttonText[i++]));
			m_buttonScripts.Add(BuildKeyBindButton(InputMapping.jumpInput, m_buttonText[i++]));
			m_buttonScripts.Add(BuildKeyBindButton(InputMapping.swipeAttackInput, m_buttonText[i++]));
			m_buttonScripts.Add(BuildKeyBindButton(InputMapping.cameraMode, m_buttonText[i++]));
			m_buttonScripts.Add(BuildKeyBindButton(InputMapping.dashInput, m_buttonText[i++]));
			m_buttonScripts.Add(BuildKeyBindButton(InputMapping.digInput, m_buttonText[i++]));
			m_buttonScripts.Add(BuildKeyBindButton(InputMapping.glidingInput, m_buttonText[i++]));
			m_buttonScripts.Add(BuildKeyBindButton(InputMapping.groundPoundInput, m_buttonText[i++]));
			m_buttonScripts.Add(BuildKeyBindButton(InputMapping.interactionInput, m_buttonText[i++]));
			m_buttonScripts.Add(BuildKeyBindButton(InputMapping.pause, m_buttonText[i++]));
			m_buttonScripts.Add(BuildKeyBindButton(InputMapping.snapCamera, m_buttonText[i++]));
			m_buttonScripts.Add(BuildKeyBindButton(InputMapping.spinningInput, m_buttonText[i++]));
			m_buttonScripts.Add(BuildKeyBindButton(InputMapping.swimInput, m_buttonText[i++]));
			m_buttonScripts.Add(BuildKeyBindButton(InputMapping.swingingInput, m_buttonText[i++]));
			m_buttonScripts.Add(BuildKeyBindButton(InputMapping.sprintInput, 1, m_buttonText[i++]));
			//m_buttonScripts.Add(BuildKeyBindButton(InputMapping.toggleDebugUI, m_buttonText[i++]));

			if (switchBack)
				InputMapping.SwitchInputJoystick();
		}

		/// <summary>
		/// Builds the key bind button (for axis-related controls)
		/// </summary>
		/// <returns>The key bind button.</returns>
		/// <param name="inputAttribute">Input attribute.</param>
		/// <param name="direction">Direction.</param>
		/// <param name="actionName">Action name.</param>
		private KeyBindButton BuildKeyBindButton(RebindableAxisInputAttribute inputAttribute, int direction, string actionName)
		{
			KeyBindButton instance = new KeyBindButton();
			instance.SetInputAttribute(inputAttribute, direction, actionName);

			Rect toAdd = new Rect(0, //X
				(m_MenuRect.height * m_menuSButtonHeightMenuSpaceHeightPercent + m_MenuRect.height * m_menuSButtonSpacingMenuSpaceHeightPercent) * buttonPos.Count + m_ScrollOffset, //Y
				m_MenuRect.width * m_menuSButtonWidthMenuSpaceWidthPercent,//Width
				m_MenuRect.height * m_menuSButtonHeightMenuSpaceHeightPercent); //Height
			buttonPos.Add(toAdd);

			return instance;
		}

		/// <summary>
		/// Builds the key bind button (for key/button-related controls)
		/// </summary>
		/// <returns>The key bind button.</returns>
		/// <param name="inputAttribute">Input attribute.</param>
		/// <param name="actionName">Action name.</param>
		private KeyBindButton BuildKeyBindButton(RebindableButtonInputAttribute inputAttribute, string actionName)
		{
			KeyBindButton instance = new KeyBindButton();
			instance.SetInputAttribute(inputAttribute, actionName);

			Rect toAdd = new Rect(0, //X
				(m_MenuRect.height * m_menuSButtonHeightMenuSpaceHeightPercent + m_MenuRect.height * m_menuSButtonSpacingMenuSpaceHeightPercent) * buttonPos.Count + m_ScrollOffset, //Y
				m_MenuRect.width * m_menuSButtonWidthMenuSpaceWidthPercent,//Width
				m_MenuRect.height * m_menuSButtonHeightMenuSpaceHeightPercent); //Height
			buttonPos.Add(toAdd);

			return instance;
		}

        /// <summary>
        /// 	Called when the back button is clicked.
        /// </summary>
        private void OnBackButtonClicked()
        {
            panelInstance.PreviousMenu();
        }

        #endregion
    }
}