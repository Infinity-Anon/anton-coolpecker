﻿using AntonCoolpecker.Abstract.Menus.States;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Menus.States
{
	/// <summary>
	/// 	None menu state.
	/// </summary>
	public class NoneMenuState : AbstractMenuState
    {
		#region Override Functions/Methods

		/// <summary>
		/// Determines whether this instance is non-state or not.
		/// </summary>
		/// <returns><c>true</c> if this instance is non-state; otherwise, <c>false</c>.</returns>
        public override bool IsNonState() { return true; }

		/// <summary>
		/// Called when the parent updates.
		/// </summary>
		/// <param name="parent">Parent.</param>
        public override void OnUpdate(MonoBehaviour parent) {}

		/// <summary>
		/// Called when button N has been hit/clicked in the menu state.
		/// </summary>
		/// <param name="n">Button number N.</param>
        protected override void HitButton(int n) {}

		#endregion
    }
}