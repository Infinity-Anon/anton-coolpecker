﻿using AntonCoolpecker.Abstract.Menus.States;

namespace AntonCoolpecker.Concrete.Menus.States
{
	/// <summary>
	/// 	Start menu state.
	/// </summary>
	public class StartMenuState : AbstractMenuState {}
}