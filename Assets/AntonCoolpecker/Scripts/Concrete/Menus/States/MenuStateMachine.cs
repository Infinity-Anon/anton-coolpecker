﻿using AntonCoolpecker.Abstract.Menus.States;
using AntonCoolpecker.Abstract.StateMachine;
using System;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Menus.States
{
	/// <summary>
	/// 	Menu state machine.
	/// </summary>
	[Serializable]
	public class MenuStateMachine : AbstractStateMachine<AbstractMenuState>
	{
		#region Variables

		[SerializeField] private AbstractMenuState m_InitialState;

		#endregion

		#region Properties

		/// <summary>
		/// 	Gets the initial state.
		/// </summary>
		/// <value>The initial state.</value>
		public override AbstractMenuState initialState { get { return m_InitialState; } }

		#endregion
	}
}