﻿using AntonCoolpecker.Abstract.Menus.States;
using System.Collections.Generic;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Menus.States
{
	/// <summary>
	/// 	Main menu state.
	/// </summary>
	public class MainMenuState : AbstractMenuState
    {
		#region Variables

        [SerializeField] private LevelSelectMenuState m_LevelSelectMenuState;
		[SerializeField] private OptionsMenuState m_OptionsMenuState;
		[SerializeField] private CreditsMenuState m_CreditsMenuState;

        [SerializeField] private float m_FontSizeHeightPercent;//Percent of the Screen.Height that will be the Font Size of the button cluster
        [SerializeField] private float m_menuButtonYHeightPercent;//Percent of the Screen.Height that will be the X of the button cluster
        //[SerializeField] private float m_menuButtonHeightHeightPercent;//Percent of the Screen.Height that will be the Hieght of each button in the cluster
        //[SerializeField] private float m_menuButtonWidthWidthPercent;//Percent of the Screen.Width that will be the Width of each button in the cluster
        [SerializeField] private float m_menuButtonSpacingHeightPercent;//Percent of the Screen.Height that will be the space between each of the button in the cluster

		#endregion

		#region Override Functions

		/// <summary>
		/// Called when the state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
        public override void OnEnter(MonoBehaviour parent)
        {
            base.OnEnter(parent);

            if (buttonPos != null)
                buttonPos.Clear();
            else
                buttonPos = new List<Rect>();

            if(m_buttonScripts != null)
                m_buttonScripts.Clear();
            else
                m_buttonScripts = new List<AbstractButton>();

            PopulateScriptList();

            for (int i = 0; i < m_buttonScripts.Count; i++)
            {
                Rect toAdd = new Rect(Screen.width * 0.5f - Screen.height * m_FontSizeHeightPercent * m_buttonText[i].Length / 2, //X
                    Screen.height * m_menuButtonYHeightPercent + (Screen.height * (m_FontSizeHeightPercent + m_menuButtonSpacingHeightPercent)) * i, //Y
                    Screen.height * m_FontSizeHeightPercent * m_buttonText[i].Length, Screen.height * m_FontSizeHeightPercent); //Width, Height
                buttonPos.Add(toAdd);
            }
        }

		/// <summary>
		/// Called when button N has been hit/clicked in the menu.
		/// </summary>
		/// <param name="n">Button number N.</param>
        protected override void HitButton(int n)
        {
            switch (n)
            {
                case 0:
                    panelInstance.NextMenu(m_LevelSelectMenuState);
                    break;
                case 1:
                    panelInstance.NextMenu(m_OptionsMenuState);
                    break;
                case 2:
                    panelInstance.NextMenu(m_CreditsMenuState);
                    break;
                case 3:
                    m_buttonScripts[n].OnButtonClicked();
                    break;
            }
        }

		/// <summary>
		/// Populates the menu based on the current menu script with buttons, sliders, text, etc.
		/// </summary>
        protected override void PopulateScriptList()
        {
            base.PopulateScriptList();

            m_buttonScripts.Add(null);
            m_buttonScripts.Add(null);
            m_buttonScripts.Add(null);
            m_buttonScripts.Add(new QuitButton());
        }

		#endregion
    }
}