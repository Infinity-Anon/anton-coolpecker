﻿using AntonCoolpecker.Abstract.Menus.States;
using System.Collections.Generic;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Menus.States
{
	/// <summary>
	/// Game over menu state.
	/// </summary>
	public class GameOverMenuState : AbstractMenuState 
	{
		#region Variables

		[SerializeField] private float m_menuButtonXWidthPercent; //Percent of the Screen.Width that will be the X of the button cluster
		[SerializeField] private float m_menuButtonYHeightPercent; //Percent of the Screen.Height that will be the X of the button cluster
		[SerializeField] private float m_menuButtonHeightHeightPercent; //Percent of the Screen.Height that will be the Hieght of each button in the cluster
		[SerializeField] private float m_menuButtonWidthWidthPercent; //Percent of the Screen.Width that will be the Width of each button in the cluster
		[SerializeField] private float m_menuButtonSpacingHeightPercent; //Percent of the Screen.Height that will be the space between each button in the cluster

		#endregion

		#region Override Functions

		/// <summary>
		/// Called when the state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnEnter(MonoBehaviour parent)
		{
			base.OnEnter(parent);

			m_buttonScripts = new List<AbstractButton>();
			m_buttonScripts.Add (new RestartGameOverButton());

			if (buttonPos != null)
				buttonPos.Clear();
			else
				buttonPos = new List<Rect>();

			for (int i = 0; i < m_buttonScripts.Count; i++)
			{
				Rect toAdd = new Rect(Screen.width * m_menuButtonXWidthPercent, //X
					Screen.height * m_menuButtonYHeightPercent + (Screen.height * (m_menuButtonHeightHeightPercent + m_menuButtonSpacingHeightPercent))* i, //Y
					Screen.width * m_menuButtonWidthWidthPercent, //Width
					Screen.height * m_menuButtonHeightHeightPercent); //Height
				buttonPos.Add(toAdd);
			}
		}

		#endregion
	}
}