﻿using AntonCoolpecker.Abstract.Menus.States;
using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using AntonCoolpecker.Concrete.Utils;
using AntonCoolpecker.Concrete.Utils.UI;
using Hydra.HydraCommon.Utils;
using System.Collections.Generic;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Menus.States
{
	/// <summary>
	/// 	Level select menu state.
	/// </summary>
	public class LevelSelectMenuState : AbstractScrollableMenuState
    {
		#region Variables

        [SerializeField] private float m_backButtonXWidthPercent;  //Percentage of Screen.Width which determines the X of the back button. 
        [SerializeField] private float m_backButtonYHeightPercent;  //Percentage of Screen.Height which determines the Y of the back button. 
        [SerializeField] private float m_backButtonWidthWidthPercent;  //Percentage of Screen.Width which determines the Width of the back button.
        [SerializeField] private float m_backButtonHeightHeightPercent;  //Percentage of Screen.Height which determines the Height of the back button.

		#endregion

		#region Override Functions

		/// <summary>
		/// Called when the state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
        public override void OnEnter(MonoBehaviour parent)
        {
            m_TotalButtonsInCluster = Application.levelCount;

            base.OnEnter(parent);

            if (buttonPos != null)
                buttonPos.Clear();
            else
                buttonPos = new List<Rect>();

            if (m_buttonText != null)
                m_buttonText.Clear();
            else
                m_buttonText = new List<string>();

            if (m_buttonScripts != null)
                m_buttonScripts.Clear();
            else
                m_buttonScripts = new List<AbstractButton>();

            for (int index = 0; index < m_TotalButtonsInCluster; index++)
                CreateSceneButton(index);

            for (int i = 0; i < m_TotalButtonsInCluster; i++)
            {
                Rect toAdd = new Rect(0, //X
                    (m_MenuRect.height * m_menuSButtonHeightMenuSpaceHeightPercent + m_MenuRect.height * m_menuSButtonSpacingMenuSpaceHeightPercent) * buttonPos.Count + m_ScrollOffset, //Y
                    m_MenuRect.width * m_menuSButtonWidthMenuSpaceWidthPercent, //Width
                    m_MenuRect.height * m_menuSButtonHeightMenuSpaceHeightPercent); //Height
                buttonPos.Add(toAdd);
            }

            float percentDown = (-m_ScrollOffset) / (m_TotalScrollDistance - m_MenuRect.height);
            float scrollInvPerc = (1 - m_ScrollRatio) * percentDown;

            buttonPos.Add(new Rect(Screen.width * m_backButtonXWidthPercent, //X
                Screen.height * m_backButtonYHeightPercent, //Y
                Screen.width * m_backButtonWidthWidthPercent,
                Screen.height * m_backButtonHeightHeightPercent));

            AbstractButton toAddButton = new AbstractButton();
            toAddButton.title = "Back";
            m_buttonScripts.Add(toAddButton);
        }

		/// <summary>
		/// Called when button N has been hit/clicked in the menu.
		/// </summary>
		/// <param name="n">Button number N.</param>
        protected override void HitButton(int n)
        {
            if (n == buttonPos.Count-1)
                panelInstance.PreviousMenu();
            else
                base.HitButton(n);
        }

		#endregion

		#region Private Functions

		/// <summary>
		/// Creates the scene button.
		/// </summary>
		/// <param name="index">Index.</param>
        private void CreateSceneButton(int index)
        { 
            if (index != Application.loadedLevel)
            {
                LevelButton instance = new LevelButton();
                instance.Set(index);
                m_buttonScripts.Add(instance);
            }
            else
            {
                AbstractButton instance = new AbstractButton();
                instance.title = string.Format("{0} - {1}", index, SceneUtils.GetSceneName(index));
                m_buttonScripts.Add(instance);
            }
        }

		#endregion
    }
}