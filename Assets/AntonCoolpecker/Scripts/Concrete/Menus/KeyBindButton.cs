﻿using AntonCoolpecker.Concrete.Menus;
using AntonCoolpecker.Concrete.Menus.HUD;
using Assets.AntonCoolpecker.Scripts.Concrete.Configuration.Controls;
using Hydra.HydraCommon.PropertyAttributes.InputAttributes;
using System;
using UnityEngine;

namespace Assets.AntonCoolpecker.Scripts.Concrete.Menus
{
	/// <summary>
	/// Key binding button - Binds a new button to a specific key when pressed.
	/// </summary>
	class KeyBindButton : AbstractButton
	{
		#region Variables

		private RebindableAxisInputAttribute m_axisInputAttribute;
		private RebindableButtonInputAttribute m_buttonInputAttribute;

		private int m_Direction; //Stores axis direction when held(ie forward or back for vertical)
		private bool m_Disabled = false; //Whether this button instance is disabled(Non-interactive) or not
		private string m_ActionName; //The name that the KeyBindButton displays
		private bool m_IsAxis; //Whether a KeyBindButton instanced binds an RebindableAxisInputAttribute or a RebindableButtonInputAttribute

		#endregion

		#region Functions/Methods

		/// <summary>
		/// Sets the input attribute for axes.
		/// </summary>
		/// <param name="inputAttribute">Input attribute.</param>
		/// <param name="direction">Direction.</param>
		/// <param name="actionName">Action name.</param>
		public void SetInputAttribute (RebindableAxisInputAttribute inputAttribute, int direction, string actionName)
		{
			m_IsAxis = true;

			m_axisInputAttribute = inputAttribute;
			m_Direction = direction;
			m_ActionName = actionName;

			if (m_Direction > 0)
				title = m_ActionName + new String ('.', Math.Max (0, 25 - m_axisInputAttribute.getPositive ().ToString ().Length - m_ActionName.Length)) + m_axisInputAttribute.getPositive ().ToString ();
			else if (m_Direction < 0)
				title = m_ActionName + new String ('.', Math.Max (0, 25 - m_axisInputAttribute.getNegative ().ToString ().Length - m_ActionName.Length)) + m_axisInputAttribute.getNegative ().ToString ();
		}

		/// <summary>
		/// Sets the input attribute for keys.
		/// </summary>
		/// <param name="inputAttribute">Input attribute.</param>
		/// <param name="actionName">Action name.</param>
		public void SetInputAttribute (RebindableButtonInputAttribute inputAttribute, string actionName)
		{
			m_IsAxis = false;

			m_buttonInputAttribute = inputAttribute;
			m_ActionName = actionName;

			title = m_ActionName + new String ('.', Math.Max (0, 25 - m_buttonInputAttribute.GetKeyCode ().ToString ().Length - m_ActionName.Length)) + m_buttonInputAttribute.GetKeyCode ().ToString ();
		}

		/// <summary>
		/// Disable this instance.
		/// </summary>
		public void disableThis ()
		{
			m_Disabled = true;
		}

		/// <summary>
		/// Enable this instance.
		/// </summary>
		public void enableThis ()
		{
			m_Disabled = false;
		}

		/// <summary>
		/// Whether the key bind button has been pressed/clicked or not.
		/// </summary>
		public bool OnBindButtonClicked ()
		{
			if (!m_Disabled) 
			{
				title = "Press a key..." + new String ('.', Math.Max (0, 25 - "Press a key...".Length));
				return true;
			}

			return false;
			//UpdateScroll();
		}

		/// <summary>
		/// Sets the end title of this button to the keycode of the pressed key.
		/// </summary>
		/// <param name="keyCode">Key code.</param>
		public void SetKeycode (KeyCode keyCode)
		{
			if (m_IsAxis) 
			{
				if (m_Direction > 0) 
				{
					m_axisInputAttribute.SetPositive (keyCode);
					title = m_ActionName + new String ('.', Math.Max (0, 25 - m_axisInputAttribute.getPositive ().ToString ().Length - m_ActionName.Length)) + m_axisInputAttribute.getPositive ().ToString ();
				} 
				else if (m_Direction < 0) 
				{
					m_axisInputAttribute.SetNegative (keyCode);
					title = m_ActionName + new String ('.', Math.Max (0, 25 - m_axisInputAttribute.getNegative ().ToString ().Length - m_ActionName.Length)) + m_axisInputAttribute.getNegative ().ToString ();
				}
			}
			else 
			{
				m_buttonInputAttribute.SetKeyCode (keyCode);
				title = m_ActionName + new String ('.', Math.Max (0, 25 - m_buttonInputAttribute.GetKeyCode ().ToString ().Length - m_ActionName.Length)) + m_buttonInputAttribute.GetKeyCode ().ToString ();
			}
		}

		/// <summary>
		/// Draw the specified rectangle, boxColor, textColor and digits.
		/// </summary>
		/// <param name="drawRect">Draw rect.</param>
		/// <param name="boxColor">Box color.</param>
		/// <param name="textColor">Text color.</param>
		/// <param name="digits">Digits.</param>
		public override void Draw (Rect drawRect, Color boxColor, HUDDigit.TextColor textColor, HUDDigit digits)
		{
			// TODO: make the button appear differently when disabled

			GUI.color = Color.white;

			// DRAW CAPITALIZED TITLE
			string capsTitle = title.ToUpper ();
			for (int i = 0; i < capsTitle.Length; i++) 
			{
				Rect doublePos = new Rect (drawRect.x + i * drawRect.width * 0.5f / (capsTitle.Length + 1),
					                             drawRect.y + drawRect.height / 2 - drawRect.width / (capsTitle.Length + 1) * 0.25f,
					                             drawRect.width / (capsTitle.Length + 1) * 0.5f, 
					                             drawRect.width / (capsTitle.Length + 1) * 0.5f);
				Rect toGet = digits.GetDigit (capsTitle [i], textColor);

				if (m_Disabled)
					GUI.color = new Color (1f, 1f, 1f, 0.5f);
				
				GUI.DrawTextureWithTexCoords (doublePos, digits.m_DigitSpriteTexture, toGet);
				GUI.color = Color.white;
			}
		}

		#endregion
	}
}