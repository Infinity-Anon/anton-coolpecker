﻿using System.Collections;
using UnityEngine;

/// <summary>
/// Random stuttering icon - The time for each frame is randomized
/// between the original stutter time values and the maximum randomization values.
/// </summary>
public class RandomStutteringIcon : StutteringIcon
{
	#region Variables

    [SerializeField] private float[] randomizationMax; //The maximum randomization values.
    [SerializeField] private float[] currentStutter; //The new stutter times generated between the random and actual stutter time values.

	#endregion

	#region Override Functions

	/// <summary>
	/// Called when the icon frame is changed to the next frame in the spritesheet.
	/// </summary>
    protected override void ChangeFrame()
    {
        if (timeKeeper >= currentStutter[currentStutterTimeFrame])
        {
            timeKeeper -= currentStutter[currentStutterTimeFrame];
            currentColumn++;

            if (currentColumn >= spriteColumn)
            {
                currentColumn = 0;
                currentRow++;

                if (currentRow >= spriteRow)
                    currentRow = 0;
            }

            currentStutter[currentStutterTimeFrame] = Random.Range(stutterTimes[currentStutterTimeFrame], randomizationMax[currentStutterTimeFrame]);
            currentStutterTimeFrame++;

            if (currentStutterTimeFrame >= stutterTimes.Length)
            {
                currentStutterTimeFrame = 0;
            }
        }
    }

	#endregion
}