﻿using AntonCoolpecker.Concrete.Utils;
using AntonCoolpecker.Utils;
using Hydra.HydraCommon.Abstract;
using System;
using System.Collections.Generic; //Required for Lists.
using UnityEngine;

namespace AntonCoolpecker.Concrete.Menus.HUD
{
	/// <summary>
	/// Base class for animated icons.
	/// </summary>
    public class AnimatingIcon : HydraMonoBehaviour
    {
		#region Variables

		[SerializeField] protected float spriteColumn; //Keeps track of the total amount of columns in the spritesheet.
		[SerializeField] protected float spriteRow; //Keeps track of the total amount of rows in the spritesheet.
		[SerializeField] protected float timePerFrame; //Amount of time per each frame for the animated icon.
        [SerializeField] protected Texture2D image; //Reference to spritesheet for the animated icon.

		protected int currentRow; //Keeps track of the current row to be drawn in the spritesheet.
		protected int currentColumn; //Keeps track of the current column to be drawn in the spritesheet.
        protected float timeKeeper = 0f; //Keeps track of elapsed time - Used for determining when to draw the next frame.

		#endregion

		#region Functions

		/// <summary>
		/// Called when the object is instantiated.
		/// </summary>
        protected override void Awake()
        {
            base.Awake();
        }

		/// <summary>
		/// Called when the animated icon is drawn.
		/// </summary>
		/// <param name="displayRect">Display rect.</param>
        public void Draw(Rect displayRect)
        {
            Rect pullCoords = new Rect(currentColumn/spriteColumn, 1f - (currentRow + 1f) / spriteRow, 1f / spriteColumn, 1f / spriteRow);
            GUI.DrawTextureWithTexCoords(displayRect, image, pullCoords);
        }

		/// <summary>
		/// Called each time the frame is updated.
		/// </summary>
        public void UpdateFrame()
        {
            timeKeeper += Time.deltaTime;
            ChangeFrame();
        }

		/// <summary>
		/// Called when the icon frame is changed to the next frame in the spritesheet.
		/// </summary>
        protected virtual void ChangeFrame()
        {
            if (timeKeeper >= timePerFrame)
            {
                timeKeeper -= timePerFrame;
                currentColumn++;

                if (currentColumn >= spriteColumn)
                {
                    currentColumn = 0;
                    currentRow++;

                    if (currentRow >= spriteRow)
                        currentRow = 0;
                }
            }
        }

		#endregion
    }
}