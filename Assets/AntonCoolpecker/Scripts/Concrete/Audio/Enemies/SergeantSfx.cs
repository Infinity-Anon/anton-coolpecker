﻿using AntonCoolpecker.Abstract.Audio.Enemies;
using System;
using System.Collections;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Audio.Enemies
{
	/// <summary>
	/// Sergeant Ant sound effects.
	/// </summary>
	//TODO: IMPLEMENT SOUND EFFECTS FOR SERGEANT ANT.
    [Serializable]
    public class SergeantSfx : AbstractEnemySfx {}
}