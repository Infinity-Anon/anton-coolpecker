﻿using AntonCoolpecker.Abstract.Audio.Enemies;
using System;
using System.Collections;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Audio.Enemies
{
	/// <summary>
	/// Soldier ant SFX class - Holds SFXs for the soldier ant.
	/// </summary>
	[Serializable] public class SoldierAntSfx : AbstractEnemySfx {}
}