﻿using AntonCoolpecker.Concrete.Configuration;
using AntonCoolpecker.Utils;
using AntonCoolpecker.Utils.Time;
using Hydra.HydraCommon.Abstract;
using System.Collections;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Audio
{
	/// <summary>
	/// Manages the in-game music.
	/// </summary>
	public class MusicManager : SingletonHydraMonoBehaviour<MusicManager>
	{
		#region Variables

		[SerializeField] private AudioSource m_MainSource; //"Main" audio source.
		[SerializeField] private AudioSource m_AntSource; //"Ant" audio source.
		[SerializeField] private AudioSource m_OtherSource; //"Other" audio source.

		[SerializeField] private AudioClip m_MainTrack; //Music to play from the main audio source.
		[SerializeField] private AudioClip m_AntTrack; //Music to play from the ant audio source.
		[SerializeField] private AudioClip m_OtherTrack; //Music to play from the "other" audio source.
		[SerializeField] private bool m_IsOtherSynchronised = false; //Is the "other" audio source synched with the other audio sources?

		private bool m_IsAnt = false; //Are we meant to be playing from the ant audio source?
		private bool m_IsOther = false; //Are we meant to be playing from the "other" audio source?

		[SerializeField] [Tweakable("Music")] private float m_CrossfadeTime; //How fast the crossfade should occur.
		[SerializeField] [Tweakable("Music")] private float m_MenuVolumeFactor; //Decides how much the menu volume lowers the other audio volumes.
		[SerializeField] [Tweakable("Music")] private float m_InitialCueDelay; //When to start playing the audio from the audio sources.

		private float m_MainFadeVolume; //Volume for the main audio source.
		private float m_AntFadeVolume; //Volume for the ant audio source.
		private float m_OtherFadeVolume; //Volume for the "other" audio source.
		private int m_NearbyAnts = 0; //Use reference counting to determine when to switch to the ant track.

		#endregion

		#region Messages

		/// <summary>
		/// Initialize audio sources and volume values.
		/// </summary>
		public void Start()
		{
			double playbackStart = AudioSettings.dspTime + m_InitialCueDelay;

			if (m_MainTrack == null) 
			{
				Debug.LogWarning("No main BGM track");
			} 
			else 
			{
				m_MainSource.clip = m_MainTrack;
				m_MainSource.PlayScheduled(playbackStart);
			}

			if (m_AntTrack == null) 
			{
				Debug.LogWarning("No ant BGM track");
			} 
			else 
			{
				m_AntSource.clip = m_AntTrack;
				m_AntSource.PlayScheduled(playbackStart);
			}

			if (m_OtherTrack != null) 
			{
				m_OtherSource.clip = m_OtherTrack;

				if (m_IsOtherSynchronised) 
				{
					m_OtherSource.PlayScheduled(playbackStart);
				}
			}

			m_MainFadeVolume = 1.0f;
			m_AntFadeVolume = 0.0f;
			m_OtherFadeVolume = 0.0f;
		}

		/// <summary>
		/// Handle audio volume values/crossfade.
		/// </summary>
		public void Update()
		{
			float fadeStep = Time.deltaTime * m_CrossfadeTime;

			AudioSource currentSource = CurrentSource();

			if (currentSource == m_MainSource) 
			{
				m_MainFadeVolume = Mathf.Min(m_MainFadeVolume + fadeStep, 1f);
				m_AntFadeVolume = Mathf.Max(m_AntFadeVolume - fadeStep, 0f);
				m_OtherFadeVolume = Mathf.Max(m_OtherFadeVolume - fadeStep, 0f);
			} 
			else if (currentSource == m_AntSource) 
			{
				m_AntFadeVolume = Mathf.Min(m_AntFadeVolume + fadeStep, 1f);
				m_MainFadeVolume = Mathf.Max(m_MainFadeVolume - fadeStep, 0f);
				m_OtherFadeVolume = Mathf.Max(m_OtherFadeVolume - fadeStep, 0f);
			} 
			else 
			{
				m_OtherFadeVolume = Mathf.Min(m_OtherFadeVolume + fadeStep, 1f);
				m_MainFadeVolume = Mathf.Max(m_MainFadeVolume - fadeStep, 0f);
				m_AntFadeVolume = Mathf.Max(m_AntFadeVolume - fadeStep, 0f);
			}

			float scaledVolume = Options.musicVolume * 0.01f;

			m_MainSource.volume = scaledVolume * m_MainFadeVolume * (GameTime.paused ? m_MenuVolumeFactor : 1f);
			m_AntSource.volume = scaledVolume * m_AntFadeVolume * (GameTime.paused ? m_MenuVolumeFactor : 1f);
			m_OtherSource.volume = scaledVolume * m_OtherFadeVolume * (GameTime.paused ? m_MenuVolumeFactor : 1f);
		}

		#endregion

		#region Public Functions

		/// <summary>
		/// Load up an 'Other' track so that it will be ready to play.
		/// </summary>
		/// <param name="newOtherClip"></param>
		public void CueOther(AudioClip newOtherClip, bool isSynchronised)
		{
			if (!m_IsOther) 
			{	
				//Load clip into source
				m_OtherTrack = newOtherClip;
				m_OtherSource.Stop();
				m_OtherSource.clip = m_OtherTrack;
				m_IsOtherSynchronised = isSynchronised;

				if (isSynchronised) 
				{
					//Figure out how to play in sync
					m_OtherSource.Play();
					m_OtherSource.timeSamples = CurrentSource().timeSamples;
				}

			} 
			else 
			{
				Debug.LogWarning("Tried to cue other track while in playback.");
			}
		}

		/// <summary>
		/// Fades from the current audio source to the ant audio source.
		/// </summary>
		public void FadeToAnt()
		{
			m_NearbyAnts += 1;
			Debug.LogFormat("{0} ants", m_NearbyAnts);

			if (m_NearbyAnts > 0) 
			{
				this.m_IsAnt = true;
			}
		}

		/// <summary>
		/// Fades from the ant audio source to the main/other source.
		/// </summary>
		public void FadeFromAnt()
		{
			m_NearbyAnts -= 1;
			Debug.LogFormat("{0} ants", m_NearbyAnts);

			if (m_NearbyAnts <= 0) 
			{
				this.m_IsAnt = false;
			}
		}

		//TODO: Implement this
		/// <summary>
		/// Fades from the current audio source to the "other" source.
		/// </summary>
		public void FadeToOther()
		{
			if (m_IsOtherSynchronised) 
			{
				//crossfade
			} 
			else
			{
				//fade out then fade in
			}
		}

		//TODO: Implement this
		/// <summary>
		/// Fades from the "other" audio source to the main/ant source.
		/// </summary>
		public void FadeFromOther()
		{
			if (m_IsOtherSynchronised) 
			{
				//crossfade
			} 
			else 
			{
				//fade out the fade in
			}
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// Gets the currently active audio source.
		/// </summary>
		/// <returns>The source.</returns>
		private AudioSource CurrentSource()
		{
			if (m_IsOther) 
			{
				return m_OtherSource;
			} 
			else if (m_IsAnt) 
			{
				return m_AntSource;
			} 
			else 
			{
				return m_MainSource;
			}
		}

		#endregion
	}
}