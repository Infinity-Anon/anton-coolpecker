﻿using Hydra.HydraCommon.PropertyAttributes;
using System;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Audio.Player
{
	/// <summary>
	/// 	FootstepsSFX contains references to footstep audio clips.
	/// </summary>
	[Serializable]
	public class FootstepsSFX
	{
		#region Variables

		[SerializeField] private SoundEffectAttribute[] m_Stone;
		[SerializeField] private SoundEffectAttribute[] m_Dirt;
		[SerializeField] private SoundEffectAttribute[] m_Sand;

		#endregion

		#region Methods

		/// <summary>
		/// 	Gets the SFX.
		/// </summary>
		/// <returns>The SFX.</returns>
		/// <param name="surfaceType">Surface type.</param>
		public SoundEffectAttribute[] GetSFX(SurfaceInfo.SurfaceType surfaceType)
		{
			switch (surfaceType)
			{
				case SurfaceInfo.SurfaceType.Stone:
					return m_Stone;

				case SurfaceInfo.SurfaceType.Dirt:
					return m_Dirt;

				case SurfaceInfo.SurfaceType.Sand:
					return m_Sand;

				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		#endregion
	}
}