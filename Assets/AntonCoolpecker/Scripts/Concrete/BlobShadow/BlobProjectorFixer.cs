﻿using AntonCoolpecker.Utils.Time;
using Hydra.HydraCommon.Abstract;
using Hydra.HydraCommon.Utils;
using UnityEngine;

namespace AntonCoolpecker.Concrete.BlobShadow
{
	/// <summary>
	/// Blob projector fixer - Projects a blob shadow on an object.
	/// </summary>
	[RequireComponent(typeof(Projector))]
	public class BlobProjectorFixer : HydraMonoBehaviour
	{
		#region Variables

		// Used for pre-scaling up the object for proper air distance scaling 
		[SerializeField] private float m_InitialScale = 5f;

		//Used when dividing the pre-scale with the player's distance to the ground for a more lenghty scaling
		[SerializeField] private float m_Addition = 11f;

		//Positive number used to casting shadows on terrains correctly
		[SerializeField] private float m_ShadowDistanceTolerance = 0.5f;

		//Values for expanding/minimizing aspect ratio if player is rotating
		[SerializeField] private float m_ARMin = 1f;
		[SerializeField] private float m_ARMax = 2f;

		private Projector m_CachedProjector;

		#endregion

		#region Properties

		/// <summary>
		/// 	Gets the projector.
		/// </summary>
		/// <value>The projector.</value>
		public Projector projector { get { return m_CachedProjector ?? (m_CachedProjector = GetComponent<Projector>()); } }

		#endregion

		#region Override Functions

		/// <summary>
		/// 	Called once every frame.
		/// </summary>
		protected override void Update()
		{
			base.Update();

			Vector3 start = projector.transform.position + projector.transform.forward.normalized * projector.nearClipPlane;

			RaycastHit hit;
			Ray ray = new Ray(start, projector.transform.forward);

			if (!Physics.Raycast(ray, out hit, projector.farClipPlane - projector.nearClipPlane, ~projector.ignoreLayers))
				return;

			if (!FilterHitInfo(hit, ray.direction, false))
				return;

			float currentScale = projector.orthographicSize;
			currentScale = m_InitialScale / (hit.distance + m_Addition);

			projector.orthographicSize = currentScale;

			float distance = hit.distance + projector.nearClipPlane;
			projector.farClipPlane = (distance + m_ShadowDistanceTolerance) * 2;

			//Scale the blob projector based on player rotation
			projector.transform.rotation = Quaternion.Euler (90, gameObject.transform.parent.rotation.eulerAngles.y - 90, 0);

			float divisionAngle = 0;

			if (gameObject.transform.parent.rotation.eulerAngles.x >= 0 && gameObject.transform.parent.rotation.eulerAngles.x < 180) 
				divisionAngle = Mathf.DeltaAngle (gameObject.transform.parent.rotation.eulerAngles.x, 90);

			if (gameObject.transform.parent.rotation.eulerAngles.x >= 180 && gameObject.transform.parent.rotation.eulerAngles.x < 360)
				divisionAngle = Mathf.DeltaAngle (gameObject.transform.parent.rotation.eulerAngles.x, 270);
			
			float finalAR = HydraMathUtils.Clamp(m_ARMax - (((m_ARMax - m_ARMin) / 90f) * divisionAngle), m_ARMin, m_ARMax); //Size the aspect ratio of projector depending on angle of player
			projector.aspectRatio = finalAR;
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// 	Returns false if the RaycastHit should be ignored.
		/// </summary>
		/// <returns><c>true</c>, if hit info is good, <c>false</c> otherwise.</returns>
		/// <param name="hitInfo">Hit info.</param>
		/// <param name="direction">Direction.</param>
		/// <param name="ignoreTriggers">If set to <c>true</c> ignore triggers.</param>
		private bool FilterHitInfo(RaycastHit hitInfo, Vector3 direction, bool ignoreTriggers)
		{
			// Ignore triggers
			if (ignoreTriggers && hitInfo.collider.isTrigger)
				return false;

			return true;
		}

		#endregion
	}
}