﻿using Hydra.HydraCommon.PropertyAttributes;
using System;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Collectables
{
	/// <summary>
	/// 	CollectableSFX stores sound effects for game collectables.
	/// </summary>
	[Serializable]
	public class CollectableSFX
	{
		#region Variables

		[SerializeField] private SoundEffectAttribute[] m_Collect;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the collectable SFXs.
		/// </summary>
		/// <value>The collect.</value>
		public SoundEffectAttribute[] collect { get { return m_Collect; } }

		#endregion
	}
}