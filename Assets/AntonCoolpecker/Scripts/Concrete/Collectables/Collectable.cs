using AntonCoolpecker.Abstract.Player;
using AntonCoolpecker.Concrete.Listeners.Collectables;
using AntonCoolpecker.Concrete.Messaging;
using AntonCoolpecker.Concrete.Menus.HUD;
using AntonCoolpecker.Utils.Time;
using Hydra.HydraCommon.Abstract;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Collectables
{
	/// <summary>
	/// Collectable type.
	/// </summary>
	public enum CollectableType
	{
		Health = 0,
        Fruit = 1,
		Doodad = 2,
		PowerCore = 3,
		Scrap = 4,
		Photo = 5
	}

	/// <summary>
	/// Level.
	/// </summary>
    public enum Level
    {
        Tutorial = 0,
        PoynettePort = 1,
        TranquilTreeline = 2,
        AncientAntics = 3,
        ColossusCoast = 4,
        VileValley = 5,
        GlimmerGlacier = 6,
        HorrendousHill = 7
    }

	/// <summary>
	/// Collectable class - Base script for collectables
	/// </summary>
	[RequireComponent(typeof(CollectableAudioListener))]
	public class Collectable : HydraMonoBehaviour
	{
		#region Variables

        [SerializeField] private CollectableType CollectableType; //Type of collectable
        [SerializeField] private Level Level; //Which level this collectable is within

		/// <summary>
		/// ID refers to which index in LevelProgress for the relevent collectable will be updated
		/// e.g. DOODAD 4 will update m_Doodads[4] in the relevant LevelProgress.
		/// If the ID is larger than the array in PlayerProgress, then the collectable will not count.
		/// e.g. If the array contains only one element, then any ID value above 0 will not count.
		/// </summary>
        [SerializeField] private int m_ID;

		/// <summary>
		/// //How fast a collectible should be pulled to the player
		/// </summary>
		[SerializeField] private float m_PullSpeed = 1.0f;

		/// <summary>
		/// The m identifier by value false identifier by name true.
		/// </summary>
        [SerializeField] private bool m_IdByValueFalse_IdByNameTrue = true; 

        //private bool m_Move = false;
		//private Vector3 m_PullPoint = Vector3.zero;
		//private float m_StartTime = 0;

		#endregion

		#region Override Functions

		/// <summary>
		/// OnTriggerEnter is called when the Collider other enters the trigger.
		/// </summary>
		/// <param name="other">Other.</param>
		protected override void OnTriggerEnter(Collider other)
		{
			base.OnTriggerEnter(other);

			if (other.gameObject.layer != 12)
				return;
			
            AbstractPlayerController player = other.GetComponent<AbstractPlayerController>();

            //First determine type of level, then type of collectable
            if (CollectableType != CollectableType.Photo)
            {
                PlayerStatistics.PlayerCollectableStatistics[CollectableType]++;
                switch (Level)
                {
                    case Level.Tutorial:
					
                        if (CollectableType == CollectableType.Doodad)
                            player.progress.Tutorial.CollectDoodad(m_ID);
                        else if (CollectableType == CollectableType.Fruit)
                            player.progress.Tutorial.CollectFruit(m_ID);
                        break;

                    case Level.PoynettePort:
					
                        if (CollectableType == CollectableType.Doodad)
                            player.progress.PoynettePort.CollectDoodad(m_ID);
                        else if (CollectableType == CollectableType.Fruit)
                            player.progress.PoynettePort.CollectFruit(m_ID);
                        break;

                    case Level.TranquilTreeline:
					
                        if (CollectableType == CollectableType.Doodad)
                            player.progress.TranquilTreeline.CollectDoodad(m_ID);
                        else if (CollectableType == CollectableType.Fruit)
                            player.progress.TranquilTreeline.CollectFruit(m_ID);
                        break;

                    case Level.AncientAntics:
					
                        if (CollectableType == CollectableType.Doodad)
                            player.progress.AncientAntics.CollectDoodad(m_ID);
                        else if (CollectableType == CollectableType.Fruit)
                            player.progress.AncientAntics.CollectFruit(m_ID);
                        break;

                    case Level.ColossusCoast:
					
                        if (CollectableType == CollectableType.Doodad)
                            player.progress.ColossusCoast.CollectDoodad(m_ID);
                        else if (CollectableType == CollectableType.Fruit)
                            player.progress.ColossusCoast.CollectFruit(m_ID);
                        break;

                    case Level.VileValley:
					
                        if (CollectableType == CollectableType.Doodad)
                            player.progress.VileValley.CollectDoodad(m_ID);
                        else if (CollectableType == CollectableType.Fruit)
                            player.progress.VileValley.CollectFruit(m_ID);
                        break;

                    case Level.GlimmerGlacier:
					
                        if (CollectableType == CollectableType.Doodad)
                            player.progress.GlimmerGlacier.CollectDoodad(m_ID);
                        else if (CollectableType == CollectableType.Fruit)
                            player.progress.GlimmerGlacier.CollectFruit(m_ID);
                        break;

                    case Level.HorrendousHill:
					
                        if (CollectableType == CollectableType.Doodad)
                            player.progress.HorrendousHill.CollectDoodad(m_ID);
                        else if (CollectableType == CollectableType.Fruit)
                            player.progress.HorrendousHill.CollectFruit(m_ID);
                        break;
                }
            }

			CollectableMessages.BroadcastOnCollected(gameObject);
			Destroy(gameObject);
		}

		/// <summary>
		/// Called before the first Update.
		/// </summary>
        protected override void Start()
        {
            base.Start();

            if (m_IdByValueFalse_IdByNameTrue)
            { 
                if (gameObject.name.Contains(" "))
                    int.TryParse(gameObject.name.Split(" "[0])[1], out m_ID);
                else
                    m_ID = 0;
            }

            //Negative values for ID are not allowed and will cause bugs, for now set to zero
			if (m_ID < 0) 
			{
				Debug.LogWarning ("The ID of an collectable cannot be lower than zero - Automatically resetting ID to 0");
				m_ID = 0;
			}
        }

//      protected override void Update()
//		{
//			if (!m_Move)
//				return;
//           
//			//Collectable speeds up over distance.
//			float distanceCovered = (GameTime.time - m_StartTime) * m_PullSpeed;
//			this.transform.position = Vector3.MoveTowards(this.transform.position, m_PullPoint, distanceCovered);
//		}

//		public void SetPullPoint(Vector3 point)
//		{
//			if (!m_Move)
//				m_StartTime = GameTime.time;
//
//			m_PullPoint = point;
//			m_Move = true;
//		}

		#endregion
	}
}