using AntonCoolpecker;
using AntonCoolpecker.Concrete;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace AntonCoolpecker.Concrete.Dialogue 
{
	/// <summary>
	/// Dialogue controller prototype.
	/// </summary>
	//TODO: REMOVE THIS SCRIPT? MIGHT NOT BE NECCESSARY ANYMORE
	public class DialogueController : MonoBehaviour
	{
		#region Variables

		public bool Automatic; //Is the conversation automatic(Trigger)?
		public Image talkPrompt; //Coversation talk prompt
		public Image convoBox; //Conversation box
		public Text panelText; //Convseration panel text
		public Camera sceneCamera; //Reference to conversation camera

		private int i = 0; //Dialogue index
		private bool canInput; //Can the player input answers/dialogue in this conversation?
		//private DialogueClip[] Lines;
		private CharacterLocomotor player; //Reference to player
		private AudioSource convoAudio; //Conversation audio

		#endregion

		#region Functions

		/// <summary>
		/// Start this instance.
		/// </summary>
		void Start ()
		{
			//Lines = GetComponents<DialogueClip> ();
			convoAudio = GetComponent<AudioSource> ();
			sceneCamera = Camera.main;
			talkPrompt.enabled = false;
			convoBox.enabled = false;
			panelText.enabled = false;
			canInput = false;
		}

		/// <summary>
		/// Raises the trigger enter event.
		/// </summary>
		/// <param name="col">Col.</param>
		void OnTriggerEnter (Collider col)
		{
			if (Automatic == true)
			{
				convoBox.enabled = true;
				panelText.enabled = true;
				canInput = true;
				Next ();
			} 
			else if (col.gameObject.tag == "Player") 
			{
				player = col.gameObject.GetComponent<CharacterLocomotor> ();
				talkPrompt.enabled = true;
				player.enabled = false;
				canInput = true;
			}
			else
				return;
		}

		/// <summary>
		/// Raises the trigger exit event.
		/// </summary>
		/// <param name="col">Col.</param>
		void OnTriggerExit(Collider col)
		{
			if (Automatic == false)
			talkPrompt.enabled = false;
			player.enabled = true;
			canInput = false;
		}

		/// <summary>
		/// Increment dialogue.
		/// </summary>
		void Next ()
		{
			//if (i == Lines.Length + 1)
				//Exit ();

			//sceneCamera.transform.LookAt (Lines [i].Character.transform);
			//Lines[i].Character.GetComponent<Animator> ().SetTrigger (Lines [i].animationTrigger);
			//convoAudio.clip = Lines [i].Dialogue;
			//panelText.text = Lines [i].dialogueText;
			i ++;
		}

		/// <summary>
		/// Exit this instance.
		/// </summary>
		void Exit()
		{
			switch (Automatic)
			{
				case true:
					convoBox.enabled = false;
					panelText.enabled = false;
					//TODO: add scene changer to take player back to the game.
					break;
				case false:
					talkPrompt.enabled = false;
					player.enabled = true;
					convoBox.enabled = false;
					panelText.enabled = false;
					break;
			}
		}

		/// <summary>
		/// Update this instance.
		/// </summary>
		void Update()
		{
			if ((canInput == true) && Input.GetButtonDown("Action 1"))
			{
				talkPrompt.enabled = false;
				convoBox.enabled = true;
				panelText.enabled = true;
				Next();
			}
		}

		#endregion
	}
}