﻿using AntonCoolpecker.Concrete.Configuration;
using Hydra.HydraCommon.PropertyAttributes;
using Hydra.HydraCommon.Utils.Audio;
using UnityEngine;

namespace AntonCoolpecker.Concrete
{
	/// <summary>
	/// 	SurfaceInfo describes things about an object such as impact sound
	/// 	and particle effects.
	/// </summary>
	public class SurfaceInfo : MonoBehaviour
	{
		#region Variables

		public enum SurfaceType
		{
			Stone,
			Dirt,
			Sand
		}

		[SerializeField] private SurfaceType m_SurfaceType;

		#endregion

		#region Properties

		/// <summary>
		/// 	Gets the type of the surface.
		/// </summary>
		/// <value>The type of the surface.</value>
		public SurfaceType surfaceType { get { return m_SurfaceType; } }

		#endregion

		#region Static Methods

		/// <summary>
		/// 	Creates a footstep at the given position.
		/// </summary>
		/// <param name="position">Position.</param>
		/// <param name="surfaceType">Surface type.</param>
		public static void Step(GameObject surface, GameObject other)
		{
			SurfaceInfo surfaceInfo = surface.GetComponent<SurfaceInfo>();

			if (surfaceInfo == null)
			{
				StepDefault(other.transform.position);
				return;
			}

			Step(other.transform.position, surfaceInfo.surfaceType);
		}

		/// <summary>
		/// 	Creates a default step at the given position.
		/// </summary>
		/// <param name="position">Position.</param>
		public static void StepDefault(Vector3 position)
		{
			Step(position, SurfaceType.Stone);
		}

		/// <summary>
		/// 	Creates a step at the given position.
		/// </summary>
		/// <param name="position">Position.</param>
		/// <param name="surfaceType">Surface type.</param>
		public static void Step(Vector3 position, SurfaceType surfaceType)
		{
			SoundEffectAttribute[] sounds = AudioConfig.instance.footstepsSFX.GetSFX(surfaceType);

			int index = Random.Range(0, (sounds.Length - 1));
			SoundEffectAttribute soundEffect = sounds[index];

			float pitch = AudioSourceUtils.IncreaseSemitones(1.0f, Random.Range(-2, 2));

			AudioSource source = AudioSourcePool.PlayOneShot(position, soundEffect.audioClip, soundEffect.volumeScale, pitch);
			source.spatialBlend = 1.0f;
		}

		#endregion
	}
}