﻿using UnityEngine;

namespace AntonCoolpecker.Concrete.Configuration
{
	/// <summary>
	/// 	Options is a wrapper around PlayerPrefs for storing user configuration changes.
	/// </summary>
	public static class Options
	{
		//*CAMERA KEYS
		private const string CAMERA_HORIZONTAL_SENSITIVITY_KEY = "CAMERA_HORIZONTAL_SENSITIVITY";
		private const string CAMERA_VERTICAL_SENSITIVITY_KEY = "CAMERA_VERTICAL_SENSITIVITY";

		private const string CAMERA_HORIZONTAL_INVERTED_KEY = "CAMERA_HORIZONTAL_INVERTED";
		private const string CAMERA_VERTICAL_INVERTED_KEY = "CAMERA_VERTICAL_INVERTED";

        private const string CAMERA_STRAFE_MODE_KEY = "CAMERA_STRAFE_MODE_KEY";
        private const string FIELD_OF_VIEW_KEY = "FIELD_OF_VIEW";

        //*AUDIO KEYS
        private const string MUSIC_VOLUME_KEY = "MUSIC_VOLUME";
		private const string SOUNDFX_VOLUME_KEY = "SOUNDFX_VOLUME";
		private const string DIALOGUE_VOLUME_KEY = "DIALOGUE_VOLUME";

        #region Properties

        //*CAMERA PROPERTIES

		/// <summary>
		/// Gets or sets the camera's field of view.
		/// </summary>
		/// <value>The camera's field of view.</value>
        public static float cameraFieldOfView
        {
            get
            {
                float fov = OptionsDefaults.instance.cameraDefaults.fieldOfView;
                return PlayerPrefs.GetFloat(FIELD_OF_VIEW_KEY, fov);
            }
            set { PlayerPrefs.SetFloat(FIELD_OF_VIEW_KEY, value); }
        }

		/// <summary>
		/// Gets or sets the horizontal camera sensitivity.
		/// </summary>
		/// <value>The horizontal camera sensitivity.</value>
        public static float cameraHorizontalSensitivity
		{
			get
			{
				float sensitivity = OptionsDefaults.instance.cameraDefaults.horizontalSensitivity;
				return PlayerPrefs.GetFloat(CAMERA_HORIZONTAL_SENSITIVITY_KEY, sensitivity);
			}
			set { PlayerPrefs.SetFloat(CAMERA_HORIZONTAL_SENSITIVITY_KEY, value); }
		}

		/// <summary>
		/// Gets or sets the vertical camera sensitivity.
		/// </summary>
		/// <value>The vertical camera sensitivity.</value>
		public static float cameraVerticalSensitivity
		{
			get
			{
				float sensitivity = OptionsDefaults.instance.cameraDefaults.verticalSensitivity;
				return PlayerPrefs.GetFloat(CAMERA_VERTICAL_SENSITIVITY_KEY, sensitivity);
			}
			set { PlayerPrefs.SetFloat(CAMERA_VERTICAL_SENSITIVITY_KEY, value); }
		}

		/// <summary>
		/// Gets or sets the horizontal camera invertion.
		/// </summary>
		/// <value><c>true</c> if camera is horizontally inverted; otherwise, <c>false</c>.</value>
		public static bool cameraHorizontalInverted
		{
			get
			{
				bool inverted = OptionsDefaults.instance.cameraDefaults.horizontalInverted;
				return PlayerPrefsGetBool (CAMERA_HORIZONTAL_INVERTED_KEY, inverted);
			}

			set { PlayerPrefsSetBool(CAMERA_HORIZONTAL_INVERTED_KEY, value); }
		}

		/// <summary>
		/// Gets or sets the vertical camera invertion.
		/// </summary>
		/// <value><c>true</c> if camera is vertically inverted; otherwise, <c>false</c>.</value>
		public static bool cameraVerticalInverted
		{
			get
			{
				bool inverted = OptionsDefaults.instance.cameraDefaults.verticalInverted;
				return PlayerPrefsGetBool (CAMERA_VERTICAL_INVERTED_KEY, inverted);
			}

			set { PlayerPrefsSetBool(CAMERA_VERTICAL_INVERTED_KEY, value); }
		}

		/// <summary>
		/// Gets or sets the camera strafe mode.
		/// </summary>
		/// <value><c>true</c> if using strafe mode; otherwise, <c>false</c>.</value>
		public static bool cameraStrafeMode
		{
			get
			{
				bool circular = OptionsDefaults.instance.cameraDefaults.strafeMode;
				return PlayerPrefsGetBool (CAMERA_STRAFE_MODE_KEY, circular);
			}

			set { PlayerPrefsSetBool(CAMERA_STRAFE_MODE_KEY, value); }
		}
			
		//*AUDIO PROPERTIES

		/// <summary>
		/// Gets or sets the music volume.
		/// </summary>
		/// <value>The music volume.</value>
		public static float musicVolume
		{
			get
			{
				float volume = OptionsDefaults.instance.audioDefaults.musicVol;
				return PlayerPrefs.GetFloat(MUSIC_VOLUME_KEY, volume);
			}
			set { PlayerPrefs.SetFloat(MUSIC_VOLUME_KEY, value); }
		}

		/// <summary>
		/// Gets or sets the sound FX volume.
		/// </summary>
		/// <value>The sound FX volume.</value>
		public static float soundFXVolume
		{
			get
			{
				float volume = OptionsDefaults.instance.audioDefaults.soundFXVol;
				return PlayerPrefs.GetFloat(SOUNDFX_VOLUME_KEY, volume);
			}
			set { PlayerPrefs.SetFloat(SOUNDFX_VOLUME_KEY, value); }
		}

		/// <summary>
		/// Gets or sets the dialogue volume.
		/// </summary>
		/// <value>The dialogue volume.</value>
		public static float dialogueVolume
		{
			get
			{
				float volume = OptionsDefaults.instance.audioDefaults.dialogueVol;
				return PlayerPrefs.GetFloat(DIALOGUE_VOLUME_KEY, volume);
			}
			set { PlayerPrefs.SetFloat(DIALOGUE_VOLUME_KEY, value); }
		}

		#endregion

		#region Methods/Functions

		/// <summary>
		/// Save the current instance of PlayerPrefs.
		/// </summary>
		public static void Save()
		{
			PlayerPrefs.Save();
		}

		/// <summary>
		/// Stores a bool in PlayerPrefs as integer type.
		/// </summary>
		/// <param name="key">Key.</param>
		/// <param name="state">If set to <c>true</c> state = 1, otherwise state = 0.</param>
		public static void PlayerPrefsSetBool(string key, bool state)
		{
			PlayerPrefs.SetInt(key, state ? 1 : 0);
		}

		/// <summary>
		/// Retrieves a bool from PlayerPrefs.
		/// </summary>
		/// <returns><c>true</c>, if a bool was retrieved, <c>false</c> otherwise.</returns>
		/// <param name="key">Key.</param>
		/// <param name="defaultValue">Default Bool Value</param>
		public static bool PlayerPrefsGetBool(string key, bool defaultValue)
		{
			int value = PlayerPrefs.GetInt(key, defaultValue ? 1 : 0);

			if (value == 1)
				return true;

			if (value == 0)
				return false;
			
			Debug.LogError ("Error: Internal bool value is out of range - value reset to false");

			return false;
		}

		#endregion
	}
}