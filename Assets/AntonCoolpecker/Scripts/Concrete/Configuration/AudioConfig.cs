﻿using AntonCoolpecker.Abstract.Configuration;
using AntonCoolpecker.Concrete.Audio.Player;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Configuration
{
	/// <summary>
	/// 	AudioConfig provides audio configuration details.
	/// </summary>
	public class AudioConfig : AbstractConfig<AudioConfig>
	{
		#region Variables

		[SerializeField] private FootstepsSFX m_FootstepsSFX;

		#endregion

		#region Properties

		/// <summary>
		/// 	Gets the footsteps SFX.
		/// </summary>
		/// <value>The footsteps SFX.</value>
		public FootstepsSFX footstepsSFX { get { return m_FootstepsSFX; } }

		#endregion
	}
}