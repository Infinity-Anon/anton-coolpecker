﻿using AntonCoolpecker.Abstract.Configuration;
using System;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Configuration
{
	/// <summary>
	/// 	Options defaults represents the default states for user configurable options.
	/// </summary>
	public class OptionsDefaults : AbstractConfig<OptionsDefaults>
	{
		#region Variables

		[SerializeField] private CameraDefaults m_CameraDefaults;
		[SerializeField] private AudioDefaults m_AudioDefaults;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the camera defaults.
		/// </summary>
		/// <value>The camera defaults.</value>
		public CameraDefaults cameraDefaults { get { return m_CameraDefaults; } }

		/// <summary>
		/// Gets the audio defaults.
		/// </summary>
		/// <value>The audio defaults.</value>
		public AudioDefaults audioDefaults { get { return m_AudioDefaults; } }

		#endregion
	}

	/// <summary>
	/// Camera defaults.
	/// </summary>
	[Serializable]
	public class CameraDefaults
	{
		#region Variables

		[SerializeField] private float m_SensitivityMin;
		[SerializeField] private float m_SensitivityMax;

		[SerializeField] private float m_HorizontalSensitivity;
		[SerializeField] private float m_VerticalSensitivity;

		[SerializeField] private float m_FieldOfViewMin;
		[SerializeField] private float m_FieldOfViewMax;
		[SerializeField] private float m_FieldOfView;

		[SerializeField] private bool m_HorizontalInverted;
		[SerializeField] private bool m_VerticalInverted;

        [SerializeField] private bool m_StrafeMode;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the minimum sensitivity for the horizontal/vertical camera sensitivity value.
		/// </summary>
		/// <value>The minimum camera sensitivity.</value>
        public float sensitivityMin { get { return m_SensitivityMin; } }

		/// <summary>
		/// Gets the maximum sensitivity for the horizontal/vertical camera sensitivity value.
		/// </summary>
		/// <value>The maximum camera sensitivity.</value>
		public float sensitivityMax { get { return m_SensitivityMax; } }

		/// <summary>
		/// Gets the horizontal camera sensitivity.
		/// </summary>
		/// <value>The horizontal camera sensitivity.</value>
		public float horizontalSensitivity { get { return m_HorizontalSensitivity; } }

		/// <summary>
		/// Gets the vertical camera sensitivity.
		/// </summary>
		/// <value>The vertical camera sensitivity.</value>
		public float verticalSensitivity { get { return m_VerticalSensitivity; } }

		/// <summary>
		/// Gets the minimum field of view.
		/// </summary>
		/// <value>The minimum field of view.</value>
		public float fieldOfViewMin { get { return m_FieldOfViewMin; } }

		/// <summary>
		/// Gets the maximum field of view.
		/// </summary>
		/// <value>The maximum field of view.</value>
        public float fieldOfViewMax { get { return m_FieldOfViewMax; } }

		/// <summary>
		/// Gets the field of view value.
		/// </summary>
		/// <value>The field of view value.</value>
        public float fieldOfView { get { return m_FieldOfView; } }

		/// <summary>
		/// Is the camera horizontally inverted?
		/// </summary>
		/// <value><c>true</c> if horizontally inverted; otherwise, <c>false</c>.</value>
        public bool horizontalInverted { get { return m_HorizontalInverted; } }

		/// <summary>
		/// Is the camera horizontally inverted?
		/// </summary>
		/// <value><c>true</c> if vertically inverted; otherwise, <c>false</c>.</value>
		public bool verticalInverted { get { return m_VerticalInverted; } }

		/// <summary>
		/// Should the camera use strafe mode or default mode?
		/// </summary>
		/// <value><c>true</c> if strafe mode; otherwise, <c>false</c>.</value>
		public bool strafeMode { get { return m_StrafeMode; } }

		#endregion
	}

	/// <summary>
	/// Audio defaults.
	/// </summary>
	[Serializable]
	public class AudioDefaults
	{
		#region Variables

		[SerializeField] private float m_MusicVolMin;
		[SerializeField] private float m_MusicVolMax;
		[SerializeField] private float m_MusicVol;

		[SerializeField] private float m_SoundFXMin;
		[SerializeField] private float m_SoundFXMax;
		[SerializeField] private float m_SoundFXVol;

		[SerializeField] private float m_DialogueMin;
		[SerializeField] private float m_DialogueMax;
		[SerializeField] private float m_DialogueVol;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the minimum music volume.
		/// </summary>
		/// <value>The minimum music volume.</value>
		public float musicVolMin { get { return m_MusicVolMin; } }

		/// <summary>
		/// Gets the maximum music volume.
		/// </summary>
		/// <value>The maximum music volume.</value>
		public float musicVolMax { get { return m_MusicVolMax; } }

		/// <summary>
		/// Gets the music volume value.
		/// </summary>
		/// <value>The music volume value.</value>
		public float musicVol{ get { return m_MusicVol; } }

		/// <summary>
		/// Gets the minimum sound effect volume.
		/// </summary>
		/// <value>The minimum sound effect volume.</value>
		public float soundFXVolMin { get { return m_SoundFXMin; } }

		/// <summary>
		/// Gets the maximum sound effect volume.
		/// </summary>
		/// <value>The maximum sound effect volume.</value>
		public float soundFXVolMax { get { return m_SoundFXMax; } }

		/// <summary>
		/// Gets the sound effect volume value.
		/// </summary>
		/// <value>The sound effect volume.</value>
		public float soundFXVol { get { return m_SoundFXVol; } }

		/// <summary>
		/// Gets the minimum dialogue volume.
		/// </summary>
		/// <value>The minimum dialogue volume.</value>
		public float dialogueVolMin { get { return m_DialogueMin; } }

		/// <summary>
		/// Gets the maximum dialogue volume.
		/// </summary>
		/// <value>The maximum dialogue volume.</value>
		public float dialogueVolMax { get { return m_DialogueMax; } }

		/// <summary>
		/// Gets the dialogue volume value.
		/// </summary>
		/// <value>The dialogue volume value.</value>
		public float dialogueVol { get { return m_DialogueVol; } }

		#endregion
	}
}