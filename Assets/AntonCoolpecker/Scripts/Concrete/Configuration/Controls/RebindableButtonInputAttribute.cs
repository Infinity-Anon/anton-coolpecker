﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Hydra.HydraCommon.PropertyAttributes.InputAttributes;
using UnityEngine;

namespace Assets.AntonCoolpecker.Scripts.Concrete.Configuration.Controls
{
	/// <summary>
	/// Rebindable button input attribute.
	/// </summary>
	[Serializable]
	public class RebindableButtonInputAttribute : ButtonInputAttribute
	{
		#region Variables

		private KeyCode m_keyCode;
		private KeyCode m_altKeyCode;
		private string buttonName; // for controllers

		#endregion

		#region Methods

		/// <summary>
		/// Initializes a new instance of the
		/// <see cref="Assets.AntonCoolpecker.Scripts.Concrete.Configuration.Controls.RebindableButtonInputAttribute"/> class.
		/// </summary>
		/// <param name="keyCode">Key code.</param>
		public RebindableButtonInputAttribute (KeyCode keyCode) : base("")
		{
			m_keyCode = keyCode;
		}

		/// <summary>
		/// Initializes a new instance of the
		/// <see cref="Assets.AntonCoolpecker.Scripts.Concrete.Configuration.Controls.RebindableButtonInputAttribute"/> class.
		/// For controllers
		/// TODO: implement rebindable controller inputs
		/// </summary>
		/// <param name="input">Input.</param>
		public RebindableButtonInputAttribute(string input) : base(input) 
		{
			buttonName = input;
		}

		/// <summary>
		/// Returns whether the given button is held down.
		/// </summary>
		/// <returns>true</returns>
		/// <c>false</c>
		new public bool GetButton()
		{
			if (buttonName != null)
				return Input.GetButton (buttonName);
			return Input.GetKey (m_keyCode) || Input.GetKey (m_altKeyCode);
		}

		/// <summary>
		/// Returns true during the frame the user pressed the given button.
		/// </summary>
		/// <returns>true</returns>
		/// <c>false</c>
		new public bool GetButtonDown()
		{
			if (buttonName != null)
				return Input.GetButtonDown (buttonName);
			return Input.GetKeyDown (m_keyCode) || Input.GetKeyDown (m_altKeyCode);
		}

		/// <summary>
		/// Returns true during the frame the user releases the given button.
		/// </summary>
		/// <returns>true</returns>
		/// <c>false</c>
		new public bool GetButtonUp()
		{
			if (buttonName != null)
				return Input.GetButtonUp (buttonName);
			return Input.GetKeyUp (m_keyCode) || Input.GetKeyUp (m_altKeyCode);
		}

		/// <summary>
		/// Sets the key code.
		/// </summary>
		/// <returns><c>true</c>, if key code was set, <c>false</c> otherwise.</returns>
		/// <param name="keyCode">Key code.</param>
		// TODO: prevent player from binding the same key to multiple actions.
		public bool SetKeyCode(KeyCode keyCode)
		{
			if (buttonName == null) 
			{
				m_keyCode = keyCode;
				return true;
			}

			return false;
		}

		/// <summary>
		/// Gets the key code.
		/// </summary>
		/// <returns>The key code.</returns>
		public KeyCode GetKeyCode()
		{
			return m_keyCode;
		}

		/// <summary>
		/// Sets the alternate key code.
		/// </summary>
		/// <returns><c>true</c>, if alternate key code was set, <c>false</c> otherwise.</returns>
		/// <param name="keyCode">Key code.</param>
		// TODO: prevent player from binding the same key to multiple actions.
		public bool SetAltKeyCode(KeyCode keyCode)
		{
			if (buttonName == null) 
			{
				m_altKeyCode = keyCode;
				return true;
			}

			return false;
		}

		/// <summary>
		/// Gets the alternate key code.
		/// </summary>
		/// <returns>The alternate key code.</returns>
		public KeyCode GetAltKeyCode()
		{
			return m_altKeyCode;
		}

		#endregion
	}
}