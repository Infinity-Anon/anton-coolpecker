﻿using AntonCoolpecker.Abstract.Configuration;
using System;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Configuration.Controls.Styles
{
	/// <summary>
	/// 	InputStyleManager provides a space where we can define some information
	/// 	about control styles inside the inspector.
	/// </summary>
	public class InputStyleManager : AbstractConfig<InputStyleManager>
	{
		#region Variables

		public enum Styles
		{
			Xbox,
			Playstation
		}

		[SerializeField] private InputStyle m_XBoxInputSprites;
		[SerializeField] private InputStyle m_PlaystationInputSprites;

		private Styles m_ActiveStyle;

		#endregion

		#region Properties

		/// <summary>
		/// 	Gets or sets the active style.
		/// </summary>
		/// <value>The active style.</value>
		public Styles activeStyle { get { return m_ActiveStyle; } set { m_ActiveStyle = value; } }

		#endregion

		#region Methods

		/// <summary>
		/// 	Gets the sprite.
		/// </summary>
		/// <returns>The sprite.</returns>
		/// <param name="icon">Icon.</param>
		public Sprite GetSprite(InputStyle.Icons icon)
		{
			return GetSprite(m_ActiveStyle, icon);
		}

		/// <summary>
		/// 	Gets the sprite.
		/// </summary>
		/// <returns>The sprite.</returns>
		/// <param name="style">Style.</param>
		/// <param name="icon">Icon.</param>
		public Sprite GetSprite(Styles style, InputStyle.Icons icon)
		{
			switch (style)
			{
				case Styles.Xbox:
					return m_XBoxInputSprites.GetSprite(icon);

				case Styles.Playstation:
					return m_PlaystationInputSprites.GetSprite(icon);

				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		#endregion
	}
}