﻿using System;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Configuration.Controls.Styles
{
	/// <summary>
	/// Input style class - Used for storing custom controller icons for ingame usage
	/// </summary>
	[Serializable]
	public class InputStyle
	{
		#region Variables

		public enum Icons
		{
			DPad,
			DPadUp,
			DPadDown,
			DPadLeft,
			DPadRight,
			DPadUpDown,
			DPadLeftRight,
			LeftStick,
			RightStick,
			Cross,
			Circle,
			Triangle,
			Square,
			Select,
			Start,
			L1,
			R1,
			L2,
			R2
		}

		[SerializeField] private Sprite m_DPad;
		[SerializeField] private Sprite m_DPadUp;
		[SerializeField] private Sprite m_DPadDown;
		[SerializeField] private Sprite m_DPadLeft;
		[SerializeField] private Sprite m_DPadRight;
		[SerializeField] private Sprite m_DPadUpDown;
		[SerializeField] private Sprite m_DPadLeftRight;
		[SerializeField] private Sprite m_LeftStick;
		[SerializeField] private Sprite m_RightStick;
		[SerializeField] private Sprite m_Cross;
		[SerializeField] private Sprite m_Circle;
		[SerializeField] private Sprite m_Triangle;
		[SerializeField] private Sprite m_Square;
		[SerializeField] private Sprite m_Select;
		[SerializeField] private Sprite m_Start;
		[SerializeField] private Sprite m_L1;
		[SerializeField] private Sprite m_R1;
		[SerializeField] private Sprite m_L2;
		[SerializeField] private Sprite m_R2;

		#endregion

		#region Methods

		/// <summary>
		/// 	Gets the sprite.
		/// </summary>
		/// <returns>The sprite.</returns>
		/// <param name="icon">Icon.</param>
		public Sprite GetSprite(Icons icon)
		{
			switch (icon)
			{
				case Icons.DPad:
					return m_DPad;
				case Icons.DPadUp:
					return m_DPadUp;
				case Icons.DPadDown:
					return m_DPadDown;
				case Icons.DPadLeft:
					return m_DPadLeft;
				case Icons.DPadRight:
					return m_DPadRight;
				case Icons.DPadUpDown:
					return m_DPadUpDown;
				case Icons.DPadLeftRight:
					return m_DPadLeftRight;
				case Icons.LeftStick:
					return m_LeftStick;
				case Icons.RightStick:
					return m_RightStick;
				case Icons.Cross:
					return m_Cross;
				case Icons.Circle:
					return m_Circle;
				case Icons.Triangle:
					return m_Triangle;
				case Icons.Square:
					return m_Square;
				case Icons.Select:
					return m_Select;
				case Icons.Start:
					return m_Start;
				case Icons.L1:
					return m_L1;
				case Icons.R1:
					return m_R1;
				case Icons.L2:
					return m_L2;
				case Icons.R2:
					return m_R2;

				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		#endregion
	}
}
