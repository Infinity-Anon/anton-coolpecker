﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Hydra.HydraCommon.PropertyAttributes.InputAttributes;
using UnityEngine;

namespace Assets.AntonCoolpecker.Scripts.Concrete.Configuration.Controls
{
    /// <summary>
    /// Axis Input attribute that can be rebound in-game.
    /// </summary>
    [Serializable]
	public class RebindableAxisInputAttribute : AxisInputAttribute
	{
		#region Variables

		private KeyCode m_keyCodePositive;
		private KeyCode m_keyCodeNegative;
		private KeyCode m_keyCodePositiveAlt;
		private KeyCode m_keyCodeNegativeAlt;
		private string axisName; // for controllers

		#endregion

		#region Public Methods

        /// <summary>
        /// Initializes a new instance of the
		/// <see cref="Assets.AntonCoolpecker.Scripts.Concrete.Configuration.Controls.RebindableAxisInputAttribute"/> class.
        /// </summary>
        /// <param name="keyCodePositive">The KeyCode indicating movement in the postive direction.</param>
		/// <param name="keyCodeNegative">The KeyCode indicating movement in the negative direction.</param>
		public RebindableAxisInputAttribute(KeyCode keyCodePositive, KeyCode keyCodeNegative) : base("")
		{
			m_keyCodePositive = keyCodePositive;
			m_keyCodeNegative = keyCodeNegative;
		}
		public RebindableAxisInputAttribute(KeyCode keyCodePositive, KeyCode keyCodeNegative, KeyCode keyCodePositiveAlt, KeyCode keyCodeNegativeAlt) : base("")
		{
			m_keyCodePositive = keyCodePositive;
			m_keyCodeNegative = keyCodeNegative;
			m_keyCodePositiveAlt = keyCodePositiveAlt;
			m_keyCodeNegativeAlt = keyCodeNegativeAlt;
		}

		/// <summary>
		/// Initializes a new instance of the
		/// <see cref="Assets.AntonCoolpecker.Scripts.Concrete.Configuration.Controls.RebindableAxisInputAttribute"/> class.
		/// For controllers
		/// TODO: implement rebindable controller inputs
		/// </summary>
		/// <param name="input">Input.</param>
		public RebindableAxisInputAttribute(string input) : base(input)
		{
			axisName = input;
		}

		/// <summary>
		/// The same as GetAxis, but without any smoothing.
		/// </summary>
		/// <returns>The raw axis.</returns>
		public new float GetAxisRaw()
        {
			if (axisName != null)
				return Input.GetAxisRaw (axisName);
			if ( (Input.GetKey(m_keyCodePositive) && Input.GetKey(m_keyCodeNegative)) || (Input.GetKey(m_keyCodePositiveAlt) && Input.GetKey(m_keyCodeNegativeAlt)) )
                return 0f;
			else if (Input.GetKey(m_keyCodePositive) || Input.GetKey(m_keyCodePositiveAlt))
                return 1f;
			else if (Input.GetKey(m_keyCodeNegative) || Input.GetKey(m_keyCodeNegativeAlt))
                return -1f;
            else
                return 0f;
        }

		/// <summary>
		/// The value will be in the range -1 - 1 for keyboard and joystick input.
		///  If the axis is setup to be delta mouse movement, the mouse delta is multiplied
		///  by the axis sensitivity and the range is not -1 - 1.
		/// </summary>
		/// <returns>The axis.</returns>
		public new float GetAxis()
		{
			if (axisName != null)
				return Input.GetAxis (axisName);
			
            return GetAxisRaw();
        }

        /// <summary>
		/// 	Rebinds the positive key in the axis. TODO: prevent player from binding the same key to multiple actions.
        /// </summary>
		/// <param name="newPositive">The KeyCode to set to the positive axis.</param>
		public bool SetPositive(KeyCode newPositive)
		{
			if (axisName == null) 
			{
				m_keyCodePositive = newPositive;
				return true;
			}

			return false;
		}
		public bool SetPositiveAlt(KeyCode newPositiveAlt)
		{
			if (axisName == null) 
			{
				m_keyCodePositiveAlt = newPositiveAlt;
				return true;
			}

			return false;
		}

        /// <summary>
		/// 	Rebinds the negative key in the axis. TODO: prevent player from binding the same key to multiple actions.
        /// </summary>
		/// <param name="newNegative">The KeyCode to set to the negative axis.</param>
		public bool SetNegative(KeyCode newNegative)
		{
			if (axisName == null) 
			{
				m_keyCodeNegative = newNegative;
				return true;
			}

			return false;
		}
		public bool SetNegativeAlt(KeyCode newNegativeAlt)
		{
			if (axisName == null)
			{
				m_keyCodeNegativeAlt = newNegativeAlt;
				return true;
			}

			return false;
		}

        /// <summary>
        /// 	Gets the postive key in the axis
		/// </summary>
		public KeyCode getPositive()
		{
			return m_keyCodePositive;
		}
		public KeyCode getPositiveAlt()
		{
			return m_keyCodePositiveAlt;
		}

        /// <summary>
        /// 	Gets the negative key in the axis
		/// </summary>
		public KeyCode getNegative()
		{
			return m_keyCodeNegative;
		}
		public KeyCode getNegativeAlt()
		{
			return m_keyCodeNegativeAlt;
		}

		#endregion
    }
}