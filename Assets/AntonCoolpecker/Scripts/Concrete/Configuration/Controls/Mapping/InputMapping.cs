﻿using Assets.AntonCoolpecker.Scripts.Concrete.Configuration.Controls;
using Hydra.HydraCommon.PropertyAttributes.InputAttributes;
using System;
using System.Collections.Generic;

namespace AntonCoolpecker.Concrete.Configuration.Controls.Mapping
{
	/// <summary>
	/// 	Input mapping.
	/// </summary>
	public static class InputMapping
	{
		//TODO: Fix so both joystick and keyboard works simultaneously(You should be able to switch to controller-only mode and back, if so desired)

		#region Variables

		private static IControlScheme s_ControlScheme;
		private static RebindableButtonInputAttribute s_ControlChange;
		private static Dictionary<Type, IControlScheme> s_ControlSchemeCache;

		private static RebindableButtonInputAttribute s_ToggleDebugUI;

		#endregion

		#region Constructor

		/// <summary>
		/// 	Initializes the <see cref="InputMapping"/> class.
		/// </summary>
		static InputMapping()
		{
			s_ControlScheme = GetControlScheme<KeyboardControlScheme>();
			s_ControlChange = new RebindableButtonInputAttribute("Submit");
			s_ToggleDebugUI = new RebindableButtonInputAttribute("Toggle Debug UI");
		}

		#endregion

		#region Static Functions/Methods

		/// <summary>
		/// 	Switch the input controller.
		/// </summary>
		public static void SwitchInputJoystick()
		{
			if (s_ControlScheme is KeyboardControlScheme)
				s_ControlScheme = GetControlScheme<JoystickControlScheme>();
			else
				s_ControlScheme = GetControlScheme<KeyboardControlScheme>();
		}

		/// <summary>
		/// 	Switch the input controller.
		/// </summary>
		public static void SwitchInputJoystick(bool keyboardFalseJoystickTrue)
		{
			if (keyboardFalseJoystickTrue)
				s_ControlScheme = GetControlScheme<JoystickControlScheme>();
			else
				s_ControlScheme = GetControlScheme<KeyboardControlScheme>();
		}

		/// <summary>
		/// Gets the current control scheme.
		/// </summary>
		/// <returns>The current control scheme.</returns>
		public static IControlScheme GetControlScheme()
		{
			return s_ControlScheme;
		}

		#endregion

        #region Common Mappings

        /// <summary>
        /// 	Gets the horizontal input.
        /// </summary>
        /// <value>The horizontal input.</value>
		public static RebindableAxisInputAttribute horizontalInput { get { return s_ControlScheme.horizontalInput; } }

        /// <summary>
		/// 	Gets the vertical input.heap 
        /// </summary>
        /// <value>The vertical input.</value>
		public static RebindableAxisInputAttribute verticalInput { get { return s_ControlScheme.verticalInput; } }

		/// <summary>
		/// 	Gets the horizontal camera input.
		/// </summary>
		/// <value>The horizontal camera input.</value>
		public static RebindableAxisInputAttribute horizontalCameraInput { get { return s_ControlScheme.horizontalCameraInput; } }

		/// <summary>
		/// 	Gets the vertical camera input.
		/// </summary>
		/// <value>The vertical camera input.</value>
		public static RebindableAxisInputAttribute verticalCameraInput { get { return s_ControlScheme.verticalCameraInput; } }

		/// <summary>
		/// 	Gets the jump input.
		/// </summary>
		/// <value>The jump input.</value>
		public static RebindableButtonInputAttribute jumpInput { get { return s_ControlScheme.jumpInput; } }

		/// <summary>
		/// 	Gets the switch character input.
		/// </summary>
		/// <value>The switch character input.</value>
		public static RebindableButtonInputAttribute switchInput { get { return s_ControlScheme.switchInput; } }

		/// <summary>
		/// 	Gets the interaction input.
		/// </summary>
		/// <value>The interaction input.</value>
		public static RebindableButtonInputAttribute interactionInput { get { return s_ControlScheme.interactionInput; } }

        /// <summary>
		/// 	Gets the interaction input.
		/// </summary>
		/// <value>The interaction input.</value>
		public static RebindableAxisInputAttribute sprintInput { get { return s_ControlScheme.action4Input; } }

        /// <summary>
        /// 	Gets the switch controller input to switch between keyboard and joystick.
        /// </summary>
        /// <value>The switch controller input.</value>
		public static RebindableButtonInputAttribute switchControllerInput { get { return s_ControlChange; } }

		/// <summary>
		/// 	Gets the debug GUI input.
		/// </summary>
		/// <value>The debug GUI input.</value>
		public static RebindableButtonInputAttribute toggleDebugUI { get { return s_ToggleDebugUI; } }

		/// <summary>
		/// 	Gets the underwater swim input.
		/// </summary>
		/// <value>The underwater swim input.</value>
		public static RebindableButtonInputAttribute swimInput { get { return s_ControlScheme.action2Input; } }

		/// <summary>
		/// 	Gets the Camera Snap input.
		/// </summary>
		/// <value>The Camera Snap input.</value>
		public static RebindableButtonInputAttribute snapCamera { get { return s_ControlScheme.snapCameraInput; } }

		/// <summary>
		/// 	Gets the input to switch the camera mode.
		/// </summary>
		/// <value>The input to switch the camera mode.</value>
		public static RebindableButtonInputAttribute cameraMode { get { return s_ControlScheme.cameraModeInput; } }

		/// <summary>
		/// 	Gets the input to open the in-game pause menu.
		/// </summary>
		/// <value>The input to open the in-game pause menu</value>
		public static RebindableButtonInputAttribute pause { get { return s_ControlScheme.pauseInput; } }

		#endregion

		#region Anton Mappings

		/// <summary>
		/// 	Gets the air dig input.
		/// </summary>
		/// <value>The air dig input.</value>
		public static RebindableButtonInputAttribute digInput { get { return s_ControlScheme.action2Input; } }

		/// <summary>
		/// 	Gets the swinging input.
		/// </summary>
		/// <value>The swinging input.</value>
		public static RebindableButtonInputAttribute swingingInput { get { return s_ControlScheme.action3Input; } }

		/// <summary>
		/// 	Gets the swipe attack input.
		/// </summary>
		/// <value>The swipe attack input.</value>
		public static RebindableButtonInputAttribute swipeAttackInput { get { return s_ControlScheme.action1Input; } }

		#endregion

		#region Coolpecker Mappings

		/// <summary>
		/// 	Gets the gliding input.
		/// </summary>
		/// <value>The gliding input.</value>
		public static RebindableButtonInputAttribute glidingInput { get { return s_ControlScheme.action3Input; } }

		/// <summary>
		/// 	Gets the spinning input.
		/// </summary>
		/// <value>The spinning input.</value>
		public static RebindableButtonInputAttribute spinningInput { get { return s_ControlScheme.action1Input; } }

		/// <summary>
		/// 	Gets the ground pound input.
		/// </summary>
		/// <value>The ground pound input.</value>
		public static RebindableButtonInputAttribute groundPoundInput { get { return s_ControlScheme.action2Input; } }

		///<summary>
		/// 	Gets the Dash input.
		/// </summary>
		/// <value>The Dash input.</value>
		public static RebindableButtonInputAttribute dashInput { get { return s_ControlScheme.dashInput; } }

		#endregion

		#region Private Methods

		/// <summary>
		/// 	Gets the control scheme.
		/// </summary>
		/// <returns>The control scheme.</returns>
		/// <typeparam name="T">The control scheme type.</typeparam>
		private static T GetControlScheme<T>() where T : class, IControlScheme, new()
		{
			if (s_ControlSchemeCache == null)
				s_ControlSchemeCache = new Dictionary<Type, IControlScheme>();

			if (!s_ControlSchemeCache.ContainsKey(typeof(T)))
				s_ControlSchemeCache[typeof(T)] = new T();

			return s_ControlSchemeCache[typeof(T)] as T;
		}

		#endregion
	}
}