﻿using Assets.AntonCoolpecker.Scripts.Concrete.Configuration.Controls;
using Hydra.HydraCommon.PropertyAttributes.InputAttributes;

namespace AntonCoolpecker.Concrete.Configuration.Controls.Mapping
{
	/// <summary>
	/// 	JoystickControlScheme provides the joystick input attributes for a control scheme.
	/// </summary>
	public class JoystickControlScheme : IControlScheme
	{
		#region Variables

		private readonly RebindableAxisInputAttribute m_HorizontalInput;
		private readonly RebindableAxisInputAttribute m_VerticalInput;
		private readonly RebindableAxisInputAttribute m_HorizontalCameraInput;
		private readonly RebindableAxisInputAttribute m_VerticalCameraInput;
		private readonly RebindableButtonInputAttribute m_JumpInput;
		private readonly RebindableButtonInputAttribute m_Action1Input;
		private readonly RebindableButtonInputAttribute m_Action2Input;
		private readonly RebindableButtonInputAttribute m_Action3Input;
		private readonly RebindableAxisInputAttribute m_Action4Input;
		private readonly AxisInputAttribute m_Action5Input;
        private readonly RebindableButtonInputAttribute m_SwitchInput;
		private readonly RebindableButtonInputAttribute m_DashInput;
		private readonly RebindableButtonInputAttribute m_SnapCameraInput;
		private readonly RebindableButtonInputAttribute m_CameraModeInput;
		private readonly RebindableButtonInputAttribute m_PauseInput;
        private readonly RebindableButtonInputAttribute m_InteractionInput;

		#endregion

		#region Constructors

        /// <summary>
        /// 	Initializes a new instance of the JoystickControlScheme class.
        /// </summary>
        public JoystickControlScheme()
		{
			// TODO: make default bindings load from a save file

			m_HorizontalInput = new RebindableAxisInputAttribute("Controller Horizontal");
			m_VerticalInput = new RebindableAxisInputAttribute("Controller Vertical");
			m_HorizontalCameraInput = new RebindableAxisInputAttribute("Controller Camera x");
			m_VerticalCameraInput = new RebindableAxisInputAttribute("Controller Camera y");
			m_JumpInput = new RebindableButtonInputAttribute(UnityEngine.KeyCode.JoystickButton0 );//  "Controller Button 0");
			m_Action1Input = new RebindableButtonInputAttribute(UnityEngine.KeyCode.JoystickButton1);//new ButtonInputAttribute("Controller Button 1");
			m_Action2Input = new RebindableButtonInputAttribute(UnityEngine.KeyCode.JoystickButton2);//("Controller Button 2");
			m_Action3Input = new RebindableButtonInputAttribute(UnityEngine.KeyCode.JoystickButton3);//("Controller Button 3");
			m_Action4Input = new RebindableAxisInputAttribute("Action 4"); //("Action 4");
			m_Action5Input = new AxisInputAttribute("Action 5");
			m_SwitchInput = new RebindableButtonInputAttribute(UnityEngine.KeyCode.JoystickButton4);//("Controller Button 4");
			m_DashInput = new RebindableButtonInputAttribute(UnityEngine.KeyCode.JoystickButton5);//("Controller Button 5");
			m_CameraModeInput = new RebindableButtonInputAttribute(UnityEngine.KeyCode.JoystickButton12);//("Controller Button 12");
			m_PauseInput = new RebindableButtonInputAttribute(UnityEngine.KeyCode.JoystickButton7);//("Controller Button 9");
			m_InteractionInput = new RebindableButtonInputAttribute(UnityEngine.KeyCode.JoystickButton3);//("Controller Button 3");
        }

		#endregion

		#region Properties

		/// <summary>
		/// Gets the horizontal input.
		/// </summary>
		/// <value>The horizontal input.</value>
		public RebindableAxisInputAttribute horizontalInput { get { return m_HorizontalInput; } }

		/// <summary>
		/// Gets the vertical input.
		/// </summary>
		/// <value>The vertical input.</value>
		public RebindableAxisInputAttribute verticalInput { get { return m_VerticalInput; } }

		/// <summary>
		/// Gets the horizontal camera input.
		/// </summary>
		/// <value>The horizontal camera input.</value>
		public RebindableAxisInputAttribute horizontalCameraInput { get { return m_HorizontalCameraInput; } }

		/// <summary>
		/// Gets the vertical camera input.
		/// </summary>
		/// <value>The vertical camera input.</value>
		public RebindableAxisInputAttribute verticalCameraInput { get { return m_VerticalCameraInput; } }  

		/// <summary>
		/// Gets the jump input.
		/// </summary>
		/// <value>The jump input.</value>
		public RebindableButtonInputAttribute jumpInput { get { return m_JumpInput; } }

		/// <summary>
		/// Gets the action1 input.
		/// </summary>
		/// <value>The action1 input.</value>
		public RebindableButtonInputAttribute action1Input { get { return m_Action1Input; } }

		/// <summary>
		/// Gets the action2 input.
		/// </summary>
		/// <value>The action2 input.</value>
		public RebindableButtonInputAttribute action2Input { get { return m_Action2Input; } }

		/// <summary>
		/// Gets the action3 input.
		/// </summary>
		/// <value>The action3 input.</value>
		public RebindableButtonInputAttribute action3Input { get { return m_Action3Input; } }

		/// <summary>
		/// Gets the action4 input.
		/// </summary>
		/// <value>The action4 input.</value>
		public RebindableAxisInputAttribute   action4Input { get { return m_Action4Input; } }

		/// <summary>
		/// Gets the action5 input.
		/// </summary>
		/// <value>The action5 input.</value>
        public AxisInputAttribute   action5Input { get { return m_Action5Input; } }

		/// <summary>
		/// Gets the switch input.
		/// </summary>
		/// <value>The switch input.</value>
        public RebindableButtonInputAttribute switchInput { get { return m_SwitchInput; } }

		/// <summary>
		/// Gets the dash input.
		/// </summary>
		/// <value>The dash input.</value>
		public RebindableButtonInputAttribute dashInput { get { return m_DashInput; } }

		/// <summary>
		/// Gets the snap camera input.
		/// </summary>
		/// <value>The snap camera input.</value>
		public RebindableButtonInputAttribute snapCameraInput { get { return m_SnapCameraInput; } }

		/// <summary>
		/// Gets the camera mode input.
		/// </summary>
		/// <value>The camera mode input.</value>
		public RebindableButtonInputAttribute cameraModeInput { get { return m_CameraModeInput; } }

		/// <summary>
		/// Gets the pause input.
		/// </summary>
		/// <value>The pause input.</value>
		public RebindableButtonInputAttribute pauseInput { get { return m_PauseInput; } }

		/// <summary>
		/// Gets the interaction input.
		/// </summary>
		/// <value>The interaction input.</value>
        public RebindableButtonInputAttribute interactionInput { get { return m_InteractionInput; } }

        #endregion
    }
}