﻿using Assets.AntonCoolpecker.Scripts.Concrete.Configuration.Controls;
using Hydra.HydraCommon.PropertyAttributes.InputAttributes;

namespace AntonCoolpecker.Concrete.Configuration.Controls.Mapping
{
	/// <summary>
	/// 	KeyboardControlScheme provides the keyboard input attributes for a control scheme.
	/// </summary>
	public class KeyboardControlScheme : IControlScheme
	{
		#region Variables

		private readonly RebindableAxisInputAttribute m_HorizontalInput;
		private readonly RebindableAxisInputAttribute m_VerticalInput;
		private readonly RebindableAxisInputAttribute m_HorizontalCameraInput;
		private readonly RebindableAxisInputAttribute m_VerticalCameraInput;
		private readonly RebindableButtonInputAttribute m_JumpInput;
		private readonly RebindableButtonInputAttribute m_Action1Input;
		private readonly RebindableButtonInputAttribute m_Action2Input;
		private readonly RebindableButtonInputAttribute m_Action3Input;
		private readonly RebindableAxisInputAttribute m_Action4Input;
		private readonly AxisInputAttribute m_Action5Input;
		private readonly RebindableButtonInputAttribute m_SwitchInput;
		private readonly RebindableButtonInputAttribute m_DashInput;
		private readonly RebindableButtonInputAttribute m_SnapCameraInput;
		private readonly RebindableButtonInputAttribute m_CameraModeInput;
		private readonly RebindableButtonInputAttribute m_PauseInput; 
        private readonly RebindableButtonInputAttribute m_InteractionInput;

		#endregion

		#region Constructors

        /// <summary>
        /// 	Initializes a new instance of the KeyboardControlScheme class.
        /// </summary>
        public KeyboardControlScheme()
		{
			// TODO: re-organize the actions in the input editor so we have one distinct set for keyboard and one for controllers
			// TODO: replace hard-coded keys, should be loaded from a save of some sorts

			m_HorizontalInput = new RebindableAxisInputAttribute(UnityEngine.KeyCode.D, UnityEngine.KeyCode.A, UnityEngine.KeyCode.RightArrow, UnityEngine.KeyCode.LeftArrow);
			m_VerticalInput = new RebindableAxisInputAttribute(UnityEngine.KeyCode.W, UnityEngine.KeyCode.S, UnityEngine.KeyCode.UpArrow, UnityEngine.KeyCode.DownArrow);
			m_HorizontalCameraInput = new RebindableAxisInputAttribute("Mouse X");
			m_VerticalCameraInput = new RebindableAxisInputAttribute("Mouse Y");
			m_JumpInput = new RebindableButtonInputAttribute(UnityEngine.KeyCode.Space);
			m_Action1Input = new RebindableButtonInputAttribute (UnityEngine.KeyCode.LeftShift);//new RebindableButtonInputAttribute("Action 1");
			m_Action1Input.SetAltKeyCode(UnityEngine.KeyCode.Mouse0);
			m_Action2Input = new RebindableButtonInputAttribute (UnityEngine.KeyCode.LeftAlt);//("Action 2");
			m_Action2Input.SetAltKeyCode(UnityEngine.KeyCode.Mouse1);
			m_Action3Input = new RebindableButtonInputAttribute(UnityEngine.KeyCode.F);//("Action 3");
			m_Action3Input.SetAltKeyCode(UnityEngine.KeyCode.Mouse2);
			// TODO: change action4Input into a buttonInputAttribute
			m_Action4Input = new RebindableAxisInputAttribute(UnityEngine.KeyCode.R, UnityEngine.KeyCode.Exclaim);//("Action 4");
			m_Action5Input = new AxisInputAttribute("Action 5");
			m_SwitchInput = new RebindableButtonInputAttribute(UnityEngine.KeyCode.Z);//("Switch");
			m_SwitchInput.SetAltKeyCode(UnityEngine.KeyCode.RightShift);
			m_DashInput = new RebindableButtonInputAttribute(UnityEngine.KeyCode.F);//("Dash");
			m_DashInput.SetAltKeyCode(UnityEngine.KeyCode.R);
			m_SnapCameraInput = new RebindableButtonInputAttribute(UnityEngine.KeyCode.Q);//("SnapCamera");
			m_SnapCameraInput.SetAltKeyCode(UnityEngine.KeyCode.JoystickButton6);
			m_CameraModeInput = new RebindableButtonInputAttribute(UnityEngine.KeyCode.Tab);//("CameraMode");
			m_PauseInput = new RebindableButtonInputAttribute(UnityEngine.KeyCode.Escape);//("Pause");
			//m_PauseInput.SetAltKeyCode(UnityEngine.KeyCode.JoystickButton7);
			m_InteractionInput = new RebindableButtonInputAttribute(UnityEngine.KeyCode.E);//("Interaction");
			m_InteractionInput.SetAltKeyCode(UnityEngine.KeyCode.RightShift);
		}

		#endregion

		#region Properties

		/// <summary>
		/// Gets the horizontal input.
		/// </summary>
		/// <value>The horizontal input.</value>
		public RebindableAxisInputAttribute horizontalInput { get { return m_HorizontalInput; } }

		/// <summary>
		/// Gets the vertical input.
		/// </summary>
		/// <value>The vertical input.</value>
		public RebindableAxisInputAttribute verticalInput { get { return m_VerticalInput; } }

		/// <summary>
		/// Gets the horizontal camera input.
		/// </summary>
		/// <value>The horizontal camera input.</value>
		public RebindableAxisInputAttribute horizontalCameraInput { get { return m_HorizontalCameraInput; } }

		/// <summary>
		/// Gets the vertical camera input.
		/// </summary>
		/// <value>The vertical camera input.</value>
		public RebindableAxisInputAttribute verticalCameraInput { get { return m_VerticalCameraInput; } }

		/// <summary>
		/// Gets the jump input.
		/// </summary>
		/// <value>The jump input.</value>
		public RebindableButtonInputAttribute jumpInput { get { return m_JumpInput; } }

		/// <summary>
		/// Gets the action 1 input.
		/// </summary>
		/// <value>The action 1 input.</value>
		public RebindableButtonInputAttribute action1Input { get { return m_Action1Input; } }

		/// <summary>
		/// Gets the action 2 input.
		/// </summary>
		/// <value>The action 2 input.</value>
		public RebindableButtonInputAttribute action2Input { get { return m_Action2Input; } }

		/// <summary>
		/// Gets the action 3 input.
		/// </summary>
		/// <value>The action 3 input.</value>
		public RebindableButtonInputAttribute action3Input { get { return m_Action3Input; } }

		/// <summary>
		/// Gets the action 4 input.
		/// </summary>
		/// <value>The action 4 input.</value>
		public RebindableAxisInputAttribute action4Input { get { return m_Action4Input; } }

		/// <summary>
		/// Gets the action 5 input.
		/// </summary>
		/// <value>The action 5 input.</value>
		public AxisInputAttribute action5Input { get { return m_Action5Input; } }

		/// <summary>
		/// Gets the switch input.
		/// </summary>
		/// <value>The switch input.</value>
        public RebindableButtonInputAttribute switchInput { get { return m_SwitchInput; } }

		/// <summary>
		/// Gets the dash input.
		/// </summary>
		/// <value>The dash input.</value>
		public RebindableButtonInputAttribute dashInput { get { return m_DashInput; } }

		/// <summary>
		/// Gets the snap camera input.
		/// </summary>
		/// <value>The snap camera input.</value>
		public RebindableButtonInputAttribute snapCameraInput { get { return m_SnapCameraInput; } }

		/// <summary>
		/// Gets the camera mode input.
		/// </summary>
		/// <value>The camera mode input.</value>
		public RebindableButtonInputAttribute cameraModeInput { get { return m_CameraModeInput; } }

		/// <summary>
		/// Gets the pause input.
		/// </summary>
		/// <value>The pause input.</value>
		public RebindableButtonInputAttribute pauseInput { get { return m_PauseInput; } }

		/// <summary>
		/// Gets the interaction input.
		/// </summary>
		/// <value>The interaction input.</value>
        public RebindableButtonInputAttribute interactionInput { get { return m_InteractionInput; } }

        #endregion
    }
}