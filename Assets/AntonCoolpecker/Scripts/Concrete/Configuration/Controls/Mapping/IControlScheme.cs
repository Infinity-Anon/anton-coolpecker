﻿using Assets.AntonCoolpecker.Scripts.Concrete.Configuration.Controls;
using Hydra.HydraCommon.PropertyAttributes.InputAttributes;

namespace AntonCoolpecker.Concrete.Configuration.Controls.Mapping
{
	/// <summary>
	/// 	IControlScheme defines all of the input attributes for a control scheme.
	/// </summary>
	public interface IControlScheme
	{
		#region Properties/Variables

		/// <summary>
		/// Gets the horizontal input.
		/// </summary>
		/// <value>The horizontal input.</value>
		RebindableAxisInputAttribute horizontalInput { get; }

		/// <summary>
		/// Gets the vertical input.
		/// </summary>
		/// <value>The vertical input.</value>
		RebindableAxisInputAttribute verticalInput { get; }

		/// <summary>
		/// Gets the horizontal camera input.
		/// </summary>
		/// <value>The horizontal camera input.</value>
		RebindableAxisInputAttribute horizontalCameraInput { get; }

		/// <summary>
		/// Gets the vertical camera input.
		/// </summary>
		/// <value>The vertical camera input.</value>
		RebindableAxisInputAttribute verticalCameraInput { get; }

		/// <summary>
		/// Gets the jump input.
		/// </summary>
		/// <value>The jump input.</value>
		RebindableButtonInputAttribute jumpInput { get; }

		/// <summary>
		/// Gets the action 1 input.
		/// </summary>
		/// <value>The action 1 input.</value>
		RebindableButtonInputAttribute action1Input { get; }

		/// <summary>
		/// Gets the action 2 input.
		/// </summary>
		/// <value>The action 2 input.</value>
		RebindableButtonInputAttribute action2Input { get; }

		/// <summary>
		/// Gets the action 3 input.
		/// </summary>
		/// <value>The action 3 input.</value>
		RebindableButtonInputAttribute action3Input { get; }

		/// <summary>
		/// Gets the action 4 input.
		/// </summary>
		/// <value>The action 4 input.</value>
		RebindableAxisInputAttribute action4Input { get; }

		/// <summary>
		/// Gets the action 5 input.
		/// </summary>
		/// <value>The action 5 input.</value>
        AxisInputAttribute action5Input { get; }

		/// <summary>
		/// Gets the switch input.
		/// </summary>
		/// <value>The switch input.</value>
		RebindableButtonInputAttribute switchInput { get; }

		/// <summary>
		/// Gets the dash input.
		/// </summary>
		/// <value>The dash input.</value>
		RebindableButtonInputAttribute dashInput { get; }

		/// <summary>
		/// Gets the snap camera input.
		/// </summary>
		/// <value>The snap camera input.</value>
		RebindableButtonInputAttribute snapCameraInput { get; }

		/// <summary>
		/// Gets the camera mode input.
		/// </summary>
		/// <value>The camera mode input.</value>
		RebindableButtonInputAttribute cameraModeInput { get; }

		/// <summary>
		/// Gets the pause input.
		/// </summary>
		/// <value>The pause input.</value>
		RebindableButtonInputAttribute pauseInput { get; }

		/// <summary>
		/// Gets the interaction input.
		/// </summary>
		/// <value>The interaction input.</value>
		RebindableButtonInputAttribute interactionInput { get; }

		#endregion
    }
}
