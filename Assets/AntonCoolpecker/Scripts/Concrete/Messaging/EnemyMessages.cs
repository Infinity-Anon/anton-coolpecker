﻿using AntonCoolpecker.Abstract.Enemies.States;
using AntonCoolpecker.Abstract.StateMachine;
using AntonCoolpecker.Concrete.Enemies;
using AntonCoolpecker.Concrete.Utils;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Messaging
{
	/// <summary>
	/// Enemy messages - Used for broadcasting enemy-related messages.
	/// </summary>
	public static class EnemyMessages
	{
		#region Broadcasts

		/// <summary>
		/// Broadcasts when the enemy changes state.
		/// </summary>
		/// <param name="enemy">Enemy.</param>
		/// <param name="changeInfo">Change info.</param>
		public static void BroadcastOnStateChange(EnemyController enemy, StateChangeInfo<AbstractEnemyState> changeInfo)
		{
			enemy.BroadcastMessage("OnStateChange", changeInfo, SendMessageOptions.RequireReceiver);
		}

		/// <summary>
		/// Broadcasts when the enemy is damaged/hit.
		/// </summary>
		/// <param name="enemy">Enemy.</param>
		/// <param name="collision">Collision.</param>
		public static void BroadcastOnDamage(EnemyController enemy, CharacterLocomotorCollisionData collision)
		{
			enemy.BroadcastMessage("OnDamage", collision, SendMessageOptions.RequireReceiver);
		}

		#endregion
	}
}