﻿using UnityEngine;

namespace AntonCoolpecker.Concrete.Messaging
{
	/// <summary>
	/// Collectable messages - Used for broadcasting collectable-related messages
	/// </summary>
	public static class CollectableMessages
	{
		#region Broadcasts

		/// <summary>
		/// Broadcasts when a collectable has been collected.
		/// </summary>
		/// <param name="gameObject">Game object.</param>
		public static void BroadcastOnCollected(GameObject gameObject)
		{
			gameObject.BroadcastMessage("OnCollected", SendMessageOptions.RequireReceiver);
		}

		#endregion
	}
}