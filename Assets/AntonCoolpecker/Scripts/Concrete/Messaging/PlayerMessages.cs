﻿using AntonCoolpecker.Abstract.Player;
using AntonCoolpecker.Abstract.Player.States;
using AntonCoolpecker.Abstract.StateMachine;
using AntonCoolpecker.Concrete.Utils;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Messaging
{
	/// <summary>
	/// Player messages - Used for broadcasting player-related messages.
	/// </summary>
	public static class PlayerMessages
	{
		#region Broadcasts

		/// <summary>
		/// Broadcasts when the player jumps.
		/// </summary>
		/// <param name="player">Player.</param>
		public static void BroadcastOnJump(AbstractPlayerController player)
		{
			player.gameObject.BroadcastMessage("OnJump", SendMessageOptions.RequireReceiver);
		}

		/// <summary>
		/// Broadcasts when the player is damaged/hit.
		/// </summary>
		/// <param name="player">Player.</param>
		/// <param name="collision">Collision.</param>
		public static void BroadcastOnDamage(AbstractPlayerController player, CharacterLocomotorCollisionData collision)
		{
			player.gameObject.BroadcastMessage("OnDamage", collision, SendMessageOptions.RequireReceiver);
		}

		/// <summary>
		/// Broadcasts when the player changes state.
		/// </summary>
		/// <param name="player">Player.</param>
		/// <param name="changeInfo">Change info.</param>
		public static void BroadcastOnStateChange(AbstractPlayerController player,
												  StateChangeInfo<AbstractPlayerState> changeInfo)
		{
			player.gameObject.BroadcastMessage("OnStateChange", changeInfo, SendMessageOptions.RequireReceiver);
		}

		/// <summary>
		/// Broadcasts when the player has switched to another character(Anton/Coolpecker).
		/// </summary>
		/// <param name="player">Player.</param>
		public static void BroadcastOnSwapEnter(AbstractPlayerController player)
		{
			player.gameObject.BroadcastMessage("OnSwapEnter", SendMessageOptions.RequireReceiver);
		}

		/// <summary>
		/// Broadcasts when a player animation event occurs.
		/// </summary>
		/// <param name="player">Player.</param>
		/// <param name="animationEvent">Animation event.</param>
		public static void BroadcastOnAnimationEvent(AbstractPlayerController player, AnimationEvent animationEvent)
		{
			player.gameObject.BroadcastMessage("OnAnimationEvent", animationEvent, SendMessageOptions.RequireReceiver);
		}

		#endregion
	}
}