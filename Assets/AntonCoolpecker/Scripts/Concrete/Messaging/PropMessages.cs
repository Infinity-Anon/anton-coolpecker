using AntonCoolpecker.Abstract.Props.States;
using AntonCoolpecker.Abstract.StateMachine;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Messaging
{
	/// <summary>
	/// Property messages - Used for broadcasting prop-related messages
	/// </summary>
	public static class PropMessages
	{
		#region Broadcasts

		/// <summary>
		/// Broadcasts on state change.
		/// </summary>
		/// <param name="gameObject">Game object.</param>
		/// <param name="changeInfo">Change info.</param>
		public static void BroadcastOnStateChange(GameObject gameObject, StateChangeInfo<AbstractPropState> changeInfo)
		{
			gameObject.BroadcastMessage("OnStateChange", changeInfo, SendMessageOptions.RequireReceiver);
		}

		#endregion
	}
}