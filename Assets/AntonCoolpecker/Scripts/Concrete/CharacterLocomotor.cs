﻿using AntonCoolpecker.Abstract.Platforms;
using AntonCoolpecker.Concrete.Utils;
using AntonCoolpecker.Utils;
using AntonCoolpecker.Utils.Time;
using System.Collections.Generic;
using Hydra.HydraCommon.Abstract;
using Hydra.HydraCommon.Extensions.Colliders;
using Hydra.HydraCommon.Utils;
using Hydra.HydraCommon.Utils.Shapes._3d;
using UnityEngine;

namespace AntonCoolpecker.Concrete
{
	[RequireComponent(typeof(CharacterController))]
	public class CharacterLocomotor : HydraMonoBehaviour
	{
		#region Variables

		[SerializeField] private float m_GroundedDistance = 0.2f;
		[SerializeField] private float m_SkywardsDistance = 10f; //Length of skywards collider cast

		[SerializeField] private bool m_UseGravity = true;
		[SerializeField] private Vector3 m_GravityScale = Vector3.one;
		[SerializeField] private bool m_UseFriction = true;
		[SerializeField] private Vector3 m_Friction = new Vector3(0.1f, 0.0f, 0.1f);
		[SerializeField] private float m_SoftGroundedTime = 0.1f;
		[SerializeField] private float m_skinWidth;

		private Vector3 m_Velocity;
		private CharacterController m_CachedCharacterController;
		private float m_LastGroundedTime;
		private List<RaycastHit> m_ColliderCastInfo;
		private Rigidbody m_Rigidy;
		private Vector3 m_OriginalGravityScale;

		#endregion

		#region Properties

		/// <summary>
		/// 	Gets the character controller.
		/// </summary>
		/// <value>The character controller.</value>
		public CharacterController characterController
		{
			get { return m_CachedCharacterController ?? (m_CachedCharacterController = GetComponent<CharacterController>()); }
		}

		/// <summary>
		/// 	Gets the rigidbody(If there is one).
		/// </summary>
		/// <value>The character controller.</value>
		public Rigidbody rigidy { get { return m_Rigidy; } set { m_Rigidy = value; } }

		/// <summary>
		/// 	Gets or sets a value indicating whether this uses gravity.
		/// </summary>
		/// <value><c>true</c> if use gravity; otherwise, <c>false</c>.</value>
		[Tweakable("Gravity")] public bool useGravity { get { return m_UseGravity; } set { m_UseGravity = value; } }

		/// <summary>
		/// 	Gets the gravity.
		/// </summary>
		/// <value>The gravity.</value>
		public Vector3 gravity { get { return Vector3.Scale(m_GravityScale, Physics.gravity); } }

		/// <summary>
		/// 	Gets or sets the gravity scale.
		/// </summary>
		/// <value>The gravity scale.</value>
		public Vector3 gravityScale { get { return m_GravityScale; } set { m_GravityScale = value; } }
		[Tweakable("Gravity")] public float gravityScaleX { get { return m_GravityScale.x; } set { m_GravityScale.x = value; } }
		[Tweakable("Gravity")] public float gravityScaleY { get { return m_GravityScale.y; } set { m_GravityScale.y = value; } }
		[Tweakable("Gravity")] public float gravityScaleZ { get { return m_GravityScale.z; } set { m_GravityScale.z = value; } }

		/// <summary>
		/// 	Gets the original gravity scale.
		/// </summary>
		/// <value>The original gravity scale.</value>
		public Vector3 originalGravityScale { get { return m_OriginalGravityScale; } }

		/// <summary>
		/// 	Gets or sets a value indicating whether this uses friction.
		/// </summary>
		/// <value><c>true</c> if use friction; otherwise, <c>false</c>.</value>
		[Tweakable("Friction")] public bool useFriction { get { return m_UseFriction; } set { m_UseFriction = value; } }

		/// <summary>
		/// 	Gets or sets the friction.
		/// </summary>
		/// <value>The friction.</value>
		public Vector3 friction { get { return m_Friction; } set { m_Friction = value; } }
		[Tweakable("Friction")] public float frictionX { get { return m_Friction.x; } set { m_Friction.x = value; } }
		[Tweakable("Friction")] public float frictionY { get { return m_Friction.y; } set { m_Friction.y = value; } }
		[Tweakable("Friction")] public float frictionZ { get { return m_Friction.z; } set { m_Friction.z = value; } }

		/// <summary>
		/// 	Gets or sets the distance of the raycast hit(s) against the sky.
		/// </summary>
		/// <value>The sky rayhit distance.</value>
		public float skyRayHitDistance { get { return m_SkywardsDistance; } set { m_SkywardsDistance = value; } }

		/// <summary>
		/// 	Gets or sets the movement vector for the current tick.
		/// </summary>
		/// <value>The velocity.</value>
		public Vector3 velocity { get { return m_Velocity; } set { m_Velocity = value; } }

		/// <summary>
		/// 	Gets the velocity with a zeroed Y component. Sets the X and Z components of the velocity.
		/// </summary>
		/// <value>The flat velocity.</value>
		public Vector3 flatVelocity
		{
			get { return new Vector3(m_Velocity.x, 0.0f, m_Velocity.z); }
			set { m_Velocity = new Vector3(value.x, m_Velocity.y, value.z); }
		}

		/// <summary>
		/// 	Gets a value indicating whether this is grounded.
		/// </summary>
		/// <value><c>true</c> if is grounded; otherwise, <c>false</c>.</value>
		public bool isGrounded { get { return GroundedCast().collider != null; } }

		/// <summary>
		/// 	Returns true if the character has been grounded in a certain amount of time.
		/// 	This is useful when the character is moving along a slope and oscillates between
		/// 	grounded and not grounded.
		/// </summary>
		/// <value><c>true</c> if is soft grounded; otherwise, <c>false</c>.</value>
		public bool isSoftGrounded
		{
			get
			{
				if (isGrounded)
					return true;

				return GameTime.time - m_LastGroundedTime < m_SoftGroundedTime;
			}
		}

		/// <summary>
		///		Copy of characterController's Skin Width, which, for unknown reasons, is not accessible through code.
		///		This is used to correct for characters sometimes sinking into the ground by the depth of their skinWidth.
		/// </summary>
		public float skinWidth { get { return m_skinWidth; } set { m_skinWidth = value; } }

		#endregion

		#region Messages

		/// <summary>
		/// 	Called when the component's parent is instantiated.
		/// </summary>
		protected override void Awake()
		{
			base.Awake();

			m_OriginalGravityScale = m_GravityScale;
		}

		/// <summary>
		/// 	Called every physics timestep.
		/// </summary>
		protected override void FixedUpdate()
		{
			base.FixedUpdate();

			if (isGrounded)
				m_LastGroundedTime = GameTime.time;

			Vector3 motion = m_Velocity * GameTime.deltaTime;

			Move(motion);

			if (m_UseGravity)
				m_Velocity += gravity * GameTime.deltaTime;

			if (m_UseFriction)
				m_Velocity -= Vector3.Scale(m_Velocity, m_Friction);
		}

		/// <summary>
		/// 	OnControllerColliderHit is called when the CharacterController hits a collider while performing a Move.
		/// </summary>
		/// <param name="hit">Hit.</param>
		protected override void OnControllerColliderHit(ControllerColliderHit hit)
		{
			base.OnControllerColliderHit(hit);

			//NOTE: The CollisionData structure given to the object is from the POV of THIS object, because it will get
			//      passed back to IDamageable.Damage()
			CharacterLocomotorCollisionData coll = new CharacterLocomotorCollisionData(hit.controller, hit.point);
			hit.gameObject.SendMessage("OnCharacterLocomotorCollision", coll, SendMessageOptions.DontRequireReceiver);
		}

		#endregion

		#region Methods

		/// <summary>
		/// 	Sets the locomotor velocity to the current velocity of the CharacterController.
		/// </summary>
		/// <returns>The velocity.</returns>
		public Vector3 UpdateVelocity()
		{
			m_Velocity = characterController.velocity;
			return m_Velocity;
		}

		/// <summary>
		/// 	Resets the gravity scale.
		/// </summary>
		public void ResetGravityScale()
		{
			m_GravityScale = m_OriginalGravityScale;
		}

		/// <summary>
		/// 	Moves the character locomotor. Handles the case where the CharacterController is disabled.
		/// </summary>
		/// <returns>The collision flags.</returns>
		/// <param name="direction">Direction.</param>
		public CollisionFlags Move(Vector3 direction)
		{
			if (!characterController.enabled)
			{
				m_Rigidy = GetComponent<Rigidbody>();

				if (m_Rigidy != null)
				{
					m_Rigidy.transform.position += direction;
					//direction = new Vector3(direction.x * 10f, direction.y * 10f, direction.z * 10f);
					//m_Rigidy.AddForce(direction);
					//CollisionFlags outputo = m_Rigidy.AddForce(direction);
					//http://wiki.unity3d.com/index.php?title=DontGoThroughThings#C.23_-_DontGoThroughThings.cs
					return CollisionFlags.None;
					//return outputo;
				}

				else
				{
					transform.position += direction;
					return CollisionFlags.None;
				}
			}

			CollisionFlags output = characterController.Move(direction);

			// Prevent gravity from accumulating.
			if ((output & CollisionFlags.Below) != 0)
				m_Velocity.y = HydraMathUtils.Max(0.0f, m_Velocity.y);

			return output;
		}

		/// <summary>
		/// 	Moves the character locomotor towards the given position at the given speed.
		/// 	Does not overshoot the destination.
		/// </summary>
		/// <returns>The collision flags.</returns>
		/// <param name="position">Position.</param>
		/// <param name="speed">Speed.</param>
		/// <param name="deltaTime">Delta time.</param>
		public CollisionFlags MoveTowards(Vector3 position, float speed, float deltaTime)
		{
			Vector3 delta = Vector3.ClampMagnitude(position - transform.position, speed * deltaTime);

			return Move(delta);
		}

		/// <summary>
		/// 	Returns the collision info of the collider against the surface it rests on.
		/// </summary>
		/// <returns>The cast.</returns>
		public RaycastHit GroundedCast()
		{
			Vector3 down = Vector3.down * m_GroundedDistance;
			RaycastHit rh = ColliderCast(down, true);
			MovingPlatform mp = null;
			if (rh.collider != null)
				mp = rh.collider.GetComponent<MovingPlatform>();
			if (mp != null)
				mp.Add(this);
			return rh;
		}

		/// <summary>
		/// 	Returns the collision info of the collider against the surface it rests on.
		/// </summary>
		/// <returns>The cast.</returns>
		public RaycastHit SkywardsCast()
		{
			Vector3 up = Vector3.up * m_SkywardsDistance;
			return ColliderCast(up, true);
		}

		/// <summary>
		/// 	Performs a cast of the player collider with the world.
		/// </summary>
		/// <returns>The collision info.</returns>
		/// <param name="direction">Direction.</param>
		/// <param name="ignoreTriggers">If set to <c>true</c> ignore triggers.</param>
		public RaycastHit ColliderCast(Vector3 direction, bool ignoreTriggers)
		{
			// Re-use the same list instead of creating a new one for each call, to save on memory allocations
			if (m_ColliderCastInfo == null)
				m_ColliderCastInfo = new List<RaycastHit>();

			Capsule capsule = characterController.ToCapsule();

			float radius = capsule.radius;
			float distance = direction.magnitude;

			RaycastHit[] hitInfos = Physics.CapsuleCastAll(capsule.point1, capsule.point2, radius, direction.normalized, distance);

			m_ColliderCastInfo.Clear();

			for (int index = 0; index < hitInfos.Length; index++)
			{
				RaycastHit hitInfo = hitInfos[index];
				if (FilterHitInfo(hitInfo, direction, ignoreTriggers))
					m_ColliderCastInfo.Add(hitInfo);
			}

			return Average(m_ColliderCastInfo);
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// 	Returns the average of the raycast hits.
		/// 	Collider is set to the first collider in the list.
		/// </summary>
		/// <param name="collisionInfo">Collision info.</param>
		private RaycastHit Average(List<RaycastHit> collisionInfo)
		{
			int count = collisionInfo.Count;
			if (count == 0)
				return default(RaycastHit);

			Vector3 pointSum = Vector3.zero;
			Vector3 normalSum = Vector3.zero;

			for (int index = 0; index < count; index++)
			{
				RaycastHit hitInfo = collisionInfo[index];

				pointSum += hitInfo.point;
				normalSum += hitInfo.normal;
			}

			RaycastHit output = collisionInfo[0];
			output.point = pointSum / count;
			output.normal = (normalSum / count).normalized;

			return output;
		}

		/// <summary>
		/// 	Returns false if the RaycastHit should be ignored.
		/// </summary>
		/// <returns><c>true</c>, if hit info was filtered, <c>false</c> otherwise.</returns>
		/// <param name="hitInfo">Hit info.</param>
		/// <param name="direction">Direction.</param>
		/// <param name="ignoreTriggers">If set to <c>true</c> ignore triggers.</param>
		private bool FilterHitInfo(RaycastHit hitInfo, Vector3 direction, bool ignoreTriggers)
		{
			// Ignore collisions with self
			if (hitInfo.collider == collider)
				return false;

			// Ignore triggers
			if (ignoreTriggers && hitInfo.collider.isTrigger)
				return false;

			// Ignore normals that face along the cast direction
			if (Vector3.Dot(direction, hitInfo.normal) >= 0.0f)
				return false;

			return true;
		}

		#endregion
	}
}