﻿using AntonCoolpecker.Abstract.Player;
using AntonCoolpecker.Concrete.Utils;
using AntonCoolpecker.Utils.Time;
using UnityEngine;

/// <summary>
/// Handles moving fake(Non-interactive) DOODADs to the poles on the DOODAD door.
/// </summary>
public class FakeDisplayDoodad : MonoBehaviour 
{
	#region Variables

    private Vector3 m_Target;
	private Vector3 m_StartPosition;
	private float m_Speed;
    private AbstractPlayerController m_Player;
    private bool m_TrueSeekPlayerFalseSeekTarget;
	private Timer m_Timer;

	#endregion

	#region Functions

	/// <summary>
	/// Start this instance.
	/// </summary>
	void Start () 
	{
        //Make sure everything gets initialised
        m_TrueSeekPlayerFalseSeekTarget = false;     
        m_Player = GameObject.FindGameObjectWithTag("Player").GetComponent<AbstractPlayerController>();
        m_Timer = new Timer();
    }

    /// <summary>
    /// Move towards a specified target
    /// </summary>
    /// <param name="target">target to move towards</param>
    /// <param name="speed"> movement speed</param>
    public void Set(Vector3 target, float speed)
    {
        m_Target = target;
        m_Target.z += 1f;
        m_Speed = speed;
        m_TrueSeekPlayerFalseSeekTarget = false;
    }

    /// <summary>
    /// Have the doodad return to the player
    /// </summary>
    /// <param name="time">length in seconds to get to player</param>
    public void Return(float time)
    {
        m_TrueSeekPlayerFalseSeekTarget = true;
        m_Timer.Reset();
        m_StartPosition = transform.position;
        m_Timer.maxTime = time;
    }

	/// <summary>
	/// Update this instance.
	/// </summary>
	void Update () 
	{
        if (m_TrueSeekPlayerFalseSeekTarget)
        {
            if (m_Player == null)
            {
                m_Player = GameObject.FindGameObjectWithTag("Player").GetComponent<AbstractPlayerController>();
            }

            if (m_Timer.complete)
            {
                gameObject.SetActive(false);
            }

            transform.position = Vector3.Lerp(m_StartPosition, m_Player.transform.position,m_Timer.elapsedAsPercentage);
        }
        else
        {
            transform.localPosition = Vector3.MoveTowards(transform.localPosition, m_Target, GameTime.deltaTime * m_Speed);
        }
	}

	#endregion
}