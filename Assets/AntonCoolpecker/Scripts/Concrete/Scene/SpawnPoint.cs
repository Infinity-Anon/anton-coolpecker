﻿using System.Collections.Generic;
using Hydra.HydraCommon.Abstract;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Scene
{
	/// <summary>
	/// 	Spawn point.
	/// </summary>
	public class SpawnPoint : HydraMonoBehaviour
	{
		#region Variables

		private List<Renderer> m_Renderers;

		#endregion

		#region Messages

		/// <summary>
		/// 	Called when the component is enabled.
		/// </summary>
		protected override void OnEnable()
		{
			base.OnEnable();

			if (Application.isPlaying)
				DisableRenderers();
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// 	Disables the renderers.
		/// </summary>
		private void DisableRenderers()
		{
			if (m_Renderers == null)
				m_Renderers = new List<Renderer>();

			m_Renderers.Clear();

			GetComponentsInChildren(m_Renderers);

			for (int index = 0; index < m_Renderers.Count; index++)
				m_Renderers[index].enabled = false;
		}

		#endregion
	}
}