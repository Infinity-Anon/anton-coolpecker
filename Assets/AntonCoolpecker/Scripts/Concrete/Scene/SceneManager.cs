﻿using AntonCoolpecker.Abstract.Player;
using AntonCoolpecker.Concrete.Cameras;
using AntonCoolpecker.Concrete.Player;
using Hydra.HydraCommon.Abstract;
using Hydra.HydraCommon.Extensions;
using Hydra.HydraCommon.Utils;
using System;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Scene
{
	/// <summary>
	/// 	Scene manager prefabs.
	/// </summary>
	[Serializable]
	public class SceneManagerPrefabs
	{
		#region Variables

		[SerializeField] private CameraController m_PlayerCameraPrefab;
		[SerializeField] private SpawnPoint m_SpawnPointPrefab;
		[SerializeField] private PlayerSwapper m_PlayerSwapperPrefab;

		#endregion

		#region Properties

		public CameraController playerCameraPrefab { get { return m_PlayerCameraPrefab; } }
		public SpawnPoint spawnPointPrefab { get { return m_SpawnPointPrefab; } }
		public PlayerSwapper playerSwapperPrefab { get { return m_PlayerSwapperPrefab; } }

		#endregion
	}

	/// <summary>
	/// 	SceneManager is responsible for handling the player-related entities in the scene, including the camera, spawn point and swapper.
	/// </summary>
	[ExecuteInEditMode]
	public class SceneManager : SingletonHydraMonoBehaviour<SceneManager>
	{
		#region Variables

		[SerializeField] private SceneManagerPrefabs m_Prefabs;
		[SerializeField] private CameraController m_PlayerCamera;
		[SerializeField] private SpawnPoint m_SpawnPoint;
		[SerializeField] private PlayerSwapper m_PlayerSwapper;
		[SerializeField] private Material m_CameraSkybox;

		#endregion

		#region Properties

		/// <summary>
		/// 	Gets the prefab factory.
		/// </summary>
		/// <value>The prefab factory.</value>
		public SceneManagerPrefabs prefabs { get { return m_Prefabs ?? (m_Prefabs = new SceneManagerPrefabs()); } }

		/// <summary>
		/// 	Gets the player camera.
		/// </summary>
		/// <value>The player camera.</value>
		public CameraController playerCamera { get { return m_PlayerCamera; } }

		/// <summary>
		/// 	Gets the spawn point.
		/// </summary>
		/// <value>The spawn point.</value>
		public SpawnPoint spawnPoint { get { return m_SpawnPoint; } }

		/// <summary>
		/// 	Gets the player swapper.
		/// </summary>
		/// <value>The player swapper.</value>
		public PlayerSwapper playerSwapper { get { return m_PlayerSwapper; } }

		/// <summary>
		/// 	Gets the camera skybox.
		/// </summary>
		/// <value>The camera skybox.</value>
		public Material cameraSkybox { get { return m_CameraSkybox; } }

		#endregion

		#region Messages

		/// <summary>
		/// 	Called when the component is enabled.
		/// </summary>
		protected override void OnEnable()
		{
			base.OnEnable();

			InstantiateDependencies();

			if (!Application.isPlaying)
				return;

			m_PlayerCamera.skybox = m_CameraSkybox;
			m_PlayerSwapper.playerCamera = m_PlayerCamera;
			AbstractPlayerController player = m_PlayerSwapper.GetActivePlayer();
			player.transform.Copy(m_SpawnPoint);
		}

		#endregion

		#region Methods

		/// <summary>
		/// 	Instantiates the dependencies.
		/// </summary>
		public void InstantiateDependencies()
		{
			Camera mainCamera = Camera.main;

			if (ObjectUtils.LazyInstantiateOrFind(prefabs.playerCameraPrefab, ref m_PlayerCamera))
			{
				if (mainCamera != m_PlayerCamera && mainCamera != null)
				{
					Debug.Log(string.Format("Removing {0}", mainCamera.name));
					ObjectUtils.SafeDestroyGameObject(mainCamera);
				}
			}

			ObjectUtils.LazyInstantiateOrFind(prefabs.spawnPointPrefab, ref m_SpawnPoint);
			ObjectUtils.LazyInstantiateOrFind(prefabs.playerSwapperPrefab, ref m_PlayerSwapper);
		}

		#endregion
	}
}