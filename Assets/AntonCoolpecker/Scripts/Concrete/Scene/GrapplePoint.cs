﻿using AntonCoolpecker.Concrete.Player.Anton;
using AntonCoolpecker.Concrete.Utils;
using Hydra.HydraCommon.Abstract;
using System.Collections.Generic;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Scene
{
	/// <summary>
	/// 	GrapplePointComparer sorts GrapplePoints in terms of which is more
	/// 	ideal for Anton to grapple at a given time.
	/// </summary>
	public class GrapplePointComparer : IComparer<GrapplePoint>
	{
		#region Variables

		public AntonController anton { get; set; }
		public Vector3 origin { get; set; }

		#endregion

		#region Methods

		/// <summary>
		/// 	Compares two objects and returns a value indicating whether one
		/// 	is less than, equal to, or greater than the other.
		/// </summary>
		/// <param name="a">A.</param>
		/// <param name="b">B.</param>
		public int Compare(GrapplePoint a, GrapplePoint b)
		{
			Vector3 toA = a.transform.position - origin;
			Vector3 toB = b.transform.position - origin;

			float distanceA = toA.magnitude;
			float distanceB = toB.magnitude;

			float antonAngleA = Vector3.Angle(anton.transform.forward, toA);
			float antonAngleB = Vector3.Angle(anton.transform.forward, toB);

			float deltaA = distanceA * antonAngleA;
			float deltaB = distanceB * antonAngleB;

			if (deltaA < deltaB)
				return -1;

			if (deltaA > deltaB)
				return 1;

			return 0;
		}

		#endregion
	}

	/// <summary>
	/// 	GrapplePoint represents an object that Anton can use to swing.
	/// 	
	/// 	Currently we use Physics.OverlapSphere to find GrapplePoints in
	/// 	a given radius, so GrapplePoints require a collider.
	/// </summary>
	[RequireComponent(typeof(Collider))]
	public class GrapplePoint : HydraMonoBehaviour
	{
		#region Variables

		private static GrapplePointComparer s_Comparer;
		private static List<GrapplePoint> s_Unsorted;
        private Timer s_CooldownTimer;

		#endregion

		#region Properties

        public Timer GrappleTimer { get { return s_CooldownTimer; } }

		#endregion

		#region Constructors

		/// <summary>
		/// 	Initializes the GrapplePoint class.
		/// </summary>
		static GrapplePoint()
		{
			s_Comparer = new GrapplePointComparer();
			s_Unsorted = new List<GrapplePoint>();
		}

		#endregion

		#region Methods

		/// <summary>
		/// 	Performs a raycast to check if there is a clear line of sight from the origin
		/// 	to the grapple point.
		/// </summary>
		/// <returns><c>true</c> if this instance is unobscured; otherwise, <c>false</c>.</returns>
		/// <param name="origin">Origin.</param>
		public bool IsUnobscured(Vector3 origin)
		{
            LayerMask layerToIgnore = 1 << 12; //IGNORE PLAYER LAYER
            layerToIgnore |= 1 << 18; //IGNORE ATTACK HITBOX LAYER

            RaycastHit[] hitInfos = Physics.RaycastAll(origin, transform.position - origin, Vector3.Distance(origin, transform.position), ~layerToIgnore); //Ignores player hitbox

			for (int index = 0; index < hitInfos.Length; index++)
			{
				RaycastHit hitInfo = hitInfos[index];
                if (hitInfo.collider != null && hitInfo.collider != collider)
                    return false;
			}

			return true;
		}

        #endregion

        #region Override Methods

        protected override void Awake()
        {
            base.Awake();
            s_CooldownTimer = new Timer();
            s_CooldownTimer.maxTime = 0;
            s_CooldownTimer.Resume();
        }

        #endregion

        #region Static Methods

        /// <summary>
        /// 	Finds the ideal grapple point for Anton.
        /// </summary>
        /// <returns>The grapple point.</returns>
        /// <param name="anton">Anton.</param>
        /// <param name="origin">Origin.</param>
        /// <param name="radius">Radius.</param>
        public static GrapplePoint FindIdeal(AntonController anton, Vector3 origin, float radius)
		{
			s_Unsorted.Clear();

			FindUnobscuredInRadius(origin, radius, s_Unsorted, anton);

			s_Comparer.anton = anton;
			s_Comparer.origin = origin;

			s_Unsorted.Sort(s_Comparer);

            return s_Unsorted.Count > 0 ? s_Unsorted[0] : null;
		}

		/// <summary>
		/// 	Finds unobscured grapple points in the given radius.
		/// </summary>
		/// <param name="origin">Origin.</param>
		/// <param name="radius">Radius.</param>
		/// <param name="output">Output.</param>
		public static void FindUnobscuredInRadius(Vector3 origin, float radius, List<GrapplePoint> output, AntonController anton)
		{
			Collider[] colliders = Physics.OverlapSphere(origin, radius);

			for (int index = 0; index < colliders.Length; index++)
			{
				Collider collider = colliders[index];
				GrapplePoint grapplePoint = collider.GetComponent<GrapplePoint>();

				if (grapplePoint == null)
					continue;

				if (!grapplePoint.IsUnobscured(origin))
					continue;

                if (!grapplePoint.GrappleTimer.complete)
                    continue;

                if (anton.m_GrappleForward)
                {
                    //Compare Angle
                    Vector3 dir = Vector3.Normalize(grapplePoint.transform.position - anton.transform.position);

                    if (Vector3.Angle(anton.transform.forward, dir) > anton.m_GrappleForwardAngleMax)
                        continue;
                }

                output.Add(grapplePoint);
			}
		}

		#endregion
	}
}