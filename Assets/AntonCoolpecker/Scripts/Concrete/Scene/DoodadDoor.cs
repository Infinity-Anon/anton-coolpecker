﻿using AntonCoolpecker.Abstract.Player;
using AntonCoolpecker.Concrete.Collectables;
using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using AntonCoolpecker.Concrete.Menus.HUD;
using AntonCoolpecker.Concrete.Utils;
using Hydra.HydraCommon.Abstract;
using System;
using System.Collections.Generic; //Required for lists.
using UnityEngine;

namespace AntonCoolpecker.Concrete.Scene
{
    /// <summary>
    /// Class for the DOODAD doors.
    /// </summary>
    public class DoodadDoor : HydraMonoBehaviour
    {
		#region Variables

        [SerializeField] private GameObject m_Door; //Reference to DOODAD door - Used for giving the display DOODADs a parent transform
        [SerializeField] private GameObject m_Light; //Reference to DOODAD door light
        [SerializeField] private GameObject m_Switch; //Reference to switch for DOODAD door
        [SerializeField] private Level m_Level; //Reference to level where DOODAD door is placed
		[SerializeField] private FakeDisplayDoodad m_DoodadModel; //The fake DOODAD to be displayed on the door
		[SerializeField] private AnimationClip m_OpeningAnimation; //Ref to animation where the DOODAD door is opened/raised

		[SerializeField] private int m_RequiredDoodads = 2; //How many DOODADs required to open the door
        [SerializeField] private float m_InputDelay = 1f; //Length of delay before input can be used again after interaction
        [SerializeField] private float m_OpeningAnimationLength = 1f; //How long will the door take to open
        [SerializeField] private float m_OpeningDelay = 0f; //Delay before opening the animation  

        private Animator m_Animator; //Reference to animator
		private Timer m_Timer; //Timer for lever interaction cooldown
		private AbstractPlayerController m_Player; //Reference to player instance

        private int m_SpentDoodads; //Amount of DOODADs that has been spent on this door
		private List<int> m_UsedDoodads; //List to hold the IDs of the spend DOODADs
		private int[] m_DisplayDoodads; //Holds each digit of the number of spent DOODADs
		private int[] m_DisplayMaxDoodads; //Holds each digit of the number of required DOODADs
        private float m_GUIPositionY; //Y position for GUI elements
        private bool m_Open; //Is the door already open?
        private bool m_Interactable; //Can you interact with the door?
		private bool m_InRange; //Is the player in range of the door?

        //DoodadPlacement
		private List<FakeDisplayDoodad> m_PositionalDoodads; //List of the DOODADs to display
		[SerializeField] private float m_DoodadPlacementSpeed; //How fast the DOODAD(s) will go to their supposed position
		[SerializeField] private Transform[] m_DoodadPoints; //Where the positioned DOODADs will go(?)
        [SerializeField] private float m_DoodadReturnTime; //Time in seconds for the doodads to return from the door to the player

        //GUIStuff
        List<Rect> numberRects = new List<Rect>();
		[SerializeField] private float m_GUITransitionTime = 2f;
		[SerializeField] private HUDDigit m_Digits; //Reference to the digits prefab in DOODAD door prefab
		[SerializeField] private Texture m_DoorImage; //Image of the door

		#endregion

		#region Messages

        /// <summary>
        /// 	Called once every frame.
        /// </summary>
        protected override void Update()
        {
            base.Update();

            //Ensure reference to player
            if (m_Player == null)
            {
                m_Player = GameObject.FindGameObjectWithTag("Player").GetComponent<AbstractPlayerController>();
            }

            //Use timer to know when the interaction lockout is over and when to change
            //the colours of the switch and light
			if (m_Timer.complete)
            {
                if (!m_Interactable)
                {
                    m_Interactable = true; 
                }

                m_Switch.GetComponent<Renderer>().material.color = new Color(1, 1, 1, 1);

                if (!m_Open)
                {
                    if (GetLevel(m_Level, m_Player).unspentDoodads >= (m_RequiredDoodads - m_SpentDoodads))
                    {
						m_Light.GetComponent<Renderer>().material.color = new Color(1f, 0.64f, 0, 0.8f);
                    }
                    else
						m_Light.GetComponent<Renderer>().material.color = new Color(1f, 0f, 0, 0.8f);
                }
            }

            //If the door has not opened and the player is within the range of the switch, then check for input
			if (!m_Open && m_InRange) 
			{
				if (InputMapping.interactionInput.GetButtonDown () && m_Interactable) 
				{
					if (GetLevel (m_Level, m_Player).unspentDoodads >= (m_RequiredDoodads - m_SpentDoodads))
						Open(m_Player);
					else if (GetLevel (m_Level, m_Player).unspentDoodads == 0)
						Refund();
					else
						AddDoodad(m_Player);
				}
			}

            UpdateRectangles();
        }

        /// <summary>
        ///     Called at start.
        /// </summary>
        protected override void Start()
        {
            base.Start();
            m_Animator = GetComponent<Animator>();
            m_Animator.speed= m_OpeningAnimation.length/m_OpeningAnimationLength;

            //Setup checks
            if (m_RequiredDoodads > m_DoodadPoints.Length)
            {
                Debug.LogWarning(gameObject.name+" requires more doodads than it has  display points for.");
            }

            //Doodads for display 
            m_PositionalDoodads = new List<FakeDisplayDoodad>();
            for (int i = 0; i < m_DoodadPoints.Length; i++)
            {
                m_PositionalDoodads.Add(Instantiate(m_DoodadModel,m_Door.transform)as FakeDisplayDoodad);
                m_PositionalDoodads[i].gameObject.SetActive(false);
            }

            //Interaction variables 
			m_Timer = new Timer();
            m_SpentDoodads = 0;
            m_Open = false;
            m_Interactable = true;
			m_Light.GetComponent<Renderer>().material.color = new Color(1f, 0f, 0, 0.8f);
            m_UsedDoodads = new List<int>();

            //GUI 
            m_GUIPositionY = Screen.height;

            int numberOfDigits = (int)Math.Floor(Math.Log10(m_RequiredDoodads) + 1);
            if (numberOfDigits < 1)
                numberOfDigits = 1;
            m_DisplayMaxDoodads = new int[numberOfDigits];

            int tempN = m_RequiredDoodads;
            for (int i = numberOfDigits - 1; i >= 0; i--)
            {
                m_DisplayMaxDoodads[i] = tempN % 10;
                tempN /= 10;
            }
            m_DisplayDoodads = new int[1];
            m_DisplayDoodads[0] = 0;
        }

        /// <summary>
        ///     Check if player has entered trigger collider
        /// </summary>
        /// <param name="col"></param>
        protected override void OnTriggerEnter(Collider col)
        {
            base.OnTriggerEnter(col);

            AbstractPlayerController player = col.GetComponent<AbstractPlayerController>();

            if (player == null)
                return;
            if (!m_Open)
            {
                m_InRange = true;
            }
        }

        /// <summary>
        ///     Check if player has left trigger collider 
        /// </summary>
        /// <param name="col"></param>
        protected override void OnTriggerExit(Collider col)
        {
            base.OnTriggerExit(col);
            AbstractPlayerController player = col.GetComponent<AbstractPlayerController>();

            if (player = null)
                return;
			
            m_InRange = false;
        }

		#endregion

        #region Private Methods/Functions

        /// <summary>
        /// Return a specified levelProgress from playerController 
        /// </summary>
        /// <param name="Level"> level to return</param>
        /// <param name="player">ref to playercontroler </param>
        /// <returns></returns>
        private Progress.LevelProgress GetLevel(Level Level, AbstractPlayerController player)
        {
            switch (Level)
            {
                case Level.Tutorial:
                    return player.progress.Tutorial;
                case Level.PoynettePort:
                    return player.progress.PoynettePort;
                case Level.TranquilTreeline:
                    return player.progress.TranquilTreeline;
                case Level.AncientAntics:
                    return player.progress.AncientAntics;
                case Level.ColossusCoast:
                    return player.progress.ColossusCoast;
                case Level.VileValley:
                    return player.progress.VileValley;
                case Level.GlimmerGlacier:
                    return player.progress.GlimmerGlacier;
                case Level.HorrendousHill:
                    return player.progress.HorrendousHill;
            }

            //If none of the cases are triggered - just return the tutorial and send error
            Debug.LogError("Unable to retrieve level progress - Returning level progress in Tutorial");

            return player.progress.Tutorial;
        }

        /// <summary>
        /// Spend one doodad, change the light, start the timer,update gui number
        /// </summary>
        /// <param name="player"> ref to player</param>
        private void AddDoodad(AbstractPlayerController player)
        {
            if (GetLevel(m_Level, player).unspentDoodads > 0)
            {
                GetLevel(m_Level, player).SpendDoodads(1, m_UsedDoodads);
                m_Light.GetComponent<Renderer>().material.color = new Color(1, 1, 1, 0.8f);
                m_Switch.GetComponent<Renderer>().material.color = new Color(1, 0.64f, 0, 1);
                m_Interactable = false;
                m_Timer.Reset();
                m_Timer.maxTime = m_InputDelay;
                m_SpentDoodads++;

                int numberOfDigits = (int)Math.Floor(Math.Log10(m_SpentDoodads) + 1);
                if (numberOfDigits < 1)
                    numberOfDigits = 1;
                m_DisplayDoodads = new int[numberOfDigits];
		
                if (PlayerStatistics.PlayerCollectableStatistics[CollectableType.Doodad] <= 0)
                {
                    Debug.LogWarning("There are no DOODADs added to the Player Statistics - Please add them to the Player Statistics script/prefab.");
                }

                PlayerStatistics.PlayerCollectableStatistics[CollectableType.Doodad]--;
                int tempN = m_SpentDoodads;
                for (int i = numberOfDigits - 1; i >= 0; i--)
                {
                    m_DisplayDoodads[i] = tempN % 10;
                    tempN /= 10;
                }

                if (m_SpentDoodads <= m_DoodadPoints.Length)
                {
                    m_PositionalDoodads[m_SpentDoodads - 1].gameObject.SetActive(true);
                    m_PositionalDoodads[m_SpentDoodads - 1].transform.position = GameObject.FindGameObjectWithTag("Player").transform.position;
                    m_PositionalDoodads[m_SpentDoodads - 1].Set(m_DoodadPoints[m_SpentDoodads - 1].localPosition, m_DoodadPlacementSpeed);
                }

            }
            else
            {
                m_Switch.GetComponent<Renderer>().material.color = new Color(1, 0.64f, 0, 1);
                m_Interactable = false;
				m_Timer.Reset();
				m_Timer.maxTime = m_InputDelay;
            }
        }

        /// <summary>
        /// Refund all doodads spent on this door to the player as well as making the positional doodads return to the player 
        /// </summary>
        private void Refund()
        {
            //Go through all positional doodads and refund all curently active ones;
            for (int i = 0; i < m_PositionalDoodads.Count; i++)
            {
                if(m_PositionalDoodads[i].isActiveAndEnabled)
                    m_PositionalDoodads[i].Return(m_DoodadReturnTime);
            }

            for (int i = 0; i < m_UsedDoodads.Count; i++)
            {
                    GetLevel(m_Level, m_Player).refundDoodads(m_UsedDoodads[i]);
                    m_SpentDoodads--;
                PlayerStatistics.PlayerCollectableStatistics[CollectableType.Doodad]++;
            }

            m_UsedDoodads.Clear();
            m_DisplayDoodads = new int[1];
            m_DisplayDoodads[0] = 0;
        }

        /// <summary>
        /// Spend the remaining number of DOODADs to open the door, play opening animation and set the bools
        /// </summary>
        /// <param name="player"></param>
        private void Open(AbstractPlayerController player)
        {
            for (int i = m_SpentDoodads; i < m_RequiredDoodads; i++)
            {
                AddDoodad(player);
            }

            m_Open = true;
			m_Light.GetComponent<Renderer>().material.color = new Color(0, 1, 0, 0.8f);
            m_Switch.GetComponent<Renderer>().material.color = new Color(1, 0.64f, 0, 1);
            Invoke("playAnimation", m_OpeningDelay);
			m_Timer.Reset();
			m_Timer.maxTime = m_InputDelay;
        }

        /// <summary>
        /// Play the door opening animation - Invoked in the Open method.
        /// </summary>
        private void playAnimation()
        {
            m_Animator.SetBool("Open", true);
        }

        #endregion

        #region GUI Functions

        /// <summary>
        /// Update rectangles for GUI elements
        /// </summary>
        private void UpdateRectangles()
        {
            if (m_InRange)
            {
                if (m_GUIPositionY > Screen.height-(Screen.height/10))
                    m_GUIPositionY -= Time.deltaTime * Screen.height / m_GUITransitionTime;
            }
            else
            {
                if (m_GUIPositionY < Screen.height)
                    m_GUIPositionY += Time.deltaTime * Screen.height / m_GUITransitionTime;
            }

            numberRects.Clear();

            for (int i = 0; i < m_DisplayDoodads.Length +m_DisplayMaxDoodads.Length+ 2; i++)
            {
                numberRects.Add(new Rect((
					Screen.width * 0.5f + (i- (m_DisplayDoodads.Length + m_DisplayMaxDoodads.Length + 2) * 0.5f) * Screen.height / 10),
					m_GUIPositionY,
					Screen.height / 10,
					Screen.height / 10));
            }
        }

        /// <summary>
        /// Allows for drawing GUI-related stuff onto the screen
        /// </summary>
        public new void OnGUI()
        {
            for (int i = 0; i < numberRects.Count; i++)
            {
                if (i <= m_DisplayDoodads.Length)
                {
                    if (i == m_DisplayDoodads.Length)
                    {
                        GUI.color = Color.white;
                        Rect toGet = m_Digits.GetDigit('/', HUDDigit.TextColor.Orange);
                        GUI.DrawTextureWithTexCoords(numberRects[i], m_Digits.m_DigitSpriteTexture, toGet);
                    }
                    else
                    {
                        GUI.color = Color.white;
                        Rect toGet = m_Digits.GetDigit(m_DisplayDoodads[i], HUDDigit.TextColor.Orange);
                        GUI.DrawTextureWithTexCoords(numberRects[i], m_Digits.m_DigitSpriteTexture, toGet);
                    }
                }
                else
                {
                    if (i == (m_DisplayDoodads.Length + m_DisplayMaxDoodads.Length+ 1))
                    {
                        PlayerStatistics.elements[(int)CollectableType.Doodad].itemIcon.Draw(numberRects[i]);
                    }
                    else
                    {
                        GUI.color = Color.white;
                        Rect toGet = m_Digits.GetDigit(m_DisplayMaxDoodads[i - m_DisplayDoodads.Length - 1], HUDDigit.TextColor.Orange);
                        GUI.DrawTextureWithTexCoords(numberRects[i], m_Digits.m_DigitSpriteTexture, toGet);
                    }
                }
            }
        }

        #endregion
    }
}