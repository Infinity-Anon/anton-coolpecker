﻿using AntonCoolpecker.Abstract.Player;
using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using AntonCoolpecker.Utils.Time;
using Hydra.HydraCommon.Abstract;
using Hydra.HydraCommon.Extensions;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace AntonCoolpecker.Concrete.Scene
{
	/// <summary>
	/// Script for the lizard sign object.
	/// </summary>
	public class LizardSign : MonoBehaviour
	{
		#region Variables

		[SerializeField] private string m_TextContent;
        [SerializeField] private Material m_BaseMat;
        [SerializeField] private Material m_UnreadMat;
        [SerializeField] private Material m_ReadMat;
        [SerializeField] private Renderer m_LizardRenderer;
		[SerializeField] private float m_MovTime = 1.0f;
		[SerializeField] private Vector2 m_ShowHeight = new Vector2(0f, 100f);
		[SerializeField] private Vector2 m_HideHeight = new Vector2(0f, -100f);
		[SerializeField] private GameObject m_MainPrefabCanvas;

        private bool m_PlayerInArea = false;
        private bool m_BeenRead = false;

		private RectTransform m_PanelTransform;
		private Text m_TextSign;

		#endregion

		#region Properties

		public string textContent { get { return m_TextContent; } }

		#endregion

		#region Messages

		private void Awake()
		{
			m_PanelTransform = m_MainPrefabCanvas.transform.Find("SignTextPanel").GetComponent<RectTransform>();
			m_TextSign = m_PanelTransform.transform.Find("SignTextText").GetComponent<Text>();
		}

        private void Update()
        {
			if (m_PlayerInArea && InputMapping.interactionInput.GetButtonDown())
            {
                this.StopCoroutine(HidePanel);
                this.StartCoroutine(ShowPanel);
                m_BeenRead = true;
                m_LizardRenderer.material = m_ReadMat;
            }
        }

        private void OnTriggerEnter(Collider col)
		{
			AbstractPlayerController player = col.GetComponent<AbstractPlayerController>();

			if (player == null)
				return;

            m_TextSign.text = m_TextContent;
            m_PlayerInArea = true;

            if (m_BeenRead)
                m_LizardRenderer.material = m_ReadMat;
            else
                m_LizardRenderer.material = m_UnreadMat;
        }

        private void OnTriggerExit(Collider col)
		{
			AbstractPlayerController player = col.GetComponent<AbstractPlayerController>();

			if (player == null)
				return;

            this.StopCoroutine(ShowPanel);
			this.StartCoroutine(HidePanel);

            m_PlayerInArea = false;
            m_LizardRenderer.material = m_BaseMat;
        }

		#endregion

		#region Private Methods/Functions

		/// <summary>
		/// Shows the sign text panel.
		/// </summary>
		/// <returns>The text panel.</returns>
		private IEnumerator ShowPanel()
		{
			float startTime = GameTime.time;

			while (GameTime.time < startTime + m_MovTime)
			{
				AnimatePanel(GameTime.time - startTime, m_ShowHeight);
				yield return new WaitForEndOfFrame();
			}

			yield return 0;
		}

		/// <summary>
		/// Hides the sign text panel.
		/// </summary>
		/// <returns>The text panel.</returns>
		private IEnumerator HidePanel()
		{
			float startTime = GameTime.time;

			while (GameTime.time < startTime + m_MovTime)
			{
				AnimatePanel(GameTime.time - startTime, m_HideHeight);
				yield return new WaitForEndOfFrame();
			}

			yield return 0;
		}

		/// <summary>
		/// Animates the sign text panel.
		/// </summary>
		/// <param name="elapsedTime">Elapsed time.</param>
		/// <param name="position">Position.</param>
		private void AnimatePanel(float elapsedTime, Vector2 position)
		{
			m_PanelTransform.anchoredPosition = Vector2.Lerp(m_PanelTransform.anchoredPosition, position,
															 (elapsedTime / m_MovTime));
		}

		#endregion
	}
}