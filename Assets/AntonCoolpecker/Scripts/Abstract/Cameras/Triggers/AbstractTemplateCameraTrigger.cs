﻿using AntonCoolpecker.Abstract.Player;
using AntonCoolpecker.Concrete.Cameras;
using Hydra.HydraCommon.Extensions;
using Hydra.HydraCommon.Utils;
using UnityEngine;

namespace AntonCoolpecker.Abstract.Cameras.Triggers
{
	/// <summary>
	/// 	AbstractTemplateCameraTrigger has a mode and a template for updating
	/// 	the current camera state.
	/// </summary>
	[ExecuteInEditMode]
	public abstract class AbstractTemplateCameraTrigger : AbstractCameraTrigger
	{
		#region Variables

		[SerializeField] private CameraController.Mode m_Mode;
		[SerializeField] private Camera m_Template;
		[SerializeField] private AbstractCameraTrigger[] m_DisableTriggers;

		#endregion

		#region Properties

		/// <summary>
		/// 	Gets the mode.
		/// </summary>
		/// <value>The mode.</value>
		public CameraController.Mode mode { get { return m_Mode; } }

		/// <summary>
		/// 	Gets the template.
		/// </summary>
		/// <value>The template.</value>
		public Camera template { get { return m_Template; } }

		#endregion

		#region Messages

		/// <summary>
		/// 	Called once every frame.
		/// </summary>
		protected override void Update()
		{
			base.Update();

			if (m_Template != null)
				UpdateTemplate();
		}

		/// <summary>
		/// 	Called when the component is enabled.
		/// </summary>
		protected override void OnEnable()
		{
			base.OnEnable();

			if (m_Template == null)
				CreateTemplate();

			if (m_DisableTriggers == null)
				m_DisableTriggers = new AbstractCameraTrigger[0];
		}

		/// <summary>
		/// 	Called when the object is destroyed.
		/// </summary>
		protected override void OnDestroy()
		{
			base.OnDestroy();

			DestroyTemplate();
		}

		#endregion

		#region Methods

		/// <summary>
		/// 	Called when the player enters the trigger.
		/// </summary>
		/// <param name="player">Player.</param>
		protected override void OnEnter(AbstractPlayerController player)
		{
			base.OnEnter(player);

			player.playerCamera.AddTrigger(this);

			SetTriggersEnabled(false);
		}

		/// <summary>
		/// 	Called when the player exits the trigger.
		/// </summary>
		/// <param name="player">Player.</param>
		protected override void OnExit(AbstractPlayerController player)
		{
			base.OnExit(player);

			player.playerCamera.RemoveTrigger(this);

			SetTriggersEnabled(true);
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// 	Sets the triggers enabled.
		/// </summary>
		/// <param name="state">If set to <c>true</c> state.</param>
		private void SetTriggersEnabled(bool state)
		{
			for (int index = 0; index < m_DisableTriggers.Length; index++)
				m_DisableTriggers[index].gameObject.SetActive(state);
		}

		/// <summary>
		/// 	Updates the template.
		/// </summary>
		private void UpdateTemplate()
		{
			bool enabled = CameraController.UseTemplate(m_Mode);
			m_Template.gameObject.SetActive(enabled);
			m_Template.enabled = false;
		}

		/// <summary>
		/// 	Creates the template.
		/// </summary>
		private void CreateTemplate()
		{
			DestroyTemplate();

			GameObject templateGo = new GameObject("Template");
			m_Template = templateGo.AddComponent<Camera>();

			m_Template.transform.Copy(transform);
			m_Template.transform.parent = transform;
		}

		/// <summary>
		/// 	Destroys the template.
		/// </summary>
		private void DestroyTemplate()
		{
			m_Template = ObjectUtils.SafeDestroyGameObject(m_Template);
		}

		#endregion
	}
}
