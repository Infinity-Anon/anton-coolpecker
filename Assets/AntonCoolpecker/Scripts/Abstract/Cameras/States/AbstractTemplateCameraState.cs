﻿using AntonCoolpecker.Concrete.Cameras;
using AntonCoolpecker.Utils.Time;
using UnityEngine;

namespace AntonCoolpecker.Abstract.Cameras.States
{
	/// <summary>
	/// 	AbstractTemplateCameraState describes a camera state that orients to match
	/// 	a given template camera.
	/// </summary>
	public abstract class AbstractTemplateCameraState : AbstractCameraState
	{
		#region Variables

		private Vector3 finalPosition; //Alternate final position in case template isn't used
		private Quaternion finalRotation; //Alternate final position in case template isn't used

		#endregion

		#region Protected Methods

		/// <summary>
		/// 	Orient the camera.
		/// </summary>
		/// <param name="parent">Parent.</param>
		protected override void Orient(MonoBehaviour parent)
		{
			OrientPosition(parent);
			OrientRotation(parent);
		}

		/// <summary>
		/// 	Orients the position.
		/// </summary>
		/// <param name="parent">Parent.</param>
		protected virtual void OrientPosition(MonoBehaviour parent)
		{
			CameraController controller = GetCameraController(parent);
			Vector3 position = controller.transform.position;

			if (controller.template != null) 
				finalPosition = controller.template.transform.position;
			else
				finalPosition = controller.transform.position;

			controller.transform.position = Vector3.Lerp (position, finalPosition, GameTime.fixedDeltaTime);
		}

		/// <summary>
		/// 	Orients the rotation.
		/// </summary>
		/// <param name="parent">Parent.</param>
		protected virtual void OrientRotation(MonoBehaviour parent)
		{
			CameraController controller = GetCameraController(parent);

			Quaternion rotation = controller.transform.rotation;

			if (controller.template != null) 
				finalRotation = controller.template.transform.rotation;
			else
				finalRotation = controller.transform.rotation;

			controller.transform.rotation = Quaternion.Slerp(rotation, finalRotation, GameTime.fixedDeltaTime);
		}

		#endregion
	}
}
