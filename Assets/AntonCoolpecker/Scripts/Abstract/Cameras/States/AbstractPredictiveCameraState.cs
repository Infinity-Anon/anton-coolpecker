﻿using AntonCoolpecker.Concrete.Cameras;
using AntonCoolpecker.Utils;
using UnityEngine;

namespace AntonCoolpecker.Abstract.Cameras.States
{
	/// <summary>
	/// 	Abstract predictive camera state.
	/// </summary>
	public abstract class AbstractPredictiveCameraState : AbstractCameraState
	{
		#region Variables

		[Tweakable("Camera")] [SerializeField] private float m_PredictionSpeed;

		#endregion

		#region Properties

		/// <summary>
		/// 	Gets the prediction speed.
		/// </summary>
		/// <value>The prediction speed.</value>
		public float predictionSpeed { get { return m_PredictionSpeed; } }

		#endregion

		#region Protected Methods/Functions

		/// <summary>
		/// Orient the camera.
		/// </summary>
		/// <param name="parent">Parent.</param>
		protected override void Orient(MonoBehaviour parent)
		{
			UpdatePredictionOffset(parent);
		}

		/// <summary>
		/// 	Gets the offset position for the target.
		/// </summary>
		/// <returns>The offset position.</returns>
		/// <param name="parent">Parent.</param>
		protected override Vector3 GetOffsetPosition(MonoBehaviour parent)
		{
			CameraController controller = GetCameraController(parent);

			return base.GetOffsetPosition(parent) + controller.predictionOffset;
		}

		/// <summary>
		/// 	Updates the prediction offset.
		/// </summary>
		/// <param name="parent">Parent.</param>
		protected abstract void UpdatePredictionOffset(MonoBehaviour parent);

		#endregion
	}
}