﻿using AntonCoolpecker.Abstract.StateMachine;
using AntonCoolpecker.Concrete.Cameras;
using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using AntonCoolpecker.Utils.Time;
using Hydra.HydraCommon.Utils;
using UnityEngine;

namespace AntonCoolpecker.Abstract.Cameras.States
{
	/// <summary>
	/// 	AbstractCameraState is the base class for all CameraController states.
	/// </summary>
	public abstract class AbstractCameraState : FiniteState<AbstractCameraState>
	{
		#region Public Override Functions

		/// <summary>
		/// 	Called when the parent late updates.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnLateUpdate(MonoBehaviour parent)
		{
			base.OnLateUpdate(parent);

			if (GameTime.paused)
				return;

			CameraController controller = GetCameraController(parent);

			if (controller.orbitEnabled)
				UpdateOrbit(parent);

			Orient(parent);

			UpdateOrbitFromOrientation(parent);
		}

		/// <summary>
		/// 	Called when the state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnEnter(MonoBehaviour parent)
		{
			base.OnEnter(parent);

			UpdateOrbitFromOrientation(parent);
		}

		#endregion

		#region Protected Methods/Functions

		/// <summary>
		/// 	Orient the camera.
		/// </summary>
		protected abstract void Orient(MonoBehaviour parent);

		/// <summary>
		/// 	Gets the camera controller.
		/// </summary>
		/// <returns>The camera controller.</returns>
		/// <param name="parent">Parent.</param>
		protected CameraController GetCameraController(MonoBehaviour parent)
		{
			return parent as CameraController;
		}

		/// <summary>
		/// 	Gets the position from rotation.
		/// </summary>
		/// <returns>The position from rotation.</returns>
		protected Vector3 GetPositionFromRotation(MonoBehaviour parent)
		{
			return GetPositionFromRotation(parent, parent.transform.rotation);
		}

		/// <summary>
		/// 	Gets the position from rotation.
		/// </summary>
		/// <returns>The position from rotation.</returns>
		/// <param name="parent">Parent.</param>
		/// <param name="rotation">Rotation.</param>
		protected Vector3 GetPositionFromRotation(MonoBehaviour parent, Quaternion rotation)
		{
			CameraController cameraController = GetCameraController(parent);
			float distance = cameraController.distance;

			Vector3 offsetPosition = GetOffsetPosition(parent);
			Vector3 back = rotation * Vector3.back;

			return back * distance + offsetPosition;
		}

		/// <summary>
		/// 	Gets the offset position for the target, in world space.
		/// </summary>
		/// <returns>The offset position.</returns>
		protected virtual Vector3 GetOffsetPosition(MonoBehaviour parent)
		{
			CameraController cameraController = GetCameraController(parent);
			Transform target = cameraController.target.transform;

			Vector3 pivotOffset = cameraController.pivotOffset;
			Vector3 output = target.position + target.TransformDirection(pivotOffset);

			return output;
		}

		#endregion

		#region Private Methods/Functions

		/// <summary>
		/// 	Updates the orbit.
		/// </summary>
		private void UpdateOrbit(MonoBehaviour parent)
		{
			CameraController controller = GetCameraController(parent);

			Vector2 orbitSpeed = controller.speed;

			if (controller.cameraSmoothing) 
			{
				float orbitX = controller.orbitX;
				float orbitY = controller.orbitY;

				orbitX += InputMapping.horizontalCameraInput.GetAxis () * (controller.invertedX ? -1 : 1) * orbitSpeed.x * controller.distance *
				GameTime.fixedDeltaTime;
				orbitY -= InputMapping.verticalCameraInput.GetAxis () * (controller.invertedY ? -1 : 1) * orbitSpeed.y * GameTime.fixedDeltaTime;

				controller.orbitX = Mathf.Lerp (controller.orbitX, orbitX, controller.smoothXLerp * Time.deltaTime);
				controller.orbitY = Mathf.Lerp (controller.orbitY, orbitY, controller.smoothYLerp * Time.deltaTime);
			} 

			else 
			{
				controller.orbitX += InputMapping.horizontalCameraInput.GetAxis () * (controller.invertedX ? -1 : 1) * orbitSpeed.x * controller.distance *
					GameTime.fixedDeltaTime;
				controller.orbitY -= InputMapping.verticalCameraInput.GetAxis () * (controller.invertedY ? -1 : 1) * orbitSpeed.y * GameTime.fixedDeltaTime;
			}

			if (controller.clampEnabled)
				ClampOrbit(parent);
		}

		/// <summary>
		/// 	Sets the x and the y for the orbit from the current camera
		/// 	orientation.
		/// </summary>
		private void UpdateOrbitFromOrientation(MonoBehaviour parent)
		{
			CameraController controller = GetCameraController(parent);

			Vector3 euler = controller.transform.rotation.eulerAngles;

			controller.orbitX = euler.y;
			controller.orbitY = euler.x;
		}

		/// <summary>
		/// 	Clamps the orbit.
		/// </summary>
		/// <param name="parent">Parent.</param>
		private void ClampOrbit(MonoBehaviour parent)
		{
			CameraController controller = GetCameraController(parent);

			controller.orbitY = ClampAngle(controller.orbitY, controller.minYLimit, controller.maxYLimit);
		}

		/// <summary>
		/// 	Clamps the angle.
		/// </summary>
		/// <returns>The angle.</returns>
		/// <param name="angle">Angle.</param>
		/// <param name="min">Minimum.</param>
		/// <param name="max">Max.</param>
		private float ClampAngle(float angle, float min, float max)
		{
			angle = Mathf.Repeat(angle, 360.0f);
			min = Mathf.Repeat(min, 360.0f);
			max = Mathf.Repeat(max, 360.0f);

			if (min < max)
				return HydraMathUtils.Clamp(angle, min, max);

			if (angle < max || angle > min)
				return angle;

			if (angle - max < min - angle)
				return max;

			return min;
		}

		#endregion
	}
}