﻿using AntonCoolpecker.Abstract.StateMachine;

namespace AntonCoolpecker.Abstract.Props.States
{
	/// <summary>
	/// Abstract property state - Base state for all prop states.
	/// </summary>
	public abstract class AbstractPropState : FiniteState<AbstractPropState> {}
}