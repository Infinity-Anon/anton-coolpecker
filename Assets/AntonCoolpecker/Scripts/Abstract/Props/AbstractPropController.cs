﻿using AntonCoolpecker.API.Damageable;
using AntonCoolpecker.Concrete.Props;
using AntonCoolpecker.Concrete.Utils;
using Hydra.HydraCommon.Abstract;
using UnityEngine;

namespace AntonCoolpecker.Abstract.Props
{
	/// <summary>
	/// Abstract prop controller - Base controller for all props.
	/// </summary>
	[DisallowMultipleComponent]
	public abstract class AbstractPropController : HydraMonoBehaviour, IDamageable
	{
		#region Variables

		[SerializeField] private PropStateMachine m_StateMachine;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the prop state machine.
		/// </summary>
		/// <value>The state machine.</value>
		public PropStateMachine stateMachine { get { return m_StateMachine; } }

		#endregion

		#region Override Functions

		/// <summary>
		/// Called when the component is enabled.
		/// </summary>
		protected override void OnEnable()
		{
			base.OnEnable();
		}

		/// <summary>
		/// Called once every frame.
		/// </summary>
		protected override void Update()
		{
			base.Update();

			stateMachine.Update(this);
		}

		/// <summary>
		/// Called every physics timestep.
		/// </summary>
		protected override void FixedUpdate()
		{
			base.FixedUpdate();

			stateMachine.FixedUpdate(this);
		}

		/// <summary>
		/// Called after all the other updates have finished.
		/// </summary>
		protected override void LateUpdate()
		{
			base.LateUpdate();

			stateMachine.LateUpdate(this);
		}

		#endregion

		#region Virtual Functions

		/// <summary>
		/// Called when the prop gets damaged.
		/// </summary>
		/// <param name="damage">Amount of damage to apply.</param>
		/// <param name="collision">Collision info.</param>
		public virtual void Damage(int damage, CharacterLocomotorCollisionData collision) {}

		#endregion
	}
}