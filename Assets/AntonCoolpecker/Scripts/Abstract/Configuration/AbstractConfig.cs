﻿using Hydra.HydraCommon.Abstract;

namespace AntonCoolpecker.Abstract.Configuration
{
	/// <summary>
	/// 	AbstractConfig is the base class for all AntonCoolpecker configurations.
	/// </summary>
	public abstract class AbstractConfig<T> : SingletonHydraScriptableObject<T>
		where T : AbstractConfig<T>
	{
		private const string MODULE_NAME = "AntonCoolpecker";
		private const string SUBDIR_NAME = "Configuration";

		/// <summary>
		/// 	Gets the name of the module.
		/// </summary>
		/// <value>The name of the module.</value>
		private static string moduleName { get { return MODULE_NAME; } }

		/// <summary>
		/// 	Gets the name of the sub directory.
		/// </summary>
		/// <value>The name of the sub directory.</value>
		private static string subDirectoryName { get { return SUBDIR_NAME; } }
	}
}