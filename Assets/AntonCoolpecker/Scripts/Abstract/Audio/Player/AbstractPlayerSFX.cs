using Hydra.HydraCommon.PropertyAttributes;
using System;
using UnityEngine;

namespace AntonCoolpecker.Abstract.Audio.Player
{
	/// <summary>
	/// 	AbstractPlayerSFX stores sound effects for actions that are common to
	/// 	the playable characters Anton and Coolpecker.
	/// </summary>
	public abstract class AbstractPlayerSFX
	{
		#region Variables

		[SerializeField] private SoundEffectAttribute[] m_Attack;
		[SerializeField] private SoundEffectAttribute[] m_Crash;
		[SerializeField] private SoundEffectAttribute[] m_Fall;
		[SerializeField] private SoundEffectAttribute[] m_Hurt;
		[SerializeField] private SoundEffectAttribute[] m_Idle;
		[SerializeField] private SoundEffectAttribute[] m_Jump;
		[SerializeField] private SoundEffectAttribute[] m_Push;

		#endregion

		#region Properties

		public SoundEffectAttribute[] attack { get { return m_Attack; } }
		public SoundEffectAttribute[] crash { get { return m_Crash; } }
		public SoundEffectAttribute[] fall { get { return m_Fall; } }
		public SoundEffectAttribute[] idle { get { return m_Idle; } }
		public SoundEffectAttribute[] hurt { get { return m_Hurt; } }
		public SoundEffectAttribute[] jump { get { return m_Jump; } }
		public SoundEffectAttribute[] push { get { return m_Push; } }

		#endregion
	}

	/// <summary>
	/// 	AntonSFX stores specific sound effects for Anton.
	/// </summary>
	[Serializable]
	public class AntonSFX : AbstractPlayerSFX
	{
		#region Variables

		[SerializeField] private SoundEffectAttribute[] m_Tongue;

		#endregion

		#region Properties

		public SoundEffectAttribute[] tongue { get { return m_Tongue; } }

		#endregion
	}

	/// <summary>
	/// 	CoolpeckerSFX stores specific sound effects for Coolpecker.
	/// </summary>
	[Serializable]
	public class CoolpeckerSFX : AbstractPlayerSFX
	{
		#region Variables

		[SerializeField] private SoundEffectAttribute[] m_Fly;

		#endregion

		#region Properties

		public SoundEffectAttribute[] fly { get { return m_Fly; } }

		#endregion
	}
}