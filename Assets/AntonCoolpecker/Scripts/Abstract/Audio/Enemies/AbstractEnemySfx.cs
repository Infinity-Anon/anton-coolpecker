﻿using Hydra.HydraCommon.Abstract;
using Hydra.HydraCommon.PropertyAttributes;
using System.Collections;
using UnityEngine;

namespace AntonCoolpecker.Abstract.Audio.Enemies
{
	/// <summary>
	/// 	Abstract enemy sound effects.
	/// </summary>
	public class AbstractEnemySfx
	{
		#region Variables

		[SerializeField] private SoundEffectAttribute[] m_Attack;
		[SerializeField] private SoundEffectAttribute[] m_Crash;
		[SerializeField] private SoundEffectAttribute[] m_Fall;
		[SerializeField] private SoundEffectAttribute[] m_Hurt;
		[SerializeField] private SoundEffectAttribute[] m_Idle;
		[SerializeField] private SoundEffectAttribute[] m_Jump;
		[SerializeField] private SoundEffectAttribute[] m_Die;
		[SerializeField] private SoundEffectAttribute[] m_Sight;

		#endregion

		#region Properties

		// Attacking sound effects
		public SoundEffectAttribute[] attack { get { return m_Attack; } }
		public SoundEffectAttribute[] crash { get { return m_Crash; } }
		public SoundEffectAttribute[] fall { get { return m_Fall; } }
		public SoundEffectAttribute[] idle { get { return m_Idle; } }
		public SoundEffectAttribute[] hurt { get { return m_Hurt; } }
		public SoundEffectAttribute[] jump { get { return m_Jump; } }

		// Sound effects when getting killed
		public SoundEffectAttribute[] die { get { return m_Die; } }

		// Sound effects when seeing the player (and commencing aggressive behaviour)
		public SoundEffectAttribute[] sight { get { return m_Sight; } }

		#endregion
	}
}