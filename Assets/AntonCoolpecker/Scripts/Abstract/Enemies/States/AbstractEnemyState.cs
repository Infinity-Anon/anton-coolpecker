﻿using AntonCoolpecker.Abstract.StateMachine;
using AntonCoolpecker.Concrete.Enemies;
using AntonCoolpecker.Concrete.Enemies.States;
using AntonCoolpecker.Utils.Time;
using System;
using UnityEngine;

namespace AntonCoolpecker.Abstract.Enemies.States
{
	/// <summary>
	/// Abstract enemy state. 
	/// Root class for enemy behaviour state machine states. Inherit enemy states from here.
	/// </summary>
	public abstract class AbstractEnemyState : TimableFiniteState<AbstractEnemyState>
	{
		#region Variables

		[SerializeField] private bool m_UseRunningSpeed = false; //Should the enemy be running?
		[SerializeField] private string m_AnimatorState;
		[SerializeField] private HitstunEnemyState m_HitstunState;
		[SerializeField] private AbstractDyingState m_DyingState;
		[SerializeField] private bool m_UseFriction = true;

		/// <summary>
		/// Whether the enemy can damage the player while in this state.
		/// If false, the hitbox child object will be deactivated while in this state.
		/// </summary>
		[SerializeField] private bool m_CanDamage = true;

		private static GameObject m_CachedPlayer;

		public enum LocomotionMode
		{
			NMAgent,
			Locomotor
		}

		#endregion

		#region Properties

		/// <summary>
		/// 	The animator state to use for this EnemyState. (=~ The animation to play)
		/// </summary>
		/// <value>The state of the animator.</value>
		public string animatorState { get { return m_AnimatorState; } }

		/// <summary>
		/// Tells the controller whether to use CharacterLocomotor or NavMeshAgent for movement in the given state
		/// </summary>
		/// <value>The locomotion mode.</value>
		public virtual LocomotionMode locomotionMode { get { return LocomotionMode.NMAgent; } }

		/// <summary>
		/// 	Gets the player.
		/// </summary>
		/// <value>The player.</value>
		public GameObject player
		{
			get
			{
				return (m_CachedPlayer && m_CachedPlayer.activeInHierarchy)
						   ? m_CachedPlayer
						   : m_CachedPlayer = GameObject.FindWithTag("Player");
			}
		}

		/// <summary>
		/// 	Returns the hitstun state associated with this state.
		/// </summary>
		/// <returns>The hitstun state.</returns>
		public virtual HitstunEnemyState HitstunState
		{
			get
			{
				if (m_HitstunState)
					return m_HitstunState;

				throw new NullReferenceException("Hitstun state not set.");
			}
		}

		/// <summary>
		/// The state to transition to when the enemy's health drops to zero.
		/// 
		/// In general, enemies are killable at any time. If you need an
		/// enemy to be invincible, prevent them from taking damage, not from
		/// dying.
		/// </summary>
		public virtual AbstractDyingState DyingState
		{
			get
			{
				if (m_DyingState)
					return m_DyingState;

				throw new NullReferenceException("Dying state not set.");
			}
		}

		#endregion

		#region Public Methods/Functions

		/// <summary>
		/// Called when the state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnEnter(MonoBehaviour parent)
		{
			base.OnEnter(parent);

			EnemyController enemy = GetEnemyController(parent);
			enemy.navMeshAgent.speed = GetSpeed(parent);

			switch (locomotionMode)
			{
				case AbstractEnemyState.LocomotionMode.Locomotor:
					enemy.usingNavMeshAgent = false;
					break;

				case AbstractEnemyState.LocomotionMode.NMAgent:
					enemy.usingNavMeshAgent = (!enemy.movesIn3D) && enemy.characterLocomotor.isGrounded;
					break;

				default:
					throw new ArgumentOutOfRangeException();
			}

			enemy.characterLocomotor.useFriction = m_UseFriction;

			if (enemy.hitBox != null) 
			{
				enemy.hitBox.SetActive(m_CanDamage);
			}
		}

		/// <summary>
		/// 	Called when the parent's physics update.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnFixedUpdate(MonoBehaviour parent)
		{
			base.OnFixedUpdate(parent);

			HandleMovement(parent);
		}

		/// <summary>
		/// Choose and execute the movement strategy.  Called every FixedUpdate
		/// </summary>
		/// <param name="parent">Parent.</param>
		public virtual void HandleMovement(MonoBehaviour parent)
		{
			EnemyController enemy = GetEnemyController(parent);

			switch (locomotionMode)
			{
				case LocomotionMode.Locomotor:
					HandleLocomotorMovement(parent);
					break;
				case LocomotionMode.NMAgent:
					if (enemy.usingNavMeshAgent)
					{
						Vector3 target = GetMotionTarget(parent);

						if ((target - enemy.navMeshAgent.destination).magnitude > enemy.mimimumNMADesintationChange)
							enemy.navMeshAgent.SetDestination(target);
					}
					else
						HandleLocomotorMovement(parent);
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}
		
		/// <summary>
		/// Move directly to the assigned target using characterLocomotor (ignoring the NavMesh)
		/// </summary>
		/// <param name="parent"></param>
		public void HandleLocomotorMovement(MonoBehaviour parent)
		{
			EnemyController enemy = GetEnemyController(parent);

			Vector3 movement = GetMotion(parent);

			ResolveMovement(enemy, movement);
			ResolveRotation(enemy, movement);
		}

		/// <summary>
		/// Get the movement speed that the enemy should currently be using.
		/// </summary>
		/// <param name="parent">Parent.</param>
		/// <returns>The speed (per second)</returns>
		public float GetSpeed(MonoBehaviour parent)
		{
			EnemyController enemy = GetEnemyController(parent);

			return m_UseRunningSpeed ? enemy.runningSpeed : enemy.walkingSpeed;
		}

		/// <summary>
		/// Return the parent as an EnemyController.
		/// If parent is not an EnemyController, returns null (but then you have bigger problems)
		/// </summary>
		/// <param name="parent">Parent behaviour</param>
		/// <returns>Parent, casted to EnemyController</returns>
		public static EnemyController GetEnemyController(MonoBehaviour parent)
		{
			return parent as EnemyController;
		}

		#endregion

		#region Private & Protected Methods/Functions

		/// <summary>
		/// 	Gets the movement vector used to reach the current motion target.
		/// </summary>
		/// <returns>The movement vector.</returns>
		/// <param name="parent">Parent.</param>
		private Vector3 GetMotion(MonoBehaviour parent)
		{
			EnemyController enemy = GetEnemyController(parent);

			Vector3 targetpos = GetMotionTarget(parent) - enemy.transform.position;

			if (!enemy.movesIn3D)
				targetpos.y = 0;
			
			if (targetpos.magnitude < GetSpeed(parent) * GameTime.deltaTime)
				return Vector3.zero;

			Vector3 motion = targetpos.normalized;
			motion = motion * GetSpeed(parent) * GameTime.deltaTime;

			return motion;
		}

		/// <summary>
		/// Accelerate to the desired movement.
		/// </summary>
		/// <param name="enemy">The EnemyController that is moving</param>
		/// <param name="movement">The desired velocity vector</param>
		protected virtual void ResolveMovement(EnemyController enemy, Vector3 movement)
		{
			movement /= GameTime.deltaTime;

			Vector3 currentVelocity = enemy.movesIn3D ? enemy.characterLocomotor.velocity : enemy.characterLocomotor.flatVelocity;
			Vector3 deltaVelocity = Vector3.ClampMagnitude(movement - currentVelocity, enemy.acceleration * GameTime.deltaTime);

			enemy.characterLocomotor.velocity += deltaVelocity;
		}

		/// <summary>
		/// Rotate to an appropriate direction, given the desired velocity
		/// </summary>
		/// <param name="enemy">The EnemyController that is rotating.</param>
		/// <param name="movement">The desired velocity vector.</param>
		protected virtual void ResolveRotation(EnemyController enemy, Vector3 movement)
		{
			movement.y = 0.0f;

			RotateTowardsVector(enemy, movement);
		}

		/// <summary>
		/// 	Rotates the enemy towards a certain vector.
		/// </summary>
		/// <param name="enemy">Enemy.</param>
		/// <param name="vector">Vector.</param>
		protected void RotateTowardsVector(EnemyController enemy, Vector3 vector)
		{
			Quaternion lookRotation = Quaternion.LookRotation(enemy.transform.forward);

			if (vector == Vector3.zero)
				return;

			Quaternion targetRotation = Quaternion.LookRotation(vector);
			float delta = enemy.rotateSpeed * GameTime.deltaTime;

			enemy.transform.rotation = Quaternion.RotateTowards(lookRotation, targetRotation, delta);
		}

		/// <summary>
		/// 	Gets the target towards which this enemy is trying to move.
		/// </summary>
		/// <returns>The motion target.</returns>
		/// <param name="parent">Parent.</param>
		protected virtual Vector3 GetMotionTarget(MonoBehaviour parent)
		{
			return parent.transform.position;
		}

		#endregion
	}
}