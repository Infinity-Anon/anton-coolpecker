﻿using AntonCoolpecker.Abstract.Enemies.States;
using AntonCoolpecker.Concrete.Enemies.States;
using AntonCoolpecker.Concrete.Utils;
using AntonCoolpecker.Concrete.Enemies;
using AntonCoolpecker.Utils.Time;
using System.Collections;
using UnityEngine;

namespace AntonCoolpecker.Abstract.Enemies.States
{
	/// <summary>
	/// Abstract dying state - State for anything that should happen between when an enemy's health is reduced to zero,
	/// and when they are removed from the scene. Do things like play dying animations and sounds in a subclass of this state.
	/// </summary>
	public class AbstractDyingState : AbstractEnemyState
	{
		#region Variables

		[SerializeField] DeadState m_DeadState;
		[SerializeField] Timer m_Timer;
		[SerializeField] private float m_Acceleration = 0.5f;

		#endregion

		#region Private Methods

		/// <summary>
		/// By default, transition to the DeadState (i.e remove from scene) when the timer is up.
		/// </summary>
		protected override AbstractEnemyState timerState
		{
			get
			{
				return m_DeadState;
			}
		}

		#endregion

		#region Public Methods

		/// <summary>
		/// The time should have enough time to play any dying animations and sounds.
		/// TODO: If we want variable-length dying animations, we may need to change this to an event-based system
		/// </summary>
		public override Timer GetStateTimer(MonoBehaviour parent)
		{
			EnemyController enemy = GetEnemyController(parent);
			return enemy.stateTimer;
		}

		/// <summary>
		/// For movement, slow down to a stop using normal deceleration.
		/// </summary>
		public override void HandleMovement(MonoBehaviour parent)
		{
			EnemyController enemy = GetEnemyController(parent);
			Vector3 currentV = enemy.characterLocomotor.velocity;
			float currentSpeed = currentV.magnitude;
			float newSpeed = Mathf.Max(0, currentSpeed - (m_Acceleration * GameTime.deltaTime));
			enemy.characterLocomotor.velocity = currentV.normalized * newSpeed;
		}

		#endregion
	}
}