﻿using AntonCoolpecker.Abstract.Menus.States;
using AntonCoolpecker.Concrete.Menus.HUD;
using Hydra.HydraCommon.Abstract;
using System.Collections;
using UnityEngine;

namespace AntonCoolpecker.Concrete.Menus
{
	//TODO: MOVE UPDATE SCROLL FUNCTION AND SET SCROLL AREA FUNCTION TO SCROLL BUTTON SCRIPT
	//TODO: MOVE SET LEVEL FUNCTION TO LEVEL BUTTON SCRIPT

	/// <summary>
	/// Abstract button class - Used for defining concrete GUI button classes.
	/// </summary>
    public class AbstractButton
    {
		#region Variables

        public string title;

		#endregion

		#region Virtual Functions/Methods

		/// <summary>
		/// Sets the specified level(Unity scene) for a button.
		/// </summary>
		/// <param name="level">Level.</param>
        public virtual void Set(int level) {}

		/// <summary>
		/// Set a specified scroll area for a (scrollable) button
		/// </summary>
		/// <param name="scrollArea">Scroll area.</param>
        public virtual void Set(Rect scrollArea) {}

		/// <summary>
		/// Called when the button has been clicked.
		/// </summary>
        public virtual void OnButtonClicked() {}

		/// <summary>
		/// Draw the specified rectangle, boxColor, textColor and digits.
		/// </summary>
		/// <param name="drawRect">Draw rect.</param>
		/// <param name="boxColor">Box color.</param>
		/// <param name="textColor">Text color.</param>
		/// <param name="digits">Digits.</param>
        public virtual void Draw(Rect drawRect, Color boxColor, HUDDigit.TextColor textColor, HUDDigit digits)
        {
            //GUI.color = boxColor;
            //GUI.DrawTexture(drawRect, Texture2D.whiteTexture);

            GUI.color = Color.white;
            string capsTitle = title.ToUpper();

            for(int i = 0; i < capsTitle.Length; i++)
            {
                Rect doublePos;

                if (drawRect.width / capsTitle.Length <= drawRect.height)
                {
                    doublePos = new Rect(drawRect.x + i * drawRect.width / capsTitle.Length,
                        drawRect.y + drawRect.height / 2 - drawRect.width / capsTitle.Length / 2,
                        drawRect.width / capsTitle.Length, drawRect.width / capsTitle.Length);
                }
                else
                {
                    doublePos = new Rect(drawRect.x + i * drawRect.height + drawRect.width/2 - drawRect.height*capsTitle.Length/2,
                        drawRect.y,
                        drawRect.height, drawRect.height);
                }

                Rect toGet = digits.GetDigit(capsTitle[i], textColor);
                GUI.DrawTextureWithTexCoords(doublePos, digits.m_DigitSpriteTexture, toGet);
            }
        }

		/// <summary>
		/// Called when the button as a whole is updated.
		/// </summary>
		/// <returns><c>true</c>, if button was updated, <c>false</c> otherwise.</returns>
		/// <param name="m_CurrentSelectTime">M current select time.</param>
		/// <param name="autoScrolling">If set to <c>true</c> auto scrolling.</param>
		/// <param name="sameDirection">If set to <c>true</c> same direction.</param>
		/// <param name="currentState">Current state.</param>
        public virtual bool UpdateButton(float m_CurrentSelectTime, bool autoScrolling, bool sameDirection, MenuController.ScrollHoldState currentState)
        {
            return false;
        }

		/// <summary>
		/// Called when the scroll of a button updates.
		/// </summary>
        public virtual void UpdateScroll() {}

		#endregion
    }
}