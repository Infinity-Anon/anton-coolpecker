﻿using AntonCoolpecker.Abstract.Menus.States;
using Hydra.HydraCommon.Abstract;
using Hydra.HydraCommon.EventArguments;
using System;

namespace AntonCoolpecker.Abstract.Menus.Panels
{
	/// <summary>
	/// 	Abstract menu panel.
	/// </summary>
	public abstract class AbstractMenuPanel : HydraMonoBehaviour
	{
		#region Variables

		public event EventHandler onPreviousMenuCallback; //Handle events regarding the previous menu
		public event EventHandler<EventArg<AbstractMenuState>> onNextMenuCallback; //Handle events regarding the next menu

		#endregion 

		#region Public Methods

		/// <summary>
		/// 	Called to handle the change of the current menu to the previous menu.
		/// </summary>
		public void PreviousMenu()
		{
			EventHandler handler = onPreviousMenuCallback;
			if (handler != null)
				handler(this, EventArgs.Empty);
		}

		/// <summary>
		/// 	Called to handle the change of the current menu to the next menu.
		/// </summary>
		/// <param name="menuState">Menu state.</param>
		public void NextMenu(AbstractMenuState menuState)
		{
			EventHandler<EventArg<AbstractMenuState>> handler = onNextMenuCallback;
			if (handler != null)
				handler(this, new EventArg<AbstractMenuState>(menuState));
		}

		#endregion
	}
}