﻿using AntonCoolpecker.Abstract.StateMachine;
using AntonCoolpecker.Concrete.Configuration;
using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using AntonCoolpecker.Concrete.Menus;
using AntonCoolpecker.Concrete.Menus.HUD;
using Hydra.HydraCommon.EventArguments;
using Hydra.HydraCommon.Utils;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace AntonCoolpecker.Abstract.Menus.States
{
    /// <summary>
    /// 	Abstract scrollable menu state.
    /// </summary>
    public class AbstractScrollableMenuState : AbstractMenuState
    {
		#region Variables

        [SerializeField] protected float m_menuSpaceXWidthPercent;  //Percentage of Screen.Width which determines the X of the button cluster display area. 
        [SerializeField] protected float m_menuSpaceYHeightPercent;  //Percentage of Screen.Height which determines the Y of the button cluster display area. 
        [SerializeField] protected float m_menuSpaceWidthWidthPercent;  //Percentage of Screen.Width which determines the Width of the button cluster display area. 
        [SerializeField] protected float m_menuSpaceHeightHeightPercent;  //Percentage of Screen.Height which determines the Height of the button cluster display area. 

        [SerializeField] protected float m_menuSButtonHeightMenuSpaceHeightPercent;  //Percentage of display area Height which determines the Height of the buttons in the display area. 
        [SerializeField] protected float m_menuSButtonWidthMenuSpaceWidthPercent;  //Percentage of display area Width which determines the Width of the buttons in the display area. 

        [SerializeField] protected float m_menuSButtonSpacingMenuSpaceHeightPercent;  //Percentage of display area Height which determines the Y-spacing of the buttons in the display area.
        
        [SerializeField] protected float m_scrollBarWidthWidthPercent;  //Percentage of Screen.Width which determines the Width of the scroll bar.

        [SerializeField] protected float m_TotalButtonsInCluster;  //Number of buttons that will be inside the menu space.

        protected float m_TotalScrollDistance; //How far you can scroll in the current menu
        protected float m_ScrollRatio; //The ratio of the scroll distance to the height of the menu rectangle

        protected Rect m_MenuRect;
        protected float m_ScrollOffset;

        protected ScrollButton m_ScrollBar;
        protected Rect m_ScrollBarRect;

		//TODO: REMOVE OR ADJUST OFFSET VARIABLE WHEN FIXING MENU MOUSE SCROLL
		private float m_BaseScrollOffset; //Stores basic scroll offset

		#endregion

		#region Override Functions/Methods

		/// <summary>
		/// Called when the state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
        public override void OnEnter(MonoBehaviour parent)
        {
            base.OnEnter(parent);

            m_MenuRect = new Rect(Screen.width * m_menuSpaceXWidthPercent,
                Screen.height * m_menuSpaceYHeightPercent,
                Screen.width * m_menuSpaceWidthWidthPercent,
                Screen.height * m_menuSpaceHeightHeightPercent);

			// buttonPos[buttonPos.Count - 1].height + buttonPos[buttonPos.Count - 1].y;
            m_TotalScrollDistance = (m_menuSButtonHeightMenuSpaceHeightPercent + m_menuSButtonSpacingMenuSpaceHeightPercent) * m_TotalButtonsInCluster * m_MenuRect.height;
            m_ScrollRatio = m_MenuRect.height / m_TotalScrollDistance;

            float percentDown = (-m_ScrollOffset) / (m_TotalScrollDistance - m_MenuRect.height);
            float scrollInvPerc = (1 - m_ScrollRatio) * percentDown;

            m_ScrollBarRect = new Rect(Screen.width - Screen.width * m_scrollBarWidthWidthPercent,
                Screen.height * scrollInvPerc,
                Screen.width * m_scrollBarWidthWidthPercent, Screen.height * m_ScrollRatio);

			m_ScrollBar = new ScrollButton();
        }

		/// <summary>
		/// Draw out the graphics for this menu state.
		/// </summary>
        public override void Draw()
        {
            GUI.BeginGroup(m_MenuRect);

            for (int i = 0; i < buttonPos.Count; i++)
            {
                Color boxColor;
                HUDDigit.TextColor textColor;

                if (i == m_selectedButton)
                {
                    boxColor = Color.black;
                    textColor = HUDDigit.TextColor.Blue;
                }
                else
                {
                    boxColor = Color.white;
                    textColor = HUDDigit.TextColor.Yellow;
                }

                if (i == m_TotalButtonsInCluster)
                    GUI.EndGroup();

                m_buttonScripts[i].Draw(buttonPos[i], boxColor, textColor, MenuController.instance.m_Digits);
            }

            GUI.color = Color.gray;
            GUI.DrawTexture(new Rect(m_ScrollBarRect.x, 0, m_ScrollBarRect.width, Screen.height), Texture2D.whiteTexture);
            GUI.color = Color.white;
            GUI.DrawTexture(m_ScrollBarRect, Texture2D.whiteTexture);
        }

		/// <summary>
		/// Updates the current menu. Returns true if SelectButtonTime needs to be reset
		/// </summary>
		/// <param name="m_CurrentSelectTime"></param>
		/// <returns></returns>
		/// <param name="autoScrolling">If set to <c>true</c> auto scrolling.</param>
		/// <param name="sameDirection">If set to <c>true</c> same direction.</param>
		/// <param name="currentState">Current state.</param>
        public override bool UpdateMenu(float m_CurrentSelectTime, bool autoScrolling, bool sameDirection, MenuController.ScrollHoldState currentState)
        {
            bool resetTime = false;

            if (m_buttonScripts != null)
            {
                Vector2 mouseTruePos = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);

                if (m_ScrollBarRect.Contains(mouseTruePos) && Input.GetMouseButtonDown(0))
                {
					m_BaseScrollOffset = m_ScrollOffset;
                    m_ScrollBar.OnButtonClicked();
                }
                else
                {
                    for (int i = 0; i < buttonPos.Count; i++)
                    {
                        if (i < buttonPos.Count - 1)
                        {
                            Rect truePos = new Rect(buttonPos[i].x + m_MenuRect.x, buttonPos[i].y + m_MenuRect.y, buttonPos[i].width, buttonPos[i].height);

                            if (truePos.Contains(mouseTruePos) && m_MenuRect.Contains(mouseTruePos))
                            {
                                if (Input.GetAxis("Mouse X") != 0 || Input.GetAxis("Mouse Y") != 0)
                                {
                                    m_selectedButton = i;
                                    m_buttonScripts[m_selectedButton].UpdateScroll();
                                }

                                if (Input.GetMouseButtonDown(0))
                                {
                                    HitButton(i);
                                }

                                break;
                            }
                        }
                        else
                        {
                            if (buttonPos[i].Contains(mouseTruePos))
                            {
                                if ((Input.GetAxis("Mouse X") != 0 || Input.GetAxis("Mouse Y") != 0))
                                {
                                    m_selectedButton = i;
                                }

                                if (Input.GetMouseButtonDown(0))
                                {
                                    HitButton(i);
                                }

                                break;
                            }
                        }
                    }
                }

                if (InputMapping.jumpInput.GetButtonDown())
                {
                    HitButton(m_selectedButton);
                }

                if (autoScrolling)
                {
                    if (m_CurrentSelectTime <= 0)
                    {
                        if (currentState == MenuController.ScrollHoldState.Top)
                        {
                            m_selectedButton--;
                            resetTime = true;
                        }
                        else if (currentState == MenuController.ScrollHoldState.Bottom)
                        {
                            m_selectedButton++;
                            resetTime = true;
                        }
                    }
                }
                else if (!sameDirection)
                {
                    if (currentState == MenuController.ScrollHoldState.Top)
                    {
                        m_selectedButton--;
                    }
                    else if (currentState == MenuController.ScrollHoldState.Bottom)
                    {
                        m_selectedButton++;
                    }
                }

                if (m_selectedButton < 0)
                    m_selectedButton = m_buttonScripts.Count - 1;
                else if (m_selectedButton >= m_buttonScripts.Count)
                    m_selectedButton = 0;

                if (m_selectedButton < buttonPos.Count - 1)
                {
                    if (buttonPos[m_selectedButton].y + buttonPos[m_selectedButton].height > m_MenuRect.height)
                    {
                        m_ScrollOffset += m_MenuRect.height - (buttonPos[m_selectedButton].y + buttonPos[m_selectedButton].height);
                        UpdateRectPositions();
                    }
                    else if (buttonPos[m_selectedButton].y < 0)
                    {
                        m_ScrollOffset -= buttonPos[m_selectedButton].y;
                        UpdateRectPositions();
                    }
                }
					
                if (!resetTime)
                    resetTime = m_buttonScripts[m_selectedButton].UpdateButton(m_CurrentSelectTime, autoScrolling, sameDirection, currentState);
            }

            float mouseDelta = m_ScrollBar.getDelta();

            if (mouseDelta != 0)
            {
                float scrollDelta = mouseDelta / Screen.height * m_TotalScrollDistance;

                m_ScrollOffset = scrollDelta + m_BaseScrollOffset;

                if (m_ScrollOffset > 0)
                    m_ScrollOffset = 0;
                else if (m_ScrollOffset < m_MenuRect.height - m_TotalScrollDistance)
                    m_ScrollOffset = m_MenuRect.height - m_TotalScrollDistance;

                UpdateRectPositions();

                if (buttonPos[m_selectedButton].y < 0)
                {
                    m_selectedButton++;
                }
                else if (buttonPos[m_selectedButton].y + buttonPos[m_selectedButton].height > m_MenuRect.height)
                {
                    m_selectedButton--;
                }
            }

            return resetTime;
        }

		#endregion

		#region Virtual Functions

		/// <summary>
		/// Updates the GUI rectangle positions.
		/// </summary>
        protected virtual void UpdateRectPositions()
        {
            for (int i = 0; i < m_TotalButtonsInCluster; i++)
            {
                buttonPos[i] = new Rect(0, //X
                    (m_MenuRect.height * m_menuSButtonHeightMenuSpaceHeightPercent + m_MenuRect.height * m_menuSButtonSpacingMenuSpaceHeightPercent) * i + m_ScrollOffset, //Y
                    m_MenuRect.width * m_menuSButtonWidthMenuSpaceWidthPercent, //Width
                    m_MenuRect.height * m_menuSButtonHeightMenuSpaceHeightPercent); //Height
            }

            float percentDown = (-m_ScrollOffset) / (m_TotalScrollDistance - m_MenuRect.height);

            float scrollInvPerc = (1 - m_ScrollRatio) * percentDown;

            m_ScrollBarRect = new Rect(Screen.width - Screen.width * m_scrollBarWidthWidthPercent,
               Screen.height * scrollInvPerc,
               Screen.width * m_scrollBarWidthWidthPercent, Screen.height * m_ScrollRatio);
        }

		#endregion
    }
}