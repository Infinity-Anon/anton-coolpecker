﻿using AntonCoolpecker.Abstract.Menus.Panels;
using AntonCoolpecker.Abstract.StateMachine;
using AntonCoolpecker.Concrete.Configuration;
using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using AntonCoolpecker.Concrete.Menus;
using AntonCoolpecker.Concrete.Menus.HUD;
using Hydra.HydraCommon.EventArguments;
using Hydra.HydraCommon.Utils;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace AntonCoolpecker.Abstract.Menus.States
{
	/// <summary>
	/// 	Abstract menu state.
	/// </summary>
	public abstract class AbstractMenuState : FiniteState<AbstractMenuState>
	{
		#region Variables

		[SerializeField] private AbstractMenuPanel m_PanelPrefab;
        [SerializeField] protected List<AbstractButton> m_buttonScripts;
        [SerializeField] protected List<string> m_buttonText;
        [SerializeField] protected int m_GUIDepth;

		private AbstractMenuPanel m_PanelInstance;
		protected List<Rect> buttonPos;
		protected int m_selectedButton;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the GUI depth.
		/// </summary>
		/// <value>The GUI depth.</value>
		public int GUIDepth { get { return m_GUIDepth; } }

		/// <summary>
		/// 	Gets the panel instance.
		/// </summary>
		/// <value>The panel instance.</value>
		public AbstractMenuPanel panelInstance { get { return m_PanelInstance; } }

		#endregion

		#region Messages

		/// <summary>
		/// 	Called when the object is loaded.
		/// </summary>
		protected override void OnEnable()
		{
			base.OnEnable();

			Subscribe();
		}

		/// <summary>
		/// 	Called when the object goes out of scope.
		/// </summary>
		protected override void OnDisable()
		{
			base.OnDisable();

			Unsubscribe();
		}

        /// <summary>
        /// Updates the current menu. Returns true if SelectButtonTime needs to be reset
        /// </summary>
        /// <param name="m_CurrentSelectTime"></param>
        /// <returns></returns>
        public virtual bool UpdateMenu(float m_CurrentSelectTime, bool autoScrolling, bool sameDirection, MenuController.ScrollHoldState currentState)
        {
            bool resetTime = false;

            if (m_buttonScripts != null)
            {
                if (buttonPos == null)
                    return resetTime;

                Vector2 mouseTruePos = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);

                for (int i = 0; i < buttonPos.Count; i++)
                {
                    if (buttonPos[i].Contains(mouseTruePos))
                    {
                        if (Input.GetAxis("Mouse X") != 0 || Input.GetAxis("Mouse Y") != 0)
                        {
                            m_selectedButton = i;
                        }

                        if (Input.GetMouseButtonDown(0))
                        {
                            HitButton(i);
                        }

                        break;
                    }
                }

                if (InputMapping.jumpInput.GetButtonDown())
                {
                    HitButton(m_selectedButton);
                }

                if (autoScrolling)
                {
                    if (m_CurrentSelectTime <= 0)
                    {
                        if (currentState == MenuController.ScrollHoldState.Top)
                        {
                            m_selectedButton--;
                            resetTime = true;
                        }
                        else if (currentState == MenuController.ScrollHoldState.Bottom)
                        {
                            m_selectedButton++;
                            resetTime = true;
                        }
                    }
                }
                else if(!sameDirection)
                {
                    if (currentState == MenuController.ScrollHoldState.Top)
                    {
                        m_selectedButton--;
                    }
                    else if (currentState == MenuController.ScrollHoldState.Bottom)
                    {
                        m_selectedButton++;
                    }
                }

                if (m_selectedButton < 0)
                    m_selectedButton = m_buttonScripts.Count - 1;
                else if (m_selectedButton >= m_buttonScripts.Count)
                    m_selectedButton = 0;
            }

            return resetTime;
        }

        #endregion

        #region Public/Protected Methods

        /// <summary>
        /// 	Called when the state becomes active.
        /// </summary>
        /// <param name="parent">Parent.</param>
        public override void OnEnter(MonoBehaviour parent)
		{
			base.OnEnter(parent);

			ShowPanel(parent);
		}

		/// <summary>
		/// 	Called before another state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnExit(MonoBehaviour parent)
		{
			base.OnExit(parent);

			HidePanel(parent);
		}

		/// <summary>
		/// Called when the menu is entered from a higher level menu. Not called when the menu becomes active after exiting a deeper menu.
		/// This is called after OnEnter.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public virtual void OnPush(MonoBehaviour parent) {}

		/// <summary>
		/// Called when the menu is exiting to return to a higher level menu.  Not called when entering a deeper menu from this menu.
		/// This is called before OnExit.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public virtual void OnPop(MonoBehaviour parent) {}

        /// <summary>
        /// Called when screen resolution has been changed.
        /// </summary>
        public virtual void OnScreenResolutionChange(MonoBehaviour parent)
        {
            OnEnter(parent);
        }

		/// <summary>
		/// Draw out the graphics for this menu state.
		/// </summary>
        public virtual void Draw()
        {
            if (buttonPos == null || buttonPos.Count == 0)
                return;

            for (int i = 0; i < buttonPos.Count; i++)
            {
                //Color boxColor;
                HUDDigit.TextColor textColor;

                if (i == m_selectedButton)
                {
                    //boxColor = new Color(0, 0, 0, 0.5f);
                    textColor = HUDDigit.TextColor.Blue;
                }
                else
                {
                    //boxColor = new Color(0,0,0,0);
                    textColor = HUDDigit.TextColor.Yellow;
                }

                //GUI.color = boxColor;
                //GUI.DrawTexture(buttonPos[i], Texture2D.whiteTexture);

                MenuController menuController = MenuController.instance;

                GUI.color = Color.white;
                string capitalized = m_buttonText[i].ToUpper();
                char[] charArray = capitalized.ToCharArray();
                for (int c = 0; c < charArray.Length; c++)
                {
                    Rect toGet = menuController.m_Digits.GetDigit(charArray[c], textColor);
                    Rect doublePos = new Rect(buttonPos[i].x + buttonPos[i].width * c / charArray.Length,
                        buttonPos[i].y + buttonPos[i].height / 2 - buttonPos[i].width / charArray.Length / 2,
                        buttonPos[i].width / charArray.Length,
                        buttonPos[i].width / charArray.Length);
                    GUI.DrawTextureWithTexCoords(doublePos, menuController.m_Digits.m_DigitSpriteTexture, toGet);
                }

                //GUI.Label(buttonPos[i], m_buttonText[i]);
            }
        }

		/// <summary>
		/// Determines whether the menu system is currently in a non-state(Essentially null state) or not.
		/// </summary>
		/// <returns><c>true</c> if this instance is a non-state; otherwise, <c>false</c>.</returns>
        public virtual bool IsNonState()
        {
            return false;
        }

		/// <summary>
		/// 	Gets the menu controller.
		/// </summary>
		/// <returns>The menu controller.</returns>
		/// <param name="parent">Parent.</param>
		protected MenuController GetMenuController(MonoBehaviour parent)
		{
			return parent as MenuController;
		}

		/// <summary>
		/// Called when button N has been hit/clicked in the menu.
		/// </summary>
		/// <param name="n">Button number N.</param>
        protected virtual void HitButton(int n)
        {
            m_buttonScripts[n].OnButtonClicked();
        }

		/// <summary>
		/// Populates the menu based on the current menu script with buttons, sliders, text, etc.
		/// </summary>
        protected virtual void PopulateScriptList() {}

		#endregion

		#region Private Functions

		/// <summary>
		/// 	Shows the panel.
		/// </summary>
		/// <param name="parent">Parent.</param>
		private void ShowPanel(MonoBehaviour parent)
		{
			if (m_PanelInstance == null)
				CreatePanel(parent);

			m_PanelInstance.gameObject.SetActive(true);
		}

		/// <summary>
		/// 	Hides the panel.
		/// </summary>
		/// <param name="parent">Parent.</param>
		private void HidePanel(MonoBehaviour parent)
		{
			if (m_PanelInstance != null)
				m_PanelInstance.gameObject.SetActive(false);
		}

		/// <summary>
		/// 	Creates the panel.
		/// </summary>
		/// <param name="parent">Parent.</param>
		private void CreatePanel(MonoBehaviour parent)
		{
			if (!ObjectUtils.LazyInstantiate(m_PanelPrefab, ref m_PanelInstance))
				return;

			m_PanelInstance.transform.SetParent(parent.transform, false);

			Subscribe();
		}

		/// <summary>
		/// 	Subscribe to panel events.
		/// </summary>
		private void Subscribe()
		{
			if (m_PanelInstance == null)
				return;

			m_PanelInstance.onPreviousMenuCallback += OnPreviousMenu;
			m_PanelInstance.onNextMenuCallback += OnNextMenu;
		}

		/// <summary>
		/// 	Unsubscribe from panel events.
		/// </summary>
		private void Unsubscribe()
		{
			if (m_PanelInstance == null)
				return;

			m_PanelInstance.onPreviousMenuCallback -= OnPreviousMenu;
			m_PanelInstance.onNextMenuCallback -= OnNextMenu;
		}

		/// <summary>
		/// 	Called when the panel wants to return to the previous menu.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="eventArgs">Event arguments.</param>
		private void OnPreviousMenu(object sender, EventArgs eventArgs)
		{
			MenuController controller = MenuController.instance;
			controller.PreviousMenu();
		}

		/// <summary>
		/// 	Called when the panel wants to change to a different menu.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="eventArgs">Event arguments.</param>
		private void OnNextMenu(object sender, EventArg<AbstractMenuState> eventArgs)
		{
			MenuController controller = MenuController.instance;
			controller.NextMenu(eventArgs.data);
		}

		#endregion
	}
}