﻿using Hydra.HydraCommon.Abstract;
using Hydra.HydraCommon.Utils;
using UnityEngine;

namespace AntonCoolpecker.Abstract.Prefabs
{
	/// <summary>
	/// 	AbstractAssetInstantiator is the base class for all asset instantiators.
	/// 
	/// 	Asset instantiators help us avoid turning assets into fragile prefabs by
	/// 	instantiating them at runtime instead.
	/// </summary>
	[ExecuteInEditMode]
	public abstract class AbstractAssetInstantiator<T> : HydraMonoBehaviour
		where T : Component
	{
		#region Variables

		[SerializeField] private T m_Asset;

		private T m_Instance;

		#endregion

		#region Properties

		/// <summary>
		/// 	Gets or sets the asset.
		/// </summary>
		/// <value>The asset.</value>
		public T asset { get { return m_Asset; } set { m_Asset = value; } }

		/// <summary>
		/// 	Gets or sets the instance.
		/// </summary>
		/// <value>The instance.</value>
		public T instance { get { return m_Instance; } set { m_Instance = value; } }

		#endregion

		#region Messages

		/// <summary>
		/// 	Called when the object is instantiated.
		/// </summary>
		protected override void Awake()
		{
			base.Awake();

			InstantiateAsset();
		}

		/// <summary>
		/// 	Called when the object is destroyed.
		/// </summary>
		protected override void OnDestroy()
		{
			base.OnDestroy();

			m_Instance = ObjectUtils.SafeDestroyGameObject(m_Instance);
		}

		#endregion

		#region Methods

		/// <summary>
		/// 	Instantiates the asset.
		/// </summary>
		private void InstantiateAsset()
		{
			if (m_Asset == null && !Application.isPlaying)
				return;

			// We actually want this to throw a null ref at runtime if the asset is null.
			// Lets us know an asset hierarchy is broken.
			if (ObjectUtils.LazyInstantiate(m_Asset, ref m_Instance))
				ConfigureInstance(m_Instance);
		}

		/// <summary>
		/// 	Configures the instance.
		/// </summary>
		/// <param name="instance">Instance.</param>
		protected virtual void ConfigureInstance(T instance)
		{
			instance.transform.SetParent(transform, false);
		}

		#endregion
	}
}