﻿using AntonCoolpecker.Abstract.Player;
using AntonCoolpecker.Concrete;
using Hydra.HydraCommon.Abstract;
using System.Collections.Generic;
using UnityEngine;

namespace AntonCoolpecker.Abstract.Platforms
{
	/// <summary>
	/// Moving Platform - Basic abstract class for all kinds of moving platforms
	/// </summary>
	[RequireComponent(typeof(Rigidbody))]
	[RequireComponent(typeof(Collider))]
	public abstract class MovingPlatform : HydraMonoBehaviour
	{
		#region Variables
		
		private bool m_IsSupportingPlayer;
		private List<CharacterLocomotor> m_CarriedCharacters;

		#endregion

		#region Properties

		/// <summary>
		/// Gets all the character locomotors currently on the platform
		/// </summary>
		/// <value>The carried characters.</value>
        public List<CharacterLocomotor> carriedCharacters { get { return m_CarriedCharacters; } }

		/// <summary>
		/// Gets or sets a value indicating whether this <see cref="AntonCoolpecker.Abstract.Platforms.MovingPlatform"/> is
		/// currently holding the player or not.
		/// </summary>
		/// <value><c>true</c> if it is supporting player; otherwise, <c>false</c>.</value>
		public bool isSupportingPlayer
		{
			get { return m_IsSupportingPlayer; }
			set
			{
				if (value == m_IsSupportingPlayer)
					return;
				
				if (value)
					OnPlayerLanding();
				else
					OnPlayerLeaving();
				
				m_IsSupportingPlayer = value;
			}
		}

		#endregion

		#region Override Functions

		/// <summary>
		/// 	Called when the component is enabled.
		/// </summary>
		protected override void OnEnable()
		{
			base.OnEnable();

			m_CarriedCharacters = new List<CharacterLocomotor>();
		}

		/// <summary>
		/// 	Called once every frame.
		/// </summary>
		protected override void Update()
		{
			base.Update();

			List<CharacterLocomotor> removeThese = new List<CharacterLocomotor>();

			foreach (CharacterLocomotor cl in m_CarriedCharacters)
			{
				if (cl.GroundedCast().collider != collider)
				{
					removeThese.Add(cl);

					if (cl.GetComponent<AbstractPlayerController>() != null)
						isSupportingPlayer = false;
				}
			}

			foreach (CharacterLocomotor cl in removeThese)
				m_CarriedCharacters.Remove(cl);
		}

		#endregion

		#region Normal Functions/Methods

		/// <summary>
		/// Handles the platform movement.
		/// </summary>
		/// <param name="movement">Movement.</param>
		/// <param name="rotation">Rotation.</param>
		protected void HandleMovement(Vector3 movement, Quaternion rotation)
		{
			RaycastHit hitInfo;
			if (collider.attachedRigidbody.SweepTest(movement, out hitInfo, movement.magnitude))
				return;

			foreach (CharacterLocomotor cl in m_CarriedCharacters)
				cl.transform.position = transform.InverseTransformPoint(cl.transform.position);

			transform.position += movement;
			transform.rotation *= rotation;

			foreach (CharacterLocomotor cl in m_CarriedCharacters)
			{
				cl.transform.position = transform.TransformPoint(cl.transform.position);
				cl.transform.rotation *= rotation;
			}
		}

		/// <summary>
		/// 	Add the specified CharacterLocomotor to the list of CLs that move alongside this platform.
		/// </summary>
		/// <param name="cl">The CL to add.</param>
		public bool Add(CharacterLocomotor cl)
		{
			bool contained = m_CarriedCharacters.Contains(cl);

			if (!contained)
			{
				m_CarriedCharacters.Add(cl);

				if (cl.GetComponent<AbstractPlayerController>() != null)
					isSupportingPlayer = true;
			}

			return contained;
		}

		#endregion

		#region Virtual Functions

		/// <summary>
		/// Called when the player has landed on the platform.
		/// </summary>
		public virtual void OnPlayerLanding() {}

		/// <summary>
		/// Called when the player has left the platform.
		/// </summary>
		public virtual void OnPlayerLeaving() {}

		#endregion
	}
}