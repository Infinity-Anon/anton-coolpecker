﻿using AntonCoolpecker.Concrete.Platforms;
using AntonCoolpecker.Concrete.Utils;
using System.Collections;
using UnityEngine;

namespace AntonCoolpecker.Abstract.Platforms
{
	/// <summary>
	/// Abstract track instruction - Base class for track instructions.
	/// </summary>
	public abstract class AbstractTrackInstruction : MonoBehaviour
	{
		#region Variables

		[SerializeField] private Timer m_Timer;

		#endregion

		#region Public Functions/Methods

		/// <summary>
		/// 	Gets the timer that defines how long track followers should obey this instruction.
		/// </summary>
		/// <value>The timer.</value>
		public Timer timer { get { return new Timer(m_Timer); } }
		
		/// <summary>
		/// 	Gets the instruction set that comes after this object's set.
		/// </summary>
		/// <value>The next instruction set.</value>
		public virtual AbstractTrackInstruction[] nextInstructionSet { get { return null; } }
		
		/// <summary>
		/// 	Gets all instructions on this GameObject.
		/// </summary>
		/// <value>The instruction set.</value>
		public AbstractTrackInstruction[] instructionSet { get { return GetComponents<AbstractTrackInstruction>(); } }
		
		/// <summary>
		/// 	Adjusts a follower without changing its position, rotation, or scale.
		/// </summary>
		/// <param name="follower">Follower.</param>
		public virtual void UpdateFollower(TrackFollower follower) {}
		
		/// <summary>
		/// 	Gets the position that a track follower should be moving towards.
		/// </summary>
		/// <returns>The next position.</returns>
		/// <param name="follower">Follower.</param>
		public virtual Vector3 GetNextPosition(TrackFollower follower)
		{
			return transform.position;
		}
		
		/// <summary>
		/// 	Gets the rotation that a track follower should be turning towards.
		/// </summary>
		/// <returns>The next rotation.</returns>
		/// <param name="follower">Follower.</param>
		public virtual Quaternion GetNextRotation(TrackFollower follower)
		{
			return transform.rotation;
		}

		#endregion
	}
}