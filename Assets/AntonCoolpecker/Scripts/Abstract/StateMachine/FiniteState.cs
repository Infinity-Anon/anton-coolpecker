﻿using Hydra.HydraCommon.Abstract;
using UnityEngine;

namespace AntonCoolpecker.Abstract.StateMachine
{
	/// <summary>
	/// 	Finite state.
	/// </summary>
	public abstract class FiniteState<T> : HydraScriptableObject
		where T : FiniteState<T>
	{
		#region Virtual Functions/Methods

		/// <summary>
		/// 	Returns a state for transition. Return self if no transition.
		/// </summary>
		/// <returns>The next state.</returns>
		/// <param name="parent">Parent.</param>
		public virtual T GetNextState(MonoBehaviour parent)
		{
			return this as T;
		}

		/// <summary>
		/// 	Called when the parent is enabled.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public virtual void OnParentEnable(MonoBehaviour parent) {}

		/// <summary>
		/// 	Called when the state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public virtual void OnEnter(MonoBehaviour parent) {}

		/// <summary>
		/// 	Called before another state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public virtual void OnExit(MonoBehaviour parent) {}

		/// <summary>
		/// 	Called when the parent updates.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public virtual void OnUpdate(MonoBehaviour parent) {}

		/// <summary>
		/// 	Called when the parent's physics update.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public virtual void OnFixedUpdate(MonoBehaviour parent) {}

		/// <summary>
		/// 	Called when the parent late updates.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public virtual void OnLateUpdate(MonoBehaviour parent) {}

		#endregion
	}
}