﻿using UnityEngine;

namespace AntonCoolpecker.Abstract.StateMachine
{
	/// <summary>
	/// 	Abstract state machine.
	/// </summary>
	public abstract class AbstractStateMachine<T>
		where T : FiniteState<T>
	{
		#region Variables

		private T m_ActiveState;

		#endregion

		#region Properties

		/// <summary>
		/// 	Gets the active state.
		/// </summary>
		/// <value>The active state.</value>
		public T activeState
		{
			get
			{
				if (m_ActiveState == null)
				{
					if (initialState == null)
						throw new UnassignedReferenceException();
					
					m_ActiveState = initialState;
				}

				return m_ActiveState;
			}
		}

		/// <summary>
		/// 	Gets the initial state.
		/// </summary>
		/// <value>The initial state.</value>
		public abstract T initialState { get; }

		#endregion

		#region Public Methods/Functions

		/// <summary>
		/// 	Sets the active state.
		/// </summary>
		/// <param name="state">State.</param>
		/// <param name="parent">Parent.</param>
		public T SetActiveState(T state, MonoBehaviour parent)
		{
			if (state == null)
				throw new UnassignedReferenceException();

			if (state == activeState)
				return activeState;

			T previous = activeState;

			activeState.OnExit(parent);
			m_ActiveState = state;
			activeState.OnEnter(parent);

			OnStateChanged(previous, activeState, parent);

			return activeState;
		}

		/// <summary>
		/// 	To be called when the parent is enabled.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public void OnEnable(MonoBehaviour parent)
		{
			activeState.OnParentEnable(parent);
			activeState.OnEnter(parent);
		}

		/// <summary>
		/// 	Performs an update on the active state.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public void Update(MonoBehaviour parent)
		{
			ProceedState(parent);
			activeState.OnUpdate(parent);
		}

		/// <summary>
		/// 	Performs a physics update on the active state.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public void FixedUpdate(MonoBehaviour parent)
		{
			ProceedState(parent);
			activeState.OnFixedUpdate(parent);
		}

		/// <summary>
		/// Lates the update.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public void LateUpdate(MonoBehaviour parent)
		{
			ProceedState(parent);
			activeState.OnLateUpdate(parent);
		}

		#endregion

		#region Protected Functions

		/// <summary>
		/// 	Proceeds to the next state.
		/// </summary>
		/// <param name="parent">Parent.</param>
		protected void ProceedState(MonoBehaviour parent)
		{
			T next = activeState.GetNextState(parent);

			if (next == null)
			{
				string error = string.Format("{0} is returning null for next state.", activeState.GetType().Name);
				throw new UnassignedReferenceException(error);
			}

			SetActiveState(next, parent);
		}

		/// <summary>
		/// 	Called when the object changes.
		/// </summary>
		/// <param name="previous">Previous.</param>
		/// <param name="current">Current.</param>
		/// <param name="parent">Parent.</param>
		protected virtual void OnStateChanged(T previous, T current, MonoBehaviour parent) {}

		#endregion
	}
}