﻿using AntonCoolpecker.Concrete.Utils;
using UnityEngine;

namespace AntonCoolpecker.Abstract.StateMachine
{
	/// <summary>
	/// 	Timable finite state.
	/// </summary>
	public abstract class TimableFiniteState<T> : FiniteState<T>
		where T : TimableFiniteState<T>
	{
		#region Properties

		/// <summary>
		/// 	Gets the state to transition to after the timer elapses.
		/// </summary>
		/// <value>The state of the timer.</value>
		protected virtual T timerState { get { return this as T; } }

		#endregion

		#region Virtual Methods

		/// <summary>
		/// 	Gets the timer to transition to another state.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public virtual Timer GetStateTimer(MonoBehaviour parent)
		{
			return null;
		}

		#endregion

		#region Override Functions/Methods

		/// <summary>
		/// Called when the state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnEnter(MonoBehaviour parent)
		{
			base.OnEnter(parent);

			if (GetStateTimer(parent) != null)
				GetStateTimer(parent).Reset();
		}

		/// <summary>
		/// 	Returns a state for transition. Return self if no transition.
		/// </summary>
		/// <returns>The next state.</returns>
		/// <param name="parent">Parent.</param>
		public override T GetNextState(MonoBehaviour parent)
		{
			if (GetStateTimer(parent) != null && GetStateTimer(parent).complete)
				return timerState;

			return base.GetNextState(parent);
		}

		#endregion
	}
}
