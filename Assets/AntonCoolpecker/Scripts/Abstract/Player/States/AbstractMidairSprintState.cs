﻿using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using AntonCoolpecker.Utils;
using System.Collections;
using UnityEngine;

namespace AntonCoolpecker.Abstract.Player.States
{
    /// <summary>
    /// Abstract midair sprinting state
    /// </summary>
    public abstract class AbstractMidairSprintState : AbstractMidairState
    {
		#region Variables

        [Tweakable("Midair Sprint")] [SerializeField] protected float m_MaxSpeedMultiplier = 1.5f;
        [Tweakable("Midair Sprint")] [SerializeField] protected float m_MaxRotationMultiplier = 1f;
        [Tweakable("Midair Sprint")] [SerializeField] protected float m_MinSpeedMultiplier = 1f;
        [Tweakable("Midair Sprint")] [SerializeField] protected float m_MinRotationMultiplier = 1f;
        [Tweakable("Midair Sprint")] [SerializeField] protected bool m_AxisIncrease = true; //Decides whether the player can manually adjust the sprinting by axis or not

		#endregion

        #region Properties

        /// <summary>
        /// Override default max speed with own value
        /// </summary>
        protected override float maxSpeedMultiplier
		{
			get
			{
				if (m_AxisIncrease) 
				{
                    return Mathf.Lerp (m_MinSpeedMultiplier, m_MaxSpeedMultiplier, InputMapping.sprintInput.GetAxisRaw());
				}
				else
					return m_MaxSpeedMultiplier;
			}
		}

		/// <summary>
        /// override default rotate speed with own value
        /// </summary>
		protected override float rotateSpeedMultiplier
		{
			get
			{
				if (m_AxisIncrease) 
				{
			        return Mathf.Lerp (m_MinRotationMultiplier, m_MaxRotationMultiplier, InputMapping.sprintInput.GetAxisRaw());
			    }
				else
					return m_MaxRotationMultiplier;
			}
		}

        #endregion
    }
}