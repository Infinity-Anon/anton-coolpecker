﻿using UnityEngine;

namespace AntonCoolpecker.Abstract.Player.States
{
	/// <summary>
	/// Abstract landing state. Currently unused.
	/// </summary>
	//TODO: DELETE THIS SCRIPT?
	public class AbstractLandingState : MonoBehaviour
	{
		/// <summary>
		/// Start this instance.
		/// </summary>
		private void Start() {}

		/// <summary>
		/// Update this instance.
		/// </summary>
		private void Update() {}
	}
}