using AntonCoolpecker.Abstract.Player.States.Hitstun;
using AntonCoolpecker.Abstract.StateMachine;
using AntonCoolpecker.Concrete.Cameras;
using AntonCoolpecker.Concrete.Messaging;
using AntonCoolpecker.Utils.Time;
using Hydra.HydraCommon.Utils;
using System;
using UnityEngine;

namespace AntonCoolpecker.Abstract.Player.States
{
	/// <summary>
	/// 	AbstractPlayerState is the base class for all player states.
	/// </summary>
	public abstract class AbstractPlayerState : TimableFiniteState<AbstractPlayerState>
	{
		#region Variables

		[SerializeField] private bool m_CanSwitchCharacters; //Can the player switch characters in the current state?
		[SerializeField] private string m_AnimatorState; //Sets the animation state for the currents state
		[SerializeField] private AbstractHitstunState m_HitstunState; //Reference to hitstun state
		[SerializeField] private bool m_DisabledControls; //Are the player controls disabled in the current state?
		[SerializeField] private bool m_UseFriction; //Is friction used in the current state?
		[SerializeField] private CameraController.Mode m_CameraMode; //Camera mode for the current state

		#endregion

		#region Properties

		/// <summary>
		/// 	Gets and sets a value indicating whether controls are activated or not.
		/// </summary>
		/// <value><c>true</c> if the controls are not activated; otherwise, <c>false</c>.</value>
		public bool disabledControls { get { return m_DisabledControls; } set { m_DisabledControls = value; } }

		/// 	Gets a value indicating whether the player can switch characters while in this state.
		/// </summary>
		/// <value><c>true</c> if can switch characters; otherwise, <c>false</c>.</value>
		public bool canSwitchCharacters { get { return m_CanSwitchCharacters; } }

		/// <summary>
		/// 	Gets the state of the animator.
		/// </summary>
		/// <value>The state of the animator.</value>
		public string animatorState { get { return m_AnimatorState; } }

		/// <summary>
		/// 	Gets the camera mode.
		/// </summary>
		/// <value>The camera mode.</value>
		public CameraController.Mode cameraMode { get { return m_CameraMode; } }

		/// <summary>
		/// 	Gets the max speed multiplier.
		/// </summary>
		/// <value>The max speed multiplier.</value>
		protected virtual float maxSpeedMultiplier { get { return 1.0f; } }

		/// <summary>
		/// 	Gets the acceleration multiplier.
		/// </summary>
		/// <value>The acceleration multiplier.</value>
		protected virtual float accelerationMultiplier { get { return 1.0f; } }

		/// <summary>
		/// 	Gets the jump height multiplier.
		/// </summary>
		/// <value>The jump height multiplier.</value>
		protected virtual float jumpHeightMultiplier { get { return 1.0f; } }

		/// <summary>
		/// 	Gets the rotate speed multiplier.
		/// </summary>
		/// <value>The rotate speed multiplier.</value>
		protected virtual float rotateSpeedMultiplier { get { return 1.0f; } }

		#endregion

		#region Methods

		/// <summary>
		/// Sets the state of the animator.
		/// </summary>
		/// <param name="newAnimatorState">New animator state.</param>
		public void SetAnimatorState(string newAnimatorState)
		{
			m_AnimatorState = newAnimatorState;
		}

		/// <summary>
		/// 	Returns the current movement vector, in world space, as determined
		/// 	by player input.
		/// </summary>
		/// <returns>The movement vector.</returns>
		/// <param name="parent">Parent.</param>
		public Vector3 GetMovementVector(MonoBehaviour parent)
		{
			AbstractPlayerController player = GetPlayerController(parent);

			Vector3 inputVector = GetCameraOrientedInput(parent);
			return inputVector * player.maxSpeed * maxSpeedMultiplier;
		}

		/// <summary>
		/// 	Jumps upwards.
		/// </summary>
		public void Jump(AbstractPlayerController player)
		{
			Jump(Vector3.up, player);
		}

		/// <summary>
		/// 	Jump in the specified direction.
		/// </summary>
		/// <param name="direction">Direction.</param>
		/// <param name="player">Player.</param>
		public void Jump(Vector3 direction, AbstractPlayerController player)
		{
			direction.Normalize();
			player.characterLocomotor.velocity += direction *
												  (player.JumpForce(player.jumpHeight * jumpHeightMultiplier) -
												   Vector3.Dot(direction, player.characterLocomotor.velocity));

			player.airTracker.canJumpCancel = true;
			PlayerMessages.BroadcastOnJump(player);
		}

		/// <summary>
		/// 	Jump in a specified direction with a specific value.
		/// </summary>
		/// <param name="direction">Direction.</param>
		public void Jump(Vector3 direction, AbstractPlayerController player, float jumpValue)
		{
			player.characterLocomotor.velocity += direction.normalized * player.JumpForce(jumpValue);

			player.airTracker.canJumpCancel = true;
			PlayerMessages.BroadcastOnJump(player);
		}

		/// <summary>
		/// 	Temporarily stops the player from falling.
		/// </summary>
		public void StartFloating(MonoBehaviour parent)
		{
			AbstractPlayerController player = GetPlayerController(parent);

			player.characterLocomotor.useGravity = false;
			player.characterLocomotor.velocity = new Vector3(player.characterLocomotor.velocity.x, 0.0f,
															 player.characterLocomotor.velocity.z);
		}

		/// <summary>
		/// 	Temporarily stops the player from falling, and allows sick aerials.
		/// </summary>
		public void StartFreeFloating(MonoBehaviour parent)
		{
			AbstractPlayerController player = GetPlayerController(parent);

			player.characterLocomotor.useGravity = false;
		}

		/// <summary>
		/// 	Causes the player to fall normally, if the player was floating before.
		/// </summary>
		public void StopFloating(MonoBehaviour parent)
		{
			AbstractPlayerController player = GetPlayerController(parent);

			player.characterLocomotor.useGravity = true;
		}

		/// <summary>
		/// 	Returns the hitstun state associated with this player state.
		/// </summary>
		/// <returns>The hitstun state.</returns>
		public virtual AbstractHitstunState GetHitstunState()
		{
			if (m_HitstunState)
				return m_HitstunState;

			throw new NullReferenceException("Hitstun state not set.");
		}

		/// <summary>
		/// 	Called when the parent's CharacterController collides while moving.
		/// </summary>
		/// <param name="hit">Hit.</param>
		/// <param name="parent">Parent.</param>
		public virtual void ParentControllerColliderHit(ControllerColliderHit hit, MonoBehaviour parent) {}

		/// <summary>
		/// 	Called when the state becomes active.
		/// </summary>
		public override void OnEnter(MonoBehaviour parent)
		{
			base.OnEnter(parent);

			AbstractPlayerController player = GetPlayerController(parent);

			// Transition the camera
			CameraController playerCamera = player.playerCamera;
			if (playerCamera.GetActiveTrigger() == null)
				playerCamera.SetMode(m_CameraMode);

			player.characterLocomotor.useFriction = m_UseFriction;
		}

		/// <summary>
		/// 	Called when the parent's physics update.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnFixedUpdate(MonoBehaviour parent)
		{
			base.OnFixedUpdate(parent);

			AbstractPlayerController player = GetPlayerController(parent);

			StateUpdate(player);

			if (!m_DisabledControls)
				HandleMovement(parent);
		}

		/// <summary>
		/// 	Returns a state for transition during a collision. Return self if no transition.
		/// </summary>
		/// <returns>The next state.</returns>
		/// <param name="hit">Hit.</param>
		/// <param name="parent">Parent.</param>
		public virtual AbstractPlayerState GetCollisionNextState(ControllerColliderHit hit, MonoBehaviour parent)
		{
			return this;
		}

		#endregion

		#region Protected Methods

		///<summaty>
		/// Performs state specific actions that need to update once per physics timestep.
		/// </summary>
		/// <param name="player">The player.</para>
		protected virtual void StateUpdate(AbstractPlayerController player)
		{
			//*Pomf* what are we gonna do in the implimentation, onii-chan?
		}

		/// <summary>
		/// Resolves the character's rotation due to the movement that just occurred in the current frame.
		/// </summary>
		/// <param name="player">The player character that just moved and needs to rotate.</param>
		/// <param name="movement">The normalised movement vector input for this frame, in world space.</param>
		protected virtual void ResolveRotation(AbstractPlayerController player, Vector3 movement)
		{
			Vector3 flatV = player.characterLocomotor.flatVelocity;

			if (flatV == Vector3.zero)
				SnapToMovementVector(player, movement);
			else
				RotateTowardsMovementVector(player, movement);
		}

		/// <summary>
		/// Gets the change in velocity, given the current velocity, desired final velocity, and max acceleration.
		/// </summary>
		/// <param name="player">The player.</param>
		/// <param name="desiredV">The desired velocity, according to the character's max speed and input direction.</param>
		/// <param name="currentV">The character's current velocity.</param>
		/// <returns>The change in velocity for this step.</returns>
		protected virtual Vector3 ResolveVelocityDelta(AbstractPlayerController player, Vector3 desiredV, Vector3 currentV)
		{
			//Ignore the current vertical component.  Note that some states should not ignore the vertical component when they override this method.
			currentV.y = 0;

			// We're not moving yet.
			if (HydraMathUtils.Approximately(currentV.magnitude, 0.0f))
			{
				Vector3 inputVector = GetInputVector(player);
				if (player.runThreshold >= inputVector.magnitude)
					return Vector3.zero;
			}

			Vector3 deltaV = desiredV - currentV;

			deltaV = Vector3.ClampMagnitude(deltaV, player.acceleration * accelerationMultiplier * GameTime.deltaTime);
			return deltaV;
		}

		/// <summary>
		/// Gets a normalised vector that represents the player's directional input (in controller/camera space).
		/// </summary>
		/// <returns>The input vector.</returns>
		protected virtual Vector3 GetInputVector(AbstractPlayerController player)
		{
			float moveHorizontal =
				AntonCoolpecker.Concrete.Configuration.Controls.Mapping.InputMapping.horizontalInput.GetAxisRaw();
			float moveVertical = AntonCoolpecker.Concrete.Configuration.Controls.Mapping.InputMapping.verticalInput.GetAxisRaw();
			return new Vector3(moveHorizontal, 0.0f, moveVertical);
		}

		/// <summary>
		/// Snaps the player's rotation towards a movement vector.
		/// </summary>
		/// <param name="player">Player.</param>
		/// <param name="movement">Movement.</param>
		protected void SnapToMovementVector(AbstractPlayerController player, Vector3 movement)
		{
			Quaternion lookRotation = Quaternion.LookRotation(player.transform.forward);

			if (movement == Vector3.zero)
				return;

			Quaternion targetRotation = Quaternion.LookRotation(movement);
			player.transform.rotation = Quaternion.RotateTowards(lookRotation, targetRotation, 360.0f);
		}

		/// <summary>
		/// 	Rotates towards movement vector.
		/// </summary>
		/// <param name="player">Player.</param>
		/// <param name="movement">Movement.</param>
		protected virtual void RotateTowardsMovementVector(AbstractPlayerController player, Vector3 movement)
		{
			Quaternion lookRotation = Quaternion.LookRotation(player.transform.forward);

			if (movement == Vector3.zero)
				return;

			Quaternion targetRotation = Quaternion.LookRotation(movement);
			float delta = player.rotateSpeed * rotateSpeedMultiplier * GameTime.deltaTime;

			player.transform.rotation = Quaternion.RotateTowards(lookRotation, targetRotation, delta);
		}

		/// <summary>
		/// 	Rotates towards velocity vector.
		/// </summary>
		/// <param name="player">Player.</param>
		protected void RotateTowardsVelocityVector(AbstractPlayerController player)
		{
			Vector3 flatV = player.characterLocomotor.velocity;
			flatV.y = 0;

			if (flatV == Vector3.zero)
				return;

			// player.rotateSpeed could be inversely proportional to movespeed
			Quaternion lookRotation = Quaternion.LookRotation(player.transform.forward);
			Quaternion targetRotation = Quaternion.LookRotation(flatV);
			float delta = player.rotateSpeed * rotateSpeedMultiplier * GameTime.deltaTime;

			player.transform.rotation = Quaternion.RotateTowards(lookRotation, targetRotation, delta);
		}

		/// <summary>
		/// 	Handles the movement.
		/// </summary>
		protected virtual void HandleMovement(MonoBehaviour parent)
		{
			AbstractPlayerController player = GetPlayerController(parent);

			Vector3 desiredMovement = GetMovementVector(parent);
			ResolveRotation(player, desiredMovement);

			Vector3 deltaV = ResolveVelocityDelta(player, desiredMovement, player.characterLocomotor.velocity);
			player.characterLocomotor.velocity += deltaV;
		}

		/// <summary>
		/// 	Transforms the input vector from camera space into world space.
		/// </summary>
		/// <returns>The camera oriented input.</returns>
		protected virtual Vector3 GetCameraOrientedInput(MonoBehaviour parent)
		{
			AbstractPlayerController player = GetPlayerController(parent);

			Vector3 input = GetInputVector(player);
			input = Vector3.ClampMagnitude(input, 1.0f);

			Vector3 cameraForward = player.playerCamera.transform.forward;
			cameraForward.y = 0.0f;

			return Quaternion.LookRotation(cameraForward) * input;
		}

		#endregion

		#region Static Methods

		/// <summary>
		/// 	Gets the player controller.
		/// </summary>
		/// <returns>The player controller.</returns>
		/// <param name="parent">Parent.</param>
		public static AbstractPlayerController GetPlayerController(MonoBehaviour parent)
		{
			return parent as AbstractPlayerController;
		}

		#endregion
	}
}