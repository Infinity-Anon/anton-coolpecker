﻿using UnityEngine;

namespace AntonCoolpecker.Abstract.Player.States
{
	/// <summary>
	/// Abstract dead state - Base class for any states where the player dies.
	/// </summary>
	public class AbstractDeadState : AbstractPlayerState
	{
		/// <summary>
		/// Called when the state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnEnter(MonoBehaviour parent) 
		{
			base.OnEnter(parent);
		}

		/// <summary>
		/// Called before another state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnExit(MonoBehaviour parent)
		{
			base.OnExit(parent);

			AbstractPlayerController player = GetPlayerController(parent);

			// The purifying cycle of birth, death, and rebirth
			ResetPhysics (player);
		}
			
		/// <summary>
		/// Cleanup in case of dying underwater
		/// </summary>
		/// <param name="player">Player.</param>
		private void ResetPhysics(AbstractPlayerController player)
		{
			Rigidbody m_Rigidy = player.gameObject.GetComponent<Rigidbody>();
			CapsuleCollider m_Collidy = player.gameObject.GetComponent<CapsuleCollider>();

			if (m_Rigidy) 
			{
				m_Rigidy.constraints = RigidbodyConstraints.None;
				Destroy (m_Rigidy);
			}

			if (m_Collidy) 
			{
				Destroy (m_Collidy);
			}

			///*Re-enable locomotor, gravity and camera limits
			//player.characterLocomotor.enabled = true;
			player.characterLocomotor.characterController.enabled = true;
			player.characterLocomotor.useGravity = true;
			player.playerCamera.clampEnabled = true;

			// WORKAROUND: assuming the player won't start in Underwater*State later in that particular body of water, this should reset things properly
			// When exiting towards DeadPlayerState, the water trigger does not have the opportunity to reset bobbing.
			if (player.isInWater && player.waterTrigger != null)
				player.waterTrigger.interpolatePlayerUpwards = true;
		}
	}
}