﻿using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using AntonCoolpecker.Utils;
using AntonCoolpecker.Utils.Time;
using Hydra.HydraCommon.Utils;
using UnityEngine;

namespace AntonCoolpecker.Abstract.Player.States
{
	/// <summary>
	/// 	DashingCoolpeckerState is used while Coolpecker is dashing. 
	/// </summary>
	public abstract class AbstractDashingState : AbstractAttackState
	{
		#region Variables

		[Tweakable("Dash")] [SerializeField] protected bool m_AutoRun = true; //Should the player always go full speed, independent of input magnitude?
		[Tweakable("Dash")] [SerializeField] protected bool m_LimitDashTurnMagnitude = true; //Should the dash be limited when it comes to turning around? (Max value = 1)
		[Tweakable("Dash")] [SerializeField] protected bool m_CameraOriented = true; //Camera-oriented dash if true, input-oriented if false.
		[Tweakable("Dash")] [SerializeField] protected float m_RotateMomentumFactor = 0.2f; //Factor of rotation momentum
		[Tweakable("Dash")] [SerializeField] protected float m_RotateAcceleration = 0.2f; //Acceleration of the dash rotation
		[Tweakable("Dash")] [SerializeField] protected bool m_LedgeCancel = false; //Whether or not dash cancels upon going over the ledge
		[Tweakable("Dash")] [SerializeField] protected float m_DashCancelAngle = 0.75f; //The percent of distance from Forwards to Backwards at which the dash is canceled.

        [Tweakable("Dash")] [SerializeField] internal bool m_InterpolateGravityScaleY = true; //Enable/disable ground interpolation
		[Tweakable("Dash")] [SerializeField] internal bool m_UseExponentialAngleCalc = true; //Calculate the ground interpolation exponentially by surface angle?
		[Tweakable("Dash")] [SerializeField] internal float m_LinearCalcInterpolationMultiplier = 0.01f; //Multiplier for linear ground interpolation
		[Tweakable("Dash")] [SerializeField] internal float m_InverseNormalForceMultiplier = 5f; //Mutiplier for when inverse normal force is applied
		[Tweakable("Dash")] [SerializeField] internal float m_LinearCalcAngleLimitX = 1f; //Minimum surface angle for linear ground interpolation
		[Tweakable("Dash")] [SerializeField] internal float m_LinearCalcAngleLimitY = 40f; //Maximum surface angle for linear ground interpolation
		[Tweakable("Dash")] [SerializeField] internal float m_GravityRayMaxDistance = 2f; //Maximum distance to ground for applying ground interpolation
		[Tweakable("Dash")] [SerializeField] internal float m_LedgeDetectionDistance = 6f; //Vertical distance for detecting vertical ledges.

		private Vector2 m_LinearCalcAngleLimit = new Vector2(1, 40); //Surface angle limits for using linear ground interpolation
		internal bool m_UseLinearCalc = true; //Use linear calculation for the ground interpolation?

		private Quaternion m_RotateMomentum = Quaternion.identity; //Keeps track of the player's rotation momentum

		#endregion

		#region Override Methods

		/// <summary>
        /// Called when a state is entered for the first time.
        /// </summary>
        /// <param name="parent"></param>
		public override void OnEnter(MonoBehaviour parent)
		{
			base.OnEnter(parent);

			m_RotateMomentum = Quaternion.identity;

            m_LinearCalcAngleLimit.x = m_LinearCalcAngleLimitX;
            m_LinearCalcAngleLimit.y = m_LinearCalcAngleLimitY;

            AbstractPlayerController player = GetPlayerController(parent);
            player.airTracker.Reset();

            if (m_InterpolateGravityScaleY)
            {
                m_UseLinearCalc = true;
            }
        }

		/// <summary>
		/// 	Called when the state becomes active.
		/// </summary>
		public override void OnExit(MonoBehaviour parent)
		{
			base.OnExit (parent);

			AbstractPlayerController player = GetPlayerController(parent);

			if (m_InterpolateGravityScaleY) 
			{
				if (player.characterLocomotor.gravityScaleY != player.characterLocomotor.originalGravityScale.y)
					setGravityScaleBack (player, player.characterLocomotor.originalGravityScale.y);

				m_UseLinearCalc = false;
			}
		}

		/// <summary>
        /// Sets pre-oriented input.
        /// </summary>
        /// <param name="player"></param>
        /// <returns></returns>
		protected override Vector3 GetInputVector(AbstractPlayerController player)
		{
			Vector3 inputVector = base.GetInputVector(player);

			if (m_AutoRun)
			{
                Vector3 flatCamera;

                if (m_CameraOriented)
                    flatCamera = new Vector3(player.playerCamera.transform.forward.x, 0, player.playerCamera.transform.forward.z);
                else
                    flatCamera = new Vector3(player.transform.forward.x, 0, player.transform.forward.z);

                Vector3 fullBlast = Quaternion.Inverse(Quaternion.LookRotation(flatCamera)) * player.transform.forward + inputVector.normalized;

                if (m_LimitDashTurnMagnitude)
					fullBlast.z = 1;

                return fullBlast;
			}
			else
				return inputVector;
		}

        /// <summary>
		/// 	Transforms the input vector from camera space into world space when camera orientated. Otherwise sets input relative to Coolpecker.
		/// </summary>
		/// <returns>The camera oriented input.</returns>
		protected override Vector3 GetCameraOrientedInput(MonoBehaviour parent)
        {
            AbstractPlayerController player = GetPlayerController(parent);

            Vector3 input = GetInputVector(player);
            input = Vector3.ClampMagnitude(input, 1.0f);

            Vector3 cameraForward;

            if (m_CameraOriented)
            {
                cameraForward = player.playerCamera.transform.forward;
            }
            else
            {
                cameraForward = player.transform.forward;
            }

            cameraForward.y = 0.0f;

            return Quaternion.LookRotation(cameraForward) * input;
        }

        /// <summary>
        /// 	Rotate towards the desired direction, maintaining the same speed.
        /// </summary>
        /// <param name="player">Player.</param>
        /// <param name="desiredMovement">Desired movement.</param>
        protected override Vector3 ResolveVelocityDelta(AbstractPlayerController player, Vector3 desiredV, Vector3 currentV)
		{
			currentV.y = 0;

			Vector3 newHeading = Vector3.RotateTowards(currentV, desiredV,
													   player.rotateSpeed * Mathf.Deg2Rad * rotateSpeedMultiplier * GameTime.deltaTime,
													   player.acceleration * accelerationMultiplier * GameTime.deltaTime);

			newHeading = m_RotateMomentum * newHeading;
			Quaternion maxRotateMomentum = Quaternion.Lerp(Quaternion.identity, Quaternion.FromToRotation(currentV, newHeading),
														   m_RotateMomentumFactor);
			m_RotateMomentum = Quaternion.Lerp(m_RotateMomentum, maxRotateMomentum, m_RotateAcceleration);

			Vector3 deltaV = newHeading - currentV;

			return deltaV;
		}

		/// <summary>
		/// Face towards the current velocity heading, instead of that frame's input.
		/// </summary>
		protected override void ResolveRotation(AbstractPlayerController player, Vector3 movement)
		{
			RotateTowardsVelocityVector(player);
		}

		/// <summary>
		/// Called when the parent's physics update.
		/// </summary>
		/// <param name="parent">Parent.</param>
        public override void OnFixedUpdate(MonoBehaviour parent)
        {
            base.OnFixedUpdate(parent);

            AbstractPlayerController player = GetPlayerController(parent);

			//*GROUND INTERPOLATION
            if (m_InterpolateGravityScaleY)
            {
                if (m_UseLinearCalc)
                {
                    RaycastHit hitInfo;
                    Vector3 rayPosition = new Vector3(player.transform.position.x, (player.transform.position.y + player.collider.bounds.size.y - 0.1f), player.transform.position.z);
                    Vector3 direction = Vector3.down;
                    float distance = m_GravityRayMaxDistance;
                    LayerMask layerToIgnore = 1 << 4; //IGNORE WATER LAYER
                    layerToIgnore |= 1 << 12; //IGNORE PLAYER LAYER
                    layerToIgnore |= 1 << 18; //IGNORE ATTACK HITBOX LAYER
                    bool canDo = Physics.Raycast(rayPosition, direction, out hitInfo, distance, ~layerToIgnore);
                    float surfaceAngle = 0f;

                    if (m_LedgeCancel)
                    {
                        RaycastHit hitInfo2;
                        Vector3 secondaryRayPos = new Vector3(player.transform.position.x, (player.transform.position.y + player.collider.bounds.size.y - 0.1f), player.transform.position.z)
                                + player.characterLocomotor.velocity * Time.fixedDeltaTime;
                        bool ledgeUndetected = Physics.Raycast(secondaryRayPos, direction, out hitInfo2, m_LedgeDetectionDistance, ~layerToIgnore);

                        if (!ledgeUndetected)
                        {
                            LedgeDetected(parent);
                        }
                    }

                    if (canDo)
                    {
						/*if (hitInfo.distance > m_GravityRayMaxDistance)
							return;*/

                        if (hitInfo.collider.gameObject.tag == "Mushroom")
                            m_UseLinearCalc = false;

                        surfaceAngle = Vector3.Angle(hitInfo.normal, Vector3.up);

						if (surfaceAngle <= m_LinearCalcAngleLimit.x && surfaceAngle >= m_LinearCalcAngleLimit.y) 
						{
							return;
						}
                    }

                    float angleCalc;

                    if (m_UseExponentialAngleCalc)
                        angleCalc = (surfaceAngle * surfaceAngle);
                    else
                        angleCalc = surfaceAngle;

                    if (surfaceAngle >= m_LinearCalcAngleLimit.x && surfaceAngle <= m_LinearCalcAngleLimit.y)
                    {
                        Vector3 normalFaceAngle = new Vector3(hitInfo.normal.x, 0f, hitInfo.normal.z).normalized;
                        float normalAngle = Vector3.Angle(normalFaceAngle, parent.transform.forward);

                        if (normalAngle <= 90f) //If character is facing same direction as the face it is standing on
                        {
                            player.characterLocomotor.gravityScaleY = (player.characterLocomotor.originalGravityScale.y * angleCalc * m_LinearCalcInterpolationMultiplier);   
                        }
                        else
                        {
                            player.characterLocomotor.gravityScaleY = player.characterLocomotor.originalGravityScale.y;
                            Vector3 inverseNormalForce = hitInfo.normal * m_InverseNormalForceMultiplier * -1f; //Create a force that is the inverse of the normal it is standing on.
                            player.characterLocomotor.velocity += inverseNormalForce;
                        }
                    }
                    else
                        player.characterLocomotor.gravityScaleY = player.characterLocomotor.originalGravityScale.y;
                }

                if (!m_UseLinearCalc)
                {
                    player.characterLocomotor.gravityScaleY = player.characterLocomotor.originalGravityScale.y;
                }
            }
        }

        #endregion

		#region Virtual Functions/Methods

		/// <summary>
		/// Called whenever a ledge has been detected.
		/// </summary>
		/// <param name="parent">Parent.</param>
        public virtual void LedgeDetected(MonoBehaviour parent) {}

		#endregion

		#region Internal/Protected Functions/Methods

		/// <summary>
		/// Sets the gravity scale back to it's original value.
		/// </summary>
		/// <param name="player">Player.</param>
		/// <param name="orggravscale">Original gravity scale.</param>
		internal void setGravityScaleBack(AbstractPlayerController player, float orggravscale)
		{
			if (!m_InterpolateGravityScaleY)
				return;

			player.characterLocomotor.gravityScaleY = orggravscale;
		}

		/// <summary>
		/// Should the dash be cancelled?
		/// </summary>
		/// <returns><c>true</c>, if dash should be cancelled, <c>false</c> otherwise.</returns>
		/// <param name="parent">Parent.</param>
		/// <param name="player">Player.</param>
        protected bool shouldDashCancel(MonoBehaviour parent, AbstractPlayerController player)
        {
            if (m_CameraOriented) //If the dash is camera-oriented
            {
                Vector3 input = GetCameraOrientedInput(parent);

                Vector3 perp = Vector3.Cross(player.characterLocomotor.transform.forward, input);
                float dir = HydraMathUtils.Abs(Vector3.Dot(perp, Vector3.up));

				//If the player doesn't use any movement input
                if (!AbstractPlayerController.HasUserInputRaw() || dir > m_DashCancelAngle)
                {
                    return true;
                }
            }
            else //If the dash is input-oriented
            {
                if (InputMapping.verticalInput.GetAxisRaw() <= 0) 
                    return true;
            }

            return false;
        }

		#endregion
    }
}