﻿using UnityEngine;

namespace AntonCoolpecker.Abstract.Player.States.Hitstun
{
	/// <summary>
	/// Abstract cancelable hitstun state - A hitstun state that the player can skip/cancel by pressing the jump button.
	/// </summary>
	public class AbstractCancelableHitstunState : AbstractHitstunState
	{
		#region Override Methods

		public override AbstractPlayerState GetNextState(MonoBehaviour parent)
		{
			if (AntonCoolpecker.Concrete.Configuration.Controls.Mapping.InputMapping.jumpInput.GetButtonDown())
				return timerState;

			return base.GetNextState(parent);
		}

		#endregion
	}
}
