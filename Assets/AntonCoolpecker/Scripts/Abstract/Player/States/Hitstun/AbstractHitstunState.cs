﻿using AntonCoolpecker.Concrete.Utils;
using UnityEngine;
using AntonCoolpecker.Utils;
using AntonCoolpecker.Utils.Time;

namespace AntonCoolpecker.Abstract.Player.States.Hitstun
{
	/// <summary>
	/// Abstract hitstun state - Base class for all player hitstun states.
	/// </summary>
	public abstract class AbstractHitstunState : AbstractPlayerState
	{
		#region Variables

		private Vector3 m_Source;

		[Tweakable("Hitstun")] [SerializeField] internal bool m_NoFrictionWhileMidair = true;

		[Tweakable("Hitstun")] [SerializeField] internal float m_HitstunForceVertical = 8.0f; //Vertical hitstun force when damaged/hit
		[Tweakable("Hitstun")] [SerializeField] internal float m_HitstunForceHorizontal = 8.0f; //Horizontal hitstun force when damaged/hit

		[Tweakable("Hitstun")] [SerializeField] internal bool m_AdjustableHitstunMovement = true; //Should the player be able to (somewhat) steer the resulting velocity of the hitstun?
		[Tweakable("Hitstun")] [SerializeField] internal float m_AdjustableHitStunVelAddToDiv = 1f; //Multiplier for the adjustable hitstun
		[Tweakable("Hitstun")] [SerializeField] internal float m_AdjustableHitStunVelMul = 0.01f; //Multiplier for the adjustable hitstun

		//[Tweakable("Hitstun")] [SerializeField] internal float m_MaxSpeedMultiplier = 1f;
		//[Tweakable("Hitstun")] [SerializeField] internal float m_AccelerationMultiplier = 1f;

		//[Tweakable("Hitstun")] [SerializeField] internal float m_MaxSpeedInitialMultiplier = 1f;
		//[Tweakable("Hitstun")] [SerializeField] internal float m_AccelerationInitialMultiplier = 1f;
		//[Tweakable("Hitstun")] [SerializeField] internal float m_MaxSpeedDesiredMultiplier = 1f;
		//[Tweakable("Hitstun")] [SerializeField] internal float m_AccelerationDesiredMultiplier = 1f;

		/*[Tweakable("Hitstun")] [SerializeField] internal bool m_EndStunOnLowVelocity = true; //Should the hitstun end when the player's velocity is low enough?
		[Tweakable("Hitstun")] [SerializeField] internal float m_MinimumEndStunVelocityXZ = 2f; //Minimum horizontal velocity value for prematurely ending hitstun
		[Tweakable("Hitstun")] [SerializeField] internal float m_MinimumEndStunVelocityY = 2f; //Minimum vertical velocity value for prematurely ending hitstun*/

		[Tweakable("Hitstun")] [SerializeField] internal bool m_EndStunOnLowVelMagnitude = true; //Should the hitstun end when the player's velocity magnitude is low enough?
		[Tweakable("Hitstun")] [SerializeField] internal float m_EndStunOnLowVelMagnitudeMinimum = 1f;

		//[Tweakable("Hitstun")] [SerializeField] internal bool m_DisableControlsTemporarily = true; //Should the controls be temporarily disabled?
		//[Tweakable("Hitstun")] [SerializeField] internal float m_DisabledControlsMaxTime = 0.1f; //Max time for disabling controls

		[Tweakable("Hitstun")] [SerializeField] internal bool m_DisableControlsTemporarily = true; //Should the controls be temporarily disabled?
		[Tweakable("Hitstun")] [SerializeField] internal float m_DisabledControlsMaxTime = 0.1f; //Max time for disabling controls

		[Tweakable("Hitstun")] [SerializeField] internal bool m_RotateTowardsHitstunSource = false; //Should the player rotate towards the source of the hitstun

		[Tweakable("Hitstun")] [SerializeField] internal float m_MaxHitstunTime = 3f;

		[Tweakable("Hitstun")] [SerializeField] private Timer m_StateTimer;
		[Tweakable("Hitstun")] [SerializeField] private AbstractPlayerState m_TimerState;

		#endregion

		#region Properties

		/// <summary>
		/// 	The origin point of the damage, in world coordinates.
		/// </summary>
		/// <value>The damage source coordinates.</value>
		public Vector3 source { get { return m_Source; } set { m_Source = value; } }

		/// <summary>
		/// 	Gets the vertical force of the hitstun.
		/// </summary>
		/// <value>The force of the hitstun.</value>
		public float hitstunForceVertical { get { return m_HitstunForceVertical; } }

		/// <summary>
		/// 	Gets the horizontal force of the hitstun.
		/// </summary>
		/// <value>The force of the hitstun.</value>
		public float hitstunForceHorizontal { get { return m_HitstunForceHorizontal; } }

		/// <summary>
		///     Gets the max speed multiplier.
		/// </summary>
		/// <value>The max speed multiplier.</value>
		//protected override float maxSpeedMultiplier { get { return m_MaxSpeedMultiplier; } }

		/// <summary>
		///     Gets the acceleration multiplier.
		/// </summary>
		/// <value>The acceleration multiplier.</value>
		//protected override float accelerationMultiplier { get { return m_AccelerationMultiplier; } }

		/// <summary>
		/// 	Gets the state to transition to after the timer elapses.
		/// </summary>
		/// <value>The state of the timer.</value>
		protected override AbstractPlayerState timerState { get { return m_TimerState; } }

		#endregion

		#region Override Methods

		/// <summary>
		///     Gets the timer to transition to another state.
		/// </summary>
		/// <value>The state timer.</value>
		public override Timer GetStateTimer(MonoBehaviour parent) { return m_StateTimer; }

		/// <summary>
		///     Called when the state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnEnter(MonoBehaviour parent)
		{
			base.OnEnter (parent);

			m_StateTimer.maxTime = m_MaxHitstunTime;

			//if (m_DisableControlsTemporarily)
			//	disabledControls = true;

			//m_MaxSpeedMultiplier = m_MaxSpeedInitialMultiplier;
			//m_AccelerationMultiplier = m_AccelerationInitialMultiplier;
		}

		/// <summary>
		/// Called when the parent updates.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnUpdate(MonoBehaviour parent)
		{
			AbstractPlayerController player = GetPlayerController (parent);

			if (m_NoFrictionWhileMidair) 
			{
				if (!player.characterLocomotor.isGrounded)
					player.characterLocomotor.useFriction = false;
				else
					player.characterLocomotor.useFriction = true;
			}

			/*if (m_EndStunOnLowVelocity)
			{
				if (Mathf.Abs(player.characterLocomotor.velocity.x) < m_MinimumEndStunVelocityXZ &&
					player.characterLocomotor.velocity.y < m_MinimumEndStunVelocityY &&
					Mathf.Abs(player.characterLocomotor.velocity.z) < m_MinimumEndStunVelocityXZ)
					m_StateTimer.End ();
			}*/

			if (m_EndStunOnLowVelMagnitude) 
			{
				if (player.characterLocomotor.velocity.magnitude < m_EndStunOnLowVelMagnitudeMinimum)
					m_StateTimer.End ();
			}

			//m_AccelerationMultiplier = Mathf.Lerp (m_AccelerationInitialMultiplier, m_AccelerationDesiredMultiplier, 1f * (m_StateTimer.elapsed * m_StateTimer.elapsed));
			//m_MaxSpeedMultiplier = Mathf.Lerp (m_MaxSpeedInitialMultiplier, m_MaxSpeedDesiredMultiplier, 1f * (m_StateTimer.elapsed));

			//if (m_DisableControlsTemporarily && m_StateTimer.elapsed > m_DisabledControlsMaxTime)
			//	disabledControls = false;

			base.OnUpdate(parent);
		}

		/// <summary>
		/// 	Handles the movement.
		/// </summary>
		protected override void HandleMovement(MonoBehaviour parent)
		{
			AbstractPlayerController player = GetPlayerController(parent);

			// ROTATE PLAYER TOWARDS SOURCE OF DAMAGE

			if (m_RotateTowardsHitstunSource) 
			{
				Vector3 directionTowardsSource = (source - player.transform.position).normalized;
				Quaternion rotationTowardsSource = Quaternion.LookRotation (directionTowardsSource);

				rotationTowardsSource = Quaternion.Euler (0, rotationTowardsSource.eulerAngles.y, 0);

				player.transform.rotation = Quaternion.Slerp (player.transform.rotation,
					rotationTowardsSource,
					player.rotateSpeed * rotateSpeedMultiplier * GameTime.deltaTime);
			}

			// HANDLE HITSTUN MOVEMENT

			if (m_DisableControlsTemporarily && m_StateTimer.elapsed < m_DisabledControlsMaxTime)
				return;

			if (m_AdjustableHitstunMovement)
			{
				Vector3 desiredMovement = GetMovementVector(parent);

				desiredMovement.Scale (new Vector3 (((m_AdjustableHitStunVelMul * player.characterLocomotor.flatVelocity.magnitude) / (m_StateTimer.elapsed + m_AdjustableHitStunVelAddToDiv)),
					1f,
					((m_AdjustableHitStunVelMul * player.characterLocomotor.flatVelocity.magnitude) / (m_StateTimer.elapsed + m_AdjustableHitStunVelAddToDiv))));

				player.characterLocomotor.velocity += desiredMovement;
			}
		}

		#endregion
	}
}