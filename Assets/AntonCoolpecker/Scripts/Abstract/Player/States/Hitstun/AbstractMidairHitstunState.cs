﻿using UnityEngine;

namespace AntonCoolpecker.Abstract.Player.States.Hitstun
{
	/// <summary>
	/// Abstract midair hitstun state - Base class for states where the player gets hitstunned while midair.
	/// </summary>
	public class AbstractMidairHitstunState : MonoBehaviour {}
}