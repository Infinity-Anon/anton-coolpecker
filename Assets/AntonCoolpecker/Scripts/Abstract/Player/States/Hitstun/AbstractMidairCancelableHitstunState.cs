﻿namespace AntonCoolpecker.Abstract.Player.States.Hitstun
{
	/// <summary>
	/// Abstract midair cancelable hitstun state - A midair hitstun state that the player can skip/cancel.
	/// </summary>
	public abstract class AbstractMidairCancelableHitstunState : AbstractPlayerState {}
}
