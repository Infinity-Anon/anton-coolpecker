﻿using AntonCoolpecker.Utils;
using AntonCoolpecker.Utils.Time;
using UnityEngine;

namespace AntonCoolpecker.Abstract.Player.States
{
    /// <summary>
	/// 	DashChargingState is used while Coolpecker is charging up his dash. 
	/// </summary>
    public abstract class AbstractDashChargingState : AbstractPlayerState
    {
        [SerializeField] [Tweakable("DashCharge")] protected float m_MaxChargeTime = 3.0f;
		[SerializeField] [Tweakable("DashCharge")] protected float m_RotateSpeedMultiplier = 0.5f;

        protected float m_CurrentChargeTime = 0f;
        protected bool m_ChargeFull = false; //Is the dash fully charged?

        private Quaternion m_RotateMomentum = Quaternion.identity;

        #region Override Methods

		/// <summary>
		/// Called when the parent updates.
		/// </summary>
		/// <param name="parent">Parent.</param>
        public override void OnUpdate(MonoBehaviour parent)
        {
            base.OnUpdate(parent);

            m_CurrentChargeTime += Time.deltaTime;

            if (m_CurrentChargeTime >= m_MaxChargeTime)
            {
                m_CurrentChargeTime = m_MaxChargeTime;
				m_ChargeFull = true;
            }
        }

        #endregion
    }
}