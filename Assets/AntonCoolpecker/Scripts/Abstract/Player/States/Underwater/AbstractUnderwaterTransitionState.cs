﻿using AntonCoolpecker.Concrete.Cameras;
using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using AntonCoolpecker.Concrete.Utils;
using AntonCoolpecker.Utils;
using Hydra.HydraCommon.Utils;
using UnityEngine;

namespace AntonCoolpecker.Abstract.Player.States.Underwater
{
	/// <summary>
	/// Abstract underwater transition state - When the player transtions from swimming state to underwater state or vice-versa.
	/// </summary>
	public abstract class AbstractUnderwaterTransitionState : AbstractPlayerState
	{
		#region Variables

		[SerializeField] private AbstractUnderwaterState m_UnderwaterState;
		[SerializeField] private AbstractMidairState m_MidairState;

		//Detects whether the player dives into or out of the water
		private bool m_OutroOnIntroOff = true; //OBSERVE: MUST ALWAYS BE SET TO TRUE UNLESS THE PLAYER IS SPAWNED IN WATER

		//Detects whether the player is using groundpound/digging move when player dives into water
		private bool m_GroundPoundAirDig = false;

		// How far the player will rotate down whilst swimming downwards(Intro)
		[Tweakable("UnderwaterTransition")] [SerializeField] internal float m_AngleRotationDown;

		// Rotation cube for proper downwards rotation
		[SerializeField] private Transform m_UnderwaterRotationCube;

		// The player will automatically go into desired state when timer is up
		[SerializeField] private Timer m_StateTimer;
		private AbstractPlayerState m_TimerState;

		// The rotation the player
		private Quaternion m_TowardsRotation;

		// Reference to WaterSurface object
		private GameObject m_WaterSurface;

		// Calculation for diving down depending on distance to water surface
		private float m_DownforcePositionY;

		// Which y-coordinate the player should move towards while transitioning to underwater swimming
		[Tweakable("UnderwaterTransition")] [SerializeField] internal float m_yLerpPosition = 3.5f;

		// The vertical distance of the ray check
		[Tweakable("UnderwaterTransition")] [SerializeField] internal float m_RayPosModY = 0f;
		// The horizontal distance of the ray check
		[Tweakable("UnderwaterTransition")] [SerializeField] internal float m_RayPosModZX = 0.5f;

		#endregion

		#region Properties

		/// <summary>
		/// 	Gets the state to transition to after the timer elapses.
		/// </summary>
		/// <value>The state of the timer.</value>
		protected override AbstractPlayerState timerState { get { return m_TimerState; } }

		/*[Tweakable("UnderwaterTransition")] public float rayPosModZX { get { return m_RayPosModZX; } set { m_RayPosModZX = value; } }
		[Tweakable("UnderwaterTransition")] public float rayPosModY { get { return m_RayPosModY; } set { m_RayPosModY = value; } }
		[Tweakable("UnderwaterTransition")] public float yLerpPosition { get { return m_yLerpPosition; } set { m_yLerpPosition = value; } }
		[Tweakable("UnderwaterTransition")] public float angleRotationDown { get { return m_AngleRotationDown; } set { m_AngleRotationDown = value; } }*/

		#endregion

		#region Override Methods

		/// <summary>
		/// 	Called when the state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnEnter(MonoBehaviour parent)
		{
			AbstractPlayerController player = GetPlayerController(parent);

			player.airTracker.Reset();

			if (player.waterTrigger != null)
			m_WaterSurface = player.waterTrigger.gameObject.transform.GetChild(0).GetChild(0).gameObject;

			if (!m_OutroOnIntroOff)
			{
				player.characterLocomotor.useGravity = false; //ELSE PLAYER WILL FLOAT DOWNWARDS IN WATER WHILST IN TRANSITION

				if (m_GroundPoundAirDig)
				{
					player.characterLocomotor.velocity = Vector3.zero;
				}

				if (!m_GroundPoundAirDig)
				{
					m_TowardsRotation = player.transform.rotation * m_UnderwaterRotationCube.transform.rotation;
				}

				m_TimerState = m_UnderwaterState;
			}

			if (m_OutroOnIntroOff)
			{
				m_TimerState = m_MidairState;
			}

			/*if (player.waterTrigger != null)
			{
				if (player.waterTrigger.interpolateWaterVelocity) 
				{
					player.waterTrigger.interpolatePlayerUpwards = false;
				}
			}*/

			base.OnEnter(parent);
		}

		/// <summary>
		/// Called before another state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnExit(MonoBehaviour parent)
		{
			AbstractPlayerController player = GetPlayerController(parent);

			if (m_OutroOnIntroOff) 
			{
				player.waterTrigger.checkForOtherWaters = true;
			}

			if (!player.characterLocomotor.useGravity && m_OutroOnIntroOff) 
			{
				player.characterLocomotor.useGravity = true;
			}

			if (!m_OutroOnIntroOff)
			{
				if (m_GroundPoundAirDig)
				m_GroundPoundAirDig = false;

				player.waterTrigger.interpolatePlayerUpwards = false;
				player.waterTrigger.checkForOtherWaters = true;
			}

			base.OnExit(parent);
		}
		
		/// <summary>
		/// 	Returns a state for transition. Return self if no transition.
		/// </summary>
		/// <returns>The next state.</returns>
		/// <param name="parent">Parent.</param>
		public override AbstractPlayerState GetNextState(MonoBehaviour parent)
		{
			AbstractPlayerController player = GetPlayerController(parent);

			if (!m_GroundPoundAirDig) //Player dives into water from swimming state
			{
				if (!m_OutroOnIntroOff) //Transitioning into underwater
				{
					if (WillGoBelowSurface(player))
						return m_UnderwaterState;
				}

				if (m_OutroOnIntroOff) //Transtioning away from underwater
				{
					if (WillReturnToSurface(player))
						return m_MidairState;
				}
			}

			if (m_GroundPoundAirDig) //Player dives into water from airdig/groundpound state
			{
				if (FastDownforce(player))
					return m_UnderwaterState;
			}

			return base.GetNextState(parent);
		}

		/// <summary>
		/// 	Called when the parent updates.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnUpdate(MonoBehaviour parent)
		{
			base.OnUpdate(parent);
		}

        /// <summary>
        ///     Gets the timer to transition to another state.
        /// </summary>
        /// <value>The state timer.</value>
		public override Timer GetStateTimer(MonoBehaviour parent)
		{
            return m_StateTimer;
		}

		#endregion

		#region Functions/Methods

		/// <summary>
		/// Has the player returned to the water surface?
		/// </summary>
		/// <returns><c>true</c>, if player is not underwater, <c>false</c> otherwise.</returns>
		/// <param name="player">Player.</param>
		private bool WillReturnToSurface(AbstractPlayerController player)
		{
			Quaternion intendedRotation = player.transform.rotation * m_UnderwaterRotationCube.transform.rotation;

			player.transform.localRotation = Quaternion.Slerp(player.transform.localRotation,
				Quaternion.Euler(0, intendedRotation.eulerAngles.y, 0),
				12f * Time.deltaTime);

			if (!player.playerCamera.IsWithinCameraClamp())
				return false;

			if (!AbstractPlayerController.HasUserInput())
				return false;

			player.transform.localRotation = Quaternion.Euler (0, intendedRotation.eulerAngles.y, 0);
			return true;
		}

		/// <summary>
		/// Has the player gone below the water surface?
		/// </summary>
		/// <returns><c>true</c>, if player is underwater, <c>false</c> otherwise.</returns>
		/// <param name="player">Player.</param>
		private bool WillGoBelowSurface(AbstractPlayerController player)
		{
			player.transform.localRotation = Quaternion.Slerp(player.transform.localRotation,
				Quaternion.Euler(m_TowardsRotation.eulerAngles.x + m_AngleRotationDown, m_TowardsRotation.eulerAngles.y, m_TowardsRotation.eulerAngles.z),
				1f * Time.deltaTime);

			Vector3 waterPos = new Vector3 (player.transform.position.x, (m_WaterSurface.transform.position.y - m_yLerpPosition), player.transform.position.z);
			player.transform.position = Vector3.Slerp (player.transform.position, waterPos, 2f * Time.deltaTime);

			if (player.transform.localRotation.eulerAngles.x > (m_TowardsRotation.eulerAngles.x - 1))
			{
				if (player.transform.position.y < m_WaterSurface.transform.position.y - (m_yLerpPosition - (m_yLerpPosition * 0.15)))
					return true;
			}

			return false;
		}

		/// <summary>
		/// Is the player underwater yet from the previous groundpound/airdig?
		/// </summary>
		/// <returns><c>true</c>, is the player is underwater, <c>false</c> otherwise.</returns>
		/// <param name="player">Player.</param>
		private bool FastDownforce(AbstractPlayerController player)
		{
			float distance = HydraMathUtils.Abs((m_WaterSurface.transform.position.y - m_DownforcePositionY));
			//Debug.Log (distance);

			float waterPosY = m_WaterSurface.transform.position.y - (m_yLerpPosition + (distance * 0.2f));
			Vector3 waterPos = new Vector3 (player.transform.position.x, waterPosY, player.transform.position.z);
			player.transform.position = Vector3.Slerp (player.transform.position, waterPos, 2f * Time.deltaTime);
			//Debug.Log (waterPosY);

			if (player.transform.position.y < (m_WaterSurface.transform.position.y - ((m_yLerpPosition - (m_yLerpPosition * 0.2f)) + (distance * 0.15f))))
				return true;

			return false;
		}

		/// <summary>
		/// Checks if the player can dive into the water.
		/// </summary>
		/// <returns><c>true</c>, if player can dive, <c>false</c> otherwise.</returns>
		/// <param name="player">Player.</param>
		public bool DownUnder(AbstractPlayerController player)
		{
			LayerMask layerToIgnore = 1 << 4; //IGNORE WATER LAYER
			layerToIgnore |= 1 << 12; //IGNORE PLAYER LAYER

			Vector3[] rayosas = new Vector3[4];

			Vector3 direction = Vector3.down;

			rayosas[0] = new Vector3(player.transform.position.x, player.transform.position.y + m_RayPosModY,
				(player.transform.position.z + m_RayPosModZX));
			rayosas[1] = new Vector3(player.transform.position.x, player.transform.position.y + m_RayPosModY,
				(player.transform.position.z - m_RayPosModZX));
			rayosas[2] = new Vector3((player.transform.position.x + m_RayPosModZX), player.transform.position.y + m_RayPosModY,
				player.transform.position.z);
			rayosas[3] = new Vector3((player.transform.position.x - m_RayPosModZX), player.transform.position.y + m_RayPosModY,
				player.transform.position.z);

			for (int index = 0; index < rayosas.Length; index++)
			{
				RaycastHit hitInfo;

				if (Physics.Raycast (rayosas [index], direction, out hitInfo, layerToIgnore)) 
				{
					//Debug.Log (hitInfo.distance);
					if (hitInfo.distance < (m_yLerpPosition * 0.4))
						return false;
				}
			}

			return true;
		}

		/// <summary>
		/// Called when the player swims into the water.
		/// </summary>
		public void SwimIn() //Player swims in
		{
			if (m_OutroOnIntroOff)
				m_OutroOnIntroOff = false;
		}

		/// <summary>
		/// Called when the player swims out of the water.
		/// </summary>
		public void SwimOut()
		{
			if (!m_OutroOnIntroOff)
				m_OutroOnIntroOff = true;
		}

		/// <summary>
		/// Called when the player airdigs/groundpounds into the water.
		/// </summary>
		public void GroundPoundAirDig()
		{
			if (!m_GroundPoundAirDig)
				m_GroundPoundAirDig = true;
		}

		/// <summary>
		/// Called when the player airdigs/groundpounds - Saves the current position
		/// in case the player airdigs/groundpounds into water.
		/// </summary>
		/// <param name="currentPositionY">Current position y.</param>
		public void CurrentDownforcePosition(float currentPositionY)
		{
			m_DownforcePositionY = currentPositionY;
		}

		#endregion
	}
}