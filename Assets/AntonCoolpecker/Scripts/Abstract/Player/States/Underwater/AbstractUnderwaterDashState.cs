﻿using UnityEngine;

namespace AntonCoolpecker.Abstract.Player.States.Underwater
{
	/// <summary>
	/// Abstract underwater dash state - When the player dashes underwater.
	/// </summary>
	public abstract class AbstractUnderwaterDashState : AbstractUnderwaterBaseState
    {
		#region Override Functions

		/// <summary>
		/// Called when the state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
        public override void OnEnter(MonoBehaviour parent)
        {
            base.OnEnter(parent);
        }

		#endregion
    }
}