using AntonCoolpecker.Concrete.Cameras;
using AntonCoolpecker.Concrete.Cameras.States;
using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using AntonCoolpecker.Concrete.Forces;
using AntonCoolpecker.Utils;
using AntonCoolpecker.Utils.Time;
using Hydra.HydraCommon.Utils;
using UnityEngine;

namespace AntonCoolpecker.Abstract.Player.States.Underwater
{
    /// <summary>
    /// 	AbstractUnderwaterBaseState is used for universial rules regarding underwater swimming.
    /// </summary>
    public abstract class AbstractUnderwaterBaseState : AbstractPlayerState
    {
        #region Override Methods

        /// <summary>
        /// 	Called when the state becomes active.
        /// </summary>
        /// <param name="parent">Parent.</param>
        public override void OnEnter(MonoBehaviour parent)
        {
            AbstractPlayerController player = GetPlayerController(parent);

            player.airTracker.Reset();
            player.characterLocomotor.useGravity = false;

            base.OnEnter(parent);
        }

        #endregion
    }
}