﻿using UnityEngine;

namespace AntonCoolpecker.Abstract.Player.States.Underwater
{
	/// <summary>
	/// Abstract underwater attack state - When the player attacks underwater.
	/// </summary>
	public abstract class AbstractUnderwaterAttackState : AbstractPlayerState
	{
		#region Variables

		[SerializeField] private Collider AttackCollider; //Collider used when attacking underwater

		#endregion

		#region Override Methods

		/// <summary>
		/// 	Called when the state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnEnter(MonoBehaviour parent)
		{
			AbstractPlayerController player = GetPlayerController(parent);

			player.airTracker.Reset();
			player.characterLocomotor.useGravity = false;

			base.OnEnter(parent);
		}

		#endregion
	}
}