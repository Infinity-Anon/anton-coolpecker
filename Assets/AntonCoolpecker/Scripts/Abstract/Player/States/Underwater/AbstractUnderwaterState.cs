using AntonCoolpecker.Concrete.Cameras;
using AntonCoolpecker.Concrete.Cameras.States;
using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using AntonCoolpecker.Concrete.Forces;
using AntonCoolpecker.Utils;
using AntonCoolpecker.Utils.Time;
using Hydra.HydraCommon.Utils;
using UnityEngine;

namespace AntonCoolpecker.Abstract.Player.States.Underwater
{
	/// <summary>
	/// 	AbstractUnderwaterState is used for universial rules regarding underwater swimming
	/// </summary>
	public abstract class AbstractUnderwaterState : AbstractPlayerState
	{
		//TODO: FIX SO THAT THE PLAYER WILL ONLY ROTATE TO CAMERA IF USING VELOCITY(CAMERA-BASED UNDERWATER CAMERA)
		//TODO: FIX SO PLAYER CAN MOVE CAMERA FREELY(MOVEMENT-BASED UNDERWATER CAMERA)

		#region Variables

		// Bubble particles - Reference, instantiated var and array for changing the particles dynamically
		[SerializeField] private ParticleSystem m_BubbleParticles;
		private ParticleSystem m_InstantiatedBP;
		private ParticleSystem.Particle[] m_BubbleParticleArray = new ParticleSystem.Particle[100];

		// Height of instantiated player collider
		[Tweakable("Underwater")] [SerializeField] internal float m_ColliderHeight = 2f;

		// Subtracted value of when the player should float upwards(Before the water surface is reached)
		[Tweakable("Underwater")] [SerializeField] internal float m_FloatUpwardsYCoord = 2f;

		[SerializeField] private AbstractSwimmingState m_SwimmingState;
		[SerializeField] private AbstractUnderwaterTransitionState m_UnderwaterTransitionState;
		[SerializeField] private AbstractMidairState m_MidairState;

		[Tweakable("Underwater")] [SerializeField] internal float m_MaxSpeedMultiplier = 0.5f;
		[Tweakable("Underwater")] [SerializeField] internal float m_AccelerationMultiplier = 0.5f;
		[Tweakable("Underwater")] [SerializeField] internal float m_JumpHeightMultiplier = 1f;
		[Tweakable("Underwater")] [SerializeField] internal float m_RotateSpeedMultiplier = 1f;

		// OBSERVE: VALUES FOR FROG SWIM ARE HANDLED FROM ABSTRACT SWIMMING ASSET!
		[Tweakable("Underwater")] [SerializeField] internal bool m_AllowFrogSwim = true;

		// Surface of the water
		private GameObject m_WaterSurface;

		// Rigidbody for player(Neccessary for proper collision detection with instantiated collider
		private Rigidbody m_Rigidy;

		// Collider for player(Neccessary for proper underwater collision detection)
		private CapsuleCollider m_Collidy;

		#endregion

		#region Properties

		/// <summary>
		/// 	Gets the max speed multiplier.
		/// </summary>
		/// <value>The max speed multiplier.</value>
		protected override float maxSpeedMultiplier { get { return m_MaxSpeedMultiplier; } }
		public float undwatMaxSpeedMultiplier { get { return m_MaxSpeedMultiplier; } set { m_MaxSpeedMultiplier = value; } }

		/// <summary>
		/// 	Gets the acceleration multiplier.
		/// </summary>
		/// <value>The acceleration multiplier.</value>
		protected override float accelerationMultiplier { get { return m_AccelerationMultiplier; } }
		public float undwatAccelerationMultiplier { get { return m_AccelerationMultiplier; } set { m_AccelerationMultiplier = value; } }

		/// <summary>
		/// 	Gets the jump force multiplier.
		/// </summary>
		/// <value>The jump force multiplier.</value>
		protected override float jumpHeightMultiplier { get { return m_JumpHeightMultiplier; } }
		public float undwatJumpHeightMultiplier { get { return m_JumpHeightMultiplier; } set { m_JumpHeightMultiplier = value; } }

		/// <summary>
		/// 	Gets the rotate speed multiplier.
		/// </summary>
		/// <value>The rotate speed multiplier.</value>
		protected override float rotateSpeedMultiplier { get { return m_RotateSpeedMultiplier; } }
		public float undwatRotateSpeedMultiplier { get { return m_RotateSpeedMultiplier; } set { m_RotateSpeedMultiplier = value; } }

		/*[Tweakable("Underwater")] public float colliderHeight { get { return m_ColliderHeight; } set { m_ColliderHeight = value; } }
		[Tweakable("Underwater")] public float floatUpwardsYCoord { get { return m_FloatUpwardsYCoord; } set { m_FloatUpwardsYCoord = value; } }
		[Tweakable("Underwater")] public bool allowFrogSwim { get { return m_AllowFrogSwim; } set { m_AllowFrogSwim = value; } }*/

		#endregion

		#region Override Functions/Methods

		/// <summary>
		/// 	Called when the object is about to be destroyed.
		/// </summary>
		protected override void OnDestroy()
		{
			base.OnDestroy();

			DestroyBubbleParticles(0.0f);
		}

		/// <summary>
		/// Called when the state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnEnter(MonoBehaviour parent)
		{
			AbstractPlayerController player = GetPlayerController(parent);

			UnderwaterCameraState undwatCameraState =
				player.playerCamera.stateMachine.GetStateByMode(CameraController.Mode.Underwater) as UnderwaterCameraState;
			undwatCameraState.StartInitialRotation ();

			///*Instantiate the bubble particles
			m_InstantiatedBP =
				Instantiate(m_BubbleParticles, (new Vector3(player.transform.position.x, player.transform.position.y + 1f, player.transform.position.z)),
					player.transform.rotation) as ParticleSystem;

			///*Rotate the buble particles to their proper rotation
			Quaternion q = m_InstantiatedBP.transform.rotation;
			q.eulerAngles = new Vector3 (270, m_InstantiatedBP.transform.rotation.eulerAngles.y, m_InstantiatedBP.transform.rotation.eulerAngles.z);
			m_InstantiatedBP.transform.rotation = q;
			
			///*Disable camera limitations and predication
			player.playerCamera.clampEnabled = false;
			player.playerCamera.predictionOffset = Vector3.zero;

			///*Disable gravity and CharacterController component
			player.characterLocomotor.useGravity = false;
			player.characterLocomotor.characterController.enabled = false;
			//player.characterLocomotor.enabled = false;

			///*Add custom rigidbody and capsule collider to player
			m_Rigidy = player.gameObject.AddComponent<Rigidbody>();
			m_Collidy = player.gameObject.AddComponent<CapsuleCollider>();

			///*Make sure the rigidbody stays put and is affected by nothing but collisions
			m_Rigidy.angularDrag = 0f;
			m_Rigidy.useGravity = false;
			m_Rigidy.freezeRotation = true;
			m_Rigidy.constraints = RigidbodyConstraints.FreezeRotation;
			//m_Rigidy.constraints = RigidbodyConstraints.FreezeAll;

			///*Fix collider settings
			m_Collidy.height = m_ColliderHeight;
			m_Collidy.center = new Vector3(0, 1, 0);
			m_Collidy.radius = 0.85f;

			m_WaterSurface = player.waterTrigger.gameObject.transform.GetChild(0).GetChild(0).gameObject;

			base.OnEnter(parent);
		}

		/// <summary>
		/// Called before another state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnExit(MonoBehaviour parent)
		{
			base.OnExit(parent);

			DestroyBubbleParticles(0.5f);
		}

		/// <summary>
		/// Called when the parent updates.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnUpdate(MonoBehaviour parent)
		{
			base.OnUpdate(parent);

			AbstractPlayerController player = GetPlayerController(parent);

			//Debug.Log (player.transform.position.y < (m_WaterSurface.transform.position.y - 1.5f));
			if (player.waterTrigger == null || m_WaterSurface == null) 
				return;

			UnderwaterCameraState undwatCameraState =
				player.playerCamera.stateMachine.GetStateByMode(CameraController.Mode.Underwater) as UnderwaterCameraState;

			if (m_AllowFrogSwim) 
			{
				if (!undwatCameraState.undWatInputFalseCameraTrue)
					m_SwimmingState.FrogOrRegularSwim (parent, InputMapping.jumpInput.GetButton());

				else
					m_SwimmingState.FrogOrRegularSwim (parent, AbstractPlayerController.HasUserInputRaw ());
				
				m_MaxSpeedMultiplier = m_SwimmingState.swimMaxSpeedMultiplier;
				m_AccelerationMultiplier = m_SwimmingState.swimAccelerationMultiplier;
			}

			// Notes for refactor: flags like "should the player be bobbing" should be controlled by the state machine
			// Storing it in WaterTrigger will result in getting out of sync unexpectedly
			if (player.transform.position.y < (m_WaterSurface.transform.position.y - m_FloatUpwardsYCoord)) 
			{
				player.waterTrigger.interpolatePlayerUpwards = false; //Make sure the player won't bob
				//player.waterTrigger.interpolateSwimVelocity = false; //Make sure the vertical position is clamped properly
			} 

			else 
			{
				player.waterTrigger.interpolatePlayerUpwards = true; //Make sure the player can bob again
				//player.waterTrigger.interpolateSwimVelocity = true; //Make sure the vertical position is clamped properly
			}

			if (player.rigidbody.useGravity)
				DisableRigidBodyGravity (player);

			m_InstantiatedBP.transform.position = new Vector3(player.transform.position.x, player.transform.position.y + 1.5f, player.transform.position.z);
		}
			
		/// <summary>
		/// Called when the parent late updates.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnLateUpdate(MonoBehaviour parent)
		{
			base.OnLateUpdate(parent);

			int len = m_InstantiatedBP.GetParticles(m_BubbleParticleArray);

			for (int i = 0; i < len; i++)
			{
				//m_Particles[i].velocity += Vector3.up * m_Drift;
				//m_BubbleParticleArray[i].color = Color.red;
				//Debug.Log(i);

				if (m_BubbleParticleArray[i].position.y > (m_WaterSurface.transform.position.y - 0.5f)) 
				{
					m_BubbleParticleArray [i].lifetime = m_BubbleParticleArray [i].lifetime + 0.1f;
				}
			}

			m_InstantiatedBP.SetParticles(m_BubbleParticleArray, len);
		}
			
		/// <summary>
		/// Called when the parent's physics update.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnFixedUpdate(MonoBehaviour parent)
		{
			base.OnFixedUpdate(parent);
		}

		/// <summary>
		/// 	Returns a state for transition. Return self if no transition.
		/// </summary>
		/// <returns>The next state.</returns>
		/// <param name="parent">Parent.</param>
		public override AbstractPlayerState GetNextState(MonoBehaviour parent)
		{
			AbstractPlayerController player = GetPlayerController(parent);

			if (!player.isInWater || (player.waterTrigger != null && player.waterTrigger.interpolatePlayerUpwards))
			{
				CleanoutUnderwaterComponents(player);
				m_UnderwaterTransitionState.SwimOut();

				return m_UnderwaterTransitionState;
			}

			return base.GetNextState(parent);
		}

		/// <summary>
		/// Resolves the character's rotation due to the movement that just occurred in the current frame.
		/// </summary>
		/// <param name="player">The player character that just moved and needs to rotate.</param>
		/// <param name="movement">The normalised movement vector input for this frame, in world space.</param>
		protected override void ResolveRotation(AbstractPlayerController player, Vector3 movement)
		{
			//DO NOTHING AT ALL
		}

		/// <summary>
		/// 	Transforms the input vector from camera space into world space.
		/// </summary>
		/// <returns>The camera oriented input.</returns>
		protected override Vector3 GetCameraOrientedInput(MonoBehaviour parent)
		{
			AbstractPlayerController player = GetPlayerController(parent);

			Vector3 input = GetInputVector(player);

			input = Vector3.ClampMagnitude(input, 1.0f);

			Vector3 cameraForward = player.playerCamera.transform.forward;

			//AbstractCameraState currentCameraState = player.playerCamera.stateMachine.activeState;
			UnderwaterCameraState undwatCameraState =
				player.playerCamera.stateMachine.GetStateByMode(CameraController.Mode.Underwater) as UnderwaterCameraState;

			//if (player.playerCamera.transform.rotation.eulerAngles.z < 1f || player.playerCamera.transform.rotation.eulerAngles.z > 359f) 
			//return new Vector3(moveHorizontal, moveVertical, 0.0f);

			if (!undwatCameraState.undWatInputFalseCameraTrue)
				input = GetUnderwaterMovementInputVector(player);

			if (Input.GetKey(KeyCode.R))
				input = GetUnderwaterCameraInputVector(player);

			//if (player.playerCamera.transform.rotation.eulerAngles.z < 181f && player.playerCamera.transform.rotation.eulerAngles.z > 179f) 
			//return new Vector3(-moveHorizontal, moveVertical, 0.0f);

			if (player.playerCamera.transform.rotation.eulerAngles.z < 181f &&
				player.playerCamera.transform.rotation.eulerAngles.z > 179f)
			{
				input.x = input.x * -1;

				if (Input.GetKey(KeyCode.R))
					input.y = input.y * -1;
			}

			if (!undwatCameraState.undWatInputFalseCameraTrue)
			{
				//If the underwater rotation is movement-based, make sure movement is based on player rotation.
				return Quaternion.LookRotation(player.transform.forward) * input;
			}

			//Else base the underwater rotation on the player's camera
			return Quaternion.LookRotation(cameraForward) * input;
		}

		/// <summary>
		/// Gets the change in velocity, given the current velocity, desired final velocity, and max acceleration.
		/// </summary>
		/// <param name="player">The player.</param>
		/// <param name="desiredV">The desired velocity, according to the character's max speed and input direction.</param>
		/// <param name="currentV">The character's current velocity.</param>
		/// <returns>The change in velocity for this step.</returns>
		protected override Vector3 ResolveVelocityDelta(AbstractPlayerController player, Vector3 desiredV, Vector3 currentV)
		{
			// We're not moving yet.
			//Debug.Log (currentV.magnitude);
			/*if (HydraMathUtils.Approximately(currentV.magnitude, 0.0f))
			{
				Vector3 inputVector = GetInputVector(player);
				if (player.runThreshold >= inputVector.magnitude)
					return Vector3.zero;
			}*/

			Vector3 deltaV = desiredV - currentV;

			deltaV = Vector3.ClampMagnitude(deltaV, player.acceleration * accelerationMultiplier * GameTime.deltaTime);
			return deltaV;
		}

		#endregion

		#region Virtual Methods

		/// <summary>
		/// Gets the underwater camera input vector.
		/// </summary>
		/// <returns>The underwater camera input vector.</returns>
		/// <param name="player">Player.</param>
		protected virtual Vector3 GetUnderwaterCameraInputVector(AbstractPlayerController player) //Used if rotation by camera input
		{
			//float moveHorizontal = InputMapping.horizontalInput.GetAxis();
			//float moveVertical = InputMapping.verticalInput.GetAxis();
			float moveHorizontal = InputMapping.horizontalInput.GetAxisRaw();
			float moveVertical = InputMapping.verticalInput.GetAxisRaw();
			return new Vector3(moveHorizontal, moveVertical, 0.0f);
		}

		/// <summary>
		/// Gets the underwater movement input vector.
		/// </summary>
		/// <returns>The underwater movement input vector.</returns>
		/// <param name="player">Player.</param>
		protected virtual Vector3 GetUnderwaterMovementInputVector(AbstractPlayerController player) //Used if rotation by movement input
		{
			float moveforback = 0f;
			float moveleftright = 0f;

			//moveforback = Mathf.Clamp(moveforback, 0f, 10f);
			//moveleftright = Mathf.Clamp(moveleftright, 0f, 10f);

			if (InputMapping.jumpInput.GetButton())
				moveforback = 1f;

			//if (!InputMapping.jumpInput.GetButton ()) 
			//{
			//if (Input.GetKey(KeyCode.F))
			//moveforback = -1f;
			//}

			return new Vector3(moveleftright, 0.0f, moveforback);
		}

		#endregion
			
		#region Private Functions/Methods

		/// <summary>
		/// Disables the rigidbody gravity.
		/// </summary>
		/// <param name="player">Player.</param>
		private void DisableRigidBodyGravity(AbstractPlayerController player)
		{
			player.rigidbody.useGravity = false;
		}

		/// <summary>
		/// Destroy the underwater components and reset player settings.
		/// </summary>
		/// <param name="player">Player.</param>
		private void CleanoutUnderwaterComponents(AbstractPlayerController player)
		{
			m_Rigidy.constraints = RigidbodyConstraints.None;

			///*Destroy rigidbody and collider
			Destroy(m_Rigidy);
			Destroy(m_Collidy);

			///*Re-enable locomotor, gravity and camera limits
			//player.characterLocomotor.enabled = true;
			player.characterLocomotor.characterController.enabled = true;
			player.characterLocomotor.useGravity = true;
			player.playerCamera.clampEnabled = true;
		}

		/// <summary>
		/// Filters the raycast hit player info.
		/// </summary>
		/// <returns><c>true</c>, if player hit info was filtered, <c>false</c> otherwise.</returns>
		/// <param name="hitInfo">Hit info.</param>
		/// <param name="parent">Parent.</param>
		private bool FilterPlayerHitInfo(RaycastHit hitInfo, MonoBehaviour parent)
		{
			//FILTER HITINFO THAT COLLECTS THE COLLISION DATA OF THE PLAYER

			AbstractPlayerController player = GetPlayerController(parent);

			// Ignore collisions with target.
			if (hitInfo.collider == player.GetComponent<Collider>())
				return false;

			// Ignore collisions with calsule collider(If kinematic rigidbody is reinforced)
			if (player.GetComponent<CapsuleCollider>() != null)
			{
				if (hitInfo.collider == player.GetComponent<CapsuleCollider>())
					return false;
			}

			// Ignore triggers
			if (player.collider.isTrigger)
				return false;

			return true;
		}

		/// <summary>
		/// 	Destroys the bubble particles.
		/// </summary>
		/// <returns>The bubble particles.</returns>
		private ParticleSystem DestroyBubbleParticles(float delay)
		{
			m_InstantiatedBP.emissionRate = 0;
			Destroy(m_InstantiatedBP.gameObject, delay);
			m_InstantiatedBP = null;
			return null;
		}

		#endregion
	}
}