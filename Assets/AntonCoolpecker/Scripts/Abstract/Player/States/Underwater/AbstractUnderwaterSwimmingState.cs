﻿using UnityEngine;

namespace AntonCoolpecker.Abstract.Player.States.Underwater
{
	/// <summary>
	/// Abstract underwater swimming state.
	/// </summary>
    public abstract class AbstractUnderwaterSwimmingState : AbstractUnderwaterBaseState
    {
		#region Override Functions

		/// <summary>
		/// Called when the state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
        public override void OnEnter(MonoBehaviour parent)
        {
            base.OnEnter(parent);
        }

		#endregion
    }
}