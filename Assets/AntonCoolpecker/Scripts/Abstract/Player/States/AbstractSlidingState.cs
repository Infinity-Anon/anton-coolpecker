using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using AntonCoolpecker.Utils;
using AntonCoolpecker.Utils.Time;
using Hydra.HydraCommon.Utils;
using UnityEngine;

namespace AntonCoolpecker.Abstract.Player.States
{
	/// <summary>
	/// Abstract sliding state - Base state for all sliding states.
	/// </summary>
	public abstract class AbstractSlidingState : AbstractPlayerState
	{
		#region Variables

		[SerializeField] private AbstractStandingState m_StandingState;
		[SerializeField] private AbstractMidairState m_MidairState;
		[SerializeField] private AbstractSwimmingState m_SwimmingState;

		[SerializeField] [Tweakable("Slide")] private float m_SlideAccelerationModifier = 1.0f;
		[SerializeField] [Tweakable("Slide")] private float m_SlideMaxSpeedModifier = 1.0f;
		[SerializeField] [Tweakable("Slide")] private float m_SlideTransitionSpeed = 1.0f; //Transition speed into sliding
		[SerializeField] [Tweakable("Slide")] private float m_Slipperiness = 1.0f; //How slippery a slope is

		[SerializeField] [Tweakable("Slide")] private static float m_SlopeLimit = 40.0f; //Maximum angle before a surface counts as a slidable slope.

		private float m_SlideSmoothing = 1.0f; //Smoothen the slide velocity delta

		#endregion

		#region Properties

		/// <summary>
		/// Gets the slide acceleration multiplier.
		/// </summary>
		/// <value>The slide acceleration multiplier.</value>
		protected override float accelerationMultiplier { get { return m_SlideAccelerationModifier; } }

		/// <summary>
		/// Gets the slide max speed multiplier.
		/// </summary>
		/// <value>The slide max speed multiplier.</value>
		protected override float maxSpeedMultiplier { get { return m_SlideMaxSpeedModifier; } }

		#endregion

		#region Override Methods

		/// <summary>
		/// 	Returns a state for transition. Return self if no transition.
		/// </summary>
		/// <returns>The next state.</returns>
		/// <param name="parent">Parent.</param>
		public override AbstractPlayerState GetNextState(MonoBehaviour parent)
		{
			AbstractPlayerController player = GetPlayerController(parent);

			if (!player.characterLocomotor.isSoftGrounded || player.isInWater)
			{
				if (!player.isInWater)
					return m_MidairState;

				//if (player.waterTrigger.AllowFloatState(player.collider, Vector3.down, 1.2f, 2f, 12))
				return m_SwimmingState;
			}

			//Debug.Log (base.GetInputVector(player).magnitude);

			float inputMagnitude = HydraMathUtils.Clamp(base.GetInputVector(player).magnitude, 0, 1);

			if (!ShouldStartSliding(parent) && (player.characterLocomotor.velocity.magnitude - (inputMagnitude * player.maxSpeed)) <= m_SlideTransitionSpeed)
				return m_StandingState;

			return base.GetNextState(parent);
		}

		/// <summary>
		/// 	Called when the parent updates.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnUpdate(MonoBehaviour parent)
		{
			base.OnUpdate(parent);

			AbstractPlayerController player = GetPlayerController(parent);

			if (AntonCoolpecker.Concrete.Configuration.Controls.Mapping.InputMapping.jumpInput.GetButtonDown())
			{
				RaycastHit hitInfo = player.characterLocomotor.GroundedCast();
				Jump(hitInfo.normal, player);
				m_SlideSmoothing = 0.0f;
			}
		}

		/// <summary>
		/// Gets the change in velocity, given the current velocity, desired final velocity, and max acceleration.
		/// </summary>
		/// <param name="player">The player.</param>
		/// <param name="desiredV">The desired velocity, according to the character's max speed and input direction.</param>
		/// <param name="currentV">The character's current velocity.</param>
		/// <returns>The change in velocity for this step.</returns>
		protected override Vector3 ResolveVelocityDelta(AbstractPlayerController player, Vector3 desiredV, Vector3 currentV)
		{
			RaycastHit hitInfo = player.characterLocomotor.GroundedCast();

			Vector3 slideVector = Vector3.ProjectOnPlane(player.characterLocomotor.gravity * m_Slipperiness + desiredV,
														 hitInfo.normal);
			Vector3 correctedV = Vector3.ProjectOnPlane(currentV, hitInfo.normal);

			Vector3 deltaV = slideVector - correctedV;

			Vector3 slideDelta = Vector3.ClampMagnitude(deltaV, player.acceleration * accelerationMultiplier * GameTime.deltaTime);
			Vector3 correctionDelta = correctedV - currentV;

			return slideDelta + (correctionDelta * m_SlideSmoothing);

			/*
			if (Vector3.Angle(hitInfo.normal, Vector3.up) < player.characterLocomotor.characterController.slopeLimit)
				return;
			 */
		}

		/// <summary>
		/// 	Called when the parent's physics update.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnFixedUpdate(MonoBehaviour parent)
		{
			base.OnFixedUpdate(parent);
		}

		/// <summary>
		/// Called when the state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnEnter(MonoBehaviour parent)
		{
			base.OnEnter(parent);
			StartFreeFloating(parent);
			m_SlideSmoothing = 1;
		}

		/// <summary>
		/// Called before another state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnExit(MonoBehaviour parent)
		{
			base.OnExit(parent);
			StopFloating(parent);
		}

		#endregion

		#region Static Methods

		/// <summary>
		/// 	Gets the slide vector.
		/// </summary>
		/// <returns>The slide vector.</returns>
		/// <param name="surfaceNormal">Surface normal.</param>
		public static Vector3 GetSlideVector(Vector3 surfaceNormal)
		{
			return Vector3.RotateTowards(surfaceNormal, Vector3.down, Mathf.PI / 2.0f, 1.0f);
		}

		/// <summary>
		/// 	Returns true if the parent is in a position to start sliding.
		/// </summary>
		/// <returns><c>true</c>, if can start sliding, <c>false</c> otherwise.</returns>
		/// <param name="parent">Parent.</param>
		public static bool ShouldStartSliding(MonoBehaviour parent)
		{
			AbstractPlayerController player = GetPlayerController(parent);
			RaycastHit hitInfo = player.characterLocomotor.GroundedCast();

			if (hitInfo.collider == null)
				return false;

			float surfaceAngle = Vector3.Angle(hitInfo.normal, Vector3.up);

			if (surfaceAngle < m_SlopeLimit)
				return false;

			return true;
		}

		#endregion
	}
}