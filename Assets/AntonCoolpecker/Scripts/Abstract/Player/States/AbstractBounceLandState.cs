﻿using AntonCoolpecker.Abstract.Player;
using AntonCoolpecker.Abstract.Player.States;
using AntonCoolpecker.Concrete.Player;
using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using AntonCoolpecker.Concrete.Utils;
using AntonCoolpecker.Utils;
using AntonCoolpecker.Utils.Time;
using Hydra.HydraCommon.Utils;
using UnityEngine;

namespace AntonCoolpecker.Abstract.Player.States
{
    /// <summary>
    /// Allow the player to bounce jump immediately after hitting the ground or after a specified time
    /// </summary>
    public abstract class AbstractBounceLandState : AbstractPlayerState
    {
		#region Variables

        [Tweakable("BounceJump")] [SerializeField] protected float m_LandStateTime; //Time until transiton to standing state, recommended that this value stays small.

        [SerializeField] private AbstractMidairState m_MidairState; //MidairState to transition to on jump.
        [SerializeField] private AbstractPlayerState m_StandingState; //StandingState to transition to once timer has finished.
        [SerializeField] private AbstractBounceJumpState m_BounceJumpState; //We will need some variables from here;

        [SerializeField] private Timer m_StateTimer; //Timer
        protected Vector3 storedInput; //Stored player input

		#endregion

		#region Override Functions/Methods

        /// <summary>
        ///     Gets the state to transition to after the timer elapses.
        /// </summary>
        /// <value>The state of the timer.</value>
        protected override AbstractPlayerState timerState { get { return m_StandingState; } }

        /// <summary>
        /// Called on entry
        /// </summary>
        /// <param name="parent"></param>
        public override void OnEnter(MonoBehaviour parent)
        {
            base.OnEnter(parent);

            m_StateTimer.maxTime = m_LandStateTime;
            
            storedInput = m_BounceJumpState.InitialVelocity;

        }

        public override void OnUpdate(MonoBehaviour parent)
        {
            AbstractPlayerController player = GetPlayerController(parent);

            if (InputMapping.jumpInput.GetButtonDown())
            {
                //apply up force
                Jump(Vector3.up, player, m_BounceJumpState.BounceForce);
                player.airTracker.bounceNumber++;

                //add old flat velocity to curent velocity
                player.characterLocomotor.velocity = player.characterLocomotor.velocity + storedInput;
            }

            base.OnUpdate(parent);
        }

        /// <summary>
        /// Called to get next state
        /// </summary>
        /// <param name="parent"></param>
        /// <returns></returns>
        public override AbstractPlayerState GetNextState(MonoBehaviour parent)
        {
            AbstractPlayerController player = GetPlayerController(parent);

            if (!player.characterLocomotor.isSoftGrounded)
                return m_MidairState;

            if (m_StateTimer.complete)
                return timerState;
			
            return base.GetNextState(parent);
        }

        /// <summary>
        ///     Gets the timer to transition to another state.
        /// </summary>
        /// <value>The state timer.</value>
		public override Timer GetStateTimer(MonoBehaviour parent)
		{
            return m_StateTimer;
		}

		#endregion
    }
}