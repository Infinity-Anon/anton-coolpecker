﻿using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using AntonCoolpecker.Utils;
using Hydra.HydraCommon.Utils;
using System.Collections;
using UnityEngine;

namespace AntonCoolpecker.Abstract.Player.States
{
    /// <summary>
    /// Abstract sprinting state
    /// </summary>
    public abstract class AbstractSprintState : AbstractStandingState  
	{
		#region Variables

        //Max multipliers
        [Tweakable("Sprint")] [SerializeField] protected float m_MaxSpeedMultiplier = 1.5f;               
        [Tweakable("Sprint")] [SerializeField] protected float m_MaxRotationMultiplier = 1f;              
        [Tweakable("Sprint")] [SerializeField] protected float m_MaxAccelerationMultiplier = 1f;

        //Base multipliers
        [Tweakable("Sprint")] [SerializeField] protected float m_MinSpeedMultiplier = 1f;
        [Tweakable("Sprint")] [SerializeField] protected float m_MinRotationMultiplier = 1f;
        [Tweakable("Sprint")] [SerializeField] protected float m_MinAccelerationMultiplier = 1f;

        //Animator variables
        [Tweakable("Sprint")] [SerializeField] protected float m_AnimationSpeedDefault = 1f; //Base animation speed
        [Tweakable("Sprint")] [SerializeField] protected float m_AnimationSpeedAddition = 0.2f; //Addition to animation speed from sprint input
        [Tweakable("Sprint")] [SerializeField] protected float m_AnimationExitTransition = 0.1f; //Animation exit time

        [Tweakable("Sprint")] [SerializeField] protected float m_AllowSprintMagnitude = 0.9f; //Input magnitude required to allow sprint
        [Tweakable("Sprint")] [SerializeField] protected bool m_AxisIncrease = true; //Decides whether the player can manually adjust the sprinting by axis or not

        //State references
        [SerializeField] private AbstractStandingState m_StandingState;

		#endregion

        #region Properties

		/// <summary>
		/// Gets the default animation speed.
		/// </summary>
		/// <value>The default animation speed.</value>
        public float AnimationSpeedDefault { get { return m_AnimationSpeedDefault; } }

		/// <summary>
		/// Gets the addition to the animation speed.
		/// </summary>
		/// <value>The animation speed addition.</value>
		public float AnimationSpeedAddition { get { return m_AnimationSpeedAddition; } }
        
		/// <summary>
		/// Gets the movement input magnitude requirement
		/// </summary>
		/// <value>The allowed sprint magnitude.</value>
        public float AllowSprintMagnitude { get { return m_AllowSprintMagnitude; } }

		/// <summary>
		/// Gets the animation exit time.
		/// </summary>
		/// <value>The animation exit time.</value>
        public float GetAnimationExitTime { get { return m_AnimationExitTransition; } }

        /// <summary>
        /// Override default max speed with own value
        /// </summary>
        protected override float maxSpeedMultiplier
        {
            get
            {
				if (m_AxisIncrease) 
				{					
					return Mathf.Lerp (m_MinSpeedMultiplier, m_MaxSpeedMultiplier, InputMapping.sprintInput.GetAxisRaw());
				}
                else
                    return m_MaxSpeedMultiplier;
            }
        }
		
        /// <summary>
        /// Override default rotate speed with own value
        /// </summary>
        protected override float rotateSpeedMultiplier
        {
            get
            {
				if (m_AxisIncrease) 
				{
                    return Mathf.Lerp (m_MinRotationMultiplier, m_MaxRotationMultiplier, InputMapping.sprintInput.GetAxisRaw());
				}
                else
                    return m_MaxRotationMultiplier;
            }
        }
		
        /// <summary>
        /// Override default accleration with own value
        /// </summary>
        protected override float accelerationMultiplier
        {
            get
            {
				if (m_AxisIncrease) 
				{
                    return Mathf.Lerp (m_MinAccelerationMultiplier, m_MaxAccelerationMultiplier, InputMapping.sprintInput.GetAxisRaw());
                }
                else
                    return m_MaxAccelerationMultiplier;
            }
        }

        #endregion

        #region Overrides

        /// <summary>
        /// when valid input is detected, store it.
        /// </summary>
        /// <param name="desiredMovement"></param>
        /// <param name="player"></param>
        protected override void HandleInputStorage(Vector3 desiredMovement, AbstractPlayerController player)
        {
            //if the input magnitude is greater than the requirement then store input
            Vector3 inputVector = new Vector3(InputMapping.horizontalInput.GetAxisRaw(), 0.0f, InputMapping.verticalInput.GetAxisRaw());
            float inputMagnitude = HydraMathUtils.Clamp(inputVector.magnitude, 0, 1);

            if (inputMagnitude >= m_AllowSprintMagnitude)
            {
                m_StorageTimer.maxTime = m_TimeToStoreInput;
                m_StorageTimer.Reset();
                m_StoredInput = desiredMovement.normalized;

                //To prevent sliding when turning away after walking into a wall, calculate the change in position
                deltaTransform = HydraMathUtils.Abs((m_lastPosition - player.transform.position).magnitude);
                m_lastPosition = player.transform.position;
            }
        }

        /// <summary>
        /// 	Returns a state for transition. Return self if no transition.
        /// </summary>
        /// <returns>The next state.</returns>
        /// <param name="parent">Parent.</param>
        public override AbstractPlayerState GetNextState(MonoBehaviour parent)
        {
            AbstractPlayerController controller = GetPlayerController(parent);

            if (InputMapping.sprintInput.GetAxisRaw() == 0 || 
				(parent.GetComponentInChildren<Animator>().GetFloat("InputMagnitude") < m_AllowSprintMagnitude && (!m_Sliding && m_StorageTimer.complete)))
            {
                return m_StandingState;
            }

            return base.GetNextState(parent);
        }

		/// <summary>
		/// Called before another state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
        public override void OnExit(MonoBehaviour parent)
        {
            parent.GetComponentInChildren<Animator>().speed = m_AnimationSpeedDefault;

            base.OnExit(parent);
        }

		#endregion
    }
}