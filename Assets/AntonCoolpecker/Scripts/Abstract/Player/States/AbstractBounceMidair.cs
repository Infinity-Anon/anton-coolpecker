﻿using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using AntonCoolpecker.Utils;
using UnityEngine;

namespace AntonCoolpecker.Abstract.Player.States
{
	/// <summary>
	/// Abstract bounce midair - When the player is midair after performing a bounce jump.
	/// </summary>
    public abstract class AbstractBounceMidair : AbstractMidairState
    {
		#region Variables

        [Tweakable("BounceJump")] [SerializeField] protected float m_SpeedMultiplier;

        [SerializeField] protected AbstractBounceJumpState m_BounceJump;

		#endregion

		#region Properties

        /// <summary>
        /// Override default max speed with own value
        /// </summary>
        protected override float maxSpeedMultiplier { get { return m_SpeedMultiplier; } }

		#endregion

		#region Override Methods

        /// <summary>
        /// 	Returns a state for transition. Return self if no transition.
        /// </summary>
        /// <returns>The next state.</returns>
        /// <param name="parent">Parent.</param>
        public override AbstractPlayerState GetNextState(MonoBehaviour parent)
        {
            AbstractPlayerController controller = GetPlayerController(parent);

            if (InputMapping.groundPoundInput.GetButtonDown() && controller.airTracker.canBounce)
            {
                return m_BounceJump;
            }

            return base.GetNextState(parent);
        }

		#endregion
    }
}