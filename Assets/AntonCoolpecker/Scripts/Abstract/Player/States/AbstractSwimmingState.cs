﻿using AntonCoolpecker.Abstract.Player.States.Underwater;
using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using AntonCoolpecker.Concrete.Forces;
using AntonCoolpecker.Concrete.HitBoxes;
using AntonCoolpecker.Concrete.Utils;
using AntonCoolpecker.Utils;
using AntonCoolpecker.Utils.Time;
using Hydra.HydraCommon.Utils;
using UnityEngine;

namespace AntonCoolpecker.Abstract.Player.States
{
	/// <summary>
	/// 	Abstract swimming state.
	/// </summary>
	public abstract class AbstractSwimmingState : AbstractPlayerState
	{
		/* ---FROG SWIM CONTROLS---
		 * INPUT-BASED:
		 * 
		 * Each time the player begins to move when still in water, a frog swim movement
		 * is initiated. If the input direction is held without interruption, the player will swim in a normal manner. 
		 * The swim movement will be reset each time the magnitude of the input is zero. 
		 * 
		 * However, if the player stops while the swim
		 * movement is slowing down at a certain moment and then quickly moves again, the player will quickly
		 * initiate another swim movement without any initial swim slowdown, setting the speed/acceleration
		 * of the movement higher. Thus the player can initiale a swim combo, getting the speed/acceleration
		 * of the swim higher and higher for each successful swim movement. When the player has reached the
		 * max combo, the acceleration/speed will not go higher but the player can continue performing the
		 * combo for a continuous high speed/acceleration for the swim movement.
		 * 
		 * BUTTON-BASED:
		 * 
		 * Same as input based, expect the player doesn't need to use and stop the movement input to
		 * use the frog swim - Instead, a button is replacing the swim movement input. Press and hold 
		 * the swim button while moving in water to initiate the frog swim movement. The swim movement input
		 * does not need to be interrupted for continuous frog swim.
		 * 
		 * */

		#region Variables

		[SerializeField] private AbstractStandingState m_StandingState;
		[SerializeField] private AbstractUnderwaterState m_UnderwaterState;
		[SerializeField] private AbstractUnderwaterTransitionState m_UnderwaterTransitionState;
		[SerializeField] private AbstractMidairState m_MidairState;

		//*MULTIPLIERS
		[Tweakable("Swim")] [SerializeField] internal float m_MaxSpeedMultiplier = 0f;
		[Tweakable("Swim")] [SerializeField] internal float m_JumpHeightMultiplier = 0.5f;
		[Tweakable("Swim")] [SerializeField] internal float m_RotateSpeedMultiplier = 0.5f;
		[Tweakable("Swim")] [SerializeField] internal float m_AccelerationMultiplier = 0f;

		//*Original values for max speed and acceleration multipliers(Serialized values
		//*above does not detect the initial value of the multipliers)
		[Tweakable("Swim")] [SerializeField] internal float m_OrigMaxSpeedMultiplier = 0.5f;
		[Tweakable("Swim")] [SerializeField] internal float m_OrigAccelerationMultiplier = 0.5f;

		//*Desired max values when the player is speeding up swim movement
		[Tweakable("Swim")] [SerializeField] internal float m_DesiredMaxSpeedMultiplier = 1.5f;
		[Tweakable("Swim")] [SerializeField] internal float m_DesiredMaxAccelerationMultiplier = 1.5f;

		//*Desired min values when player is slowing down the swim movement
		[SerializeField] internal float m_DesiredMinSpeedMultiplier = 0f;
		[SerializeField] internal float m_DesiredMinAccelerationMultiplier = 0f;

		//*Addon values for each time the player successfully executes a swim movement
		[Tweakable("Swim")] [SerializeField] internal float m_MaxSwimMultiplierSpeedAddon = 0.15f;
		[Tweakable("Swim")] [SerializeField] internal float m_MaxSwimMultiplierAccelAddon = 0.15f;

		//*Max value for swim combo
		[Tweakable("Swim")] [SerializeField] internal float m_MaxSwimComboCount = 5;

		//*Values for clamping the threshold for a successful swim combo
		//[Tweakable("Swim")] [SerializeField] private Vector2 m_ReswimClamp = new Vector2(0.4f, 0.7f);
		[Tweakable("Swim")] [SerializeField] internal float m_ReswimClampX = 0.4f;
		[Tweakable("Swim")] [SerializeField] internal float m_ReswimClampY = 0.7f;

		//*Values for the ability to jump out of water while swimming
		[Tweakable("Swim")] [SerializeField] internal float m_DesiredJumpHeight = 10.3f;
		[Tweakable("Swim")] [SerializeField] internal float m_OriginalJumpHeightOffset = 0f;
		[Tweakable("Swim")] [SerializeField] internal float m_JumpDistanceOffset = 0.8f;
		[Tweakable("Swim")] [SerializeField] internal float m_RayCheckJumpOffset = 1f;

		//*Button press frog swim is false, input movement frog swim is true
		[Tweakable("Swim")] [SerializeField] internal bool m_ButtonFalseInputTrue = true;
		[Tweakable("Swim")] [SerializeField] internal bool m_AllowFrogSwim = true;
		[Tweakable("Swim")] [SerializeField] internal bool m_JumpUpLimit = false;

		//*OBSERVE: The max time of the swim timers are serialized in values seperate from the timers,
		//*due to current timer values not being properly reset if game is paused/reset.
		[Tweakable("Swim")] [SerializeField] internal float m_OriginalInitalSwimTimer = 0.2f;
		[Tweakable("Swim")] [SerializeField] internal float m_OriginalEndOfSwimTimer = 0.25f;

		//*SWIMMING VALUES
		private bool initialSwim = false;
		private bool lerpToMax = false;
		private bool hasSwimmed = false;
		private bool notSwimming = false;

		private float currentInputMagnitude = 0;

		private bool m_HasSwimJumped;
		private bool m_HasSwimmed;

		private Timer m_InitalSwimTimer; 
		private Timer m_EndOfSwimTimer; 

		private int m_SwimComboCount = 0;

		private float m_OriginalJumpHeight;

		#endregion

		#region Properties

		/// <summary>
		/// 	Gets the max speed multiplier.
		/// </summary>
		/// <value>The max speed multiplier.</value>
		protected override float maxSpeedMultiplier { get { return m_MaxSpeedMultiplier; } }
		public float swimMaxSpeedMultiplier { get { return m_MaxSpeedMultiplier; } set { m_MaxSpeedMultiplier = value; } }

		/// <summary>
		/// 	Gets the jump force multiplier.
		/// </summary>
		/// <value>The jump force multiplier.</value>
		protected override float jumpHeightMultiplier { get { return m_JumpHeightMultiplier; } }
		public float swimJumpHeightMultiplier { get { return m_JumpHeightMultiplier; } set { m_JumpHeightMultiplier = value; } }

		/// <summary>
		/// 	Gets the rotate speed multiplier.
		/// </summary>
		/// <value>The rotate speed multiplier.</value>
		protected override float rotateSpeedMultiplier { get { return m_RotateSpeedMultiplier; } }
		public float swimRotateSpeedMultiplier { get { return m_RotateSpeedMultiplier; } set { m_RotateSpeedMultiplier = value; } }

		/// <summary>
		/// 	Gets the acceleration multiplier.
		/// </summary>
		/// <value>The acceleration multiplier.</value>
		protected override float accelerationMultiplier { get { return m_AccelerationMultiplier; } }
		public float swimAccelerationMultiplier { get { return m_AccelerationMultiplier; } set { m_AccelerationMultiplier = value; } }

		/// <summary>
		/// 	Gets the timer for initial swimming
		/// </summary>
		/// <value>The initial swim timer.</value>
		public Timer initialSwimTimer { get { return m_InitalSwimTimer; } }

		/// <summary>
		/// 	Gets the timer for end of swimming
		/// </summary>
		/// <value>The end swim timer.</value>
		public Timer endSwimTimer { get { return m_EndOfSwimTimer; } }

		/*[Tweakable("Swim")] public float origMaxSpeedMultiplier { get { return m_OrigMaxSpeedMultiplier; } set { m_OrigMaxSpeedMultiplier = value; } }
		[Tweakable("Swim")] public float origAccelerationMultiplier { get { return m_OrigAccelerationMultiplier; } set { m_OrigAccelerationMultiplier = value; } }

		[Tweakable("Swim")] public float desiredMaxSpeedMultiplier { get { return m_DesiredMaxSpeedMultiplier; } set { m_DesiredMaxSpeedMultiplier = value; } }
		[Tweakable("Swim")] public float desiredMaxAccelerationMultiplier { get { return m_DesiredMaxAccelerationMultiplier; } set { m_DesiredMaxAccelerationMultiplier = value; } }

		[Tweakable("Swim")] public float desiredMinSpeedMultiplier { get { return m_DesiredMinSpeedMultiplier; } set { m_DesiredMinSpeedMultiplier = value; } }
		[Tweakable("Swim")] public float desiredMinAccelerationMultiplier { get { return m_DesiredMinAccelerationMultiplier; } set { m_DesiredMinAccelerationMultiplier = value; } }

		[Tweakable("Swim")] public float maxSwimMultiplierSpeedAddon { get { return m_MaxSwimMultiplierSpeedAddon; } set { m_MaxSwimMultiplierSpeedAddon = value; } }
		[Tweakable("Swim")] public float maxSwimMultiplierAccelAddon { get { return m_MaxSwimMultiplierAccelAddon; } set { m_MaxSwimMultiplierAccelAddon = value; } }

		[Tweakable("Swim")] public float maxSwimComboCount { get { return m_MaxSwimComboCount; } set { m_MaxSwimComboCount = value; } }

		[Tweakable("Swim")] public float reswimClampMax { get { return m_ReswimClampY; } set { m_ReswimClampY = value; } }
		[Tweakable("Swim")] public float reswimClampMin { get { return m_ReswimClampX; } set { m_ReswimClampX = value; } }

		[Tweakable("Swim")] public float desiredJumpHeight { get { return m_DesiredJumpHeight; } set { m_DesiredJumpHeight = value; } }
		[Tweakable("Swim")] public float originalJumpHeightOffset { get { return m_OriginalJumpHeightOffset; } set { m_OriginalJumpHeightOffset = value; } }
		[Tweakable("Swim")] public float jumpDistanceOffset { get { return m_JumpDistanceOffset; } set { m_JumpDistanceOffset = value; } }
		[Tweakable("Swim")] public float rayCheckJumpOffset { get { return m_RayCheckJumpOffset; } set { m_RayCheckJumpOffset = value; } }

		//[Tweakable("Swim")] public bool buttonFalseInputTrue { get { return m_ButtonFalseInputTrue; } set { m_ButtonFalseInputTrue = value; } }
		//[Tweakable("Swim")] public bool allowFrogSwim { get { return m_AllowFrogSwim; } set { m_AllowFrogSwim = value; } }

		[Tweakable("Swim")] public float originalInitalSwimTimer { get { return m_OriginalInitalSwimTimer; } set { m_OriginalInitalSwimTimer = value; } }
		[Tweakable("Swim")] public float originalEndOfSwimTimer { get { return m_OriginalEndOfSwimTimer; } set { m_OriginalEndOfSwimTimer = value; } }*/

		#endregion

		#region Override Functions/Methods

		/// <summary>
		/// 	Called when the state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnEnter(MonoBehaviour parent)
		{
			base.OnEnter(parent);
			m_HasSwimJumped = false;
			AbstractPlayerController player = GetPlayerController(parent);

			player.airTracker.Reset();
			player.airTracker.canAirJump = false;

			if (m_OriginalJumpHeight != null) 
			{
				if (m_JumpUpLimit)
					m_OriginalJumpHeight = player.jumpHeight + m_OriginalJumpHeightOffset;
				
				else 
				{
					m_OriginalJumpHeight = player.jumpHeight;
					player.jumpHeight = m_DesiredJumpHeight;
				}
			}

			player.characterLocomotor.useGravity = false;

			m_InitalSwimTimer = new Timer();
			m_EndOfSwimTimer = new Timer();

			//Make sure the swim timer is re-initialized
			m_InitalSwimTimer.Reset();
			m_InitalSwimTimer.Pause();

			//The end of the swim is reset and paused until player has reached end of the swim
			m_EndOfSwimTimer.Reset();
			m_EndOfSwimTimer.Pause();

			initialSwim = true;
			lerpToMax = false;
			hasSwimmed = false;
			notSwimming = false;

			if (m_AllowFrogSwim) 
			{
				m_MaxSpeedMultiplier = 0f;
				m_AccelerationMultiplier = 0f;
			}

			else 
			{
				m_MaxSpeedMultiplier = m_OrigMaxSpeedMultiplier;
				m_AccelerationMultiplier = m_OrigAccelerationMultiplier;
			}

			m_SwimComboCount = 0;

			m_InitalSwimTimer.maxTime = m_OriginalInitalSwimTimer;
			m_EndOfSwimTimer.maxTime = m_OriginalEndOfSwimTimer;
		}

		/// <summary>
		/// Called before another state becomes active.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnExit(MonoBehaviour parent)
		{
			AbstractPlayerController player = GetPlayerController(parent);
			player.characterLocomotor.useGravity = true;
			player.jumpHeight = m_OriginalJumpHeight;
			base.OnExit(parent);
		}

		/// <summary>
		/// 	Returns a state for transition. Return self if no transition.
		/// </summary>
		/// <returns>The next state.</returns>
		/// <param name="parent">Parent.</param>
		public override AbstractPlayerState GetNextState(MonoBehaviour parent)
		{
			AbstractPlayerController player = GetPlayerController(parent);

			//if (!player.isInWater || !player.waterTrigger.AllowFloatState(player.collider, Vector3.down, 1.2f, 2f, 12))
			if (!player.isInWater)
			{
				if (player.characterLocomotor.isGrounded)
					return m_StandingState;

				return m_MidairState;
			}

			if (InputMapping.swimInput.GetButtonDown ()) 
			{
				if (m_UnderwaterTransitionState.DownUnder(player) && player.waterTrigger.allowUnderwater) 
				{
					m_UnderwaterTransitionState.SwimIn ();
					return m_UnderwaterTransitionState;
				}
			}

			return base.GetNextState(parent);
		}

		/// <summary>
		/// 	Called when the parent updates.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnUpdate(MonoBehaviour parent)
		{
			base.OnUpdate(parent);

			AbstractPlayerController player = GetPlayerController(parent);

			// If the player tries to swim jump very high by pressing the jump button twice before leaving the water,
			// it shouldn't work. When the player jumps, they can't jump again until the player begins moving downwards.
			if (player.characterLocomotor.velocity.y < 0)
				m_HasSwimJumped = false;

			if (AntonCoolpecker.Concrete.Configuration.Controls.Mapping.InputMapping.jumpInput.GetButtonDown() && !m_HasSwimJumped)
			{
				Jump(player);
				m_HasSwimJumped = true;
			}

			if (m_AllowFrogSwim)
				FrogOrRegularSwim(parent, AbstractPlayerController.HasUserInputRaw ());

			if (m_JumpUpLimit) 
			{
				if (CanJumpUp (player, 1.0f) || CanJumpUp (player, 1.4f) || CanJumpUp (player, 2.2f)) 
				{
					if (player.waterTrigger.interpolateWaterVelocity)
						player.jumpHeight = m_DesiredJumpHeight + 1f;

					if (!player.waterTrigger.interpolateWaterVelocity)
						player.jumpHeight = m_DesiredJumpHeight;
				} 

				else
					player.jumpHeight = m_OriginalJumpHeight;
			}
		}

		#endregion

		#region Functions

		/// <summary>
		/// Handles the frog/regular swim functionality.
		/// </summary>
		/// <param name="parent">Parent.</param>
		/// <param name="controlCheck">If set to <c>true</c>, the player is using movement input.</param>
		public void FrogOrRegularSwim(MonoBehaviour parent, bool controlCheck)
		{
			AbstractPlayerController player = GetPlayerController(parent);

			//When the player is using movement input, handle the swim
			if ((controlCheck && m_ButtonFalseInputTrue) || (controlCheck && !m_ButtonFalseInputTrue && InputMapping.swipeAttackInput.GetButton()))
			{
				if (notSwimming)
					isSwimming ();

				//If this is the initial part of the current swim
				if (initialSwim)
				{
					if (m_InitalSwimTimer.paused)
						m_InitalSwimTimer.Resume();

					if (m_InitalSwimTimer.complete) 
					{
						//Debug.Log ("Initial swim timer complete");
						InitialAccelToMax ();
					} 

					if (!m_InitalSwimTimer.complete) 
					{
						m_MaxSpeedMultiplier = Mathf.Lerp (m_MaxSpeedMultiplier, currentMaxSpeedMultiplier(0.2f), 0.1f * GameTime.deltaTime);
						m_AccelerationMultiplier = Mathf.Lerp (m_AccelerationMultiplier, currentAccelerationMultiplier(-0.2f), 0.1f * GameTime.deltaTime);
					}
				}

				//If this is NOT the initial part of the current swim
				if (!initialSwim) 
				{
					if (!hasSwimmed) 
					{
						if (lerpToMax) 
						{
							//Debug.Log ("Going to max speed value");

							m_MaxSpeedMultiplier = Mathf.Lerp (m_MaxSpeedMultiplier, currentMaxSpeedMultiplier(0.2f), 10f * GameTime.deltaTime);
							m_AccelerationMultiplier = Mathf.Lerp (m_AccelerationMultiplier, currentAccelerationMultiplier(-0.2f), 10f * GameTime.deltaTime);

							if (m_MaxSpeedMultiplier > currentMaxSpeedMultiplier(0f) || m_AccelerationMultiplier > currentAccelerationMultiplier(-0.4f))
								EndAccelToMin ();
						}

						if (!lerpToMax) 
						{
							//Debug.Log ("Going to min speed value");

							m_MaxSpeedMultiplier = Mathf.Lerp (m_MaxSpeedMultiplier, 0f, 2f * GameTime.deltaTime);
							m_AccelerationMultiplier = Mathf.Lerp (m_AccelerationMultiplier, 0f, 2f * GameTime.deltaTime);

							if (m_MaxSpeedMultiplier < (m_DesiredMinSpeedMultiplier + 0.2f) || m_AccelerationMultiplier < (m_DesiredMinAccelerationMultiplier + 0.2f))
								HasSwimmed ();
						}
					}

					if (hasSwimmed) 
					{
						if (m_MaxSpeedMultiplier != m_OrigMaxSpeedMultiplier || m_AccelerationMultiplier != m_OrigAccelerationMultiplier) 
						{
							//Debug.Log ("Returning to normal swim");

							m_MaxSpeedMultiplier = Mathf.Lerp (m_MaxSpeedMultiplier, m_OrigMaxSpeedMultiplier, 4f * GameTime.deltaTime);
							m_AccelerationMultiplier = Mathf.Lerp (m_AccelerationMultiplier, m_OrigMaxSpeedMultiplier, 4f * GameTime.deltaTime);
						}
					}
				}
			}

			if (controlCheck && !m_ButtonFalseInputTrue && !InputMapping.swipeAttackInput.GetButton()) 
			{
				//Debug.Log ("Returning to normal swim - Button press style");

				m_MaxSpeedMultiplier = Mathf.Lerp (m_MaxSpeedMultiplier, m_OrigMaxSpeedMultiplier, 4f * GameTime.deltaTime);
				m_AccelerationMultiplier = Mathf.Lerp (m_AccelerationMultiplier, m_OrigMaxSpeedMultiplier, 4f * GameTime.deltaTime);
			}

			//if (!AbstractPlayerController.HasUserInput ())
			if (!controlCheck || (!m_ButtonFalseInputTrue && !InputMapping.swipeAttackInput.GetButton())) 
			{
				if (!notSwimming) 
				{
					resetTimers();

					if (m_EndOfSwimTimer.paused)
						m_EndOfSwimTimer.Resume();

					if (!initialSwim && !hasSwimmed && !lerpToMax) 
					{
						if (withinReswimRange ()) 
						{
							//Debug.Log ("WITHIN RANGE");
							noWaitingTime ();

							if (m_SwimComboCount < m_MaxSwimComboCount)
								AddToSwimCombo();
							
							//Debug.Log (m_SwimComboCount);
						}
					} 

					else 
					{
						//Debug.Log ("OUTSIDE RANGE");
						yesWaitingTime ();
						ResetSwimCombo ();
					}

					resetSwim ();
					notSwimmingAtAll ();
				}

				if (m_EndOfSwimTimer.complete) 
				{
					//Debug.Log ("End swim timer complete");
					ResetSwimCombo ();
					resetTimers();
					yesWaitingTime();
				}
			}
		}

		/// <summary>
		/// Sets the initial input magnitude.
		/// </summary>
		/// <param name="inputMagVector">Input magnitude vector.</param>
		/*private void setInitialInputMagnitude(Vector3 inputMagVector)
		{
			currentInputMagnitude = HydraMathUtils.Max (HydraMathUtils.Abs(inputMagVector.x), HydraMathUtils.Abs(inputMagVector.z));
		}*/

		/// <summary>
		/// Called after the beginning of a swim to set the acceleration multiplier to maximum value.
		/// </summary>
		private void InitialAccelToMax()
		{
			lerpToMax = true;
			initialSwim = false;
		}
			
		/// <summary>
		/// Called at the end of a swim to set the acceleration multiplier to minimum value.
		/// </summary>
		private void EndAccelToMin()
		{
			lerpToMax = false;
		}

		/// <summary>
		/// Called when it's clear that the player has started swimming
		/// </summary>
		/// <returns><c>true</c> if this instance has swimmed; otherwise, <c>false</c>.</returns>
		private void HasSwimmed()
		{
			hasSwimmed = true;
		}

		/// <summary>
		/// Called when the player's swim is reset(When not swimming a.k.a using input).
		/// </summary>
		private void resetSwim()
		{
			initialSwim = true;
			lerpToMax = false;
			hasSwimmed = false;

			if (m_ButtonFalseInputTrue) 
			{
				m_MaxSpeedMultiplier = 0f;
				m_AccelerationMultiplier = 0f;
			}
		}

		/// <summary>
		/// Called when the swimming timers are reset.
		/// </summary>
		private void resetTimers()
		{
			//Make sure the player initializes the swim again
			m_InitalSwimTimer.Reset();
			m_InitalSwimTimer.Pause();

			//The end of the swim is reset and paused until it's reached again
			m_EndOfSwimTimer.Reset();
			m_EndOfSwimTimer.Pause();
		}

		/// <summary>
		/// If there is no waiting time before the initial swim reaches proper velocity.
		/// </summary>
		private void noWaitingTime()
		{
			m_InitalSwimTimer.maxTime = 0;
		}

		/// <summary>
		/// If there is waiting time before the initial swim reaches proper velocity.
		/// </summary>
		private void yesWaitingTime()
		{
			m_InitalSwimTimer.maxTime = m_OriginalInitalSwimTimer;
		}

		/// <summary>
		/// Called when the player is not swimming at all(Therefore using no input).
		/// </summary>
		private void notSwimmingAtAll()
		{
			notSwimming = true;
		}

		/// <summary>
		/// Called when the player is swimming(Using input).
		/// </summary>
		private void isSwimming()
		{
			notSwimming = false;
		}

		/// <summary>
		/// Adds one to the swim combo.
		/// </summary>
		private void AddToSwimCombo()
		{
			m_SwimComboCount = m_SwimComboCount + 1;
		}

		/// <summary>
		/// Resets the swim combo fully.
		/// </summary>
		private void ResetSwimCombo()
		{
			m_SwimComboCount = 0;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Calculates the current max speed multiplier value while swimming.
		/// </summary>
		/// <returns>The current max speed multiplier value</returns>
		/// <param name="addi">Value to be added to the max speed acceleration multiplier.</param>
		private float currentMaxSpeedMultiplier(float addi)
		{
			return ((m_DesiredMaxSpeedMultiplier + addi) + (m_MaxSwimMultiplierSpeedAddon * m_SwimComboCount));
		}

		/// <summary>
		/// Calculates the current acceleration multiplier value while swimming.
		/// </summary>
		/// <returns>The current acceleration multiplier value</returns>
		/// <param name="addi">Value to be added to the current acceleration multiplier.</param>
		private float currentAccelerationMultiplier(float addi)
		{
			return ((m_DesiredMaxAccelerationMultiplier + addi) + (m_MaxSwimMultiplierAccelAddon * m_SwimComboCount));
		}

		/// <summary>
		/// Determines whether the player's max speed multiplier(Slowing down when player releases input) is
		/// within range for continuing a swimming combo.
		/// </summary>
		/// <returns><c>true</c>, if the player is within reswimming range, <c>false</c> otherwise.</returns>
		private bool withinReswimRange()
		{
			if (m_MaxSpeedMultiplier > m_ReswimClampX && m_MaxSpeedMultiplier < m_ReswimClampY)
				return true;

			if (m_MaxSpeedMultiplier > m_ReswimClampX && m_MaxSpeedMultiplier < m_ReswimClampY)
				return true;

			return false;
		}

		/// <summary>
		/// Determines whether the player can jump up to a horizontal surface while close to a vertical surface or not.
		/// </summary>
		/// <returns><c>true</c> if this instance can jump up to a vertical surface, otherwise, <c>false</c>.</returns>
		/// <param name="player">The player</param>
		/// <param name="forwardDist">The forward distance to a vertical surface.</param>
		private bool CanJumpUp(AbstractPlayerController player, float forwardDist)
		{
			RaycastHit hitInfo1;
			RaycastHit hitInfo2;
			
			Vector3[] rayPositions = new Vector3[2];

			rayPositions[0] = new Vector3(player.transform.position.x, player.transform.position.y + player.collider.bounds.size.y + m_RayCheckJumpOffset,
			                              (player.transform.position.z));
			rayPositions[1] = new Vector3(player.transform.position.x, player.transform.position.y - m_RayCheckJumpOffset,
			                              (player.transform.position.z));

			Vector3[] directions = new Vector3[2];
			directions [0] = Vector3.down;
			directions [1] = Vector3.up;

			float distance = player.collider.bounds.size.y + m_JumpDistanceOffset;

			LayerMask layerToIgnore = 1 << 4; //IGNORE WATER LAYER
			layerToIgnore |= 1 << 12; //IGNORE PLAYER LAYER
			//TODO: ADD OTHER LAYERS TO BE IGNORED

			bool canDo1 = Physics.Raycast((rayPositions [0] + (player.transform.forward * forwardDist)), directions[0], out hitInfo1, distance, ~layerToIgnore);
			bool canDo2 = Physics.Raycast((rayPositions [1] + (player.transform.forward * forwardDist)), directions[1], out hitInfo2, distance, ~layerToIgnore);

			if ((canDo1 && canDo2) || canDo1)
				return true;

			return false;
		}

		#endregion
	}
}