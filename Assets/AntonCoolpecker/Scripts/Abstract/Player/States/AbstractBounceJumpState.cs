﻿using AntonCoolpecker.Abstract.Player;
using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using AntonCoolpecker.Concrete.Utils;
using AntonCoolpecker.Utils;
using AntonCoolpecker.Utils.Time;
using System.Collections;
using UnityEngine;

namespace AntonCoolpecker.Abstract.Player.States
{
    /// <summary>
	/// Abstract bounce jump state - When the player does a bounce jump while midair(No previous sprint).
    /// </summary>
    public abstract class AbstractBounceJumpState : AbstractPlayerState 
	{
        [Tweakable("BounceJump")] [SerializeField] protected float m_DownForce = 5; //Force to move the player downwards
        [Tweakable("BounceJump")] [SerializeField] protected int m_BounceLimit; //Total number of allowed bounces
        [Tweakable("BounceJump")] [SerializeField] protected float m_RequiredDistanceToGround; //How close to the ground the player needs to be before bouncing
        [Tweakable("BounceJump")] [SerializeField] protected float m_BounceForce = 20; //Force to apply upwards after the bounce land
        
        [SerializeField] private AbstractMidairState m_MidairState; //Base Midair state
        [SerializeField] private AbstractBounceLandState m_LandingState; //Landing state for bounce

        private Vector3 m_InitialVelocity; //Store entry velocity

        #region Properties 

		/// <summary>
		/// Gets the bounce force.
		/// </summary>
		/// <value>The bounce force.</value>
        public float BounceForce { get { return m_BounceForce; } }

		/// <summary>
		/// Gets the initial velocity.
		/// </summary>
		/// <value>The initial velocity.</value>
        public Vector3 InitialVelocity { get { return m_InitialVelocity; } }

        #endregion

        #region Overrides

        /// <summary>
        /// On enter, store the flat velocty and apply downwards force.
        /// </summary>
        /// <param name="parent"></param>
        public override void OnEnter(MonoBehaviour parent)
        {
            AbstractPlayerController player = GetPlayerController(parent);

            m_InitialVelocity = player.characterLocomotor.flatVelocity;
           
            player.characterLocomotor.velocity = Vector3.down * m_DownForce;

            base.OnEnter(parent);
        }

        /// <summary>
        /// Get next state
        /// </summary>
        /// <param name="parent"></param>
        /// <returns></returns>
        public override AbstractPlayerState GetNextState(MonoBehaviour parent)
        {
            AbstractPlayerController player = GetPlayerController(parent);

            //Check if we are close enough to the ground, if so do stuff
            Ray ray = new Ray(parent.transform.position, -parent.transform.up);
            RaycastHit hit;

            if ((Physics.Raycast(ray, out hit, m_RequiredDistanceToGround) && !hit.collider.GetComponent<Concrete.Collectables.Collectable>())
				&& InputMapping.jumpInput.GetButtonDown())
            {
                //Set current velocity to zero, then apply upforce and old flat velocity
                player.characterLocomotor.velocity = Vector3.zero;
                Jump(Vector3.up,player, m_BounceForce);
                player.airTracker.bounceNumber++;

                if (player.airTracker.bounceNumber >= m_BounceLimit)
                    player.airTracker.canBounce = false;
				
                player.characterLocomotor.velocity = player.characterLocomotor.velocity+ m_InitialVelocity;

                return m_MidairState;
            }

            if (player.characterLocomotor.isGrounded)
                return m_LandingState;

            return base.GetNextState(parent);
        }

        #endregion
    }
}