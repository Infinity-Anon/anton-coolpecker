﻿using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using AntonCoolpecker.Concrete.Utils;
using AntonCoolpecker.Utils;
using AntonCoolpecker.Utils.Time;
using Hydra.HydraCommon.Utils;
using UnityEngine;

//TODO: Leaving the slide with the control stick still facing the opposite direction to the player results in sudden snap - Fix so that this doesn't happen!
namespace AntonCoolpecker.Abstract.Player.States
{
	/// <summary>
	/// 	Abstract standing state.
	/// </summary>
	public abstract class AbstractStandingState : AbstractPlayerState
	{
		#region Variables

		[SerializeField] private AbstractMidairState m_MidairState;
		[SerializeField] private AbstractSlidingState m_SlidingState;
		[SerializeField] private AbstractSwimmingState m_SwimmingState;

		//GROUND INTERPOLATION
		[Tweakable("Standing")] [SerializeField] internal bool m_InterpolateGravityScaleY = true; //Enable/disable ground interpolation
		[Tweakable("Standing")] [SerializeField] internal bool m_UseExponentialAngleCalc = true; //Calculate the ground interpolation exponentially by surface angle?
		//[Tweakable("Standing")] [SerializeField] internal float m_OverrideGravityScaleY = 3f;
		[Tweakable("Standing")] [SerializeField] internal float m_LinearCalcInterpolationMultiplier = 0.01f; //Multiplier for linear ground interpolation
		[Tweakable("Standing")] [SerializeField] internal float m_LinearCalcAngleLimitX = 1f; //Minimum surface angle for linear ground interpolation
		[Tweakable("Standing")] [SerializeField] internal float m_LinearCalcAngleLimitY = 40f; //Maximum surface angle for linear ground interpolation
		[Tweakable("Standing")] [SerializeField] internal float m_GravityRayMaxDistance = 0.1f; //Maximum distance to ground for applying ground interpolation

		private Vector2 m_LinearCalcAngleLimit = new Vector2(1, 40); //Surface angle limits for using linear ground interpolation
		private bool m_UseLinearCalc = true; //Use linear calculation for the ground interpolation?

		//180 DEGREE TURN SLIDE
		[Tweakable("180Turn")] [SerializeField] internal bool m_UseSlideTurn = true; //Allow the player to do slide turns
		[Tweakable("180Turn")] [SerializeField] internal bool m_UseSlideEdgeDetection = true; //Allow the slide to detect edges
		[Tweakable("180Turn")] [SerializeField] internal bool m_UseSlideMinimumDeltaPositon = true;  //Require the slide to have a minimum entry speed
		[Tweakable("180Turn")] [SerializeField] internal float m_SlideTurnAngleTolerance = 40f; //Angle of tolerance required to trigger a slide turn
		[Tweakable("180Turn")] [SerializeField] internal float m_SlideTurnEdgeDetectionDistance = 3f; //Angle of tolerance required to trigger a slide turn
		[Tweakable("180Turn")] [SerializeField] internal float m_SlideRotationTurnTime = 0.4f; //Time to complete a slide (Rotation)
		[Tweakable("180Turn")] [SerializeField] internal float m_SlideVelocityTurnTime = 0.5f; //Time to complete a slide (Velocity)
		[Tweakable("180Turn")] [SerializeField] internal float m_TimeToStoreInput = 0.1f; //Time that the last directional input will be stored when directional input = 0
		[Tweakable("180Turn")] [SerializeField] internal float m_MinimumTransformDelta = 0.1f; //A required minimum change in transform between the current frame and the previous frame in order to trigger a slide

        private Timer m_SlideRotationTimer;
        private Timer m_SlideVelocityTimer;

        protected bool m_Sliding; //Is the player doing a 180 degree turn slide?
        protected Timer m_StorageTimer;
        private Vector3 m_InitialSlideVelocity; //Stores the velocity of the initial 180 degree turn slide
        private Vector3 m_InitialSlideDesieredMovement; //Stores the desired movement of the initial 180 degree turn slide
        private Vector3 m_InitialSlideForward; //Stores the forward of the initial 180 degree turn slide
        protected Vector3 m_StoredInput; //Stores desired input in 180 degree turn slide
        protected Vector3 m_lastPosition; //Stores the player's position in the last update
        private float m_AngleDifference; //Stores the angle difference between the desired 180 degree turn slide re-direction and the player's current direction
        protected float deltaTransform = 0; //Stores the player's positional delta

		#endregion

        #region Properties

		/// <summary>
		/// Gets or sets a value indicating whether this
		/// <see cref="AntonCoolpecker.Abstract.Player.States.AbstractStandingState"/> should interpolate gravity scale y.
		/// </summary>
		/// <value><c>true</c> if interpolating gravity scale y; otherwise, <c>false</c>.</value>
        public bool interpolateGravityScaleY { get { return m_InterpolateGravityScaleY; } set { m_InterpolateGravityScaleY = value; } }

		/// <summary>
		/// Gets or sets a value indicating whether this
		/// <see cref="AntonCoolpecker.Abstract.Player.States.AbstractStandingState"/> use exponential angle calculation
		/// for the ground interpolation.
		/// </summary>
		/// <value><c>true</c> if use exponential angle calculate; otherwise, <c>false</c>.</value>
		public bool useExponentialAngleCalc { get { return m_UseExponentialAngleCalc; } set { m_UseExponentialAngleCalc = value; } }

		/// <summary>
		/// Gets or sets the linear ground interpolation multiplier.
		/// </summary>
		/// <value>The linear calculate interpolation multiplier.</value>
		public float linearCalcInterpolationMultiplier { get { return m_LinearCalcInterpolationMultiplier; } set { m_LinearCalcInterpolationMultiplier = value; } }

		/// <summary>
		/// Gets or sets the linear ground interpolation angle minimum.
		/// </summary>
		/// <value>The linear calculate angle limit x.</value>
		public float linearCalcAngleLimitX { get { return m_LinearCalcAngleLimitX; } set { m_LinearCalcAngleLimitX = value; } }

		/// <summary>
		/// Gets or sets the linear calculate angle limit y.
		/// </summary>
		/// <value>The linear calculate angle limit y.</value>
		public float linearCalcAngleLimitY { get { return m_LinearCalcAngleLimitY; } set { m_LinearCalcAngleLimitY = value; } }

        #endregion

        #region Override Methods

        /// <summary>
        /// when valid input is detected, store it.
        /// </summary>
        /// <param name="desiredMovement"></param>
        /// <param name="player"></param>
        protected virtual void HandleInputStorage(Vector3 desiredMovement,AbstractPlayerController player)
        {
            //If the desired movement is not zero, then store the movement vector
            if (desiredMovement != Vector3.zero)
            {
                m_StorageTimer.maxTime = m_TimeToStoreInput;
                m_StorageTimer.Reset();
                m_StoredInput = desiredMovement.normalized;

                //To prevent sliding when turning away after walking into a wall, calculate the change in position
                deltaTransform = HydraMathUtils.Abs((m_lastPosition - player.transform.position).magnitude);
                m_lastPosition = player.transform.position;
            }
        }

        /// <summary>
        /// 	Handles the movement.
        /// </summary>
        protected override void HandleMovement(MonoBehaviour parent)
        {
            AbstractPlayerController player = GetPlayerController(parent);

            Vector3 desiredMovement = GetMovementVector(parent);
            HandleInputStorage(desiredMovement, player);

            if (m_StorageTimer.complete)
            {
                m_StoredInput = Vector3.zero;
                m_lastPosition = player.transform.position;
                deltaTransform = 0;
            }

            if ((m_UseSlideTurn) && ((m_Sliding&&Vector3.Angle(desiredMovement.normalized,m_InitialSlideForward) >= (180 - m_SlideTurnAngleTolerance)) ||
                ((!m_UseSlideMinimumDeltaPositon || deltaTransform >=m_MinimumTransformDelta) &&
                (Vector3.Angle(m_StoredInput,player.transform.forward) >= (180 - m_SlideTurnAngleTolerance)))))
            {               
                if (!m_Sliding)
                {
                    //First frame of the slide
                    m_Sliding = true;
                    m_InitialSlideVelocity = player.characterLocomotor.flatVelocity;
                    m_InitialSlideDesieredMovement = desiredMovement.normalized;
                    m_SlideRotationTimer.maxTime = m_SlideRotationTurnTime;
                    m_SlideVelocityTimer.maxTime = m_SlideVelocityTurnTime;
                    m_InitialSlideForward = player.transform.forward;
                    m_SlideRotationTimer.Reset();
                    m_SlideVelocityTimer.Reset();
                    m_AngleDifference = Vector3.Angle(desiredMovement.normalized, player.transform.forward);
                }

                ResolveRotation(player, desiredMovement);
                player.characterLocomotor.flatVelocity = Vector3.Lerp(m_InitialSlideVelocity,m_InitialSlideDesieredMovement,
                    m_SlideVelocityTimer.elapsedAsPercentage);
				
                if (m_SlideRotationTimer.complete && m_SlideVelocityTimer.complete)
                    m_Sliding = false;
            }
            else
            {
				if (m_Sliding)
                	m_Sliding = false;

                RotateTowardsMovementVector(player, desiredMovement);
                Vector3 deltaV = ResolveVelocityDelta(player, desiredMovement, player.characterLocomotor.velocity);
                player.characterLocomotor.velocity += deltaV;
            }
        }

        /// <summary>
        /// 	Rotates towards movement vector.
        /// </summary>
        /// <param name="player">Player.</param>
        /// <param name="movement">Movement.</param>
        protected override void RotateTowardsMovementVector(AbstractPlayerController player, Vector3 movement)
        {
            Quaternion lookRotation = Quaternion.LookRotation(player.transform.forward);

            if (movement == Vector3.zero)
                return;

            Quaternion targetRotation = Quaternion.LookRotation(movement);
            float delta = player.rotateSpeed * rotateSpeedMultiplier * GameTime.deltaTime;

            //If sliding, then rotate so we hit the target at the end of the slide timer
			if (m_Sliding && m_UseSlideTurn)
                player.transform.rotation = Quaternion.RotateTowards(Quaternion.LookRotation(m_InitialSlideForward),
					Quaternion.LookRotation(m_InitialSlideDesieredMovement), m_AngleDifference * m_SlideRotationTimer.elapsedAsPercentage);
            else
                player.transform.rotation = Quaternion.RotateTowards(lookRotation, targetRotation, delta);
        }

        /// <summary>
        /// 	Returns a state for transition. Return self if no transition.
        /// </summary>
        /// <returns>The next state.</returns>
        /// <param name="parent">Parent.</param>
        public override AbstractPlayerState GetNextState(MonoBehaviour parent)
		{
			AbstractPlayerController player = GetPlayerController(parent);

			if (!player.characterLocomotor.isSoftGrounded || player.isInWater)
			{
                if (!player.isInWater)
                {
                    return m_MidairState;
                }
				return m_SwimmingState;
			}

			if (AbstractSlidingState.ShouldStartSliding(parent))
				return m_SlidingState;

			return base.GetNextState(parent);
		}

		/// <summary>
		/// 	Called when the state becomes active.
		/// </summary>
		public override void OnEnter(MonoBehaviour parent)
		{
			base.OnEnter(parent);

            m_SlideRotationTimer= new Timer();
            m_SlideVelocityTimer= new Timer();
            m_StorageTimer = new Timer();
            m_StoredInput = Vector3.zero;
            m_Sliding = false;
			m_LinearCalcAngleLimit.x = m_LinearCalcAngleLimitX;
			m_LinearCalcAngleLimit.y = m_LinearCalcAngleLimitY;

			AbstractPlayerController player = GetPlayerController(parent);
			player.airTracker.Reset();

            m_lastPosition = player.transform.position;

			if (m_InterpolateGravityScaleY) 
			{
				//player.characterLocomotor.ResetGravityScale();
				//m_OverrideGravityScaleY = player.characterLocomotor.gravityScaleY;
				//player.characterLocomotor.ResetGravityScale();
				m_UseLinearCalc = true;
			}
		}

		/// <summary>
		/// 	Called when the state becomes active.
		/// </summary>
		public override void OnExit(MonoBehaviour parent)
		{
			base.OnExit (parent);

			AbstractPlayerController player = GetPlayerController(parent);

			if (m_InterpolateGravityScaleY) 
			{
				if (player.characterLocomotor.gravityScaleY != player.characterLocomotor.originalGravityScale.y)
					setGravityScaleBack (player, player.characterLocomotor.originalGravityScale.y);

				m_UseLinearCalc = false;
			}
		}

		/// <summary>
		/// 	Called when the parent updates.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnUpdate(MonoBehaviour parent)
		{
			base.OnUpdate(parent);

			AbstractPlayerController player = GetPlayerController(parent);

            fixSkinWidthOverlap(player);
            
            if (m_StorageTimer == null || m_SlideVelocityTimer == null || m_SlideRotationTimer == null)
            {
                m_SlideRotationTimer = new Timer();
                m_SlideVelocityTimer = new Timer();
                m_StorageTimer = new Timer();
                m_StoredInput = Vector3.zero;
                m_Sliding = false;
                m_lastPosition = player.transform.position;
            }

			if (AntonCoolpecker.Concrete.Configuration.Controls.Mapping.InputMapping.jumpInput.GetButtonDown ()) 
			{
                m_Sliding = false;

                if (m_InterpolateGravityScaleY) 
				{
					setGravityScaleBack (player, player.characterLocomotor.originalGravityScale.y);
					m_UseLinearCalc = false;
				}

				m_MidairState.SetAnimatorState ("Jump");
				Jump (player);
			}
		}

		/// <summary>
		/// 	Called when the parent updates.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public override void OnFixedUpdate(MonoBehaviour parent)
		{
			base.OnFixedUpdate(parent);

			AbstractPlayerController player = GetPlayerController(parent);

			//*GROUND INTERPOLATION
			if (m_InterpolateGravityScaleY) 
			{
				if (m_UseLinearCalc) 
				{
					RaycastHit hitInfo;
					Vector3 rayPosition = new Vector3(player.transform.position.x, (player.transform.position.y + player.collider.bounds.size.y - 0.1f), player.transform.position.z)
                        + player.characterLocomotor.velocity*Time.fixedDeltaTime;
					Vector3 direction = Vector3.down;
					float distance = m_GravityRayMaxDistance; //Mathf.Infinity;
                    LayerMask layerToIgnore = 1 << 4; //IGNORE WATER LAYER
					layerToIgnore |= 1 << 12; //IGNORE PLAYER LAYER
					bool canDo = Physics.Raycast(rayPosition, direction, out hitInfo, distance, ~layerToIgnore);
					float surfaceAngle = player.characterLocomotor.originalGravityScale.y;

					if (canDo) 
					{
						if (hitInfo.collider.gameObject.tag == "Mushroom")
							m_UseLinearCalc = false;

						surfaceAngle = Vector3.Angle (hitInfo.normal, Vector3.up);
					}

					float angleCalc;

					if (m_UseExponentialAngleCalc)
						angleCalc = (surfaceAngle * surfaceAngle);
					else
						angleCalc = surfaceAngle;

					if (surfaceAngle >= m_LinearCalcAngleLimit.x && surfaceAngle <= m_LinearCalcAngleLimit.y)
						player.characterLocomotor.gravityScaleY = (player.characterLocomotor.originalGravityScale.y * angleCalc * m_LinearCalcInterpolationMultiplier);
					else
						player.characterLocomotor.gravityScaleY = player.characterLocomotor.originalGravityScale.y;
				}

                if (m_Sliding && m_UseSlideEdgeDetection)
                {
                    LayerMask layerToIgnore = 1 << 4; //IGNORE WATER LAYER
                    layerToIgnore |= 1 << 12; //IGNORE PLAYER LAYER
                    layerToIgnore |= 1 << 18; //IGNORE ATTACK HITBOX LAYER
                    RaycastHit hitInfo2;
                    Vector3 rayPos = new Vector3(player.transform.position.x, (player.transform.position.y + player.collider.bounds.size.y - 0.1f), player.transform.position.z)
                            + player.characterLocomotor.velocity * Time.fixedDeltaTime;
                    bool ledgeUndetected = Physics.Raycast(rayPos, Vector3.down, out hitInfo2, m_SlideTurnEdgeDetectionDistance, ~layerToIgnore);

                    if (!ledgeUndetected)
                    {
                        player.characterLocomotor.flatVelocity = Vector3.zero;
                        m_Sliding = false;
                    }
                }

                if (!m_UseLinearCalc) 
				{
					player.characterLocomotor.gravityScaleY = player.characterLocomotor.originalGravityScale.y;
				}
			}
		}

		#endregion

		#region Private Functions

		/// <summary>
		/// Due to CharacterController.skinWidth, characters will sometimes appear to be floating up to skinWidth distance above the ground.
		/// To fix this, first we adjust the size and vertical position of the collider to account for the skinWidth.
		/// This makes it so most of the time the character is standing on the ground, but sometimes they will sink in a bit.
		/// Then, we have to call this method to correct the sinkage.
		/// 
		/// See: http://answers.unity3d.com/questions/25334/charactercontrollerskinwidth-and-vertical-displace.html
		/// </summary>
		private void fixSkinWidthOverlap(AbstractPlayerController player)
		{
			RaycastHit hit = player.characterLocomotor.GroundedCast();
			float displacement = player.characterLocomotor.skinWidth - hit.distance;
			if (hit.collider != null && displacement > 0 &&
				displacement > player.characterLocomotor.characterController.contactOffset)
				player.characterLocomotor.characterController.Move(Vector3.up * displacement);
		}

		/// <summary>
		/// Sets the gravity scale back to it's original value.
		/// </summary>
		/// <param name="player">Player.</param>
		/// <param name="orggravscale">Original gravity scale.</param>
		private void setGravityScaleBack(AbstractPlayerController player, float orggravscale)
		{
			if (!m_InterpolateGravityScaleY)
				return;

			player.characterLocomotor.gravityScaleY = orggravscale;
		}

        #endregion
    }
}