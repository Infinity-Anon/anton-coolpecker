﻿using AntonCoolpecker.Concrete.HitBoxes;
using UnityEngine;

namespace AntonCoolpecker.Abstract.Player.States
{
	/// <summary>
	/// 	Abstract attack state - Base class for all attack states.
	/// </summary>
	public abstract class AbstractAttackState : AbstractPlayerState
	{
		#region Variables

		public const string CLONE_SUFFIX = "(Clone)"; //Used for finding the hitbox instance

		[SerializeField] private HitBox m_HitBoxPrefab;
		[SerializeField] private int m_Damage = 2;

		#endregion

		#region Properties

		/// <summary>
		/// 	Gets or sets the hit box prefab.
		/// </summary>
		/// <value>The hit box prefab.</value>
		public HitBox hitBoxPrefab { get { return m_HitBoxPrefab; } set { m_HitBoxPrefab = value; } }

		/// <summary>
		/// 	Gets or sets the damage.
		/// </summary>
		/// <value>The damage.</value>
		public int damage { get { return m_Damage; } set { m_Damage = value; } }

		#endregion

		#region Methods

		/// <summary>
		/// 	Gets the hit box. Instantiates it if it does not exist.
		/// </summary>
		/// <returns>The hit box instance.</returns>
		/// <param name="parent">Parent.</param>
		public HitBox GetHitBox(MonoBehaviour parent)
		{
			Transform child = parent.transform.Find(m_HitBoxPrefab.name + CLONE_SUFFIX);
			if (child != null)
				return child.GetComponent<HitBox>();

			Vector3 position = parent.transform.position + parent.transform.rotation * m_HitBoxPrefab.transform.position;
			Quaternion rotation = parent.transform.rotation * m_HitBoxPrefab.transform.rotation;

			HitBox output = Instantiate(m_HitBoxPrefab, position, rotation) as HitBox;
			output.transform.parent = parent.transform;

			return output;
		}

		#endregion
	}
}