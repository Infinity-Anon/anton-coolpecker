﻿using AntonCoolpecker.Abstract.Player.States;
using AntonCoolpecker.Abstract.Player.States.Hitstun;
using AntonCoolpecker.Abstract.StateMachine;
using AntonCoolpecker.API.Damageable;
using AntonCoolpecker.API.Listeners.Player;
using AntonCoolpecker.Concrete;
using AntonCoolpecker.Concrete.Cameras;
using AntonCoolpecker.Concrete.Collectables;
using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using AntonCoolpecker.Concrete.Forces;
using AntonCoolpecker.Concrete.Menus.HUD;
using AntonCoolpecker.Concrete.Messaging;
using AntonCoolpecker.Concrete.Platforms;
using AntonCoolpecker.Concrete.Player;
using AntonCoolpecker.Concrete.Progress;
using AntonCoolpecker.Concrete.StateMachine;
using AntonCoolpecker.Concrete.Utils;
using AntonCoolpecker.Utils;
using AntonCoolpecker.Utils.Time;
using Hydra.HydraCommon.Abstract;
using Hydra.HydraCommon.Extensions;
using Hydra.HydraCommon.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AntonCoolpecker.Abstract.Player
{
	//TODO: Implement a better calculation that takes the lerping value from the beginning of the jump to the apex in account accurately

	/// <summary>
	/// 	AbstractPlayerController is the base class for all player controllers.
	/// </summary>
	[DisallowMultipleComponent]
	[RequireComponent(typeof(CharacterLocomotor))]
	public abstract class AbstractPlayerController : HydraMonoBehaviour, IDamageable, IPlayerListener
	{
		#region Variables

		//* REFERENCES
		[SerializeField] private PlayerStateMachine m_StateMachine;
		[SerializeField] private AbstractPlayerState m_DeathState;
        [SerializeField] private PlayerCutsceneState m_CutSceenState;

		//* BASIC VALUES
		[SerializeField] private float m_Acceleration; //Acceleration of the player
		[SerializeField] private float m_IceAcceleration; //Acceleration of the player when on ice
		[SerializeField] private float m_MaxSpeed; //Max speed of the player
		[SerializeField] private float m_JumpHeight = 2.0f; //Maximum kump height when the player does a normal jump
		//TODO: MOVE THIS VARIABLE BELOW TO THE ANTON STATES/SCRIPTS - THIS IS UNRELATED TO COOLPECKER AND THE PLAYER AS A WHOLE
		[SerializeField] private float m_bounceHeight = 1.5f; //Maximum bounce height.
		[SerializeField] private float m_RotateSpeed; //Hortizontal player rotation speed.
		[SerializeField] private float m_RunThreshold; //Running threshold(Magnitude from 0 to 1)

		//* JUMP ACCELERATION/DECELERATION VALUES
		[SerializeField] private bool m_UseJumpAccelDecel = true; //Should jump acceleration/decelration be used?
		[SerializeField] private bool m_Skip_Lerp2From_Use_Current_Instead = false; //Should we use next gravity scale value directly instead of lerping to it?
		[SerializeField] private bool m_StabilizeJump = false; //Stabilize the jump acceleration/deceleration
		[SerializeField] private float m_JumpAccelLerp1From = 3f; //Gravity scale to lerp from when starting the jump
		[SerializeField] private float m_JumpAccelLerp1To = 3f; //Gravity scale to lerp to when reaching the apex of the jump
		[SerializeField] private float m_JumpAccelLerp1Speed = 3f; //How fast the lerp from start of jump to the apex of the jump should occur
		[SerializeField] private float m_JumpAccelLerp2From = 3f; //Gravity scale to lerp from after apex of jump has been reached
		[SerializeField] private float m_JumpAccelLerp2To = 3f; //Gravity scale to lerp to when reaching the end of the jump(landing)
		[SerializeField] private float m_JumpAccelLerp2Speed= 3f; //How fast the lerp from apex of jump to the end of the jump(landing) should occur

		//* COLLECTABLE VALUES
		[SerializeField] private float m_CollectablePullRadius = 1.5f; //Minimum proximity before an collectible is automatically pulled to the player

		//* CACHE
		private CharacterLocomotor m_CachedCharacterLocomotor;
		private List<Renderer> m_CachedChildRenderers;
		private List<Projector> m_CachedChildProjectors;

		//* PRIVATE JUMP ACCELERATION/DECELERATION BOOLEANS
		private bool m_HasPlayerJumped = false; //Has the player jumped?
		private bool m_HasReachedApex = false; //Has the player reached jumping apex?
		private bool m_DoJumpStabilization = false; //Keeps track whether to utilize jump stabilization or not

		//* ETC.
		private bool m_IsInWater; //Is the player in water?
		private AerialActionTracker m_AirTracker; //Keeps track of the player's air actions

		private CameraController m_PlayerCamera; //Camera controller for the player's camera
		private WaterTrigger m_WaterTrigger; //Reference to the water script in the water mesh that the player is mainly intersecting.

		private List<Collectable> m_SearchCollectables; //Current list of collectables within proximity of the player

		private PlayerProgress m_Progress; //Cache the PlayerProgress, so we don't have to look up Main all the time

		private static bool m_ShowDebugUI = true; //Should the Debug GUI be shown?

		private List<int> m_CollidingWaters = new List<int>(); //Keeps track of the IDs of the currently intersecting water meshes.

		#endregion

		#region Properties

		/// <summary>
		/// 	Gets the state machine.
		/// </summary>
		/// <value>The state machine.</value>
		public PlayerStateMachine stateMachine { get { return m_StateMachine; } }

		/// <summary>
		/// Gets the state to transition to upon death.
		/// </summary>
		/// <value>Death state.</value>
		public AbstractPlayerState deathState { get { return m_DeathState; } }

		/// <summary>
		/// 	Gets the character locomotor.
		/// </summary>
		/// <value>The character locomotor.</value>
		public CharacterLocomotor characterLocomotor
		{
			get { return m_CachedCharacterLocomotor ?? (m_CachedCharacterLocomotor = GetComponent<CharacterLocomotor>()); }
		}

		/// <summary>
		/// 	Gets or sets the player camera.
		/// </summary>
		/// <value>The player camera.</value>
		public CameraController playerCamera { get { return m_PlayerCamera; } set { m_PlayerCamera = value; } }

		/// <summary>
		/// 	Gets or sets the water trigger script(Set if the player is colliding with water).
		/// </summary>
		/// <value>The water trigger script.</value>
		public WaterTrigger waterTrigger { get { return m_WaterTrigger; } set { m_WaterTrigger = value; } }

		/// <summary>
		/// 	Gets the air tracker.
		/// </summary>
		/// <value>The air tracker.</value>
		public AerialActionTracker airTracker { get { return m_AirTracker; } }

		/// <summary>
		/// 	Gets or sets the acceleration.
		/// </summary>
		/// <value>The acceleration.</value>
		[Tweakable("Movement")]
		public float iceAcceleration { get { return m_IceAcceleration; } set { m_IceAcceleration = value; } }

		/// <summary>
		/// 	Gets or sets the acceleration.
		/// </summary>
		/// <value>The acceleration.</value>
		[Tweakable("Movement")]
		public float acceleration
		{
			get
			{
				Collider c = characterLocomotor.GroundedCast().collider;
				Ice ice = c ? c.GetComponent<Ice>() : null;

				if (ice && ice.pointIsIcy(characterLocomotor.GroundedCast().point))
					return m_IceAcceleration;
				else
					return m_Acceleration;
			}
			set { m_Acceleration = value; }
		}

		/// <summary>
		/// 	Gets or sets the max speed.
		/// </summary>
		/// <value>The max speed.</value>
		[Tweakable("Movement")]
		public float maxSpeed { get { return m_MaxSpeed; } set { m_MaxSpeed = value; } }

		/// <summary>
		/// 	Gets or sets the height of the jump.
		/// </summary>
		/// <value>The height of the jump.</value>
		[Tweakable("Jump")]
		public float jumpHeight { get { return m_JumpHeight; } set { m_JumpHeight = value; } }

		/// <summary>
		/// 	Gets or sets the height of the bounce.
		/// </summary>
		/// <value>The height of the bounce.</value>
		[Tweakable("Bounce")]
		public float bounceHeight { get { return m_bounceHeight; } set { m_bounceHeight = value; } }

		/// <summary>
		/// 	Gets or sets the rotate speed, in degrees.
		/// </summary>
		/// <value>The rotate speed.</value>
		[Tweakable("Movement")]
		public float rotateSpeed { get { return m_RotateSpeed; } set { m_RotateSpeed = value; } }

		/// <summary>
		/// 	Gets or sets a value indicating whether this AbstractPlayerController is in water or not.
		/// </summary>
		/// <value><c>true</c> if is in water; otherwise, <c>false</c>.</value>
		public bool isInWater { get { return m_IsInWater; } set { m_IsInWater = value; } }

		/// <summary>
		/// 	Is the player dead?
		/// </summary>
		/// <value><c>true</c> if player is dead; otherwise, <c>false</c>.</value>
		public bool isDead { get { return stateMachine.activeState is AbstractDeadState; } }

		/// <summary>
		/// A value between 0.0 and 1.0, representing at what magnitude of the input vector should the player start running.
		/// It's mirrored to the (-1.0;1.0) range
		/// This is to aid turning in place.
		/// </summary>
		/// <value>The run threshold.</value>
		[Tweakable("Movement")]
		public float runThreshold { get { return m_RunThreshold; } set { m_RunThreshold = value; } }

		/// <summary>
		/// 	Gets the center of the character's physics capsule, in world space.
		/// </summary>
		/// <value>The centroid.</value>
		public Vector3 centroid { get { return transform.TransformPoint(characterLocomotor.characterController.center); } }

		/// <summary>
		/// Gets the player's progress(From the Main prefab if initially null).
		/// </summary>
		/// <value>The progress.</value>
		public PlayerProgress progress
		{
			get
			{
				if (m_Progress == null) 
				{
					m_Progress = FindObjectOfType<Main>().playerProgress;
				}

				return m_Progress;
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether this
		/// <see cref="AntonCoolpecker.Abstract.Player.AbstractPlayerController"/> 
		/// should use jump acceleration/deceleration or not.
		/// </summary>
		/// <value><c>true</c> if using jump accel/decel; otherwise, <c>false</c>.</value>
		[Tweakable("JumpAccelDecel")]
		public bool useJumpAccelDecel { get { return m_UseJumpAccelDecel; } set { m_UseJumpAccelDecel = value; } }

		/// <summary>
		/// Gets or sets a value indicating whether this
		/// <see cref="AntonCoolpecker.Abstract.Player.AbstractPlayerController"/> 
		/// should not use lerping and instead go directly to next lerp value when
		/// reaching a certain point in the jump(Start, apex, land).
		/// </summary>
		/// <value><c>true</c> if skip lerp2 from use current value instead; otherwise, <c>false</c>.</value>
		[Tweakable("JumpAccelDecel")]
		public bool skipLerp2FromUseCurrentValueInstead { get { return m_Skip_Lerp2From_Use_Current_Instead; } set { m_Skip_Lerp2From_Use_Current_Instead = value; } }

		//NOTE: CURRENTLY ONLY RECOMMENDED FOR DEBUGGING JUMP VALUES WITH A SOMEWHAT CONSISTENT JUMP FORCE IN-GAME!
		/// <summary>
		/// Gets or sets a value indicating whether this
		/// <see cref="AntonCoolpecker.Abstract.Player.AbstractPlayerController"/> should 
		/// stabilize the jump when utilizing jump acceleration/deceleration
		/// (Due to inconsistencies with jump height).
		/// </summary>
		/// <value><c>true</c> if stabilize jump; otherwise, <c>false</c>.</value>
		[Tweakable("JumpAccelDecel")]
		public bool stabilizeJump { get { return m_StabilizeJump; } set { m_StabilizeJump = value; } }

		/// <summary>
		/// Gets or sets the first jump accel/decel lerp value(Start of jump)
		/// </summary>
		/// <value>The jump accel lerp1 from.</value>
		[Tweakable("JumpAccelDecel")]
		public float jumpAccelLerp1From { get { return m_JumpAccelLerp1From; } set { m_JumpAccelLerp1From = value; } }

		/// <summary>
		/// Gets or sets the second jump accel/decel lerp value(Reached apex of jump)
		/// </summary>
		/// <value>The jump accel lerp 1 to.</value>
		[Tweakable("JumpAccelDecel")]
		public float jumpAccelLerp1To { get { return m_JumpAccelLerp1To; } set { m_JumpAccelLerp1To = value; } }

		/// <summary>
		/// Gets or sets how fast the first part of the jump accel/decel lerp should occur(Value 1 to Value 2).
		/// </summary>
		/// <value>The jump accel lerp 1 speed.</value>
		[Tweakable("JumpAccelDecel")]
		public float jumpAccelLerp1Speed { get { return m_JumpAccelLerp1Speed; } set { m_JumpAccelLerp1Speed = value; } }

		/// <summary>
		/// Gets or sets the third jump accel/decel lerp value(After apex of jump)
		/// </summary>
		/// <value>The jump accel lerp 2 from.</value>
		[Tweakable("JumpAccelDecel")]
		public float jumpAccelLerp2From { get { return m_JumpAccelLerp2From; } set { m_JumpAccelLerp2From = value; } }

		/// <summary>
		/// Gets or sets the fourth jump accel/decel lerp value(Before end of jump)
		/// </summary>
		/// <value>The jump accel lerp 2 to.</value>
		[Tweakable("JumpAccelDecel")]
		public float jumpAccelLerp2To { get { return m_JumpAccelLerp2To; } set { m_JumpAccelLerp2To = value; } }

		/// <summary>
		/// Gets or sets how fast the second part of the jump accel/decel lerp should occur(Value 3 to Value 4).
		/// </summary>
		/// <value>The jump accel lerp 2 speed.</value>
		[Tweakable("JumpAccelDecel")]
		public float jumpAccelLerp2Speed { get { return m_JumpAccelLerp2Speed; } set { m_JumpAccelLerp2Speed = value; } }

		#endregion

		#region Messages

		/// <summary>
		/// 	Called when the component is enabled.
		/// </summary>
		protected override void OnEnable()
		{
			base.OnEnable();

			if (m_AirTracker == null)
				m_AirTracker = new AerialActionTracker();

			if (m_SearchCollectables == null)
				m_SearchCollectables = new List<Collectable>();

			if (m_CachedChildRenderers == null)
				m_CachedChildRenderers = new List<Renderer>();

			if (m_CachedChildProjectors == null)
				m_CachedChildProjectors = new List<Projector>();
		}

		/// <summary>
		/// 	Called once every frame.
		/// </summary>
		protected override void Update()
		{
			base.Update();

			m_StateMachine.Update(this);

			if (AntonCoolpecker.Concrete.Configuration.Controls.Mapping.InputMapping.toggleDebugUI.GetButtonDown())
				m_ShowDebugUI = !m_ShowDebugUI;

			if (AntonCoolpecker.Concrete.Configuration.Controls.Mapping.InputMapping.switchControllerInput.GetButtonDown())
				AntonCoolpecker.Concrete.Configuration.Controls.Mapping.InputMapping.SwitchInputJoystick();
		}

		/// <summary>
		/// 	Called every physics timestep.
		/// </summary>
		protected override void FixedUpdate()
		{
			base.FixedUpdate();

			JumpAccelDecel();

			//PullCollectables();

			m_StateMachine.FixedUpdate(this);

			CustomWaterEnabler();
		}

		/// <summary>
		/// 	Called after all Updates have finished.
		/// </summary>
		protected override void LateUpdate()
		{
			base.LateUpdate();

			m_StateMachine.LateUpdate(this);
		}

		/// <summary>
		/// 	Called when the Damageable gets damaged.
		/// </summary>
		/// <param name="damage">Amount of damage to apply.</param>
		/// <param name="collision">Collision info.</param>
		public virtual void Damage(int damage, CharacterLocomotorCollisionData collision)
		{
			if (m_StateMachine.activeState is AbstractHitstunState || m_StateMachine.activeState is AbstractDeadState)
				return;

			PlayerMessages.BroadcastOnDamage(this, collision);
			
			if (PlayerStatistics.elements[0] != null && damage > 0)
				PlayerStatistics.PlayerCollectableStatistics[CollectableType.Health] -= damage;

			Knockback(collision);

			if (PlayerStatistics.PlayerCollectableStatistics[CollectableType.Health] <= 0)
			{
				Die(collision);
			}
			else
			{
				GetStunned(collision);
			}
		}

		/// <summary>
		/// 	Called when an jump event is fired.
		/// </summary>
		/// <param name="jumpEven">Jump event.</param>
		public void OnJump()
		{
			if (m_UseJumpAccelDecel) 
			{
				m_HasPlayerJumped = true;
				m_HasReachedApex = false;
				m_DoJumpStabilization = true;
			}
		}
			
		/// <summary>
		/// Called when the player enters a cutscene.
		/// </summary>
		/// <param name="CutsceneID">Cutscene ID.</param>
		/// <param name="scene">Scene.</param>
        public void EnterCutscene(int CutsceneID,string scene)
        {
            if (progress.GetCurrentLevelProgress(scene).flags[CutsceneID] == AntonCoolpecker.Concrete.Progress.CollectedStatus.Uncollected)
            {
                progress.GetCurrentLevelProgress(scene).SetPermanentFlag(CutsceneID);
                m_StateMachine.SetActiveState(m_CutSceenState, this);
            }
        }

		/// <summary>
		/// Called when the player exits a cutscene(End of cutscene, by default).
		/// </summary>
        public void ExitCutscene()
        {
            m_CutSceenState.Transition = true;
        }

		/// <summary>
		/// Called when the state is changed.
		/// </summary>
		/// <param name="changeInfo">Change info.</param>
        public void OnStateChange(StateChangeInfo<AbstractPlayerState> changeInfo) {}

		/// <summary>
		/// Called when the player gets damaged/hit.
		/// </summary>
		/// <param name="collision">The collision info.</param>
		public void OnDamage(CharacterLocomotorCollisionData collision) {}

		/// <summary>
		/// Called when the player changes playable character.
		/// </summary>
		public void OnSwapEnter()
        {
            PlayerStatistics.elements[0].SwapPortraits();
            PlayerStatistics.elements[0].ForceShow();
        }

		/// <summary>
		/// Called when an player-related animation event is fired.
		/// </summary>
		/// <param name="animationEvent">Animation event.</param>
		public virtual void OnAnimationEvent(AnimationEvent animationEvent) {}

		/// <summary>
		/// 	OnControllerColliderHit is called when the CharacterController hits a collider while performing a Move.
		/// </summary>
		/// <param name="hit">Hit.</param>
		protected override void OnControllerColliderHit(ControllerColliderHit hit)
		{
			base.OnControllerColliderHit(hit);

			m_StateMachine.OnControllerColliderHit(hit, this);

			if (hit.gameObject.tag == "Mushroom") 
			{
				m_CachedCharacterLocomotor.velocity = hit.transform.TransformDirection (Vector3.forward * HydraMathUtils.Clamp (m_CachedCharacterLocomotor.velocity.magnitude * m_bounceHeight, 5, 20));
				hit.gameObject.GetComponent<Animator>().SetTrigger ("Touch");
			}
			else
				return;
		}

		/// <summary>
		/// 	OnTriggerEnter is called when the Collider other enters the trigger.
		/// </summary>
		/// <param name="other">Other.</param>
		protected override void OnTriggerEnter(Collider other)
		{
			base.OnTriggerEnter(other);

			if (other.GetComponent<WaterTrigger>() != null) 
			{
				m_WaterTrigger = other.GetComponent<WaterTrigger>();
				m_CollidingWaters.Add (other.GetComponent<WaterTrigger>().GetInstanceID());
			}
		}

		/// <summary>
		/// 	OnTriggerEnter is called when the Collider other enters the trigger.
		/// </summary>
		/// <param name="other">Other.</param>
		protected override void OnTriggerStay(Collider other)
		{
			base.OnTriggerStay(other);

			if (other.GetComponent<WaterTrigger> () != null) 
			{
				if (other.GetComponent<WaterTrigger> ().GetInstanceID() == m_CollidingWaters[(m_CollidingWaters.Count - 1)])
					other.GetComponent<WaterTrigger> ().ignorePlayer = false;
				else
					other.GetComponent<WaterTrigger> ().ignorePlayer = true;

				m_WaterTrigger = other.GetComponent<WaterTrigger> ();
			}
		}

		/// <summary>
		/// 	OnTriggerExit is called when the Collider other has stopped touching the trigger.
		/// </summary>
		/// <param name="other">Other.</param>
		protected override void OnTriggerExit(Collider other)
		{
			base.OnTriggerExit(other);

			if (other.GetComponent<WaterTrigger>() != null) 
			{
				m_CollidingWaters.Remove(other.GetComponent<WaterTrigger>().GetInstanceID());
			}

			if (m_CollidingWaters.Count <= 0)
				m_WaterTrigger = null;
		}

		/// <summary>
		/// 	OnCollisionEnter is called
		/// </summary>
		/// <param name="other">Other.</param>
		protected override void OnCollisionEnter(Collision collision)
		{
			// Debug-draw all contact points and normals
			//foreach (ContactPoint contact in collision.contacts)
			//Debug.DrawRay(contact.point, contact.normal, Color.white);

			print("Now in contact with " + collision.transform.name);

			if (characterLocomotor.rigidy != null)
			{
				characterLocomotor.rigidy.velocity = Vector3.zero;
			}
		}

		private void OnCollisionStay(Collision collision)
		{
			//foreach (ContactPoint contact in collision.contacts)
			//Debug.DrawRay(contact.point, contact.normal, Color.white);

			if (characterLocomotor.rigidy != null)
			{
				characterLocomotor.rigidy.velocity = Vector3.zero;
				characterLocomotor.rigidy.velocity = characterLocomotor.rigidy.velocity * 0.0001f;
			}
		}

		private void OnCollisionExit(Collision collision)
		{
			print("No longer in contact with " + collision.transform.name);

			if (characterLocomotor.rigidy != null)
			{
				characterLocomotor.rigidy.velocity = Vector3.zero;
			}
		}

		/// <summary>
		/// 	Called to draw to the GUI.
		/// </summary>
		protected void OnGUI()
		{
			//base.OnGUI();

			if (!m_ShowDebugUI)
				return;

			Vector3 screenPosition = Camera.main.WorldToScreenPoint(centroid);
			GUI.Label(new Rect(screenPosition.x, Screen.height - screenPosition.y, 256, 32), stateMachine.activeState.name);

			Color oldColor = GUI.color;
			Color groundedColor = characterLocomotor.isGrounded ? Color.green : Color.red;
			Color softGroundedColor = characterLocomotor.isSoftGrounded ? Color.green : Color.red;

			GUI.color = groundedColor;
			GUI.Label(new Rect(screenPosition.x, (Screen.height - screenPosition.y) + 16, 256, 32), "isGrounded");

			GUI.color = softGroundedColor;
			GUI.Label(new Rect(screenPosition.x, (Screen.height - screenPosition.y) + 32, 256, 32), "isSoftGrounded");

			GUI.color = oldColor;
		}

		#endregion

		#region Methods

		/// <summary>
		/// 	Shows the instance.
		/// </summary>
		public void Show()
		{
			Show(true);
		}

		/// <summary>
		/// 	Hides the instance.
		/// </summary>
		public void Hide()
		{
			Show(false);
		}

		/// <summary>
		/// 	Sets the state of any visual components.
		/// </summary>
		/// <param name="show">If set to <c>true</c> show.</param>
		public void Show(bool show)
		{
			transform.FindAllRecursive(m_CachedChildRenderers);
			transform.FindAllRecursive(m_CachedChildProjectors);

			for (int index = 0; index < m_CachedChildRenderers.Count; index++)
				m_CachedChildRenderers[index].enabled = show;

			for (int index = 0; index < m_CachedChildProjectors.Count; index++)
				m_CachedChildProjectors[index].enabled = show;
		}

		/// <summary>
		/// 	Returns true when the user is using movement input.
		/// </summary>
		/// <returns><c>true</c> if this instance has user input; otherwise, <c>false</c>.</returns>
		public static bool HasUserInput()
		{
			float horizontal = InputMapping.horizontalInput.GetAxis();
			float vertical = InputMapping.verticalInput.GetAxis();

			if (!HydraMathUtils.Approximately(horizontal, 0.0f))
				return true;

			if (!HydraMathUtils.Approximately(vertical, 0.0f))
				return true;

			return false;
		}

		/// <summary>
		/// 	Returns true when the user is using movement input(Raw input data, no smoothing).
		/// </summary>
		/// <returns><c>true</c> if this instance has user input; otherwise, <c>false</c>.</returns>
		public static bool HasUserInputRaw()
		{
			float horizontal = InputMapping.horizontalInput.GetAxisRaw();
			float vertical = InputMapping.verticalInput.GetAxisRaw();

			if (horizontal == 0f && vertical == 0f)
				return false;

			return true;
		}

		/// <summary>
		/// Respawns the player. Split off into a coroutine so it can wait for the transition animation to finish.
		/// The value of WaitForSeconds can be adjusted appropriately.
		/// </summary>
		public virtual IEnumerator Respawn()
		{
			yield return new WaitForSeconds (3.0f);

			m_StateMachine.SetActiveState(m_StateMachine.initialState, this);
			PlayerStatistics.PlayerCollectableStatistics [CollectableType.Health] = PlayerStatistics.PlayerCollectableStatistics.maxHealth;

			// AntonCoolpecker.Concrete.Scene.SceneManager.instance is undefined for some reason if you load a scene directly
			// But works if a scene is loaded by Unity (menu or level transitions)
			try 
			{
				this.transform.Copy(AntonCoolpecker.Concrete.Scene.SceneManager.instance.spawnPoint);
			}
			catch (System.NullReferenceException)
			{
				Debug.LogFormat("Caught NullReferenceException in Respawn()");
			}

			UnityEngine.SceneManagement.Scene currentScene = UnityEngine.SceneManagement.SceneManager.GetActiveScene ();
			m_Progress.GetCurrentLevelProgress (currentScene.name).ResetAll ();
			playerCamera.Reset ();

			TransitionSystem.transitionSystem.transitionIn ();
		}

		/// <summary>
		/// Handles transition to GameOver scene. Split off into a coroutine so it can wait for the transition animation to finish.
		/// The value of WaitForSeconds can be adjusted appropriately.
		/// </summary>
		/// <returns>The over.</returns>
		public virtual IEnumerator GameOver()
		{
			TransitionSystem.transitionSystem.transitionOut ();

			yield return new WaitForSeconds (2.0f);

			UnityEngine.SceneManagement.SceneManager.LoadScene ("GameOver"); //TODO: maybe this is ugly
			PlayerStatistics.PlayerCollectableStatistics.lives = 3; //TODO: GetCurrentMaxLives() or something

			TransitionSystem.transitionSystem.transitionIn();
		}

		/// <summary>
		/// Wrapper for Die() in case the collision data is needed.
		/// </summary>
		protected virtual void Die(CharacterLocomotorCollisionData collision)
		{
			Die();
		}

		/// <summary>
		/// Play death animation, then respawn or game over.
		/// </summary>
		protected virtual void Die()
		{
			// possible refactor: make SceneManager.SpawnPlayer()
			m_StateMachine.SetActiveState(deathState, this);

			PlayerStatistics.PlayerCollectableStatistics.lives = PlayerStatistics.PlayerCollectableStatistics.lives - 1;

			if (PlayerStatistics.PlayerCollectableStatistics.lives == 0) 
			{
				UnityEngine.SceneManagement.Scene currentScene = UnityEngine.SceneManagement.SceneManager.GetActiveScene ();
				m_Progress.GetCurrentLevelProgress (currentScene.name).ResetAll ();
				StartCoroutine(GameOver());
			} 
			else 
			{
				TransitionSystem.transitionSystem.transitionOut();
				StartCoroutine(Respawn());
			}
		}

		/// <summary>
		/// Called when the player gets stunned by damage/hit.
		/// </summary>
		/// <param name="collision">Collision.</param>
		protected virtual void GetStunned(CharacterLocomotorCollisionData collision)
		{
			//TODO: Make the player get knocked away from the enemy, rather than away from the collision point?

			AbstractHitstunState hitstunState = m_StateMachine.activeState.GetHitstunState();
			hitstunState.source = collision.point;
			m_StateMachine.SetActiveState(hitstunState, this);
		}

		/// <summary>
		/// Wrapper for two-argument Knockback(). Workaround for a race condition occurring in Damage()
		/// </summary>
		/// <param name="collision">Collision data.</param>
		protected virtual void Knockback(CharacterLocomotorCollisionData collision)
		{
			try 
			{
				Knockback(collision, 
					m_StateMachine.activeState.GetHitstunState().hitstunForceHorizontal, 
					m_StateMachine.activeState.GetHitstunState().hitstunForceVertical);
			}
			catch 
			{
				Knockback(collision, 2.0f, 2.0f);
			}
		}
			
		/// <summary>
		/// Apply a knockback effect away from the collision point.
		/// </summary>
		/// <param name="collision">Collision data.</param>
		/// <param name="forceScale">Scaling constant.</param>
		protected virtual void Knockback(CharacterLocomotorCollisionData collision, float forceScaleXZ, float forceScaleY)
		{
			Vector3 knockbackDirection = (transform.position - collision.point);
			knockbackDirection.y = 0;

			//On flat surfaces, continue going in same direction
			if (knockbackDirection.magnitude < 0.1f)
				knockbackDirection = characterLocomotor.flatVelocity;

			//Keep the horizontal direction, but angle 30° up
			knockbackDirection.y = Mathf.Sqrt(knockbackDirection.sqrMagnitude/3);

			//If horizontally still when landing on hitbox, then randomize the direction with reasonable values to knock the player away
			if (knockbackDirection == Vector3.zero)
			{
				while (knockbackDirection.x == 0.0f)
					knockbackDirection.x = Random.Range (-0.5f, 0.5f);

				knockbackDirection.y = Random.Range (0.2f, 0.5f);

				while (knockbackDirection.z == 0.0f)
					knockbackDirection.z = Random.Range (-0.5f, 0.5f);
			}

			knockbackDirection.Normalize();

			//Ignore pre-existing velocity when you get stunned
			characterLocomotor.velocity = new Vector3 (knockbackDirection.x * forceScaleXZ,
				knockbackDirection.y * forceScaleY,
				knockbackDirection.z * forceScaleXZ);
		}

		/// <summary>
		/// 	Calculates the jump force necessary for this character to reach a certain jump height.
		/// </summary>
		/// <returns>The necessary force.</returns>
		/// <param name="jumpHeight">Jump height.</param>
		public float JumpForce(float jumpHeight)
		{
			if (m_UseJumpAccelDecel)
				characterLocomotor.gravityScaleY = m_JumpAccelLerp1From;
			
			return Mathf.Sqrt(jumpHeight * -characterLocomotor.gravity.y * 2f);
		}

		#endregion

		#region Private Methods/Functions

		/// <summary>
		/// 	Pulls the nearby collectables.
		/// </summary>
//		private void PullCollectables()
//		{
//			m_SearchCollectables.Clear();
//			FindCollectables(centroid, m_CollectablePullRadius, m_SearchCollectables);
//
//			for (int index = 0; index < m_SearchCollectables.Count; index++)
//				m_SearchCollectables[index].SetPullPoint(centroid);
//		}

		/// <summary>
		/// 	Checks if the player should transfer to swimming state or return to original state.
		/// </summary>
		private void CustomWaterEnabler()
		{
			if (m_WaterTrigger != null)
			{
				if (!m_WaterTrigger.AllowFloatState (this.collider, Vector3.up, Mathf.Infinity, 1f))
					m_IsInWater = false;

				else
					m_IsInWater = true;
			}

			if (m_WaterTrigger == null)
				m_IsInWater = false;
		}

		/// <summary>
		/// 	Finds the collectables.
		/// </summary>
		/// <param name="origin">Origin.</param>
		/// <param name="radius">Radius.</param>
		/// <param name="output">Output.</param>
		private void FindCollectables(Vector3 origin, float radius, List<Collectable> output)
		{
			Collider[] colliders = Physics.OverlapSphere(origin, radius);

			for (int index = 0; index < colliders.Length; index++)
			{
				Collider collider = colliders[index];

				Collectable collectible = collider.GetComponent<Collectable>();
				if (collectible == null)
					continue;

				output.Add(collectible);
			}
		}

		/// <summary>
		/// Detects whether a keyboard or a controller/joytick is currently used
		/// </summary>
		/// <returns><c>true</c> if using a joystick or a controller, <c>false</c> if using a keyboard.</returns>
		private bool keyboardFalseJoystickTrue()
		{
			//CHECK FOR ANY CONNECTED CONTROLLERS - ELSE JUST USE KEYBOARD
			int noOfConnectedControllers = Input.GetJoystickNames().Length;
			if (noOfConnectedControllers <= 0)
				return false;

			//CHECK FOR JOYSTICK INPUT
			if (Input.GetAxisRaw ("Controller Horizontal") != 0f || Input.GetAxisRaw ("Controller Vertical") != 0f)
				return true;

			if (Input.GetAxisRaw ("Controller Camera x") != 0f || Input.GetAxisRaw("Controller Camera y") != 0f)
				return true;

			int maxControllerButtons = 12;
			for (int i = 0; i < maxControllerButtons; i++) 
			{
				//if (Input.GetButton ("Controller Button " + i + "") == null)
				//break;

				if (Input.GetButton("Controller Button " + i + ""))
					return true;
			}

			//AFTER NO ACTIVITY FROM ANY JOYSTICKS, JUST USE KEYBOARD
			return false;
		}
			
		/// <summary>
		/// Called when the player has landed.
		/// </summary>
		private void HasLanded()
		{
			//characterLocomotor.ResetGravityScale ();
			characterLocomotor.gravityScaleY = characterLocomotor.originalGravityScale.y;
			m_HasPlayerJumped = false;
			m_HasReachedApex = false;
		}

		/// <summary>
		/// Called when the player has reached the apex of a jump
		/// </summary>
		private void ReachedApex() 
		{
			if (!m_Skip_Lerp2From_Use_Current_Instead)
				characterLocomotor.gravityScaleY = m_JumpAccelLerp2From;

			m_HasReachedApex = true; 
		}

		/// <summary>
		/// Called when stabilizing the jump acceleration/deleceration
		/// by compensating the velocity of the player.
		/// </summary>
		/// <param name="difference">Difference.</param>
		private void AddVelocity(float difference)
		{
			Vector3 direction = Vector3.up;
			float jumpUp = 0f;

			if (difference < 0) 
			{
				jumpUp = Mathf.Sqrt (jumpHeight * -(Physics.gravity.y * -difference) * 2f);
				characterLocomotor.velocity -= direction.normalized * jumpUp;
			}

			if (difference > 0) 
			{
				jumpUp = Mathf.Sqrt (jumpHeight * -(Physics.gravity.y * difference) * 2f);
				characterLocomotor.velocity += direction.normalized * jumpUp;
			}

			m_DoJumpStabilization = false;
		}

		/// <summary>
		/// Handles the jump acceleration/deceleration.
		/// </summary>
		private void JumpAccelDecel()
		{
			if (m_UseJumpAccelDecel) 
			{
				if (m_HasPlayerJumped) 
				{
					if (!m_HasReachedApex) 
					{
						if (characterLocomotor.velocity.y <= 0f)
							ReachedApex ();

						float beforeLerp = characterLocomotor.gravityScaleY;

						characterLocomotor.gravityScaleY = Mathf.Lerp (characterLocomotor.gravityScaleY, m_JumpAccelLerp1To, m_JumpAccelLerp1Speed * GameTime.fixedDeltaTime);

						float afterLerp = characterLocomotor.gravityScaleY;

						float diff = afterLerp - beforeLerp;

						if (m_DoJumpStabilization && m_StabilizeJump)
							AddVelocity (diff);
					}

					if (m_HasReachedApex) 
					{
						characterLocomotor.gravityScaleY = Mathf.Lerp (characterLocomotor.gravityScaleY, m_JumpAccelLerp2To, m_JumpAccelLerp2Speed * GameTime.fixedDeltaTime);
					}
				}

				if (characterLocomotor.isGrounded && m_HasPlayerJumped && m_HasReachedApex) 
				{
					HasLanded ();
				}
			}
		}

		#endregion
	}
}