﻿using AntonCoolpecker.Abstract.Enemies.States;
using AntonCoolpecker.Abstract.StateMachine;
using AntonCoolpecker.API.Listeners.Enemies;
using Hydra.HydraCommon.Abstract;
using UnityEngine;

namespace AntonCoolpecker.Abstract.Listeners.Enemies
{
	//TODO: AbstractAnimationListener for stuff shared between AbstractPlayerAnimationListener and AbstractEnemyAnimationListener?

	/// <summary>
	/// Abstract enemy animation listener.
	/// </summary>
	[DisallowMultipleComponent]
	public class AbstractEnemyAnimationListener : HydraMonoBehaviour, IEnemyListener
	{
		#region Variables

		[SerializeField] private Animator m_AnimatorAsset;
		[SerializeField] private RuntimeAnimatorController m_AnimatorController;

		private Animator m_AnimatorInstance;

		#endregion

		#region Properties

		/// <summary>
		/// 	Gets the Animator instance.
		/// </summary>
		/// <value>The Animator instance.</value>
		/// 
		/// TODO: Investigate if animator needs to be instantiated dynamically
		/// 	  Until then, just use the animator asset
		public Animator animator { get { return m_AnimatorInstance ?? (m_AnimatorInstance = m_AnimatorAsset); } }

		#endregion

		#region Public Methods

		/// <summary>
		/// Called whenever the enemy changes state.
		/// </summary>
		/// <param name="changeInfo">Change info.</param>
		public virtual void OnStateChange(StateChangeInfo<AbstractEnemyState> changeInfo)
		{
			PlayAnimation(changeInfo.current.animatorState, false);
		}

		/// <summary>
		/// Raises the animation event event.
		/// </summary>
		/// <param name="animationEvent">Animation event.</param>
		public virtual void OnAnimationEvent(AnimationEvent animationEvent) {}

		/// <summary>
		/// 	Plays the animation.
		/// </summary>
		/// <param name="name">Name.</param>
		/// <param name="immediate">If set to <c>true</c> immediate.</param>
		public void PlayAnimation(string name, bool immediate)
		{
			if (string.IsNullOrEmpty(name))
				return;

			if (immediate)
				animator.CrossFade(name, 0.0f, -1, 0.0f);
			else
				animator.CrossFade(name, 0.0f);
		}

		/// <summary>
		/// Raises the damage event whenever the enemy gets damaged/hit.
		/// </summary>
		/// <param name="collision">Collision.</param>
		public void OnDamage(Concrete.Utils.CharacterLocomotorCollisionData collision) {}

		#endregion
	}
}