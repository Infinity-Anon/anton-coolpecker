﻿using AntonCoolpecker.Abstract.Audio.Enemies;
using AntonCoolpecker.Abstract.StateMachine;
using AntonCoolpecker.Abstract.Enemies.States;
using AntonCoolpecker.API.Listeners.Enemies;
using AntonCoolpecker.Concrete.Enemies;
using AntonCoolpecker.Concrete.Utils;
using Hydra.HydraCommon.Abstract;
using Hydra.HydraCommon.PropertyAttributes;
using Hydra.HydraCommon.Utils.Audio;
using System.Collections;
using UnityEngine;

namespace AntonCoolpecker.Abstract.Listeners.Enemies
{
	/// <summary>
	/// Abstract enemy audio listener.
	/// </summary>
	[DisallowMultipleComponent]
	[RequireComponent(typeof(EnemyController))]
	public abstract class AbstractEnemyAudioListener : HydraMonoBehaviour, IEnemyListener
	{
		#region Variables

		[SerializeField] private int m_LowerSemitone; //Sets the minimum randomized semitone when a SFX is played.
		[SerializeField] private int m_UpperSemitone; //Sets the maximum randomized semitone when a SFX is played.

		private EnemyController m_CachedEnemyController;

		#endregion

		#region Properties

		/// <summary>
		/// 	Gets the player controller.
		/// </summary>
		/// <value>The player controller.</value>
		public EnemyController enemy
		{
			get { return m_CachedEnemyController ?? (m_CachedEnemyController = GetComponent<EnemyController>()); }
		}

		/// <summary>
		/// 	Gets the sound effects.
		/// </summary>
		/// <value>The sound effects.</value>
		public abstract AbstractEnemySfx sfx { get; }

		#endregion

		#region Messages

		/// <summary>
		/// 	Called when the state changes.
		/// </summary>
		/// <param name="changeInfo">Change info.</param>
		public virtual void OnStateChange(StateChangeInfo<AbstractEnemyState> changeInfo) {}

		/// <summary>
		/// 	Called when an animation event is fired.
		/// </summary>
		/// <param name="animationEvent">Animation event.</param>
		public virtual void OnAnimationEvent(AnimationEvent animationEvent) {}

		/// <summary>
		/// Raises the damage event.
		/// </summary>
		/// <param name="collision">Collision.</param>
		public void OnDamage(CharacterLocomotorCollisionData collision) {}

		#endregion

		#region Methods

		/// <summary>
		/// 	OneShot a random clip from the array.
		/// </summary>
		/// <param name="extends">Extends.</param>
		/// <param name="clips">Clips.</param>
		/// <param name="volumeScale">Volume scale.</param>
		public void PlayOneShot(SoundEffectAttribute[] soundEffects)
		{
			int index = Random.Range(0, (soundEffects.Length - 1));
			SoundEffectAttribute soundEffect = soundEffects[index];

			float pitch = AudioSourceUtils.IncreaseSemitones(1.0f, Random.Range(m_LowerSemitone, m_UpperSemitone));
			AudioSource source = AudioSourcePool.PlayOneShot(gameObject, soundEffect.audioClip, soundEffect.volumeScale, pitch);
			source.spatialBlend = 1.0f;
		}

		#endregion
	}
}