﻿using AntonCoolpecker.Abstract.Player;
using AntonCoolpecker.Abstract.Player.States;
using AntonCoolpecker.Abstract.StateMachine;
using AntonCoolpecker.API.Listeners.Player;
using AntonCoolpecker.Concrete;
using AntonCoolpecker.Concrete.Configuration.Controls.Mapping;
using AntonCoolpecker.Concrete.Messaging;
using AntonCoolpecker.Concrete.Utils;
using AntonCoolpecker.Utils.Time;
using Hydra.HydraCommon.Abstract;
using Hydra.HydraCommon.EventArguments;
using Hydra.HydraCommon.Extensions;
using Hydra.HydraCommon.Utils;
using UnityEngine;

namespace AntonCoolpecker.Abstract.Listeners.Player
{
	/// <summary>
	/// Abstract player animation listener.
	/// </summary>
	[DisallowMultipleComponent]
	[RequireComponent(typeof(AbstractPlayerController))]
	public class AbstractPlayerAnimationListener : HydraMonoBehaviour, IPlayerListener
	{
		#region Variables

		[SerializeField] private Animator m_AnimatorAsset;
		[SerializeField] private RuntimeAnimatorController m_AnimatorController;

		private Animator m_AnimatorInstance;
		private AbstractPlayerController m_CachedPlayerController;
		private AnimationEventReceiver m_EventReceiver;

		private bool m_Init; //Has the animator been initialized?

		#endregion

		#region Properties

		/// <summary>
		/// 	Gets the Animator instance.
		/// </summary>
		/// <value>The Animator instance.</value>
		public Animator animator { get { return m_AnimatorInstance; } }

		/// <summary>
		/// 	Gets the player controller.
		/// </summary>
		/// <value>The player controller.</value>
		public AbstractPlayerController player
		{
			get { return m_CachedPlayerController ?? (m_CachedPlayerController = GetComponent<AbstractPlayerController>()); }
		}

		#endregion

		#region Messages

		/// <summary>
		/// 	Called when the component is enabled.
		/// </summary>
		protected override void OnEnable()
		{
			base.OnEnable();

			if (m_AnimatorInstance == null)
				InstantiateAnimator();

			Subscribe();
		}

		/// <summary>
		/// 	Called when the component is disabled.
		/// </summary>
		protected override void OnDisable()
		{
			base.OnDisable();

			Unsubscribe();
		}

		/// <summary>
		/// 	Called once every frame.
		/// </summary>
		protected override void Update()
		{
			base.Update();

			if (m_Init == false)
			{
				m_Init = true;
				return;
			}

			Vector3 inputVector = new Vector3(InputMapping.horizontalInput.GetAxisRaw(), 0.0f, InputMapping.verticalInput.GetAxisRaw());
			float inputMagnitude = HydraMathUtils.Clamp(inputVector.magnitude, 0, 1);

            if (player.stateMachine.activeState.disabledControls)
                animator.SetFloat("InputMagnitude", 0);
            else
			    animator.SetFloat ("InputMagnitude", inputMagnitude);
           
            if (player.characterLocomotor.isGrounded)
				animator.SetBool("GroundedTrigger", true);
			if (!player.characterLocomotor.isGrounded)
				animator.SetBool("GroundedTrigger", false);

			if (player.isInWater)
				animator.SetBool("WaterTrigger", true);
			if (!player.isInWater)
				animator.SetBool("WaterTrigger", false);

			if (player.characterLocomotor.velocity.y <= 0f && player.characterLocomotor.isGrounded && animator.GetBool("JumpTrigger"))
				animator.SetBool("JumpTrigger", false);
			
            if (player.stateMachine.activeState is AbstractSprintState)
            {
                AbstractSprintState sprintState = player.stateMachine.activeState as AbstractSprintState;
                animator.speed = sprintState.AnimationSpeedDefault + (sprintState.AnimationSpeedAddition * InputMapping.sprintInput.GetAxisRaw());
            }
        }

		/// <summary>
		/// 	Called when the object is destroyed.
		/// </summary>
		protected override void OnDestroy()
		{
			base.OnDestroy();

			DestroyAnimator();
		}

		/// <summary>
		/// 	Called when the player jumps.
		/// </summary>
		public virtual void OnJump()
		{
			animator.Play("Jump", -1, 0f);
			animator.SetBool("JumpTrigger", true);
		}

		/// <summary>
		/// 	Called when the state changes.
		/// </summary>
		/// <param name="changeInfo">Change info.</param>
		public virtual void OnStateChange(StateChangeInfo<AbstractPlayerState> changeInfo)
		{
			PlayAnimation(changeInfo.current.animatorState, false);

            if (changeInfo.previous is AbstractSprintState && changeInfo.current is AbstractStandingState)
            {
                animator.SetBool("Sprint", false);
                AbstractSprintState sprintstate = changeInfo.previous as AbstractSprintState;
                animator.CrossFade("Sprint",sprintstate.GetAnimationExitTime, 0, animator.GetCurrentAnimatorStateInfo(0).normalizedTime);
            }

            if (changeInfo.current is AbstractSprintState)
                animator.SetBool("Sprint", true);

            if (!(changeInfo.current is AbstractMidairState) && (player.characterLocomotor.isGrounded || player.isInWater)) 
				animator.SetBool("JumpTrigger", false);

			//If we're changing character in water, make sure the PC doesn't fade into previous animation(Midair animation, that is)
			if (changeInfo.previous is AbstractSwimmingState)
				return;

			if (changeInfo.current is AbstractMidairState && !animator.GetBool("JumpTrigger")) 
			{
				animator.Play (changeInfo.previous.animatorState, -1, 0f);
				animator.CrossFade (changeInfo.current.animatorState, 0.1f);
			}
		}

		/// <summary>
		/// 	Called when the player is damaged.
		/// </summary>
		/// <param name="collision">The collision info.</param>
		public virtual void OnDamage(CharacterLocomotorCollisionData collision)
		{
			//TODO: There will be a damage animation
			PlayAnimation("Midair", false);
		}

		/// <summary>
		/// 	Called when we swap to the player.
		/// </summary>
		public virtual void OnSwapEnter()
		{
			animator.Rebind ();
			PlayAnimation(player.stateMachine.activeState.animatorState, true);
		}

		/// <summary>
		/// 	Called when an animation event is fired.
		/// </summary>
		/// <param name="animationEvent">Animation event.</param>
		public virtual void OnAnimationEvent(AnimationEvent animationEvent)
		{
			if (animationEvent.stringParameter == "Footstep")
				Footstep();
		}

		#endregion

		#region Methods

		/// <summary>
		/// 	Plays the animation.
		/// </summary>
		/// <param name="name">Name.</param>
		/// <param name="immediate">If set to <c>true</c> immediate.</param>
		public void PlayAnimation(string name, bool immediate)
		{
			if (string.IsNullOrEmpty(name))
				return;

			// Don't play falling animation if we jumped.
			if (player.stateMachine.activeState is AbstractMidairState && IsPlayingAnimation("Jump"))
				return;

			if (immediate)
				animator.CrossFade(name, 0.0f, -1, 0.0f);
			else
				animator.CrossFade(name, 0.0f);
		}

		/// <summary>
		/// 	Determines whether this instance is playing the specified animation.
		/// </summary>
		/// <returns><c>true</c> if this instance is playing the specified animation; otherwise, <c>false</c>.</returns>
		/// <param name="name">Name.</param>
		public bool IsPlayingAnimation(string name)
		{
			return IsPlayingAnimation(0, name);
		}

		/// <summary>
		/// 	Determines whether this instance is playing the specified animation.
		/// </summary>
		/// <returns><c>true</c> if this instance is playing the specified animation; otherwise, <c>false</c>.</returns>
		/// <param name="layer">Layer.</param>
		/// <param name="name">Name.</param>
		public bool IsPlayingAnimation(int layer, string name)
		{
			AnimatorClipInfo[] clipInfos = animator.GetCurrentAnimatorClipInfo(layer);

			for (int index = 0; index < clipInfos.Length; index++)
			{
				AnimatorClipInfo clipInfo = clipInfos[index];
				if (clipInfo.clip.name == name)
					return true;
			}

			return false;
		}

		#endregion

		#region Private Functions

		/// <summary>
		/// 	Makes a footstep.
		/// </summary>
		private void Footstep()
		{
			RaycastHit hit = player.characterLocomotor.GroundedCast();

			if (hit.collider == null)
				return;

			SurfaceInfo.Step(hit.collider.gameObject, gameObject);
		}

		/// <summary>
		/// 	Subscribes to the animation events.
		/// </summary>
		private void Subscribe()
		{
			m_EventReceiver.onAnimationEventCallback += OnAnimationEvent;
		}

		/// <summary>
		/// 	Unsubscribes from the animation events.
		/// </summary>
		private void Unsubscribe()
		{
			m_EventReceiver.onAnimationEventCallback -= OnAnimationEvent;
		}

		/// <summary>
		/// 	Called when an animation event is fired.
		/// </summary>
		private void OnAnimationEvent(object sender, EventArg<AnimationEvent> args)
		{
			PlayerMessages.BroadcastOnAnimationEvent(player, args.data);
		}

		/// <summary>
		/// 	Instantiates the animator.
		/// </summary>
		private void InstantiateAnimator()
		{
			if (!ObjectUtils.LazyInstantiate(m_AnimatorAsset, ref m_AnimatorInstance))
				return;

			m_AnimatorInstance.gameObject.SetLayerRecursive(gameObject.layer);

			m_Init = false;

			m_AnimatorInstance.transform.parent = transform;
			m_AnimatorInstance.runtimeAnimatorController = m_AnimatorController;

			m_EventReceiver = m_AnimatorInstance.gameObject.AddComponent<AnimationEventReceiver>();

			// Stops the animation when the game pauses
			m_AnimatorInstance.gameObject.AddComponent<AnimatorGameTimeSubscriber>();
		}

		/// <summary>
		/// 	Destroys the animator.
		/// </summary>
		private void DestroyAnimator()
		{
			m_AnimatorInstance = ObjectUtils.SafeDestroyGameObject(m_AnimatorInstance);
			m_EventReceiver = ObjectUtils.SafeDestroyGameObject(m_EventReceiver);
		}

		#endregion
	}
}