﻿using AntonCoolpecker.Abstract.Audio.Player;
using AntonCoolpecker.Abstract.Player;
using AntonCoolpecker.Abstract.Player.States;
using AntonCoolpecker.Abstract.StateMachine;
using AntonCoolpecker.API.Listeners.Player;
using AntonCoolpecker.Concrete.Configuration;
using AntonCoolpecker.Concrete.Utils;
using Hydra.HydraCommon.Abstract;
using Hydra.HydraCommon.PropertyAttributes;
using Hydra.HydraCommon.Utils.Audio;
using UnityEngine;

namespace AntonCoolpecker.Abstract.Listeners.Player
{
	/// <summary>
	/// Abstract player audio listener.
	/// </summary>
	[DisallowMultipleComponent]
	[RequireComponent(typeof(AbstractPlayerController))]
	public abstract class AbstractPlayerAudioListener : HydraMonoBehaviour, IPlayerListener
	{
		#region Variables

		[SerializeField] private int m_LowerSemitone; //Sets the minimum randomized semitone when a SFX is played.
		[SerializeField] private int m_UpperSemitone; //Sets the maximum randomized semitone when a SFX is played.

		private AbstractPlayerController m_CachedPlayerController;

		#endregion

		#region Properties

		/// <summary>
		/// 	Gets the player controller.
		/// </summary>
		/// <value>The player controller.</value>
		public AbstractPlayerController player
		{
			get { return m_CachedPlayerController ?? (m_CachedPlayerController = GetComponent<AbstractPlayerController>()); }
		}

		/// <summary>
		/// 	Gets the sound effects.
		/// </summary>
		/// <value>The sound effects.</value>
		public abstract AbstractPlayerSFX sfx { get; }

		#endregion

		#region Messages

		/// <summary>
		/// 	Called when the player jumps.
		/// </summary>
		public virtual void OnJump()
		{
			PlayOneShot(sfx.jump);
		}

		/// <summary>
		/// 	Called when the state changes.
		/// </summary>
		/// <param name="changeInfo">Change info.</param>
		public virtual void OnStateChange(StateChangeInfo<AbstractPlayerState> changeInfo) {}

		/// <summary>
		/// 	Called when the player is damaged.
		/// </summary>
		/// <param name="collision">The collision info.</param>
		public virtual void OnDamage(CharacterLocomotorCollisionData collision)
		{
			PlayOneShot(sfx.hurt);
		}

		/// <summary>
		/// 	Called when we swap to the player.
		/// </summary>
		public virtual void OnSwapEnter() {}

		/// <summary>
		/// 	Called when an animation event is fired.
		/// </summary>
		/// <param name="animationEvent">Animation event.</param>
		public virtual void OnAnimationEvent(AnimationEvent animationEvent) {}

		#endregion

		#region Methods

		/// <summary>
		/// 	OneShot a random clip from the array.
		/// </summary>
		/// <param name="extends">Extends.</param>
		/// <param name="clips">Clips.</param>
		/// <param name="volumeScale">Volume scale.</param>
		public void PlayOneShot(SoundEffectAttribute[] soundEffects)
		{
			int index = Random.Range(0, (soundEffects.Length - 1));
			SoundEffectAttribute soundEffect = soundEffects[index];

			float pitch = AudioSourceUtils.IncreaseSemitones(1.0f, Random.Range(m_LowerSemitone, m_UpperSemitone));

			AudioSource source = AudioSourcePool.PlayOneShot(gameObject, soundEffect.audioClip, soundEffect.volumeScale, pitch);
			source.volume = Options.soundFXVolume * 0.01f;
			source.spatialBlend = 1.0f;
		}

		#endregion
	}
}