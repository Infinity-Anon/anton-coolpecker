﻿using Hydra.HydraCommon.Abstract;
using Hydra.HydraCommon.Utils;
using Hydra.HydraCommon.Utils.Shapes._3d;
using System.Collections.Generic;
using UnityEngine;

namespace AntonCoolpecker
{
	/// <summary>
	/// 	DiggableMesh provides methods for calculating the diggable area of a mesh, e.g. a piece of ground.
	/// </summary>
	[DisallowMultipleComponent]
	[RequireComponent(typeof(MeshCollider))]
	public class DiggableMesh : HydraMonoBehaviour
	{
		#region Variables

		[SerializeField] private float m_MaxSteepness = 0.5f;

		private Triangle3d[] m_TempMeshTriangles;
		private Line3d[] m_TempTriangleLines;

		private MeshCollider m_CachedMeshCollider;
		private List<Triangle3d> m_CachedTriangles;
		private Dictionary<Line3d, Triangle3d> m_CachedEdges;

		#endregion

		#region Properties

		/// <summary>
		/// 	Gets the mesh collider.
		/// </summary>
		/// <value>The mesh collider.</value>
		public MeshCollider meshCollider
		{
			get { return m_CachedMeshCollider ?? (m_CachedMeshCollider = GetComponent<MeshCollider>()); }
		}

		/// <summary>
		/// 	Gets or sets the max steepness. This value determines which faces of the mesh are diggable
		/// 	based on incline. 0.0f faces upwards, 1.0f is a vertical wall.
		/// </summary>
		/// <value>The max steepness.</value>
		public float maxSteepness { get { return m_MaxSteepness; } set { m_MaxSteepness = ValidateMaxSteepness(value); } }

		#endregion

		#region Messages

		/// <summary>
		/// 	Called when the component is enabled.
		/// </summary>
		protected override void OnEnable()
		{
			base.OnEnable();

			if (m_CachedTriangles == null)
				m_CachedTriangles = new List<Triangle3d>();

			if (m_CachedEdges == null)
				m_CachedEdges = new Dictionary<Line3d, Triangle3d>();
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// 	Gets the valid triangles for digging.
		/// </summary>
		/// <param name="output">Output.</param>
		private void GetTriangles(List<Triangle3d> output)
		{
			Mesh mesh = meshCollider.sharedMesh;
			if (mesh == null)
				return;

			MeshUtils.ToTriangles(mesh, transform.worldToLocalMatrix, ref m_TempMeshTriangles);

			for (int index = 0; index < m_TempMeshTriangles.Length; index++)
			{
				Triangle3d triangle = m_TempMeshTriangles[index];
				Vector3 normal = triangle.normal;

				float dot = Vector3.Dot(normal, Vector3.up);
				float dotDelta = 1.0f - dot;

				if (dotDelta > m_MaxSteepness)
					continue;

				output.Add(triangle);
			}
		}

		/// <summary>
		/// 	Gets the edges of the diaggable shell.
		/// </summary>
		/// <param name="triangles">Triangles.</param>
		/// <param name="output">Output.</param>
		private void GetEdges(List<Triangle3d> triangles, Dictionary<Line3d, Triangle3d> output)
		{
			for (int index = 0; index < triangles.Count; index++)
			{
				Triangle3d triangle = triangles[index];
				triangle.GetLines(ref m_TempTriangleLines);

				for (int edgeIndex = 0; edgeIndex < m_TempTriangleLines.Length; edgeIndex++)
				{
					Line3d edge = m_TempTriangleLines[edgeIndex];

					// Find matching edge
					Line3d? foundOther = null;
					foreach (KeyValuePair<Line3d, Triangle3d> pair in m_CachedEdges)
					{
						Line3d otherEdge = pair.Key;
						if (edge.IsEqualUnordered(otherEdge))
						{
							foundOther = otherEdge;
							break;
						}
					}

					if (foundOther != null)
					{
						output.Remove((Line3d)foundOther);
						continue;
					}

					output.Add(edge, triangle);
				}
			}
		}

		#endregion

		#region Static Methods

		/// <summary>
		/// 	Validates the max steepness.
		/// </summary>
		/// <returns>The max steepness.</returns>
		/// <param name="steepness">Steepness.</param>
		public static float ValidateMaxSteepness(float steepness)
		{
			return HydraMathUtils.Clamp(steepness, 0.0f, 1.0f);
		}

		#endregion
	}
}