﻿using System.Collections;
using UnityEngine;

/// <summary>
/// Billboard class.
/// </summary>
public class Billboard : MonoBehaviour
{
	#region Variables

	private Camera m_Camera;

	#endregion

	#region Methods

	/// <summary>
	/// Start this instance.
	/// </summary>
	void Start ()
    {
        m_Camera = Camera.main;
	}
	
	/// <summary>
	/// Update this instance.
	/// </summary>
	void Update ()
    {
        transform.LookAt(transform.position + m_Camera.transform.rotation * Vector3.forward,
            m_Camera.transform.rotation * Vector3.up);
    }

	#endregion
}