﻿using System.Collections.Generic;
using UnityEngine;

namespace AntonCoolpecker
{
	/// <summary>
	/// Camera path.
	/// </summary>
    public class CameraPath : MonoBehaviour
    {
        public List<CameraPoint> EditorPoints = new List<CameraPoint>();    //To alow for reordeing this is a list
        [HideInInspector] public CameraPoint[] Points;                      //I think its faster to store an array of the points at runtime and it no longer needs to be edited
        //If there is a way to expose these to just the custom editor i dont know it
        [SerializeField] public int m_ID;                               //Our ID (where in the flags array is this set)
        [SerializeField] public Concrete.Collectables.Level m_Level;    //Our Level(which flag array are we in)
        [SerializeField] public bool Skipable;                          //is this skipable (NOT YET IMPLEMENTED)
        public int ID { get { return m_ID; } }
        public string Level { get { return m_Level.ToString(); } }
   
        void Awake()
        {
            Points = EditorPoints.ToArray();

            for (int i = 0; i < Points.Length; i++)
             Points[i].position +=transform.position;
        }
        //Draw gizmos when the gameobject is selected
        void OnDrawGizmosSelected()
        {
            for (int i = 0; i < EditorPoints.Count; i++)
            {
                //Need to ajust the matrix for fulstrum
                Gizmos.matrix = Matrix4x4.TRS(transform.TransformPoint( EditorPoints[i].position), EditorPoints[i].Rotation, Vector3.one);
                Gizmos.color = new Color(1, 0, 0, 0.5f);
                Gizmos.DrawSphere(Vector3.zero, 1f);
                Gizmos.color = new Color(0, 0, 1, 0.5f);
                if(EditorPoints[i].DrawFrustrum)
                    Gizmos.DrawFrustum(Vector3.zero,EditorPoints[i].FieldOfView,Camera.main.farClipPlane, 0, Camera.main.aspect);
                Gizmos.color = Color.red;
                Gizmos.matrix = Matrix4x4.TRS(transform.position,transform.rotation, Vector3.one);
                if (i < EditorPoints.Count - 1)
                    Gizmos.DrawLine(EditorPoints[i].position, (EditorPoints[i + 1].position));
            }
        }
    }

    
    [System.Serializable]
    public class CameraPoint
    {
        public Vector3 position;
        public float TimeToThisNode;
        public Quaternion Rotation;
        public float FieldOfView;
        public bool DrawFrustrum;
        public bool RequireInputToAdvance;
        public bool TimeOrSpeed;//Not Yet Implemented
        public bool InputAutoAdvance;
    }
}
