﻿using Hydra.HydraCommon.Abstract;
using UnityEngine;

namespace AntonCoolpecker.Utils
{
	/// <summary>
	/// 	Version display.
	/// </summary>
	public class VersionDisplay : HydraMonoBehaviour
	{
		#region Variables

		private static GUIStyle s_Style;

		#endregion

		#region Properties

		/// <summary>
		/// 	Gets the style.
		/// </summary>
		/// <value>The style.</value>
		public static GUIStyle style
		{
			get
			{
				if (s_Style == null)
				{
					s_Style = new GUIStyle();
					s_Style.alignment = TextAnchor.LowerRight;
					s_Style.normal.textColor = Color.blue;
				}
				return s_Style;
			}
		}

		#endregion

		#region Messages

		/// <summary>
		/// 	Called to draw to the GUI.
		/// </summary>
		protected void OnGUI()
		{
			//base.OnGUI();

			int width = Screen.width;
			int height = Screen.height;

			Rect rect = new Rect(32.0f, height - 32.0f, width - 64.0f, height * 2.0f / 100.0f);
			style.fontSize = (int)(height * 2.0f / 50.0f);

			GUI.Label(rect, VersionNumber.version.ToString(), style);
		}

		#endregion
	}
}
