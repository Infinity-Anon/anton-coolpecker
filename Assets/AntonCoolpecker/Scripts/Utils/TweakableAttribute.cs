﻿namespace AntonCoolpecker.Utils
{
	/// <summary>
	/// Add this onto any property or field that you want to be able to tweak in the Debug GUI
	/// </summary>
	[System.AttributeUsage(System.AttributeTargets.Property | System.AttributeTargets.Field)]
	public class TweakableAttribute : System.Attribute
	{
		#region Variables

		private readonly string m_Category;

		public string category { get { return m_Category; } }

		#endregion

		#region Constructors

		public TweakableAttribute(string category)
		{
			m_Category = category;
		}

		#endregion
	}
}
