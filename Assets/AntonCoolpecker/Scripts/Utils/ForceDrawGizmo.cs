﻿using UnityEngine;

namespace AntonCoolpecker.Utils
{
	/// <summary>
	/// A script to attach to empty objects to ensure they are visible in the editor.
	/// </summary>
	public class ForceDrawGizmo : MonoBehaviour
	{
		#region Functions

		/// <summary>
		/// Raises the draw gizmos event.
		/// </summary>
		private void OnDrawGizmos()
		{
			Gizmos.color = Color.blue;
			Gizmos.DrawSphere(transform.position, 0.5f);
		}

		#endregion
	}
}
