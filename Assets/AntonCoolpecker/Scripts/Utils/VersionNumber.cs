﻿using System;

namespace AntonCoolpecker.Utils
{
	/// <summary>
	/// 	Provides a version number to a project.
	/// </summary>
	public static class VersionNumber
	{
		#region Variables

		public const int MAJOR = 0;
		public const int MINOR = 0;
		public const int BUILD = 1;

		private static Version s_Version;

		#endregion

		#region Properties

		/// <summary>
		/// 	Gets the version.
		/// </summary>
		/// <value>The version.</value>
		public static Version version { get { return s_Version ?? (s_Version = new Version(MAJOR, MINOR, BUILD)); } }

		#endregion
	}
}
