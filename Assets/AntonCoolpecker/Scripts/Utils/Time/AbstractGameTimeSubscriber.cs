﻿using Hydra.HydraCommon.Abstract;
using System;

namespace AntonCoolpecker.Utils.Time
{
	/// <summary>
	/// 	AbstractGameTimeSubscriber provides abstract methods for handling
	/// 	pause/unpause functionality.
	/// </summary>
	public abstract class AbstractGameTimeSubscriber : HydraMonoBehaviour
	{
		#region Messages

		/// <summary>
		/// 	Called when the component is enabled.
		/// </summary>
		protected override void OnEnable()
		{
			base.OnEnable();

			GameTime.onPausedCallback += OnGamePausedChanged;
		}

		/// <summary>
		/// 	Called when the component is disabled.
		/// </summary>
		protected override void OnDisable()
		{
			base.OnDisable();

			GameTime.onPausedCallback -= OnGamePausedChanged;
		}

		#endregion

		#region Protected Methods

		/// <summary>
		/// 	Called when the game is paused.
		/// </summary>
		protected abstract void Pause();

		/// <summary>
		/// 	Called when the game is unpaused.
		/// </summary>
		protected abstract void Unpause();

		#endregion

		#region Private Methods

		/// <summary>
		/// 	Called when the GameTime paused state changes.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="eventArgs">Event arguments.</param>
		private void OnGamePausedChanged(object sender, EventArgs eventArgs)
		{
			if (GameTime.paused)
				Pause();
			else
				Unpause();
		}

		#endregion
	}
}
