﻿using UnityEngine;

namespace AntonCoolpecker.Utils.Time
{
	/// <summary>
	/// 	RigidbodyGameTimeSubscriber handles the pausing and unpausing of Rigidbodies.
	/// </summary>
	[RequireComponent(typeof(Rigidbody))]
	public class RigidbodyGameTimeSubscriber : AbstractGameTimeSubscriber
	{
		#region Variables

		private Vector3 m_AngularVelocity;
		private Vector3 m_Velocity;

		#endregion

		#region Override Functions

		/// <summary>
		/// 	Called when the game is paused.
		/// </summary>
		protected override void Pause()
		{
			m_AngularVelocity = rigidbody.angularVelocity;
			m_Velocity = rigidbody.velocity;

			rigidbody.angularVelocity = Vector3.zero;
			rigidbody.velocity = Vector3.zero;
		}

		/// <summary>
		/// 	Called when the game is unpaused.
		/// </summary>
		protected override void Unpause()
		{
			rigidbody.angularVelocity = m_AngularVelocity;
			rigidbody.velocity = m_Velocity;
		}

		#endregion
	}
}