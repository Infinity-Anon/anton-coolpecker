using System;

namespace AntonCoolpecker.Utils.Time
{
	/// <summary>
	/// 	GameTime is a wrapper around Unity's Time class for allowing the
	/// 	pausing and resuming of the game.
	/// </summary>
	public static class GameTime
	{
		#region Variables

		public static event EventHandler onPausedCallback;

		private static bool s_Paused;

		// The GameTime at pause
		private static float s_PausedGameTime;
		private static float s_PausedFixedGameTime;

		// The Time at pause
		private static float s_PausedTime;
		private static float s_PausedFixedTime;

		// How much we offset Time to get to GameTime
		private static float s_OffsetTime;
		private static float s_OffsetFixedTime;

		#endregion

		#region Properties

		/// <summary>
		/// 	Gets or sets a value indicating whether this <see cref="GameTime"/>
		/// 	is paused.
		/// </summary>
		/// <value><c>true</c> if paused; otherwise, <c>false</c>.</value>
		public static bool paused
		{
			get { return s_Paused; }
			set
			{
				if (value == s_Paused)
					return;

				if (value)
				{
					s_PausedGameTime = time;
					s_PausedFixedGameTime = fixedTime;

					s_PausedTime = UnityEngine.Time.time;
					s_PausedFixedTime = UnityEngine.Time.fixedTime;
				}
				else
				{
					s_OffsetTime += UnityEngine.Time.time - s_PausedTime;
					s_OffsetFixedTime += UnityEngine.Time.fixedTime - s_PausedFixedTime;
				}

				s_Paused = value;

				EventHandler handler = onPausedCallback;
				if (handler != null)
					handler(null, EventArgs.Empty);
			}
		}

		/// <summary>
		/// 	The time in seconds it took to complete the last frame.
		/// 	Returns 0 if the game is paused.
		/// </summary>
		/// <value>The delta time.</value>
		public static float deltaTime
		{
			get
			{
				if (s_Paused)
					return 0.0f;
				return UnityEngine.Time.deltaTime;
			}
		}

		/// <summary>
		/// 	The interval in seconds at which physics and other fixed frame rate updates
		/// 	(like MonoBehaviour's FixedUpdate) are performed.
		/// 	Returns 0 if the game is paused.
		/// </summary>
		/// <value>The fixed delta time.</value>
		public static float fixedDeltaTime
		{
			get
			{
				if (s_Paused)
					return 0.0f;
				return UnityEngine.Time.fixedDeltaTime;
			}
		}

		/// <summary>
		/// 	A smoothed out Time.deltaTime.
		/// 	Returns 0 if the game is paused.
		/// </summary>
		/// <value>The smooth delta time.</value>
		public static float smoothDeltaTime
		{
			get
			{
				if (s_Paused)
					return 0.0f;
				return UnityEngine.Time.smoothDeltaTime;
			}
		}

		/// <summary>
		/// 	The time of the current fixedDeltaTime step.
		/// 	This value will be lower than Time.fixedTime if the game has been paused
		/// 	for any period of time.
		/// </summary>
		/// <value>The fixed time.</value>
		public static float fixedTime
		{
			get
			{
				if (s_Paused)
					return s_PausedFixedGameTime;
				return UnityEngine.Time.fixedTime - s_OffsetFixedTime;
			}
		}

		/// <summary>
		/// 	The time at the beginning of this frame.
		/// 	This value will be lower than Time.time if the game has been paused
		/// 	for any period of time.
		/// </summary>
		/// <value>The time.</value>
		public static float time
		{
			get
			{
				if (s_Paused)
					return s_PausedGameTime;
				return UnityEngine.Time.time - s_OffsetTime;
			}
		}

		#endregion
	}
}
