﻿using System;
using UnityEngine;

namespace AntonCoolpecker.Utils
{
	/// <summary>
	/// Interpolation utility script.
	/// </summary>
	[Serializable]
	public class Interpolater
	{
		#region Variables

		[SerializeField] private Interpolant m_Interpolant;

		private const float HALF_PI = Mathf.PI / 2.0f;
		private float m_LastResult;
		public static float STOPOSOIDAL_STOP_LENGTH = 0.25f;

		public enum Interpolant
		{
			Linear = 0,
			Quadratic = 1,
			ReverseQuadratic = 2,
			Sinusoidal = 3,

			// Creates a smooth curve that's similar to a sine wave, but with sharper corners.
			// Graph it yourself, if you'd like: 0.5 - 1/(cos(pi*x)+sec(pi*x)).
			Pseudosoidal = 4,

			// Like a sine wave, but with short periods of constancy near 0 and 1.
			Stoposoidal = 5,

			// Created by nesting three sine functions. Similar to Stoposoidal and Pseudosoidal.
			Sinusinusinusoidal = 6,

			QuarterCircleUpper = 7,
			QuarterCircleLower = 8
		}

		#endregion

		#region Properties

		/// <summary>
		/// 	Gets the last result interpolated by this interpolater.
		/// </summary>
		/// <value>The last result.</value>
		public float lastResult { get { return m_LastResult; } }

		/// <summary>
		/// 	Gets or sets the interpolant used by this interpolater.
		/// </summary>
		/// <value>The interpolant.</value>
		public Interpolant interpolant { get { return m_Interpolant; } set { m_Interpolant = value; } }

		#endregion

		#region Constructors

		public Interpolater(Interpolant interpolant)
		{
			m_Interpolant = interpolant;
		}

		#endregion

		#region Public Methods

		/// <summary>
		/// 	Interpolate a float between two other floats.
		/// </summary>
		/// <returns>The interpolated float.</returns>
		/// <param name="zeroToOne">Zero to one.</param>
		/// <param name="from">From.</param>
		/// <param name="to">To.</param>
		public float Interpolate(float zeroToOne, float from, float to)
		{
			m_LastResult = Interpolate(zeroToOne, from, to, m_Interpolant);
			return m_LastResult;
		}

		/// <summary>
		/// 	Find the float between zero and one that would need to be interpolated to find a certain result.
		/// </summary>
		/// <returns>The reverse-interpolated value.</returns>
		/// <param name="value">Value.</param>
		/// <param name="from">From.</param>
		/// <param name="to">To.</param>
		public float InterpolateReverse(float value, float from, float to)
		{
			return InterpolateReverse(value, from, to, m_Interpolant);
		}

		/// <summary>
		/// 	Interpolates the derivative of the interpolant between two other floats.
		/// </summary>
		/// <returns>The interpolated derivative.</returns>
		/// <param name="zeroToOne">Zero to one.</param>
		/// <param name="from">From.</param>
		/// <param name="to">To.</param>
		public float InterpolateDerivative(float zeroToOne, float from, float to)
		{
			return InterpolateDerivative(zeroToOne, from, to, m_Interpolant);
		}

		/// <summary>
		/// 	Interpolate a Vector3 between two other Vector3s.
		/// </summary>
		/// <returns>The interpolated Vector3.</returns>
		/// <param name="zeroToOne">Zero to one.</param>
		/// <param name="from">From.</param>
		/// <param name="to">To.</param>
		public Vector3 Interpolate(float zeroToOne, Vector3 from, Vector3 to)
		{
			return Interpolate(zeroToOne, from, to, m_Interpolant);
		}

		public float InterpolateReverse(Vector3 value, Vector3 from, Vector3 to)
		{
			return InterpolateReverse(value, from, to, m_Interpolant);
		}

		/// <summary>
		/// 	Interpolates a Vector3 along a bezier of Vector3s.
		/// </summary>
		/// <returns>The interpolated Vector3.</returns>
		/// <param name="zeroToOne">Zero to one.</param>
		/// <param name="beziers">Beziers.</param>
		public Vector3 InterpolateBezier(float zeroToOne, Vector3[] beziers)
		{
			return InterpolateBezier(zeroToOne, beziers, m_Interpolant);
		}

		/// <summary>
		/// 	Interpolates a Vector3 around a circle.
		/// </summary>
		/// <returns>The interpolated Vector3.</returns>
		/// <param name="">.</param>
		/// <param name="from">From.</param>
		/// <param name="arcCenter">Arc center.</param>
		/// <param name="to">To.</param>
		/// <param name="useLargerArc">If set to <c>true</c> use larger arc.</param>
		public Vector3 InterpolateAroundCircle(float zeroToOne, Vector3 from, Vector3 arcCenter, Vector3 normal)
		{
			return InterpolateAroundCircle(zeroToOne, from, arcCenter, normal, m_Interpolant);
		}

		/// <summary>
		/// 	Interpolates a Vector3 along an arc.
		/// </summary>
		/// <returns>The interpolated Vector3.</returns>
		/// <param name="zeroToOne">Zero to one.</param>
		/// <param name="from">From.</param>
		/// <param name="arcCenter">Arc center.</param>
		/// <param name="to">To.</param>
		/// <param name="useLargerArc">If set to <c>true</c> use larger arc.</param>
		public Vector3 InterpolateArc(float zeroToOne, Vector3 from, Vector3 arcCenter, Vector3 to, bool useLargerArc)
		{
			return InterpolateArc(zeroToOne, from, arcCenter, to, useLargerArc, m_Interpolant);
		}

		/// <summary>
		/// 	Finds the float between zero and one that would need to be interpolated to find a certain result.
		/// </summary>
		/// <returns>The reverse-interpolated float.</returns>
		/// <param name="value">Value.</param>
		/// <param name="from">From.</param>
		/// <param name="arcCenter">Arc center.</param>
		/// <param name="to">To.</param>
		/// <param name="useLargerArc">If set to <c>true</c> use larger arc.</param>
		public float InterpolateArcReverse(Vector3 value, Vector3 from, Vector3 arcCenter, Vector3 to, bool useLargerArc)
		{
			return InterpolateArcReverse(value, from, arcCenter, to, useLargerArc, m_Interpolant);
		}

		/// <summary>
		/// 	Interpolates a Quaternion between two other Quaternions.
		/// </summary>
		/// <returns>The interpolated Quaternion.</returns>
		/// <param name="zeroToOne">Zero to one.</param>
		/// <param name="from">From.</param>
		/// <param name="to">To.</param>
		/// <param name="useLargerArc">If set to <c>true</c> use larger arc.</param>
		public Quaternion Interpolate(float zeroToOne, Quaternion from, Quaternion to, bool useLargerArc)
		{
			return Interpolate(zeroToOne, from, to, useLargerArc, m_Interpolant);
		}

		#endregion

		#region Static Methods

		/// <summary>
		/// 	Caps F from zero to one.
		/// </summary>
		/// <returns>F, capped from zero to one.</returns>
		/// <param name="f">F.</param>
		public static float CapZeroToOne(float f)
		{
			if (f < 0.0f)
				f = 0.0f;
			else if (f > 1.0f)
				f = 1.0f;
			return f;
		}

		/// <summary>
		/// 	Interpolates a float between two other floats.
		/// </summary>
		/// <returns>The interpolated float.</returns>
		/// <param name="zeroToOne">Zero to one.</param>
		/// <param name="from">From.</param>
		/// <param name="to">To.</param>
		/// <param name="interpolant">Interpolant.</param>
		public static float Interpolate(float zeroToOne, float from, float to, Interpolant interpolant)
		{
			float f;

			switch (interpolant)
			{
				case Interpolant.Linear:
					f = zeroToOne;
					break;
				case Interpolant.Quadratic:
					f = zeroToOne * zeroToOne;
					break;
				case Interpolant.ReverseQuadratic:
					f = 1.0f - (zeroToOne - 1.0f) * (zeroToOne - 1.0f);
					break;
				case Interpolant.Sinusoidal:
					f = 0.5f - Mathf.Cos(Mathf.PI * zeroToOne) / 2;
					break;
				case Interpolant.Pseudosoidal:
					f = 0.5f - 1.0f / (Mathf.Cos(Mathf.PI * zeroToOne) + 1.0f / Mathf.Cos(Mathf.PI * zeroToOne));
					break;
				case Interpolant.Stoposoidal:
					if (zeroToOne < STOPOSOIDAL_STOP_LENGTH / 2.0f)
						f = 0.0f;
					else if (zeroToOne < 1.0f - STOPOSOIDAL_STOP_LENGTH / 2.0f)
					{
						f = Interpolate((zeroToOne - STOPOSOIDAL_STOP_LENGTH / 2.0f) / (1.0f - STOPOSOIDAL_STOP_LENGTH), 0, 1,
										Interpolant.Sinusoidal);
					}
					else
						f = 1.0f;
					break;
				case Interpolant.Sinusinusinusoidal:
					f = 0.5f + 0.5f * Mathf.Sin(HALF_PI * Mathf.Sin(HALF_PI * Mathf.Sin(HALF_PI * 2.0f * zeroToOne - HALF_PI)));
					break;
				case Interpolant.QuarterCircleUpper:
					f = Mathf.Sqrt(1 - (zeroToOne - 1.0f) * (zeroToOne - 1.0f));
					break;
				case Interpolant.QuarterCircleLower:
					f = 1.0f - Mathf.Sqrt(zeroToOne * zeroToOne - 1.0f);
					break;
				default:
					f = 0.0f;
					break;
			}
			return from + f * (to - from);
		}

		/// <summary>
		/// 	Finds the float between zero and one that would need to be interpolated to find a certain result.
		/// </summary>
		/// <returns>The reverse-interpolated float.</returns>
		/// <param name="value">Value.</param>
		/// <param name="from">From.</param>
		/// <param name="to">To.</param>
		/// <param name="interpolant">Interpolant.</param>
		public static float InterpolateReverse(float value, float from, float to, Interpolant interpolant)
		{
			float zeroToOne = (value - from) / (to - from);
			float f;

			switch (interpolant)
			{
				case Interpolant.Linear:
					// f = zeroToOne;
					f = zeroToOne;
					break;
				case Interpolant.Quadratic:
					//f = zeroToOne * zeroToOne;
					f = Mathf.Sqrt(zeroToOne);
					break;
				case Interpolant.ReverseQuadratic:
					//f = 1.0f - (zeroToOne - 1.0f) * (zeroToOne - 1.0f);
					f = 1.0f - Mathf.Sqrt(1.0f - zeroToOne);
					break;
				case Interpolant.Sinusoidal:
					//f = 0.5f - Mathf.Cos(Mathf.PI * zeroToOne) / 2;
					f = Mathf.Acos(1.0f - 2 * zeroToOne) / Mathf.PI;
					break;
				case Interpolant.Pseudosoidal:
					//f = 0.5f - 1.0f / (Mathf.Cos(Mathf.PI * zeroToOne) + 1.0f / Mathf.Cos(Mathf.PI * zeroToOne));
					throw new NotImplementedException("Reverse not yet calculated.");
				case Interpolant.Stoposoidal:
					/*if (zeroToOne < STOPOSOIDAL_STOP_LENGTH / 2.0f)
						f = 0.0f;
					else if (zeroToOne < 1.0f - STOPOSOIDAL_STOP_LENGTH / 2.0f)
						f = Interpolate((zeroToOne - STOPOSOIDAL_STOP_LENGTH / 2.0f) / (1.0f - STOPOSOIDAL_STOP_LENGTH), 0, 1, Interpolant.Sinusoidal);
					else
						f = 1.0f;*/
					throw new NotImplementedException("Reverse not yet calculated.");
				case Interpolant.Sinusinusinusoidal:
					//f = 0.5f + 0.5f * Mathf.Sin(HALF_PI * Mathf.Sin(HALF_PI * Mathf.Sin(HALF_PI * 2.0f * zeroToOne - HALF_PI)));
					throw new NotImplementedException("Reverse not yet calculated.");
				case Interpolant.QuarterCircleUpper:
					//f = Mathf.Sqrt(1 - (zeroToOne - 1.0f) * (zeroToOne - 1.0f));
					f = Interpolate(zeroToOne, from, to, Interpolant.QuarterCircleLower);
					break;
				case Interpolant.QuarterCircleLower:
					//f = 1.0f - Mathf.Sqrt(zeroToOne * zeroToOne - 1.0f);
					f = Interpolate(zeroToOne, from, to, Interpolant.QuarterCircleUpper);
					break;
				default:
					f = 0.0f;
					break;
			}
			return f;
		}

		/// <summary>
		/// 	Interpolates the derivative of the interpolant between two floats.
		/// </summary>
		/// <returns>The interpolated derivative.</returns>
		/// <param name="zeroToOne">Zero to one.</param>
		/// <param name="from">From.</param>
		/// <param name="to">To.</param>
		/// <param name="interpolant">Interpolant.</param>
		public static float InterpolateDerivative(float zeroToOne, float from, float to, Interpolant interpolant)
		{
			float f;

			switch (interpolant)
			{
				case Interpolant.Linear:
					//f = zeroToOne;
					f = 1.0f;
					break;
				case Interpolant.Quadratic:
					//f = zeroToOne * zeroToOne;
					f = 2.0f * zeroToOne;
					break;
				case Interpolant.ReverseQuadratic:
					//f = 1.0f - (zeroToOne - 1.0f) * (zeroToOne - 1.0f);
					f = -2.0f * (zeroToOne - 1.0f);
					break;
				case Interpolant.Sinusoidal:
					//f = 0.5f - Mathf.Cos(Mathf.PI * zeroToOne) / 2;
					f = Mathf.Sin(Mathf.PI * zeroToOne) / 2 * Mathf.PI;
					break;
				case Interpolant.Pseudosoidal:
					//f = 0.5f - 1.0f / (Mathf.Cos(Mathf.PI * zeroToOne) + 1.0f / Mathf.Cos(Mathf.PI * zeroToOne));
					throw new NotImplementedException("Derivative not yet calculated.");
				case Interpolant.Stoposoidal:
					if (zeroToOne < STOPOSOIDAL_STOP_LENGTH / 2.0f)
						//f = 0.0f;
						f = 0.0f;
					else if (zeroToOne < 1.0f - STOPOSOIDAL_STOP_LENGTH / 2.0f)
						//f = Interpolate((zeroToOne - STOPOSOIDAL_STOP_LENGTH / 2.0f) / (1.0f - STOPOSOIDAL_STOP_LENGTH), 0, 1, Interpolant.Sinusoidal);
						throw new NotImplementedException("Derivative not yet calculated.");
					else
						//f = 1.0f;
						f = 0.0f;
					break;
				case Interpolant.Sinusinusinusoidal:
					//f = 0.5f + 0.5f * Mathf.Sin(HALF_PI * Mathf.Sin(HALF_PI * Mathf.Sin(HALF_PI * 2.0f * zeroToOne - HALF_PI)));
					throw new NotImplementedException("Derivative not yet calculated.");
				case Interpolant.QuarterCircleUpper:
					//f = Mathf.Sqrt(1 - (zeroToOne - 1.0f) * (zeroToOne - 1.0f));
					throw new NotImplementedException("Derivative not yet calculated.");
				case Interpolant.QuarterCircleLower:
					//f = 1.0f - Mathf.Sqrt(zeroToOne * zeroToOne - 1.0f);
					throw new NotImplementedException("Derivative not yet calculated.");
				default:
					f = 0.0f;
					break;
			}
			//return from + f * (to - from);
			return f * (to - from);
		}

		/// <summary>
		/// 	Interpolates a Vector3 between two other Vector3s.
		/// </summary>
		/// <returns>The interpolated Vector3.</returns>
		/// <param name="zeroToOne">Zero to one.</param>
		/// <param name="from">From.</param>
		/// <param name="to">To.</param>
		/// <param name="interpolant">Interpolant.</param>
		public static Vector3 Interpolate(float zeroToOne, Vector3 from, Vector3 to, Interpolant interpolant)
		{
			float newZeroToOne = Interpolate(zeroToOne, 0.0f, 1.0f, interpolant);
			return Vector3.Lerp(from, to, newZeroToOne);
		}

		/// <summary>
		/// 	Finds the float between zero and one that would need to be interpolated to find a certain result.
		/// </summary>
		/// <returns>The reverse-interpolated float.</returns>
		/// <param name="value">Value.</param>
		/// <param name="from">From.</param>
		/// <param name="to">To.</param>
		/// <param name="interpolant">Interpolant.</param>
		public static float InterpolateReverse(Vector3 value, Vector3 from, Vector3 to, Interpolant interpolant)
		{
			// Find the point on the to-from line that's closest to value.
			Vector3 projectedPoint = Vector3.Project(value - from, to - from);
			return InterpolateReverse((from + projectedPoint).magnitude, from.magnitude, to.magnitude, interpolant);
		}

		/// <summary>
		/// 	Interpolates a Vector3 along a bezier of Vector3s.
		/// </summary>
		/// <returns>The interpolated Vector3.</returns>
		/// <param name="zeroToOne">Zero to one.</param>
		/// <param name="beziers">Beziers.</param>
		/// <param name="interpolant">Interpolant.</param>
		public static Vector3 InterpolateBezier(float zeroToOne, Vector3[] beziers, Interpolant interpolant)
		{
			if (beziers.Length == 0)
				return Vector3.zero;

			Vector3[] bezierscopy = new Vector3[beziers.Length];
			for (int i = 0; i < beziers.Length; i++)
				bezierscopy[i] = beziers[i];

			for (int i = bezierscopy.Length; i > 0; i--)
			{
				for (int j = 0; j < i - 1; j++)
					bezierscopy[j] = Interpolate(zeroToOne, bezierscopy[j], bezierscopy[j + 1], interpolant);
			}

			return beziers[0];
		}

		/// <summary>
		/// 	Interpolates a Vector3 around a circle.
		/// </summary>
		/// <returns>The interpolated Vector3.</returns>
		/// <param name="zeroToOne">Zero to one.</param>
		/// <param name="from">From.</param>
		/// <param name="arcCenter">Arc center.</param>
		/// <param name="normal">Normal.</param>
		/// <param name="interpolant">Interpolant.</param>
		public static Vector3 InterpolateAroundCircle(float zeroToOne, Vector3 fromto, Vector3 arcCenter, Vector3 normal,
													  Interpolant interpolant)
		{
			fromto -= arcCenter;
			//Vector3 projectionDistance = fromto;
			fromto = Vector3.ProjectOnPlane(fromto, normal);
			//projectionDistance -= fromto;

			Vector3 ortho = Vector3.Cross(fromto, normal);
			ortho.Normalize();
			ortho *= fromto.magnitude; // Ortho is perpendicular to From and on the plane defined by Normal.

			float angleRads = Interpolate(zeroToOne, 0.0f, 2.0f * Mathf.PI, interpolant);

			return arcCenter + fromto * Mathf.Cos(angleRads) + ortho * Mathf.Sin(angleRads); // + projectionDistance;
		}

		/// <summary>
		/// 	Interpolates a Vector3 along an arc.
		/// </summary>
		/// <returns>The interpolated Vector3.</returns>
		/// <param name="zeroToOne">Zero to one.</param>
		/// <param name="from">From.</param>
		/// <param name="arcCenter">Arc center.</param>
		/// <param name="to">To.</param>
		/// <param name="useLargerArc">If set to <c>true</c> use larger arc.</param>
		/// <param name="interpolant">Interpolant.</param>
		public static Vector3 InterpolateArc(float zeroToOne, Vector3 from, Vector3 arcCenter, Vector3 to, bool useLargerArc,
											 Interpolant interpolant)
		{
			from -= arcCenter;
			to -= arcCenter;
			Vector3 normal = Vector3.Cross(to, from);
			if (normal == Vector3.zero)
				normal = Vector3.ProjectOnPlane(Vector3.up, from);
			float angle = Vector3.Angle(from, to);
			if (useLargerArc)
				angle -= 360.0f;
			zeroToOne *= angle / 360.0f;
			return arcCenter + InterpolateAroundCircle(zeroToOne, from, Vector3.zero, normal, interpolant);
		}

		/// <summary>
		/// 	Finds the float between zero and one that would need to be interpolated to find a certain result.
		/// </summary>
		/// <returns>The reverse-interpolated float.</returns>
		/// <param name="value">Value.</param>
		/// <param name="from">From.</param>
		/// <param name="arcCenter">Arc center.</param>
		/// <param name="to">To.</param>
		/// <param name="useLargerArc">If set to <c>true</c> use larger arc.</param>
		/// <param name="interpolant">Interpolant.</param>
		public static float InterpolateArcReverse(Vector3 value, Vector3 from, Vector3 arcCenter, Vector3 to,
												  bool useLargerArc, Interpolant interpolant)
		{
			throw new NotImplementedException("Reverse not yet calculated.");
		}

		/// <summary>
		/// 	Interpolates a Quaternion between two other Quaternions.
		/// </summary>
		/// <returns>The interpolated Quaternion.</returns>
		/// <param name="zeroToOne">Zero to one.</param>
		/// <param name="from">From.</param>
		/// <param name="to">To.</param>
		/// <param name="useLargerArc">If set to <c>true</c> use larger arc.</param>
		/// <param name="interpolant">Interpolant.</param>
		public static Quaternion Interpolate(float zeroToOne, Quaternion from, Quaternion to, bool useLargerArc,
											 Interpolant interpolant)
		{
			/*float arcAngle = Quaternion.Angle(from, to);
			if (useLargerArc)
				arcAngle -= 360.0f;
			float angle = Interpolate(zeroToOne, 0.0f, arcAngle, interpolant);
			float t = Interpolate (zeroToOne, 0.0f, 1.0f, interpolant);

			Quaternion result;
			if (useLargerArc && angle < arcAngle + 180.0f)
				//result = Quaternion.RotateTowards(Quaternion.Inverse(from), to, angle - 180.0f);
				//result = Quaternion.Slerp(Quaternion.Inverse(from), to, t);
			else
				//result = Quaternion.RotateTowards(from, to, angle);
				//result = Quaternion.Slerp(from, to, t);

			return result;*/
			float angle; Vector3 axis;
			Quaternion delta = Quaternion.FromToRotation(from * Vector3.right, to * Vector3.right);
			delta.ToAngleAxis(out angle, out axis);
			if (useLargerArc)
				angle -= 360;
			angle = Interpolate(zeroToOne, 0.0f, angle, interpolant);
			delta = Quaternion.AngleAxis(angle, axis);
			return from * delta;
		}

		#endregion
	}
}
