﻿using System.Collections;
using System.Runtime.InteropServices;
using UnityEngine;

namespace AntonCoolpecker.Utils
{
	/// <summary>
	/// Web utilities.
	/// </summary>
	public static class WebUtils
	{
		#if (UNITY_WEBGL)
		[DllImport("__Internal")] public static extern void DownloadFile(string content, string filename);
		#endif
	}
}