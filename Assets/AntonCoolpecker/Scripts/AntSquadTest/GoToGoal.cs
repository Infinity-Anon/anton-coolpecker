﻿using Hydra.HydraCommon.Abstract;
using System.Collections.Generic;
using UnityEngine;

namespace AntonCoolpecker.AntSquadTest
{
	/// <summary>
	/// Used for testing an ant squad's ability to move towards a set list of goals(Waypoints).
	/// </summary>
	[RequireComponent(typeof(NavMeshAgent))]
	public class GoToGoal : HydraMonoBehaviour
	{
		#region Variables

		[SerializeField] private List<Transform> m_Goals; //List of waypoints
		[SerializeField] private float m_WaypointTolerance = 0.1f; //How close the ant squad must be to a certain waypoint to declare that waypoint as reached

		private int m_NextWPIndex = 0; //Keeps track of the waypoints that the ant squad wanders from/to
		private NavMeshAgent m_CachedAgent;
		private List<Transform>.Enumerator m_GoalsEnumerator;

		#endregion

		#region Properties

		/// <summary>
		/// 	Gets the nav mesh agent.
		/// </summary>
		/// <value>The nav mesh agent.</value>
		public NavMeshAgent navMeshAgent { get { return m_CachedAgent ?? (m_CachedAgent = GetComponent<NavMeshAgent>()); } }

		#endregion

		#region Messages

		/// <summary>
		/// 	Called before the first Update.
		/// </summary>
		protected override void Start()
		{
			base.Start();

			if (m_Goals[0] == null)
			{
				Debug.LogErrorFormat("No waypoints assigned to {0}", ToString());
				return;
			}

			navMeshAgent.destination = m_Goals[0].position;
			m_NextWPIndex = GetNextWPIndex();
		}

		/// <summary>
		/// 	Called once every frame.
		/// </summary>
		protected override void Update()
		{
			base.Update();

			// We've reached our current goal (and goal list is not empty)
			if (navMeshAgent.remainingDistance > m_WaypointTolerance)
				return;

			if (m_Goals.Count <= 1)
				return;

			if (m_Goals[m_NextWPIndex] == null)
				return;

			navMeshAgent.destination = m_Goals[m_NextWPIndex].position;
			m_NextWPIndex = GetNextWPIndex();
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// 	Gets the index of the next WP.
		/// 	Not meant as a getter method. Only to implement index wraparound
		/// </summary>
		/// <returns>The next WP index.</returns>
		private int GetNextWPIndex()
		{
			int next_val = (m_NextWPIndex + 1) % m_Goals.Count;

			// Ensure we don't index with a negative value
			if (next_val >= 0)
				return next_val;

			return 0;
		}

		#endregion
	}
}