﻿using AntonCoolpecker.Utils.Time;
using Hydra.HydraCommon.Abstract;
using UnityEngine;

namespace AntonCoolpecker.AntSquadTest
{
	/// <summary>
	/// Used for testing how ant squads run around a certain radius.
	/// </summary>
	[DisallowMultipleComponent]
	[RequireComponent(typeof(CharacterController))]
	public class RunAround : HydraMonoBehaviour
	{
		#region Variables

		// Arbitrary values. Feel free to change them
		[SerializeField] private float m_RotateRadius = 3.0f;
		[SerializeField] private float m_Speed = 200.0f;

		private CharacterController m_CachedCharacterController;

		#endregion

		#region Properties

		/// <summary>
		/// 	Gets the character controller.
		/// </summary>
		/// <value>The character controller.</value>
		public CharacterController characterController
		{
			get { return m_CachedCharacterController ?? (m_CachedCharacterController = GetComponent<CharacterController>()); }
		}

		/// <summary>
		/// 	Gets or sets the rotate radius.
		/// </summary>
		/// <value>The rotate radius.</value>
		public float rotateRadius { get { return m_RotateRadius; } set { m_RotateRadius = value; } }

		/// <summary>
		/// 	Gets or sets the speed.
		/// </summary>
		/// <value>The speed.</value>
		public float speed { get { return m_Speed; } set { m_Speed = value; } }

		#endregion

		#region Messages

		/// <summary>
		/// 	Called once every frame.
		/// </summary>
		protected override void Update()
		{
			base.Update();

			Vector3 pivot = transform.TransformPoint(Vector3.right * rotateRadius);
			transform.RotateAround(pivot, Vector3.up, m_Speed * GameTime.deltaTime);
		}

		#endregion
	}
}
