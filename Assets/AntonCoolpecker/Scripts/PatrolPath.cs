﻿using System.Collections.Generic;
using UnityEngine;

namespace AntonCoolpecker
{
	/// <summary>
	/// Patrol path - Used for patrol paths of enemies/NPCs/etc.
	/// </summary>
    public class PatrolPath : MonoBehaviour
    {
		#region Variables

		[SerializeField] public List<PatrolPoint> EditorPoints = new List<PatrolPoint>();
		[SerializeField] public bool TrueLoopPathFalseReversePath;
		[HideInInspector] public PatrolPoint[] Points;

		#endregion

		#region Messages

		/// <summary>
		/// Awake this instance.
		/// </summary>
        void Awake()
        {
            Points = EditorPoints.ToArray();

            for (int i = 0; i < Points.Length; i++)
                Points[i].position += transform.position;
        }

		#endregion

		#region Methods

		/// <summary>
		/// Gets the index of the closest point.
		/// </summary>
		/// <returns>The closest point index.</returns>
		/// <param name="position">Position.</param>
        public int GetClosestPointIndex(Vector3 position)
        {
            float distance = Vector3.Distance(position, Points[0].position);
            int index = 0;

            for (int i = 1; i < Points.Length; i++)
            {
                if (Vector3.Distance(position, Points[i].position) < distance)
                {
                    index = i;
                    distance = Vector3.Distance(position, Points[i].position);
                }
            }
            
            return index;
        }

		/// <summary>
		/// Gets the closest point.
		/// </summary>
		/// <returns>The closest point.</returns>
		/// <param name="index">Index.</param>
		/// <param name="point">Point.</param>
		/// <param name="Enemy">Enemy.</param>
        public Vector3 GetClosestPoint(int index, Vector3 point, AntonCoolpecker.Concrete.Enemies.EnemyController Enemy)
        {
			Vector3 nextPoint, previousPoint;

            if (!TrueLoopPathFalseReversePath && (index == 0 || index == Points.Length - 1))
            {
                if (index == 0)
                    nextPoint = previousPoint = Points[1].position;
                else
                    nextPoint = previousPoint = Points[index - 1].position;        
            }
            else if (Points.Length > 2)
            {
                if (index == Points.Length - 1)
                {
                    nextPoint = Points[0].position;
                    previousPoint = Points[index - 1].position;
                }
                else if (index == 0)
                {
                    previousPoint = Points[Points.Length - 1].position;
                    nextPoint = Points[index + 1].position;
                }
                else
                {
                    nextPoint = Points[index + 1].position;
                    previousPoint = Points[index - 1].position;
                }
            }
            else if (Points.Length == 2)
            {
                nextPoint = Points[0].position;
                previousPoint = Points[1].position;
            }
            else
            {
                nextPoint = Vector3.zero;
                previousPoint = Vector3.zero;
                return Points[index].position;
            }

            Vector3 eval1 = LineEvaluation(Points[index].position, nextPoint, point);
            Vector3 eval2 = LineEvaluation(previousPoint, Points[index].position, point);

            if (Vector3.Distance(point, eval1) < Vector3.Distance(point, eval2))
            {
                if (PointOnLine(eval1, Points[index].position, nextPoint))
                    return eval1;
                else
                {
					//Check if eval 2 is on line, and if it is, is it closer
                    if (PointOnLine(eval2, previousPoint, Points[index].position) && Vector3.Distance(point, eval2) < Vector3.Distance(point, Points[index].position))
                    {
                        if (Enemy.patrolIndex == 0)
							Enemy.patrolIndex = Points.Length - 1;
                        else
							Enemy.patrolIndex--;
						
                        return eval2;
                    }
                    else
                        return Points[index].position;
                }
            }
            else
            {
                if (PointOnLine(eval2, previousPoint, Points[index].position))
                {
					if (Enemy.patrolIndex == 0)
						Enemy.patrolIndex = Points.Length - 1;
                    else
						Enemy.patrolIndex--;
					
                    return eval2;
                }
                else
                {
                    if ((PointOnLine(eval1, Points[index].position, nextPoint)) && Vector3.Distance(point, eval1) < Vector3.Distance(point, Points[index].position))
                        return eval1;
                    else
                        return Points[index].position;
                }
            }
        }

        /// <summary>
        /// Returns an evaluation of a point between a line of two vectors.
        /// </summary>
        /// <returns>The line evaluation.</returns>
        /// <param name="l1">Line vector 1.</param>
        /// <param name="l2">Line vector 2.</param>
        /// <param name="p">Point.</param>
        private Vector3 LineEvaluation(Vector3 l1, Vector3 l2, Vector3 p)
        {
            Vector3 d = (l2 - l1).normalized;
            return l1 + (Vector3.Dot(p - l1, d) * d); 
        }

		/// <summary>
		/// Determines whether a point is between a line of two vectors or not.
		/// </summary>
		/// <returns><c>true</c>, if a point is between two vectors, <c>false</c> otherwise.</returns>
		/// <param name="pt">Point.</param>
		/// <param name="v1">Line vector 1.</param>
		/// <param name="v2">Line vector 2.</param>
        bool PointOnLine(Vector3 pt, Vector3 v1, Vector3 v2)
        {
            float dot = Vector3.Dot( (pt - v1).normalized,(v2-v1).normalized);

            if (dot >= 0)
            {
                if ((pt - v1).magnitude <= (v2 - v1).magnitude)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        /// <summary>
        /// Draw gizmos when the gameobject is selected.
        /// </summary>
        void OnDrawGizmosSelected()
        {
            Gizmos.color = new Color(1, 0, 0);

            for (int i = 0; i < EditorPoints.Count; i++)
            {
                Gizmos.DrawSphere(EditorPoints[i].position + transform.position, 1f);

                if (i < EditorPoints.Count - 1)
                    Gizmos.DrawLine(EditorPoints[i].position + transform.position, EditorPoints[i + 1].position + transform.position);
                else if (TrueLoopPathFalseReversePath)
                    Gizmos.DrawLine(EditorPoints[i].position + transform.position, EditorPoints[0].position + transform.position);
            }
        }

		#endregion
    }

	#region Internal Structures/Classes

	/// <summary>
	/// Patrol point.
	/// </summary>
	[System.Serializable]
    public struct PatrolPoint
    {
        public Vector3 position;
        public float delay;
    }

	#endregion
}