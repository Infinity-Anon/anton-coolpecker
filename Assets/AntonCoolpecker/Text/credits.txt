>Programmers

jailbreaker
bones
bradh96
vesuvian
uberklaus
dankdavev2
cptdrmoreno
diglididudeng
gravity
nosrick
jaxson
jeb
krendil
pompolic
tarqvinivs
thilth 
brokenideascompany
jimjamthedwarf
torbrex
cwook
hudsonzero

>Writers

chaoguy2006
infinity-anon
lorley
val
spikethestupido

>Artists

anonasloth
bones
thedean50
diskless
enkay_b
springer
brokenideascompany
f'tang
a_moth
uberklaus

>Modelers

snapai
Burri
chibikami
diskless
cptdrmoreno
hol
jeryen
mysteriousoctopuss
shiggy
sven
Thirim
thirite
howiestern
slimfandango
f'tang
uberklaus
topdong

>Animators

snapai
animfag
Burri
thirite
doktorpebbles
bones

>Level Designers

jailbreaker
thedean50
diskless
enkay_b
a_moth
jeb
brokenideascompany
thecrancher

>Soundwork

azuli
burnuh
jeb
applegreen

>Voice Acting

d.b.joe
applegreen
a_moth
miri
jijivoice
Guscraw
fredtheundead
freeman
globin
lordofthemorning
ksmooth
vonoppendown

>People with unknown roles in the team(Please assign these names elsewhere in the credits if possible)

12jrutherford
connicron
shuv
shoveldog
lloydd
hotrod-mccoolguy
cptcoconut
Dorma
greatlange
