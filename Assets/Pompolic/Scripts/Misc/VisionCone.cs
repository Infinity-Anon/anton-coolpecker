﻿using UnityEngine;

namespace AntonCoolpecker.Utils
{
	/// <summary>
	/// Vision cone - draws a visual cone.
	/// </summary>
	public class VisionCone : MonoBehaviour
	{
		#region Variables

		[SerializeField] private float m_FieldOfView;
		[SerializeField] private float m_ConeLength;
		[SerializeField] private Mesh m_ConeMesh;

		#endregion

		#region Private Methods

		/// <summary>
		/// Raises the draw gizmos event.
		/// </summary>
		private void OnDrawGizmos()
		{
			Vector3 coneOrigin = transform.position;
			coneOrigin.y = 1.6f;
			Gizmos.color = Color.yellow;
			Gizmos.DrawFrustum(coneOrigin, m_FieldOfView, m_ConeLength, 0, (16.0f / 9.0f));
		}

		#endregion
	}
}