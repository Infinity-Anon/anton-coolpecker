﻿using AntonCoolpecker.Concrete.Menus.DebugGUI;
using Hydra.HydraCommon.Abstract;
using UnityEngine;
using UnityEngine.UI;

namespace AntonCoolpecker.Concrete.Menus
{
	/// <summary>
	/// 	Shows the debug GUI when pressed.
	/// </summary>
	public class DebugButton : AbstractButton
	{
        #region Private Methods

        /// <summary>
        /// 	Called when the button is clicked.
        /// </summary>
        public override void OnButtonClicked()
		{
			Main maino = GameObject.Find("Main").GetComponent<Main>();
			maino.debugGui.Show ();
		}

		#endregion
	}
}
