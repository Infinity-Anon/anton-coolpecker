﻿using Hydra.HydraCommon.Abstract;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace AntonCoolpecker.Concrete.Menus
{
	//TODO: IMPLEMENT PROPER TRANSITION TO MAIN MENU

	/// <summary>
	/// 	Loads the main menu scene when pressed.
	/// </summary>
	public class MainMenuButton : AbstractButton
	{
		#region Private Functions

		/// <summary>
		/// 	Called when the button is clicked.
		/// </summary>
		public override void OnButtonClicked()
		{
			SceneManager.LoadScene ("Main");
        }

		#endregion
	}
}