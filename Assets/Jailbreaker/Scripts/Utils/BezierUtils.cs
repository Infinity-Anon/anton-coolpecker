﻿using Hydra.HydraCommon.Utils;
using UnityEngine;

namespace Jailbreaker.Utils
{
	/// <summary>
	/// Bezier utils.
	/// </summary>
	public class BezierUtils
	{
		#region Variables

		private const float ONE_THIRTY_SECONDTH = 1.0f / 32.0f;

		#endregion

		#region Methods

		/// <summary>
		/// Return an approximation of the length of a bezier curve. Point 0 is the start, point 1 is the control, and point 2 is the end.
		/// </summary>
		/// <returns>The length estimate.</returns>
		/// <param name="x0">The point 0 x value.</param>
		/// <param name="y0">The point 0 y value.</param>
		/// <param name="z0">The point 0 z value.</param>
		/// <param name="x1">The point 1 x value.</param>
		/// <param name="y1">The point 1 y value.</param>
		/// <param name="z1">The point 1 z value.</param>
		/// <param name="x2">The point 2 x value.</param>
		/// <param name="y2">The point 2 y value.</param>
		/// <param name="z2">The point 2 z value.</param>
		public static float BezierLengthEstimate(float x0, float y0, float z0,
												 float x1, float y1, float z1,
												 float x2, float y2, float z2)
		{
			float total = 0.0f;

			// Divide the bezier into 32 line segments and sum up their lengths.
			for (float f = 0.0f; f < 1; f += ONE_THIRTY_SECONDTH)
			{
				float[] pointHere = BezierPoint(x0, y0, z0, 
												x1, y1, z1,
												x2, y2, z2,
												f);
				
				float[] pointNext = BezierPoint(x0, y0, z0,
												x1, y1, z1,
												x2, y2, z2,
												f + ONE_THIRTY_SECONDTH);
				
				total += FindDistance(pointHere, pointNext);
			}

			return total;
		}
			
		/// <summary>
		/// Return the length of a bezier curve. Point 0 is the start, point 1 is the control, and point 2 is the end.
		/// </summary>
		/// <returns>The length.</returns>
		/// <param name="x0">The point 0 x value.</param>
		/// <param name="y0">The point 0 y value.</param>
		/// <param name="z0">The point 0 z value.</param>
		/// <param name="x1">The point 1 x value.</param>
		/// <param name="y1">The point 1 y value.</param>
		/// <param name="z1">The point 1 z value.</param>
		/// <param name="x2">The point 2 x value.</param>
		/// <param name="y2">The point 2 y value.</param>
		/// <param name="z2">The point 2 z value.</param>
		public static float BezierLength(float x0, float y0, float z0,
										 float x1, float y1, float z1,
										 float x2, float y2, float z2)
		{
			// Transform into 2D space.
			// Rotate clockwise with a constant Y around point 0 until z1 = z0.

			float dist01 = FindDistance(new float[] {x0, 0, z0}, new float[] {x1, 0, z1});
			float dist02 = FindDistance(new float[] {x0, 0, z0}, new float[] {x2, 0, z2});

			float angle01 = Mathf.Atan2(z1 - z0, x1 - x0);
			float angle02 = Mathf.Atan2(z2 - z0, x2 - x0);

			x1 = x0 + dist01;
			x2 = x0 + dist02 * Mathf.Cos(angle02 - angle01);
			z1 = z0;
			z2 = z0 + dist02 * Mathf.Sin(angle02 - angle01);

			// Rotate clockwise with a constant Z around point 0 until y1 = y0.

			dist01 = FindDistance(new float[] {x0, y0, 0}, new float[] {x1, y1, 0});
			dist02 = FindDistance(new float[] {x0, y0, 0}, new float[] {x2, y2, 0});

			angle01 = Mathf.Atan2(y1 - y0, x1 - x0);
			angle02 = Mathf.Atan2(y2 - y0, x2 - x0);

			x1 = x0 + dist01;
			x2 = x0 + dist02 * Mathf.Cos(angle02 - angle01);
			y1 = y0;
			y2 = y0 + dist02 * Mathf.Sin(angle02 - angle01);

			// Rotate clockwise with a constant X around point 0 until z2 = z0.

			dist02 = FindDistance(new float[] {0, y0, z0}, new float[] {0, y2, z2});

			z2 = z0;
			y2 = y0 + dist02;

			/* This entire algorithm requires an assumption that I can't even prove - the assumption that the line drawn from an intersection of two tangent lines to a parabola to the
			 * midpoint of the line segment between the points at which those lines are tangent is parallel to the parabola's axis of symmetry. <http://i.imgur.com/LTeqawF.png>
			 * But it seems to work just fine. Must be magic.*/

			float xMidpoint = (x0 + x2) / 2, yMidpoint = (y0 + y2) / 2;
			float angle1ToMidpoint = Mathf.Atan2(yMidpoint - y1, xMidpoint - x1);

			float angle1To0 = Mathf.Atan2(y0 - y1, x0 - x1);
			float angle0To1 = Mathf.Atan2(y1 - y0, x1 - x0);
			float angle1To2 = Mathf.Atan2(y2 - y1, x2 - x1);
			float angle2To1 = Mathf.Atan2(y1 - y2, x1 - x2);
			float angle0To2 = Mathf.Atan2(y2 - y0, x2 - x0);
			float angle2To0 = Mathf.Atan2(y0 - y2, x0 - x2);

			// These were just the names I used while working out the geometry of this problem.

			float angleV = angle0To2 - angle0To1;
			float angleW = angle2To0 - angle2To1;
			float angleJ = angle1To0 - angle1ToMidpoint;
			float angleK = angle1To2 - angle1ToMidpoint;
			float angleG = angleK - angleJ;
			float angleA = angleJ - angleV;
			float angleB = angleK - angleW;
			float angleX = 2 * angleG;

			float distance0To1 = FindDistance(new float[] {x0, y0, 0}, new float[] {x1, y1, 0});
			float distance0To2 = FindDistance(new float[] {x0, y0, 0}, new float[] {x2, y2, 0});
			float distance1To2 = FindDistance(new float[] {x1, y1, 0}, new float[] {x2, y2, 0});

			float angle0ToFocus = angle0To2 + angleA;
			//float angle2ToFocus = angle2To0 + angleB; // For debugging purposes.

			float distance0ToFocus = distance0To1 * Mathf.Cos(angleJ);
			float distance2ToFocus = distance1To2 * Mathf.Cos(angleK);

			if (HydraMathUtils.Abs(Mathf.Sin(angleX)) > 0.00001f)
			{
				distance2ToFocus = (distance0To2 * Mathf.Sin(angleA)) / Mathf.Sin(angleX);
				distance0ToFocus = (distance0To2 * Mathf.Sin(-angleB)) / Mathf.Sin(angleX);
			}

			float xF = x0 + Mathf.Cos(angle0ToFocus) * distance0ToFocus;
			float yF = y0 + Mathf.Sin(angle0ToFocus) * distance0ToFocus;

			//float xF2 = x2 + Mathf.Cos(angle2ToFocus) * distance2ToFocus; // For debugging purposes.
			//float yF2 = y2 + Mathf.Sin(angle2ToFocus) * distance2ToFocus; // These should be the same as xF and yF.

			float angle0ToDrix = angle1ToMidpoint - Mathf.PI;
			float xDrixNear0 = x0 + Mathf.Cos(angle0ToDrix) * distance0ToFocus;
			float yDrixNear0 = y0 + Mathf.Sin(angle0ToDrix) * distance0ToFocus;
			float xDrixNear2 = x2 + Mathf.Cos(angle0ToDrix) * distance2ToFocus;
			float yDrixNear2 = y2 + Mathf.Sin(angle0ToDrix) * distance2ToFocus;

			float distanceDrixNear0ToFocus = FindDistance(new float[] {xDrixNear0, yDrixNear0, 0}, new float[] {xF, yF, 0});
			float distanceDrixNear2ToFocus = FindDistance(new float[] {xDrixNear2, yDrixNear2, 0}, new float[] {xF, yF, 0});

			float focalLength = Mathf.Sin(angleJ) * distanceDrixNear0ToFocus / 2;
			//float focalLength2 = Mathf.Sin(-angleK) * distanceDrixNear2ToFocus / 2; // For debugging purposes. Again, this should be the same as focalLength.

			float perpDistance0 = Mathf.Cos(angleJ) * distanceDrixNear0ToFocus;
			float perpDistance2 = Mathf.Cos(-angleK) * distanceDrixNear2ToFocus;

			float h0 = perpDistance0 / 2;
			float h2 = -perpDistance2 / 2;
			float q0 = Mathf.Sqrt(focalLength * focalLength + h0 * h0);
			float q2 = Mathf.Sqrt(focalLength * focalLength + h2 * h2);
			float length = HydraMathUtils.Abs((h0 * q0 - h2 * q2) / focalLength + focalLength * Mathf.Log((h0 + q0) / (h2 + q2)));

			return length;
		}
			
		/// <summary>
		/// Find a point on the bezier a certain percent along it. Has nothing to do with distance along that bezier, though. Point 0 is the start, point 1 is the control, and point 2 is the end.
		/// </summary>
		/// <returns>The point.</returns>
		/// <param name="x0">The point 0 x value.</param>
		/// <param name="y0">The point 0 y value.</param>
		/// <param name="z0">The point 0 z value.</param>
		/// <param name="x1">The point 1 x value.</param>
		/// <param name="y1">The point 1 y value.</param>
		/// <param name="z1">The point 1 z value.</param>
		/// <param name="x2">The point 2 x value.</param>
		/// <param name="y2">The point 2 y value.</param>
		/// <param name="z2">The point 2 z value.</param>
		/// <param name="zeroToOne">Zero to one.</param>
		public static float[] BezierPoint(float x0, float y0, float z0, float x1, float y1, float z1, float x2, float y2,
										  float z2, float zeroToOne)
		{
			float xA = Mathf.Lerp(x0, x1, zeroToOne);
			float xB = Mathf.Lerp(x1, x2, zeroToOne);
			float xC = Mathf.Lerp(xA, xB, zeroToOne);

			float yA = Mathf.Lerp(y0, y1, zeroToOne);
			float yB = Mathf.Lerp(y1, y2, zeroToOne);
			float yC = Mathf.Lerp(yA, yB, zeroToOne);

			float zA = Mathf.Lerp(z0, z1, zeroToOne);
			float zB = Mathf.Lerp(z1, z2, zeroToOne);
			float zC = Mathf.Lerp(zA, zB, zeroToOne);

			return new float[] {xC, yC, zC};
		}

		/// <summary>
		/// Finds the distance between two arrays.
		/// Each array is a point is in the form of {x, y, z}.
		/// </summary>
		/// <returns>The distance.</returns>
		/// <param name="from">From.</param>
		/// <param name="to">To.</param>
		public static float FindDistance(float[] from, float[] to)
		{
			float xsq = to[0] - from[0];
			xsq *= xsq;

			float ysq = to[1] - from[1];
			ysq *= ysq;

			float zsq = to[2] - from[2];
			zsq *= zsq;

			return Mathf.Sqrt(xsq + ysq + zsq);
		}

		#endregion
	}
}