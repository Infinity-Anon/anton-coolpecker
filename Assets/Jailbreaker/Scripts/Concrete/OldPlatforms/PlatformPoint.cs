﻿using Hydra.HydraCommon.Abstract;
using Jailbreaker.Utils;
using UnityEngine;

namespace Jailbreaker.Concrete.Platforms
{
	/// <summary>
	/// Platform point.
	/// </summary>
	public class PlatformPoint : HydraMonoBehaviour
	{
		#region Variables

		[SerializeField] private PlatformPoint m_NextPoint;
		[SerializeField] private PlatformBezier m_Bezier;
		[SerializeField] private float m_SpeedCoefficient = 1.0f; // Change this to make the platform move faster or slower while near this point.

		private PlatformPoint m_PreviousPoint;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the next platform point.
		/// </summary>
		/// <returns>The next platform point.</returns>
		public PlatformPoint GetNext()
		{
			return m_NextPoint;
		}

		/// <summary>
		/// Gets the previous platform point.
		/// </summary>
		/// <returns>The previous platform point.</returns>
		public PlatformPoint GetPrevious()
		{
			return m_PreviousPoint;
		}

		/// <summary>
		/// Gets the bezier.
		/// </summary>
		/// <returns>The bezier.</returns>
		public PlatformBezier GetBezier()
		{
			return m_Bezier;
		}

		/// <summary>
		/// Gets the speed coefficient.
		/// </summary>
		/// <returns>The speed coefficient.</returns>
		public float GetSpeedCoefficient()
		{
			return m_SpeedCoefficient;
		}

		#endregion

		#region Messages

		/// <summary>
		/// Called before the first Update.
		/// </summary>
		protected override void Start()
		{
			base.Start();

			GetNext().SetPrevious(this);
		}

		#endregion

		#region Public Methods

		/// <summary>
		/// Called when getting the distance until the next platform point from the current.
		/// </summary>
		/// <returns>The to next.</returns>
		public float DistanceToNext()
		{
			float nextDistance = 0;

			if (GetNext())
			{
				if (GetBezier())
				{
					nextDistance = BezierUtils.BezierLength(transform.position.x, transform.position.y, transform.position.z,
															GetBezier().transform.position.x, GetBezier().transform.position.y, GetBezier().transform.position.z,
															GetNext().transform.position.x, GetNext().transform.position.y, GetNext().transform.position.z);
				}
				else
				{
					float[] myPointValues = {transform.position.x, transform.position.y, transform.position.z};
					float[] nextPointValues =
					{
						GetNext().transform.position.x, GetNext().transform.position.y,
						GetNext().transform.position.z
					};

					nextDistance = BezierUtils.FindDistance(myPointValues, nextPointValues);
				}
			}

			return nextDistance;
		}

		/// <summary>
		/// Gets the remaining distance until the next platform point.
		/// </summary>
		/// <returns>The remaining distance.</returns>
		public float GetRemainingDistance()
		{
			float f = DistanceToNext();

			if (GetNext())
				f += GetNext().GetRemainingDistance();
			
			return f;
		}

		/// <summary>
		/// Interpolates the position.
		/// </summary>
		/// <returns>The new resulting position.</returns>
		/// <param name="zeroToOne">Zero to one.</param>
		public Vector3 InterpolatePosition(float zeroToOne)
		{
			Vector3 newPosition = transform.position;
			if (GetNext())
			{
				if (GetBezier())
				{
					float[] positions = BezierUtils.BezierPoint(transform.position.x, transform.position.y, transform.position.z,
																GetBezier().transform.position.x, GetBezier().transform.position.y, GetBezier().transform.position.z,
																GetNext().transform.position.x, GetNext().transform.position.y, GetNext().transform.position.z,
																zeroToOne);
					newPosition = new Vector3(positions[0], positions[1], positions[2]);
				}
				else
				{
					newPosition = Vector3.MoveTowards(transform.position, GetNext().transform.position,
													  zeroToOne * (GetNext().transform.position - transform.position).magnitude);
				}
			}
			return newPosition;
		}

		/// <summary>
		/// Interpolates the rotation.
		/// </summary>
		/// <returns>The new resulting rotation.</returns>
		/// <param name="zeroToOne">Zero to one.</param>
		public Quaternion InterpolateRotation(float zeroToOne)
		{
			Quaternion newRotation = transform.rotation;
			if (GetNext())
			{
				newRotation = Quaternion.RotateTowards(transform.rotation, GetNext().transform.rotation,
													   zeroToOne * Quaternion.Angle(transform.rotation, GetNext().transform.rotation));
			}
			return newRotation;
		}

		/// <summary>
		/// Interpolates the scale.
		/// </summary>
		/// <returns>The new resulting scale.</returns>
		/// <param name="zeroToOne">Zero to one.</param>
		public Vector3 InterpolateScale(float zeroToOne)
		{
			Vector3 newScale = transform.localScale;

			if (GetNext())
			{
				newScale = Vector3.MoveTowards(transform.localScale, GetNext().transform.localScale,
											   zeroToOne * (GetNext().transform.localScale - transform.localScale).magnitude);
			}

			return newScale;
		}

		/// <summary>
		/// Sets the previous point.
		/// </summary>
		/// <param name="pp">The previous point.</param>
		public void SetPrevious(PlatformPoint pp)
		{
			m_PreviousPoint = pp;
		}

		#endregion
	}
}