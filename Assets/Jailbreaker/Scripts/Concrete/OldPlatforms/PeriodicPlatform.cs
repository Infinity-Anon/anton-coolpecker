namespace Jailbreaker.Concrete.Platforms
{
	/// <summary>
	/// Periodic platform.
	/// </summary>
	public class PeriodicPlatform : MovingPlatform
	{
		#region Methods

		/// <summary>
		/// Called at the end of the platform's motion
		/// </summary>
		protected override void EndOfMotion()
		{
			base.EndOfMotion();

			TurnAround();
			StartMoving();
		}

		#endregion
	}
}