﻿namespace Jailbreaker.Concrete.Platforms
{
	/// <summary>
	/// Circle platform.
	/// </summary>
	public class CirclePlatform : MovingPlatform
	{
		#region Methods

		/// <summary>
		/// Called before the first Update.
		/// </summary>
		protected override void Start()
		{
			base.Start();

			StartMoving();
		}

		/// <summary>
		/// Called when the motion of the platform reaches its' end.
		/// </summary>
		protected override void EndOfMotion()
		{
			base.EndOfMotion();

			BackToStart();
			StartMoving();
		}

		#endregion
	}
}