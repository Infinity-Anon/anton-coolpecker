﻿using AntonCoolpecker.Utils;
using AntonCoolpecker.Utils.Time;
using Hydra.HydraCommon.Abstract;
using Hydra.HydraCommon.Utils;
using UnityEngine;

namespace Jailbreaker.Concrete.Platforms
{
	/// <summary>
	/// Moving platform.
	/// </summary>
	[RequireComponent(typeof(Rigidbody))]
	public class MovingPlatform : HydraMonoBehaviour
	{
		#region Variables

		[SerializeField] private PlatformPoint m_StartingPoint;
		[SerializeField] private Interpolater.Interpolant m_Interpolant;

		[SerializeField] private float m_InterpolationPercent; // Ranges from 0-1. Used to interpolate this GO's actual position. 1 represents the start of its path, 0 represents the end.
		[SerializeField] private bool m_IsMoving = true;
		[SerializeField] private float m_MotionPeriod; // The time this platform takes to get from one end of its path to the other.

		private PlatformPoint m_Point; // The point this platform most recently passed.
		private Interpolater m_Terp;

		private float m_TotalDistance; // The total length of this platform's path.
		private float m_PointDistance; // The length of this platform's path up to, but not including, the current point.

		#endregion

		#region Properties

		/// <summary>
		/// 	Gets or sets a value indicating whether this MovingPlatform is moving.
		/// </summary>
		/// <value><c>true</c> if moving; otherwise, <c>false</c>.</value>
		public bool isMoving { get { return m_IsMoving; } set { m_IsMoving = value; } }

		#endregion

		#region Private/Protected Functions/Methods

		/// <summary>
		/// 	Called when the component is enabled.
		/// </summary>
		protected override void OnEnable()
		{
			base.OnEnable();

			if (m_Terp == null)
				m_Terp = new Interpolater(m_Interpolant);
		}

		/// <summary>
		/// 	Called before the first Update.
		/// </summary>
		protected override void Start()
		{
			base.Start();

			m_Point = m_StartingPoint;
			m_TotalDistance = m_Point.GetRemainingDistance();
			UpdatePosition();
		}

		/// <summary>
		/// 	Called every physics timestep.
		/// </summary>
		protected override void FixedUpdate()
		{
			base.FixedUpdate();

			if (!m_IsMoving)
				return;

			m_InterpolationPercent += (1.0f / m_MotionPeriod) * GameTime.fixedDeltaTime *
									  Mathf.Lerp(m_Point.GetSpeedCoefficient(), m_Point.GetNext().GetSpeedCoefficient(), PercentageThroughPoint());
			if (m_InterpolationPercent >= 1.0f || m_InterpolationPercent <= 0.0f)
				EndOfMotion();

			UpdatePosition();
		}

		/// <summary>
		/// Called at the end of the platform's motion
		/// </summary>
		protected virtual void EndOfMotion()
		{
			StopMoving();
			m_InterpolationPercent = Interpolater.CapZeroToOne(m_InterpolationPercent);
		}

		#endregion

		#region Public Methods/Functions

		/// <summary>
		/// Called when the platform starts moving.
		/// </summary>
		public void StartMoving()
		{
			m_IsMoving = true;
		}

		/// <summary>
		/// Called when the platform stops moving.
		/// </summary>
		public void StopMoving()
		{
			m_IsMoving = false;
		}

		/// <summary>
		/// Turns around the platform.
		/// </summary>
		public void TurnAround()
		{
			m_MotionPeriod = -m_MotionPeriod;
		}

		/// <summary>
		/// Updates the platform position.
		/// </summary>
		public void UpdatePosition()
		{
			float ptp = PercentageThroughPoint();

			if (float.IsNaN(ptp))
				ptp = 0.0f;
			
			if (ptp > 1.0f && m_Point.GetNext().GetNext())
			{
				m_PointDistance += m_Point.DistanceToNext();
				m_Point = m_Point.GetNext();
				ptp -= 1.0f;
			}
			else if (ptp < 0.0f && m_Point.GetPrevious())
			{
				m_Point = m_Point.GetPrevious();
				m_PointDistance -= m_Point.DistanceToNext();
				ptp += 1.0f;
			}

			Vector3 newPosition = m_Point.InterpolatePosition(ptp);
			GetComponent<Rigidbody>().MovePosition(newPosition);
		}

		/// <summary>
		/// Called when the platform is back at the start of its' movement
		/// </summary>
		public void BackToStart()
		{
			m_InterpolationPercent = 0.0f;
			StopMoving();
		}

		/// <summary>
		/// Returns percentages through the platform point.
		/// </summary>
		/// <returns> Percentages through the point.</returns>
		public float PercentageThroughPoint()
		{
			return (m_Terp.Interpolate(m_InterpolationPercent, 0.0f, 1.0f) * m_TotalDistance - m_PointDistance) / m_Point.DistanceToNext();
		}

		#endregion
	}
}