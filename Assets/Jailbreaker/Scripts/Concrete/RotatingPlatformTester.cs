using AntonCoolpecker.Abstract.Platforms;
using System;
using UnityEngine;

namespace Jailbreaker.Concrete
{
	/// <summary>
	/// Rotating platform tester.
	/// </summary>
	public class RotatingPlatformTester : MovingPlatform
	{
		#region Variables

		[SerializeField] private Quaternion m_RotationDelta;

		#endregion

		#region Methods

		/// <summary>
		/// Called every physics timestep.
		/// </summary>
		protected override void FixedUpdate()
		{
			base.FixedUpdate();

			HandleMovement(Vector3.zero, m_RotationDelta);
		}

		#endregion
	}
}