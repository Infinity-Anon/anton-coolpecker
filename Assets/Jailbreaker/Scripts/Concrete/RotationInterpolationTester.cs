﻿using AntonCoolpecker.Utils;
using System.Collections;
using UnityEngine;

namespace Jailbreaker.Concrete
{
	/// <summary>
	/// Rotation interpolation tester.
	/// </summary>
	public class RotationInterpolationTester : MonoBehaviour
	{
		#region Variables

		[SerializeField] private float m_ZeroToOne;
		[SerializeField] private Interpolater m_Interpolater;
		[SerializeField] private float m_Delta;
		[SerializeField] private bool m_UseLargerRotationArc;

		#endregion

		#region Methods

		/// <summary>
		/// Update this instance.
		/// </summary>
		public void Update()
		{
			if (m_ZeroToOne + m_Delta * Time.deltaTime > 1 || m_ZeroToOne + m_Delta * Time.deltaTime < 0)
				m_Delta = -m_Delta;
			
			m_ZeroToOne += m_Delta * Time.deltaTime;
			transform.rotation = m_Interpolater.Interpolate(m_ZeroToOne, Quaternion.LookRotation(Vector3.forward), Quaternion.LookRotation(Vector3.right), m_UseLargerRotationArc);
		}

		#endregion
	}
}