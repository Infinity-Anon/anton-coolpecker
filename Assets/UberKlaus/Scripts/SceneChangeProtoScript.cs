﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Scene change prototype script.
/// </summary>
public class SceneChangeProtoScript : MonoBehaviour 
{
	#region Variables

	public GameObject LSPanel;

	#endregion

	#region Private Functions

	/// <summary>
	/// Raises the trigger enter event.
	/// </summary>
	/// <param name="col">Col.</param>
	void OnTriggerEnter (Collider col)
	{
		if (col.gameObject.tag == "Player")
		{
			LSPanel.SetActive (true);
		}
	}

	/// <summary>
	/// Raises the trigger exit event.
	/// </summary>
	/// <param name="col">Col.</param>
	void OnTriggerExit (Collider col)
	{
		if (col.gameObject.tag == "Player")
		{
			LSPanel.SetActive (false);
		}
	}

	#endregion

	#region Public Functions

	/// <summary>
	/// Go to the hub scene.
	/// </summary>
	public void GoToHub ()
	{
		Application.LoadLevel ("ProtoScene");
	}

	/// <summary>
	/// Goes to first area scene.
	/// </summary>
	public void GoToArea1 ()
	{
		Application.LoadLevel ("Test_area1");
	}

	#endregion
}