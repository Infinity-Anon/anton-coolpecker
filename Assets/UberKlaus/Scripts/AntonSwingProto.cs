﻿using System.Collections;
using UnityEngine;

/// <summary>
/// Anton swing prototype script.
/// </summary>
public class AntonSwingProto : MonoBehaviour 
{
	#region Variables

	public Rigidbody RigidbodyObject;
	private CharacterController CC;

	private bool SwingMode = false;

	#endregion

	#region Methods

	/// <summary>
	/// Start this instance.
	/// </summary>
	void Start () 
	{
		CC = GetComponent<CharacterController>();
	}

	/// <summary>
	/// Update this instance.
	/// </summary>
	void Update ()
	{
		if (Input.GetKeyDown(KeyCode.J))
		{
			PhysicsOnOff ();
		}
	}

	/// <summary>
	/// Sets the settings for the swing physics.
	/// </summary>
	public void PhysicsOnOff ()
	{
		if (SwingMode)
		{
			SwingMode = false;
		}
		else
		{
			SwingMode = true;
		}

		SwingPhysics(SwingMode);
	}

	/// <summary>
	/// Defines the settings for the swing physics.
	/// </summary>
	/// <param name="OnOff">If set to <c>true</c> on off.</param>
	public void SwingPhysics (bool OnOff)
	{
		if (OnOff == true)
		{
			CC.enabled = false;
			RigidbodyObject.isKinematic = false;
		}
		else
		{
			CC.enabled = true;
			RigidbodyObject.isKinematic = true;
		}
	}

	#endregion
}