using AntonCoolpecker.Utils;
using AntonCoolpecker.Utils.Time;
using Hydra.HydraCommon.Utils;
using UnityEngine;

/// <summary>
/// Abstract target follower.
/// </summary>
public abstract class AbstractTargetFollower : MonoBehaviour
{
	#region Variables

	public enum UpdateType                                  // The available methods of updating are:
	{
		Auto,                                               // Let the script decide how to update
		FixedUpdate,                                        // Update in FixedUpdate (for tracking rigidbodies).
		LateUpdate,                                         // Update in LateUpdate. (for tracking objects that are moved in Update)
	}
	
	[SerializeField] protected Transform target;              		// The target object to follow
	[SerializeField] private bool autoTargetPlayer = true; 			// Whether the rig should automatically target the player.
	[SerializeField] private UpdateType updateType;         		// stores the selected update type

	#endregion

	#region Properties

	public Transform Target { get { return this.target; } }

	#endregion

	#region Methods
	
	/// <summary>
	/// Start this instance.
	/// </summary>
	virtual protected void Start() 
	{
		// if auto targeting is used, find the object tagged "Player"
		// any class inheriting from this should call base.Start() to perform this action!

		if (autoTargetPlayer) 
		{
			FindAndTargetPlayer();
		}
	}

	/// <summary>
	/// Fixed update.
	/// </summary>
	void FixedUpdate() 
	{
		// we update from here if updatetype is set to Fixed, or in auto mode,
		// if the target has a rigidbody, and isn't kinematic.

		if (autoTargetPlayer && (target == null || !target.gameObject.activeSelf)) 
		{
			FindAndTargetPlayer();
		}

		if (updateType == UpdateType.FixedUpdate || updateType == UpdateType.Auto && target != null && (target.GetComponent<Rigidbody>() != null && !target.GetComponent<Rigidbody>().isKinematic)) 
		{
			FollowTarget(GameTime.deltaTime);
		}
	}
	
	/// <summary>
	/// Late update.
	/// </summary>
	void LateUpdate() 
	{
		// we update from here if updatetype is set to Late, or in auto mode,
		// if the target does not have a rigidbody, or - does have a rigidbody but is set to kinematic.

		if (autoTargetPlayer && (target == null || !target.gameObject.activeSelf)) 
		{
			FindAndTargetPlayer();
		}

		if (updateType == UpdateType.LateUpdate || updateType == UpdateType.Auto && target != null && (target.GetComponent<Rigidbody>() == null || target.GetComponent<Rigidbody>().isKinematic)) 
		{
			FollowTarget(GameTime.deltaTime);
		}
	}

	/// <summary>
	/// Follows the target.
	/// </summary>
	/// <param name="deltaTime">Delta time.</param>
	protected abstract void FollowTarget(float deltaTime);

	/// <summary>
	/// Finds the player and targets him.
	/// </summary>
	public void FindAndTargetPlayer() 
	{
		// only target if we don't already have a target
		if (target == null) 
		{
			// auto target an object tagged player, if no target has been assigned
			var targetObj = GameObject.FindGameObjectWithTag("Player");	

			if (targetObj) 
			{
				SetTarget(targetObj.transform);
			}
		}
	}

	/// <summary>
	/// Sets the target.
	/// </summary>
	/// <param name="newTransform">New transform.</param>
	public virtual void SetTarget (Transform newTransform) 
	{
		target = newTransform;
	}

	#endregion
}