﻿using System.Collections;
using UnityEngine;

/// <summary>
/// Restart button script.
/// </summary>
public class RestartButtonScript : MonoBehaviour 
{
	#region Functions

	/// <summary>
	/// Restarts the game.
	/// </summary>
	public void RestartGame()
	{
		Application.LoadLevel(0);
	}

	#endregion
}